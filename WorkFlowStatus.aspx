<%@ Page Language="vb" AutoEventWireup="false" Codebehind="WorkflowStatus.aspx.vb" Inherits="docAssistWeb.WorkflowStatus"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title id="pageTitle">Workflow Action</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<table class="PageContent" height="80" cellSpacing="0" cellPadding="0" width="400" border="0">
				<tr>
					<td colspan="2">&nbsp;<img src="Images/actions.gif"></td>
				</tr>
				<tr>
					<td style="WIDTH: 128px; HEIGHT: 16px" vAlign="top"><BR>
						&nbsp;&nbsp;Action Description:
					</td>
					<TD style="HEIGHT: 16px" vAlign="bottom"><asp:textbox id="txtStatusDesc" runat="server" Font-Size="8.25pt" Font-Names="Verdana" Width="208px"
							MaxLength="35"></asp:textbox></TD>
				</tr>
				<tr>
					<td style="WIDTH: 128px; HEIGHT: 15px" align="left">&nbsp;&nbsp;Action Note:<BR>
						&nbsp;&nbsp;<BR>
					</td>
					<TD style="HEIGHT: 15px"><asp:textbox id="txtStatusNote" runat="server" Width="208px" Height="49px" TextMode="MultiLine"
							MaxLength="255"></asp:textbox></TD>
				</tr>
				<tr>
					<td style="WIDTH: 128px">&nbsp; Route Action:&nbsp;</td>
					<TD><asp:dropdownlist id="ddlRouteAction" runat="server" Width="208px">
							<asp:ListItem Value="F">Forward</asp:ListItem>
							<asp:ListItem Value="B">Back</asp:ListItem>
							<asp:ListItem Value="E">End</asp:ListItem>
						</asp:dropdownlist>
						<asp:label id="lblError" runat="server" Font-Names="Verdana" Font-Size="8.25pt" ForeColor="Red"
							Visible="False"></asp:label></TD>
				</tr>
				<tr>
					<td style="WIDTH: 128px" align="right"><asp:literal id="litReload" runat="server"></asp:literal></td>
					<TD vAlign="top" align="right">
						<a href="#" onclick="javascript:window.close()"><img src="Images/btn_cancel.gif" border="0"></a>
						<asp:imagebutton id="btnSave" runat="server" ImageUrl="Images/btn_save.gif"></asp:imagebutton></TD>
				</tr>
			</table>
		</form>
		<script language="javascript">
			
			function submitForm()
			{
				if (event.keyCode == 13)
				{
					event.cancelBubble = true;
					event.returnValue = false;
					document.all.btnSave.click();
				}
			}
		</script>
	</body>
</HTML>

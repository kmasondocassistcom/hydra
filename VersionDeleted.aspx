<%@ Page Language="vb" AutoEventWireup="false" Codebehind="VersionDeleted.aspx.vb" Inherits="docAssistWeb.VersionDeleted"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>docAssist - Oops!</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body MS_POSITIONING="GridLayout" bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="frmVersionDeleted" method="post" runat="server">
			<table width="100%" height="100%">
				<tr>
					<td><img src="dummy.gif" width="50" height="1"></td>
					<td align="left"><img src="Images/errorPage/128_integration.gif"><BR>
						<BR>
						<BR>
					</td>
					<td><img src="dummy.gif" width="20" height="1">
						<asp:ImageButton id="btnStart" runat="server" Height="0px" Width="0px"></asp:ImageButton></td>
					<td width="100%"><span style="FONT-WEIGHT: bold; FONT-SIZE: 24pt; COLOR: gray; FONT-FAMILY: 'Trebuchet MS'">Oops!</span><br>
						<br>
						<span style="FONT-SIZE: 12pt; COLOR: gray; FONT-FAMILY: 'Trebuchet MS'">This 
							document is no longer available. It may have been deleted by another user or 
							processed&nbsp;by a system service.<BR>
							<BR>
						</span>
						<br>
						<br>
						<br>
						<br>
					</td>
					<td></td>
					<td width="50"></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>

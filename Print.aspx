<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="Controls/MenuBar.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="Print.aspx.vb" Inherits="docAssistWeb.Print"%>
<%@ Register TagPrefix="uc1" TagName="docHeader" Src="Controls/docHeader.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<TITLE>Print Document</TITLE>
		<META content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<META content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<META content="JavaScript" name="vs_defaultClientScript">
		<META content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
		<base target="_self">
	</HEAD>
	<BODY id="page_body" bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0" runat="server">
		<FORM id="Form1" method="post" runat="server">
			<p>Your document will open in the Adobe Acrobat reader. Once it has loaded you can print it.</p>
			<TABLE class="PageContent" cellSpacing="0" cellPadding="0" width="300">
				<TR>
					<TD><ASP:RADIOBUTTON id="rdoAllPages" onclick="rdoPages_click(this);" runat="server" Checked="True"></ASP:RADIOBUTTON>&nbsp;All 
						Pages</TD>
				</TR>
				<TR>
					<TD><ASP:RADIOBUTTON id="rdoRange" onclick="rdoPages_click(this);" runat="server"></ASP:RADIOBUTTON>&nbsp;From
						<ASP:TEXTBOX id="txtRangeFrom" runat="server" Width="37px" Font-Size="8.25pt" Font-Names="Verdana"
							Enabled="False" Height="22px">1</ASP:TEXTBOX>&nbsp;to&nbsp;
						<ASP:TEXTBOX id="txtRangeTo" runat="server" Width="38px" Font-Size="8.25pt" Font-Names="Verdana"
							Enabled="False" Height="22px"></ASP:TEXTBOX></TD>
				</TR>
				<TR style="VISIBILITY:hidden">
					<TD id="rowRedaction" runat="server"><asp:checkbox id="chkRedactions" runat="server" Text="Remove Redactions" DESIGNTIMEDRAGDROP="162"></asp:checkbox></TD>
				</TR>
				<TR>
					<TD id="rowAnnotations" runat="server"><asp:checkbox id="chkAnnotations" runat="server" Text="Include Annotations"></asp:checkbox></TD>
				</TR>
				<tr>
					<td align="right"><ASP:IMAGEBUTTON id="imgExport" runat="server" ImageUrl="Images/btn_small_ok.gif"></ASP:IMAGEBUTTON><BR>
						<asp:Label id="lblPageError" runat="server" Visible="False" ForeColor="Red" CssClass="Error"></asp:Label></td>
				</tr>
			</TABLE>
			<INPUT id="hidPostCount" type="hidden" value="1" name="hidPostCount" RUNAT="server">
			<SCRIPT language="javascript">
			
			var bRunning 
			var submitted = false
			
			function disableOptions()
			{
				if (document.all.chkRedactions != undefined)
				{
					document.all.chkRedactions.disabled = true;
					document.all.chkRedactions.checked = false;
				}
				
				if (document.all.chkAnnotations != undefined)
				{
					document.all.chkAnnotations.disabled = true;
					document.all.chkAnnotations.checked = false;
				}
			
				//disable upper controls
				document.all.rdoRange.disabled = true;
				document.all.txtRangeFrom.disabled = true;
				document.all.txtRangeTo.disabled = true;
				document.all.rdoAllPages.disabled = true;
			}
			
			function enableOptions()
			{
				if (document.all.chkRedactions != undefined)
				{
					document.all.chkRedactions.disabled = false;
				}
				if (document.all.chkAnnotations != undefined)
				{
					document.all.chkAnnotations.disabled = false;
				}					

				//enable upper controls
				document.all.rdoRange.disabled = false;
				document.all.txtRangeFrom.disabled = false;
				document.all.txtRangeTo.disabled = false;
				document.all.rdoAllPages.disabled = false;
			}
			
			function nb_numericOnly(evt)
			{
				evt = (evt) ? evt : event;
				var charCode = (evt.charCode) ? evt.charCode : ((evt.keyCode) ? evt.keyCode
				: ((evt.which) ? evt.which : 0));
				if (charCode > 31 && (charCode < 48 || charCode >57)) {return false;}
				return true;
			}

			function disable()
			{
				if(submitted == false)
				{
			    submitted = true;
				document.body.style.cursor = "wait";
				}
				else
				{
				document.all.imgSend.disabled = true;
				}
			}
				
			function rdoPages_click(obj)
			{
				if (obj.id == "rdoAllPages")
				{
					document.all.rdoRange.checked = false;
					document.all.txtRangeFrom.disabled = true;
					document.all.txtRangeTo.disabled = true;
				} else
				{
					document.all.rdoAllPages.checked = false;
					document.all.txtRangeFrom.disabled = false;
					document.all.txtRangeTo.disabled = false;
				}
			}

			function catchSubmit()
			{
				if (event.keyCode == 13)
				{
					event.cancelBubble = true;
					event.returnValue = false;
					//document.all.lnkSearch.click();
					return false;
				}
			}
						
			</SCRIPT>
		</FORM>
	</BODY>
</HTML>

Partial Class ManageIntegration
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Session("mAdminMapTable") = "INTEGRATION" Then
            lnkAddMap.Text = "Create Search Lookup"
            lnkChangeMap.Text = "Change Search Lookup"
            lnkRemoveMap.Text = "Remove Search Lookup"

        Else
            lnkAddMap.Text = "Create Index"
            lnkChangeMap.Text = "Change Index"
            lnkRemoveMap.Text = "Remove Index"

        End If
    End Sub

    Private Sub lnkAddMap_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkAddMap.Click

        Response.Redirect("ManageMapAdd.aspx")
    End Sub

    Private Sub lnkChangeMap_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkChangeMap.Click
        Response.Redirect("ManageMapChange.aspx")
    End Sub

    Private Sub lnkRemoveMap_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkRemoveMap.Click
        Session("mAdminMapMode") = "REMOVE"
        Response.Redirect("ManageMapRemove.aspx")
    End Sub

End Class

Partial Class ManageDocumentsAttributesRequiredChange
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents Imagebutton1 As System.Web.UI.WebControls.ImageButton
    Protected WithEvents Imagebutton2 As System.Web.UI.WebControls.ImageButton
    Protected WithEvents Label2 As System.Web.UI.WebControls.Label
    Protected WithEvents Label3 As System.Web.UI.WebControls.Label
    Protected WithEvents Label5 As System.Web.UI.WebControls.Label
    Protected WithEvents TextBox1 As System.Web.UI.WebControls.TextBox

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region


    Private mobjUser As Accucentric.docAssist.Web.Security.User
    Private mconSqlImage As SqlClient.SqlConnection
    Dim dtAttributesSelected As DataTable


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
        Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))

        Dim conSql As New SqlClient.SqlConnection

        Try

            If Not Page.IsPostBack = True Then

                txtDocumentName.Text = Session("mAdminDocumentName")
                dtAttributesSelected = Session("SelectedAttributes")
                lstAttributes.DataTextField = "AttributeName"
                lstAttributes.DataValueField = "AttributeID"
                lstAttributes.DataSource = dtAttributesSelected
                lstAttributes.DataBind()
                SetAttributesRequired()

                conSql = Functions.BuildConnection(Me.mobjUser)

                For Each dr As DataRow In dtAttributesSelected.Rows

                    Dim cmdSql As New SqlClient.SqlCommand("acsAdminAttributeListValidate")
                    cmdSql.Connection = conSql
                    cmdSql.CommandType = CommandType.StoredProcedure
                    cmdSql.Parameters.Add("@DocumentId", Session("mAdminDocumentID"))
                    cmdSql.Parameters.Add("@AttributeId", dr("AttributeId"))
                    Dim da As New SqlClient.SqlDataAdapter(cmdSql)
                    Dim dt As New DataTable
                    da.Fill(dt)

                    If dt.Rows(0)("AttributeDataType") = "ListView" Then

                        Dim listItemValidate As New ListItem(dr("AttributeName"))
                        listItemValidate.Value = dr("AttributeId")

                        If dt.Rows(0)("ListValidate") = True Then
                            listItemValidate.Selected = True
                        Else
                            listItemValidate.Selected = False
                        End If

                        lstValidate.Items.Add(listItemValidate)

                        Dim listItem As New ListItem(dr("AttributeName"))
                        listItem.Value = dr("AttributeId")

                        If dt.Rows(0)("ListViewByCabinet") = True Then
                            listItem.Selected = True
                        Else
                            listItem.Selected = False
                        End If

                        lstCabFilter.Items.Add(listItem)

                    End If

                Next

            End If

        Catch ex As Exception

        Finally

            If conSql.State <> ConnectionState.Closed Then
                conSql.Close()
                conSql.Dispose()
            End If

        End Try

    End Sub

    Private Sub Imagebutton3_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Imagebutton3.Click
        If DocumentExists(txtDocumentName.Text) And txtDocumentName.Text <> Session("mAdminDocumentName") Then
            lblError.Text = "Document " & txtDocumentName.Text & " already exists."
            Exit Sub
        End If


        lblError.Text = ""
        UpdateDocument(txtDocumentName.Text)
        UpdateDocumentAttributes(Session("mAdminDocumentID"))
        Response.Redirect("ManageDocuments.aspx")
    End Sub

    Private Function UpdateDocument(ByVal strDocumentName As String) As Boolean

        Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))
        Me.mobjUser.AccountId = guidAccountId
        Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
        Me.mconSqlImage.Open()


        Dim cmdSql As SqlClient.SqlCommand
        Dim dtsql As New DataTable
        cmdSql = New SqlClient.SqlCommand("acsAdminDocumentChange", Me.mconSqlImage)
        cmdSql.CommandType = CommandType.StoredProcedure
        cmdSql.Parameters.Add("@DocumentID", Session("mAdminDocumentID"))
        cmdSql.Parameters.Add("@VersionMode", Session("VersionMode"))
        cmdSql.Parameters.Add("@NewDocumentName", strDocumentName)
        cmdSql.ExecuteNonQuery()

    End Function



    Private Function UpdateDocumentFolders() As Boolean
        Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
        Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))

        Me.mobjUser.AccountId = guidAccountId
        Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
        Me.mconSqlImage.Open()

        Dim cmdSql As SqlClient.SqlCommand
        Dim dr As DataRow

        'Clear any existing document locations
        cmdSql = New SqlClient.SqlCommand("acsAdminDocumentLocationsClear", Me.mconSqlImage)
        cmdSql.CommandType = CommandType.StoredProcedure
        cmdSql.Parameters.Add("@DocumentID", Session("mAdminDocumentID"))
        cmdSql.ExecuteNonQuery()


        For Each dr In Session("FoldersSelected").rows

            cmdSql = New SqlClient.SqlCommand("acsAdminDocumentLocationsAdd", Me.mconSqlImage)
            cmdSql.CommandType = CommandType.StoredProcedure
            cmdSql.Parameters.Add("@FolderID", dr("FolderID"))
            cmdSql.Parameters.Add("@DocumentID", Session("mAdminDocumentID"))
            cmdSql.ExecuteNonQuery()

        Next


    End Function


    Private Sub UpdateDocumentAttributes(ByVal iNewDocumentID As Integer)

        Try

            Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
            Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))

            Me.mobjUser.AccountId = guidAccountId
            Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
            Me.mconSqlImage.Open()


            Dim cmdSql As SqlClient.SqlCommand

            'Clear any existing document attributes
            cmdSql = New SqlClient.SqlCommand("acsAdminDocumentAttributesClear", Me.mconSqlImage)
            cmdSql.CommandType = CommandType.StoredProcedure
            cmdSql.Parameters.Add("@DocumentID", iNewDocumentID)
            cmdSql.ExecuteNonQuery()

            Dim icount As Integer = lstAttributes.Items.Count
            Dim x As Integer

            For x = 0 To icount - 1

                cmdSql = New SqlClient.SqlCommand("acsAdminDocumentAttributesAdd", Me.mconSqlImage)
                cmdSql.CommandType = CommandType.StoredProcedure
                cmdSql.Parameters.Add("@DocumentID", iNewDocumentID)
                cmdSql.Parameters.Add("@AttributeID", lstAttributes.Items(x).Value)
                cmdSql.Parameters.Add("@AttributeDisplayOrder", x + 1)
                cmdSql.Parameters.Add("@Required", lstAttributes.Items(x).Selected)

                For Each lstItem As ListItem In lstValidate.Items
                    If lstAttributes.Items(x).Value = lstItem.Value And lstItem.Selected Then
                        cmdSql.Parameters.Add("@ListValidate", True)
                        Exit For
                    End If
                Next

                For Each lstFilterItem As ListItem In lstCabFilter.Items
                    If lstAttributes.Items(x).Value = lstFilterItem.Value And lstFilterItem.Selected Then
                        cmdSql.Parameters.Add("@ListViewByCabinet", True)
                        Exit For
                    End If
                Next

                cmdSql.ExecuteNonQuery()

            Next

        Catch ex As Exception

        End Try

    End Sub

    Private Function DocumentExists(ByVal strDocumentName As String) As Boolean
        Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
        Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))

        Me.mobjUser.AccountId = guidAccountId
        Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
        Me.mconSqlImage.Open()

        Dim cmdSql As SqlClient.SqlCommand

        Dim dtsql As New DataTable
        cmdSql = New SqlClient.SqlCommand("acsAdminDocumentCheck", Me.mconSqlImage)
        cmdSql.CommandType = CommandType.StoredProcedure
        cmdSql.Parameters.Add("@DocumentName", strDocumentName)
        Dim dasql As New SqlClient.SqlDataAdapter(cmdSql)
        dasql.Fill(dtsql)


        If dtsql.Rows.Count > 0 Then
            Return True
        Else
            Return False

        End If
    End Function



    Private Sub SetAttributesRequired()

        Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
        Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))

        Me.mobjUser.AccountId = guidAccountId
        Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
        Me.mconSqlImage.Open()

        Dim lstItem As System.Web.UI.WebControls.ListItem

        For Each lstItem In lstAttributes.Items

            Dim cmdSql As SqlClient.SqlCommand
            Dim dtsql As New DataTable
            cmdSql = New SqlClient.SqlCommand("acsAdminAttributeRequired", Me.mconSqlImage)
            cmdSql.CommandType = CommandType.StoredProcedure
            cmdSql.Parameters.Add("@DocumentID", Session("mAdminDocumentID"))
            cmdSql.Parameters.Add("@AttributeID", lstItem.Value)
            Dim da As New SqlClient.SqlDataAdapter(cmdSql)
            da.Fill(dtsql)
            If dtsql.Rows.Count > 0 Then
                If dtsql.Rows(0).Item(0) = True Then
                    lstItem.Selected = True
                End If
            End If
            dtsql.Clear()

        Next

    End Sub



    Private Sub Imagebutton4_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Imagebutton4.Click
        Response.Redirect("ManageDocuments.aspx")

    End Sub
End Class

Imports Accucentric.docAssist

Partial Class DefaultAccount
    Inherits System.Web.UI.Page

    Private mobjUser As Web.Security.User

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        '
        ' Build the user object
        '
        Try
            Me.mobjUser = BuildUserObject(Request.Cookies("docAssist"))

        Catch ex As Exception

        End Try

        If Me.mobjUser Is Nothing Then Response.Redirect("Default.aspx")


        If Not Page.IsPostBack Then
            Dim dtAccounts As New Web.Data.dtUserAccounts
            Dim oEnc As New Encryption.Encryption
            dtAccounts.Fill(oEnc.Decrypt(ConfigurationSettings.AppSettings("Connection")), Me.mobjUser.UserId.ToString)

            If dtAccounts.Rows.Count > 0 Then
                Dim dv As New DataView(dtAccounts, Nothing, "AccountDesc", DataViewRowState.CurrentRows)
                ddlAccounts.DataSource = dv
                ddlAccounts.DataTextField = "AccountDesc"
                ddlAccounts.DataValueField = dtAccounts.AccountId.ColumnName
                ddlAccounts.DataBind()

                Dim dvDefault As New DataView(dtAccounts, dtAccounts.DefaultAccount.ColumnName & "=True", Nothing, DataViewRowState.CurrentRows)
                Dim guidDefault As System.Guid
                If dvDefault.Count > 0 Then

                End If
                '
                ' highlight default account
                For Each li As DataListItem In ddlAccounts.Items
                Next
            Else
                Response.Redirect("default.aspx")
            End If

        End If

    End Sub

End Class

Partial Class ScanOptions
    Inherits System.Web.UI.Page

    Private mobjUser As Accucentric.docAssist.Web.Security.User

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try
            Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
            Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))
            Me.mobjUser.AccountId = guidAccountId
        Catch ex As Exception
            Response.Redirect("Default.aspx")
        End Try

        If Not Page.IsPostBack Then

            Try

                Dim ws As New docAssistImages
                Dim ds As New DataSet

                ds = ws.IndexerOptions(Me.mobjUser.UserId.ToString, Me.mobjUser.AccountId.ToString, Nothing)

                Dim dv As New DataView(ds.Tables(0), "OptionKey='SCANWARN'", Nothing, DataViewRowState.OriginalRows)

                If dv.Count = 0 Then

                    WarningSize.Text = "0"
                    chkWarning.Checked = False

                Else

                    If dv(0)("OptionValue") = True Then
                        chkWarning.Checked = True
                    Else
                        chkWarning.Checked = False
                    End If

                    'Load size
                    dv = New DataView(ds.Tables(0), "OptionKey='SCANWARNSIZE'", Nothing, DataViewRowState.OriginalRows)
                    If dv.Count = 0 Then
                        WarningSize.Text = "0"
                    Else
                        WarningSize.Text = dv(0)("OptionValue")
                    End If

                End If


            Catch ex As Exception

            End Try

        End If

    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click

        If Not IsNumeric(WarningSize.Text) Then
            lblMessage.Text = "Warning size nust be numeric."
            Exit Sub
        End If

        Try
            Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
            Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))
            Me.mobjUser.AccountId = guidAccountId
        Catch ex As Exception
            Response.Redirect("Default.aspx")
        End Try

        Dim conSqlCompany As SqlClient.SqlConnection

        Try

            conSqlCompany = Functions.BuildConnection(Me.mobjUser)
            conSqlCompany.Open()

            Dim cmdSql As New SqlClient.SqlCommand("acsSetIndexerValue", conSqlCompany)
            cmdSql.CommandType = CommandType.StoredProcedure
            cmdSql.Parameters.Add("@UserID", Me.mobjUser.ImagesUserId)
            cmdSql.Parameters.Add("@OptionKey", "SCANWARN")
            cmdSql.Parameters.Add("@OptionValue", chkWarning.Checked)
            cmdSql.ExecuteNonQuery()

            cmdSql.Parameters.Clear()
            cmdSql.Parameters.Add("@UserID", Me.mobjUser.ImagesUserId)
            cmdSql.Parameters.Add("@OptionKey", "SCANWARNSIZE")
            cmdSql.Parameters.Add("@OptionValue", WarningSize.Text)
            cmdSql.ExecuteNonQuery()

            lblMessage.Text = "Settings saved."

        Catch ex As Exception

            lblMessage.Text = "An error has occured. Please try again."

        Finally

            If conSqlCompany.State <> ConnectionState.Closed Then
                conSqlCompany.Close()
                conSqlCompany.Dispose()
            End If

        End Try

    End Sub

End Class

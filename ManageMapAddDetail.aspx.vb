Partial Class ManageMapAddDetail
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents dgMapping As System.Web.UI.WebControls.DataGrid

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region


    Private mobjUser As Accucentric.docAssist.Web.Security.User
    Private mconSqlImage As SqlClient.SqlConnection
    Dim dtMapping As New DataTable



    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Not Page.IsPostBack Then
            lblCurrent.Text = Session("mAdminIntegrationPublisherName") & " / " & _
            Session("mAdminIntegrationProductName") & " / " & _
            Session("mAdminIntegrationVersion") & " / " & _
            Session("mAdminIntegrationFormName")
            PopulateDocumentList()
            'PopulateAttributeList()
            '            PopulateFieldList()



        End If
    End Sub

    

    Private Sub PopulateDocumentList()

        Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
        Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))

        Me.mobjUser.AccountId = guidAccountId
        Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
        Me.mconSqlImage.Open()

        Dim cmdSql As SqlClient.SqlCommand

        Dim dtsql As New DataTable
        cmdSql = New SqlClient.SqlCommand("acsAdminIntegrationDocuments", Me.mconSqlImage)
        cmdSql.CommandType = CommandType.StoredProcedure
        Dim dasql As New SqlClient.SqlDataAdapter(cmdSql)
        dasql.Fill(dtsql)

        If dtsql.Rows.Count <= 0 Then
            'TODO: No Cabinets to change
            cmbDocument.Items.Clear()
        Else
            cmbDocument.DataTextField = "DocumentName"
            cmbDocument.DataValueField = "DocumentID"
            cmbDocument.DataSource = dtsql
            cmbDocument.DataBind()

        End If

    End Sub

    


   
    Private Sub btnNext_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnNext.Click
        
        If DocumentUsed = True Then
            lblError.Text = "Document already used in an existing integration."
            Exit Sub
        End If
        Session("mAdminIntegrationDocumentID") = cmbDocument.SelectedValue
        Session("mAdminIntegrationDocumentName") = cmbDocument.SelectedItem.Text
        Response.Redirect("ManageMapAddDetailMap.aspx")


    End Sub




    Private Function DocumentUsed() As Boolean
        Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
        Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))

        Dim strMode As String
        If Session("mAdminMapTable") = "INTEGRATION" Then strMode = 0 Else strMode = 1
        Me.mobjUser.AccountId = guidAccountId
        Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
        Me.mconSqlImage.Open()

        Dim cmdSql As SqlClient.SqlCommand

        Dim dtsql As New DataTable
        cmdSql = New SqlClient.SqlCommand("acsAdminDocumentIntegrationCheck", Me.mconSqlImage)
        cmdSql.CommandType = CommandType.StoredProcedure
        cmdSql.Parameters.Add("@DocumentID", cmbDocument.SelectedValue)
        cmdSql.Parameters.Add("@FormID", Session("mAdminIntegrationFormID"))
        cmdSql.Parameters.Add("@Mode", strMode)
        Dim dasql As New SqlClient.SqlDataAdapter(cmdSql)
        dasql.Fill(dtsql)


        If dtsql.Rows.Count > 0 Then
            Return True
        Else
            Return False

        End If

    End Function

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("ManageIntegration.aspx")

    End Sub
End Class

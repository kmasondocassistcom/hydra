Partial Class ManageAttributesChangeList
    Inherits System.Web.UI.Page

    Private mconSqlImage As SqlClient.SqlConnection
    Private mobjUser As Accucentric.docAssist.Web.Security.User
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        txtListAdd.Attributes("onkeypress") = "catchSubmit();"

        If Not Page.IsPostBack Then
            txtListAdd.MaxLength = 75
            lblAttributeName.Text = Session("mAdminAttributeName")
            PopulateListItems()
        End If

    End Sub



    Private Function AttributeNameExists() As Boolean
        Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
        Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))

        Me.mobjUser.AccountId = guidAccountId
        Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
        Me.mconSqlImage.Open()

        Dim cmdSql As SqlClient.SqlCommand

        Dim dtsql As New DataTable
        cmdSql = New SqlClient.SqlCommand("acsAdminAttributeCheck", Me.mconSqlImage)
        cmdSql.CommandType = CommandType.StoredProcedure
        cmdSql.Parameters.Add("@AttributeName", Session("mAdminAttributeName"))
        Dim dasql As New SqlClient.SqlDataAdapter(cmdSql)
        dasql.Fill(dtsql)


        If dtsql.Rows.Count > 0 Then
            Return True
        Else
            Return False

        End If

    End Function

    Private Function SaveAttributeChange() As Boolean

        Dim cmdSql As SqlClient.SqlCommand
        Dim dtsql As New DataTable
        cmdSql = New SqlClient.SqlCommand("acsAdminAttributeChange", Me.mconSqlImage)
        cmdSql.CommandType = CommandType.StoredProcedure
        cmdSql.Parameters.Add("@AttributeID", Session("mAdminAttributeID"))
        cmdSql.Parameters.Add("@AttributeName", Session("mAdminAttributeName"))
        cmdSql.Parameters.Add("@AttributeDataType", "ListView")
        cmdSql.Parameters.Add("@DataFormat", "")
        cmdSql.Parameters.Add("@Length", "")
        cmdSql.ExecuteNonQuery()


        'Clear List
        cmdSql = New SqlClient.SqlCommand("acsAdminAttributeListsClear", Me.mconSqlImage)
        cmdSql.CommandType = CommandType.StoredProcedure
        cmdSql.Parameters.Add("@AttributeID", Session("mAdminAttributeID"))
        cmdSql.ExecuteNonQuery()

        'Save List
        Dim objlstitem As Web.UI.WebControls.ListItem
        For Each objlstitem In lstItems.Items
            cmdSql = New SqlClient.SqlCommand("acsAdminAttributeListsAdd", Me.mconSqlImage)
            cmdSql.CommandType = CommandType.StoredProcedure
            cmdSql.Parameters.Add("@AttributeID", Session("mAdminAttributeID"))
            cmdSql.Parameters.Add("@ValueDescription", objlstitem.Text)
            cmdSql.ExecuteNonQuery()
        Next






    End Function



    Private Sub Linkbutton2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        If lstItems.Items.Count <= 0 Then
            lblError.Text = "List must contain at least one item"
            Exit Sub
        End If

        If AttributeNameExists() And lblAttributeName.Text <> Session("mAdminAttributeName") Then
            lblError.Text = "Attribute name already exists."
            Exit Sub
        Else

            SaveAttributeChange()
            Response.Redirect("ManageAttributes.aspx")
        End If


    End Sub


    Private Function PopulateListItems() As Boolean
        Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
        Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))

        Me.mobjUser.AccountId = guidAccountId
        Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
        Me.mconSqlImage.Open()

        Dim cmdSql As SqlClient.SqlCommand

        Dim dtsql As New DataTable
        cmdSql = New SqlClient.SqlCommand("acsAdminAttributeListValues", Me.mconSqlImage)
        cmdSql.CommandType = CommandType.StoredProcedure
        cmdSql.Parameters.Add("@AttributeID", Session("mAdminAttributeID"))
        Dim dasql As New SqlClient.SqlDataAdapter(cmdSql)
        dasql.Fill(dtsql)


        If dtsql.Rows.Count > 0 Then

            Dim dr As DataRow
            For Each dr In dtsql.Rows
                lstItems.Items.Add(dr("ValueDescription"))
            Next


        End If

    End Function


    Private Sub btnFinish_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFinish.Click
        If lstItems.Items.Count <= 0 Then
            lblError.Text = "List must contain at least one item"
            Exit Sub
        End If

        If AttributeNameExists() And lblAttributeName.Text <> Session("mAdminAttributeName") Then
            lblError.Text = "Attribute name already exists."
            Exit Sub
        Else

            SaveAttributeChange()
            Response.Redirect("ManageAttributes.aspx")
        End If
    End Sub

    Private Sub btnMoveUp_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnMoveUp.Click

        If lstItems.Items.Count = 0 Then Exit Sub

        Dim i As Integer = lstItems.SelectedIndex

        If i > 0 Then
            Dim listItem As System.Web.UI.WebControls.ListItem = lstItems.SelectedItem
            lstItems.Items.RemoveAt(i)
            lstItems.Items.Insert(i - 1, listItem)
            lstItems.SelectedIndex = i - 1
            lblError.Text = ""
        End If

    End Sub

    Private Sub btnMoveDown_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnMoveDown.Click

        If lstItems.Items.Count = 0 Then Exit Sub

        Dim i As Integer = lstItems.SelectedIndex

        If i < lstItems.Items.Count - 1 Then
            Dim listItem As System.Web.UI.WebControls.ListItem = lstItems.SelectedItem
            lstItems.Items.RemoveAt(i)
            lstItems.Items.Insert(i + 1, listItem)
            lstItems.SelectedIndex = i + 1
            lblError.Text = ""
        End If

    End Sub

    Private Sub btnRemove_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnRemove.Click

        If lstItems.Items.Count = 0 Then Exit Sub
        If lstItems.SelectedIndex = -1 Then Exit Sub


        Dim i As Integer = lstItems.SelectedIndex

        Dim listItem As System.Web.UI.WebControls.ListItem = lstItems.SelectedItem
        lstItems.Items.RemoveAt(i)
        lblError.Text = ""

    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAdd.Click

        If txtListAdd.Text.Trim = "" Then Exit Sub

        'Check if item exist
        Dim listItem As System.Web.UI.WebControls.ListItem

        For Each listItem In lstItems.Items
            If listItem.Text.ToUpper = txtListAdd.Text.ToUpper Then
                lblError.Text = "Value already exist in this list."
                Exit Sub
            End If
        Next

        lstItems.Items.Add(txtListAdd.Text)
        txtListAdd.Text = ""
        lblError.Text = ""

    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("ManageAttributes.aspx")
    End Sub
End Class

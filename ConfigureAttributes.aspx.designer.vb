﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'     Runtime Version:2.0.50727.3082
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On



Partial Public Class _Attributes

    '''<summary>
    '''Form1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Form1 As Global.System.Web.UI.HtmlControls.HtmlForm

    '''<summary>
    '''hostname control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hostname As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''docHeader control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents docHeader As Global.docAssistWeb.docHeader

    '''<summary>
    '''AdminNav1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents AdminNav1 As Global.docAssistWeb.AdminNav

    '''<summary>
    '''lblPageHeading control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPageHeading As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''ddlDocuments control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlDocuments As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''lstAvailable control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lstAvailable As Global.System.Web.UI.WebControls.ListBox

    '''<summary>
    '''lstAssigned control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lstAssigned As Global.System.Web.UI.WebControls.ListBox

    '''<summary>
    '''hiddenDocumentAttributes control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hiddenDocumentAttributes As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''txtAttributeName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtAttributeName As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''ddlDataType control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlDataType As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''txtDataFormat control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtDataFormat As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtLength control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtLength As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtList control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtList As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''showMessage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents showMessage As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''btnSave control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnSave As Global.System.Web.UI.WebControls.LinkButton
End Class

Imports Accucentric.docAssist
Imports Accucentric.docAssist.Web
Imports Accucentric.docAssist.Web.Security
Imports Accucentric.docAssist.Web.Security.Functions
Imports Accucentric.docAssist.Data
Imports Accucentric.docAssist.Data.Images

Imports Atalasoft.Imaging
Imports Atalasoft.Imaging.Codec
Imports Atalasoft.Imaging.Codec.Tiff
Imports Atalasoft.Imaging.Drawing
Imports Atalasoft.Imaging.ImageProcessing
Imports Atalasoft.Imaging.ImageProcessing.Document
Imports Atalasoft.Imaging.ImageProcessing.Transforms
Imports Atalasoft.Imaging.WebControls
Imports Atalasoft.Imaging.WebControls.Annotations
Imports Atalasoft.Annotate
Imports Atalasoft.Annotate.UI
Imports Atalasoft.Annotate.Renderer

Imports System.IO
Imports System.Reflection
Imports System.Web.SessionState
Imports System.Runtime.Serialization
Imports System.Runtime.Serialization.SerializationBinder

Namespace ViewDocument

    Partial Class ViewDocument
        Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private mobjUser As Accucentric.docAssist.Web.Security.User
        Private dsVersionControl As New Accucentric.docAssist.Data.dsVersionControl
        Private SecurityLevel As Web.Security.SecurityLevel
        Private wfMode As Functions.WorkflowMode
        Private fileType As Functions.FileType

        Private conMaster As New SqlClient.SqlConnection
        Private conCompany As New SqlClient.SqlConnection

        Private versionId As Integer = 0
        Private docId As Integer = 0
        Private pageCount As Integer = 0

        Public Sub New()
            AnnotationRenderers.Add(GetType(StickyNote), New TextAnnotationRenderingEngine)
            AnnotationRenderers.Add(GetType(TextTool), New TextAnnotationRenderingEngine)
        End Sub

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

            Try

                CheckForTimeout()

                Me.conCompany = Functions.BuildConnection(Me.mobjUser)
                Me.conMaster = Functions.BuildMasterConnection

                If IsNothing(Request.QueryString("VID")) Or Not IsNumeric(Request.QueryString("VID")) Then
                    'Todo: Invalid Version ID

                Else
                    versionId = Request.QueryString("VID")
                End If

                If Not Page.IsPostBack Then

                    'Try
                    '    'Find current row
                    '    Dim dc As New DataColumn("VersionId")
                    '    Session("dtVersionId").PrimaryKey = New DataColumn() {Session("dtVersionId").Columns("VersionId")}
                    '    Dim tmpDr As DataRow = Session("dtVersionId").Rows.Find(versionId)
                    '    Session("intCurrentRow") = (tmpDr("RowID") + 1)
                    '    Session("intPrevNextVersionId") = versionId
                    'Catch ex As Exception

                    'End Try

                    Try

                        LoadDocument()

                        LoadAttributes()

                        LoadWorkflow()

                        LoadToolbars()

                        LoadThumbnails()

                        'Log Web View
                        Functions.TrackRecentItem(Me.mobjUser, "VW", versionId)

                        Select Case fileType

                            Case Functions.FileType.ATTACHMENT
                                'If Request.QueryString("ref") = "pdfviewer" Then
                                '    Session("Filename") = Nothing
                                '    Dim returns As String = BuildPage(Me.mcookieSearch.VersionID, 1, Me.mobjUser.DefaultQuality, True)
                                '    frameFile.Visible = False
                                '    ReturnScripts.Add("pdfViewer = true;")
                                '    tblThumbnails.Visible = False
                                '    tdAnnotations.Visible = False
                                'Else
                                '    ImageType.Value = "3"
                                '    tblThumbnails.Visible = False
                                '    objViewer.Visible = False
                                '    Page.Controls.Remove(objViewer)
                                '    objViewer = Nothing
                                '    frameFile.Visible = True
                                '    frameFile.Attributes("src") = "ViewAttachments.aspx"
                                '    tdAnnotations.Visible = False
                                '    frameAttributes.Attributes("src") = "ViewerAttributes.aspx"
                                '    ReturnScripts.Add("pdfViewer = false;")
                                'End If
                            Case Functions.FileType.SCAN

                                LoadImage()
                                'Dim returns As String = BuildPage(versionId, 1, Me.mobjUser.DefaultQuality)
                                'ReturnScripts.Add("var res = " & returns & ";clearQuality();if (res > 0) { addQuality('Low','1'); }if (res >= 50) { addQuality('Medium','50');	}if (res >= 100) { addQuality('High','100'); }selectQuality(document.all.DownloadQuality.value);")
                                'frameFile.Visible = False
                                'ReturnScripts.Add("pdfViewer = false;")
                        End Select

                        'If Request.QueryString("WFA") = "1" Then
                        '    frameAttributes.Attributes("src") = "ViewerAttributes.aspx?WFA=1"
                        'Else
                        '    frameAttributes.Attributes("src") = "ViewerAttributes.aspx"
                        'End If

                        'If intRestrict = 0 Then
                        '    Me.mcookieSearch.HideVersionDropDownList = True
                        'Else
                        '    Me.mcookieSearch.HideVersionDropDownList = False
                        'End If

                        'pageNo.Value = "1"
                        'ReturnControls.Add(pageNo)

                        'accountId.Value = Me.mobjUser.AccountId.ToString
                        'ReturnControls.Add(accountId)

                        'DownloadQuality.Value = Me.mobjUser.DefaultQuality
                        'ReturnControls.Add(DownloadQuality)

                        'If Request.QueryString("ref") <> "pdfviewer" Then
                        '    PageCount.Value = pages
                        '    ReturnControls.Add(PageCount)
                        'End If

                        'docId.Value = documentId
                        'ReturnControls.Add(docId)

                        'If Not IsNothing(objViewer) Then

                        '    Select Case mobjUser.DefaultDocView
                        '        Case 0
                        '            objViewer.AutoZoom = Atalasoft.Imaging.WebControls.AutoZoomMode.BestFit
                        '        Case 1
                        '            objViewer.AutoZoom = Atalasoft.Imaging.WebControls.AutoZoomMode.FitToWidth
                        '        Case 2
                        '            objViewer.AutoZoom = Atalasoft.Imaging.WebControls.AutoZoomMode.FitToHeight
                        '        Case Else
                        '            objViewer.AutoZoom = Atalasoft.Imaging.WebControls.AutoZoomMode.BestFit
                        '    End Select

                        'End If

                    Catch ex As Exception

                        If ex.Message = "Version has been deleted." Then
                            Server.Transfer("VersionDeleted.aspx", True)
                        Else
                            Dim conMaster As SqlClient.SqlConnection
                            conMaster = Functions.BuildMasterConnection
                            Functions.LogException(mobjUser.UserId.ToString, mobjUser.AccountId.ToString, "", "Viewer Page Load (First Segment): " & ex.Message, ex.StackTrace, Server.MachineName, "", Request.UrlReferrer.ToString, conMaster, True)
                        End If

                    End Try

                End If

            Catch ex As Exception

                Dim conMaster As SqlClient.SqlConnection
                conMaster = Functions.BuildMasterConnection
                Functions.LogException(mobjUser.UserId.ToString, mobjUser.AccountId.ToString, "", "Viewer Page Load Error: " & ex.Message, ex.StackTrace, Server.MachineName, "", Request.UrlReferrer.ToString, conMaster, True)

            Finally

                If Me.conMaster.State <> ConnectionState.Closed Then
                    Me.conMaster.Close()
                    Me.conMaster.Dispose()
                End If

                If Me.conCompany.State <> ConnectionState.Closed Then
                    Me.conCompany.Close()
                    Me.conCompany.Dispose()
                End If

            End Try

        End Sub

        <RemoteInvokable()> _
        Public Function Rotate(ByVal versionId As String, ByVal pageNo As String, ByVal degrees As String)

            Dim bolReturn As Boolean = False
            Dim dsVersionControl As Accucentric.docAssist.Data.dsVersionControl

            Try

                'Load the existing dataset
                If Not Session(versionId & "VersionControl") Is Nothing Then
                    dsVersionControl = Session(versionId & "VersionControl")
                End If

                If pageNo = "-1" Then

                    'Rotate all pages
                    For Each ivRow As Accucentric.docAssist.Data.dsVersionControl.ImageVersionDetailRow In dsVersionControl.ImageVersionDetail.Rows
                        ivRow.RotateImage += degrees
                        ivRow.Modified = Now()
                    Next

                Else

                    'Check for the page already existing
                    Dim objFind(1) As Object
                    objFind(0) = versionId
                    objFind(1) = pageNo
                    Dim drImageVersionDetail As Accucentric.docAssist.Data.dsVersionControl.ImageVersionDetailRow = dsVersionControl.ImageVersionDetail.Rows.Find(objFind)

                    'Save the image rotation
                    If Not drImageVersionDetail Is Nothing Then
                        drImageVersionDetail.RotateImage += degrees
                        drImageVersionDetail.Modified = Now()
                    End If

                End If

                Dim rot As New RotateCommand(degrees)
                viewer.ApplyCommand(rot)

                ''Rotate annotations
                'For Each annotation As AnnotationUI In viewer.Annotations.Layers(0).Items
                '    annotation.Rotate(degrees)
                'Next

                'For Each annotation As AnnotationUI In viewer.Annotations.Layers(1).Items
                '    annotation.Rotate(degrees)
                'Next

                'viewer.Annotations.Layers(0).Rotate(degrees)
                'viewer.Annotations.Layers(1).Rotate(degrees)
                'viewer.UpdateAnnotations()

                'Save the session variable
                Session(versionId & "VersionControl") = dsVersionControl

                Return True

            Catch ex As Exception
                Throw ex
            Finally

            End Try

        End Function

        <RemoteInvokable()> _
        Public Function LoadPage(ByVal versionId As String, ByVal pageNo As String, ByVal quality As String)

            If Me.conCompany.State <> ConnectionState.Open Then
                Me.mobjUser = BuildUserObject(Request.Cookies("docAssist"))
                Me.conCompany = Functions.BuildConnection(Me.mobjUser)
            End If

            Try

                Dim dsVersionControl As dsVersionControl = Session(versionId & "VersionControl")

                Dim find(1) As Object
                find(0) = versionId
                find(1) = pageNo
                dsVersionControl.ImageVersionDetail.PrimaryKey = New DataColumn() {dsVersionControl.ImageVersionDetail.Columns("VersionId"), dsVersionControl.ImageVersionDetail.Columns("SeqId")}
                Dim drVersionImageDetail As dsVersionControl.ImageVersionDetailRow = dsVersionControl.ImageVersionDetail.Rows.Find(find)
                Dim dvImageAnnotation As New DataView(dsVersionControl.ImageAnnotation, "ImageDetID = " & drVersionImageDetail.ImageDetID.ToString, "ImageDetID", DataViewRowState.CurrentRows)
                Dim rotationDegrees As Long = drVersionImageDetail.RotateImage
                Dim intImageDetID As Integer = drVersionImageDetail.ImageDetID

                Dim sqlCmd As New SqlClient.SqlCommand("acsImageDetail", Me.conCompany)
                sqlCmd.CommandType = CommandType.StoredProcedure
                sqlCmd.Parameters.Add("@ImageDetId", intImageDetID)

                If Not Me.conCompany.State = ConnectionState.Open Then Me.conCompany.Open()
                Dim dr As SqlClient.SqlDataReader = sqlCmd.ExecuteReader(CommandBehavior.SingleRow)

                Dim bytImage As Byte()

                'Track the page request
                Functions.TrackPageRequest(Me.mobjUser, "Web View", "Out", versionId, intImageDetID, ConfigurationSettings.AppSettings("Connection"))

                'Read the image
                If dr.Read() Then
                    bytImage = dr("Image")
                End If
                dr.Close()

                If Not bytImage Is Nothing Then

                    If bytImage.Length > 0 Then

                        Dim fileName As String = System.Guid.NewGuid().ToString & ".tif"
                        Dim saveFileName As String = Server.MapPath("tmp\" & fileName)

                        'If pdf Then
                        '    Dim oFileStream As System.IO.FileStream
                        '    oFileStream = New System.IO.FileStream(saveFileName.Replace(".tif", ".pdf"), System.IO.FileMode.Create)
                        '    oFileStream.Write(bytImage, 0, bytImage.Length)
                        '    oFileStream.Close()
                        '    Dim outputFile As String = saveFileName.Replace(".tif", "_pdfview.tif")
                        '    Functions.ExtractTIFPageFromPDF(saveFileName.Replace(".tif", ".pdf"), intPageNo, outputFile)

                        '    Session("Filename") = saveFileName.Replace(".tif", ".pdf")

                        '    strReturn(0) = outputFile
                        '    strReturn(1) = MaxQuality
                        '    strReturn(2) = quality
                        '    Return strReturn
                        'Else

                        Dim image As Atalasoft.Imaging.AtalaImage
                        image = Atalasoft.Imaging.AtalaImage.FromByteArray(bytImage)

                        'End If

                        Dim MaxQuality As Integer

                        Dim rawDepth As Integer = image.ColorDepth

                        dQuality.Items.Clear()

                        Select Case image.ColorDepth
                            Case 1
                                MaxQuality = 1
                                dQuality.Items.Add(New ListItem("Low", "1"))
                            Case 4
                                MaxQuality = 50
                                dQuality.Items.Add(New ListItem("Low", "1"))
                                dQuality.Items.Add(New ListItem("Medium", "50"))
                            Case 8
                                MaxQuality = 50
                                dQuality.Items.Add(New ListItem("Low", "1"))
                                dQuality.Items.Add(New ListItem("Medium", "50"))
                            Case 24
                                MaxQuality = 100
                                dQuality.Items.Add(New ListItem("Low", "1"))
                                dQuality.Items.Add(New ListItem("Medium", "50"))
                                dQuality.Items.Add(New ListItem("High", "100"))
                        End Select

                        'Rotate
                        Dim rotate As New Atalasoft.Imaging.ImageProcessing.Transforms.RotateCommand(rotationDegrees * -1)
                        image = rotate.Apply(image).Image

                        Dim tiffEncode As New TiffEncoder(TiffCompression.Lzw, False)

                        Select Case quality
                            Case DownloadQuality.G4
                                tiffEncode.Compression = TiffCompression.Group4FaxEncoding
                                image.Resolution = New Atalasoft.Imaging.Dpi(150, 150, ResolutionUnit.DotsPerInch)
                                If image.ColorDepth <> 1 Then image = image.GetChangedPixelFormat(Atalasoft.Imaging.PixelFormat.Pixel1bppIndexed)
                                If image.ColorDepth <> 1 Then
                                    Dim cmd As New ImageProcessing.Document.DynamicThresholdCommand
                                    image = cmd.Apply(image).Image
                                End If
                            Case DownloadQuality.GREYSCALE
                                If rawDepth < 8 Then
                                    'Cannot apply grayscale, use 1 bit
                                    tiffEncode.Compression = TiffCompression.Group4FaxEncoding
                                    image.Resolution = New Atalasoft.Imaging.Dpi(150, 150, ResolutionUnit.DotsPerInch)
                                    'If Image.ColorDepth <> 1 Then Image = Image.GetChangedPixelFormat(Atalasoft.Imaging.PixelFormat.Pixel1bppIndexed)
                                    If image.ColorDepth <> 1 Then
                                        Dim cmd As New ImageProcessing.Document.DynamicThresholdCommand
                                        image = cmd.Apply(image).Image
                                    End If
                                Else
                                    tiffEncode.Compression = TiffCompression.Lzw
                                    image.Resolution = New Atalasoft.Imaging.Dpi(200, 200, ResolutionUnit.DotsPerInch)
                                    If image.ColorDepth <> 8 Then image = image.GetChangedPixelFormat(Atalasoft.Imaging.PixelFormat.Pixel8bppGrayscale)
                                End If
                            Case DownloadQuality.COLOR
                                If rawDepth < 24 Then
                                    'Cant apply color, find best alternative
                                    If rawDepth < 8 Then
                                        'Cannot apply grayscale, use 1 bit
                                        tiffEncode.Compression = TiffCompression.Group4FaxEncoding
                                        image.Resolution = New Atalasoft.Imaging.Dpi(150, 150, ResolutionUnit.DotsPerInch)
                                        'If Image.ColorDepth <> 1 Then Image = Image.GetChangedPixelFormat(Atalasoft.Imaging.PixelFormat.Pixel1bppIndexed)
                                        If image.ColorDepth <> 1 Then
                                            Dim cmd As New ImageProcessing.Document.DynamicThresholdCommand
                                            image = cmd.Apply(image).Image
                                        End If
                                    Else
                                        tiffEncode.Compression = TiffCompression.Lzw
                                        image.Resolution = New Atalasoft.Imaging.Dpi(200, 200, ResolutionUnit.DotsPerInch)
                                        If image.ColorDepth <> 8 Then image = image.GetChangedPixelFormat(Atalasoft.Imaging.PixelFormat.Pixel8bppGrayscale)
                                    End If
                                Else
                                    tiffEncode.Compression = TiffCompression.Lzw
                                    image.Resolution = New Atalasoft.Imaging.Dpi(300, 300, ResolutionUnit.DotsPerInch)
                                    If image.ColorDepth <> 24 Then image = image.GetChangedPixelFormat(Atalasoft.Imaging.PixelFormat.Pixel24bppBgr)
                                End If
                        End Select

                        Dim strm As New System.IO.MemoryStream(image.ToByteArray(tiffEncode))
                        viewer.Open(strm, 0)

                        'Annotations
                        Dim bytAnnotation As Byte()

                        viewer.ClearAnnotations()

                        If dvImageAnnotation.Count > 0 Then

                            'Load annotation from session
                            If Not dvImageAnnotation.Item(0).Item("Annotation") Is Nothing And Not dvImageAnnotation.Item(0).Item("Annotation") Is System.DBNull.Value Then
                                bytAnnotation = dvImageAnnotation.Item(0).Item("Annotation")
                            End If

                        Else

                            'Load from database
                            sqlCmd = New SqlClient.SqlCommand("acsImageAnnotation", Me.conCompany)
                            sqlCmd.CommandType = CommandType.StoredProcedure
                            sqlCmd.Parameters.Add("@VersionID", versionId)
                            sqlCmd.Parameters.Add("@ImageDetId", intImageDetID)

                            Dim drAnn As SqlClient.SqlDataReader = sqlCmd.ExecuteReader(CommandBehavior.SingleRow)

                            If drAnn.Read() Then
                                bytAnnotation = drAnn("Annotation")
                            End If

                            drAnn.Close()

                        End If

                        viewer.Annotations.Layers.Clear()
                        viewer.Annotations.Layers.Add(New LayerAnnotation)
                        viewer.Annotations.Layers.Add(New LayerAnnotation)

                        If Not bytAnnotation Is Nothing Then

                            Dim msR As New MemoryStream(bytAnnotation, 0, bytAnnotation.Length)
                            msR.Seek(0, SeekOrigin.Begin)

                            Dim tmpViewer As New WebAnnotationController
                            tmpViewer.Load(msR, AnnotationDataFormat.Xmp)

                            For Each ann As AnnotationUI In tmpViewer.Layers(0).Items
                                viewer.Annotations.Layers(0).Items.Add(ann)
                            Next

                            tmpViewer.Dispose()

                        End If

                    End If

                End If

                viewer.UpdateAnnotations()

            Catch ex As Exception

                Throw ex

            End Try

        End Function

        Public Function BuildPageQuality(ByVal intVersionId As Integer, ByVal intPageNo As Integer, ByVal RotationDegrees As Integer, ByVal guidAccountId As Guid, ByVal Quality As Integer, ByVal pdf As Boolean) As String()

            Dim strReturn() As String = " ,  , ".Split(",")
            Dim conSql As New SqlClient.SqlConnection
            Dim MaxQuality As String
            Dim Image As AtalaImage

            Session("CurrentPage") = intPageNo

            Try

                Dim Request As HttpRequest = Context.Request
                Me.mobjUser = BuildUserObject(Request.Cookies("docAssist"))
                Me.mobjUser.AccountId = guidAccountId
                conSql = BuildConnection(Me.mobjUser)

                Dim dsVersionControl As dsVersionControl
                If Not Session(intVersionId & "VersionControl") Is Nothing Then
                    dsVersionControl = Session(intVersionId & "VersionControl")
                Else
                    dsVersionControl = Accucentric.docAssist.Data.GetVersionControl(intVersionId, conSql, Me.mobjUser.ImagesUserId)
                    Session(intVersionId & "VersionControl") = dsVersionControl
                End If

                Dim objFind(1) As Object
                objFind(0) = intVersionId
                objFind(1) = intPageNo
                dsVersionControl.ImageVersionDetail.PrimaryKey = New DataColumn() {dsVersionControl.ImageVersionDetail.Columns("VersionId"), dsVersionControl.ImageVersionDetail.Columns("SeqId")}
                Dim drVersionImageDetail As dsVersionControl.ImageVersionDetailRow = dsVersionControl.ImageVersionDetail.Rows.Find(objFind)

                RotationDegrees = drVersionImageDetail.RotateImage

                Dim intImageDetID As Integer = drVersionImageDetail.ImageDetID
                Dim sqlCmd As New SqlClient.SqlCommand("acsImageDetail", conSql)
                sqlCmd.CommandType = CommandType.StoredProcedure
                sqlCmd.Parameters.Add("@ImageDetId", intImageDetID)

                If Not conSql.State = ConnectionState.Open Then conSql.Open()

                Dim dr As SqlClient.SqlDataReader = sqlCmd.ExecuteReader(CommandBehavior.SingleRow)
                Dim bytImage As Byte()

                Try
                    'Track the page request
                    Functions.TrackPageRequest(Me.mobjUser, "Web View", "Out", intVersionId, intImageDetID, ConfigurationSettings.AppSettings("Connection"))
                Catch ex As Exception

                End Try

                'Read the image
                If dr.Read() Then
                    bytImage = dr("Image")
                End If
                dr.Close()

                If Not bytImage Is Nothing Then

                    If bytImage.Length > 0 Then

                        Dim fileName As String = System.Guid.NewGuid().ToString & ".tif"
                        Dim saveFileName As String = Server.MapPath("tmp\" & fileName)

                        If pdf Then
                            Dim oFileStream As System.IO.FileStream
                            oFileStream = New System.IO.FileStream(saveFileName.Replace(".tif", ".pdf"), System.IO.FileMode.Create)
                            oFileStream.Write(bytImage, 0, bytImage.Length)
                            oFileStream.Close()
                            Dim outputFile As String = saveFileName.Replace(".tif", "_pdfview.tif")
                            Functions.ExtractTIFPageFromPDF(saveFileName.Replace(".tif", ".pdf"), intPageNo, outputFile)

                            Session("Filename") = saveFileName.Replace(".tif", ".pdf")

                            strReturn(0) = outputFile
                            strReturn(1) = MaxQuality
                            strReturn(2) = Quality
                            Return strReturn
                        Else
                            Image = Atalasoft.Imaging.AtalaImage.FromByteArray(bytImage)
                        End If

                        Dim rawDepth As Integer = Image.ColorDepth

                        Select Case Image.ColorDepth
                            Case 1
                                MaxQuality = 1
                            Case 4
                                MaxQuality = 50
                            Case 8
                                MaxQuality = 50
                            Case 24
                                MaxQuality = 100
                        End Select

                        'Rotate
                        Dim rotate As New Atalasoft.Imaging.ImageProcessing.Transforms.RotateCommand(RotationDegrees * -1)
                        Image = rotate.Apply(Image).Image

                        Dim tiffEncode As New TiffEncoder(TiffCompression.Lzw, False)

                        Select Case Quality
                            Case DownloadQuality.G4
                                tiffEncode.Compression = TiffCompression.Group4FaxEncoding
                                Image.Resolution = New Atalasoft.Imaging.Dpi(150, 150, ResolutionUnit.DotsPerInch)
                                'If Image.ColorDepth <> 1 Then Image = Image.GetChangedPixelFormat(Atalasoft.Imaging.PixelFormat.Pixel1bppIndexed)
                                If Image.ColorDepth <> 1 Then
                                    Dim cmd As New ImageProcessing.Document.DynamicThresholdCommand
                                    Image = cmd.Apply(Image).Image
                                End If
                            Case DownloadQuality.GREYSCALE
                                If rawDepth < 8 Then
                                    'Cannot apply grayscale, use 1 bit
                                    tiffEncode.Compression = TiffCompression.Group4FaxEncoding
                                    Image.Resolution = New Atalasoft.Imaging.Dpi(150, 150, ResolutionUnit.DotsPerInch)
                                    'If Image.ColorDepth <> 1 Then Image = Image.GetChangedPixelFormat(Atalasoft.Imaging.PixelFormat.Pixel1bppIndexed)
                                    If Image.ColorDepth <> 1 Then
                                        Dim cmd As New ImageProcessing.Document.DynamicThresholdCommand
                                        Image = cmd.Apply(Image).Image
                                    End If
                                Else
                                    tiffEncode.Compression = TiffCompression.Lzw
                                    Image.Resolution = New Atalasoft.Imaging.Dpi(200, 200, ResolutionUnit.DotsPerInch)
                                    If Image.ColorDepth <> 8 Then Image = Image.GetChangedPixelFormat(Atalasoft.Imaging.PixelFormat.Pixel8bppGrayscale)
                                End If
                            Case DownloadQuality.COLOR
                                If rawDepth < 24 Then
                                    'Cant apply color, find best alternative
                                    If rawDepth < 8 Then
                                        'Cannot apply grayscale, use 1 bit
                                        tiffEncode.Compression = TiffCompression.Group4FaxEncoding
                                        Image.Resolution = New Atalasoft.Imaging.Dpi(150, 150, ResolutionUnit.DotsPerInch)
                                        'If Image.ColorDepth <> 1 Then Image = Image.GetChangedPixelFormat(Atalasoft.Imaging.PixelFormat.Pixel1bppIndexed)
                                        If Image.ColorDepth <> 1 Then
                                            Dim cmd As New ImageProcessing.Document.DynamicThresholdCommand
                                            Image = cmd.Apply(Image).Image
                                        End If
                                    Else
                                        tiffEncode.Compression = TiffCompression.Lzw
                                        Image.Resolution = New Atalasoft.Imaging.Dpi(200, 200, ResolutionUnit.DotsPerInch)
                                        If Image.ColorDepth <> 8 Then Image = Image.GetChangedPixelFormat(Atalasoft.Imaging.PixelFormat.Pixel8bppGrayscale)
                                    End If
                                Else
                                    tiffEncode.Compression = TiffCompression.Lzw
                                    Image.Resolution = New Atalasoft.Imaging.Dpi(300, 300, ResolutionUnit.DotsPerInch)
                                    If Image.ColorDepth <> 24 Then Image = Image.GetChangedPixelFormat(Atalasoft.Imaging.PixelFormat.Pixel24bppBgr)
                                End If
                        End Select

                        Image.Save(saveFileName, tiffEncode, Nothing)

                        strReturn(0) = saveFileName
                        strReturn(1) = MaxQuality
                        strReturn(2) = Quality

                        Return strReturn

                    End If

                End If

            Catch ex As Exception

                strReturn = Nothing
                'Throw New System.Web.Services.Protocols.SoapException(ex.Message & vbCrLf & ex.StackTrace, System.Xml.XmlQualifiedName.Empty, "text")
                Throw ex

            Finally

                'Image.Dispose()

                If Not conSql Is Nothing Then
                    If Not conSql.State = ConnectionState.Closed Then conSql.Close()
                End If

            End Try

            strReturn(0) = strReturn(0).ToString.Replace("\", "/")
            strReturn(1) = MaxQuality
            strReturn(2) = Quality

            Return strReturn

        End Function

        Private Sub CheckForTimeout()

            Try

                Functions.CheckTimeout(Me.Page)

                Me.mobjUser = BuildUserObject(Request.Cookies("docAssist"))

            Catch ex As Exception

                Select Case ex.Message
                    Case "DualLogin"
                        Response.Cookies("docAssist")("DualLogin") = True
                        If Request.Url.Query.Trim.Length > 0 Then
                            Response.Redirect("Default.aspx" & Request.Url.Query)
                        Else
                            Server.Transfer("Default.aspx")
                        End If
                    Case "User is not logged on"
                        If Request.Url.Query.Trim.Length > 0 Then
                            Response.Redirect("Default.aspx" & Request.Url.Query)
                        Else
                            Server.Transfer("Default.aspx")
                        End If
                    Case "Thread was being aborted."
                        If Request.Url.Query.Trim.Length > 0 Then
                            Response.Redirect("Default.aspx" & Request.Url.Query)
                        Else
                            Server.Transfer("Default.aspx")
                        End If
                End Select

            End Try

        End Sub

        Private Sub LoadThumbnails()

            Dim sb As New System.Text.StringBuilder

            sb.Append("<script>thmb[0] = '';") 'Dummy so page numbers line up with the array on the client side
            Dim pageCount As Integer = 1
            For Each dr As Accucentric.docAssist.Data.dsVersionControl.ImageDetailRow In dsVersionControl.ImageDetail.Rows
                sb.Append("thmb[" & pageCount & "] = '" & dr.ImageDetID.ToString & "';")
                pageCount += 1
            Next
            sb.Append("</script>")

            RegisterStartupScript("ThumbnailInfo", sb.ToString)

        End Sub

        Private Sub LoadDocument()

            ' Check if this user is supposed to see this version or the latest version
            Dim intRestrict, intVersionIdReturned As Integer
            Functions.UserVersionOnlyCheck(Me.mobjUser.ImagesUserId, versionId, intVersionIdReturned, intRestrict, Me.conCompany, fileType, docId, pageCount)
            versionId = intVersionIdReturned

            ' Load document into session
            dsVersionControl = Accucentric.docAssist.Data.GetVersionControl(versionId, Me.conCompany, Me.mobjUser.ImagesUserId)
            Session(versionId & "VersionControl") = dsVersionControl

            ' Client side variables
            RegisterStartupScript("", "<script>userId = '" & Me.mobjUser.UserId.ToString & "';accountId = '" & Me.mobjUser.AccountId.ToString & "';acctId = '" & Me.mobjUser.AccountId.ToString.Replace("-", "").ToUpper & "';versionId='" & versionId & "';" & "pageCount='" & pageCount & "';</script>")
            '"imagesUserID='" & Me.mobjUser.ImagesUserId & "';" & _

            RegisterStartupScript("close", "<script>closeViewer = " & Me.mobjUser.CloseViewer.ToString.ToLower & ";</script>")

        End Sub

        Private Sub LoadWorkflow()

            If Functions.ModuleSetting(Functions.ModuleCode.WORKFLOW, Me.mobjUser, Me.conCompany) Then

                Dim currentRouteId As Integer

                'Check if workflow console should be displayed
                wfMode = CheckWorkflow(Me.mobjUser.ImagesUserId, versionId, Me.mobjUser, currentRouteId)

                RegisterStartupScript("workFlowMode", "<script>workflowMode = '" & wfMode & "';</script>")
                RegisterStartupScript("currentRouteId", "<script>currentRouteId = '" & currentRouteId & "';</script>")

                hideWorkflowSection()
                hideWorkflowTabs()

                Select Case wfMode
                    Case Functions.WorkflowMode.NO_ACCESS
                        hideWorkflowSection()
                    Case Functions.WorkflowMode.AUTHORIZED_SUBMITTER_NOT_SUBMITTED
                        submitMode(currentRouteId)
                        showWorkflowSection()
                        hideWorkflowTabs()
                        'frameWorkflow.Attributes("src") = "WorkflowSubmit.aspx?Mode=" & Functions.WorkflowMode.AUTHORIZED_SUBMITTER_NOT_SUBMITTED
                        'frameWorkflow.Attributes("scrolling") = "no"
                    Case Functions.WorkflowMode.VIEW_ACCESS_REVIEWER_PENDING
                        reviewMode()
                        showWorkflowSection()
                        hideWorkflowTabs()
                        'RegisterStartupScript("WFPane", "<script language='javascript'>document.all.tblWorkflow.style.display = 'block';</script>")
                        'frameWorkflow.Attributes("src") = "WorkflowHistory.aspx"
                        'frameWorkflow.Attributes("scrolling") = "auto"
                    Case Functions.WorkflowMode.ACTION_ACCESS_REVIEWER_PENDING
                        reviewMode()
                        showWorkflowSection()
                        showWorkflowTabs()
                        'frameWorkflow.Attributes("src") = "WorkflowReview.aspx?Mode=" & Functions.WorkflowMode.ACTION_ACCESS_REVIEWER_PENDING & "&Route=" & intCurrentRouteId
                        'frameWorkflow.Attributes("scrolling") = "no"
                    Case Functions.WorkflowMode.AUTHORIZED_SUBMITTER_SUBMITTED
                        historyOnlyMode()
                        showWorkflowSection()
                        hideWorkflowTabs()
                        'frameWorkflow.Attributes("src") = "WorkflowHistory.aspx"
                        'frameWorkflow.Attributes("scrolling") = "auto"
                    Case Functions.WorkflowMode.REVIEWED
                        historyOnlyMode()
                        showWorkflowSection()
                        hideWorkflowTabs()
                        'frameWorkflow.Attributes("src") = "WorkflowHistory.aspx"
                        'frameWorkflow.Attributes("scrolling") = "auto"
                    Case Functions.WorkflowMode.AUTHORIZED_SUBMITTER_RESUBMIT
                        submitMode(currentRouteId)
                        showWorkflowSection()
                        showWorkflowTabs()
                        'frameWorkflow.Attributes("src") = "WorkflowSubmit.aspx?Mode=" & Functions.WorkflowMode.AUTHORIZED_SUBMITTER_NOT_SUBMITTED
                        'frameWorkflow.Attributes("scrolling") = "no"
                    Case Else
                        hideWorkflowSection()
                End Select

            Else

                ' Account level workflow disabled, hide everything
                hideWorkflowSection()
                wfMode = Functions.WorkflowMode.NO_ACCESS ' Toolbar looks at this variable for permissions

            End If

        End Sub

        Private Sub LoadWorkflowPane()

            'Dim objWf As New Workflow(Me.conCompany, False)

            'Dim dsSubmit As New DataSet
            'dsSubmit = objWf.WFSubmitterWorkflowsGet(Me.mobjUser.ImagesUserId, , CInt(Session("NewDocumentId")))

            'ddlWorkflow.Items.Add(New ListItem("Select a workflow...", "-2"))

            'For Each dr As DataRow In dsSubmit.Tables(0).Rows
            '    Dim intCategoryId As Integer = dr("CategoryId")
            '    Dim strCategoryName As String = dr("CategoryName")
            '    Dim dvFilter As New DataView(dsSubmit.Tables(1))
            '    dvFilter.RowFilter = "CategoryId=" & intCategoryId

            '    ddlWorkflow.Items.Add(New ListItem(strCategoryName, "OPTGROUP"))
            '    For X As Integer = 0 To dvFilter.Count - 1
            '        ddlWorkflow.Items.Add(New ListItem(dvFilter(X)("WorkflowName"), dvFilter(X)("WorkflowId")))
            '    Next
            '    ddlWorkflow.Items.Add(New ListItem(strCategoryName, "/OPTGROUP"))

            'Next

            'ddlWorkflow.SelectedValue = -2

        End Sub

        Private Sub submitMode(ByVal currentRouteId As Integer)

            workflow.Attributes("class") = "visible"
            actionSection.Attributes("class") = "hidden"

            '''Disable controls
            ''queue.Attributes("disabled") = "enabled"
            ''urgent.Attributes("disabled") = "enabled"
            ''notes.Attributes("disabled") = "enabled"

            ''Load workflows
            'Dim objWf As New Workflow(Me.conCompany, False)

            'Dim dsSubmit As New DataSet
            'dsSubmit = objWf.WFSubmitterWorkflowsGet(Me.mobjUser.ImagesUserId, , docId)

            'workflow.Items.Add(New ListItem("Select a workflow...", "-2"))

            'For Each dr As DataRow In dsSubmit.Tables(0).Rows
            '    Dim intCategoryId As Integer = dr("CategoryId")
            '    Dim strCategoryName As String = dr("CategoryName")
            '    Dim dvFilter As New DataView(dsSubmit.Tables(1))
            '    dvFilter.RowFilter = "CategoryId=" & intCategoryId

            '    'workflow.Items.Add(New ListItem(strCategoryName, "OPTGROUP"))
            '    For X As Integer = 0 To dvFilter.Count - 1
            '        workflow.Items.Add(New ListItem(dvFilter(X)("WorkflowName"), dvFilter(X)("WorkflowId")))
            '    Next
            '    'workflow.Items.Add(New ListItem(strCategoryName, "/OPTGROUP"))

            'Next

            'workflow.Value = -2

            ''workflow.AutoPostBack = True

        End Sub

        Private Sub reviewMode()

            workflowSection.Attributes("class") = "hidden"
            action.Attributes("class") = "visible"
            action.Attributes("onchange") = "loadQueues();"

            'Load actions
            Dim objWf As New Workflow(Functions.BuildConnection(Me.mobjUser))
            Dim dsSubmit As New DataSet
            dsSubmit = objWf.WFReviewerActionsGet(versionId, Me.mobjUser.ImagesUserId)

            'lblWorkflow.Text = dsSubmit.Tables(0).Rows(0)("WorkflowName")

            action.DataSource = dsSubmit.Tables(1)
            action.DataTextField = "StatusDesc"
            action.DataValueField = "QueueActionID"
            action.DataBind()

            urgent.Checked = dsSubmit.Tables(3).Rows(0)("Urgent")
            'lblQueueName.Text = dsSubmit.Tables(3).Rows(0)("QueueName")

            action.Items.Add(New ListItem("Select an action...", "-2"))
            action.Value = -2

            'Build script for automating folder moves based on workflow action
            Dim sb As New System.Text.StringBuilder
            sb.Append("<script>function automateFolder(){")
            sb.Append("switch($('#action').val()){")

            For Each dr As DataRow In dsSubmit.Tables(1).Rows
                sb.Append("case '" & dr("QueueActionID") & "':")
                sb.Append("setFolder('" & dr("FolderID") & "', escape('" & dr("FolderPath") & "'));")
                sb.Append("break;")
            Next

            sb.Append("}}</script>")

            RegisterStartupScript("folderAutomation", sb.ToString)

        End Sub

        Private Sub historyOnlyMode()
            workflowContent.Attributes("class") = "hidden"
            wfHistory.Attributes("class") = "visible"
        End Sub

        Private Sub hideWorkflowSection()
            workflowContainer.Attributes("class") = "hidden"
        End Sub

        Private Sub showWorkflowSection()
            workflowContainer.Attributes("class") = "visible"
        End Sub

        Private Sub showWorkflowTabs()
            wfTabs.Attributes("class") = "visible"
        End Sub

        Private Sub hideWorkflowTabs()
            wfTabs.Attributes("class") = "hidden"
        End Sub

        Private Sub LoadToolbars()

            Try

                'Next/Prev result buttons
                Dim currentRow, nextDocumentVersionId As Integer
                Dim resultCount As Integer = Session("intTotalDocuments")
                Dim tmpDocs As DataTable = Session("dtVersionId")

                If tmpDocs.Rows.Count > 0 Then

                    Dim dr() As DataRow = tmpDocs.Select("VersionId = '" & versionId & "'")
                    currentRow = dr(0)("Row")
                    Session("intCurrentRow") = currentRow

                    'Get previous document
                    Dim drPrev() As DataRow = tmpDocs.Select("Row = '" & currentRow - 1 & "'")
                    If Not IsNothing(drPrev) Then
                        If drPrev.Length > 0 Then
                            previousResult.Attributes.Item("class") = "sprite btn-previous-result-on"
                            Dim prevId As Integer = drPrev(0)("VersionID")
                            previousResult.Attributes.Item("onclick") = "loadVersionId(" & prevId & ");"
                        Else
                            'No previous document found
                            previousResult.Attributes.Item("class") = "sprite btn-previous-result-off"
                            previousResult.Attributes.Item("onclick") = "return true;"
                        End If
                    Else
                        'No previous document found
                        previousResult.Attributes.Item("class") = "sprite btn-previous-result-off"
                        previousResult.Attributes.Item("onclick") = "return true;"
                    End If

                    'Get next document
                    Dim drNext() As DataRow = tmpDocs.Select("Row = '" & currentRow + 1 & "'")
                    If Not IsNothing(drNext) Then
                        If drNext.Length > 0 Then
                            nextResult.Attributes.Item("class") = "sprite btn-next-result-on"
                            Dim nextId As Integer = drNext(0)("VersionID")
                            nextResult.Attributes.Item("onclick") = "loadVersionId(" & nextId & ");"
                        Else
                            'No next document found
                            nextResult.Attributes.Item("class") = "sprite btn-next-result-off"
                            nextResult.Attributes.Item("onclick") = "return true;"
                        End If
                    Else
                        'No next document found
                        nextResult.Attributes.Item("class") = "sprite btn-next-result-off"
                        nextResult.Attributes.Item("onclick") = "return true;"
                    End If


                    'If currentRow = Session("intTotalDocuments") Then
                    '    btnNext.ImageUrl = "Images/results_next_off.gif"
                    '    btnNext.Enabled = False
                    'Else
                    '    btnNext.ImageUrl = "Images/results_next.gif"
                    '    btnNext.Enabled = True
                    'End If

                    'If currentRow <= 1 Then
                    '    previousResult.Attributes.Item("class") = "sprite btn-previous-result-off"
                    '    btnPrevious.ImageUrl = "Images/results_previous_off.gif"
                    '    btnPrevious.Enabled = False
                    'Else
                    '    btnPrevious.ImageUrl = "Images/results_previous.gif"
                    '    btnPrevious.Enabled = True
                    'End If

                End If

            Catch ex As Exception
                'Do not throw an excpetion here. This only affects the 2 buttons.
                previousResult.Attributes.Item("class") = "sprite btn-previous-result-off"
                previousResult.Attributes.Item("onclick") = "return true;"
                nextResult.Attributes.Item("class") = "sprite btn-next-result-off"
                nextResult.Attributes.Item("onclick") = "return true;"
            End Try

            Select Case DocumentSecurityLevel(versionId, Me.mobjUser, Me.conCompany)

                Case SecurityLevel.NoAccess

                    'Check for active workflow
                    'tdAttachmentDelete.Visible = False
                    If wfMode = Functions.WorkflowMode.VIEW_ACCESS_REVIEWER_PENDING Or wfMode = Functions.WorkflowMode.ACTION_ACCESS_REVIEWER_PENDING _
                    Or wfMode = Functions.WorkflowMode.REVIEWED Then
                        savedoc.Visible = True
                        If wfMode = Functions.WorkflowMode.REVIEWED Then savedoc.Visible = False
                        nextResult.Visible = False
                        previousResult.Visible = False
                    Else
                        'TODO: No access boot
                        'Response.Redirect("Default.aspx")
                        Exit Sub
                    End If

                    savedoc.Visible = False

                Case SecurityLevel.Read

                    'Check for workflow
                    'tdAttachmentDelete.Visible = False
                    If wfMode = Functions.WorkflowMode.VIEW_ACCESS_REVIEWER_PENDING Or wfMode = Functions.WorkflowMode.ACTION_ACCESS_REVIEWER_PENDING _
                    Or wfMode = Functions.WorkflowMode.REVIEWED Then
                        savedoc.Visible = True
                        nextResult.Visible = False
                        previousResult.Visible = False
                        'imgAttachments.Visible = True
                    End If

                    move.Visible = False
                    delete.Visible = False
                    split.Visible = False
                    rotateLeft.Visible = False
                    rotateRight.Visible = False
                    flip.Visible = False

                Case SecurityLevel.Change

                    'Show save
                    savedoc.Visible = True
                    'imgAttachments.Visible = True
                    'tdAttachmentDelete.Visible = False

                Case SecurityLevel.Delete

                    'Show save
                    savedoc.Visible = True
                    'imgAttachments.Visible = True
                    'SaveSpacer.Visible = True

                Case Else

                    'Hide save
                    savedoc.Visible = False
                    'imgAttachments.Visible = False

            End Select

            'E-mail/Export
            Select Case Functions.EmailExportSecurityLevel(docId, Me.mobjUser, Me.conCompany)
                Case Web.Security.Functions.SecurityLevelEmailExport.EmailExportDisabled
                    email.Visible = False
                Case Web.Security.Functions.SecurityLevelEmailExport.EmailExportEnabled
                    email.Visible = True
                Case Else
                    email.Visible = False
            End Select

            'Print
            Select Case Functions.PrintSecurityLevel(docId, Me.mobjUser, Me.conCompany)
                Case Web.Security.Functions.SecurityLevelPrint.PrintDisabled
                    print.Visible = False
                Case Web.Security.Functions.SecurityLevelPrint.PrintEnabled
                    print.Visible = True
                Case Else
                    print.Visible = False
            End Select

            'Annoations/Redactions
            Dim bAnnotation, bRedaction As Boolean
            Functions.DocumentAnnotationSecurityLevel(Me.mobjUser.ImagesUserId, versionId, Me.conCompany, bAnnotation, bRedaction)

            If Not bAnnotation Then
                sticky.Visible = False
                pencil.Visible = False
                highlighter.Visible = False
                txttool.Visible = False
            End If

            If Not bRedaction Then
                'Not implemented
            End If

            ''Top toolbar
            'Dim mSecurityLevel As Accucentric.docAssist.Web.Security.SecurityLevel
            'Try
            '    Me.mobjUser = BuildUserObject(Request.Cookies("docAssist"))
            '    Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
            '    Me.mcookieSearch = New cookieSearch(Me.Page)
            '    Me.mcookieImage = New cookieImage(Me.Page)
            'Catch ex As Exception
            '    If ex.Message = "User is not logged on" Then
            '        Response.Redirect("default.aspx")
            '    End If
            'End Try

            'If Request.QueryString("WFA") = "1" Then
            '    Session("Workflow") = True
            'Else
            '    Session("Workflow") = False
            'End If

            'If Not Page.IsPostBack Then

            '    AttachmentDelete.Attributes("onclick") = "return confirm('Are you sure you want to delete this attachment?');"

            '    Dim mSecurityLevel As Accucentric.docAssist.Web.Security.SecurityLevel
            '    Dim intVersionId As Integer

            '    Try

            '        Select Case CType(Session("FileType"), Functions.FileType)
            '            Case Functions.FileType.ATTACHMENT
            '                If Request.QueryString("ref") = "pdfviewer" Then
            '                    PDFMode()
            '                Else
            '                    AttachmentMode()
            '                End If
            '            Case Else
            '                tdDocumentText.Attributes.Add("onclick", "textPopup(0);")
            '                tdAttachmentDelete.Visible = False
            '        End Select

            '        'Get VersionId of the image to load text from
            '        If Me.mcookieSearch.VersionID > 0 Then
            '            intVersionId = Me.mcookieSearch.VersionID
            '            versionId.Value = intVersionId

            '            ' Load the data and save to the session
            '            'Dim dsVersionControl As New Accucentric.docAssist.Data.dsVersionControl
            '            ''dsVersionControl = Session(intVersionId & "VersionControl")

            '            'dsVersionControl = Data.GetVersionControl(intVersionId, Me.mconSqlImage, Me.mobjUser.ImagesUserId)
            '            Dim intPageCount As Integer = CType(Parent.FindControl("pageCount"), System.Web.UI.HtmlControls.HtmlInputHidden).Value 'dsVersionControl.ImageDetail.Rows.Count
            '            'Session(intVersionId & "VersionControl") = dsVersionControl
            '            'Session(intVersionId & "PageCount") = intPageCount
            '            'Session(intVersionId & "VersionControl") = dsVersionControl

            '            If intPageCount = 1 Then
            '                lblPagesOf.Text = " 1 of 1"
            '                txtPageNo.Visible = False
            '            Else
            '                lblPagesOf.Text = " of " & intPageCount.ToString
            '            End If

            '            Dim intDocumentId As Integer = CType(Parent.FindControl("docId"), System.Web.UI.HtmlControls.HtmlInputHidden).Value 'CType(dsVersionControl.Images.Rows(0), Accucentric.docAssist.Data.dsVersionControl.ImagesRow).DocumentID

            '            Dim wfMode As Functions.WorkflowMode
            '            Try
            '                wfMode = Session("WorkflowMode")
            '            Catch ex As Exception

            '            End Try

            '            Try

            '                Dim conSqlCompany As New SqlClient.SqlConnection
            '                Try
            '                    'Attachments button
            '                    'companydb(FAAttachmentStatusbyVersionID)
            '                    '@VersionID --returns 1 if attachments exists or 0 if not, use to change the attachment button in the viewer to show presence of attachments

            '                    conSqlCompany = Functions.BuildConnection(Me.mobjUser)
            '                    conSqlCompany.Open()
            '                    Dim cmdSql As New SqlClient.SqlCommand("FAAttachmentStatusbyVersionID", conSqlCompany)
            '                    cmdSql.CommandType = CommandType.StoredProcedure
            '                    cmdSql.Parameters.Add("@VersionID", Me.mcookieSearch.VersionID)

            '                    Dim intAttachmentTotals As Integer = cmdSql.ExecuteScalar

            '                    If cmdSql.ExecuteScalar > 0 Then
            '                        'change image
            '                        'imgAttachments.Src = "Images/paperClipHot.gif"
            '                        imgAttachments.Alt = intAttachmentTotals.ToString & " Attachments"
            '                    End If

            '                Catch ex As Exception
            '                    If conSqlCompany.State <> ConnectionState.Closed Then
            '                        conSqlCompany.Close()
            '                        conSqlCompany.Dispose()
            '                    End If
            '                End Try

            '                'Hide save button by default, if it needs to be shown it will be below
            '                imgSave.Visible = False
            '                SaveCell.Visible = False



            '                If Me.mobjUser.EditionID = AccountEdition.SMALL_BUSINESS_EDITION Then
            '                    tdShare.Visible = True
            '                Else
            '                    tdShare.Visible = False
            '                End If

            '                'Check e-mail/export permissions
            '                Select Case DocumentEmailExportSecurityLevel(intDocumentId)
            '                    Case Web.Security.Functions.SecurityLevelEmailExport.EmailExportDisabled
            '                        'Hide e-mail/export button
            '                        imgMail.Attributes("style") = "DISPLAY: none"
            '                    Case Web.Security.Functions.SecurityLevelEmailExport.EmailExportEnabled
            '                        'show e-mail/export button
            '                        imgMail.Attributes("style") = "DISPLAY: block"
            '                    Case Else
            '                        'Hide e-mail/export button
            '                        imgMail.Attributes("style") = "DISPLAY: none"
            '                End Select

            '                'Check printing permissions
            '                Select Case DocumentPrintSecurityLevel(intDocumentId)
            '                    Case Web.Security.Functions.SecurityLevelPrint.PrintDisabled
            '                        'Hide print button
            '                        imgPrint.Attributes("style") = "DISPLAY: none"
            '                    Case Web.Security.Functions.SecurityLevelPrint.PrintEnabled
            '                        'show print button
            '                        imgPrint.Attributes("style") = "DISPLAY: block"
            '                    Case Else
            '                        'Hide print button
            '                        imgPrint.Attributes("style") = "DISPLAY: none"
            '                End Select

            '                'Print permissions
            '                Dim blnAnnotation As Boolean = False
            '                Dim blnRedaction As Boolean = False
            '                Functions.DocumentAnnotationSecurityLevel(Me.mobjUser.ImagesUserId, Me.mcookieSearch.VersionID, Me.mconSqlImage, blnAnnotation, blnRedaction)
            '                Annotation.Value = CInt(blnAnnotation)
            '                Redaction.Value = CInt(blnRedaction)

            '            Catch ex As Exception
            '                Throw ex
            '            Finally
            '                If Not Me.mconSqlImage.State = ConnectionState.Closed Then Me.mconSqlImage.Close()
            '            End Try
            '        End If

            '        'Get variable information
            '        If Not Page.IsPostBack Then
            '            If Me.mcookieSearch.VersionID > 0 Then
            '                Me.mintVersionId = Me.mcookieSearch.VersionID
            '                'Disable previous and next result if we are on the first result
            '                DisableNextButton()
            '                DisablePreviousButton()
            '            ElseIf Not Me.mcookieImage.ServerFileName Is Nothing _
            '                    And Not Me.mcookieImage.LocalFileName Is Nothing Then
            '                ddlVersions.Visible = False
            '                imgMail.Visible = False
            '                anchorPrint.Visible = False
            '            End If
            '            If CType(Session("FileType"), Functions.FileType) = Functions.FileType.SCAN Then InitializeViewer(Me.mintVersionId)

            '            ddlVersions.Enabled = Me.mcookieSearch.HideVersionDropDownList

            '        End If

            '        Try
            '            If Session("HideResultsButton") = True Then
            '                BackCell.Visible = False
            '                Session("HideResultsButton") = False
            '            End If
            '        Catch ex As Exception

            '        End Try

            '        'Next/Prev result buttons
            '        Dim ResultCount As Integer = Session("intTotalDocuments")
            '        Dim tmpDocs As DataTable = Session("dtVersionId")
            '        If tmpDocs.Rows.Count > 0 Then
            '            Dim dr() As DataRow = tmpDocs.Select("VersionId = '" & Me.mcookieSearch.VersionID & "'")
            '            CurrentRow = dr(0)("Row")
            '            Session("intCurrentRow") = CurrentRow
            '            DisableNextButton()
            '            DisablePreviousButton()
            '        End If

            '    Catch ex As Exception

            '        If ex.Message = "Version has been deleted." Then
            '            Server.Transfer("VersionDeleted.aspx")
            '        End If

            '    End Try

            'End If



        End Sub

        Private Sub LoadAttributes()

            If Functions.ModuleSetting(Functions.ModuleCode.DOCUMENT_TYPES_ATTRIBUTES, Me.mobjUser, Me.conCompany) Then

                ''Load attribute config
                'Dim xml As String = GetAttributeConfig(Me.mobjUser.UserId.ToString, Me.mobjUser.AccountId.ToString, Me.conCompany)
                'ReturnScripts.Add("xmlData = '" & xml & "';")

                Dim objAttachments As New FileAttachments(Me.conCompany, False)
                Dim ds As New DataSet
                ds = objAttachments.HeaderGet(versionId)

                Dim dt As DataTable = ds.Tables(0)

                If dt.Rows.Count > 0 Then
                    txtTitle.Value = Server.UrlDecode(dt.Rows(0)("DocumentTitle"))
                    txtDescription.Value = Server.UrlDecode(dt.Rows(0)("Description"))
                    txtTags.Value = Server.UrlDecode(dt.Rows(0)("Tags"))
                End If

                If ds.Tables(1).Rows.Count > 0 Then
                    If ds.Tables(1).Rows(0)("FolderId") > 0 Then
                        txtFolder.Value = CStr(ds.Tables(1).Rows(0)("FolderName")).Replace("&#43;", "+")
                        FolderId.Value = ds.Tables(1).Rows(0)("FolderId")
                    Else
                        txtFolder.Value = "<None>"
                        FolderId.Value = 0
                    End If
                End If

                'Retrieve the documents
                Dim sqlCmd As New SqlClient.SqlCommand("acsDocumentsByCabinetFolder", Me.conCompany)
                Dim da As New SqlClient.SqlDataAdapter(sqlCmd)
                sqlCmd.CommandType = CommandType.StoredProcedure
                sqlCmd.Parameters.Add("@UserName", Me.mobjUser.UserId)
                sqlCmd.Parameters.Add("@CabinetId", "0")
                sqlCmd.Parameters.Add("@FolderId", "0")

                'Try
                '    If Not IsNothing(Request.Cookies("Workflow")("Enabled")) Then
                '        'Workflow only user, no access to any documents but has to get into this specific document
                '        If Request.Cookies("Workflow")("Enabled") = True Then
                '            sqlCmd.Parameters.Add("@VersionId", versionId)
                '            Response.Cookies("Workflow")("Enabled") = Nothing
                '        End If
                '    End If
                'Catch ex As Exception
                'End Try

                Dim dtDocuments As New Documents
                da.Fill(dtDocuments)
                ddlDocuments.DataSource = dtDocuments
                ddlDocuments.DataTextField = dtDocuments.DocumentName.ColumnName
                ddlDocuments.DataValueField = dtDocuments.DocumentId.ColumnName
                ddlDocuments.DataBind()

                docNo.InnerText = CType(dsVersionControl.Images.Rows(0), Accucentric.docAssist.Data.dsVersionControl.ImagesRow).ImageID
                docId = CType(dsVersionControl.Images.Rows(0), Accucentric.docAssist.Data.dsVersionControl.ImagesRow).DocumentID
                ddlDocuments.Items.FindByValue(docId).Selected = True

                'Existing Attributes
                'Dim dtAttributes As New Accucentric.docAssist.Data.Images.Attributes
                'dtAttributes.Fill(docId, Attributes.AttributeParameterType.DocumentId, Me.mobjUser.ImagesUserId, Me.mconSqlImage)
                ''loadAttributeRow(rowNumber, Type, attributeId, Format, length, required, value)
                'loadAttributeRow(attributeId, value)

                'addAttributeRow(rowNumber, type, attributeId, format, length, required)

                Dim attributeScript As New System.Text.StringBuilder
                attributeScript.Append("function loadAttributes(){")
                Dim row As Integer = 0
                For Each dr As DataRow In dsVersionControl.ImageAttributes.Rows
                    'addNewRow(value, type, attributeId, format, length, required, imageAttributeId)
                    attributeScript.Append("addNewRow('" & row & "','" & dr("AttributeValue") & "','" & dr("AttributeDataType") & "','" & dr("AttributeID") & "','" & dr("DataFormat") & "','" & dr("Length") & "','" & dr("Required") & "','" & dr("ImageAttributeID") & "');")
                    'attributeScript.Append("addAttributeRow(" & row & ",'" & dr("AttributeDataType") & "','" & dr("AttributeID") & "','" & dr("DataFormat") & "','" & dr("Length") & "','" & dr("Required") & "');")
                    'attributeScript.Append("loadAttributeRow(" & dr("AttributeID") & ",'" & dr("AttributeValue") & "','" & dr("ImageAttributeID") & "');")
                    row += 1
                Next
                attributeScript.Append("}")
                RegisterStartupScript("loadAttributes", "<script>" & attributeScript.ToString & ";rowCount = " & row & ";</script>")

            Else

                'Hide doc types and attributes

            End If

        End Sub

        Private Sub showAttributes()
            attributeContainer.Attributes("class") = "visible"
        End Sub

        Private Sub hideAttributes()
            attributeContainer.Attributes("class") = "hidden"
        End Sub

        Private Sub LoadImage()

            'Set viewer default view
            Select Case mobjUser.DefaultDocView
                Case 0
                    viewer.AutoZoom = Atalasoft.Imaging.WebControls.AutoZoomMode.BestFit
                Case 1
                    viewer.AutoZoom = Atalasoft.Imaging.WebControls.AutoZoomMode.FitToWidth
                Case 2
                    viewer.AutoZoom = Atalasoft.Imaging.WebControls.AutoZoomMode.FitToHeight
                Case Else
                    viewer.AutoZoom = Atalasoft.Imaging.WebControls.AutoZoomMode.BestFit
            End Select

            'Load versions
            Dim dtVersion As New ImageVersion
            dtVersion.GetAllVersionsByVersionId(versionId, Me.conCompany)

            If dtVersion.Rows.Count > 0 Then
                versions.DataSource = dtVersion
                versions.DataValueField = dtVersion.VersionId.ColumnName
                versions.DataTextField = dtVersion.VersionNum.ColumnName
                versions.DataBind()

                versions.Value = versionId

                'If dtVersion.Rows.Count > 1 Then
                '    versions.Enabled = True
                'Else
                '    versions.Enabled = False
                'End If

                'versions.Attributes("onchange") = "var d = new Date();var random = d.getHours()+d.getMinutes()+d.getSeconds()+d.getMilliseconds();document.location.href='ViewDocument.aspx?VID='+this.options[this.selectedIndex].value+'&'+random;"
                'versions.Attributes("onchange") = "loadQuality(this.options[this.selectedIndex].value);"

            End If

            LoadPage(versionId, 1, 0)

        End Sub

        Private Sub viewer_AnnotationCreated(ByVal sender As Object, ByVal e As Atalasoft.Imaging.WebControls.Annotations.AnnotationCreatedEventArgs) Handles viewer.AnnotationCreated

            'Me.mobjUser = BuildUserObject(Request.Cookies("docAssist"))

            Select Case viewer.Annotations.Layers.Count
                Case 0
                    viewer.Annotations.Layers.Add(New LayerAnnotation)
                    viewer.Annotations.Layers.Add(New LayerAnnotation)
                Case 1
                    viewer.Annotations.Layers.Add(New LayerAnnotation)
            End Select

            'Sticky note
            Dim sticky As StickyNote = CType(IIf(TypeOf e.AnnotationData Is StickyNote, e.AnnotationData, Nothing), StickyNote)
            If Not sticky Is Nothing Then
                sticky.Text = "Double click to edit"
                sticky.Fill = New AnnotationBrush(Color.Yellow)
                sticky.Shadow = New AnnotationBrush(Color.Gray)
                sticky.ShadowOffset = New PointF(10, 10)
                sticky.Font = New AnnotationFont("Arial", 60.0F)
                sticky.CanRotate = True
                Dim stickyAnn As New TextAnnotation(sticky)
                viewer.Annotations.Layers(0).Items.Add(stickyAnn)
                Return
            End If

            Dim textTl As TextTool = CType(IIf(TypeOf e.AnnotationData Is TextTool, e.AnnotationData, Nothing), TextTool)
            If Not textTl Is Nothing Then
                textTl.Text = "Double click to edit"
                textTl.Fill = New AnnotationBrush(Color.White)
                textTl.FontBrush = New AnnotationBrush(Color.Black)
                textTl.Font = New AnnotationFont("Arial", 60.0F)
                textTl.CanRotate = True
                Dim txtTool As New TextAnnotation(textTl)
                viewer.Annotations.Layers(0).Items.Add(txtTool)
                Return
            End If

            Dim rect As RectangleData = CType(IIf(TypeOf e.AnnotationData Is RectangleData, e.AnnotationData, Nothing), RectangleData)
            If Not rect Is Nothing Then
                rect.Outline = New AnnotationPen(Color.Transparent, 1)
                rect.Fill = New AnnotationBrush(Color.FromArgb(128, Color.Yellow))
                rect.CanRotate = True
                Dim rectAnno As New RectangleAnnotation(rect)
                viewer.Annotations.Layers(0).Items.Add(rectAnno)
                Return
            End If

            Dim ellipse As EllipseData = CType(IIf(TypeOf e.AnnotationData Is EllipseData, e.AnnotationData, Nothing), EllipseData)
            If Not ellipse Is Nothing Then
                ellipse.CanRotate = True
                ellipse.Outline = New AnnotationPen(Color.Red, 12)
                ellipse.Fill = New AnnotationBrush(Color.Transparent)
                Dim ellipseAnno As New EllipseAnnotation(ellipse)
                viewer.Annotations.Layers(0).Items.Add(ellipseAnno)
                Return
            End If

        End Sub

        <RemoteInvokable()> _
        Public Function Track(ByVal versionId As String, ByVal pageNo As String) As Boolean

            Try

                Dim annotations As Byte()
                Dim redactions As Byte()

                'Annotations
                If viewer.Annotations.Layers(0).Items.Count > 0 Then

                    Dim annoCon As New WebAnnotationController
                    Dim msAnn As New MemoryStream

                    If viewer.Annotations.Layers(0).Items.Count > 0 Then
                        annoCon.Layers.Add(viewer.Annotations.Layers(0))
                    End If

                    msAnn.Seek(0, SeekOrigin.Begin)
                    annoCon.Save(msAnn, AnnotationDataFormat.Xmp)
                    annotations = msAnn.ToArray()

                End If

                'Dim view2 As New MemoryStream(annotations)
                'Dim a2 As New WebAnnotationController
                'view2.Seek(0, SeekOrigin.Begin)
                'a2.Load(view2, AnnotationDataFormat.Xmp)
                'Dim f = 8

                'Redactions
                'If objViewer.Annotations.Layers(1).Items.Count > 0 Then

                '    Dim redCon As New WebAnnotationController
                '    Dim msRed As New MemoryStream

                '    If objViewer.Annotations.Layers(1).Items.Count > 0 Then
                '        redCon.Layers.Add(objViewer.Annotations.Layers(1))
                '        redCon.Save(msRed, AnnotationDataFormat.Xmp)
                '    End If

                '    redactions = msRed.ToArray()

                'End If

                Dim dsVersion As Accucentric.docAssist.Data.dsVersionControl

                'Load the existing dataset
                If Not Session(versionId & "VersionControl") Is Nothing Then
                    dsVersion = Session(versionId & "VersionControl")
                End If

                'Check for the page already existing
                'First get the ImageDetID column for the page being processed
                Dim objFind(1) As Object
                objFind(0) = versionId
                objFind(1) = pageNo
                Dim drImageVersionDetail As Accucentric.docAssist.Data.dsVersionControl.ImageVersionDetailRow = dsVersion.ImageVersionDetail.Rows.Find(objFind)

                'Then get the annotation for the page processed
                Dim objFind2(1) As Object
                objFind2(0) = drImageVersionDetail("VersionID")
                objFind2(1) = drImageVersionDetail("ImageDetID")

                Dim drImageAnnotation As Accucentric.docAssist.Data.dsVersionControl.ImageAnnotationRow
                drImageAnnotation = dsVersion.ImageAnnotation.Rows.Find(objFind2)

                ' Save the annotation
                If Not drImageAnnotation Is Nothing Then
                    If viewer.Annotations.Layers(0).Items.Count = 0 Then
                        If Not annotations Is Nothing Then drImageAnnotation.Annotation = Nothing
                    Else
                        If Not annotations Is Nothing Then drImageAnnotation.Annotation = annotations
                    End If
                Else
                    Dim drImageAnnotationRow As Accucentric.docAssist.Data.dsVersionControl.ImageAnnotationRow
                    drImageAnnotationRow = dsVersion.ImageAnnotation.NewImageAnnotationRow
                    drImageAnnotationRow("VersionID") = drImageVersionDetail("VersionID")
                    drImageAnnotationRow("ImageDetID") = drImageVersionDetail("ImageDetID")
                    If Not annotations Is Nothing Then drImageAnnotationRow("Annotation") = annotations
                    'If Not redactions Is Nothing Then drImageAnnotationRow("Redaction") = redactions
                    dsVersion.ImageAnnotation.Rows.Add(drImageAnnotationRow)
                End If

                'Save the session variable
                Session(versionId & "VersionControl") = dsVersion

            Catch ex As Exception

            End Try

        End Function

    End Class

    Public Class StickyNote
        Inherits TextData
        Public Sub New()
            MyBase.New()
        End Sub
        Public Sub New(ByVal info As SerializationInfo, ByVal context As StreamingContext)
            MyBase.New(info, context)
        End Sub
    End Class

    Public Class TextTool
        Inherits TextData
        Public Sub New()
            MyBase.New()
        End Sub
        Public Sub New(ByVal info As SerializationInfo, ByVal context As StreamingContext)
            MyBase.New(info, context)
        End Sub
    End Class

End Namespace


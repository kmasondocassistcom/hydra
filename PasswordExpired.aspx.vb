Partial Class PasswordExpired
    Inherits System.Web.UI.Page

    Private objUser As Accucentric.docAssist.Web.Security.User
    Private strUserEmail As String



#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        txtConfirmPass.Attributes("onkeypress") = "submitForm();"
        txtCurrent.Attributes("onkeypress") = "submitForm();"
        txtNewPass.Attributes("onkeypress") = "submitForm();"

        lblReq.Text = Functions.PasswordStrongValidationMessage

        Try
            Me.objUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
            strUserEmail = objUser.EMailAddress
        Catch ex As Exception

        End Try

    End Sub

    Private Sub btnChange_ServerClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnChange.ServerClick

        Dim objEncrypt As New Encryption.Encryption
        Dim strPassword As String
        Dim strConnection As String = ConfigurationSettings.AppSettings("Connection")
        Dim conSql As New SqlClient.SqlConnection(Functions.ConnectionString())

        lblMsg.Text = ""

        If txtCurrent.Value.Length = 0 Or txtNewPass.Value.Length = 0 Or txtConfirmPass.Value.Length = 0 Then
            lblMsg.Text = "You must enter text into all the password fields."
            Exit Sub
        End If

        Try

            If conSql.State <> ConnectionState.Open Then conSql.Open()

            Dim cmdSql As New SqlClient.SqlCommand("SELECT password FROM Users WHERE EmailAddress = @EMailAddress", conSql)
            cmdSql.Parameters.Add("@EMailAddress", strUserEmail)

            strPassword = objEncrypt.Decrypt(cmdSql.ExecuteScalar)

            'Check if current password is correct
            If txtCurrent.Value <> strPassword Then
                lblMsg.Text = "Invalid current password."
                Exit Sub
            End If

            'Check if passwords match
            If txtNewPass.Value <> txtConfirmPass.Value Then
                lblMsg.Text = "New passwords must match."
                Exit Sub
            End If

            'Check for strong password
            If Functions.PasswordStrongValidation(txtConfirmPass.Value) = False Then
                lblMsg.Text = Functions.PasswordStrongValidationMessage
                Exit Sub
            End If

            'Check if password is different than new one
            If txtCurrent.Value = txtNewPass.Value Then
                lblMsg.Text = Functions.PasswordStrongValidationMessage
                Exit Sub
            End If

            'Change password
            Functions.ChangePassword(Me.objUser.UserId.ToString, Me.objUser.AccountId.ToString, objEncrypt.Encrypt(txtNewPass.Value), conSql)

            Response.Write("<script language=""javascript"">window.parent.GB_hide(false);</script>")

            'Close sql connection.
            If conSql.State = ConnectionState.Open Then conSql.Close()

            Session("glbnPasswordExpired") = False

            'Select Case Me.objUser.DefaultStartPage
            '    Case 0
            '        Response.Redirect("Search.aspx")
            '    Case 1
            '        Response.Redirect("DocumentManager.aspx")
            '    Case Else
            '        Response.Redirect("Search.aspx")
            'End Select

        Catch ex As Exception

            Dim conMaster As SqlClient.SqlConnection
            conMaster = Functions.BuildMasterConnection
            Functions.LogException(objUser.UserId.ToString, objUser.AccountId.ToString, "", "Change Password: " & ex.Message, ex.StackTrace, Server.MachineName, "", Request.UrlReferrer.ToString, conMaster, True)

            lblMsg.Text = "An error has occured. Please try again."

        Finally

            If conSql.State <> ConnectionState.Closed Then
                conSql.Close()
                conSql.Dispose()
            End If

        End Try

    End Sub

End Class
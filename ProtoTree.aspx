<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ProtoTree.aspx.vb" Inherits="docAssistWeb.ProtoTree"%>
<%@ Register TagPrefix="componentart" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Prototype Tree</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<link type="text/css" rel="stylesheet" href="http://yui.yahooapis.com/2.5.2/build/treeview/assets/skins/sam/treeview.css">
		<base target="_self">
	</HEAD>
	<body class="yui-skin-sam">
		<form id="Form1" method="post" runat="server">
			<div id="svc" style="BEHAVIOR: url(webservice.htc)"></div>
			<script src="http://yui.yahooapis.com/2.5.2/build/yahoo/yahoo-min.js"></script>
			<script src="http://yui.yahooapis.com/2.5.2/build/event/event-min.js"></script>
			<script src="http://yui.yahooapis.com/2.5.2/build/treeview/treeview-min.js"></script>
			<script src=scripts/exploreTree.js></script>
			<a onclick="addFolder();" href="#">Add Folder</a><br><br>
			<div id="treeDiv1"></div>
		
		</form>
	</body>
</HTML>

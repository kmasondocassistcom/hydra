Imports Telerik.WebControls
Imports Telerik.WebControls.RadUploadUtils
Imports Accucentric.docAssist
Imports Accucentric.docAssist.Data.Images

Partial Class Upload
    Inherits System.Web.UI.Page

    Dim mconSqlImage As SqlClient.SqlConnection
    Dim mobjUser As Accucentric.docAssist.Web.Security.User
    Dim mcookieSearch As cookieSearch

    Protected WithEvents lblCurrentVersion As System.Web.UI.WebControls.Label

    Protected WithEvents btnCancel As System.Web.UI.WebControls.ImageButton

    Dim intFolderId As Integer

    Dim source As New ArrayList

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Response.Expires = 0
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        Try

            Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
            Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))
            Me.mobjUser.AccountId = guidAccountId
            Me.mcookieSearch = New cookieSearch(Me)

            intFolderId = CInt(Request.QueryString("FolderId"))

            If Not Page.IsPostBack Then

                Dim ds As New DataSet
                ds = Session("AdvancedUpload")

                UploadTemplate.DataSource = ds.Tables(0)
                UploadTemplate.DataBind()

            End If

        Catch ex As Exception

#If DEBUG Then
            lblError.Text = ex.Message
#Else
            lblerror.Text = "An error has occured."
#End If

        Finally

        End Try

    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click

        Response.Redirect("FileAttachments.aspx")

    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click

        Dim sqlTran As SqlClient.SqlTransaction

        Try

            Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
            Me.mconSqlImage.Open()

            sqlTran = Me.mconSqlImage.BeginTransaction

            Dim objAttachments As New FileAttachments(Me.mconSqlImage, False, sqlTran)

            Dim intGoodSaves As Integer = 0

            For Each item As DataGridItem In UploadTemplate.Items

                'Dim txtTitle As TextBox = item.FindControl("txtTitle")
                'Dim txtDescription As TextBox = item.FindControl("txtDescription")
                'Dim txtTags As TextBox = item.FindControl("txtTags")

                Dim lblTitle As Label = item.FindControl("lblTitle")
                Dim lblDescription As Label = item.FindControl("lblDescription")
                Dim lblTags As Label = item.FindControl("lblTags")

                Dim lblFileName As Label = item.FindControl("lblFileName")
                Dim lblGuidName As Label = item.FindControl("lblGuidName")
                Dim lblFolderId As Label = item.FindControl("lblFolderId")
                intFolderId = CInt(lblFolderId.Text)

                'Save file
                Dim file As String = lblGuidName.Text

                'Find attributes user control
                Dim objAttributesGrid As System.Web.UI.UserControl = CType(item.FindControl("AttributesGrid"), System.Web.UI.UserControl)
                Dim ddlDocument As DropDownList = objAttributesGrid.FindControl("ddlDocuments")

                Dim byteArray As Byte() = GetDataAsByteArray(file)
                Dim intNewVersionId As Integer

                'Dim fileSize As new System.IO.FileInfo(file)

                Dim intImageId As Integer = objAttachments.InsertFolderAttachment(intFolderId, ddlDocument.SelectedValue, Me.mobjUser.ImagesUserId, lblTitle.Text, lblDescription.Text, _
                "", lblTags.Text, New System.IO.FileInfo(file).Length, System.IO.Path.GetFileName(lblFileName.Text), byteArray, intNewVersionId)

                'Find nested attributes datagrid
                Dim dgAttributes As New DataGrid
                dgAttributes = CType(objAttributesGrid.FindControl("dgAttributes"), DataGrid)

                Dim intResult As Integer

                For Each dgItem As DataGridItem In dgAttributes.Items

                    Dim lblAttributeDataType As Label = dgItem.FindControl("lblAttributeDataType")
                    Dim ddlAttribute As DropDownList = dgItem.FindControl("ddlAttribute")
                    Dim txtAttributeValue As TextBox = dgItem.FindControl("txtAttributeValue")
                    Dim txtAttributeDate As TextBox = dgItem.FindControl("txtAttributeDate")
                    Select Case lblAttributeDataType.Text
                        Case "Date"
                            If txtAttributeDate.Text.Trim.Length > 0 Then

                                If Not IsDate(txtAttributeDate.Text) Then
                                    lblError.Text = "Invalid date format."
                                    lblError.Visible = True
                                    Exit Sub
                                Else
                                    intResult = objAttachments.InsertFolderAttachmentAttribute(intImageId, CInt(ddlAttribute.SelectedValue), CDate(txtAttributeDate.Text).ToString("yyyy/MM/dd"))
                                End If

                            End If
                        Case Else
                            If txtAttributeValue.Text.Trim.Length > 0 Then
                                intResult = objAttachments.InsertFolderAttachmentAttribute(intImageId, CInt(ddlAttribute.SelectedValue), txtAttributeValue.Text)
                            End If
                    End Select

                Next

                'Workflow is needed
                Dim wfPane As WorkflowPane = item.FindControl("WorkflowPane")
                If Not IsNothing(wfPane) Then

                    Dim ddlWorkflows As DropDownList = wfPane.FindControl("ddlWorkflow")
                    Dim ddlQueues As DropDownList = wfPane.FindControl("ddlQueues")
                    Dim txtNotes As TextBox = wfPane.FindControl("txtNotes")
                    Dim chkUrgent As CheckBox = wfPane.FindControl("chkUrgent")

                    'insert document to workflow
                    If ddlWorkflows.SelectedValue <> "-2" And ddlWorkflows.SelectedValue <> "" Then
                        Dim objWorkflow As New Workflow(Me.mconSqlImage, False, sqlTran)
                        Dim intRouteId As Integer = objWorkflow.WFRouteInsert(intNewVersionId, Me.mobjUser.ImagesUserId, ddlWorkflows.SelectedValue, chkUrgent.Checked, ddlQueues.SelectedValue, txtNotes.Text)
                    End If

                End If

                'Track attachment
                Try
                    Accucentric.docAssist.Web.Data.Functions.TrackPageRequest(Me.mobjUser.UserId, Me.mobjUser.AccountId, "Attachment Add", "In", intNewVersionId, intImageId.ToString, Functions.GetMasterConnectionString)
                Catch ex As Exception

                End Try

                'Log recent item
                Try
                    Dim blnReturn As Boolean = Functions.TrackRecentItem(Me.mobjUser, "SV", intNewVersionId)
                    If Not blnReturn Then
                        'error logging recent item
                    End If
                Catch ex As Exception
                End Try

                intGoodSaves += 1

            Next

            sqlTran.Commit()

            litDone.Text = "<script language=javascript>parent.hideAdd();parent.browseFolder(" & intFolderId & ");document.getElementById('AddFrame').src = 'dummy.htm';</script>"

            Session("UploadFiles") = Nothing

        Catch ex As Exception

            Dim conMaster As SqlClient.SqlConnection
            conMaster = Functions.BuildMasterConnection
            Functions.LogException(mobjUser.UserId.ToString, mobjUser.AccountId.ToString, "", "Upload.aspx Error Details: " & ex.Message, ex.StackTrace, Server.MachineName, "", Request.UrlReferrer.ToString, conMaster, True)

            sqlTran.Rollback()
            lblError.Visible = True

#If DEBUG Then
            lblError.Text = ex.Message
#Else
            lblerror.Text = "An error has occured while saving files. Please try again."
#End If

        Finally

            If Me.mconSqlImage.State <> ConnectionState.Closed Then
                Me.mconSqlImage.Close()
                Me.mconSqlImage.Dispose()
            End If

        End Try

    End Sub

    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.PreRender

        Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)

        Try
            'User controls Page_Load event fires before the parent (Upload.aspx), the controls are all rendered on the datarow, loop through and hide whatever shouldnt be visible. Easy enough! :)

            'Check if workflow should be displayed on this row.
            Dim objWorkflow As New Workflow(Me.mconSqlImage, False)

            For Each dgRow As DataGridItem In UploadTemplate.Items

                'Find attributes grid to get document id.
                Dim objAttributesGrid As AttributesGrid = CType(dgRow.FindControl("AttributesGrid"), AttributesGrid)
                If Not IsNothing(objAttributesGrid) Then

                    Dim ddlDocument As New DropDownList
                    ddlDocument = objAttributesGrid.FindControl("ddlDocuments")

                    Dim ds As New DataSet
                    ds = objWorkflow.WFSubmitterWorkflowsGet(Me.mobjUser.ImagesUserId, , CInt(ddlDocument.SelectedValue))

                    'Find workflow pane
                    Dim wfPane As WorkflowPane = dgRow.FindControl("WorkflowPane")

                    If ds.Tables(0).Rows.Count > 0 Then
                        wfPane.Visible = True
                        wfPane.UserId = Me.mobjUser.ImagesUserId
                        wfPane.DocumentId = ddlDocument.SelectedValue
                        If Not Page.IsPostBack Then wfPane.LoadWorkflows()
                    Else
                        wfPane.Visible = False
                    End If

                End If

            Next

        Catch ex As Exception

        Finally
            If Me.mconSqlImage.State <> ConnectionState.Closed Then
                Me.mconSqlImage.Close()
                Me.mconSqlImage.Dispose()
            End If

        End Try

    End Sub

    Private Sub UploadTemplate_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub
End Class
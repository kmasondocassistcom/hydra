Partial Class RouteUsers
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try
            If Request.QueryString(0).ToString <> "" Then

                Dim objUser As Accucentric.docAssist.Web.Security.User = BuildUserObject(Request.Cookies("docAssist"))
                Dim conSqlImage As SqlClient.SqlConnection = Functions.BuildConnection(objUser)
                Dim sqlCmd As New SqlClient.SqlCommand("acsWFRouteDetailByRouteID", conSqlImage)
                sqlCmd.CommandType = CommandType.StoredProcedure
                sqlCmd.Parameters.Add("@RouteID", Request.QueryString(0).ToString)
                sqlCmd.Parameters.Add("@AccountID", objUser.AccountId)

                Dim dsRouteUsers As New DataSet
                Dim sqlDa As New SqlClient.SqlDataAdapter(sqlCmd)

                sqlDa.Fill(dsRouteUsers)
                With dsRouteUsers.Tables(0)
                    .Columns.Remove("RouteDetID")
                    .Columns.Remove("RouteID")
                    .Columns.Remove("UserID")
                    .Columns.Remove("RouteSequence")
                    .Columns.Remove("RouteDescription")
                    .Columns(1).ColumnName = "Last Name"
                    .Columns(2).ColumnName = "First Name"
                    .Columns(3).ColumnName = "E-Mail Address"
                End With


                dgRouteUsers.DataSource = dsRouteUsers.Tables(0)
                dgRouteUsers.DataBind()

            End If



        Catch ex As Exception

        End Try

        

    End Sub

End Class

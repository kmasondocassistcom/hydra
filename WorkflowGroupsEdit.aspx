<%@ Page Language="vb" AutoEventWireup="false" Codebehind="WorkflowGroupsEdit.aspx.vb" Inherits="docAssistWeb.WorkflowGroupsEdit"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title id="pageTitle">Workflow Group</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<table cellpadding="0" cellspacing="0" height="390" width="530" border="0" class="PageContent">
				<tr>
					<td>
						<P>&nbsp; <IMG src="Images/groups.gif">&nbsp;<BR>
							&nbsp;<BR>
							&nbsp;
							<asp:Label id="lblMessage" runat="server"></asp:Label></P>
					</td>
				</tr>
				<tr>
					<td>&nbsp;
						<asp:Label id="lblGroup" runat="server">Group Name:</asp:Label>&nbsp;
						<asp:TextBox id="txtGroupName" runat="server" Width="208px" Font-Names="Verdana" Font-Size="8.25pt"
							MaxLength="75"></asp:TextBox>
					</td>
				</tr>
				<tr>
					<td>&nbsp;
						<asp:Label id="lblGroupEnabled" runat="server"> Enabled:</asp:Label>&nbsp;
						<asp:CheckBox id="chkEnabled" runat="server"></asp:CheckBox>
					</td>
				</tr>
				<tr>
					<td height="140">
						<table cellpadding="0" cellspacing="0" border="0" height="100%" width="100%" class="PageContent">
							<TR>
								<TD width="50%" height="15">&nbsp;&nbsp;Available Users:</TD>
								<TD width="25" height="15" rowSpan="1"></TD>
								<TD width="50%" height="15">&nbsp; Assigned Users:</TD>
							</TR>
							<tr>
								<TD width="50%" height="100%">&nbsp;
									<asp:ListBox id="lstAvailable" runat="server" Width="215px" Height="200px"></asp:ListBox></TD>
								<TD vAlign="top" align="center" width="25"><BR>
									<BR>
									<BR>
									<BR>
									<asp:ImageButton id="btnAdd" runat="server" ImageUrl="Images/goRight.gif"></asp:ImageButton><BR>
									<BR>
									<BR>
									<asp:ImageButton id="btnRemove" runat="server" ImageUrl="Images/goLeft.gif"></asp:ImageButton></TD>
								<td width="50%">&nbsp;
									<asp:ListBox id="lstAssisgned" runat="server" Width="215px" Height="200px"></asp:ListBox>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td align="right">&nbsp;
						<asp:Label id="lblError" runat="server" Font-Names="Verdana" Font-Size="8.25pt" ForeColor="Red"
							Visible="False"></asp:Label>
						<a href="#" onclick="javascript:window.close()"><img src="Images/btn_cancel.gif" border="0"></a>
						<asp:ImageButton id="btnSave" runat="server" ImageUrl="Images/btn_save.gif"></asp:ImageButton></td>
				</tr>
				<tr height="100%">
					<td></td>
				</tr>
			</table>
		</form>
		<script language="javascript">
			
			function submitForm()
			{
				if (event.keyCode == 13)
				{
					event.cancelBubble = true;
					event.returnValue = false;
					document.all.btnSave.click();
				}
			}
			
			function reloadData()
			{
				opener.location.reload();window.close();
			}
		</script>
	</body>
</HTML>

Imports Accucentric.docAssist.Data.Images

Partial Class AccountInboundEmail
    Inherits System.Web.UI.Page

    Dim mobjUser As Accucentric.docAssist.Web.Security.User

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents rdoNeverExpire As System.Web.UI.WebControls.RadioButton
    Protected WithEvents rdoExpireInterval As System.Web.UI.WebControls.RadioButton
    Protected WithEvents txtDays As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnSave As System.Web.UI.WebControls.ImageButton

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try
            mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
            Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))
            mobjUser.AccountId = guidAccountId
        Catch ex As Exception
            Response.Redirect("Default.aspx")
        End Try

        Dim conMaster As New SqlClient.SqlConnection
        Try

            If Not Page.IsPostBack Then

                conMaster = Functions.BuildMasterConnection

                Dim prefs As New Accucentric.docAssist.Data.Images.Admin(conMaster)
                Dim accountAlias As String = prefs.InboundEmailPrefixGet(Me.mobjUser.AccountId.ToString)

                txtAccountAlias.Text = accountAlias

            End If

        Catch ex As Exception

        Finally

            If conMaster.State <> ConnectionState.Closed Then
                conMaster.Close()
                conMaster.Dispose()
            End If

        End Try

    End Sub

    Private Sub btnSaveSettings_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSaveSettings.Click

        Dim conMaster As New SqlClient.SqlConnection

        Try

            If txtAccountAlias.Text.Trim.Length = 0 Then
                lblError.Text = "Invalid alias. Please enter a value that contains only letters and numbers."
                Exit Sub
            End If

            conMaster = Functions.BuildMasterConnection

            Dim prefs As New Accucentric.docAssist.Data.Images.Admin(conMaster)
            Dim retVal As Integer = prefs.InboundEmailPrefixSet(Me.mobjUser.AccountId.ToString, txtAccountAlias.Text.Trim)

            Select Case retVal
                Case -1
                    lblError.Text = "This alias is already in use. Please choose another one."
                Case Else
                    lblError.Text = "Settings successfully saved."
            End Select

        Catch ex As Exception

#If DEBUG Then
            lblError.Text = ex.Message
#Else
            lblError.Text = "An error has occured while saving this preference. Please try again. If the problem continues please contact support."
#End If

        Finally

            If conMaster.State <> ConnectionState.Closed Then
                conMaster.Close()
                conMaster.Dispose()
            End If

        End Try

    End Sub

End Class
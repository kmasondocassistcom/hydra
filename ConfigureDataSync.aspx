<%@ Register TagPrefix="uc1" TagName="AdminNav" Src="Controls/AdminNav.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ConfigureDataSync.aspx.vb" Inherits="docAssistWeb.ConfigureDataSync"%>
<%@ Register TagPrefix="uc1" TagName="docHeader" Src="Controls/docHeader.ascx" %>
<%@ Register TagPrefix="uc1" TagName="TopRight" Src="Controls/TopRight.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>docAssist - Manage Documents</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body class="Prefs" bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0" MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<TABLE height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TBODY>
					<TR>
						<TD width="100%" colSpan="3"><uc1:docheader id="docHeader" runat="server"></uc1:docheader></TD>
					</TR>
					<TR vAlign="middle" height="100%">
						<TD class="Navigation1" style="BORDER-RIGHT: #cccccc thin solid" vAlign="top" height="100%"><uc1:adminnav id="AdminNav2" runat="server"></uc1:adminnav><IMG height="1" src="dummy.gif" width="120"></TD>
						<TD vAlign="top" noWrap align="center" height="100%"><BR>
							<TABLE cellSpacing="0" cellPadding="0" width="92%" align="left" border="0">
								<TBODY>
									<TR>
										<TD width="20" height="15" rowSpan="6">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</TD>
										<TD class="PrefHeader" height="15">Configure Data Synchronization
										</TD>
									</TR>
									<TR>
										<TD align="right"><IMG height="10" src="dummy.gif" width="1">
										</TD>
									</TR>
									<TR vAlign="top" align="left">
										<TD align="left" width="100%">
											<TABLE class="Prefs" cellSpacing="0" cellPadding="0" width="100%" align="left" border="0">
												<TBODY>
													<TR>
														<TD class="LabelName" style="WIDTH: 206px" noWrap align="right"></TD>
														<TD class="Text" bgColor="whitesmoke"><IMG height="1" src="dummy.gif" width="2">&nbsp;
															<asp:checkbox id="chkEnabled" runat="server" Text="Enabled" Checked="True"></asp:checkbox></TD>
													</TR>
													<tr>
														<td colSpan="2"><IMG height="5" src="spacer.gif" width="1"></td>
													</tr>
													<TR>
														<TD class="LabelName" style="WIDTH: 206px; HEIGHT: 15px" noWrap align="right" width="205">Direction:</TD>
														<TD class="Text">&nbsp;<asp:radiobutton id="rdoIn" runat="server" Text="Local data into docAssist" GroupName="Direction"
																Checked="True"></asp:radiobutton><asp:radiobutton id="rdoOut" runat="server" Text="docAssist data to my local database" GroupName="Direction"></asp:radiobutton></TD>
													</TR>
													<tr>
														<td colSpan="2"><IMG height="5" src="spacer.gif" width="1"></td>
													</tr>
													<TR align="right">
														<TD class="LabelName" style="WIDTH: 206px" vAlign="top" noWrap align="right">ODBC 
															Object:</TD>
														<TD class="Text" bgColor="whitesmoke">&nbsp;<IMG onclick="browseODBC();" alt="Choose ODBC Source" src="Images/browse.gif" style="CURSOR: hand">&nbsp;<IMG id="ClearODBC" style="CURSOR: hand" onclick="clearODBCSource();" alt="Clear ODBC Object"
																src="Images/clearselection.gif" runat="server"><span id="ODBCSource" runat="server" class=PageContent>[None]</span>&nbsp;<A onclick="addODBCSource();" href="#">Add...</A></TD>
													</TR>
													<tr>
														<td colSpan="2"><IMG height="5" src="spacer.gif" width="1"></td>
													</tr>
													<TR>
														<TD class="LabelName" style="WIDTH: 206px" noWrap align="right" width="205">Job 
															Type:</TD>
														<TD>&nbsp;<asp:dropdownlist id="ddlJobType" runat="server" AutoPostBack="True" Width="200px">
																<asp:ListItem Value="0">Attribute Synchronization</asp:ListItem>
															</asp:dropdownlist></TD>
													</TR>
													<tr>
														<td colSpan="2"><IMG height="5" src="spacer.gif" width="1"></td>
													</tr>
													<TR>
														<TD class="LabelName" style="WIDTH: 206px" vAlign="top" noWrap align="right" width="176">Job 
															Description:</TD>
														<TD class="Text" bgColor="#f5f5f5">&nbsp;<asp:textbox id="JobName" runat="server" Width="350px"></asp:textbox></TD>
													</TR>
													<tr>
														<td colSpan="2"><IMG height="5" src="spacer.gif" width="1"></td>
													</tr>
													<TR>
														<TD class="Text" style="WIDTH: 206px" noWrap align="right" width="205"></TD>
														<TD class="Text"><asp:checkbox id="chkMissingData" runat="server" Text="Add Missing Data"></asp:checkbox><BR>
															<asp:checkbox id="chkUpdateData" runat="server" Text="Update Existing Data"></asp:checkbox></TD>
													</TR>
													<tr>
														<td colSpan="2"><IMG height="5" src="spacer.gif" width="1"></td>
													</tr>
													<TR>
														<TD class="LabelName" style="WIDTH: 206px" vAlign="top" noWrap align="right" width="176">Document 
															Type:</TD>
														<TD class="Text" bgColor="#f5f5f5">&nbsp;<asp:dropdownlist id="ddlDocuments" runat="server" AutoPostBack="True" Width="200px"></asp:dropdownlist></TD>
													</TR>
													<tr>
														<td colSpan="2"><IMG height="5" src="spacer.gif" width="1"></td>
													</tr>
													<TR>
														<TD class="LabelName" style="WIDTH: 206px" noWrap align="right" width="205">Filter 
															to Cabinet (optional):</TD>
														<TD class="Text">&nbsp; <IMG onclick="chooseCabinet();" alt="Choose Cabinet" src="Images/browse.gif" style="CURSOR: hand">&nbsp;<IMG id="ClearCabinet" style="CURSOR: hand" onclick="clearCabinet();" alt="Clear Cabinet"
																src="Images/clearselection.gif" runat="server">&nbsp;<span class=PageContent id="CabinetFilter">[None]</span>
														</TD>
													</TR>
													<tr>
														<td colSpan="2"><IMG height="5" src="spacer.gif" width="1"></td>
													</tr>
													<TR>
														<TD class="LabelName" style="WIDTH: 206px" vAlign="top" noWrap align="right" width="176">Details:</TD>
														<TD class="Text" noWrap bgColor="#f5f5f5">&nbsp;<asp:datagrid id="dgAttributes" runat="server" Width="400px" Font-Size="9pt" AutoGenerateColumns="False"
																CssClass="Text">
																<AlternatingItemStyle BackColor="Silver"></AlternatingItemStyle>
																<Columns>
																	<asp:TemplateColumn HeaderText="Enabled">
																		<ItemTemplate>
																			<asp:Label id=AttributeId runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AttributeId") %>' Visible="False">
																			</asp:Label>
																			<asp:CheckBox id="Enabled" runat="server"></asp:CheckBox>
																		</ItemTemplate>
																	</asp:TemplateColumn>
																	<asp:TemplateColumn HeaderText="Primary Key">
																		<ItemTemplate>
																			<asp:CheckBox id="PrimaryKey" runat="server"></asp:CheckBox>
																		</ItemTemplate>
																	</asp:TemplateColumn>
																	<asp:TemplateColumn HeaderText="docAssist">
																		<ItemTemplate>
																			<asp:Label id="docAssistAttribute" runat="server" Visible="True" Text='<%# DataBinder.Eval(Container, "DataItem.AttributeName") %>'>
																			</asp:Label>
																		</ItemTemplate>
																	</asp:TemplateColumn>
																	<asp:TemplateColumn HeaderText="Local Database">
																		<ItemTemplate>
																			<asp:TextBox id="LocalAttributeName" runat="server" Width="130px"></asp:TextBox>
																		</ItemTemplate>
																	</asp:TemplateColumn>
																</Columns>
															</asp:datagrid></TD>
													</TR>
													<TR height="100%">
														<TD class="Text" style="WIDTH: 206px" noWrap align="right" width="205"></TD>
														<TD><asp:label id="lblError" runat="server" CssClass="ErrorText" Visible="False"></asp:label></TD>
													</TR>
													<TR>
														<TD class="Text" style="WIDTH: 176px" noWrap align="right" width="176"></TD>
														<TD><IMG height="1" src="spacer.gif" width="375">
															<asp:imagebutton id="btnAdd" runat="server" ImageUrl="Images/save_small.gif"></asp:imagebutton></TD>
													</TR>
												</TBODY>
											</TABLE>
										</TD>
									</TR>
								</TBODY>
							</TABLE>
							<INPUT id="ODBCID" type="hidden" runat="server"> <INPUT id="CabinetID" type="hidden" runat="server">
						</TD>
						<TD height="100%">&nbsp;</TD>
					</TR>
					<TR>
						<TD class="PageFooter" vAlign="bottom" bgColor="#cccccc" colSpan="3"><uc1:topright id="docFooter" runat="server"></uc1:topright></TD>
					</TR>
					<TR height="1">
						<TD style="BACKGROUND-COLOR: #959394" width="20"><IMG height="1" src="images/spacer.gif" width="20"></TD>
						<TD style="BACKGROUND-COLOR: #959394" width="100%"><IMG height="1" src="images/spacer.gif" width="100%"></TD>
						<TD style="BACKGROUND-COLOR: #959394" width="20"><IMG height="1" src="images/spacer.gif" width="20"></TD>
					</TR>
				</TBODY>
			</TABLE>
			<input id="ODBC" type="hidden">
			<script language="javascript">
			
				function browseODBC()
				{
					var ODBC = document.getElementById('ODBC');
					var val = window.showModalDialog('ODBCSources.aspx','ODBCSources','resizable:no;status:no;dialogHeight:230px;dialogWidth:250px;scroll:no');
					if (val != undefined)
					{
						varArray = val.split('�');
						document.getElementById('ODBCID').value = varArray[0];
						document.getElementById('ODBCSource').innerHTML = varArray[1];
					}
				}
				
				function addODBCSource()
				{
					var ODBC = document.getElementById('ODBC');
					var val = window.showModalDialog('CreateODBCSource.aspx','CreateODBCSource','resizable:no;status:no;dialogHeight:200px;dialogWidth:350px;scroll:no');
					if (val != undefined)
					{
						varArray = val.split('�');
						document.getElementById('ODBCID').value = varArray[0];
						document.getElementById('ODBCSource').innerHTML = varArray[1];
					}
				}

				function chooseCabinet()
				{
					var val = window.showModalDialog('Cabinet.aspx?','FolderLookup','resizable:no;status:no;dialogHeight:625px;dialogWidth:250px;scroll:no');
					if (val != undefined)
					{
						varArray = val.split('�');
						document.getElementById('CabinetFilter').innerHTML = varArray[1];
						document.getElementById('CabinetID').value = varArray[0];
					}
				}
				
				function clearODBCSource()
				{
					document.getElementById('ODBCID').value = '';
					document.getElementById('ODBCSource').innerHTML = '[None]';
				}

				function clearCabinet()
				{
					document.getElementById('CabinetID').value = '';
					document.getElementById('CabinetFilter').innerHTML = '[None]';
				}
			
			</script>
		</form>
	</body>
</HTML>
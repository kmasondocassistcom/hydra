Partial Class ManageDocumentsAttributesOrder
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Dim dtAttributesSelected As New DataTable


    Private mobjUser As Accucentric.docAssist.Web.Security.User
    Private mconSqlImage As SqlClient.SqlConnection

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'btnMoveUp.Attributes("onclick") = "moveOptionUp(this.form['lstAttributes'])"
        'btnMoveDown.Attributes("onclick") = "moveOptionDown(this.form['lstAttributes'])"

        'Put user code to initialize the page here
        If Not Page.IsPostBack = True Then
            txtDocumentName.Text = Session("mAdminDocumentName")
            dtAttributesSelected = Session("SelectedAttributes")
            lstAttributes.DataTextField = "AttributeName"
            lstAttributes.DataValueField = "AttributeID"
            lstAttributes.DataSource = dtAttributesSelected
            lstAttributes.DataBind()

        End If
    End Sub



    

   

    Private Function AttributesSelected() As DataTable
        Dim dtAttributes As New DataTable
        Dim dcID As New DataColumn("AttributeID", GetType(System.Int64), Nothing)
        Dim dcValue As New DataColumn("AttributeName", GetType(System.String), Nothing)
        Dim dcOrder As New DataColumn("DisplayOrder", GetType(System.Int64), Nothing)
        dtAttributes.Columns.Add(dcID)
        dtAttributes.Columns.Add(dcValue)
        dtAttributes.Columns.Add(dcOrder)


        Dim iCount As Integer = lstAttributes.Items.Count
        Dim x


        For x = 0 To iCount - 1

            Dim dr As DataRow
            dr = dtAttributes.NewRow
            dr("AttributeID") = lstAttributes.Items(x).Value
            dr("AttributeName") = lstAttributes.Items(x).Text
            dr("DisplayOrder") = x + 1
            dtAttributes.Rows.Add(dr)


        Next

        Return dtAttributes

    End Function


    Private Sub btnFinish_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFinish.Click

        'Save attributes selected
        Session("SelectedAttributes") = AttributesSelected()
        Response.Redirect("ManageDocumentsAttributesRequired.aspx")

    End Sub

    Private Sub btnMoveUp_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnMoveUp.Click
        If lstAttributes.Items.Count = 0 Then Exit Sub

        Dim i As Integer = lstAttributes.SelectedIndex

        If i > 0 Then
            Dim listItem As System.Web.UI.WebControls.ListItem = lstAttributes.SelectedItem
            lstAttributes.Items.RemoveAt(i)
            lstAttributes.Items.Insert(i - 1, listItem)
            lstAttributes.SelectedIndex = i - 1
            lblError.Text = ""
        End If
    End Sub

    Private Sub btnMoveDown_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnMoveDown.Click
        If lstAttributes.Items.Count = 0 Then Exit Sub

        Dim i As Integer = lstAttributes.SelectedIndex

        If i < lstAttributes.Items.Count - 1 Then
            Dim listItem As System.Web.UI.WebControls.ListItem = lstAttributes.SelectedItem
            lstAttributes.Items.RemoveAt(i)
            lstAttributes.Items.Insert(i + 1, listItem)
            lstAttributes.SelectedIndex = i + 1
            lblError.Text = ""
        End If
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("ManageDocuments.aspx")

    End Sub
End Class

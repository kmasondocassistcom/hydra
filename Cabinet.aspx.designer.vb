﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'     Runtime Version:2.0.50727.3082
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On



Partial Public Class Cabinet

    '''<summary>
    '''Form1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Form1 As Global.System.Web.UI.HtmlControls.HtmlForm

    '''<summary>
    '''tblShare control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tblShare As Global.System.Web.UI.HtmlControls.HtmlTable

    '''<summary>
    '''btnNewShare control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnNewShare As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''btnShareNewFolder control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnShareNewFolder As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''Rename control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Rename As Global.System.Web.UI.HtmlControls.HtmlAnchor

    '''<summary>
    '''imgRenameShare control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents imgRenameShare As Global.System.Web.UI.HtmlControls.HtmlImage

    '''<summary>
    '''btnRemoveFolder control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnRemoveFolder As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''treeSearch control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents treeSearch As Global.ComponentArt.Web.UI.TreeView

    '''<summary>
    '''NodeId control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents NodeId As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''NewText control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents NewText As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''NodeText control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents NodeText As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''ParentFolder control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ParentFolder As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''btnRename control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnRename As Global.System.Web.UI.WebControls.ImageButton
End Class

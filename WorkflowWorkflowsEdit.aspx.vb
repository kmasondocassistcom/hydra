Imports Accucentric.docAssist.Data.Images

Partial Class WorkflowWorkflowsEdit
    Inherits System.Web.UI.Page


    Private intWorkflowId As Integer
    Private intMode As Integer


    Private pageTitle As New System.Web.UI.HtmlControls.HtmlGenericControl

    Private mobjUser As Accucentric.docAssist.Web.Security.User


    Private tmpSubAssigned As New DataTable
    Private tmpSubAvailable As New DataTable

    Private tmpDocAssigned As New DataTable
    Private tmpDocAvailable As New DataTable

    Private tmpQueAssigned As New DataTable
    Private tmpQueAvailable As New DataTable



#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        txtWorkflowName.Attributes("onkeypress") = "submitForm();"

        Try
            intMode = CInt(Request.QueryString("Mode"))
            intWorkflowId = CInt(Request.QueryString("ID"))
        Catch ex As Exception

        End Try

        'Security
        Try
            Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
            Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))
            Me.mobjUser.AccountId = guidAccountId
        Catch ex As Exception
            Response.Redirect("Default.aspx")
        End Try

        If Page.IsPostBack = False Then
            'Actions 
            Try
                Select Case intMode
                    Case 0
                        'lblMessage.Text = "Enter a name for the new category." 
                        LoadNewWorkflow()
                    Case 1
                        'Load category to change.
                        LoadExistingWorkflow()
                    Case 2

                End Select

            Catch ex As Exception

            End Try
        End If

        'Mode
        'TODO: Page title

    End Sub

    Private Function LoadExistingWorkflow()

        Dim sqlConImages As New SqlClient.SqlConnection

        Try
            sqlConImages = Functions.BuildConnection(Me.mobjUser)
            Dim objWf As New Workflow(sqlConImages, False)

            '
            ' Category
            '
            Dim dtCategory As New DataTable
            dtCategory = objWf.WFCategoryGet()
            ddlCategories.DataTextField = "CategoryName"
            ddlCategories.DataValueField = "CategoryId"
            ddlCategories.DataSource = dtCategory
            ddlCategories.DataBind()

            Dim lstitem As New ListItem
            lstitem.Text = ""
            lstitem.Value = 0
            lstitem.Selected = True
            ddlCategories.Items.Add(lstitem)


            Dim dtWorkflow As New DataTable
            dtWorkflow = objWf.WFWorkflowGetID(intWorkflowId)
            txtWorkflowName.Text = dtWorkflow.Rows(0)("WorkflowName")
            chkEnabled.Checked = dtWorkflow.Rows(0)("WorkflowEnabled")
            If Not IsNothing(ddlCategories.Items.FindByValue(dtWorkflow.Rows(0)("CategoryID"))) Then ddlCategories.SelectedValue = dtWorkflow.Rows(0)("CategoryID")

            '
            ' Submitters
            '
            Dim dtSubAvailable As New DataTable
            Dim dtSubAssigned As New DataTable

            dtSubAvailable = objWf.WFWorkflowSubmittersGetAvailableID(Me.mobjUser.AccountId.ToString, intWorkflowId)
            lstSubmitAvailable.DataTextField = "UserName"
            lstSubmitAvailable.DataValueField = "UserId"
            lstSubmitAvailable.DataSource = dtSubAvailable
            lstSubmitAvailable.DataBind()

            dtSubAssigned = objWf.WFWorkflowSubmittersGetAssignedID(Me.mobjUser.AccountId.ToString, intWorkflowId)
            lstSubmitAssigned.DataTextField = "UserName"
            lstSubmitAssigned.DataValueField = "UserId"
            lstSubmitAssigned.DataSource = dtSubAssigned
            lstSubmitAssigned.DataBind()
            '
            ' Documents
            '
            Dim dtDocAvailable As New DataTable
            Dim dtDocAssigned As New DataTable

            dtDocAvailable = objWf.WFWorkflowDocumentsGetAvailableID(intWorkflowId)
            lstDocumentsAvailable.DataTextField = "DocumentName"
            lstDocumentsAvailable.DataValueField = "DocumentId"
            lstDocumentsAvailable.DataSource = dtDocAvailable
            lstDocumentsAvailable.DataBind()

            dtDocAssigned = objWf.WFWorkflowDocumentsGetAssignedID(intWorkflowId)
            lstDocumentsAssigned.DataTextField = "DocumentName"
            lstDocumentsAssigned.DataValueField = "DocumentId"
            lstDocumentsAssigned.DataSource = dtDocAssigned
            lstDocumentsAssigned.DataBind()
            '
            ' Queues
            '
            Dim dtQueAvailable As New DataTable
            Dim dtQueAssigned As New DataTable

            dtQueAvailable = objWf.WFWorkflowQueuesGetAvailableID(intWorkflowId)
            lstQueuesAvailable.DataTextField = "QueueName"
            lstQueuesAvailable.DataValueField = "QueueId"
            lstQueuesAvailable.DataSource = dtQueAvailable
            lstQueuesAvailable.DataBind()

            dtQueAssigned = objWf.WFWorkflowQueuesGetAssignedID(intWorkflowId)
            lstQueuesAssigned.DataTextField = "QueueName"
            lstQueuesAssigned.DataValueField = "QueueId"
            lstQueuesAssigned.DataSource = dtQueAssigned
            lstQueuesAssigned.DataBind()

        Catch ex As Exception
            If sqlConImages.State <> ConnectionState.Closed Then sqlConImages.Close()
            sqlConImages = Nothing
        Finally
            If sqlConImages.State <> ConnectionState.Closed Then sqlConImages.Close()
            sqlConImages = Nothing
        End Try

    End Function

    Private Function LoadNewWorkflow()

        Dim sqlConImages As New SqlClient.SqlConnection

        Try
            chkEnabled.Checked = True

            sqlConImages = Functions.BuildConnection(Me.mobjUser)
            Dim objWf As New Workflow(sqlConImages, False)

            '
            ' Category
            '
            Dim dtCategory As New DataTable
            dtCategory = objWf.WFCategoryGet()
            ddlCategories.DataTextField = "CategoryName"
            ddlCategories.DataValueField = "CategoryId"
            ddlCategories.DataSource = dtCategory
            ddlCategories.DataBind()

            Dim lstitem As New ListItem
            lstitem.Text = ""
            lstitem.Value = -1
            lstitem.Selected = True
            ddlCategories.Items.Add(lstitem)

            '
            ' Submitters
            '
            Dim dtSubAvailable As New DataTable
            Dim dtSubAssigned As New DataTable

            dtSubAvailable = objWf.WFWorkflowSubmittersGetAvailableID(Me.mobjUser.AccountId.ToString, 0)
            lstSubmitAvailable.DataTextField = "UserName"
            lstSubmitAvailable.DataValueField = "UserId"
            lstSubmitAvailable.DataSource = dtSubAvailable
            lstSubmitAvailable.DataBind()

            dtSubAssigned = objWf.WFWorkflowSubmittersGetAssignedID(Me.mobjUser.AccountId.ToString, 0)
            lstSubmitAssigned.DataTextField = "UserName"
            lstSubmitAssigned.DataValueField = "UserId"
            lstSubmitAssigned.DataSource = dtSubAssigned
            lstSubmitAssigned.DataBind()
            '
            ' Documents
            '
            Dim dtDocAvailable As New DataTable
            Dim dtDocAssigned As New DataTable

            dtDocAvailable = objWf.WFWorkflowDocumentsGetAvailableID(0)
            lstDocumentsAvailable.DataTextField = "DocumentName"
            lstDocumentsAvailable.DataValueField = "DocumentId"
            lstDocumentsAvailable.DataSource = dtDocAvailable
            lstDocumentsAvailable.DataBind()

            dtDocAssigned = objWf.WFWorkflowDocumentsGetAssignedID(0)
            lstDocumentsAssigned.DataTextField = "DocumentName"
            lstDocumentsAssigned.DataValueField = "DocumentId"
            lstDocumentsAssigned.DataSource = dtDocAssigned
            lstDocumentsAssigned.DataBind()
            '
            ' Queues
            '
            Dim dtQueAvailable As New DataTable
            Dim dtQueAssigned As New DataTable

            dtQueAvailable = objWf.WFWorkflowQueuesGetAvailableID(0)
            lstQueuesAvailable.DataTextField = "QueueName"
            lstQueuesAvailable.DataValueField = "QueueId"
            lstQueuesAvailable.DataSource = dtQueAvailable
            lstQueuesAvailable.DataBind()

            dtQueAssigned = objWf.WFWorkflowQueuesGetAssignedID(0)
            lstQueuesAssigned.DataTextField = "QueueName"
            lstQueuesAssigned.DataValueField = "QueueId"
            lstQueuesAssigned.DataSource = dtQueAssigned
            lstQueuesAssigned.DataBind()

        Catch ex As Exception
            If sqlConImages.State <> ConnectionState.Closed Then sqlConImages.Close()
            sqlConImages = Nothing
        Finally
            If sqlConImages.State <> ConnectionState.Closed Then sqlConImages.Close()
            sqlConImages = Nothing
        End Try

    End Function


    Private Function LoadGroup()

        'Try
        '    Dim wfGroups As New Accucentric.docAssist.Data.Images.Workflow(Functions.BuildConnection(Me.mobjUser))


        '    Dim dtAssisgnedUsers As New DataTable
        '    dtAssisgnedUsers = wfGroups.WFGroupMembersGetAssignedID(Me.mobjUser.AccountId.ToString, intWorkflowId)
        '    lstAssisgned.DataSource = dtAssisgnedUsers
        '    lstAssisgned.DataBind()

        '    wfGroups = New Accucentric.docAssist.Data.Images.Workflow(Functions.BuildConnection(Me.mobjUser))
        '    Dim dtAvailableUsers As DataTable
        '    dtAvailableUsers = wfGroups.WFGroupMembersGetAvailableID(Me.mobjUser.AccountId.ToString, intWorkflowId)

        '    lstAvailable.DataTextField = "UserName"
        '    lstAvailable.DataValueField = "UserId"
        '    lstAvailable.DataSource = dtAvailableUsers
        '    lstAvailable.DataBind()
        'Catch ex As Exception

        'End Try

    End Function

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click

        Try
            If txtWorkflowName.Text.Length = 0 Then
                lblError.Text = "Workflow Name is required."
                lblError.Visible = True
                'ReturnControls.Add(lblError)
                Exit Sub
            End If
        Catch ex As Exception

        End Try

        Dim sqlTran As SqlClient.SqlTransaction

        Try

            Dim sqlConImages As New SqlClient.SqlConnection
            sqlConImages = Functions.BuildConnection(Me.mobjUser)

            If sqlConImages.State <> ConnectionState.Open Then sqlConImages.Open()

            sqlTran = sqlConImages.BeginTransaction

            Dim wfGroup As New Accucentric.docAssist.Data.Images.Workflow(sqlConImages, False, sqlTran)

            Select Case intMode
                Case 0 'Add

                    Dim intWFId As Integer = wfGroup.WFWorkflowInsert(txtWorkflowName.Text, "", ddlCategories.SelectedValue, chkEnabled.Checked)

                    If intWFId = 0 Then
                        lblError.Text = "Workflow already exists."
                        lblError.Visible = True
                        'ReturnControls.Add(lblError)
                        Exit Sub
                    End If

                    'Submitters
                    For Each lstItem As ListItem In lstSubmitAssigned.Items
                        wfGroup.WFWorkflowSubmitterInsert(intWFId, lstItem.Value)
                    Next

                    'Documents
                    For Each lstItem As ListItem In lstDocumentsAssigned.Items
                        wfGroup.WFWorkflowDocumentInsert(intWFId, lstItem.Value)
                    Next

                    'Queues
                    For Each lstItem As ListItem In lstQueuesAssigned.Items
                        wfGroup.WFWorkflowQueueInsert(intWFId, lstItem.Value)
                    Next

                Case 1 'Edit

                    Dim intWFId As Integer = wfGroup.WFWorkflowUpdate(intWorkflowId, txtWorkflowName.Text, "", ddlCategories.SelectedValue, chkEnabled.Checked)

                    If intWFId = 0 Then
                        lblError.Text = "Workflow already exists."
                        lblError.Visible = True
                        'ReturnControls.Add(lblError)
                        Exit Sub
                    End If

                    'Submitters
                    If lstSubmitAssigned.Items.Count = 0 Then
                        wfGroup.WFWorkflowSubmitterInsert(intWorkflowId, 0)
                    Else

                        wfGroup.WFWorkflowSubmitterInsert(intWorkflowId, 0)
                        For Each lstItem As ListItem In lstSubmitAssigned.Items
                            wfGroup.WFWorkflowSubmitterInsert(intWorkflowId, lstItem.Value)
                        Next
                    End If

                    'Documents
                    If lstDocumentsAssigned.Items.Count = 0 Then
                        wfGroup.WFWorkflowDocumentInsert(intWorkflowId, -1)
                    Else
                        wfGroup.WFWorkflowDocumentInsert(intWorkflowId, -1)
                        For Each lstItem As ListItem In lstDocumentsAssigned.Items
                            wfGroup.WFWorkflowDocumentInsert(intWorkflowId, lstItem.Value)
                        Next
                    End If

                    'Queues
                    If lstQueuesAssigned.Items.Count = 0 Then
                        wfGroup.WFWorkflowQueueInsert(intWorkflowId, 0)
                    Else
                        wfGroup.WFWorkflowQueueInsert(intWorkflowId, 0)
                        For Each lstItem As ListItem In lstQueuesAssigned.Items
                            wfGroup.WFWorkflowQueueInsert(intWorkflowId, lstItem.Value)
                        Next
                    End If


                Case 2

            End Select

            System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, "Close", "opener.location.reload();window.close();", True)

        Catch ex As Exception
            sqlTran.Rollback()
        Finally
            sqlTran.Commit()
        End Try

    End Sub

    'Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)

    '    Try
    '        If lstAvailable.SelectedIndex = -1 Then Exit Sub

    '        Dim mdcUserId As New DataColumn
    '        Dim mdcUserName As New DataColumn
    '        mdcUserId = New DataColumn("UserId", GetType(System.Int32), Nothing, MappingType.Element)
    '        mdcUserName = New DataColumn("UserName", GetType(System.String), Nothing, MappingType.Element)
    '        dtAssisgnedUsers.Columns.AddRange(New DataColumn() {mdcUserId, mdcUserName})

    '        For x As Integer = 0 To lstAssisgned.Items.Count - 1
    '            Dim dr2 As DataRow
    '            dr2 = dtAssisgnedUsers.NewRow
    '            dr2("UserId") = lstAssisgned.Items(x).Value
    '            dr2("UserName") = lstAssisgned.Items(x).Text
    '            dtAssisgnedUsers.Rows.Add(dr2)
    '        Next

    '        Dim lstItem As New ListItem
    '        lstItem = lstAvailable.SelectedItem

    '        'lstAssisgned.Items.Add(lstItem)
    '        lstAvailable.Items.Remove(lstItem)

    '        Dim dr As DataRow
    '        dr = dtAssisgnedUsers.NewRow

    '        dr("UserId") = lstItem.Value
    '        dr("UserName") = lstItem.Text

    '        dtAssisgnedUsers.Rows.Add(dr)

    '        lstAssisgned.DataSource = dtAssisgnedUsers
    '        lstAssisgned.DataTextField = "UserName"
    '        lstAssisgned.DataValueField = "UserId"
    '        lstAssisgned.DataBind()





    '        'ReturnControls.Add(lstAssisgned)
    '        'ReturnControls.Add(lstAvailable)


    '    Catch ex As Exception

    '    End Try
    'End Sub

    'Private Sub btnRemove_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)

    '    Try
    '        If lstAssisgned.SelectedIndex = -1 Then Exit Sub

    '        'Create table that contains available users
    '        Dim mdcUserId As New DataColumn
    '        Dim mdcUserName As New DataColumn
    '        mdcUserId = New DataColumn("UserId", GetType(System.Int32), Nothing, MappingType.Element)
    '        mdcUserName = New DataColumn("UserName", GetType(System.String), Nothing, MappingType.Element)
    '        dtAvailableUsers.Columns.AddRange(New DataColumn() {mdcUserId, mdcUserName})

    '        'Get current 
    '        For x As Integer = 0 To lstAvailable.Items.Count - 1
    '            Dim dr2 As DataRow
    '            dr2 = dtAvailableUsers.NewRow
    '            dr2("UserId") = lstAvailable.Items(x).Value
    '            dr2("UserName") = lstAvailable.Items(x).Text
    '            dtAvailableUsers.Rows.Add(dr2)
    '        Next

    '        Dim lstItem As New ListItem
    '        lstItem = lstAssisgned.SelectedItem
    '        lstAssisgned.Items.Remove(lstItem)

    '        Dim dr As DataRow
    '        dr = dtAvailableUsers.NewRow

    '        dr("UserId") = lstItem.Value
    '        dr("UserName") = lstItem.Text

    '        dtAvailableUsers.Rows.Add(dr)


    '        lstAvailable.DataSource = dtAvailableUsers
    '        lstAvailable.DataTextField = "UserName"
    '        lstAvailable.DataValueField = "UserId"
    '        lstAvailable.DataBind()


    '        'ReturnControls.Add(lstAssisgned)
    '        'ReturnControls.Add(lstAvailable)

    '    Catch ex As Exception

    '    End Try

    'End Sub

    Private Sub btnSubmittersAdd_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSubmittersAdd.Click

        Try

            If lstSubmitAvailable.SelectedIndex = -1 Then Exit Sub

            Dim mdcUserId As New DataColumn
            Dim mdcUserName As New DataColumn
            mdcUserId = New DataColumn("UserId", GetType(System.Int32), Nothing, MappingType.Element)
            mdcUserName = New DataColumn("UserName", GetType(System.String), Nothing, MappingType.Element)
            tmpSubAssigned.Columns.AddRange(New DataColumn() {mdcUserId, mdcUserName})

            For x As Integer = 0 To lstSubmitAssigned.Items.Count - 1
                Dim tmpDr As DataRow
                tmpDr = tmpSubAssigned.NewRow
                tmpDr("UserId") = lstSubmitAssigned.Items(x).Value
                tmpDr("UserName") = lstSubmitAssigned.Items(x).Text
                tmpSubAssigned.Rows.Add(tmpDr)
            Next

            Dim lstItem As New ListItem
            lstItem = lstSubmitAvailable.SelectedItem

            lstSubmitAvailable.Items.Remove(lstItem)

            Dim dr As DataRow
            dr = tmpSubAssigned.NewRow

            dr("UserId") = lstItem.Value
            dr("UserName") = lstItem.Text

            tmpSubAssigned.Rows.Add(dr)

            lstSubmitAssigned.DataSource = tmpSubAssigned
            lstSubmitAssigned.DataTextField = "UserName"
            lstSubmitAssigned.DataValueField = "UserId"
            lstSubmitAssigned.DataBind()

            'ReturnControls.Add(lstSubmitAssigned)
            'ReturnControls.Add(lstSubmitAvailable)

        Catch ex As Exception

        End Try

    End Sub

    Private Sub btnSubmittersRemove_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSubmittersRemove.Click

        Try

            If lstSubmitAssigned.SelectedIndex = -1 Then Exit Sub

            Dim mdcUserId As New DataColumn
            Dim mdcUserName As New DataColumn
            mdcUserId = New DataColumn("UserId", GetType(System.Int32), Nothing, MappingType.Element)
            mdcUserName = New DataColumn("UserName", GetType(System.String), Nothing, MappingType.Element)
            tmpSubAvailable.Columns.AddRange(New DataColumn() {mdcUserId, mdcUserName})

            For x As Integer = 0 To lstSubmitAvailable.Items.Count - 1
                Dim tmpDr As DataRow
                tmpDr = tmpSubAvailable.NewRow
                tmpDr("UserId") = lstSubmitAvailable.Items(x).Value
                tmpDr("UserName") = lstSubmitAvailable.Items(x).Text
                tmpSubAvailable.Rows.Add(tmpDr)
            Next

            Dim lstItem As New ListItem
            lstItem = lstSubmitAssigned.SelectedItem

            lstSubmitAssigned.Items.Remove(lstItem)

            Dim dr As DataRow
            dr = tmpSubAvailable.NewRow

            dr("UserId") = lstItem.Value
            dr("UserName") = lstItem.Text

            tmpSubAvailable.Rows.Add(dr)

            lstSubmitAvailable.DataSource = tmpSubAvailable
            lstSubmitAvailable.DataTextField = "UserName"
            lstSubmitAvailable.DataValueField = "UserId"
            lstSubmitAvailable.DataBind()

            'ReturnControls.Add(lstSubmitAssigned)
            'ReturnControls.Add(lstSubmitAvailable)

        Catch ex As Exception

        End Try

    End Sub

    Private Sub btnDocumentsAdd_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDocumentsAdd.Click

        Try

            If lstDocumentsAvailable.SelectedIndex = -1 Then Exit Sub

            Dim mdcUserId As New DataColumn
            Dim mdcUserName As New DataColumn
            mdcUserId = New DataColumn("DocumentId", GetType(System.Int32), Nothing, MappingType.Element)
            mdcUserName = New DataColumn("DocumentName", GetType(System.String), Nothing, MappingType.Element)
            tmpDocAssigned.Columns.AddRange(New DataColumn() {mdcUserId, mdcUserName})

            For x As Integer = 0 To lstDocumentsAssigned.Items.Count - 1
                Dim tmpDr As DataRow
                tmpDr = tmpDocAssigned.NewRow
                tmpDr("DocumentId") = lstDocumentsAssigned.Items(x).Value
                tmpDr("DocumentName") = lstDocumentsAssigned.Items(x).Text
                tmpDocAssigned.Rows.Add(tmpDr)
            Next

            Dim lstItem As New ListItem
            lstItem = lstDocumentsAvailable.SelectedItem

            lstDocumentsAvailable.Items.Remove(lstItem)

            Dim dr As DataRow
            dr = tmpDocAssigned.NewRow

            dr("DocumentId") = lstItem.Value
            dr("DocumentName") = lstItem.Text

            tmpDocAssigned.Rows.Add(dr)

            lstDocumentsAssigned.DataSource = tmpDocAssigned
            lstDocumentsAssigned.DataTextField = "DocumentName"
            lstDocumentsAssigned.DataValueField = "DocumentId"
            lstDocumentsAssigned.DataBind()

            'ReturnControls.Add(lstDocumentsAssigned)
            'ReturnControls.Add(lstDocumentsAvailable)

        Catch ex As Exception

        End Try

    End Sub

    Private Sub btnDocumentsRemove_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDocumentsRemove.Click

        Try

            If lstDocumentsAssigned.SelectedIndex = -1 Then Exit Sub

            Dim mdcUserId As New DataColumn
            Dim mdcUserName As New DataColumn
            mdcUserId = New DataColumn("DocumentId", GetType(System.Int32), Nothing, MappingType.Element)
            mdcUserName = New DataColumn("DocumentName", GetType(System.String), Nothing, MappingType.Element)
            tmpDocAvailable.Columns.AddRange(New DataColumn() {mdcUserId, mdcUserName})

            For x As Integer = 0 To lstDocumentsAvailable.Items.Count - 1
                Dim tmpDr As DataRow
                tmpDr = tmpDocAvailable.NewRow
                tmpDr("DocumentId") = lstDocumentsAvailable.Items(x).Value
                tmpDr("DocumentName") = lstDocumentsAvailable.Items(x).Text
                tmpDocAvailable.Rows.Add(tmpDr)
            Next

            Dim lstItem As New ListItem
            lstItem = lstDocumentsAssigned.SelectedItem

            lstDocumentsAssigned.Items.Remove(lstItem)

            Dim dr As DataRow
            dr = tmpDocAvailable.NewRow

            dr("DocumentId") = lstItem.Value
            dr("DocumentName") = lstItem.Text

            tmpDocAvailable.Rows.Add(dr)

            lstDocumentsAvailable.DataSource = tmpDocAvailable
            lstDocumentsAvailable.DataTextField = "DocumentName"
            lstDocumentsAvailable.DataValueField = "DocumentId"
            lstDocumentsAvailable.DataBind()

            'ReturnControls.Add(lstDocumentsAssigned)
            'ReturnControls.Add(lstDocumentsAvailable)

        Catch ex As Exception

        End Try

    End Sub

    Private Sub btnQueuesAdd_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnQueuesAdd.Click

        Try

            If lstQueuesAvailable.SelectedIndex = -1 Then Exit Sub

            Dim mdcUserId As New DataColumn
            Dim mdcUserName As New DataColumn
            mdcUserId = New DataColumn("QueueId", GetType(System.Int32), Nothing, MappingType.Element)
            mdcUserName = New DataColumn("QueueName", GetType(System.String), Nothing, MappingType.Element)
            tmpQueAssigned.Columns.AddRange(New DataColumn() {mdcUserId, mdcUserName})

            For x As Integer = 0 To lstQueuesAssigned.Items.Count - 1
                Dim tmpDr As DataRow
                tmpDr = tmpQueAssigned.NewRow
                tmpDr("QueueId") = lstQueuesAssigned.Items(x).Value
                tmpDr("QueueName") = lstQueuesAssigned.Items(x).Text
                tmpQueAssigned.Rows.Add(tmpDr)
            Next

            Dim lstItem As New ListItem
            lstItem = lstQueuesAvailable.SelectedItem

            lstQueuesAvailable.Items.Remove(lstItem)

            Dim dr As DataRow
            dr = tmpQueAssigned.NewRow

            dr("QueueId") = lstItem.Value
            dr("QueueName") = lstItem.Text

            tmpQueAssigned.Rows.Add(dr)

            lstQueuesAssigned.DataSource = tmpQueAssigned
            lstQueuesAssigned.DataTextField = "QueueName"
            lstQueuesAssigned.DataValueField = "QueueId"
            lstQueuesAssigned.DataBind()

            'ReturnControls.Add(lstQueuesAssigned)
            'ReturnControls.Add(lstQueuesAvailable)

        Catch ex As Exception

        End Try

    End Sub

    Private Sub btnQueuesRemove_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnQueuesRemove.Click

        Try

            If lstQueuesAssigned.SelectedIndex = -1 Then Exit Sub

            Dim mdcUserId As New DataColumn
            Dim mdcUserName As New DataColumn
            mdcUserId = New DataColumn("QueueId", GetType(System.Int32), Nothing, MappingType.Element)
            mdcUserName = New DataColumn("QueueName", GetType(System.String), Nothing, MappingType.Element)
            tmpQueAvailable.Columns.AddRange(New DataColumn() {mdcUserId, mdcUserName})

            For x As Integer = 0 To lstQueuesAvailable.Items.Count - 1
                Dim tmpDr As DataRow
                tmpDr = tmpQueAvailable.NewRow
                tmpDr("QueueId") = lstQueuesAvailable.Items(x).Value
                tmpDr("QueueName") = lstQueuesAvailable.Items(x).Text
                tmpQueAvailable.Rows.Add(tmpDr)
            Next

            Dim lstItem As New ListItem
            lstItem = lstQueuesAssigned.SelectedItem

            lstQueuesAssigned.Items.Remove(lstItem)

            Dim dr As DataRow
            dr = tmpQueAvailable.NewRow

            dr("QueueId") = lstItem.Value
            dr("QueueName") = lstItem.Text

            tmpQueAvailable.Rows.Add(dr)

            lstQueuesAvailable.DataSource = tmpQueAvailable
            lstQueuesAvailable.DataTextField = "QueueName"
            lstQueuesAvailable.DataValueField = "QueueId"
            lstQueuesAvailable.DataBind()

            'ReturnControls.Add(lstQueuesAssigned)
            'ReturnControls.Add(lstQueuesAvailable)

        Catch ex As Exception

        End Try

    End Sub
End Class

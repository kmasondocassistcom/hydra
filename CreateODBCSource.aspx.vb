'Imports Accucentric.docAssist.Data.Images

'Partial Class CreateODBCSource
'    Inherits zumiControls.zumiPage

'    Private mconCompany As New SqlClient.SqlConnection
'    Protected WithEvents ObjectId As System.Web.UI.HtmlControls.HtmlInputHidden
'    Private mobjUser As Accucentric.docAssist.Web.Security.User

'#Region " Web Form Designer Generated Code "

'    'This call is required by the Web Form Designer.
'    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

'    End Sub

'    'NOTE: The following placeholder declaration is required by the Web Form Designer.
'    'Do not delete or move it.
'    Private designerPlaceholderDeclaration As System.Object

'    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
'        'CODEGEN: This method call is required by the Web Form Designer
'        'Do not modify it using the code editor.
'        InitializeComponent()
'    End Sub

'#End Region

'    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

'        Response.Expires = 0
'        Response.Cache.SetNoStore()
'        Response.AppendHeader("Pragma", "no-cache")

'        Try
'            Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
'        Catch ex As Exception
'            Response.Redirect("Default.aspx")
'            Exit Sub
'        End Try

'        Try

'            If Not Page.IsPostBack Then

'                If Not IsNothing(Request.QueryString("ID")) Then
'                    'Load source
'                    mconCompany = Functions.BuildConnection(Me.mobjUser)
'                    Dim Sync As New DataSync(mconCompany)

'                    Dim dt As New DataTable
'                    dt = Sync.DIObjectGet(CInt(Request.QueryString("ID")))

'                    ObjectName.Text = dt.Rows(0)("ObjectName")
'                    ODBCName.Text = dt.Rows(0)("ODBCName")
'                    UserID.Text = dt.Rows(0)("UID")
'                    Password.Text = dt.Rows(0)("PWD")
'                    ConfirmPassword.Text = dt.Rows(0)("PWD")

'                End If

'            End If

'        Catch ex As Exception

'#If DEBUG Then
'            Err.Text = ex.Message
'#Else
'            Err.Text = "An error has occured."
'#End If
'            Try
'                Functions.LogException(mobjUser.UserId.ToString, mobjUser.AccountId.ToString, "", "Error loading ODBC Source: " & ex.Message, ex.StackTrace, Server.MachineName, "", Request.UrlReferrer.ToString, Functions.BuildMasterConnection, True)
'            Catch loggingEx As Exception
'            End Try

'        Finally

'            ReturnControls.Add(Err)

'            If mconCompany.State <> ConnectionState.Closed Then
'                mconCompany.Close()
'                mconCompany.Dispose()
'            End If
'        End Try

'    End Sub

'    Private Sub Save_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Save.Click

'        Try

'            If Password.Text <> ConfirmPassword.Text Then
'                Err.Text = "Passwords must match."
'                Err.Visible = True
'                Exit Sub
'            End If

'            mconCompany = Functions.BuildConnection(Me.mobjUser)
'            Dim Sync As New DataSync(mconCompany)

'            If Not IsNothing(Request.QueryString("ID")) Then
'                'Update
'                Sync.DIObjectUpdate(CInt(Request.QueryString("ID")), ODBCName.Text, ObjectName.Text, UserID.Text, Password.Text)
'                ReturnScripts.Add("window.returnValue='REFRESH';window.close();")
'            Else
'                'Add
'                Dim objectId As Integer = Sync.DIObjectInsert(ObjectName.Text, ODBCName.Text, UserID.Text, Password.Text)
'                'If objectId > 0 Then ReturnScripts.Add("if confirm('Do you want to use the newly created ODBC source?'){ window.returnValue = '" & objectId.ToString & "�" & ODBCName.Text.ToString & "';}else{window.returnValue = undefined;}window.close();")
'                If objectId > 0 Then ReturnScripts.Add("window.returnValue = '" & objectId.ToString & "�" & ODBCName.Text.ToString & "';window.close();")
'            End If

'        Catch ex As Exception

'#If DEBUG Then
'            Err.Text = ex.Message
'#Else
'            err.Text = "An error has occured."
'#End If

'            Try
'                Functions.LogException(mobjUser.UserId.ToString, mobjUser.AccountId.ToString, "", "Error adding ODBC Source: " & ex.Message, ex.StackTrace, Server.MachineName, "", Request.UrlReferrer.ToString, Functions.BuildMasterConnection, True)
'            Catch loggingEx As Exception
'            End Try

'        Finally

'            ReturnControls.Add(Err)

'            If mconCompany.State <> ConnectionState.Closed Then
'                mconCompany.Close()
'                mconCompany.Dispose()
'            End If

'        End Try

'    End Sub

'End Class
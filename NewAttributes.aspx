<%@ Page Language="vb" AutoEventWireup="false" Codebehind="NewAttributes.aspx.vb" Inherits="docAssistWeb.NewAttributes"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title></title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="css/jquery-ui.css" type="text/css" rel="stylesheet">
		<LINK href="css/jquery.autocomplete.css" type="text/css" rel="stylesheet">
		<LINK href="css/jquery.alerts.css" type="text/css" rel="stylesheet">
		<LINK href="css/jquery.alerts.css" type="text/css" rel="stylesheet">
		<LINK href="css/core.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<table width="270" height="97%">
				<tr>
					<td align="right" height="100%" valign="top">
						<table width="100%">
							<tr>
								<td colspan="2" height="25" id="stat">
									<span id="statMessage"></span>
								</td>
							</tr>
							<tr>
								<td nowrap><img id="help" src="Images/help_question.jpg"></td>
								<td colspan="1" align="right" width="100%">
									<a onclick="save();"><img src="Images/smallred_save.gif" border="0"></a>
								</td>
							</tr>
							<tr>
								<td colspan="2"><h3>Document Information</h3>
								</td>
							</tr>
							<tr>
								<td align="right">Folder:</td>
								<td nowrap>
									<input type="text" id="txtFolder" size="22" readonly>&nbsp;<IMG id="ChooseFolder" style="CURSOR: hand" onclick="folderLookup();" height="19" alt="Choose Folder"
										src="Images/addToFolder_btn.gif"><IMG height="1" src="spacer.gif" width="5"><IMG id="ClearFolder" style="CURSOR: hand" onclick="clearFolder();" height="20" alt="Clear Folder"
										src="Images/clear.gif" runat="server">
								</td>
							</tr>
							<TR>
								<TD align="right">Title:</TD>
								<td>
									<input type="text" id="txtTitle" size="32">
								</td>
							</TR>
							<tr>
								<td align="right">Description:</td>
								<td>
									<input type="text" id="txtDescription" size="32">
								</td>
							</tr>
							<tr>
								<td align="right">Tags:</td>
								<td>
									<input type="text" id="txtTags" size="32">
								</td>
							</tr>
							<tr>
								<td align="right">
									Document:
								</td>
								<td>
									<select id="ddlDocuments" style="WIDTH:191px">
									</select>
								</td>
							</tr>
						</table>
						<table width="100%">
							<tr id="error" style="DISPLAY: none">
								<td>
									<div class="ui-widget">
										<div class="ui-state-error ui-corner-all" style="PADDING-RIGHT: 0.7em; PADDING-LEFT: 0.7em; PADDING-BOTTOM: 0px; PADDING-TOP: 0px">
											<p id="errMsg">
											</p>
										</div>
									</div>
								</td>
							</tr>
							<tr id="notice" style="DISPLAY: none">
								<td>
									<div class="ui-widget">
										<div class="ui-state-highlight ui-corner-all" style="PADDING-RIGHT: 0.7em; MARGIN-TOP: 20px; PADDING-LEFT: 0.7em; PADDING-BOTTOM: 0px; PADDING-TOP: 0px">
											<p id="noticeMsg"><span class="ui-icon ui-icon-info" style="FLOAT: left; MARGIN-RIGHT: 0.3em"></span>
											</p>
										</div>
									</div>
								</td>
							</tr>
						</table>
						<table width="100%">
							<tr>
								<td align="right" height="25">
									<a id="imgCapture" href="#" onclick="integrationCapture();"><img src="Images/capture.gif" border="0" alt="Capture Attributes"></a>
									<a id="imgAdd" href="#" onclick="addAttribute();"><img src="Images/bot23.gif" border="0" alt="Add Attribute"></a>
									<a id="imgDelete" href="#" onclick="remove();"><img src="Images/circle_minus.gif" border="0" alt="Remove Selected Attribute"></a>
								</td>
							</tr>
							<tr>
								<td align="left" valign="top">
									<div id="attributeScroll" style="OVERFLOW: auto">
										<table id="grid" cellspacing="0" width="200">
											<DIV></DIV>
										</table>
									</div>
								</td>
							</tr>
						</table>
						<input type="hidden" id="xml"><INPUT id="FolderId" type="hidden" value="0">
					</td>
				</tr>
				<tr>
					<td>
						<table width="100%" ID="tblWorkflow">
							<!--<tr>
								<td colspan="2"><h3>Workflow</h3>
								</td>
							</tr>-->
							<tr>
								<td align="right">Workflow:</td>
								<td align="right">
									<select id="Workflow" style="WIDTH:205px">
									</select>
								</td>
							</tr>
							<tr>
								<td align="right">Queue:</td>
								<td align="right">
									<select id="ddlQueue" style="WIDTH:205px">
									</select>
								</td>
							</tr>
							<tr>
								<td>&nbsp;&nbsp;&nbsp;&nbsp;Notes:</td>
								<td align="right">
									<input type="checkbox" id="chkUrgent" class="checkbox">Urgent
								</td>
							</tr>
							<tr>
								<td align="right" colspan="2"><textarea style="WIDTH:250px;HEIGHT:75px" id="txtNotes"></textarea>
								</td>
							</tr>
							<!--<tr>
								<td></td>
								<td>
									<span id="wfInstructions"></span>
								</td>
							</tr>-->
						</table>
					</td>
				</tr>
			</table>
			<DIV id="webSvc" style="BEHAVIOR: url(webservice.htc)"></DIV>
			<script src="scripts/jquery.js" type="text/javascript"></script>
			<script src="scripts/jquery.ui.draggable.js" type="text/javascript"></script>
			<script src="scripts/jquery.alerts.js" type="text/javascript"></script>
			<script src="scripts/jquery.numeric.pack.js" type="text/javascript"></script>
			<script src="scripts/ui.datepicker.js" type="text/javascript"></script>
			<script src="scripts/jquery-ui.min.js" type="text/javascript"></script>
			<script src="scripts/jquery.qtip-1.0.0.min.js" type="text/javascript"></script>
			<script src="scripts/jquery.maskedinput.js" type="text/javascript"></script>
			<script src="scripts/lightboxDialog.js" type="text/javascript"></script>
			<script src="scripts/effects.core.js" type="text/javascript"></script>
			<script src="scripts/effects.highlight.js" type="text/javascript"></script>
			<script src="scripts/attributes.js" type="text/javascript"></script>
			<script>
				function folderLookup() {
					var val = window.showModalDialog('Tree.aspx?','FolderLookup','resizable:no;status:no;dialogHeight:625px;dialogWidth:260px;scroll:no');
					if (val != undefined) { varArray = val.split('�');$("#txtFolder").val(varArray[1]);	$("#FolderId").val(varArray[0].replace('W',''));}
				}
				function clearFolder() {
					$("#txtFolder").val('');$("#FolderId").val('0');		
				}			
			</script>
		</form>
	</body>
</HTML>

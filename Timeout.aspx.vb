Partial Class Timeout
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here

        Dim i As Integer

        Try
            For i = 0 To Request.Cookies.Count - 1
                Response.Cookies.Remove(Request.Cookies(i).Name)
            Next
        Catch ex As Exception
        End Try

        Response.Cookies("docAssist")("UserId") = ""
        Response.Cookies("docAssist")("AccountId") = ""
        Response.Cookies("docAssist")("IndexScanTabs") = ""
        Response.Cookies("docAssist").Expires = New DateTime(1990, 1, 1)

        Response.Cookies("docAssistImage")("VersionId") = ""
        Response.Cookies("docAssistImage").Expires = New DateTime(1990, 1, 1)
        Response.Cookies.Clear()

        Dim cookieDocAssist As System.Web.HttpCookie = Request.Cookies("docAssist")
        If Not cookieDocAssist Is Nothing Then cookieDocAssist.Expires = Now().AddDays(-1)
        Dim cookieImages As System.Web.HttpCookie = Request.Cookies("docAssistImage")
        If Not cookieDocAssist Is Nothing Then cookieDocAssist.Expires = Now().AddDays(-1)
        Dim cookieSearch As New cookieSearch(Me.Page)
        cookieSearch.Expire()

        'Response.Redirect("Default.aspx")
    End Sub

End Class

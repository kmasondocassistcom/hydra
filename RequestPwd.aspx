<%@ Page Language="vb" AutoEventWireup="false" Codebehind="RequestPwd.aspx.vb" Inherits="docAssistWeb.RequestPwd"%>
<%@ Register TagPrefix="uc1" TagName="docHeader" Src="Controls/docHeader.ascx" %>
<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="Controls/MenuBar.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<TITLE>docAssist</TITLE>
		<META content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<META content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<META content="JavaScript" name="vs_defaultClientScript">
		<META content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<BODY onload="document.all.txtEMailAddress.focus();">
		<FORM id="Form1" method="post" runat="server">
			<TABLE height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<td colSpan="3"></td>
				</TR>
				<TR vAlign="middle" width="100%">
					<TD class="PageContent" vAlign="middle" align="center" width="100%" height="100%">
						<P>&nbsp; <IMG src="Images/request_pass.gif"><BR>
							<BR>
							&nbsp;&nbsp;&nbsp;&nbsp;<ASP:LABEL id="lblText" runat="server">Please enter your e-mail address. Your password will be sent to you.</ASP:LABEL><BR>
							&nbsp;&nbsp;&nbsp;
							<BR>
							&nbsp;&nbsp;&nbsp;
							<ASP:TEXTBOX id="txtEMailAddress" runat="server" WIDTH="270px"></ASP:TEXTBOX>&nbsp;
						</P>
						<P>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<A href="Default.aspx"><img src="Images/btn_cancel.gif" border="0"></A>&nbsp;&nbsp;
							<asp:ImageButton id="lnkSend" runat="server" ImageUrl="Images/btn_send.gif"></asp:ImageButton>&nbsp;&nbsp;&nbsp;
							<BR>
							<BR>
							&nbsp;&nbsp;&nbsp;
							<ASP:LABEL id="lblResults" runat="server" VISIBLE="False"></ASP:LABEL>
							<BR>
							&nbsp;&nbsp;&nbsp;</P>
					</TD>
				</TR>
			</TABLE>
			<INPUT id="junk" type="text" style="DISPLAY:none">
		</FORM>
	</BODY>
</HTML>

Partial Class ManageMapAdd
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private mobjUser As Accucentric.docAssist.Web.Security.User
    Private mconSqlImage As SqlClient.SqlConnection

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Not Page.IsPostBack Then


            If Session("mAdminMapTable") <> "INTEGRATION" Then
                chkAdvanced.Visible = False
                chkAdvanced.Checked = False
                btnNext.ImageUrl = "Images/btn_nextadmin.gif"
            Else
                btnNext.ImageUrl = "Images/btn_finish.gif"
                chkAdvanced.Visible = True
            End If
            PopulatePublishersList()
            PopulateProductList()
            PopulateVersionList()
            PopulateForms()

            Try

                If Not IsNothing(Session("mAdminIntegrationPublisherID")) Then
                    cmbPublisher.SelectedValue = Session("mAdminIntegrationPublisherID")
                    cmbPublisher_SelectedIndexChanged(Nothing, Nothing)
                End If


                If Not IsNothing(Session("mAdminIntegrationProductID")) Then
                    cmbProduct.SelectedValue = Session("mAdminIntegrationProductID")
                    cmbProduct_SelectedIndexChanged(Nothing, Nothing)

                End If

                If Not IsNothing(Session("mAdminIntegrationVersion")) Then
                    cmbVersion.SelectedItem.Text = Session("mAdminIntegrationVersion")
                    cmbVersion_SelectedIndexChanged(Nothing, Nothing)
                End If

                If Not IsNothing(Session("mAdminIntegrationFormID")) Then
                    cmbForm.SelectedValue = Session("mAdminIntegrationFormID")
                End If

            Catch ex As Exception

            End Try

        End If


    End Sub

    Private Function ApplicationUsed() As Boolean
        Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
        Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))

        Dim strMode As String
        If Session("mAdminMapTable") = "INTEGRATION" Then strMode = 0 Else strMode = 1
        If strMode = 1 Then Return False
        Me.mobjUser.AccountId = guidAccountId
        Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
        Me.mconSqlImage.Open()

        Dim cmdSql As SqlClient.SqlCommand

        Dim dtsql As New DataTable
        cmdSql = New SqlClient.SqlCommand("acsAdminApplicationIntegrationCheck", Me.mconSqlImage)
        cmdSql.CommandType = CommandType.StoredProcedure
        cmdSql.Parameters.Add("@FormID", cmbForm.SelectedValue)
        Dim dasql As New SqlClient.SqlDataAdapter(cmdSql)
        dasql.Fill(dtsql)


        If dtsql.Rows.Count > 0 Then
            Return True
        Else
            Return False

        End If

    End Function


    Private Sub PopulatePublishersList()

        Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
        Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))

        Me.mobjUser.AccountId = guidAccountId
        Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
        Me.mconSqlImage.Open()

        Dim cmdSql As SqlClient.SqlCommand

        Dim dtsql As New DataTable
        cmdSql = New SqlClient.SqlCommand("acsAdminIntegrationPublishers", Me.mconSqlImage)
        cmdSql.CommandType = CommandType.StoredProcedure
        Dim dasql As New SqlClient.SqlDataAdapter(cmdSql)
        dasql.Fill(dtsql)

        If dtsql.Rows.Count <= 0 Then
            'TODO: No Cabinets to change
            cmbPublisher.Items.Clear()
        Else
            cmbPublisher.DataTextField = "PublisherName"
            cmbPublisher.DataValueField = "PublisherID"
            cmbPublisher.DataSource = dtsql
            cmbPublisher.DataBind()

        End If

    End Sub

    '
    Private Sub PopulateProductList()

        If cmbPublisher.SelectedItem Is Nothing Then
            cmbPublisher.Items.Clear()
            Exit Sub
        End If


        Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
        Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))

        Me.mobjUser.AccountId = guidAccountId
        Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
        Me.mconSqlImage.Open()

        Dim cmdSql As SqlClient.SqlCommand

        Dim dtsql As New DataTable
        cmdSql = New SqlClient.SqlCommand("acsAdminIntegrationProducts", Me.mconSqlImage)
        cmdSql.CommandType = CommandType.StoredProcedure
        cmdSql.Parameters.Add("@PublisherID", cmbPublisher.SelectedValue)
        Dim dasql As New SqlClient.SqlDataAdapter(cmdSql)
        dasql.Fill(dtsql)

        If dtsql.Rows.Count <= 0 Then
            'TODO: No Cabinets to change
            cmbProduct.Items.Clear()
        Else
            cmbProduct.DataTextField = "ProductName"
            cmbProduct.DataValueField = "ProductID"
            cmbProduct.DataSource = dtsql
            cmbProduct.DataBind()
        End If

    End Sub



    Private Sub PopulateVersionList()


        If cmbProduct.SelectedItem Is Nothing Then
            cmbVersion.Items.Clear()
            Exit Sub
        End If


        Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
        Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))

        Me.mobjUser.AccountId = guidAccountId
        Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
        Me.mconSqlImage.Open()

        Dim cmdSql As SqlClient.SqlCommand

        Dim dtsql As New DataTable
        cmdSql = New SqlClient.SqlCommand("acsAdminIntegrationVersions", Me.mconSqlImage)
        cmdSql.CommandType = CommandType.StoredProcedure
        cmdSql.Parameters.Add("@ProductID", cmbProduct.SelectedValue)
        Dim dasql As New SqlClient.SqlDataAdapter(cmdSql)
        dasql.Fill(dtsql)

        If dtsql.Rows.Count <= 0 Then
            'TODO: No Cabinets to change
            cmbVersion.Items.Clear()
        Else
            cmbVersion.DataTextField = "Version"
            cmbVersion.DataValueField = "Version"
            cmbVersion.DataSource = dtsql
            cmbVersion.DataBind()
        End If

    End Sub


    Private Sub PopulateForms()

        If cmbVersion.SelectedItem Is Nothing Or cmbProduct.SelectedValue Is Nothing Then
            cmbForm.Items.Clear()
            Exit Sub
        End If


        Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
        Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))

        Me.mobjUser.AccountId = guidAccountId
        Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
        Me.mconSqlImage.Open()

        Dim cmdSql As SqlClient.SqlCommand

        Dim dtsql As New DataTable
        cmdSql = New SqlClient.SqlCommand("acsAdminIntegrationForms", Me.mconSqlImage)
        cmdSql.CommandType = CommandType.StoredProcedure
        cmdSql.Parameters.Add("@ProductID", cmbProduct.SelectedValue)
        cmdSql.Parameters.Add("@Version", cmbVersion.SelectedItem.Text)

        Dim dasql As New SqlClient.SqlDataAdapter(cmdSql)
        dasql.Fill(dtsql)

        If dtsql.Rows.Count <= 0 Then
            'TODO: No Cabinets to change
            cmbForm.Items.Clear()
        Else
            cmbForm.DataTextField = "FormName"
            cmbForm.DataValueField = "FormID"
            cmbForm.DataSource = dtsql
            cmbForm.DataBind()
        End If

    End Sub

    Private Sub cmbVersion_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbVersion.SelectedIndexChanged
        PopulateForms()
    End Sub

    Private Sub cmbPublisher_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbPublisher.SelectedIndexChanged
        PopulateProductList()
        PopulateVersionList()
        PopulateForms()
    End Sub

    Private Sub cmbProduct_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbProduct.SelectedIndexChanged
        PopulateVersionList()
        PopulateForms()
    End Sub



    Private Sub btnNext_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnNext.Click
        If cmbPublisher.SelectedItem Is Nothing Or cmbProduct.SelectedItem Is Nothing Or cmbVersion.SelectedItem Is Nothing Or cmbForm.SelectedItem Is Nothing Then
            lblError.Text = "All items must have a selection."
            Exit Sub
        End If

        ' If ApplicationUsed() Then
        ' lblError.Text = "This application is currently mapped to another document. You must change or remove the existing mapping before continuing."
        ' Exit Sub
        ' End If

        Session("mAdminIntegrationPublisherID") = cmbPublisher.SelectedValue
        Session("mAdminIntegrationProductID") = cmbProduct.SelectedValue
        Session("mAdminIntegrationVersion") = cmbVersion.SelectedItem.Text
        Session("mAdminIntegrationFormID") = cmbForm.SelectedValue

        Session("mAdminIntegrationPublisherName") = cmbPublisher.SelectedItem.Text
        Session("mAdminIntegrationProductName") = cmbProduct.SelectedItem.Text
        Session("mAdminIntegrationFormName") = cmbForm.SelectedItem.Text

        If Session("mAdminMapTable") = "INTEGRATION" Then

            If chkAdvanced.Checked = False Then 'Save Simple Integration
                SaveSimpleIntegration()
                Response.Redirect("ManageIntegration.aspx")
                Exit Sub
            End If
        End If

        Response.Redirect("ManageMapAddDetail.aspx")



    End Sub

    Private Sub SaveSimpleIntegration()
        Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
        Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))

        Me.mobjUser.AccountId = guidAccountId
        Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
        Me.mconSqlImage.Open()
        Dim cmdSql As SqlClient.SqlCommand
        Dim dtsql As New DataTable

        Dim iDocumentID As Integer = 0

        cmdSql = New SqlClient.SqlCommand("acsAdminIntegrationMapClear", Me.mconSqlImage)
        cmdSql.CommandType = CommandType.StoredProcedure
        cmdSql.Parameters.Add("@DocumentID", iDocumentID)
        cmdSql.Parameters.Add("@FormID", Session("mAdminIntegrationFormID"))
        cmdSql.ExecuteNonQuery()


        cmdSql = New SqlClient.SqlCommand("acsAdminIntegrationMapAdd", Me.mconSqlImage)
        cmdSql.CommandType = CommandType.StoredProcedure
        cmdSql.Parameters.Add("@DocumentID", iDocumentID)
        cmdSql.Parameters.Add("@FormID", Session("mAdminIntegrationFormID"))
        cmdSql.ExecuteNonQuery()




    End Sub
    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("ManageIntegration.aspx")
    End Sub

    Private Sub chkAdvanced_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkAdvanced.CheckedChanged
        If chkAdvanced.Checked = True Then
            btnNext.ImageUrl = "Images/btn_nextadmin.gif"
        Else
            btnNext.ImageUrl = "Images/btn_finish.gif"
        End If

    End Sub
End Class

<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ListLookup.aspx.vb" Inherits="docAssistWeb.ListLookup"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>docAssist - List Lookup</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" onload="initWS();" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<DIV id="svcFilter" style="BEHAVIOR: url(webservice.htc)" onresult="displayList()"></DIV>
			<DIV id="svcAdd" style="BEHAVIOR: url(webservice.htc)" onresult="itemAdded()"></DIV>
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
					<td colSpan="2"><IMG height="7" src="spacer.gif" width="1"></td>
				</tr>
				<tr height="15">
					<td noWrap colSpan="1">
						<IMG height="1" src="spacer.gif" width="15"><asp:textbox id="txtSearchText" runat="server" AutoPostBack="False" Width="300px"></asp:textbox>
					</td>
					<td vAlign="baseline" noWrap align="left" width="100%">
						<table cellpadding="0" cellspacing="0" border="0" width="80">
							<tr align="right">
								<td width="33%"><A style="DISPLAY: none" id="lnkAdd" onclick="showExtra('add');" href="#" alt="Add Item">
										<IMG src="Images/list.add.gif" border="0"></A></td>
								<td width="33%"><A style="DISPLAY: none" id="lnkEdit" onclick="showExtra('edit');" href="#" alt="Edit Item">
										<IMG src="Images/list.edit.gif" border="0" alt="Edit Item"></A></td>
								<td width="33%">
									<A style="DISPLAY: none" id="lnkRemove" onclick="removeItem();" href="#"><IMG src="Images/list.delete.gif" border="0" alt="Remove Item"></A></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr id="addMode">
					<td noWrap colSpan="2" align="center">
						<table class="SearchGrid" cellSpacing="0" cellPadding="0" width="285" style="BORDER-RIGHT: #000000 thin solid; BORDER-TOP: #000000 thin solid; BORDER-LEFT: #000000 thin solid">
							<tr bgcolor="whitesmoke">
								<td><img src="spacer.gif" height="1" width="16"></td>
								<td nowrap width="100%"><b><span id="Mode"></span></b></td>
								<td></td>
								<td align="center" noWrap vAlign="baseline">&nbsp;<a href="#" onclick="hideExtra();"><img src="Images/list.close.gif" border="0"></a></td>
							</tr>
						</table>
						<table class="SearchGrid" cellSpacing="0" cellPadding="0" width="285" style="BORDER-RIGHT: #000000 thin solid; BORDER-LEFT: #000000 thin solid; BORDER-BOTTOM: #000000 thin solid">
							<tr bgcolor="whitesmoke">
								<td width="50%"></td>
								<td align="right">Value:</td>
								<td>&nbsp;<input type="text" id="txtValue" size="40" onkeypress="if (event.keyCode == 13) { if (numColumns == 1) { document.all.okBtn.click(); } else { document.all.txtDescription.focus(); } }"></td>
								<td width="50%"></td>
							</tr>
							<tr bgcolor="whitesmoke" id="descRow">
								<td></td>
								<td align="right">Description:</td>
								<td>&nbsp;<INPUT id="txtDescription" type="text" size="40" onkeypress="if (event.keyCode == 13) { document.all.okBtn.click(); }"></td>
								<td></td>
							</tr>
							<tr bgcolor="whitesmoke">
								<td align="right" colspan="4">
									<img src="Images/sm.btn.save.gif" style="CURSOR: hand" onclick="btnOk_click();" id="okBtn"></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr height="100%">
					<td colSpan="2">
						<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td><IMG height="1" src="spacer.gif" width="15"></td>
								<td vAlign="top" width="100%">
									<div style="OVERFLOW: auto; WIDTH: 100%; HEIGHT: 100%">
										<table cellpadding="0" cellspacing="0" width="100%">
											<tr style="CURSOR: hand">
												<td width="50%" class="SelectedColumn" id="colVal" onclick="setSortMode(0);">Value</td>
												<td width="50%" class="UnSelectedColumn" id="colDesc" onclick="setSortMode(1);">Description</td>
											</tr>
										</table>
										<div id="innerGrid" style="WIDTH: 100%; HEIGHT: 100%" align="left">Loading...</div>
									</div>
								</td>
								<td colSpan="1"><IMG height="1" src="spacer.gif" width="15"></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td align="right" colSpan="2">
						<asp:imagebutton id="btnFilter" runat="server" Width="0px" Height="0px"></asp:imagebutton><IMG src="Images/smallred_cancel.gif" style="CURSOR: hand" onclick="window.close();"><IMG height="1" src="spacer.gif" width="10">
						<IMG onclick="selectItem();" style="CURSOR: hand" src="Images/btn_small_ok.gif">
						<IMG height="1" src="spacer.gif" width="10"></td>
				</tr>
			</table>
			<input id="hiddenAttributeId" type="hidden" runat="server"> <input id="hiddenValue" type="hidden" runat="server">
			<input id="hiddenDesc" type="hidden" runat="server"> <input id="hiddenAttributeListId" type="hidden" runat="server"><INPUT id="hiddenFolderId" type="hidden" runat="server"><INPUT id="hiddenDocumentId" type="hidden" runat="server">
		</form>
		<script language="javascript">
		
				var callid;
				var hostname;
				var mode;
				var numColumns;
				var sortMode = 0;
				var accessLevel;
				
				var addMode = document.getElementById('addMode');
				if (addMode != undefined) { addMode.style.display = 'none'; }
				
				function setSortMode(modeVal)
				{
					sortMode = modeVal;
					
					if (modeVal == 0)
					{
						colVal.className = 'SelectedColumn';
						colDesc.className = 'UnSelectedColumn';	
					}
					else
					{
						colVal.className = 'UnSelectedColumn';
						colDesc.className = 'SelectedColumn';	
					}
					filterList();
				}
								
				function btnOk_click()
				{
					if (document.getElementById('txtValue').value == '')
					{
						alert('You must specify a value');
						return true;
					}
				
					if (mode == 'add')
					{
						//add new list item
						var svcAdd = document.getElementById('svcAdd');
						svcAdd.useService(hostname + "/docAssistImages.asmx?wsdl", "docAssistImages");
						svcAdd.docAssistImages.callService("AddListItem", document.getElementById('hiddenAttributeId').value, document.getElementById('txtValue').value, document.getElementById('txtDescription').value, document.getElementById('hiddenDocumentId').value, document.getElementById('hiddenFolderId').value);
					}
					else
					{
						//edit existing item
						var svcAdd = document.getElementById('svcAdd');
						svcAdd.useService(hostname + "/docAssistImages.asmx?wsdl", "docAssistImages");
						svcAdd.docAssistImages.callService("EditListItem", document.getElementById('hiddenAttributeId').value, document.getElementById('hiddenAttributeListId').value, document.getElementById('txtValue').value, document.getElementById('txtDescription').value);
					}
				}
							
				function removeItem()
				{
					if (document.getElementById('hiddenAttributeListId').value == '')
					{
						alert('You must select an item.');
						return false;
					}	
					else
					{
						var svcAdd = document.getElementById('svcAdd');
						svcAdd.useService(hostname + "/docAssistImages.asmx?wsdl", "docAssistImages");
						svcAdd.docAssistImages.callService("ListItemRemove", document.getElementById('hiddenAttributeListId').value, document.getElementById('txtValue').value, document.getElementById('txtDescription').value);
					}
				}
								
				function itemAdded()
				{
					if (event.result.value == -1)
					{
						alert('A list item with this value already exist.');
						document.getElementById('txtValue').focus();
						return true;
					}
					
					hideExtra();
					filterList();
				}
				
				function showExtra(e)
				{
					if (e == 'add')
					{
						mode = 'add';
						document.getElementById('Mode').innerHTML = 'Add List Item';
						document.getElementById('txtValue').value = '';
						document.getElementById('txtDescription').value = '';
					}
					else
					{
						if (document.getElementById('hiddenAttributeListId').value == '')
						{
							alert('Please select a list item to edit.');
							return true;
						}
						
						document.getElementById('txtValue').value = document.getElementById('hiddenValue').value;
						document.getElementById('txtDescription').value = document.getElementById('hiddenDesc').value;
						
						mode = 'edit';
						document.getElementById('Mode').innerHTML = 'Edit List Item';
					}
					
					if (numColumns == 1) { descRow.style.display = 'none'; }
										
					addMode.style.display = 'block';
					document.getElementById('txtValue').focus();											

					//disable list
					innerGrid.disabled = true;
				}
				
				function hideExtra()
				{
					addMode.style.display = 'none';
					
					//enable list
					innerGrid.disabled = false;
					
					document.getElementById('txtSearchText').focus();
				}
							
				function initWS()
				{
					var svcFilter = document.getElementById('svcFilter');
					svcFilter.useService(hostname + "/docAssistImages.asmx?wsdl", "docAssistImages");
					svcFilter.onServiceAvailable = filterList();
				}
			
				function filterList()
				{
					callid = svcFilter.docAssistImages.callService("FilterList", document.getElementById('hiddenAttributeId').value, sortMode, document.getElementById('txtSearchText').value, document.getElementById('hiddenDocumentId').value, document.getElementById('hiddenFolderId').value);
					document.getElementById('hiddenValue').value = '';
					document.getElementById('hiddenDesc').value = '';
					document.getElementById('hiddenAttributeListId').value = '';
				}

				function displayList()
				{
					innerGrid.innerHTML = event.result.value[0];
					numColumns = event.result.value[1];
					accessLevel = event.result.value[2];

					switch(accessLevel)
					{
						case '0':
							break;
						case '1':
							lnkAdd.style.display = 'inline';
							break;
						case '2':
							lnkAdd.style.display = 'inline';
							lnkEdit.style.display = 'inline';
							break;
						case '3':
							lnkAdd.style.display = 'inline';
							lnkEdit.style.display = 'inline';
							lnkRemove.style.display = 'inline';
							break;
						default:
							break;
					}					
					
					if (numColumns == 1)
					{
						colDesc.style.display = 'none';
						colVal.style.width = '100%';
					}
				}
				
				function pickItem(value,desc,id)
				{
					document.getElementById('hiddenValue').value = value;
					document.getElementById('hiddenDesc').value = desc;
					document.getElementById('hiddenAttributeListId').value = id;
				}
				
				function selectItem()
				{
					if (document.getElementById('hiddenAttributeListId').value == '')
					{
						alert('You must select an item.');
						return false;
					}	
					else
					{
						if (numColumns == 1)
						{
							window.returnValue = document.getElementById('hiddenValue').value + '�';
						}
						else
						{
							window.returnValue = document.getElementById('hiddenValue').value + '�' + document.getElementById('hiddenDesc').value;
						}

						window.close();
					}
				}

				function highlightTableRow(tblTemp, selectedRow) 
				{
					tblTemp = listTable;

					var tr = tblTemp.getElementsByTagName('tr'); 
					for(var j = 0; j < tr.length; j++) 
					{
						if(tr[j].parentNode.nodeName == 'TBODY') 
						{
							tr[j].style.backgroundColor = 'white';
						} 
					}
					
					selectedRow.style.backgroundColor = 'gainsboro';
				} 
				
		</script>
	</body>
</HTML>

<%@ Register TagPrefix="uc1" TagName="TopRight" Src="Controls/TopRight.ascx" %>
<%@ Register TagPrefix="uc1" TagName="docHeader" Src="Controls/docHeader.ascx" %>
<%@ Register TagPrefix="uc1" TagName="AdminNav" Src="Controls/AdminNav.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ManageMapAddDetailMap.aspx.vb" Inherits="docAssistWeb.ManageMapAddDetailMap"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>docAssist - Manage Integration</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<TABLE height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TBODY>
					<TR>
						<TD width="100%" colSpan="5"><uc1:docheader id="docHeader" runat="server"></uc1:docheader></TD>
					</TR>
					<TR vAlign="middle" height="100%">
						<TD class="Navigation1" height="100%" style="BORDER-RIGHT: #cccccc thin solid" vAlign="top">
							<uc1:AdminNav id="AdminNav1" runat="server"></uc1:AdminNav><IMG height="1" src="dummy.gif" width="120" DESIGNTIMEDRAGDROP="101"></TD>
						<TD vAlign="top" align="center" height="100%"><BR>
							<table cellSpacing="0" cellPadding="0" width="100%" border="0">
								<tr>
									<td><IMG height="1" src="dummy.gif" width="35"><IMG src="Images/manage_integration.gif"></td>
								</tr>
							</table>
							<table cellSpacing="0" cellPadding="0" width="100%">
					</TR>
					<tr height="50%">
					</tr>
					<tr height="15" width="100%">
						<td></td>
						<td align="left"><asp:label id="Label3" runat="server" Width="75px" Height="1px" Font-Size="8.25pt" Font-Names="Verdana">Application:</asp:label></td>
						<td><asp:label id="lblCurrent" runat="server" Width="100%" Height="3px" Font-Size="8.25pt" Font-Names="Verdana"></asp:label></td>
						<td></td>
					</tr>
					<tr height="15" width="100%">
						<td style="HEIGHT: 25px"></td>
						<td align="left"><asp:label id="Label4" runat="server" Width="64px" Height="3px" Font-Size="8.25pt" Font-Names="Verdana">Document:</asp:label></td>
						<TD style="HEIGHT: 25px"><asp:label id="lblDocument" runat="server" Width="360px" Height="3px" Font-Size="8.25pt" Font-Names="Verdana"></asp:label></TD>
						<TD style="HEIGHT: 30px" width="50%"></TD>
					<TR height="15" width="100%">
						<td style="HEIGHT: 21px" width="50%"></td>
						<td style="WIDTH: 72px; HEIGHT: 21px" align="left">&nbsp;
						</td>
						<td style="HEIGHT: 21px"><asp:label id="Label1" runat="server" Width="136px" Font-Size="8.25pt" Font-Names="Verdana">Application Form Fields</asp:label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<asp:label id="Label2" runat="server" Width="119px" Font-Size="8.25pt" Font-Names="Verdana">Document Attributes</asp:label></td>
						<td style="HEIGHT: 21px" width="50%"></td>
					</TR>
					<tr height="15" width="100%">
						<td style="WIDTH: 393px; HEIGHT: 14px" width="393"></td>
						<td style="WIDTH: 72px; HEIGHT: 14px">
							<P align="left">&nbsp;&nbsp;
							</P>
						</td>
						<TD style="HEIGHT: 14px" align="left"><asp:dropdownlist id="cmbAppFields" runat="server" Width="180px" Height="32px" Font-Size="8.25pt"
								Font-Names="Verdana"></asp:dropdownlist>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<asp:dropdownlist id="cmbAttributes" runat="server" Width="180px" Height="32px" Font-Size="8.25pt"
								Font-Names="Verdana"></asp:dropdownlist></TD>
						<TD style="HEIGHT: 14px" width="50%"><asp:imagebutton id="btnAdd" runat="server" ImageUrl="Images/add_item.gif"></asp:imagebutton></TD>
					</tr>
					<TR height="15" width="100%">
						<TD style="WIDTH: 393px; HEIGHT: 22px" width="393"></TD>
						<TD style="WIDTH: 72px; HEIGHT: 22px" vAlign="middle">
							<P align="left">&nbsp;&nbsp;
							</P>
						</TD>
						<TD style="HEIGHT: 22px" align="left">
							&nbsp;&nbsp;&nbsp;
							<asp:CheckBox id="chkInteractive" runat="server" Font-Names="Verdana" Font-Size="8.25pt" Width="153px"
								Text="Interactive Selection" TextAlign="Left" AutoPostBack="True"></asp:CheckBox>
						</TD>
						<TD style="HEIGHT: 22px" width="50%"></TD>
					</TR>
					<TR height="15" width="100%">
						<TD width="50%"></TD>
						<TD style="WIDTH: 72px; HEIGHT: 17px"><BR>
						</TD>
						<TD style="HEIGHT: 17px" valign="top">
							<P style="FONT-SIZE: 8pt; FONT-FAMILY: Verdana"><asp:label id="Label5" runat="server" Width="80px" Font-Size="8.25pt" Font-Names="Verdana">Mappings</asp:label><BR>
								<asp:listbox id="lstMappings" runat="server" Width="440px" Height="200px" Font-Size="8.25pt"
									Font-Names="Verdana"></asp:listbox><BR>
								<span id="FolderMappings" runat="server">
									<asp:label id="Label6" runat="server" Font-Names="Verdana" Font-Size="8.25pt" Width="150px"
										Font-Bold="True">Folder Mappings</asp:label><BR>
									Application Field:
									<asp:DropDownList id="ddlAppFolder" runat="server" Width="180px"></asp:DropDownList><BR>
									<BR>
									<asp:TextBox id="txtFolder" runat="server"></asp:TextBox><IMG height="1" src="spacer.gif" width="5"><IMG id="ChooseFolder" style="CURSOR: hand" onclick="FolderLookup()" height="19" alt="Choose Folder"
										src="Images/addToFolder_btn.gif" runat="server"><IMG height="1" src="spacer.gif" width="5"><IMG id="ClearFolder" style="CURSOR: hand" onclick="document.getElementById('FolderId').value = '';document.getElementById('txtFolder').value = '';ActivateSave();"
										height="20" alt="Clear Folder" src="Images/clear.gif" runat="server"><IMG height="1" src="spacer.gif" width="20">Detection 
									Value:
									<asp:TextBox id="txtFolderValue" runat="server"></asp:TextBox>&nbsp;
									<asp:imagebutton id="btnAddAppFolder" runat="server" ImageUrl="Images/add_item.gif"></asp:imagebutton>
									<BR>
									<asp:listbox id="lstAppFolder" runat="server" Font-Names="Verdana" Font-Size="8.25pt" Height="200px"
										Width="440px"></asp:listbox><BR>
									Attribute Capture Mode:
									<asp:RadioButton id="rdoReplace" runat="server" Text="Replace" CssClass="PageContent" Checked="True"
										GroupName="Attributes"></asp:RadioButton>&nbsp;
									<asp:RadioButton id="rdoAppend" runat="server" Text="Append" CssClass="PageContent" GroupName="Attributes"></asp:RadioButton><BR>
									<BR>
								</span>
								<asp:label id="lblError" runat="server" Width="100%" Height="16px" ForeColor="Red" Font-Size="8.25pt"
									Font-Names="Verdana"></asp:label></P>
						</TD>
						<TD vAlign="top" width="50%"><BR>
							<BR>
							&nbsp;
							<asp:imagebutton id="btnMoveUp" runat="server" ImageUrl="Images/move_up.gif"></asp:imagebutton><BR>
							<BR>
							&nbsp;
							<asp:imagebutton id="btnRemove" runat="server" ImageUrl="Images/remove_item.gif"></asp:imagebutton><BR>
							<BR>
							&nbsp;
							<asp:imagebutton id="btnMoveDown" runat="server" ImageUrl="Images/move_down.gif"></asp:imagebutton><BR>
							<BR>
							<BR>
							<BR>
							<BR>
							<BR>
							<BR>
							<BR>
							<BR>
							<BR>
							&nbsp;
							<asp:imagebutton id="btnRemoveFolderMapping" runat="server" ImageUrl="Images/remove_item.gif"></asp:imagebutton><BR>
							<BR>
							<BR>
						</TD>
					</TR>
					<TR height="15" width="100%">
						<TD style="WIDTH: 395px" width="395">
							<input id="FolderId" type="hidden" name="FolderId" runat="server"></TD>
						<TD style="WIDTH: 72px"><BR>
							<P>&nbsp;</P>
						</TD>
						<TD align="right"><asp:imagebutton id="btnCancel" runat="server" ImageUrl="Images/btn_canceladmin.gif"></asp:imagebutton>&nbsp;<asp:imagebutton id="btnNext" runat="server" ImageUrl="Images/btn_finish.gif"></asp:imagebutton></TD>
						<TD width="50"></TD>
					</TR>
					<TR height="100%">
						<TD></TD>
					</TR>
				</tr></TABLE>
			</TD>
			<TD class="RightContent" height="100%">&nbsp;</TD>
			</TR>
			<TR>
				<TD class="PageFooter" vAlign="bottom" bgColor="#cccccc" colSpan="3"><uc1:topright id="docFooter" runat="server"></uc1:topright></TD>
			</TR>
			<TR height="1">
				<TD style="BACKGROUND-COLOR: #959394" width="20"><IMG height="1" src="images/spacer.gif" width="20"></TD>
				<TD style="BACKGROUND-COLOR: #959394" width="100%"><IMG height="1" src="images/spacer.gif" width="100%"></TD>
				<TD style="BACKGROUND-COLOR: #959394" width="20"><IMG height="1" src="images/spacer.gif" width="20"></TD>
			</TR>
			</TBODY></TABLE>&nbsp;</form>
		<script language="javascript" src="scripts/tooltip_wrap.js"></script>
		<script language="javascript">

				function FolderLookup()
				{
					var val = window.showModalDialog('Tree.aspx?','FolderLookup','resizable:no;status:no;dialogHeight:625px;dialogWidth:250px;scroll:no');
					if (val != undefined)
					{
						varArray = val.split('�');
						document.all.FolderId.value = varArray[0].replace('W','');
						document.all.txtFolder.value = varArray[1];
					}
				}
				
				function ClearFolder()
				{
					document.getElementById('FolderId').value = '';
					document.getElementById('txtFolder').value = '';
				}

				function catchSubmit()
				{
					if (event.keyCode == 13)
					{
						event.cancelBubble = true;
						event.returnValue = false;
						return false;
					}
				}
				
		</script>
	</body>
</HTML>

Imports Accucentric.docAssist.Data.Images

Partial Class WorkflowQueuesEdit
    Inherits System.Web.UI.Page

    'Inherits System.Web.UI.Page

    Private intQueueId As Integer
    Private intMode As Integer
    Private intLoadSequence As Integer = 0


    Private pageTitle As New System.Web.UI.HtmlControls.HtmlGenericControl

    Private mobjUser As Accucentric.docAssist.Web.Security.User

    Private dsPaths As New DataSet
    Private dtSelectedAction As New DataTable
    Private dtActions As New DataTable
    Private dtQueuesAvailable As New DataTable
    Private dtQueuesAssisgned As New DataTable
    Protected WithEvents Label4 As System.Web.UI.WebControls.Label
    Private dtNumAttributes As New DataTable

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim conSql As New SqlClient.SqlConnection

        'RegisterWaitDisabled(ddlGroup)

        txtQueue.Attributes("onkeypress") = "submitForm();"
        txtQueueCode.Attributes("onkeypress") = "submitForm();"
        txtDescription.Attributes("onkeypress") = "submitForm();"

        Try
            intMode = CInt(Request.QueryString("Mode"))
            intQueueId = CInt(Request.QueryString("ID"))

            'Security
            Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
            Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))
            Me.mobjUser.AccountId = guidAccountId
        Catch ex As Exception
            Response.Redirect("Default.aspx")
        End Try

        If Not Me.mobjUser.SysAdmin Then
            Response.Redirect("Default.aspx")
        End If

        If Page.IsPostBack = False Then

            Try
                'Get numeric attributes
                conSql = Functions.BuildConnection(Me.mobjUser)
                If conSql.State <> ConnectionState.Open Then conSql.Open()
                Dim cmdSql As New SqlClient.SqlCommand("WFNumericAttributesByQueueID", conSql)
                cmdSql.CommandType = CommandType.StoredProcedure
                cmdSql.Parameters.Add("@QueueId", intQueueId)
                Dim da As New SqlClient.SqlDataAdapter(cmdSql)
                da.Fill(dtNumAttributes)

                ddlTotalAttribute.DataSource = dtNumAttributes
                ddlTotalAttribute.DataTextField = "AttributeName"
                ddlTotalAttribute.DataValueField = "AttributeId"
                ddlTotalAttribute.DataBind()
                ddlTotalAttribute.Items.Add(New ListItem("None", 0))
                ddlTotalAttribute.SelectedValue = 0

                Dim sqlConImages As SqlClient.SqlConnection
                sqlConImages = Functions.BuildConnection(Me.mobjUser)
                Dim objWf As New Workflow(sqlConImages, False)

                '
                ' Category
                '
                Dim dtCategory As New DataTable
                dtCategory = objWf.WFCategoryGet()
                ddlCategories.DataTextField = "CategoryName"
                ddlCategories.DataValueField = "CategoryId"
                ddlCategories.DataSource = dtCategory
                ddlCategories.DataBind()

                Dim lstitem As New ListItem
                lstitem.Text = ""
                lstitem.Value = 0
                lstitem.Selected = True
                ddlCategories.Items.Add(lstitem)


                If sqlConImages.State = ConnectionState.Open Then sqlConImages.Close()

            Catch ex As Exception

            Finally
                'ReturnScripts.Add(ddlTotalAttribute)
                If conSql.State <> ConnectionState.Closed Then conSql.Close()
                conSql = Nothing
            End Try

            'Actions 
            Try

                Select Case intMode
                    Case 0 'Add mode
                        NewQueue()
                    Case 1
                        LoadQueue()
                    Case 2

                End Select

            Catch ex As Exception

            End Try
        End If

    End Sub



    Private Function NewQueue()

        Dim conSql As New SqlClient.SqlConnection

        Try
            conSql = Functions.BuildConnection(Me.mobjUser)
            If conSql.State <> ConnectionState.Open Then conSql.Open()

            Dim objWf As New Workflow(conSql, False)

            'Groups
            Dim dtGroups As New DataTable
            dtGroups = objWf.WFGroupsGet()
            ddlGroup.DataSource = dtGroups
            ddlGroup.DataValueField = "GroupId"
            ddlGroup.DataTextField = "GroupName"
            ddlGroup.DataBind()
            ddlGroup_SelectedIndexChanged(Nothing, Nothing)

            'Dim ddi As New ListItem
            'ddi.Value = -1
            'ddi.Text = "<None>"
            'ddi.Selected = True

            'ddlGroup.Items.Add(ddi)

            dsPaths = objWf.WFQueueActionsGet(0)

            Dim relAvailable As System.Data.DataRelation
            Dim relAssigned As System.Data.DataRelation
            relAvailable = New DataRelation("StatusAvaialble", New DataColumn() {dsPaths.Tables(1).Columns("StatusId")}, New DataColumn() {dsPaths.Tables(2).Columns("StatusId")}, False)
            relAssigned = New DataRelation("StatusAssigned", New DataColumn() {dsPaths.Tables(1).Columns("StatusId")}, New DataColumn() {dsPaths.Tables(3).Columns("StatusId")}, False)
            dsPaths.Relations.Add(relAvailable)
            dsPaths.Relations.Add(relAssigned)

            ddlActionType.DataValueField = "StatusId"
            ddlActionType.DataTextField = "StatusDesc"
            ddlActionType.DataSource = dsPaths.Tables(0)
            ddlActionType.DataBind()

            Dim dvPath As New DataView
            dvPath = dsPaths.Tables(1).DefaultView
            dgRoutingPath.DataSource = dvPath
            dgRoutingPath.DataBind()

            'ReturnScripts.Add(dgRoutingPath)

        Catch ex As Exception
            If conSql.State <> ConnectionState.Closed Then conSql.Close()
            conSql = Nothing
        Finally
            If conSql.State <> ConnectionState.Closed Then conSql.Close()
            conSql = Nothing
        End Try

    End Function

    Private Function LoadQueue()

        Dim conSql As New SqlClient.SqlConnection

        Try
            conSql = Functions.BuildConnection(Me.mobjUser)
            If conSql.State <> ConnectionState.Open Then conSql.Open()

            Dim objWf As New Workflow(conSql, False)

            Dim dtHeader As New DataTable
            dtHeader = objWf.WFQueueGetID(intQueueId)

            txtQueueCode.Text = dtHeader.Rows(0)("QueueCode")
            txtDescription.Text = dtHeader.Rows(0)("QueueNoteDetail")
            txtInstructions.Text = dtHeader.Rows(0)("QueueInstruction")
            txtQueue.Text = dtHeader.Rows(0)("QueueName")
            ddlTotalAttribute.SelectedValue = dtHeader.Rows(0)("TotalAttributeID")
            ddlCategories.SelectedValue = dtHeader.Rows(0)("CategoryID")

            'Groups
            Dim dtGroups As New DataTable
            dtGroups = objWf.WFGroupsGet()
            ddlGroup.DataSource = dtGroups
            ddlGroup.DataValueField = "GroupId"
            ddlGroup.DataTextField = "GroupName"
            ddlGroup.DataBind()
            ddlGroup.SelectedValue = dtHeader.Rows(0)("GroupId")
            ddlGroup_SelectedIndexChanged(Nothing, Nothing)

            'Load users
            Dim dtAccess As New DataTable
            dtAccess = objWf.WFQueueAccessGetID(intQueueId, ddlGroup.SelectedValue)
            dgUserPermissions.DataSource = dtAccess
            dgUserPermissions.DataBind()

            dsPaths = objWf.WFQueueActionsGet(intQueueId)

            Dim relAvailable As System.Data.DataRelation
            Dim relAssigned As System.Data.DataRelation
            relAvailable = New DataRelation("StatusAvaialble", New DataColumn() {dsPaths.Tables(1).Columns("StatusId")}, New DataColumn() {dsPaths.Tables(2).Columns("StatusId")}, False)
            relAssigned = New DataRelation("StatusAssigned", New DataColumn() {dsPaths.Tables(1).Columns("StatusId")}, New DataColumn() {dsPaths.Tables(3).Columns("StatusId")}, False)
            dsPaths.Relations.Add(relAvailable)
            dsPaths.Relations.Add(relAssigned)

            ddlActionType.DataValueField = "StatusId"
            ddlActionType.DataTextField = "StatusDesc"
            ddlActionType.DataSource = dsPaths.Tables(0)
            ddlActionType.DataBind()

            Dim dvPath As New DataView
            dvPath = dsPaths.Tables(1).DefaultView
            dgRoutingPath.DataSource = dvPath
            dgRoutingPath.DataBind()

            'ReturnScripts.Add(dgRoutingPath)

        Catch ex As Exception
            If conSql.State <> ConnectionState.Closed Then conSql.Close()
            conSql = Nothing
        Finally
            If conSql.State <> ConnectionState.Closed Then conSql.Close()
            conSql = Nothing
        End Try

    End Function

    Private Function IsEmpty(ByVal strText As String) As Boolean
        Return strText.Trim.Length = 0
    End Function

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click

        Dim conSqlNotifications As New SqlClient.SqlConnection

        Dim blnExit As Boolean = False

        Try
            If txtQueue.Text.Length = 0 Then
                lblError.Text = "Queue Name is required."
                lblError.Visible = True
                'ReturnScripts.Add(lblError)
                Exit Sub
            End If

            If txtQueueCode.Text.Length = 0 Then
                lblError.Text = "Queue Code is required."
                lblError.Visible = True
                'ReturnScripts.Add(lblError)
                Exit Sub
            End If

            If txtDescription.Text.Length = 0 Then
                lblError.Text = "Description is required."
                lblError.Visible = True
                'ReturnScripts.Add(lblError)
                Exit Sub
            End If
        Catch ex As Exception

        End Try


        Dim sqlTran As SqlClient.SqlTransaction
        Dim sqlConImages As New SqlClient.SqlConnection

        Try

            sqlConImages = Functions.BuildConnection(Me.mobjUser)

            If sqlConImages.State <> ConnectionState.Open Then sqlConImages.Open()

            sqlTran = sqlConImages.BeginTransaction

            Dim wfQueue As New Accucentric.docAssist.Data.Images.Workflow(sqlConImages, False, sqlTran)

            'Check that any forward action has at least one queue assigned
            For Each dgRowItem As DataGridItem In dgRoutingPath.Items

                Dim lblAction As Label = CType(dgRowItem.FindControl("lblDisplayType"), Label)

                If Not lblAction Is Nothing Then

                    Dim lstAssignedCheck As ListBox = CType(dgRowItem.FindControl("lstAssigned"), ListBox)

                    If lblAction.Text = "Forward" Then
                        If lstAssignedCheck.Items.Count = 0 Then
                            lblError.Text = "You must assign a queue to forward actions."
                            lblError.Visible = True
                            'ReturnScripts.Add(lblError)
                            blnExit = True
                            Exit Sub
                        End If
                    End If

                End If
            Next

            Select Case intMode
                Case 0 'Add

                    'Header
                    Dim intQueueId As Integer = wfQueue.WFQueueInsert(txtQueueCode.Text, txtQueue.Text, txtInstructions.Text, txtDescription.Text, ddlGroup.SelectedValue, 0, ddlTotalAttribute.SelectedValue, ddlCategories.SelectedValue)

                    If intQueueId = 0 Then
                        lblError.Text = "Queue Code already exists."
                        lblError.Visible = True
                        'ReturnScripts.Add(lblError)
                        Exit Sub
                    End If

                    'User permissions
                    For Each rowItem As DataGridItem In dgUserPermissions.Items
                        Dim lblUserId As Label = CType(rowItem.FindControl("lblUserId"), Label)

                        Dim rdoNone As RadioButton = CType(rowItem.FindControl("rdoNone"), RadioButton)
                        Dim rdoView As RadioButton = CType(rowItem.FindControl("rdoView"), RadioButton)
                        Dim rdoReview As RadioButton = CType(rowItem.FindControl("rdoReview"), RadioButton)

                        Dim intAccess As Integer = 0

                        If rdoNone.Checked Then intAccess = 0
                        If rdoView.Checked Then intAccess = 1
                        If rdoReview.Checked Then intAccess = 2

                        wfQueue.WFQueueAccessInsert(intQueueId, CInt(lblUserId.Text), intAccess)

                    Next

                    'Routing paths
                    For Each dgRowItem As DataGridItem In dgRoutingPath.Items

                        Dim txtFolderId As TextBox = CType(dgRowItem.FindControl("txtFolder"), TextBox)
                        Dim FolderId As System.Web.UI.HtmlControls.HtmlInputHidden = CType(dgRowItem.FindControl("FolderId"), System.Web.UI.HtmlControls.HtmlInputHidden)
                        Dim rdoLeave As RadioButton = CType(dgRowItem.FindControl("rdoLeave"), RadioButton)
                        Dim rdoClear As RadioButton = CType(dgRowItem.FindControl("rdoClear"), RadioButton)
                        Dim rdoMove As RadioButton = CType(dgRowItem.FindControl("rdoMove"), RadioButton)

                        Dim MovefolderId As Integer
                        If rdoLeave.Checked Then
                            MovefolderId = -1
                        ElseIf rdoClear.Checked Then
                            MovefolderId = 0
                        ElseIf rdoMove.Checked Then
                            MovefolderId = FolderId.Value
                        End If

                        Dim ddlBackAction As DropDownList = CType(dgRowItem.FindControl("ddlBackAction"), DropDownList)

                        'Action
                        Dim ddlAction As DropDownList = CType(dgRowItem.FindControl("ddlAction"), DropDownList)
                        Dim intStatusId As Integer = ddlAction.SelectedValue

                        Dim chkTotal As CheckBox = CType(dgRowItem.FindControl("chkTotal"), CheckBox)
                        'Dim ddlAttributes As DropDownList = CType(dgRowItem.FindControl("ddlAttributes"), DropDownList)
                        Dim blnTotalEnabled As Boolean = False
                        If chkTotal.Checked Then blnTotalEnabled = True

                        Dim intQueueActionId As Integer = wfQueue.WFQueueActionInsert(intQueueId, intStatusId, dgRowItem.ItemIndex + 1, ddlBackAction.SelectedValue, blnTotalEnabled, MovefolderId)

                        'Assigned Queues
                        Dim lstAssigned As ListBox = CType(dgRowItem.FindControl("lstAssigned"), ListBox)
                        For Each lstItem As ListItem In lstAssigned.Items
                            Dim intAssignedQueueId As Integer = lstItem.Value
                            Dim intSeq As Integer = 1
                            wfQueue.WFQueueActionDetailInsert(intQueueActionId, intAssignedQueueId, intSeq)
                            intSeq += 1
                        Next

                    Next

                    'litQueue.Text = "<script language='javascript'>opener.location.reload();window.close();</script>"
                    'System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.NewGuid.ToString, "reloadData();", True)

                Case 1 'Edit

                    'Header
                    wfQueue.WFQueueUpdate(intQueueId, txtQueueCode.Text, txtQueue.Text, txtInstructions.Text, txtDescription.Text, ddlGroup.SelectedValue, 0, ddlTotalAttribute.SelectedValue, ddlCategories.SelectedValue)

                    If intQueueId = 0 Then
                        lblError.Text = "Queue Code already exists."
                        lblError.Visible = True
                        'ReturnScripts.Add(lblError)
                        Exit Sub
                    End If


                    'User permissions

                    'Remove all users
                    wfQueue.WFQueueAccessInsert(intQueueId, 0, 0)

                    For Each rowItem As DataGridItem In dgUserPermissions.Items
                        Dim lblUserId As Label = CType(rowItem.FindControl("lblUserId"), Label)
                        Dim rdoNone As RadioButton = CType(rowItem.FindControl("rdoNone"), RadioButton)
                        Dim rdoView As RadioButton = CType(rowItem.FindControl("rdoView"), RadioButton)
                        Dim rdoReview As RadioButton = CType(rowItem.FindControl("rdoReview"), RadioButton)

                        Dim intAccess As Integer = 0

                        If rdoNone.Checked Then intAccess = 0
                        If rdoView.Checked Then intAccess = 1
                        If rdoReview.Checked Then intAccess = 2

                        wfQueue.WFQueueAccessInsert(intQueueId, CInt(lblUserId.Text), intAccess)

                    Next

                    'Routing paths

                    'Clear actions
                    wfQueue.WFQueueActionInsert(intQueueId, 0, 0, 0, False, 0)

                    For Each dgRowItem As DataGridItem In dgRoutingPath.Items

                        Dim txtFolderId As TextBox = CType(dgRowItem.FindControl("txtFolder"), TextBox)
                        Dim FolderId As System.Web.UI.HtmlControls.HtmlInputHidden = CType(dgRowItem.FindControl("FolderId"), System.Web.UI.HtmlControls.HtmlInputHidden)
                        Dim rdoLeave As RadioButton = CType(dgRowItem.FindControl("rdoLeave"), RadioButton)
                        Dim rdoClear As RadioButton = CType(dgRowItem.FindControl("rdoClear"), RadioButton)
                        Dim rdoMove As RadioButton = CType(dgRowItem.FindControl("rdoMove"), RadioButton)

                        Dim MovefolderId As Integer
                        If rdoLeave.Checked Then
                            MovefolderId = -1
                        ElseIf rdoClear.Checked Then
                            MovefolderId = 0
                        ElseIf rdoMove.Checked Then
                            MovefolderId = FolderId.Value
                        End If

                        'Action
                        Dim ddlAction As DropDownList = CType(dgRowItem.FindControl("ddlAction"), DropDownList)
                        Dim intStatusId As Integer = ddlAction.SelectedValue

                        Dim ddlBackAction As DropDownList = CType(dgRowItem.FindControl("ddlBackAction"), DropDownList)

                        Dim chkTotal As CheckBox = CType(dgRowItem.FindControl("chkTotal"), CheckBox)
                        Dim ddlAttributesTotal As DropDownList = CType(dgRowItem.FindControl("ddlAttributes"), DropDownList)
                        Dim totalEnabled As Boolean = False
                        If chkTotal.Checked Then totalEnabled = True

                        Dim intQueueActionId As Integer = wfQueue.WFQueueActionInsert(intQueueId, intStatusId, dgRowItem.ItemIndex + 1, ddlBackAction.SelectedValue, totalEnabled, MovefolderId)

                        'Assigned Queues
                        Dim lstAssigned As ListBox = CType(dgRowItem.FindControl("lstAssigned"), ListBox)
                        For Each lstItem As ListItem In lstAssigned.Items
                            Dim intAssignedQueueId As Integer = lstItem.Value
                            Dim intSeq As Integer = 1
                            wfQueue.WFQueueActionDetailInsert(intQueueActionId, intAssignedQueueId, intSeq)
                            intSeq += 1
                        Next

                    Next

                    'System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.NewGuid.ToString, "reloadData();", True)

                    'Notifications
                    conSqlNotifications = Functions.BuildConnection(Functions.ConnectionType.Notifications_Workflow)
                    If conSqlNotifications.State <> ConnectionState.Open Then conSqlNotifications.Open()

                    Dim strTableName As String = System.Guid.NewGuid.ToString.Replace("-", "")

                    Dim sbCmd As New System.Text.StringBuilder
                    sbCmd.Append("CREATE TABLE ##" & strTableName & " (WebUserID varchar(36) NULL)" & vbCr)

                    For Each dgi As DataGridItem In dgUserPermissions.Items

                        Dim lblUserId As Label = CType(dgi.FindControl("lblUserId"), Label)

                        Dim rdoNone As RadioButton = CType(dgi.FindControl("rdoNone"), RadioButton)
                        Dim rdoView As RadioButton = CType(dgi.FindControl("rdoView"), RadioButton)
                        Dim rdoReview As RadioButton = CType(dgi.FindControl("rdoReview"), RadioButton)
                        Dim lblWebUserId As Label = CType(dgi.FindControl("lblWebUserId"), Label)

                        If rdoView.Checked Or rdoReview.Checked Then
                            sbCmd.Append("INSERT INTO ##" & strTableName & " (WebUserID) VALUES ('" & lblWebUserId.Text & "')" & vbCrLf)
                        End If

                    Next

                    Dim sqlCmd As New SqlClient.SqlCommand(sbCmd.ToString, conSqlNotifications)
                    sqlCmd.CommandType = CommandType.Text
                    sqlCmd.ExecuteScalar()

                    Dim objWf As New Workflow(conSqlNotifications)
                    objWf.WFSubscriberQueueReconcile(intQueueId, "##" & strTableName, Me.mobjUser.AccountId.ToString)

                Case 2

            End Select

            If blnExit = False Then
                sqlTran.Commit()
            Else
                sqlTran.Rollback()
            End If

            System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, "Close", "opener.location.reload();window.close();", True)

        Catch ex As Exception

            sqlTran.Rollback()

            lblError.Text = "An error occured while saving this queue. If the problem continues please contact support."
            lblError.Visible = True
            'ReturnScripts.Add(lblError)

        Finally

            If conSqlNotifications.State <> ConnectionState.Closed Then
                conSqlNotifications.Close()
                conSqlNotifications.Dispose()
            End If

            If sqlConImages.State <> ConnectionState.Closed Then
                sqlConImages.Close()
                sqlConImages.Dispose()
            End If

        End Try

    End Sub

    Private Sub ddlGroup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlGroup.SelectedIndexChanged

        If ddlGroup.SelectedValue = -1 Then
            dgUserPermissions.DataSource = Nothing
            dgUserPermissions.DataBind()
            'ReturnScripts.Add(dgUserPermissions)
            Exit Sub
        End If

        Dim conSql As New SqlClient.SqlConnection

        Try
            conSql = Functions.BuildConnection(Me.mobjUser)
            If conSql.State <> ConnectionState.Open Then conSql.Open()

            Dim objWf As New Workflow(conSql, False)

            'Load user permissions
            Dim dtPerms As New DataTable
            dtPerms = objWf.WFQueueAccessGetID(0, ddlGroup.SelectedValue)

            dgUserPermissions.DataSource = dtPerms
            dgUserPermissions.DataBind()


        Catch ex As Exception
            If conSql.State <> ConnectionState.Closed Then conSql.Close()
            conSql = Nothing
        Finally
            If conSql.State <> ConnectionState.Closed Then conSql.Close()
            conSql = Nothing
            'ReturnScripts.Add(dgUserPermissions)
        End Try

    End Sub

    Private Function ReverseBind(Optional ByVal blnAddNewRow As Boolean = True) As DataSet

        Dim dsTmp As New DataSet
        Dim dr As DataRow

        Try

            Dim objWf As New Workflow(Functions.BuildConnection(Me.mobjUser))
            dsTmp = objWf.WFQueueActionsGet(-1)

            'Routing paths
            For Each dgRowItem As DataGridItem In dgRoutingPath.Items

                'Action
                Dim ddlAction As DropDownList = CType(dgRowItem.FindControl("ddlAction"), DropDownList)
                Dim strWorkflowAction As String = CType(dgRowItem.FindControl("lblDisplayType"), Label).Text
                Dim intStatusId As Integer = ddlAction.SelectedValue
                Dim intSeqId As Integer = dgRowItem.ItemIndex + 1

                dr = dsTmp.Tables(1).NewRow
                dr("QueueId") = intQueueId
                dr("StatusId") = intStatusId
                dr("SeqId") = intSeqId
                dr("WorkflowAction") = strWorkflowAction

                'Folder Actions
                Dim rdoLeave As RadioButton = dgRowItem.FindControl("rdoLeave")
                Dim rdoClear As RadioButton = dgRowItem.FindControl("rdoClear")
                Dim rdoMove As RadioButton = dgRowItem.FindControl("rdoMove")
                Dim txtFolder As TextBox = dgRowItem.FindControl("txtFolder")
                Dim FolderId As System.Web.UI.HtmlControls.HtmlInputHidden = dgRowItem.FindControl("FolderId")

                If rdoLeave.Checked Then
                    dr("FolderId") = -1
                ElseIf rdoClear.Checked Then
                    dr("FolderId") = 0
                Else
                    If FolderId.Value = "" Then
                        lblError.Text = "Please select a folder to move to."
                        'ReturnScripts.Add(lblError)
                        Exit Function
                    Else
                        dr("FolderId") = FolderId.Value
                        dr("FolderName") = txtFolder.Text
                    End If
                End If

                dsTmp.Tables(1).Rows.Add(dr)

                'Available Queues
                Dim lstAvailable As ListBox = CType(dgRowItem.FindControl("lstAvailable"), ListBox)
                For Each lstItem As ListItem In lstAvailable.Items
                    dr = dsTmp.Tables(2).NewRow
                    dr("StatusId") = intStatusId
                    dr("QueueId") = lstItem.Value
                    dr("QueueCode") = lstItem.Text
                    dsTmp.Tables(2).Rows.Add(dr)
                Next

                'Assigned Queues
                Dim lstAssigned As ListBox = CType(dgRowItem.FindControl("lstAssigned"), ListBox)
                For Each lstItem As ListItem In lstAssigned.Items
                    dr = dsTmp.Tables(3).NewRow
                    dr("StatusId") = intStatusId
                    dr("QueueId") = lstItem.Value
                    dr("QueueCode") = lstItem.Text
                    dsTmp.Tables(3).Rows.Add(dr)
                Next

            Next

            If blnAddNewRow = True Then
                'Add new row
                dr = dsTmp.Tables(1).NewRow
                dr("QueueId") = intQueueId
                dr("StatusId") = ddlActionType.SelectedValue

                Dim dvFilter As New DataView(dsTmp.Tables(0))
                dvFilter.RowFilter = "StatusId=" & ddlActionType.SelectedValue
                dr("WorkflowAction") = dvFilter(0)(3)

                Dim SeqId As Integer = dsTmp.Tables(1).Rows.Count + 1
                dr("SeqId") = SeqId
                dsTmp.Tables(1).Rows.Add(dr)

                Dim Wf As New Workflow(Functions.BuildConnection(Me.mobjUser))

                Dim dtTmp As New DataTable
                dtTmp = Wf.WFQueueGet

                For Each drRow As DataRow In dtTmp.Rows
                    dr = dsTmp.Tables(2).NewRow
                    dr("QueueId") = drRow("QueueId")
                    dr("StatusId") = ddlActionType.SelectedValue
                    dr("QueueCode") = drRow("QueueCode")
                    dr("QueueName") = drRow("QueueName")
                    dsTmp.Tables(2).Rows.Add(dr)
                Next
            End If


            Return dsTmp

        Catch ex As Exception

        End Try

    End Function

    Private Sub btnAddRoute_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)

        Try

            'Check if this action type already exist
            For Each dgRowItem As DataGridItem In dgRoutingPath.Items
                Dim intActionAddId As Integer = ddlActionType.SelectedValue
                Dim ddlAction As DropDownList = CType(dgRowItem.FindControl("ddlAction"), DropDownList)
                If ddlAction.SelectedValue = intActionAddId Then
                    Dim strMsg As String = "This action already exists. You can edit the action below."
                    'litQueue.Text = "<script language='javascript'>alert(" & strMsg & ");</script>"
                    System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.NewGuid.ToString, "promptUser('" & strMsg & "');", True)
                    Exit Sub
                End If
            Next

            'Dim ds As New DataSet
            dsPaths = ReverseBind()

            Dim relAvailable As System.Data.DataRelation
            Dim relAssigned As System.Data.DataRelation
            relAvailable = New DataRelation("StatusAvaialble", New DataColumn() {dsPaths.Tables(1).Columns("StatusId")}, New DataColumn() {dsPaths.Tables(2).Columns("StatusId")}, False)
            relAssigned = New DataRelation("StatusAssigned", New DataColumn() {dsPaths.Tables(1).Columns("StatusId")}, New DataColumn() {dsPaths.Tables(3).Columns("StatusId")}, False)
            dsPaths.Relations.Add(relAvailable)
            dsPaths.Relations.Add(relAssigned)

            Dim dvPath As New DataView
            dvPath = dsPaths.Tables(1).DefaultView
            dgRoutingPath.DataSource = dvPath
            dgRoutingPath.DataBind()

            'ReturnScripts.Add(dgRoutingPath)

        Catch ex As Exception

        End Try

    End Sub

    Protected Sub lstAvailable_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub dgRoutingPath_ItemCommand1(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgRoutingPath.ItemCommand
        Try



            Select Case e.CommandName

                Case "Add"

                    Try
                        Dim dgRowItem As DataGridItem = e.Item
                        Dim lstAvailable As ListBox = CType(dgRowItem.FindControl("lstAvailable"), ListBox)
                        Dim lstAssigned As ListBox = CType(dgRowItem.FindControl("lstAssigned"), ListBox)

                        If lstAvailable.SelectedIndex = -1 Then Exit Sub

                        Dim lstItem As New ListItem
                        lstItem = lstAvailable.SelectedItem

                        Dim dtAssisgnedQueues As New DataTable

                        Dim mdcUserId As New DataColumn
                        Dim mdcUserName As New DataColumn
                        mdcUserId = New DataColumn("QueueId", GetType(System.Int32), Nothing, MappingType.Element)
                        mdcUserName = New DataColumn("QueueCode", GetType(System.String), Nothing, MappingType.Element)
                        dtAssisgnedQueues.Columns.AddRange(New DataColumn() {mdcUserId, mdcUserName})

                        For x As Integer = 0 To lstAssigned.Items.Count - 1
                            Dim dr2 As DataRow
                            dr2 = dtAssisgnedQueues.NewRow
                            dr2("QueueId") = lstAssigned.Items(x).Value
                            dr2("QueueCode") = lstAssigned.Items(x).Text
                            dtAssisgnedQueues.Rows.Add(dr2)
                        Next

                        lstAvailable.Items.Remove(lstItem)

                        Dim dr As DataRow
                        dr = dtAssisgnedQueues.NewRow

                        dr("QueueId") = lstItem.Value
                        dr("QueueCode") = lstItem.Text

                        dtAssisgnedQueues.Rows.Add(dr)

                        lstAssigned.DataSource = dtAssisgnedQueues
                        lstAssigned.DataTextField = "QueueCode"
                        lstAssigned.DataValueField = "QueueId"
                        lstAssigned.DataBind()

                        'ReturnScripts.Add(lstAssigned)
                        'ReturnScripts.Add(lstAvailable)

                    Catch ex As Exception

                    End Try

                Case "Remove"

                    Try
                        Dim dgRowItem As DataGridItem = e.Item
                        Dim lstAvailable As ListBox = CType(dgRowItem.FindControl("lstAvailable"), ListBox)
                        Dim lstAssigned As ListBox = CType(dgRowItem.FindControl("lstAssigned"), ListBox)

                        If lstAssigned.SelectedIndex = -1 Then Exit Sub

                        Dim lstItem As New ListItem
                        lstItem = lstAssigned.SelectedItem

                        Dim dtAvailableQueues As New DataTable

                        Dim mdcUserId As New DataColumn
                        Dim mdcUserName As New DataColumn
                        mdcUserId = New DataColumn("QueueId", GetType(System.Int32), Nothing, MappingType.Element)
                        mdcUserName = New DataColumn("QueueCode", GetType(System.String), Nothing, MappingType.Element)
                        dtAvailableQueues.Columns.AddRange(New DataColumn() {mdcUserId, mdcUserName})

                        For x As Integer = 0 To lstAvailable.Items.Count - 1
                            Dim dr2 As DataRow
                            dr2 = dtAvailableQueues.NewRow
                            dr2("QueueId") = lstAvailable.Items(x).Value
                            dr2("QueueCode") = lstAvailable.Items(x).Text
                            dtAvailableQueues.Rows.Add(dr2)
                        Next

                        lstAssigned.Items.Remove(lstItem)

                        Dim dr As DataRow
                        dr = dtAvailableQueues.NewRow

                        dr("QueueId") = lstItem.Value
                        dr("QueueCode") = lstItem.Text

                        dtAvailableQueues.Rows.Add(dr)

                        lstAvailable.DataSource = dtAvailableQueues
                        lstAvailable.DataTextField = "QueueCode"
                        lstAvailable.DataValueField = "QueueId"
                        lstAvailable.DataBind()

                        'ReturnScripts.Add(lstAssigned)
                        'ReturnScripts.Add(lstAvailable)

                    Catch ex As Exception

                    End Try


            End Select

        Catch ex As Exception

        Finally

        End Try
    End Sub

    Private Sub dgRoutingPath_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgRoutingPath.ItemDataBound

        If dgRoutingPath.Items.Count = 0 Then
            dgRoutingPath.Visible = False
        Else
            dgRoutingPath.Visible = True
        End If

        Try

            Dim dgRowItem As DataGridItem = e.Item
            Dim ddlAction As DropDownList = CType(dgRowItem.FindControl("ddlAction"), DropDownList)
            Dim lstAvailable As ListBox = CType(dgRowItem.FindControl("lstAvailable"), ListBox)
            Dim lstAssigned As ListBox = CType(dgRowItem.FindControl("lstAssigned"), ListBox)
            Dim lblDisplay As Label = CType(dgRowItem.FindControl("lblDisplay"), Label)
            Dim lblDisplayType As Label = CType(dgRowItem.FindControl("lblDisplayType"), Label)
            Dim totalTable As HtmlTable = CType(dgRowItem.FindControl("totalTable"), HtmlTable)

            Dim txtFolderId As TextBox = CType(dgRowItem.FindControl("txtFolder"), TextBox)
            Dim FolderId As System.Web.UI.HtmlControls.HtmlInputHidden = CType(dgRowItem.FindControl("FolderId"), System.Web.UI.HtmlControls.HtmlInputHidden)
            Dim ChooseFolder As HtmlImage = CType(dgRowItem.FindControl("ChooseFolder"), HtmlImage)

            Dim rdoLeave As RadioButton = CType(dgRowItem.FindControl("rdoLeave"), RadioButton)
            Dim rdoClear As RadioButton = CType(dgRowItem.FindControl("rdoClear"), RadioButton)
            Dim rdoMove As RadioButton = CType(dgRowItem.FindControl("rdoMove"), RadioButton)

            If Not ddlAction Is Nothing Then

                If DataBinder.Eval(e.Item.DataItem, "FolderId") Is System.DBNull.Value Then
                    rdoLeave.Checked = True
                Else
                    Select Case DataBinder.Eval(e.Item.DataItem, "FolderId")
                        Case -1
                            rdoLeave.Checked = True
                            rdoClear.Checked = False
                            rdoMove.Checked = False
                        Case 0
                            rdoClear.Checked = True
                            rdoLeave.Checked = False
                            rdoMove.Checked = False
                        Case Else
                            rdoMove.Checked = True
                            rdoLeave.Checked = False
                            rdoClear.Checked = False
                            txtFolderId.Text = DataBinder.Eval(e.Item.DataItem, "FolderName")
                            FolderId.Value = DataBinder.Eval(e.Item.DataItem, "FolderId")
                    End Select
                End If
                'ReturnScripts.Add(rdoLeave)
                'ReturnScripts.Add(rdoClear)
                'ReturnScripts.Add(rdoMove)
                'ReturnScripts.Add(txtFolderId)
                'ReturnScripts.Add(FolderId)


                ChooseFolder.Attributes.Add("onclick", "FolderLookup('" & txtFolderId.ClientID & "','" & FolderId.ClientID & "');")

                totalTable.Visible = False

                ddlAction.DataTextField = "StatusDesc"
                ddlAction.DataValueField = "StatusId"
                ddlAction.SelectedValue = DataBinder.Eval(e.Item.DataItem, "StatusId")
                ddlAction.DataSource = dsPaths.Tables(0)
                ddlAction.DataBind()
                lblDisplay.Text = ddlAction.SelectedItem.Text
                lblDisplayType.Text = DataBinder.Eval(e.Item.DataItem, "WorkflowAction")

                Dim chkTotal As CheckBox = CType(dgRowItem.FindControl("chkTotal"), CheckBox)
                Dim lblTotal As Label = CType(dgRowItem.FindControl("lblTotal"), Label)

                If lblTotal.Text = "True" Then
                    chkTotal.Checked = True
                Else
                    chkTotal.Checked = False
                End If

                'ReturnScripts.Add(chkTotal)

            End If

            If Not lblDisplayType Is Nothing Then

                Select Case lblDisplayType.Text
                    Case "Forward"

                    Case "Back"
                        lstAssigned.Visible = False
                        lstAvailable.Visible = False
                        Dim btnAdd As ImageButton = CType(dgRowItem.FindControl("btnAdd"), ImageButton)
                        Dim btnRemove As ImageButton = CType(dgRowItem.FindControl("btnRemove"), ImageButton)
                        btnAdd.Visible = False
                        btnRemove.Visible = False

                        Dim tblListContainer As Table = CType(dgRowItem.FindControl("ListContainer"), Table)
                        Dim tblBackAction As Table = CType(dgRowItem.FindControl("tblBackAction"), Table)

                        If Not tblListContainer Is Nothing Then tblListContainer.Style("display") = "none"

                        Dim ddlBackAction As DropDownList = CType(dgRowItem.FindControl("ddlBackAction"), DropDownList)
                        ddlBackAction.Visible = True
                        ddlBackAction.SelectedValue = DataBinder.Eval(e.Item.DataItem, "BackAction")
                        'ReturnScripts.Add(dgRowItem)
                    Case "End"
                        lstAssigned.Visible = False
                        lstAvailable.Visible = False
                        Dim btnAdd As ImageButton = CType(dgRowItem.FindControl("btnAdd"), ImageButton)
                        Dim btnRemove As ImageButton = CType(dgRowItem.FindControl("btnRemove"), ImageButton)
                        btnAdd.Visible = False
                        btnRemove.Visible = False

                        Dim tblListContainer As Table = CType(dgRowItem.FindControl("ListContainer"), Table)
                        Dim tblBackAction As Table = CType(dgRowItem.FindControl("tblBackAction"), Table)

                        If Not tblListContainer Is Nothing Then tblListContainer.Style("display") = "none"

                        Dim lblComplete As Label = CType(dgRowItem.FindControl("lblComplete"), Label)
                        lblComplete.Visible = True
                        'ReturnScripts.Add(dgRowItem)
                    Case "Cancelled"
                        lstAssigned.Visible = False
                        lstAvailable.Visible = False
                        Dim btnAdd As ImageButton = CType(dgRowItem.FindControl("btnAdd"), ImageButton)
                        Dim btnRemove As ImageButton = CType(dgRowItem.FindControl("btnRemove"), ImageButton)
                        btnAdd.Visible = False
                        btnRemove.Visible = False

                        Dim tblListContainer As Table = CType(dgRowItem.FindControl("ListContainer"), Table)
                        Dim tblBackAction As Table = CType(dgRowItem.FindControl("tblBackAction"), Table)

                        If Not tblListContainer Is Nothing Then tblListContainer.Style("display") = "none"

                        Dim lblComplete As Label = CType(dgRowItem.FindControl("lblComplete"), Label)
                        lblComplete.Text = "Cancelled"
                        lblComplete.Visible = True

                End Select
            End If

            If Not lstAvailable Is Nothing Then
                lstAvailable.DataTextField = "QueueCode"
                lstAvailable.DataValueField = "QueueId"

                lstAssigned.DataTextField = "QueueCode"
                lstAssigned.DataValueField = "QueueId"

                Dim dr As DataRow
                For Each dr In dsPaths.Tables(1).Rows(e.Item.ItemIndex).GetChildRows("StatusAvaialble")
                    Dim lstItem As New ListItem
                    lstItem.Value = dr("QueueId")
                    lstItem.Text = dr("QueueCode")
                    lstItem.Selected = False
                    lstAvailable.Items.Add(lstItem)
                Next

                Dim dr2 As DataRow
                For Each dr2 In dsPaths.Tables(1).Rows(e.Item.ItemIndex).GetChildRows("StatusAssigned")
                    Dim lstItem As New ListItem
                    lstItem.Value = dr2("QueueId")
                    lstItem.Text = dr2("QueueCode")
                    lstItem.Selected = False
                    lstAssigned.Items.Add(lstItem)
                Next

                'ReturnScripts.Add(lstAvailable)

            End If

        Catch ex As Exception

        End Try

    End Sub

    Private Sub dgRoutingPath_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgRoutingPath.SelectedIndexChanged

    End Sub

    Private Sub lnkAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkAdd.Click

        Try

            'Check if this action type already exist
            For Each dgRowItem As DataGridItem In dgRoutingPath.Items
                Dim intActionAddId As Integer = ddlActionType.SelectedValue
                Dim ddlAction As DropDownList = CType(dgRowItem.FindControl("ddlAction"), DropDownList)
                If ddlAction.SelectedValue = intActionAddId Then
                    Dim strMsg As String = "This action already exists. You can edit the action below."
                    'litQueue.Text = "<script language='javascript'>alert('" & strMsg & "');</script>"
                    System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.NewGuid.ToString, "promptUser('" & strMsg & "');", True)
                    Exit Sub
                End If
            Next


            'Dim ds As New DataSet
            dsPaths = ReverseBind()

            Dim relAvailable As System.Data.DataRelation
            Dim relAssigned As System.Data.DataRelation
            relAvailable = New DataRelation("StatusAvaialble", New DataColumn() {dsPaths.Tables(1).Columns("StatusId")}, New DataColumn() {dsPaths.Tables(2).Columns("StatusId")}, False)
            relAssigned = New DataRelation("StatusAssigned", New DataColumn() {dsPaths.Tables(1).Columns("StatusId")}, New DataColumn() {dsPaths.Tables(3).Columns("StatusId")}, False)
            dsPaths.Relations.Add(relAvailable)
            dsPaths.Relations.Add(relAssigned)

            Dim dvPath As New DataView
            dvPath = dsPaths.Tables(1).DefaultView
            dgRoutingPath.DataSource = dvPath
            dgRoutingPath.DataBind()

            'ReturnScripts.Add(dgRoutingPath)

        Catch ex As Exception

        End Try

    End Sub

    Private Sub dgRoutingPath_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgRoutingPath.DeleteCommand

        Try

            Dim dgRowItem As DataGridItem = e.Item
            Dim intStatusId As Integer = CType(dgRowItem.FindControl("ddlAction"), DropDownList).SelectedValue

            dsPaths = ReverseBind(False)

            Dim relAvailable As System.Data.DataRelation
            Dim relAssigned As System.Data.DataRelation
            relAvailable = New DataRelation("StatusAvaialble", New DataColumn() {dsPaths.Tables(1).Columns("StatusId")}, New DataColumn() {dsPaths.Tables(2).Columns("StatusId")}, False)
            relAssigned = New DataRelation("StatusAssigned", New DataColumn() {dsPaths.Tables(1).Columns("StatusId")}, New DataColumn() {dsPaths.Tables(3).Columns("StatusId")}, False)
            dsPaths.Relations.Add(relAvailable)
            dsPaths.Relations.Add(relAssigned)

            dsPaths.Tables(1).Rows(dgRowItem.ItemIndex).Delete()

            Dim dvPath As New DataView
            dvPath = dsPaths.Tables(1).DefaultView
            dgRoutingPath.DataSource = dvPath
            dgRoutingPath.DataBind()

            'ReturnScripts.Add(dgRoutingPath)

        Catch ex As Exception

        End Try

    End Sub

    Private Sub dgUserPermissions_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgUserPermissions.ItemDataBound

        Try
            Dim lblAccess As Label = CType(e.Item.FindControl("lblAccess"), Label)

            Dim rdoNone As RadioButton = CType(e.Item.FindControl("rdoNone"), RadioButton)
            Dim rdoView As RadioButton = CType(e.Item.FindControl("rdoView"), RadioButton)
            Dim rdoReview As RadioButton = CType(e.Item.FindControl("rdoReview"), RadioButton)

            If Not lblAccess Is Nothing Then
                Select Case lblAccess.Text
                    Case "0"
                        rdoNone.Checked = True
                    Case "1"
                        rdoView.Checked = True
                    Case "2"
                        rdoReview.Checked = True
                    Case "3"
                        rdoNone.Checked = True
                End Select
            End If

        Catch ex As Exception

        End Try

    End Sub

End Class
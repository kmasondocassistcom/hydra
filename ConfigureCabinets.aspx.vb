Imports Accucentric.docAssist

Partial Class _Cabinets
    Inherits System.Web.UI.Page

    Private mobjUser As Web.Security.User
    Private mconSqlImage As SqlClient.SqlConnection

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        '
        ' 
        '
        Try
            If Request.Cookies("docAssist") Is Nothing Then
                Response.Redirect("Default.aspx")
                Exit Sub
            End If

            Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
            Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))

            Me.mobjUser.AccountId = guidAccountId
            Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)

        Catch ex As Exception
            Response.Redirect("default.aspx")
            Response.Flush()
            Response.End()
        End Try
        '
        ' Add object events
        '
        btnSave.Attributes("OnClick") = "SelectAllAssigned();"
        lstAssigned.Attributes("OnChange") = "CabinetSelected(this);"
        '
        ' Load up the objects
        '
        If Not Page.IsPostBack Then
            Try
                hostname.Value = Request.Url.AbsoluteUri.Substring(0, Request.Url.AbsoluteUri.Length - Request.Url.AbsolutePath.Length)
#If DEBUG Then
                hostname.Value = hostname.Value & "/docAssistWeb1"
#End If
                If Not Session("Attributes") Is Nothing Then Session("Attributes") = Nothing
                If Not Session("Documents") Is Nothing Then Session("Documents") = Nothing
                If Not Session("CabinetFolders") Is Nothing Then Session("CabinetFolders") = Nothing
                If Not Session("Cabinets") Is Nothing Then Session("Cabinets") = Nothing

                Dim dsFolders As dsConfiguration = GetConfiguration()
                lstAssigned.DataSource = dsFolders.Cabinets
                lstAssigned.DataTextField = "CabinetName"
                lstAssigned.DataValueField = "CabinetId"
                lstAssigned.DataBind()
                Session("Cabinets") = dsFolders

            Catch ex As Exception
                Throw ex
            Finally
                If Not Me.mconSqlImage.State = ConnectionState.Closed Then Me.mconSqlImage.Close()
            End Try
        Else
            If Session("Cabinets") Is Nothing Then Throw New Exception("Page was not referenced correctly, please refresh")
        End If


    End Sub

    Private Function GetConfiguration() As dsConfiguration

        Dim ds As New dsConfiguration
        Dim cmdsqlCabinets As New SqlClient.SqlCommand("SELECT * FROM Cabinets ORDER BY CabinetName", Me.mconSqlImage)

        cmdsqlCabinets.CommandTimeout = DEFAULT_COMMAND_TIMEOUT

        Dim da As New SqlClient.SqlDataAdapter

        If Not Me.mconSqlImage.State = ConnectionState.Open Then Me.mconSqlImage.Open()
        da.SelectCommand = cmdsqlCabinets
        da.Fill(ds.Cabinets)

        If Not Me.mconSqlImage.State = ConnectionState.Closed Then Me.mconSqlImage.Close()

        Return ds

    End Function

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click

        Dim dsConfig As dsConfiguration = CType(Session("Cabinets"), dsConfiguration)
        '
        ' Update cabinets
        '
        Dim strItems() As String = hiddenCabinets.Value.Split(",")
        UpdateCabinets(strItems, dsConfig)
        '
        ' Save the changes back to the server
        '
        Dim trnSql As SqlClient.SqlTransaction
        Try
            Me.mconSqlImage.Open()
            trnSql = Me.mconSqlImage.BeginTransaction

            Data.Images.UpdateCabinets(dsConfig.Cabinets, Me.mconSqlImage, trnSql)

            trnSql.Commit()

            showMessage.InnerHtml = "Cabinets were saved succesfully."
            txtCabinetName.Text = ""
            lstAssigned.DataSource = dsConfig.Cabinets
            lstAssigned.DataBind()
        Catch ex As Exception
            trnSql.Rollback()
#If DEBUG Then
            showMessage.InnerHtml = ex.Message & "<BR>" & ex.StackTrace
#Else
            showMessage.InnerHtml = "An error occured during the save.  Your changes have not been saved."
#End If

        Finally
            trnSql.Dispose()
            Session("CabinetFolders") = dsConfig
            If Not Me.mconSqlImage.State = ConnectionState.Closed Then Me.mconSqlImage.Close()
        End Try


    End Sub

    Private Function UpdateCabinets(ByVal strItems() As String, ByRef dsConfig As dsConfiguration) As Boolean

        If strItems.Length > 1 Or (strItems(0).Trim.Length > 0) Then
            If dsConfig.Cabinets.PrimaryKey.Length = 0 Then dsConfig.Cabinets.PrimaryKey = New DataColumn() {dsConfig.Cabinets.Columns("CabinetId")}
            Dim dr As DataRow
            For Each strItem As String In strItems
                Dim strValues() As String = strItem.Split(";")
                Dim drCabinet As dsConfiguration.CabinetsRow
                Dim intFolderId = CInt(strValues(0))
                If intFolderId < 0 Then
                    '
                    ' new folder
                    drCabinet = dsConfig.Cabinets.NewRow
                    drCabinet.CabinetName = strValues(1)
                    dsConfig.Cabinets.Rows.Add(drCabinet)
                Else
                    '
                    ' updated document
                    drCabinet = dsConfig.Cabinets.Rows.Find(strValues(0))
                    If Not drCabinet Is Nothing Then
                        If Not drCabinet.RowState = DataRowState.Deleted Then
                            If Not drCabinet.CabinetName = strValues(1) Then drCabinet.CabinetName = strValues(1)
                        End If
                    End If
                End If
            Next
        End If
        Return True
    End Function
End Class

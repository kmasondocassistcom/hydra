Partial Class ManageDocumentsAttributesRequired
    Inherits System.Web.UI.Page



    Private mobjUser As Accucentric.docAssist.Web.Security.User
    Private mconSqlImage As SqlClient.SqlConnection


#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents btnFinish As System.Web.UI.WebControls.ImageButton
    Protected WithEvents btnCancel As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lblError As System.Web.UI.WebControls.Label
    Protected WithEvents lstAttributes As MetaBuilders.WebControls.CheckedListBox
    Protected WithEvents Label1 As System.Web.UI.WebControls.Label
    Protected WithEvents txtDocumentname As System.Web.UI.WebControls.TextBox
    Protected WithEvents Label4 As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Dim dtAttributesSelected As DataTable

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
        Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))

        Dim consql As New SqlClient.SqlConnection

        Try

            If Not Page.IsPostBack = True Then
                txtDocName.Text = Session("mAdminDocumentName")
                dtAttributesSelected = Session("SelectedAttributes")
                lstAtt.DataTextField = "AttributeName"
                lstAtt.DataValueField = "AttributeID"
                lstAtt.DataSource = dtAttributesSelected
                lstAtt.DataBind()

                consql = Functions.BuildConnection(Me.mobjUser)

                For Each dr As DataRow In dtAttributesSelected.Rows

                    Dim cmdSql As New SqlClient.SqlCommand("acsAdminAttributeListValidate")
                    cmdSql.Connection = consql
                    cmdSql.CommandType = CommandType.StoredProcedure
                    cmdSql.Parameters.Add("@DocumentId", Session("mAdminDocumentID"))
                    cmdSql.Parameters.Add("@AttributeId", dr("AttributeId"))
                    Dim da As New SqlClient.SqlDataAdapter(cmdSql)
                    Dim dt As New DataTable
                    da.Fill(dt)

                    If dt.Rows(0)("AttributeDataType") = "ListView" Then

                        Dim listItem As New ListItem(dr("AttributeName"))
                        listItem.Value = dr("AttributeId")

                        If dt.Rows(0)("ListValidate") = True Then
                            listItem.Selected = True
                        End If

                        lstValidate.Items.Add(listItem)

                        If dt.Rows(0)("ListViewByCabinet") = True Then
                            listItem.Selected = True
                        Else
                            listItem.Selected = False
                        End If

                        lstFilter.Items.Add(listItem)

                    End If

                Next

            End If

        Catch ex As Exception

        Finally

            If consql.State <> ConnectionState.Closed Then
                consql.Close()
                consql.Dispose()
            End If

        End Try

    End Sub

    Private Function DocumentExists(ByVal strDocumentName As String) As Boolean
        Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
        Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))

        Me.mobjUser.AccountId = guidAccountId
        Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
        Me.mconSqlImage.Open()

        Dim cmdSql As SqlClient.SqlCommand

        Dim dtsql As New DataTable
        cmdSql = New SqlClient.SqlCommand("acsAdminDocumentCheck", Me.mconSqlImage)
        cmdSql.CommandType = CommandType.StoredProcedure
        cmdSql.Parameters.Add("@DocumentName", strDocumentName)
        Dim dasql As New SqlClient.SqlDataAdapter(cmdSql)
        dasql.Fill(dtsql)


        If dtsql.Rows.Count > 0 Then
            Return True
        Else
            Return False

        End If
    End Function


    Private Sub Imagebutton1_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Imagebutton1.Click

        If DocumentExists(txtDocName.Text) Then
            lblErr.Text = "Document " & txtDocName.Text & " already exists."
            Exit Sub
        Else
            SaveDocument()
            Session("blnAdminOnlyMode") = False
            Response.Redirect("ManageDocuments.aspx")
        End If

    End Sub


    Private Function SaveDocument() As Boolean
        Dim iNewDocumentID As Integer
        Dim cmdSql As SqlClient.SqlCommand
        Dim dtsql As New DataTable
        cmdSql = New SqlClient.SqlCommand("acsAdminDocumentAdd", Me.mconSqlImage)
        cmdSql.CommandType = CommandType.StoredProcedure
        cmdSql.Parameters.Add("@DocumentName", txtDocName.Text)
        cmdSql.Parameters.Add("@DocumentID", SqlDbType.Int)
        cmdSql.Parameters.Add("@VersionMode", Session("VersionMode"))
        cmdSql.Parameters("@DocumentID").Direction = ParameterDirection.Output
        cmdSql.ExecuteNonQuery()

        iNewDocumentID = cmdSql.Parameters("@DocumentID").Value


        If iNewDocumentID > 0 Then
            'SaveDocumentLocations(iNewDocumentID)
            SaveDocumentAttributes(iNewDocumentID)
            Return True
        Else
            Return False
            Exit Function
        End If


    End Function
    Private Sub SaveDocumentAttributes(ByVal iNewDocumentID As Integer)

        Try

            Dim cmdSql As SqlClient.SqlCommand

            'Clear any existing document attributes
            cmdSql = New SqlClient.SqlCommand("acsAdminDocumentAttributesClear", Me.mconSqlImage)
            cmdSql.CommandType = CommandType.StoredProcedure
            cmdSql.Parameters.Add("@DocumentID", iNewDocumentID)
            cmdSql.ExecuteNonQuery()

            Dim icount As Integer = lstAtt.Items.Count
            Dim x As Integer

            For x = 0 To icount - 1

                cmdSql = New SqlClient.SqlCommand("acsAdminDocumentAttributesAdd", Me.mconSqlImage)
                cmdSql.CommandType = CommandType.StoredProcedure
                cmdSql.Parameters.Add("@DocumentID", iNewDocumentID)
                cmdSql.Parameters.Add("@AttributeID", lstAtt.Items(x).Value)
                cmdSql.Parameters.Add("@AttributeDisplayOrder", x + 1)
                cmdSql.Parameters.Add("@Required", lstAtt.Items(x).Selected)

                For Each lstItem As ListItem In lstValidate.Items
                    If lstAtt.Items(x).Value = lstItem.Value And lstItem.Selected Then
                        cmdSql.Parameters.Add("@ListValidate", True)
                        Exit For
                    End If
                Next

                For Each lstFilterItem As ListItem In lstFilter.Items
                    If lstAtt.Items(x).Value = lstFilterItem.Value And lstFilterItem.Selected Then
                        cmdSql.Parameters.Add("@ListViewByCabinet", True)
                        Exit For
                    End If
                Next

                cmdSql.ExecuteNonQuery()

            Next

        Catch ex As Exception

        End Try

    End Sub
    Private Sub SaveDocumentLocations(ByVal iNewDocumentID As Integer)


        Dim cmdSql As SqlClient.SqlCommand
        Dim dr As DataRow

        'Clear any existing document locations
        cmdSql = New SqlClient.SqlCommand("acsAdminDocumentLocationsClear", Me.mconSqlImage)
        cmdSql.CommandType = CommandType.StoredProcedure
        cmdSql.Parameters.Add("@DocumentID", iNewDocumentID)
        cmdSql.ExecuteNonQuery()


        For Each dr In Session("FoldersSelected").rows

            cmdSql = New SqlClient.SqlCommand("acsAdminDocumentLocationsAdd", Me.mconSqlImage)
            cmdSql.CommandType = CommandType.StoredProcedure
            cmdSql.Parameters.Add("@FolderID", dr("FolderID"))
            cmdSql.Parameters.Add("@DocumentID", iNewDocumentID)
            cmdSql.ExecuteNonQuery()

        Next


    End Sub

    Private Sub Imagebutton1_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Imagebutton1.PreRender

    End Sub

    Private Sub Imagebutton2_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Imagebutton2.Click
        Response.Redirect("ManageDocuments.aspx")

    End Sub
End Class

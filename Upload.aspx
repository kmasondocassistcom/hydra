<%@ Register TagPrefix="uc1" TagName="WorkflowPane" Src="Controls/WorkflowPane.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="Upload.aspx.vb" Inherits="docAssistWeb.Upload"%>
<%@ Register TagPrefix="uc1" TagName="AttributesGrid" Src="Controls/AttributesGrid.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>docAssist - Add Attachments</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
		<base target="_self">
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form class="PageContent" id="Form1" method="post" runat="server">
			<TABLE id="Table1" cellSpacing="0" cellPadding="0" width="450" border="0"> <!-- Parent grey container -->
				<TR valign="top">
					<TD vAlign="top">
						<DIV class="Container">
							<DIV class="north">
								<DIV class="east">
									<DIV class="south">
										<DIV class="west">
											<DIV class="ne">
												<DIV class="se">
													<DIV class="sw">
														<DIV class="nw">
															<TABLE id="tblMain" cellSpacing="0" cellPadding="0" width="95%" border="0">
																<TR>
																	<TD noWrap align="left"><SPAN class="Heading">Add Attachments</SPAN>
																	</TD>
																</TR>
																<tr>
																	<td vAlign="top" align="left">
																		<DIV style="OVERFLOW: auto; HEIGHT: 100%">
																			<asp:datagrid id="UploadTemplate" runat="server" CssClass="PageContent" autogeneratecolumns="False"
																				ShowHeader="False">
																				<AlternatingItemStyle BackColor="#E0E0E0"></AlternatingItemStyle>
																				<Columns>
																					<asp:TemplateColumn HeaderText="Attributes">
																						<HeaderStyle Width="400px"></HeaderStyle>
																						<ItemStyle HorizontalAlign="Left" VerticalAlign="Top"></ItemStyle>
																						<ItemTemplate>
																							<TABLE id="Table2" cellSpacing="0" cellPadding="0" width="100%" border="0">
																								<TR>
																									<TD noWrap align="left"><SPAN class="Text">Title:&nbsp;</SPAN></TD>
																									<TD vAlign="top" noWrap align="right" width="100%">
																										<asp:Label id=lblTitle runat="server" CssClass="Text" Visible="True" Text='<%# DataBinder.Eval(Container, "DataItem.Title") %>'>
																										</asp:Label></TD>
																								</TR>
																								<TR>
																									<TD noWrap align="right"><SPAN class="Text">Description:&nbsp;</SPAN></TD>
																									<TD noWrap width="100%">
																										<asp:Label id=lblDescription runat="server" CssClass="Text" Visible="True" Text='<%# DataBinder.Eval(Container, "DataItem.Description") %>'>
																										</asp:Label></TD>
																								</TR>
																								<TR>
																									<TD noWrap align="right"><SPAN class="Text">Comments:&nbsp;</SPAN></TD>
																									<TD noWrap width="100%">
																										<asp:Label id=lblComments runat="server" CssClass="Text" Visible="True" Text='<%# DataBinder.Eval(Container, "DataItem.Comments") %>'>
																										</asp:Label></TD>
																								</TR>
																								<TR>
																									<TD noWrap align="right"><SPAN class="Text">Tags:&nbsp;</SPAN></TD>
																									<TD noWrap width="100%">
																										<asp:Label id=lblTags runat="server" CssClass="Text" Visible="True" Text='<%# DataBinder.Eval(Container, "DataItem.Tags") %>'>
																										</asp:Label></TD>
																								</TR>
																							</TABLE>
																							<uc1:AttributesGrid id="AttributesGrid" runat="server"></uc1:AttributesGrid>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:TemplateColumn HeaderText="Workflow">
																						<HeaderStyle Width="400px"></HeaderStyle>
																						<ItemTemplate>
																							<uc1:WorkflowPane id="WorkflowPane" runat="server"></uc1:WorkflowPane>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:TemplateColumn Visible="False" HeaderText="File">
																						<ItemTemplate>
																							<asp:Label id=lblFolderId runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.FolderId") %>'>
																							</asp:Label>
																							<asp:Label id=lblGuidName runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.GuidName") %>'>
																							</asp:Label>
																							<asp:Label id=lblFilename runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.Filename") %>'>
																							</asp:Label>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																				</Columns>
																			</asp:datagrid></DIV>
																	</td>
																</tr>
																<tr>
																	<td align="right">
																		<asp:Literal id="litDone" runat="server"></asp:Literal><asp:Literal id="litFolder" runat="server"></asp:Literal><asp:label id="lblError" runat="server" CssClass="ErrorText"></asp:label>&nbsp;<a href="javascript:parent.searchMode();"><img border="0" src="Images/smallred_cancel.gif"></a>&nbsp;
																		<asp:imagebutton id="btnSave" runat="server" ImageUrl="Images/smallred_save.gif"></asp:imagebutton></td>
																</tr>
															</TABLE>
														</DIV>
													</DIV>
												</DIV>
											</DIV>
										</DIV>
									</DIV>
								</DIV>
							</DIV>
						</DIV>
					</TD>
				</TR>
			</TABLE>
			<DIV></DIV>
			<DIV></DIV>
			<DIV></DIV>
			<DIV></DIV>
			<DIV></DIV>
			<DIV></DIV>
			<DIV></DIV>
			<DIV></DIV>
			<DIV></DIV>
			</TD></TR></TABLE></form>
		<script language="javascript" src="scripts/upload.js"></script>
	</body>
</HTML>

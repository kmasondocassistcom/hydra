﻿Public Partial Class LoadTime
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim start As DateTime = Now

        Dim versionID As Integer = Request.QueryString("ID")
        Dim page As Integer = Request.QueryString("Page")

        'Build new page
        Dim ws As New docAssistImages

        Dim mobjUser As Accucentric.docAssist.Web.Security.User
        mobjUser = BuildUserObject(Request.Cookies("docAssist"))

        Dim beforeWebSvc As DateTime = now

        Dim returns() As String = ws.BuildPageQuality(versionID, 1, 0, mobjUser.AccountId, 0, False)

        Dim afterWebSvc As DateTime = now


        Dim webSvcExecution As Double = Math.Round(afterWebSvc.Subtract(beforeWebSvc).TotalMilliseconds() / 1000, 2)

        
        WebAnnotationViewer1.ClearAnnotations()
        WebAnnotationViewer1.Open(returns(0))

        Dim done As DateTime = now
        Dim totalExecution As Double = Math.Round(done.Subtract(start).TotalMilliseconds() / 1000, 2)

        Results.Text = "Web service execution: " & webSvcExecution & "<br>Total execution: " & totalExecution
        'http://localhost/docAssistWeb1/LoadTime.aspx?ID=6535&Page=1

    End Sub

End Class
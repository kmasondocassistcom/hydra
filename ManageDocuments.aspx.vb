Partial Class ManageDocuments
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
    End Sub



    Private Sub lnkAddDocument_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkAddDocument.Click
        Session("mAdminDocumentMode") = "ADD"
        Response.Redirect("ManageDocumentsAdd.aspx")
    End Sub

    Private Sub lnkChangeDocument_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkChangeDocument.Click
        'Response.Cookies("Admin")("DocumentMode") = "CHANGE"
        Session("mAdminDocumentMode") = "CHANGE"
        Response.Redirect("ManageDocumentsChange.aspx")
    End Sub

    Private Sub lnkRemoveDocument_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkRemoveDocument.Click
        'Response.Cookies("Admin")("DocumentMode") = "REMOVE"
        Session("mAdminDocumentMode") = "REMOVE"
        Response.Redirect("ManageDocumentsChange.aspx")
    End Sub
End Class

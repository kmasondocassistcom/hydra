<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ScanOptions.aspx.vb" Inherits="docAssistWeb.ScanOptions"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title></title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body class="Prefs" bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form class="Prefs" id="frmPreferences" method="post" runat="server">
			<br>
			<table class="Prefs" cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
				<tr>
					<td width="20" rowSpan="6"></td>
					<td class="PrefHeader">Scan Options
					</td>
				</tr>
				<tr>
					<td><BR>
					</td>
				</tr>
				<tr>
					<td align="left">
						<table class="Prefs" cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
							<tr>
								<td width="20" rowSpan="4"></td>
								<td class="LabelName" noWrap align="right" width="160">
									<asp:CheckBox id="chkWarning" runat="server"></asp:CheckBox></td>
								<td noWrap>&nbsp;Warn on saving a document with an average page size greater than
									<asp:TextBox id="WarningSize" runat="server"></asp:TextBox>
									&nbsp;bytes</td>
							</tr>
							<tr>
								<td width="20" rowSpan="4"></td>
								<td noWrap align="left" width="160">
								</td>
								<td>
									<asp:ImageButton id="btnSave" runat="server" ImageUrl="Images/btn_save.gif"></asp:ImageButton>&nbsp;
									<asp:Label id="lblMessage" runat="server" ForeColor="Red"></asp:Label></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			</TD></TR></TABLE></TR></TABLE></form>
	</body>
</HTML>

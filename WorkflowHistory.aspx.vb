Imports Accucentric.docAssist.Web.Security
Imports Accucentric.docAssist.Data.Images

Partial Class WorkflowHistory
    'Inherits zumiControls.zumiPage
    Inherits System.Web.UI.Page


    Private intVersionId

    Private mobjUser As User
    Private mcookieSearch As cookieSearch


    Private dtHistory As DataTable


#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Response.Expires = 0
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        '
        ' Check that user is authenticated.
        '
        Try
            If Request.Cookies("docAssist") Is Nothing Then
                Response.Redirect("Default.aspx")
                Exit Sub
            End If

            Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
            Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))

            Me.mcookieSearch = New cookieSearch(Me)

            Me.mobjUser.AccountId = guidAccountId
        Catch ex As Exception
            Response.Redirect("Default.aspx")
            Exit Sub
        End Try

        '
        ' Check that user should be seeing this screen.
        '
        'Try
        '    If CheckWorkflow(Me.mobjUser.ImagesUserId, intVersionId, Me.mobjUser) = False Then
        '        Response.Redirect("Default.aspx")
        '        Exit Sub
        '    End If
        'Catch ex As Exception

        'End Try

        '
        ' Enable elements if user is in action mode
        '
        'If Request.QueryString("Mode") = "W" Then
        '    ddlQueue.Enabled = True
        '    chkUrgent.Enabled = True
        '    txtNotes.Enabled = True
        'End If

        'Dim intRouteId As Integer = Request.QueryString("Route")
        'litRoute.Text = "<script language='javascript'>parent.frames[""frameAttributes""].RouteId(" & intRouteId & ");</script>"

        '
        'Load data
        '
        If Not Page.IsPostBack Then

            Try
                Dim objWf As New Workflow(Functions.BuildConnection(Me.mobjUser))

                Dim dsHistory As New DataSet

                dsHistory = objWf.WFRouteDetail(Me.mcookieSearch.VersionID, Me.mobjUser.TimeDiffFromGMT)

                dgHistory.DataSource = dsHistory.Tables(1)
                dgHistory.DataBind()

                lblWorkflowName.Text = dsHistory.Tables(0).Rows(0)("WorkflowName")

            Catch ex As Exception

            End Try

        End If

    End Sub

    Private Sub dgHistory_ItemCreated(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs)

    End Sub

    Private Sub dgHistory_ItemDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs)

        Try
            Dim lblNote As Label = CType(e.Item.FindControl("lblNote"), Label)
            Dim lblDateTime As Label = CType(e.Item.FindControl("lblDateTime"), Label)

            If Not lblDateTime Is Nothing Then
                Dim dtTime As DateTime = CType(lblDateTime.Text, DateTime)
                lblDateTime.Text = dtTime.ToShortDateString
            End If

            If Not lblNote Is Nothing Then
                If lblNote.Text = "" Then lblNote.Visible = False
            End If

        Catch ex As Exception

        End Try

    End Sub
End Class

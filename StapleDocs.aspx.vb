Partial Class StapleDocs
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private mobjUser As Accucentric.docAssist.Web.Security.User

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Response.Expires = 0
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        Try
            Me.mobjUser = BuildUserObject(Request.Cookies("docAssist"))
        Catch ex As Exception
            If ex.Message = "User is not logged on" Then
                'TODO: Close window
                Response.Redirect("Default.aspx")
            End If
        End Try


        If Not Page.IsPostBack Then

            'Load session table with version id's/image id's
            Dim dt As New DataTable
            dt = Session("StapleDocs")

            If dt.Rows.Count > 0 Then
                dgDocs.DataSource = dt
                dgDocs.DataBind()
            End If

        End If

    End Sub

    Private Sub dgDocs_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgDocs.ItemDataBound

        Try

            Dim lnkUp As LinkButton = e.Item.FindControl("lnkUp")
            Dim lnkDown As LinkButton = e.Item.FindControl("lnkDown")

            If Not IsNothing(lnkDown) Then

                If e.Item.ItemIndex = 0 Then
                    lnkUp.Visible = False
                End If

                If e.Item.ItemIndex + 1 = dgDocs.DataSource.rows.count Then
                    lnkDown.Visible = False
                End If

            End If

        Catch ex As Exception

        End Try

    End Sub

    Private Sub dgDocs_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgDocs.ItemCreated

        Try

            Dim lnkUp As LinkButton = e.Item.FindControl("lnkUp")
            Dim lnkDown As LinkButton = e.Item.FindControl("lnkDown")

            If Not IsNothing(lnkDown) Then
                AddHandler lnkUp.Click, AddressOf upClick
                AddHandler lnkDown.Click, AddressOf downClick
            End If

        Catch ex As Exception

        End Try

    End Sub

    Private Sub upClick(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim index As Integer = CType(CType(sender, LinkButton).Parent.Parent, System.Web.UI.WebControls.DataGridItem).ItemIndex

        'Reverese bind the grid
        Dim dt As New DataTable
        dt.Columns.Add(New DataColumn("VersionId"))
        dt.Columns.Add(New DataColumn("ImageId"))

        For Each dgi As DataGridItem In dgDocs.Items
            Dim lblDocNo As Label = dgi.FindControl("lblDocNo")
            Dim lblVersionId As Label = dgi.FindControl("lblVersionId")
            Dim dr As DataRow
            dr = dt.NewRow
            dr("ImageId") = lblDocNo.Text
            dr("VersionId") = lblVersionId.Text
            dt.Rows.Add(dr)
        Next

        'Swap the row
        Dim rowToMove As DataRow = dt.Rows(index)
        Dim toMoveImageId As Integer = rowToMove("ImageId")
        Dim toMoveVersionId As Integer = rowToMove("VersionId")

        Dim targetRow As DataRow = dt.Rows(index - 1)
        Dim targetImageId As Integer = targetRow("ImageId")
        Dim targetVersionId As Integer = targetRow("VersionId")

        rowToMove("ImageId") = targetImageId
        rowToMove("VersionId") = targetVersionId
        targetRow("ImageId") = toMoveImageId
        targetRow("VersionId") = toMoveVersionId

        'Update the grid
        dgDocs.DataSource = dt
        dgDocs.DataBind()

    End Sub

    Private Sub downClick(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim index As Integer = CType(CType(sender, LinkButton).Parent.Parent, System.Web.UI.WebControls.DataGridItem).ItemIndex

        'Reverese bind the grid
        Dim dt As New DataTable
        dt.Columns.Add(New DataColumn("VersionId"))
        dt.Columns.Add(New DataColumn("ImageId"))

        For Each dgi As DataGridItem In dgDocs.Items
            Dim lblDocNo As Label = dgi.FindControl("lblDocNo")
            Dim lblVersionId As Label = dgi.FindControl("lblVersionId")
            Dim dr As DataRow
            dr = dt.NewRow
            dr("ImageId") = lblDocNo.Text
            dr("VersionId") = lblVersionId.Text
            dt.Rows.Add(dr)
        Next

        'Swap the row
        Dim rowToMove As DataRow = dt.Rows(index)
        Dim toMoveImageId As Integer = rowToMove("ImageId")
        Dim toMoveVersionId As Integer = rowToMove("VersionId")

        Dim targetRow As DataRow = dt.Rows(index + 1)
        Dim targetImageId As Integer = targetRow("ImageId")
        Dim targetVersionId As Integer = targetRow("VersionId")

        rowToMove("ImageId") = targetImageId
        rowToMove("VersionId") = targetVersionId
        targetRow("ImageId") = toMoveImageId
        targetRow("VersionId") = toMoveVersionId

        'Update the grid
        dgDocs.DataSource = dt
        dgDocs.DataBind()

        'ReturnControls.Add(dgDocs)

    End Sub

    Private Sub btnStaple_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnStaple.Click

        Dim conSql As SqlClient.SqlConnection
        conSql = Functions.BuildConnection(Me.mobjUser)
        conSql.Open()

        Dim tran As SqlClient.SqlTransaction

        Try

            Dim strVersionIds As New System.Text.StringBuilder

            Dim x As Integer = 1
            Dim verId As Integer

            For Each dgi As DataGridItem In dgDocs.Items
                Dim lblVersionId As Label = dgi.FindControl("lblVersionId")
                If x = 1 Then verId = lblVersionId.Text
                strVersionIds.Append(lblVersionId.Text)
                If dgi.ItemIndex + 1 < dgDocs.Items.Count Then strVersionIds.Append(",")
                x += 1
            Next

            tran = conSql.BeginTransaction

            Dim objFolders As New Accucentric.docAssist.Data.Images.Folders(conSql, False, tran)
            Dim newVersionId, newPageCount As Integer
            objFolders.StapleDocument(strVersionIds.ToString, newVersionId, newPageCount)

            tran.Commit()

            System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.Empty.ToString, "window.returnValue = '" & newVersionId & "," & newPageCount & "," & strVersionIds.ToString & "';window.close();", True)

        Catch ex As Exception

            tran.Rollback()

        Finally

            If conSql.State <> ConnectionState.Closed Then
                conSql.Close()
                conSql.Dispose()
            End If

        End Try

    End Sub

End Class
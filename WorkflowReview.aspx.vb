Imports Accucentric.docAssist.Web.Security
Imports Accucentric.docAssist.Data.Images

Partial Class WorkflowReview
    Inherits System.Web.UI.Page


    Private intVersionId

    Private mobjUser As User
    Private mcookieSearch As cookieSearch


    Private dsSubmit As New DataSet


#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'RegisterWaitDisabled(ddlQueue)
        'RegisterWaitDisabled(ddlAction)

        'ddlQueue.Attributes("onchange") = "parent.frames['frameAttributes'].QueueIdSet(this.options[this.selectedIndex].value);"

        '
        'Page Init
        '
        txtNotes.Attributes("onkeypress") = "PassWorkflowNote();"
        txtNotes.Attributes("onblur") = "PassWorkflowNote();"

        '
        ' Check that user is authenticated.
        '
        Dim strVersionId As String
        Try
            If Request.Cookies("docAssist") Is Nothing Then
                Response.Redirect("Default.aspx")
                Exit Sub
            End If

            Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
            Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))

            Me.mcookieSearch = New cookieSearch(Me)

            Me.mobjUser.AccountId = guidAccountId
            strVersionId = Me.mcookieSearch.VersionID
        Catch ex As Exception
            Response.Redirect("Default.aspx")
            Exit Sub
        End Try

        '
        ' Check that user should be seeing this screen.
        '
        Try
            If CheckWorkflow(Me.mobjUser.ImagesUserId, Me.mcookieSearch.VersionID, Me.mobjUser) = False Then
                Response.Redirect("Default.aspx")
                Exit Sub
            End If
        Catch ex As Exception

        End Try

        If Not Request.QueryString("Route") = "" Then
            Dim intRouteId As Integer = Request.QueryString("Route")
            Session(strVersionId & "Mode") = Request.QueryString("Mode")
            Session(strVersionId & "Route") = intRouteId.ToString
        End If

        'Load data
        If Not Page.IsPostBack Then

            Try
                Dim objWf As New Workflow(Functions.BuildConnection(Me.mobjUser))
                dsSubmit = objWf.WFReviewerActionsGet(Me.mcookieSearch.VersionID, Me.mobjUser.ImagesUserId)

                lblWorkflow.Text = dsSubmit.Tables(0).Rows(0)("WorkflowName")

                ddlAction.DataSource = dsSubmit.Tables(1)
                ddlAction.DataTextField = "StatusDesc"
                ddlAction.DataValueField = "QueueActionId"
                ddlAction.DataBind()

                chkUrgent.Checked = dsSubmit.Tables(3).Rows(0)("Urgent")
                lblQueueName.Text = dsSubmit.Tables(3).Rows(0)("QueueName")

                ddlAction.Items.Add(New ListItem("Select an action...", "-2"))
                ddlAction.SelectedValue = -2

            Catch ex As Exception

            End Try

        End If

    End Sub

    Private Sub ddlQueue_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlQueue.SelectedIndexChanged

        Try
            Dim strVersionId As String = Me.mcookieSearch.VersionID
            Session(strVersionId & "QueueId") = ddlQueue.SelectedValue
            'ReturnScripts.Add("parent.frames[""frameAttributes""].ActivateSave();")

        Catch ex As Exception

        End Try

    End Sub

    Private Sub chkUrgent_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkUrgent.CheckedChanged

        Try

            Dim intPass As Integer = chkUrgent.Checked
            Dim strVersionId As String = Me.mcookieSearch.VersionID
            Session(strVersionId & "Urgent") = intPass
            'ReturnScripts.Add("parent.frames[""frameAttributes""].ActivateSave();")

        Catch ex As Exception

        End Try

    End Sub

    Private Function Hide()
        lblQueue.Visible = False
        lblNotes.Visible = False
        ddlQueue.Visible = False
        txtNotes.Visible = False
        chkUrgent.Visible = False
        'ReturnControls.Add(lblQueue)
        'ReturnControls.Add(lblNotes)
        'ReturnControls.Add(ddlQueue)
        'ReturnControls.Add(txtNotes)
        'ReturnControls.Add(chkUrgent)
    End Function

    Private Function Show()
        lblQueue.Visible = True
        lblNotes.Visible = True
        ddlQueue.Visible = True
        txtNotes.Visible = True
        chkUrgent.Visible = True
        'ReturnControls.Add(lblQueue)
        'ReturnControls.Add(lblNotes)
        'ReturnControls.Add(ddlQueue)
        'ReturnControls.Add(txtNotes)
        'ReturnControls.Add(chkUrgent)
    End Function

    Private Sub ddlAction_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAction.SelectedIndexChanged

        Dim strVersionId As String = Me.mcookieSearch.VersionID

        Try

            If ddlAction.SelectedValue <> -2 Then
                ddlQueue.Enabled = True
                chkUrgent.Enabled = True
                txtNotes.Enabled = True
            Else
                ddlQueue.DataSource = Nothing
                ddlQueue.DataBind()
                ddlQueue.Enabled = False
                chkUrgent.Checked = False
                chkUrgent.Enabled = False
                txtNotes.Text = ""
                txtNotes.Enabled = False

                Session(strVersionId & "WorkflowId") = "0"

                'ReturnControls.Add(ddlQueue)
                'ReturnControls.Add(chkUrgent)
                'ReturnControls.Add(txtNotes)
                Exit Sub
            End If

            Dim objWf As New Workflow(Functions.BuildConnection(Me.mobjUser))
            dsSubmit = objWf.WFReviewerActionsGet(Me.mcookieSearch.VersionID, Me.mobjUser.ImagesUserId)

            Dim dvActionFilter As New DataView(dsSubmit.Tables(1))
            dvActionFilter.RowFilter = "QueueActionId=" & ddlAction.SelectedValue

            'Move to folder
            Select Case dvActionFilter(0)("FolderId")
                Case 0
                    'Clear
                    'ReturnScripts.Add("parent.frames[""frameAttributes""].setFolder('','');")
                Case -1
                    'Leave alone
                Case Else
                    'Move
                    'ReturnScripts.Add("parent.frames[""frameAttributes""].setFolder('" & dvActionFilter(0)("FolderId") & "','" & dvActionFilter(0)("FolderPath") & "');")
            End Select

            Select Case dvActionFilter(0)("WorkflowAction")
                Case "E"

                    Show()
                    ddlQueue.Visible = False
                    lblQueue.Visible = False
                    chkUrgent.Visible = False

                    lblAction.Text = "This workflow will be completed."
                    lblAction.Visible = True

                    'ReturnScripts.Add("parent.frames[""frameAttributes""].ActivateSave();")
                    Session(strVersionId & "WorkflowId") = dsSubmit.Tables(1).Rows(ddlAction.SelectedIndex)("StatusId")
                    Session(strVersionId & "QueueId") = "0"
                    Exit Sub

                Case "C"

                    Show()
                    ddlQueue.Visible = False
                    lblQueue.Visible = False
                    chkUrgent.Visible = False

                    lblAction.Text = "This workflow will be cancelled."
                    lblAction.Visible = True

                    'ReturnScripts.Add("parent.frames[""frameAttributes""].ActivateSave();")
                    Session(strVersionId & "WorkflowId") = dsSubmit.Tables(1).Rows(ddlAction.SelectedIndex)("StatusId")
                    Session(strVersionId & "QueueId") = "0"

                    Exit Sub

                Case "B"
                    Show()
                    lblAction.Text = ""
                    lblAction.Visible = False

                    Dim x As Integer = 0
                    Dim y As Integer = 0

                    Select Case dvActionFilter(0)("BackAction")
                        Case 0
                            x = dsSubmit.Tables(4).Rows.Count - 1
                            y = dsSubmit.Tables(4).Rows.Count - 1
                        Case 1
                            x = 0
                            y = dsSubmit.Tables(4).Rows.Count - 1
                        Case 2
                            x = 0
                            y = 0
                    End Select

                    ddlQueue.Items.Clear()
                    For intCounter As Integer = x To y
                        Dim lstItem As New ListItem(dsSubmit.Tables(4).Rows(intCounter)("QueueCode"), dsSubmit.Tables(4).Rows(intCounter)("QueueId"))
                        ddlQueue.Items.Add(lstItem)
                    Next

                    'ReturnScripts.Add("parent.frames[""frameAttributes""].ActivateSave();")
                    Session(strVersionId & "WorkflowId") = dsSubmit.Tables(1).Rows(ddlAction.SelectedIndex)("StatusId")
                    Session(strVersionId & "QueueId") = ddlQueue.SelectedValue

                Case "F"
                    Show()
                    lblAction.Text = ""
                    lblAction.Visible = False

                    Dim dvFilter As New DataView(dsSubmit.Tables(2))
                    dvFilter.RowFilter = "QueueActionId=" & ddlAction.SelectedValue

                    ddlQueue.DataTextField = "QueueCode"
                    ddlQueue.DataValueField = "QueueId"
                    ddlQueue.DataSource = dvFilter
                    ddlQueue.DataBind()

                    ddlQueue.Enabled = True
                    txtNotes.Enabled = True
                    chkUrgent.Enabled = True

                    'ReturnScripts.Add("parent.frames[""frameAttributes""].ActivateSave();")
                    Session(strVersionId & "WorkflowId") = dsSubmit.Tables(1).Rows(ddlAction.SelectedIndex)("StatusId")
                    Session(strVersionId & "QueueId") = ddlQueue.SelectedValue

            End Select

        Catch ex As Exception

        End Try

    End Sub

    Private Sub txtNotes_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtNotes.TextChanged

        Try

            Dim strVersionId As String = Me.mcookieSearch.VersionID
            'ReturnScripts.Add("parent.frames[""frameAttributes""].ActivateSave();")
            Session(strVersionId & "Notes") = txtNotes.Text

        Catch ex As Exception

        End Try

    End Sub

End Class
<%@ Register TagPrefix="uc1" TagName="docHeader" Src="Controls/docHeader.ascx" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="WorkflowNotificationsAdd.aspx.vb" Inherits="docAssistWeb.WorkflowNotificationsAdd"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<TITLE>docAssist</TITLE>
		<META content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<META content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<META content="JavaScript" name="vs_defaultClientScript">
		<META content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<BODY class="Prefs" bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<FORM id="Form1" method="post" runat="server">
			<br>
			<table cellSpacing="0" cellPadding="0" align="left" border="0" width="92%">
				<tr>
					<td width="20" rowSpan="6" height="15"></td>
					<td class="PrefHeader" height="15">Add Workflow Notification
					</td>
				</tr>
				<tr>
					<td align="right">
						<img src="dummy.gif" height="10" width="1">
					</td>
				</tr>
				<tr valign="top" align="left">
					<td align="left" width="100%">
						<table class="Prefs" cellSpacing="0" cellPadding="0" align="left" border="0" width="100%">
							<tr>
								<td width="20" rowSpan="12"></td>
								<td class="LabelName" noWrap align="right">Notification Type:</td>
								<td class="Text" bgColor="whitesmoke"><asp:radiobutton id="rdoInitial" runat="server" GroupName="ReminderType" Checked="True" Text="Initial"></asp:radiobutton><asp:radiobutton id="rdoReminder" runat="server" GroupName="ReminderType" Text="Reminder"></asp:radiobutton></td>
							</tr>
							<tr>
								<td class="Text" noWrap align="right" width="160"></td>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td class="LabelName" noWrap align="right">Queue:</td>
								<td class="Text" bgColor="whitesmoke"><IMG height="1" src="dummy.gif" width="2">
									<asp:dropdownlist id="ddlQueue" runat="server" CssClass="Text" Width="200px"></asp:dropdownlist></td>
							</tr>
							<tr>
								<td class="Text" noWrap align="right" width="160"></td>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td class="Text" noWrap align="right"></td>
								<td class="Text" bgColor="whitesmoke">&nbsp;
									<asp:RadioButton id="rdoNormal" runat="server" Text="Normal" Checked="True"></asp:RadioButton>
									<asp:RadioButton id="rdoUrgent" runat="server" Text="Urgent"></asp:RadioButton>
									<asp:RadioButton id="rdoBoth" runat="server" Text="Both"></asp:RadioButton></td>
							</tr>
							<tr>
								<td class="Text" noWrap align="right" width="160"></td>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td class="LabelName" vAlign="top" noWrap align="right" width="160">Schedule:</td>
								<td>
									<table cellSpacing="0" cellPadding="0" width="100%" border="0">
										<tr>
											<td bgColor="whitesmoke"><asp:radiobutton id="rdoRealTime" runat="server" GroupName="Schedule" Checked="True" Text="Real-Time"
													CssClass="Text"></asp:radiobutton></td>
										</tr>
										<tr>
											<td class="Text"><asp:radiobutton id="rdoDaily" runat="server" GroupName="Schedule" Text="Daily at"></asp:radiobutton>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												<ew:timepicker id="txtDaily" runat="server" CssClass="Text" CellPadding="5px" NumberOfColumns="1"
													PopupWidth="75px" PopupHeight="220px" MinuteInterval="FifteenMinutes" Width="57px">
													<ClearTimeStyle BackColor="White"></ClearTimeStyle>
													<TimeStyle Font-Size="8pt" Font-Names="Arial" ForeColor="Black" BackColor="White"></TimeStyle>
													<SelectedTimeStyle Font-Size="8pt" Font-Names="Arial" Font-Bold="True" BackColor="White"></SelectedTimeStyle>
												</ew:timepicker></td>
										</tr>
										<TR>
											<td class="Text" bgColor="whitesmoke"><asp:radiobutton id="rdoWeekly" runat="server" GroupName="Schedule" Text="Weekly at"></asp:radiobutton>&nbsp;&nbsp;
												<ew:timepicker id="txtWeekly" runat="server" CssClass="Text" CellPadding="5px" NumberOfColumns="1"
													PopupWidth="75px" PopupHeight="220px" MinuteInterval="FifteenMinutes" Width="57px">
													<ClearTimeStyle BackColor="White"></ClearTimeStyle>
													<TimeStyle Font-Size="8pt" Font-Names="Arial" ForeColor="Black" BackColor="White"></TimeStyle>
													<SelectedTimeStyle Font-Size="8pt" Font-Names="Arial" Font-Bold="True" BackColor="White"></SelectedTimeStyle>
												</ew:timepicker></td>
										</TR>
										<tr>
											<td class="Text" id="Weekly" bgColor="whitesmoke"><IMG height="1" src="dummy.gif" width="10">
												<asp:checkbox id="chkMonday" runat="server" Text="Monday" Font-Size="8pt"></asp:checkbox>&nbsp;<asp:checkbox id="chkTuesday" runat="server" Text="Tuesday" Font-Size="8pt"></asp:checkbox>&nbsp;<asp:checkbox id="chkWednesday" runat="server" Text="Wednesday" Font-Size="8pt"></asp:checkbox>&nbsp;<asp:checkbox id="chkThursday" runat="server" Text="Thursday" Font-Size="8pt"></asp:checkbox>&nbsp;<asp:checkbox id="chkFriday" runat="server" Text="Friday" Font-Size="8pt"></asp:checkbox>&nbsp;<asp:checkbox id="chkSaturday" runat="server" Text="Saturday" Font-Size="8pt"></asp:checkbox>&nbsp;<asp:checkbox id="chkSunday" runat="server" Text="Sunday" Font-Size="8pt"></asp:checkbox>&nbsp;</td>
										</tr>
									</table>
									<BR>
									<asp:Label id="lblError" runat="server" CssClass="ErrorText" Visible="False"></asp:Label>
								</td>
							</tr>
							<tr>
								<td class="Text" noWrap align="right" width="160"></td>
								<td>&nbsp;<IMG height="1" src="dummy.gif" width="350">
									<asp:imagebutton id="btnAdd" runat="server" ImageUrl="Images/btn_save.gif"></asp:imagebutton></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</FORM>
		<SCRIPT language="javascript">
			
			disableWeekly();
		
			function disableRealTime()
			{
				document.getElementById('rdoRealTime').disabled = true;
				document.getElementById('rdoRealTime').checked = false;
				document.getElementById('rdoDaily').checked = true;
			}

			function enableRealTime()
			{
				document.getElementById('rdoRealTime').disabled = false;
			}
			
			function disableWeekly()
			{
				document.getElementById('Weekly').disabled = true;
			}

			function enableWeekly()
			{
				document.getElementById('Weekly').disabled = false;
			}
			
			function disableNotUrgent()
			{
				document.getElementById('rdoNormal').checked = false;
				document.getElementById('rdoBoth').checked = false;
			}
			
				function disableNotNormal()
			{
				document.getElementById('rdoUrgent').checked = false;
				document.getElementById('rdoBoth').checked = false;
			}
		
						function disableNotBoth()
			{
				document.getElementById('rdoUrgent').checked = false;
				document.getElementById('rdoNormal').checked = false;
			}
		
		</SCRIPT>
	</BODY>
</HTML>

Partial Class Notes
    Inherits System.Web.UI.Page

    Dim mobjUser As Accucentric.docAssist.Web.Security.User
    Dim mconSqlImage As SqlClient.SqlConnection
    Protected WithEvents txtNote As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnSave As System.Web.UI.WebControls.ImageButton
    Dim mcookieSearch As cookieSearch


#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'AB
        Try

            Me.mobjUser = BuildUserObject(Request.Cookies("docAssist"))
            Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
            Me.mcookieSearch = New cookieSearch(Me)

        Catch ex As Exception

            If ex.Message = "User is not logged on" Then
                Response.Redirect("Default.aspx")
            End If

        End Try

        Try

            If Me.mconsqlimage.State <> ConnectionState.Open Then
                Me.mconsqlimage.Open()
            End If

            Dim sqlCmd As New SqlClient.SqlCommand("acsImageNotesByVersionID", Me.mconSqlImage)
            sqlCmd.CommandType = CommandType.StoredProcedure
            sqlCmd.Parameters.Add("@VersionID", Me.mcookieSearch.VersionID)
            sqlCmd.Parameters.Add("@DiffFromGMT", Me.mobjUser.TimeDiffFromGMT)

            Dim dtNotes As New DataTable
            Dim sqlda As New SqlClient.SqlDataAdapter(sqlCmd)
            sqlda.Fill(dtNotes)

            Dim sbDocumentText As New System.Text.StringBuilder
            Dim intPageCount As Integer = 1

            Me.mconSqlImage.Close()
            Me.mconSqlImage.Dispose()

            lblNotes.Visible = True

            If dtNotes.Rows.Count = 0 Then
                'No notes available message.
                lblNotes.Text = "No notes have been saved for this document."
            Else
                'Format notes
                Dim sbNotes As New System.Text.StringBuilder
                Dim dr As DataRow

                For Each dr In dtNotes.Rows
                    sbNotes.Append("<BR><BR><b>Added by " & "<a href=""mailto:" & dr("EmailAddress") & """>" & dr("username") & "</a>" & " on " & FormatDateTime(dr("notedatetime"), DateFormat.LongDate) & " " & FormatDateTime(dr("notedatetime"), DateFormat.LongTime) & "</b><BR><BR>" & dr("note") & "<BR><BR>")
                Next

                lblNotes.Text = sbNotes.ToString

            End If

        Catch ex As Exception

            'Error loading notes
            Me.mconSqlImage.Close()
            Me.mconSqlImage.Dispose()

        End Try
    End Sub

End Class

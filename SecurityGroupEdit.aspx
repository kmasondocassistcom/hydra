<%@ Page Language="vb" AutoEventWireup="false" Codebehind="SecurityGroupEdit.aspx.vb" Inherits="docAssistWeb.SecurityGroupEdit" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
  <HEAD>
		<TITLE>docAssist - Security Groups</TITLE>
		<META http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<META content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<META content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<META content="JavaScript" name="vs_defaultClientScript">
		<META content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
		<base target="_self">
  </HEAD>
	<BODY id="pageBody" runat="server">
		<FORM id="Form1" method="post" runat="server">
			<table height="800" cellSpacing="0" cellPadding="0" width="900" border="0">
				<tr>
					<td vAlign="top">
						<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td vAlign="top" height="100%">
									<div class="Container">
										<div class="north">
											<div class="east">
												<div class="south">
													<div class="west">
														<div class="ne">
															<div class="se">
																<div class="sw">
																	<div class="nw">
																		<table id="surround" height="100%" cellSpacing="0" cellPadding="0" width="95%" border="0"
																			class="PageContent">
																			<tr>
																				<td align="right">
																					<table class="PageContent" cellSpacing="0" cellPadding="0" width="100%" border="0">
																						<tr>
																							<td noWrap colSpan="2"><span class="Heading">Group:</span></td>
																						</tr>
																						<tr>
																							<TD noWrap><IMG height="1" src="spacer.gif" width="7"></TD>
																							<td noWrap width="100%"><span class="Text" id="GroupNameLabel" runat="server"><IMG height="1" src="spacer.gif" width="31">Group 
																									Name:&nbsp;</span><asp:textbox id="txtGroupName" runat="server" Width="300px" Font-Names="Trebuchet MS" Font-Size="8pt"
																									MaxLength="45"></asp:textbox></td>
																						</tr>
																						<tr>
																							<TD noWrap></TD>
																							<td noWrap><span class="Text" id="GroupDescLabel" runat="server">Group Description: </span>
																								<asp:textbox id="txtGroupDesc" runat="server" Width="300px" Font-Names="Trebuchet MS" Font-Size="8pt"
																									MaxLength="100"></asp:textbox></td>
																						</tr>
																						<tr>
																							<td noWrap align="left" colSpan="2"><span class="Heading">Rights:</span>
																								<table id="Tabs" cellSpacing="0" cellPadding="0" border="0" runat="server" width="100%">
																									<tr style="FONT-FAMILY: 'Trebuchet MS'">
																										<td class="selectedTab" id="tdDocs" onclick="showDocs();" align="center" width="150">Documents</td>
																										<td class="unselectedTab"><IMG height="1" src="spacer.gif" width="5"></td>
																										<td class="unselectedTab" id="tdAttributes" onclick="showAttributes();" noWrap align="center"
																											width="120">Attributes</td>
																										<td class="unselectedTab"><IMG height="1" src="spacer.gif" width="5"></td>
																										<td class="unselectedTab" id="tdLookups" onclick="showLookups();" noWrap align="center" style="DISPLAY: none"
																											width="120">Lookups</td>
																										<td class="unselectedTab" style="BORDER-BOTTOM: #6382ad 1px solid" width="650">&nbsp;</td>
																									</tr>
																								</table>
																								<span id="documentRights">
																									<table class="SearchGrid" style="BORDER-RIGHT: lightgrey thin solid; BORDER-LEFT: lightgrey thin solid; BORDER-TOP-STYLE: none; BORDER-BOTTOM: lightgrey thin"
																										borderColor="gainsboro" cellSpacing="0" cellPadding="0" border="1">
																										<tr>
																											<td vAlign="middle" align="center" width="195"><b>&nbsp;Document</b></td>
																											<td vAlign="bottom" width="280"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Access<BR>
																												</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<A id="NoAccessAll" href="javascript:NoAccessAll()">No 
																													Access</A> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <A id="ReadOnlyAll" href="javascript:ReadOnlyAll()">
																													Read Only</A>&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;<A id="ChangeAll" href="javascript:ChangeAll()">Change</A>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																												<A id="DeleteAll" href="javascript:DeleteAll()">Delete </A>
																											</td>
																											<td vAlign="bottom" align="center" width="61"><b>&nbsp;Creator Only<BR>
																												</b><A id="CreatorAll" href="javascript:CreatorAll()">All</A>
																											</td>
																											<td vAlign="bottom" align="center" width="62"><b>&nbsp;Current Version<BR>
																												</b><A id="CurrentAll" href="javascript:CurrentAll()">All</A>
																											</td>
																											<td vAlign="bottom" align="center" width="62"><b>&nbsp;Print<BR>
																												</b><A id="PrintAll" href="javascript:PrintAll()">All</A>
																											</td>
																											<td vAlign="bottom" align="center" width="62"><b>&nbsp;E-Mail &amp; Download<BR>
																												</b><A id="MailAll" href="javascript:MailAll()">All</A>
																											</td>
																											<td vAlign="bottom" align="center" width="68"><b>&nbsp;Manage Annotations<BR>
																												</b><A id="AnnotationsAll" href="javascript:AnnotationsAll()">All</A>
																											</td>
																											<td vAlign="bottom" align="center" width="64" colSpan="2" style="DISPLAY:none"><b>&nbsp;Manage Redactions<BR>
																												</b><A id="RedactionsAll" href="javascript:RedactionsAll()">All</A>
																											</td>
																											<td width="18"></td>
																										</tr>
																									</table>
																									<div class="SmartFolderGrids" style="BORDER-RIGHT: silver thin solid; OVERFLOW: auto; BORDER-LEFT: silver thin solid; BORDER-BOTTOM: silver thin solid; HEIGHT: 200px"><asp:datagrid id="dgGroupPermissions" runat="server" Width="100%" AutoGenerateColumns="False"
																											CssClass="SearchGrid" ShowHeader="False">
																											<AlternatingItemStyle BackColor="White"></AlternatingItemStyle>
																											<ItemStyle BackColor="Silver"></ItemStyle>
																											<HeaderStyle Font-Bold="True"></HeaderStyle>
																											<Columns>
																												<asp:TemplateColumn HeaderText="Document">
																													<ItemStyle Width="180px"></ItemStyle>
																													<ItemTemplate>
																														<asp:Label id="lblDocumentName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DocumentName") %>'>
																														</asp:Label>
																														<asp:Label id=lblDocumentId runat="server" Visible=False Text='<%# DataBinder.Eval(Container, "DataItem.Sec_Value") %>'>
																														</asp:Label>
																													</ItemTemplate>
																												</asp:TemplateColumn>
																												<asp:TemplateColumn HeaderText="Access">
																													<ItemStyle HorizontalAlign="Center" Width="290px"></ItemStyle>
																													<ItemTemplate>
																														<asp:Label id="lblSecLevel" runat="server" Visible=False Text='<%# DataBinder.Eval(Container, "DataItem.Sec_Level") %>'>
																														</asp:Label>
																														<asp:RadioButton id="rdoNoAccess" runat="server" GroupName="Access" Text="No Access"></asp:RadioButton>
																														<asp:RadioButton id="rdoReadOnly" runat="server" GroupName="Access" Text="Read Only"></asp:RadioButton>
																														<asp:RadioButton id="rdoChange" runat="server" GroupName="Access" Text="Change"></asp:RadioButton>
																														<asp:RadioButton id="rdoDelete" runat="server" GroupName="Access" Text="Delete"></asp:RadioButton>
																													</ItemTemplate>
																												</asp:TemplateColumn>
																												<asp:TemplateColumn HeaderText="Creator Only">
																													<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
																													<ItemStyle HorizontalAlign="Center" Width="65px"></ItemStyle>
																													<ItemTemplate>
																														<asp:CheckBox id=chkCreatorOnly runat="server" Checked='<%# DataBinder.Eval(Container, "DataItem.CreatorOnly") %>'>
																														</asp:CheckBox>
																													</ItemTemplate>
																												</asp:TemplateColumn>
																												<asp:TemplateColumn HeaderText="Current Version Only">
																													<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
																													<ItemStyle HorizontalAlign="Center" Width="65px"></ItemStyle>
																													<ItemTemplate>
																														<asp:CheckBox id="chkCurrentVersionOnly" runat="server" Checked='<%# DataBinder.Eval(Container, "DataItem.VersionOnly") %>'>
																														</asp:CheckBox>
																													</ItemTemplate>
																												</asp:TemplateColumn>
																												<asp:TemplateColumn HeaderText="Print">
																													<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
																													<ItemStyle HorizontalAlign="Center" Width="65px"></ItemStyle>
																													<ItemTemplate>
																														<asp:CheckBox id="chkPrint" runat="server" Checked='<%# DataBinder.Eval(Container, "DataItem.Print") %>'>
																														</asp:CheckBox>
																													</ItemTemplate>
																												</asp:TemplateColumn>
																												<asp:TemplateColumn HeaderText="E-Mail/Export">
																													<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
																													<ItemStyle HorizontalAlign="Center" Width="65px"></ItemStyle>
																													<ItemTemplate>
																														<asp:CheckBox id="chkEmailExport" runat="server" Checked='<%# DataBinder.Eval(Container, "DataItem.Export") %>'>
																														</asp:CheckBox>
																													</ItemTemplate>
																												</asp:TemplateColumn>
																												<asp:TemplateColumn HeaderText="Manage Annotations">
																													<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
																													<ItemStyle HorizontalAlign="Center" Width="70px"></ItemStyle>
																													<ItemTemplate>
																														<asp:CheckBox Checked='<%# DataBinder.Eval(Container, "DataItem.Annotate") %>' id="chkAnnotations" runat="server">
																														</asp:CheckBox>
																													</ItemTemplate>
																												</asp:TemplateColumn>
																												<asp:TemplateColumn HeaderText="Manage Redactions" Visible=False>
																													<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
																													<ItemStyle HorizontalAlign="Center" Width="65px"></ItemStyle>
																													<ItemTemplate>
																														<asp:CheckBox id="chkRedactions" runat="server" Checked='<%# DataBinder.Eval(Container, "DataItem.Redact") %>'>
																														</asp:CheckBox>
																													</ItemTemplate>
																												</asp:TemplateColumn>
																											</Columns>
																										</asp:datagrid></div>
																								</span><span id="spanAttributes" style="BORDER-RIGHT: lightgrey thin solid; DISPLAY: none; BORDER-LEFT: lightgrey thin solid; WIDTH: 100%; BORDER-BOTTOM: lightgrey thin; HEIGHT: 252px">
																									<table class="SearchGrid" cellpadding="0" cellspacing="0" border="0" width="100%">
																										<tr>
																											<td width="100%" align="right">
																												<asp:CheckBox id="rdoAdvancedPerms" runat="server" Text="Enable advanced attribute permissions"></asp:CheckBox></td>
																										</tr>
																									</table>
																									<table class="SearchGrid" style="BORDER-RIGHT: lightgrey thin solid; BORDER-LEFT: lightgrey thin solid; BORDER-BOTTOM: lightgrey thin"
																										borderColor="gainsboro" cellSpacing="0" cellPadding="0" border="0">
																										<tr>
																											<td vAlign="middle" width="404" align="center"><b>&nbsp;Document/Attribute</b></td>
																											<td vAlign="bottom" align="center" width="405" colSpan="2"><b>Access Level<BR>
																												</b><A id="A1" href="javascript:attributeNoAccessAll()">None</A>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																												<A id="A2" href="javascript:attributeReadOnlyAll()">Read Only</A>&nbsp;&nbsp;&nbsp;&nbsp; 
																												&nbsp;<A id="A3" href="javascript:attributeChangeAll()">Change</A>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																												<A id="A4" href="javascript:attributeDeleteAll()">Delete </A>
																											</td>
																										</tr>
																									</table>
																									<div style="OVERFLOW: auto; HEIGHT: 220px">
																										<asp:datagrid id="dgAttributes" runat="server" ShowHeader="False" CssClass="SearchGrid" AutoGenerateColumns="False"
																											Width="100%">
																											<SelectedItemStyle Wrap="False"></SelectedItemStyle>
																											<AlternatingItemStyle Wrap="False" BackColor="White"></AlternatingItemStyle>
																											<ItemStyle Wrap="False" BackColor="Silver"></ItemStyle>
																											<HeaderStyle Font-Bold="True"></HeaderStyle>
																											<Columns>
																												<asp:TemplateColumn HeaderText="Document/Attribute">
																													<ItemStyle Width="50%"></ItemStyle>
																													<ItemTemplate>
																														<asp:Label id=lblDocName runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DocumentName") %>' Font-Bold="True">
																														</asp:Label>
																														<asp:Label id=lblAttributeName runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AttributeName") %>'>
																														</asp:Label>
																														<asp:Label id=lblAttributeId runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AttributeId") %>' Visible="False">
																														</asp:Label>
																														<asp:Label id="lblDocumentId" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DocumentId") %>' Visible="False">
																														</asp:Label>
																													</ItemTemplate>
																												</asp:TemplateColumn>
																												<asp:TemplateColumn HeaderText="Access">
																													<ItemStyle HorizontalAlign="Center" Width="800px"></ItemStyle>
																													<ItemTemplate>
																														<asp:DropDownList id="ddlAccess" runat="server" Width="150px" SelectedIndex='<%# DataBinder.Eval(Container, "DataItem.AccessLevel") %>'>
																															<asp:ListItem Value="0">None</asp:ListItem>
																															<asp:ListItem Value="1">Read Only</asp:ListItem>
																															<asp:ListItem Value="2">Change</asp:ListItem>
																															<asp:ListItem Value="3">Delete</asp:ListItem>
																														</asp:DropDownList>
																													</ItemTemplate>
																												</asp:TemplateColumn>
																											</Columns>
																										</asp:datagrid></div>
																								</span><span id="spanLookups" style="BORDER-RIGHT: lightgrey thin solid; DISPLAY: none; BORDER-LEFT: lightgrey thin solid; WIDTH: 100%; BORDER-BOTTOM: lightgrey thin; HEIGHT: 252px">
																									<TABLE class="SearchGrid" id="Table2" style="BORDER-RIGHT: lightgrey thin solid; BORDER-LEFT: lightgrey thin solid; BORDER-BOTTOM: lightgrey thin"
																										borderColor="gainsboro" cellSpacing="0" cellPadding="0" border="0">
																										<TR>
																											<TD vAlign="middle" align="center" width="404"><B>&nbsp;Lookup</B></TD>
																											<TD vAlign="bottom" align="center" width="405" colSpan="2"><B>Access Level<BR>
																												</B><A id="A5" href="javascript:lookupNoAccessAll()">Read Only</A>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																												<A id="A6" href="javascript:lookupReadOnlyAll()">Add</A>&nbsp;&nbsp;&nbsp;&nbsp; 
																												&nbsp;<A id="A7" href="javascript:lookupChangeAll()">Change</A>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																												<A id="A8" href="javascript:lookupDeleteAll()">Delete </A>
																											</TD>
																										</TR>
																									</TABLE>
																									<asp:datagrid id="dgLookups" runat="server" Width="100%" ShowHeader="False" CssClass="SearchGrid"
																										AutoGenerateColumns="False">
																										<SelectedItemStyle Wrap="False"></SelectedItemStyle>
																										<AlternatingItemStyle Wrap="False" BackColor="White"></AlternatingItemStyle>
																										<ItemStyle Wrap="False" BackColor="Silver"></ItemStyle>
																										<HeaderStyle Font-Bold="True"></HeaderStyle>
																										<Columns>
																											<asp:TemplateColumn HeaderText="Document/Attribute">
																												<ItemStyle Width="50%"></ItemStyle>
																												<ItemTemplate>
																													<asp:Label id=lblLookupAttributeId runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AttributeId") %>' Font-Bold="True" Visible="False">
																													</asp:Label>
																													<asp:Label id=lblLookupName runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AttributeName") %>'>
																													</asp:Label>
																												</ItemTemplate>
																											</asp:TemplateColumn>
																											<asp:TemplateColumn HeaderText="Access">
																												<ItemStyle HorizontalAlign="Center" Width="800px"></ItemStyle>
																												<ItemTemplate>
																													<asp:DropDownList id=ddlLookup runat="server" Width="150px" SelectedIndex='<%# DataBinder.Eval(Container, "DataItem.AccessLevel") %>'>
																														<asp:ListItem Value="0">Read Only</asp:ListItem>
																														<asp:ListItem Value="1">Add</asp:ListItem>
																														<asp:ListItem Value="2">Change</asp:ListItem>
																														<asp:ListItem Value="3">Delete</asp:ListItem>
																													</asp:DropDownList>
																												</ItemTemplate>
																											</asp:TemplateColumn>
																										</Columns>
																									</asp:datagrid>
																								</span>
																								<BR>
																								<span class="Heading">Users:</span>
																								<BR>
																								<table>
																									<tr>
																										<td>
																											<TABLE class="PageContent" id="Table1" height="100%" cellSpacing="0" cellPadding="0" border="0">
																												<TR>
																													<TD width="50%" height="15">&nbsp;&nbsp;<span class="Text">Available Users:&nbsp;</span></TD>
																													<TD width="25" height="15" rowSpan="1"></TD>
																													<TD width="50%" height="15">&nbsp;<span class="Text">Assigned Users:&nbsp;</span></TD>
																												</TR>
																												<TR>
																													<TD vAlign="top" width="50%" height="100%"><asp:listbox id="lstAvailable" runat="server" Width="215px" Height="180px" SelectionMode="Multiple"></asp:listbox></TD>
																													<TD vAlign="top" align="center" width="25"><BR>
																														<BR>
																														<asp:imagebutton id="btnAssignAll" runat="server" ImageUrl="Images/addall.gif"></asp:imagebutton><BR>
																														<BR>
																														<asp:imagebutton id="btnAdd" runat="server" ImageUrl="Images/addone.gif"></asp:imagebutton><BR>
																														<BR>
																														<asp:imagebutton id="btnRemove" runat="server" ImageUrl="Images/removeone.gif"></asp:imagebutton><BR>
																														<BR>
																														<asp:imagebutton id="btnRemoveAll" runat="server" ImageUrl="Images/removeall.gif"></asp:imagebutton></TD>
																													<TD vAlign="top" width="50%">&nbsp;
																														<asp:listbox id="lstAssigned" runat="server" Width="215px" Height="180px" SelectionMode="Multiple"></asp:listbox></TD>
																												</TR>
																											</TABLE>
																										</td>
																									</tr>
																								</table>
																							</td>
																						</tr>
																					</table>
																					<span class="Text" id="ErrorMessage" runat="server"></span>&nbsp;&nbsp;<A href="javascript:CloseWindow();"><IMG src="Images/smallred_cancel.gif" border="0"></A>&nbsp; 
																					&nbsp;<asp:imagebutton id="btnSave" runat="server" ImageUrl="Images/smallred_save.gif" ToolTip="Save"></asp:imagebutton></td>
																			</tr>
																		</table>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</FORM>
		<script language="javascript">

			function NameError()
			{
				document.all.GroupNameLabel.className='ErrorText';
			}

			function CloseWindow()
			{
				opener.location.reload();
				window.close();
			}

			function ignoreEvent()
			{
				if (event.keyCode == 13)
				{
					event.cancelBubble = true;
					event.returnValue = false;
				}
			}

			function showDocs()
			{
				var documentRights = document.getElementById('documentRights');
				if(documentRights != undefined) { documentRights.style.display = 'block'; }
				var spanAttributes = document.getElementById('spanAttributes');
				if(spanAttributes != undefined) { spanAttributes.style.display = 'none'; }
				var spanLookups = document.getElementById('spanLookups');
				if(spanLookups != undefined) { spanLookups.style.display = 'none'; }
				if (document.getElementById('tdDocs') != undefined) { document.getElementById('tdDocs').className = 'selectedTab'; }
				if (document.getElementById('tdAttributes') != undefined) { document.getElementById('tdAttributes').className = 'unselectedTab'; }
				if (document.getElementById('tdLookups') != undefined) { document.getElementById('tdLookups').className = 'unselectedTab'; }
			}
				
			function showAttributes()
			{
				
				var documentRights = document.getElementById('documentRights');
				if(documentRights != undefined) { documentRights.style.display = 'none'; }
				var spanAttributes = document.getElementById('spanAttributes');
				if(spanAttributes != undefined) { spanAttributes.style.display = 'block'; }
				var spanLookups = document.getElementById('spanLookups');
				if(spanLookups != undefined) { spanLookups.style.display = 'none'; }
				document.getElementById('tdDocs').className = 'unselectedTab';
				document.getElementById('tdAttributes').className = 'selectedTab';
				if (document.getElementById('tdLookups') != undefined) { document.getElementById('tdLookups').className = 'unselectedTab'; }
			}

			function showLookups()
			{
				var documentRights = document.getElementById('documentRights');
				if(documentRights != undefined) { documentRights.style.display = 'none'; }
				var spanAttributes = document.getElementById('spanAttributes');
				if(spanAttributes != undefined) { spanAttributes.style.display = 'none'; }
				var spanLookups = document.getElementById('spanLookups');
				if(spanLookups != undefined) { spanLookups.style.display = 'block'; }
				if (document.getElementById('tdDocs') != undefined) { document.getElementById('tdDocs').className = 'unselectedTab'; }
				if (document.getElementById('tdAttributes') != undefined) { document.getElementById('tdAttributes').className = 'unselectedTab'; }
				if (document.getElementById('tdLookups') != undefined) { document.getElementById('tdLookups').className = 'selectedTab'; }
			}
			
			var allowMassAction = false;
			function setEnable()
			{
				chk = document.getElementById('rdoAdvancedPerms');
				if (chk.checked)
				{
					allowMassAction = true;
					enable();
				}
				else
				{
					allowMassAction = false;
					disable();
				}
			}
			
		</script>
	</BODY>
</HTML>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="AddFiles.aspx.vb" Inherits="docAssistWeb.AddFiles"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>docAssist - Add Attachments</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
		<base target="_self">
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form class="PageContent" id="Form1" method="post" runat="server">
			<TABLE id="Table1" height="600" cellSpacing="0" cellPadding="0" width="950" border="0"> <!-- Parent grey container -->
				<TR>
					<TD vAlign="top">
						<DIV class="Container">
							<DIV class="north">
								<DIV class="east">
									<DIV class="south">
										<DIV class="west">
											<DIV class="ne">
												<DIV class="se">
													<DIV class="sw">
														<DIV class="nw">
															<TABLE id="tblMain" height="620" cellSpacing="0" cellPadding="0" width="95%" border="0">
																<TR>
																	<TD noWrap align="left"><SPAN class="Heading">Add Attachments</SPAN></TD>
																</TR>
																<tr>
																	<td vAlign="top" align="left"><br>
																		<div style="OVERFLOW: auto; HEIGHT: 330px">
																			<TABLE id="formBlockInput" cellSpacing="2" cellPadding="0" border="0">
																				<TR class="formHead">
																					<TD width="283"><span class="Text">Choose file to upload:</span></TD>
																					<TD><span class="Text">Title:</span></TD>
																					<td><span class="Text">Description:</span></td>
																					<td><span class="Text">Comments:</span></td>
																					<td><span class="Text">Tags:</span></td>
																					<td><A id="AddRow" onclick="addRow(this); return false" href="#"><IMG src="Images/bot23.gif" border="0"></A></td>
																				</TR>
																				<TR vAlign="top">
																					<TD><INPUT id="txtFilename" style="FONT-SIZE: 8pt; FONT-FAMILY: 'Trebuchet MS'" type="file"
																							size="40" name="txtFilename"></TD>
																					<TD><asp:textbox id="txtTitle" runat="server" Width="140px" Font-Size="8pt" Font-Names="Trebuchet MS"></asp:textbox></TD>
																					<td width="15"><asp:textbox id="txtDescription" runat="server" Width="140px" Font-Size="8pt" Font-Names="Trebuchet MS"></asp:textbox></td>
																					<td width="15"><asp:textbox id="txtComments" runat="server" Width="140px" Font-Size="8pt" Font-Names="Trebuchet MS"></asp:textbox></td>
																					<td width="15"><asp:textbox id="txtTags" runat="server" Width="140px" Font-Size="8pt" Font-Names="Trebuchet MS"></asp:textbox></td>
																					<td vAlign="bottom" width="15"><A onclick="remRow(this); return false" href="#"><IMG src="Images/circle_minus.gif" border="0"></A></td>
																				</TR>
																			</TABLE>
																		</div>
																		<br>
																		</td>
																</tr>
																<tr>
																	<td align="right">
																		<asp:Label id="lblError" runat="server" CssClass="ErrorText"></asp:Label>&nbsp;<asp:imagebutton id="btnCancel" runat="server" ImageUrl="Images/smallred_cancel.gif"></asp:imagebutton>&nbsp;
																		<asp:ImageButton id="btnSave" runat="server" ImageUrl="Images/smallred_save.gif"></asp:ImageButton></td>
																</tr>
															</TABLE>
														</DIV>
													</DIV>
												</DIV>
											</DIV>
										</DIV>
									</DIV>
								</DIV>
							</DIV>
						</DIV>
					</TD>
				</TR>
			</TABLE>
			<DIV></DIV>
			<DIV></DIV>
			<DIV></DIV>
			<DIV></DIV>
			<DIV></DIV>
			<DIV></DIV>
			<DIV></DIV>
			<DIV></DIV>
			<DIV></DIV>
			</TD></TR></TABLE></form>
		<script language="javascript" src="scripts/upload.js"></script>
		<script language="javascript">
		
			document.all.AddRow.click();
			document.all.AddRow.click();
			document.all.AddRow.click();
			document.all.AddRow.click();
			document.all.AddRow.click();
			document.all.AddRow.click();
			
			function RadProgressManagerOnClientProgressStarted()
			{
				<%= radProgress.ClientID %>.Show();
			}
			
		</script>
	</body>
</HTML>

'Imports Accucentric.docAssist.Data.Images

'Partial Class ConfigureDataSync
'    Inherits System.Web.UI.Page

'    Dim mobjUser As Accucentric.docAssist.Web.Security.User

'    Dim mconSqlCompany As New SqlClient.SqlConnection
'    Dim mconSqlSynchNotifications As New SqlClient.SqlConnection

'#Region " Web Form Designer Generated Code "

'    'This call is required by the Web Form Designer.
'    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

'    End Sub
'    Protected WithEvents btnSave As System.Web.UI.WebControls.ImageButton

'    'NOTE: The following placeholder declaration is required by the Web Form Designer.
'    'Do not delete or move it.
'    Private designerPlaceholderDeclaration As System.Object

'    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
'        'CODEGEN: This method call is required by the Web Form Designer
'        'Do not modify it using the code editor.
'        InitializeComponent()
'    End Sub

'#End Region

'    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

'        Try
'            mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
'            Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))
'            mobjUser.AccountId = guidAccountId
'        Catch ex As Exception
'            Response.Redirect("Default.aspx")
'        End Try

'        Try

'            If Not Page.IsPostBack Then

'                mconSqlCompany = Functions.BuildConnection(Me.mobjUser)

'                LoadDocuments()

'                Dim Attributes As DataTable = Functions.GetAttributesByDocumentId(ddlDocuments.SelectedValue, Me.mobjUser.ImagesUserId, mconSqlCompany)

'                dgAttributes.DataSource = Attributes
'                dgAttributes.DataBind()

'            End If

'        Catch ex As Exception

'        Finally

'            If mconSqlCompany.State <> ConnectionState.Closed Then
'                mconSqlCompany.Close()
'                mconSqlCompany.Dispose()
'            End If

'        End Try

'    End Sub

'    Private Function LoadDocuments() As DataTable

'        Dim sqlCmd As SqlClient.SqlCommand = New SqlClient.SqlCommand("acsDocumentsByCabinetFolder", mconSqlCompany)
'        sqlCmd.CommandType = CommandType.StoredProcedure
'        sqlCmd.Parameters.Add("@UserName", Me.mobjUser.UserId)
'        sqlCmd.Parameters.Add("@CabinetId", "0")
'        sqlCmd.Parameters.Add("@FolderId", "0")

'        Dim daSql As New SqlClient.SqlDataAdapter(sqlCmd)
'        Dim dt As New DataTable
'        daSql.Fill(dt)

'        Dim dv As New DataView(dt)
'        dv.RowFilter = "DocumentId > 0"

'        ddlDocuments.DataSource = dv
'        ddlDocuments.DataTextField = "DocumentName"
'        ddlDocuments.DataValueField = "DocumentId"
'        ddlDocuments.DataBind()

'    End Function

'    Private Function LoadAttributes()

'        Try

'            mconSqlCompany = Functions.BuildConnection(Me.mobjUser)

'            Dim Attributes As DataTable = Functions.GetAttributesByDocumentId(ddlDocuments.SelectedValue, Me.mobjUser.ImagesUserId, mconSqlCompany)
'            dgAttributes.DataSource = Attributes
'            dgAttributes.DataBind()

'        Catch ex As Exception

'        Finally

'            ReturnControls.Add(dgAttributes)

'            If mconSqlCompany.State <> ConnectionState.Closed Then
'                mconSqlCompany.Close()
'                mconSqlCompany.Dispose()
'            End If

'        End Try

'    End Function

'    Private Sub ddlDocuments_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDocuments.SelectedIndexChanged
'        LoadAttributes()
'    End Sub

'    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAdd.Click

'        Dim inDirection As String = "IN"
'        Dim outDirection As String = "IN"

'        Try

'            mconSqlSynchNotifications = Functions.BuildConnection(Functions.ConnectionType.Notifications_DataSynch)

'            Dim Sync As New DataSync(mconSqlSynchNotifications, False)

'            Dim direction As String
'            If rdoIn.Checked Then
'                direction = inDirection
'            Else
'                direction = outDirection
'            End If

'            Dim daAdd, hostAdd, daUpdate, hostUpdate As Boolean

'            If direction = inDirection Then
'                If chkMissingData.Checked Then daAdd = True
'                If chkUpdateData.Checked Then daUpdate = True
'            End If

'            If direction = outDirection Then
'                If chkMissingData.Checked Then hostAdd = True
'                If chkUpdateData.Checked Then hostUpdate = True
'            End If

'            Dim detailJobId As Integer = Sync.DIJobInsert(chkEnabled.Checked, ddlJobType.SelectedValue, direction, JobName.Text, 0, ddlDocuments.SelectedValue, 0, daAdd, hostAdd, daUpdate, hostUpdate, 0, 0)

'            For Each dgi As DataGridItem In dgAttributes.Items

'                Dim Enabled As CheckBox = dgi.FindControl("Enabled")
'                Dim PrimaryKey As CheckBox = dgi.FindControl("PrimaryKey")
'                Dim LocalAttributeName As TextBox = dgi.FindControl("LocalAttributeName")
'                Dim AttributeId As Label = dgi.FindControl("AttributeId")

'                If Enabled.Checked Then
'                    Sync.DIAttributeSyncDetailInsert(detailJobId, AttributeId.Text, LocalAttributeName.Text, PrimaryKey.Checked)
'                End If

'            Next

'        Catch ex As Exception

'        Finally

'            ReturnControls.Add(dgAttributes)

'            If mconSqlSynchNotifications.State <> ConnectionState.Closed Then
'                mconSqlSynchNotifications.Close()
'                mconSqlSynchNotifications.Dispose()
'            End If

'        End Try

'    End Sub

'    Private Sub dgAttributes_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgAttributes.PreRender

'        'Build script to allow only one primary key checked
'        Dim sb As New System.Text.StringBuilder

'        For Each dgi As DataGridItem In dgAttributes.Items

'            Dim Enabled As CheckBox = dgi.FindControl("Enabled")
'            Dim PrimaryKey As CheckBox = dgi.FindControl("PrimaryKey")
'            Dim LocalAttributeName As TextBox = dgi.FindControl("LocalAttributeName")
'            Dim AttributeId As Label = dgi.FindControl("AttributeId")

'            If Not PrimaryKey Is Nothing Then
'                sb.Append("document.all." & PrimaryKey.ClientID & ".checked = false;")
'            End If

'        Next

'        For Each dgiAdd As DataGridItem In dgAttributes.Items

'            Dim PrimaryKey As CheckBox = dgiAdd.FindControl("PrimaryKey")

'            If Not PrimaryKey Is Nothing Then
'                PrimaryKey.Attributes.Add("onclick", sb.ToString)
'                PrimaryKey.Attributes.Add("oncheck", "var chkbox = document.all." & PrimaryKey.ClientID & "; if (chkbox.checked = true) { chkbox.checked = true;return true; } else { chkbox.checked = false;return true; }")
'            End If

'        Next

'        ReturnControls.Add(dgAttributes)

'    End Sub

'End Class
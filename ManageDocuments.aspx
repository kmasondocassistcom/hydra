<%@ Register TagPrefix="uc1" TagName="AdminNav" Src="Controls/AdminNav.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ManageDocuments.aspx.vb" Inherits="docAssistWeb.ManageDocuments"%>
<%@ Register TagPrefix="uc1" TagName="docHeader" Src="Controls/docHeader.ascx" %>
<%@ Register TagPrefix="uc1" TagName="TopRight" Src="Controls/TopRight.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>docAssist - Manage Documents</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="GridLayout" bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<TABLE height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TBODY>
					<TR>
						<TD width="100%" colSpan="3">
							<uc1:docHeader id="docHeader" runat="server"></uc1:docHeader>
						</TD>
					</TR>
					<TR vAlign="middle" height="100%">
						<TD class="Navigation1" height="100%" style="BORDER-RIGHT: #cccccc thin solid" vAlign="top">
							<uc1:AdminNav id="AdminNav2" runat="server"></uc1:AdminNav><IMG height="1" src="dummy.gif" width="120"></TD>
						<TD class="PageContent" vAlign="top" align="center" height="100%"><BR>
							<table cellpadding="0" cellspacing="0" border="0" width="100%">
								<tr>
									<td><IMG src="Images/manage_documents.gif"></td>
									<td align="right"></td>
								</tr>
							</table>
							<BR>
							<BR>
							<asp:linkbutton id="lnkAddDocument" runat="server" Height="24px" Width="136px">Add Document Type</asp:linkbutton>
							<asp:linkbutton id="lnkChangeDocument" runat="server" Height="24px" Width="160px">Change Document Type</asp:linkbutton>&nbsp;
							<asp:linkbutton id="lnkRemoveDocument" runat="server" Height="24px" Width="152px">Remove Document Type</asp:linkbutton>
		</form>
		</TD>
		<TD class="RightContent" height="100%">&nbsp;</TD>
		</TR>
		<TR>
			<TD colSpan="3" class="PageFooter" bgColor="#cccccc" vAlign="bottom">
				<uc1:TopRight id="docFooter" runat="server"></uc1:TopRight>
			</TD>
		</TR>
		<TR height="1">
			<TD style="BACKGROUND-COLOR: #959394" width="20"><IMG height="1" src="images/spacer.gif" width="20"></TD>
			<TD style="BACKGROUND-COLOR: #959394" width="100%"><IMG height="1" src="images/spacer.gif" width="100%"></TD>
			<TD style="BACKGROUND-COLOR: #959394" width="20"><IMG height="1" src="images/spacer.gif" width="20"></TD>
		</TR>
		</TBODY></TABLE>
	</body>
</HTML>

<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="WorkflowNotifications.aspx.vb"
    Inherits="docAssistWeb.WorkflowNotifications" %>

<%@ Register TagPrefix="uc1" TagName="docHeader" Src="Controls/docHeader.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>docAssist</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <link href="Styles.css" type="text/css" rel="stylesheet">
</head>
<body class="Prefs">
    <form id="Form1" method="post" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <br>
            <table cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
                <tr>
                    <td width="20" rowspan="6">
                    </td>
                    <td class="PrefHeader">
                        Workflow Notifications
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <img height="1" src="dummy.gif" width="17">
                        <asp:LinkButton ID="lnkAdd" runat="server" Font-Size="8pt" Font-Names="Trebuchet MS"
                            ForeColor="#BD0000">Add Notification</asp:LinkButton><img height="5" src="dummy.gif"
                                width="1">
                    </td>
                </tr>
                <tr>
                    <td>
                        <table class="Prefs" cellspacing="0" cellpadding="0" width="100%" align="center"
                            border="0">
                            <tr>
                                <td width="20">
                                    <img height="1" src="dummy.gif" width="20">
                                </td>
                                <td class="Text" nowrap align="left" width="100%">
                                    <span class="Text" id="NoRows" runat="server"></span>
                                    <asp:DataGrid ID="dgAlerts" runat="server" Font-Names="Trebuchet MS" Font-Size="9pt"
                                        AutoGenerateColumns="False" Width="100%">
                                        <AlternatingItemStyle BackColor="WhiteSmoke"></AlternatingItemStyle>
                                        <HeaderStyle Font-Size="10pt" Font-Bold="True" BackColor="Gainsboro"></HeaderStyle>
                                        <Columns>
                                            <asp:TemplateColumn>
                                                <HeaderStyle Width="30px"></HeaderStyle>
                                                <ItemStyle Wrap="False"></ItemStyle>
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="btnDelete" runat="server" ImageUrl="Images/trash.gif" CommandName="Del">
                                                    </asp:ImageButton>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Notification Type" ItemStyle-Width="120px">
                                                <HeaderStyle Wrap="False"></HeaderStyle>
                                                <ItemStyle Wrap="False"></ItemStyle>
                                                <ItemTemplate>
                                                    <asp:Label ID="AlertId" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SubscriptionID") %>'
                                                        Visible="False">
                                                    </asp:Label>
                                                    <asp:Label ID="lblType" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AlertType") %>'
                                                        Visible="False">
                                                    </asp:Label>
                                                    <asp:Label ID="lblAlertType" runat="server" Visible="True"></asp:Label>
                                                </ItemTemplate>
                                                <FooterStyle Wrap="False"></FooterStyle>
                                            </asp:TemplateColumn>
                                            <asp:BoundColumn DataField="QueueName" HeaderText="Queue">
                                                <HeaderStyle Wrap="False"></HeaderStyle>
                                                <ItemStyle Wrap="False"></ItemStyle>
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="Priority" HeaderText="Priority">
                                                <HeaderStyle Wrap="False"></HeaderStyle>
                                                <ItemStyle Wrap="False"></ItemStyle>
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="Details" HeaderText="Details">
                                                <HeaderStyle Wrap="False"></HeaderStyle>
                                                <ItemStyle Wrap="False"></ItemStyle>
                                            </asp:BoundColumn>
                                        </Columns>
                                    </asp:DataGrid>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>

            <script language="javascript">

                function disableRealTime() {
                    document.getElementById('rdoRealTime').disabled = true;
                    document.getElementById('rdoRealTime').checked = false;
                    document.getElementById('rdoDaily').checked = true;
                }

                function enableRealTime() {
                    document.getElementById('rdoRealTime').disabled = false;
                }

                function disableWeekly() {
                    document.getElementById('Weekly').disabled = true;
                }
		
            </script>

        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>

Imports System.Web.Services
Imports Accucentric.docAssist

<System.Web.Services.WebService(Namespace:="http://tempuri.org/docAssistWeb1/UserAdmin")> _
Public Class UserAdmin
    Inherits System.Web.Services.WebService

    Private mobjUser As Accucentric.docAssist.Web.Security.User

#Region " Web Services Designer Generated Code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Web Services Designer.
        InitializeComponent()

        'Add your own initialization code after the InitializeComponent() call

    End Sub

    'Required by the Web Services Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Web Services Designer
    'It can be modified using the Web Services Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        components = New System.ComponentModel.Container
    End Sub

    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        'CODEGEN: This procedure is required by the Web Services Designer
        'Do not modify it using the code editor.
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

#End Region

    <WebMethod(EnableSession:=True)> _
    Public Function AddAttribute(ByVal intAttributeId As Integer, ByVal strAttributeName As String, ByVal strDataType As String, _
            ByVal strDataFormat As String, ByVal intLength As Integer, ByVal strAttributeList As String) As Integer

        Try
            '
            ' Validate the user and make sure they are authorized for this code
            '
            Dim Request As HttpRequest = Context.Request
            Me.mobjUser = BuildUserObject(Request.Cookies("docAssist"))

            If Request.Cookies("docAssist") Is Nothing Or Me.mobjUser Is Nothing Then
                Throw New Exception("Unauthorized use of web service.")
            ElseIf Not Me.mobjUser.SecAdmin Then
                Throw New Exception("Unauthorized use of web service.")
            End If

            Dim ds As dsConfiguration = CType(Session("Attributes"), dsConfiguration)
            Dim dtAttribute As dsConfiguration.AttributesDataTable = ds.Attributes
            Dim dtAttributesLists As dsConfiguration.AttributesListsDataTable = ds.AttributesLists
            Dim drAttribute As dsConfiguration.AttributesRow
            Dim bolNewRecord As Boolean = False
            If intAttributeId > 0 Then
                drAttribute = dtAttribute.Rows.Find(intAttributeId)                                 ' Get the attribute
            Else
                drAttribute = dtAttribute.NewRow                                                    ' Make a new attribute
                bolNewRecord = True
            End If
            '
            ' Update the attribute row data
            drAttribute.AttributeName = strAttributeName
            drAttribute.AttributeDataType = strDataType
            drAttribute.DataFormat = strDataFormat
            drAttribute.Length = intLength
            If bolNewRecord Then dtAttribute.Rows.Add(drAttribute)
            '
            ' Add the list
            '
            If strDataType = "List" Then
                '
                ' Delete the existing list
                Dim dv As New DataView(dtAttributesLists, dtAttribute.AttributeIDColumn.ColumnName & " = " & intAttributeId.ToString(), _
                    Nothing, DataViewRowState.CurrentRows)
                For Each dvr As DataRowView In dv
                    Dim drItem As dsConfiguration.AttributesListsRow = dvr.Row
                    drItem.Delete()
                Next
                '
                ' Add the new list
                Dim strList() As String = strAttributeList.Split(Chr(10))
                For Each strListItem As String In strList
                    Dim drListItem As dsConfiguration.AttributesListsRow = dtAttributesLists.NewRow
                    drListItem.AttributeID = intAttributeId
                    drListItem.ValueDescription = strListItem
                    dtAttributesLists.Rows.Add(drListItem)
                Next
            End If
            '
            ' Save the changes back to the dataset
            '
            Session("Attributes") = ds
        Catch ex As Exception
            Throw ex
        Finally

        End Try


    End Function

    <WebMethod(EnableSession:=True)> _
    Public Function AttributeProperties(ByVal intAttributeId As Integer) As String
        Dim strReturn As String
        Dim conSql As SqlClient.SqlConnection
        Dim daAttribute, daList As SqlClient.SqlDataAdapter
        Try
            '
            ' Validate the user and make sure they are authorized for this code
            '
            Dim ds As New DataSet("Attributes")
            Dim Request As HttpRequest = Context.Request
            Me.mobjUser = BuildUserObject(Request.Cookies("docAssist"))

            If Request.Cookies("docAssist") Is Nothing Or Me.mobjUser Is Nothing Then
                Throw New Exception("Unauthorized use of web service.")
            ElseIf Not Me.mobjUser.SecAdmin Then
                Throw New Exception("Unauthorized use of web service.")
            End If
            '
            ' Get the values for the attributeId
            '
            Dim dsConfig As dsConfiguration = CType(Session("Attributes"), dsConfiguration)
            Dim drConfigAttributes As dsConfiguration.AttributesRow = dsConfig.Attributes.Rows.Find(intAttributeId)
            Dim dtAttributes As New Data.Images.Attributes("Attributes")
            Dim dtAttributesLists As New Data.Images.AttributesLists("AttributesLists")

            If Not drConfigAttributes Is Nothing Then
                Dim dr As Data.Images.AttributesRow = dtAttributes.NewRow
                dr.AttributeId = drConfigAttributes.AttributeID
                dr.AttributeName = drConfigAttributes.AttributeName
                dr.AttributeDataType = IIf(drConfigAttributes("AttributeDataType") Is System.DBNull.Value, "String", drConfigAttributes("AttributeDataType"))
                dr.AttributeDataFormat = IIf(drConfigAttributes("DataFormat") Is System.DBNull.Value, "", drConfigAttributes("DataFormat"))
                dr.Length = IIf(drConfigAttributes("Length") Is System.DBNull.Value, 255, drConfigAttributes("Length"))
                dtAttributes.Rows.Add(dr)
                Dim drConfigList() As dsConfiguration.AttributesListsRow = drConfigAttributes.GetChildRows("AttributesAttributesLists")
                For Each drListItem As dsConfiguration.AttributesListsRow In drConfigList
                    If drListItem.RowState <> DataRowState.Deleted Then
                        Dim drList As Data.Images.AttributesListsRow = dtAttributesLists.NewRow
                        drList.AttributeID = drListItem.AttributeID
                        drList.AttributeListId = drListItem.AttributeListId
                        drList.ValueDescription = Server.HtmlEncode(drListItem.ValueDescription)
                        dtAttributesLists.Rows.Add(drList)
                    End If
                Next

            End If
            '
            ' Add the data table to the database
            '
            ds.Tables.AddRange(New DataTable() {dtAttributes, dtAttributesLists})
            '
            ' Read the dataset into memory for XML
            '
            Dim memStream As New System.IO.MemoryStream
            ds.WriteXml(memStream)
            memStream.Seek(0, IO.SeekOrigin.Begin)
            Dim readStream As New System.IO.StreamReader(memStream)
            strReturn = readStream.ReadToEnd()

        Catch ex As Exception
            Throw ex
        Finally
            If Not conSql Is Nothing Then
                If Not conSql.State = ConnectionState.Closed Then conSql.Close()
            End If
        End Try
        Return strReturn

    End Function

    <WebMethod()> _
    Public Function CabinetFolders(ByVal intCabinetId As Integer) As String
        Dim strReturn As String
        Dim conSql As SqlClient.SqlConnection
        Dim daAssigned, daAvailable As SqlClient.SqlDataAdapter
        Try
            Dim ds As New DataSet("CabinetFolders")
            Dim Request As HttpRequest = Context.Request
            Me.mobjUser = BuildUserObject(Request.Cookies("docAssist"))

            If Request.Cookies("docAssist") Is Nothing Or Me.mobjUser Is Nothing Then
                Throw New Exception("Unauthorized use of web service.")
            ElseIf Not Me.mobjUser.SecAdmin Then
                Throw New Exception("Unauthorized use of web service.")
            End If

            conSql = Functions.BuildConnection(Me.mobjUser)
            Dim dtCabinetFolders As New DataTable("Assigned")
            dtCabinetFolders.Columns.Add(New DataColumn("FolderId", GetType(System.String)))
            dtCabinetFolders.Columns.Add(New DataColumn("FolderName", GetType(System.String)))

            Dim dtFolders As New DataTable("Available")
            dtFolders.Columns.Add(New DataColumn("FolderId", GetType(System.String)))
            dtFolders.Columns.Add(New DataColumn("FolderName", GetType(System.String)))

            Dim cmdSqlAssigned As New SqlClient.SqlCommand("SELECT FolderId, FolderName FROM CabinetFolders c WHERE c.CabinetId = @CabinetId ORDER BY FolderName", conSql)
            cmdSqlAssigned.CommandTimeout = DEFAULT_COMMAND_TIMEOUT
            cmdSqlAssigned.Parameters.Add("@CabinetId", intCabinetId)

            Dim cmdSqlAvailable As New SqlClient.SqlCommand("SELECT FolderId, FolderName FROM CabinetFolders c WHERE isnull(c.CabinetId, 0) = 0 ORDER BY FolderName", conSql)
            cmdSqlAvailable.CommandTimeout = DEFAULT_COMMAND_TIMEOUT

            daAssigned = New SqlClient.SqlDataAdapter(cmdSqlAssigned)
            daAvailable = New SqlClient.SqlDataAdapter(cmdSqlAvailable)

            conSql.Open()
            daAssigned.Fill(dtCabinetFolders)
            daAvailable.Fill(dtFolders)
            conSql.Close()
            ds.Tables.AddRange(New DataTable() {dtCabinetFolders, dtFolders})
            '
            ' Read the dataset into an memory for XML
            '
            Dim memStream As New System.IO.MemoryStream
            ds.WriteXml(memStream)
            memStream.Seek(0, IO.SeekOrigin.Begin)
            Dim readStream As New System.IO.StreamReader(memStream)
            strReturn = readStream.ReadToEnd()

        Catch ex As Exception
            Throw ex

        Finally
            daAssigned.Dispose()
            daAvailable.Dispose()
            If Not conSql.State = ConnectionState.Closed Then conSql.Close()
            conSql.Dispose()
        End Try

        Return strReturn

    End Function

    <WebMethod()> _
    Public Function DocumentAttributes(ByVal intDocumentId As Integer) As String

        Dim strReturn As String
        Dim conSql As SqlClient.SqlConnection
        Dim daAssigned, daAvailable As SqlClient.SqlDataAdapter
        Try
            Dim ds As New DataSet("DocumentAttributes")
            Dim Request As HttpRequest = Context.Request
            Me.mobjUser = BuildUserObject(Request.Cookies("docAssist"))

            If Request.Cookies("docAssist") Is Nothing Or Me.mobjUser Is Nothing Then
                Throw New Exception("Unauthorized use of web service.")
            ElseIf Not Me.mobjUser.SecAdmin Then
                Throw New Exception("Unauthorized use of web service.")
            End If

            conSql = Functions.BuildConnection(Me.mobjUser)
            Dim dtDocumentAttributes As New Data.Images.Attributes
            dtDocumentAttributes.TableName = "Assigned"
            Dim dtAvailableAttributes As New Data.Images.Attributes
            dtAvailableAttributes.TableName = "Available"

            Dim cmdSqlAssigned As New SqlClient.SqlCommand("SELECT Attributes.AttributeId, AttributeName FROM Attributes INNER JOIN DocumentAttribute da ON Attributes.AttributeId = da.AttributeId WHERE da.DocumentId = @DocumentId ORDER BY AttributeDisplayOrder", conSql)
            cmdSqlAssigned.CommandTimeout = DEFAULT_COMMAND_TIMEOUT
            cmdSqlAssigned.Parameters.Add("@DocumentId", intDocumentId)

            Dim cmdSqlAvailable As New SqlClient.SqlCommand("SELECT AttributeId, AttributeName FROM Attributes WHERE NOT EXISTS (SELECT * FROM DocumentAttribute WHERE DocumentId = @DocumentId AND Attributes.AttributeId = DocumentAttribute.AttributeId) ORDER BY AttributeName", conSql)
            cmdSqlAvailable.CommandTimeout = DEFAULT_COMMAND_TIMEOUT
            cmdSqlAvailable.Parameters.Add("@DocumentId", intDocumentId)

            daAssigned = New SqlClient.SqlDataAdapter(cmdSqlAssigned)
            daAvailable = New SqlClient.SqlDataAdapter(cmdSqlAvailable)

            conSql.Open()
            daAssigned.Fill(dtDocumentAttributes)
            daAvailable.Fill(dtAvailableAttributes)
            conSql.Close()
            ds.Tables.AddRange(New DataTable() {dtDocumentAttributes, dtAvailableAttributes})
            '
            ' Read the dataset into an memory for XML
            '
            Dim memStream As New System.IO.MemoryStream
            ds.WriteXml(memStream)
            memStream.Seek(0, IO.SeekOrigin.Begin)
            Dim readStream As New System.IO.StreamReader(memStream)
            strReturn = readStream.ReadToEnd()

        Catch ex As Exception
            Throw ex

        Finally
            daAssigned.Dispose()
            daAvailable.Dispose()
            If Not conSql.State = ConnectionState.Closed Then conSql.Close()
            conSql.Dispose()
        End Try

        Return strReturn

    End Function

    <WebMethod()> _
    Public Function FolderDocuments(ByVal intFolderId As Integer) As String
        Dim strReturn As String
        Dim conSql As SqlClient.SqlConnection
        Dim daAssigned, daAvailable As SqlClient.SqlDataAdapter
        Try
            Dim ds As New DataSet("FolderDocuments")
            Dim Request As HttpRequest = Context.Request
            Me.mobjUser = BuildUserObject(Request.Cookies("docAssist"))

            If Request.Cookies("docAssist") Is Nothing Or Me.mobjUser Is Nothing Then
                Throw New Exception("Unauthorized use of web service.")
            ElseIf Not Me.mobjUser.SecAdmin Then
                Throw New Exception("Unauthorized use of web service.")
            End If

            conSql = Functions.BuildConnection(Me.mobjUser)
            Dim dtDocumentAttributes As New DataTable
            dtDocumentAttributes.TableName = "Assigned"
            Dim dtAvailableAttributes As New DataTable
            dtAvailableAttributes.TableName = "Available"

            Dim cmdSqlAssigned As New SqlClient.SqlCommand("SELECT DocumentId, DocumentName FROM Documents WHERE EXISTS (SELECT * FROM DocumentLocations WHERE FolderId = @FolderId AND Documents.DocumentId = DocumentLocations.DocumentId) ORDER BY DocumentName", conSql)
            cmdSqlAssigned.CommandTimeout = DEFAULT_COMMAND_TIMEOUT
            cmdSqlAssigned.Parameters.Add("@FolderId", intFolderId)

            Dim cmdSqlAvailable As New SqlClient.SqlCommand("SELECT DocumentId, DocumentName FROM Documents WHERE NOT EXISTS (SELECT * FROM DocumentLocations WHERE FolderId = @FolderId AND Documents.DocumentId = DocumentLocations.DocumentId) ORDER BY DocumentName", conSql)
            cmdSqlAvailable.CommandTimeout = DEFAULT_COMMAND_TIMEOUT
            cmdSqlAvailable.Parameters.Add("@FolderId", intFolderId)

            daAssigned = New SqlClient.SqlDataAdapter(cmdSqlAssigned)
            daAvailable = New SqlClient.SqlDataAdapter(cmdSqlAvailable)

            conSql.Open()
            daAssigned.Fill(dtDocumentAttributes)
            daAvailable.Fill(dtAvailableAttributes)
            conSql.Close()
            ds.Tables.AddRange(New DataTable() {dtDocumentAttributes, dtAvailableAttributes})
            '
            ' Read the dataset into an memory for XML
            '
            Dim memStream As New System.IO.MemoryStream
            ds.WriteXml(memStream)
            memStream.Seek(0, IO.SeekOrigin.Begin)
            Dim readStream As New System.IO.StreamReader(memStream)
            strReturn = readStream.ReadToEnd()

        Catch ex As Exception
            Throw ex

        Finally
            daAssigned.Dispose()
            daAvailable.Dispose()
            If Not conSql.State = ConnectionState.Closed Then conSql.Close()
            conSql.Dispose()
        End Try

        Return strReturn
    End Function

    <WebMethod(EnableSession:=True)> _
    Public Function SetUser(ByVal intEditUser As Integer) As Boolean
        Dim bolReturn As Boolean = False
        Try
            Dim Request As HttpRequest = Context.Request
            Me.mobjUser = BuildUserObject(Request.Cookies("docAssist"))

            If Request.Cookies("docAssist") Is Nothing Or Me.mobjUser Is Nothing Then
                Throw New Exception("Access denied. Error creating user object.")
            ElseIf Not (Me.mobjUser.SecAdmin = True Or Me.mobjUser.SysAdmin = True) Then
                Throw New Exception("Access denied. You do not have rights to edit users.")
            End If

            Session("EditUser") = intEditUser

            bolReturn = True

        Catch ex As Exception
            Throw ex
        End Try

        Return bolReturn

    End Function

End Class

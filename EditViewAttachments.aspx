<%@ Page Language="vb" AutoEventWireup="false" Codebehind="EditViewAttachments.aspx.vb" Inherits="docAssistWeb.EditViewAttachments"%>
<%@ Register TagPrefix="radu" Namespace="Telerik.WebControls" Assembly="RadUpload" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>docAssist - Add Attachments</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<TABLE class="PageContent" id="Table1" height="600" cellSpacing="0" cellPadding="0" width="960"
				border="0"> <!-- Parent grey container -->
				<TR>
					<TD vAlign="top">
						<DIV class="Container">
							<DIV class="north">
								<DIV class="east">
									<DIV class="south">
										<DIV class="west">
											<DIV class="ne">
												<DIV class="se">
													<DIV class="sw">
														<DIV class="nw">
															<TABLE class="PageContent" id="tblMain" height="100%" cellSpacing="0" cellPadding="0" width="95%"
																border="0">
																<TR>
																	<TD noWrap align="left"><SPAN class="Heading">Attachment Details</SPAN></TD>
																</TR>
																<tr>
																	<td vAlign="top" align="left">
																		<table cellSpacing="0" cellPadding="0" width="100%" border="0">
																			<tr>
																				<td noWrap><IMG height="1" src="dummy.gif" width="7"></td>
																				<td noWrap align="right"><span class="Text" id="FileName" runat="server">Filename:&nbsp;
																					</span>
																				</td>
																				<td noWrap><asp:label id="lblFileName" runat="server" CssClass="Text"></asp:label></td>
																				<td align="right" width="100%"><span class="Text" id="CurrentVersion" runat="server">Current 
																						Version:&nbsp;<asp:label id="lblCurrentVersion" runat="server" CssClass="Text" Visible="True"></asp:label></span>
																				</td>
																			</tr>
																			<tr>
																				<td noWrap><IMG height="1" src="dummy.gif" width="7"></td>
																				<td noWrap align="right"><span class="Text" id="Title" runat="server">Title:&nbsp; </span>
																				</td>
																				<td noWrap><asp:label id="lblTitle" runat="server" CssClass="Text" Visible="False"></asp:label><asp:textbox id="txtTitle" runat="server" Visible="False" Font-Size="8pt" Font-Names="Trebuchet MS"
																						Width="300px" MaxLength="255"></asp:textbox></td>
																				<td width="100%" align="right">
																					<asp:Label id="lblModifyUser" runat="server" CssClass="Text"></asp:Label></td>
																			</tr>
																			<tr>
																				<td noWrap><IMG height="1" src="dummy.gif" width="7"></td>
																				<td noWrap align="right"><span class="Text" id="Description" runat="server">Description:&nbsp;
																					</span>
																				</td>
																				<td noWrap><asp:label id="lblComments" runat="server" CssClass="Text" Visible="False"></asp:label><asp:textbox id="txtComments" runat="server" Visible="False" Font-Size="8pt" Font-Names="Trebuchet MS"
																						Width="300px" MaxLength="255"></asp:textbox></td>
																				<td width="100%" align="right">
																					<asp:Label CssClass="Text" id="lblModifyDate" runat="server"></asp:Label></td>
																			</tr>
																			<tr>
																				<td noWrap><IMG height="1" src="dummy.gif" width="7"></td>
																				<td noWrap align="right"><span class="Text" id="Tags" runat="server">Tags:&nbsp; </span>
																				</td>
																				<td noWrap><asp:label id="lblTags" runat="server" CssClass="Text" Visible="False"></asp:label><asp:textbox id="txtTags" runat="server" Visible="False" Font-Size="8pt" Font-Names="Trebuchet MS"
																						Width="300px" MaxLength="255"></asp:textbox></td>
																				<td width="100%" align="right">
																					<asp:Label id="lblHeaderComments" runat="server" CssClass="Text"></asp:Label></td>
																			</tr>
																		</table>
																		<br>
																		<TABLE id="tblCheckInOut" cellSpacing="0" cellPadding="0" width="100%" border="0" runat="server">
																			<TR>
																				<TD noWrap><IMG height="1" src="dummy.gif" width="7"></TD>
																				<TD noWrap align="left" width="100%"><asp:label id="lblCheckOutStatus" runat="server" CssClass="Text"></asp:label></TD>
																			</TR>
																			<TR>
																				<TD noWrap><IMG height="1" src="dummy.gif" width="7"></TD>
																				<TD noWrap align="left"><asp:imagebutton id="btnCheckOut" runat="server" Visible="False" ImageUrl="Images/checkout.gif"></asp:imagebutton><asp:imagebutton id="btnCancelCheckOut" runat="server" Visible="False" ImageUrl="Images/cancelcheckout.gif"></asp:imagebutton></TD>
																			</TR>
																		</TABLE>
																		<br>
																		<table id="Upload" cellSpacing="0" cellPadding="0" width="100%" border="0" runat="server">
																			<tr>
																				<td vAlign="top" noWrap><span class="Text" id="UploadDirections" runat="server">To 
																						upload a newer version of this file select it below and click Save.</span>
																					<BR>
																					<INPUT id="txtFilename" style="FONT-SIZE: 8pt; FONT-FAMILY: 'Trebuchet MS'" type="file"
																						size="40" name="txtFilename" runat="server">
																					<asp:checkbox id="chkNewVersion" runat="server" CssClass="Text" Text="Create New Version"></asp:checkbox>&nbsp;
																					<span class="Text" id="CommentLabel" runat="server">Comments:</span>
																					<asp:textbox id="txtVersionComments" runat="server" CssClass="DisabledTextbox" Font-Size="8pt"
																						Font-Names="Trebuchet MS" Width="380px" MaxLength="255" Enabled="False"></asp:textbox></SPAN></td>
																			</tr>
																		</table>
																		<BR>
																		<div style="OVERFLOW: auto; WIDTH: 100%; HEIGHT: 230px" id="VersionsTable">
																			<span class="Text" id="VersionGrid">
																				<asp:Label id="lblNoVersions" runat="server" CssClass="Text" Visible="False"></asp:Label>
																				<asp:datagrid id="dgAttachmentVersionDetail" runat="server" CssClass="SearchGrid" Width="100%"
																					AutoGenerateColumns="False">
																					<Columns>
																						<asp:TemplateColumn HeaderText="Filename">
																							<HeaderStyle Width="130px"></HeaderStyle>
																							<ItemTemplate>
																								<asp:Label id="lblDetailVersionId" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.VersionId") %>' Visible=False>
																								</asp:Label>
																								<ASP:LINKBUTTON id="lnkLoad" runat="server" CommandName="Select">
																									<asp:Label id="lblDetailFileName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FileName") %>'>
																									</asp:Label>
																								</ASP:LINKBUTTON>
																							</ItemTemplate>
																						</asp:TemplateColumn>
																						<asp:TemplateColumn HeaderText="Comments">
																							<HeaderStyle Width="200px"></HeaderStyle>
																							<ItemTemplate>
																								<asp:Label id="lblDetailComments" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Comments") %>'>
																								</asp:Label>
																							</ItemTemplate>
																						</asp:TemplateColumn>
																						<asp:TemplateColumn HeaderText="Version">
																							<HeaderStyle Width="75px"></HeaderStyle>
																							<ItemTemplate>
																								<asp:Label id="lblDetailVersion" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.VersionNum") %>'>
																								</asp:Label>
																							</ItemTemplate>
																						</asp:TemplateColumn>
																						<asp:TemplateColumn HeaderText="Size">
																							<HeaderStyle Width="75px"></HeaderStyle>
																							<ItemTemplate>
																								<asp:Label id="lblDetailSize" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Size") %>'>
																								</asp:Label>
																							</ItemTemplate>
																						</asp:TemplateColumn>
																						<asp:TemplateColumn HeaderText="User">
																							<HeaderStyle Width="70px"></HeaderStyle>
																							<ItemTemplate>
																								<asp:Label id="lblUser" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.UserName") %>'>
																								</asp:Label>
																							</ItemTemplate>
																						</asp:TemplateColumn>
																					</Columns>
																				</asp:datagrid>
																			</span>
																		</div>
																		<table cellSpacing="0" cellPadding="0" width="100%" border="0">
																			<tr>
																				<td>
																					&nbsp;</td>
																			</tr>
																			<tr>
																				<td align="right"><img src="dummy.gif" height="140" width="1" id="highSpace">&nbsp;
																					<BR>
																					<asp:Label id="lblError" runat="server" CssClass="ErrorText"></asp:Label>&nbsp;
																					<asp:imagebutton id="btnCancel" runat="server" ImageUrl="Images/smallred_cancel.gif"></asp:imagebutton>&nbsp;<asp:imagebutton id="btnSave" runat="server" ImageUrl="Images/smallred_save.gif"></asp:imagebutton></td>
																			</tr>
																		</table>
																	</td>
																</tr>
															</TABLE>
														</DIV>
													</DIV>
												</DIV>
											</DIV>
										</DIV>
									</DIV>
								</DIV>
							</DIV>
						</DIV>
					</TD>
				</TR>
			</TABLE>
			<DIV></DIV>
			<DIV></DIV>
			<DIV></DIV>
			<DIV></DIV>
			<DIV></DIV>
			<DIV></DIV>
			<DIV></DIV>
			<DIV></DIV>
			<DIV>&nbsp;</DIV>
			</TD></TR></TABLE></form>
		<script language="javascript">

			function RadProgressManagerOnClientProgressStarted()
			{
				if (document.all.txtFilename.value != '')
				{
					document.all.VersionsTable.style.display='none';
					<%= radProgress.ClientID %>.Show();
				}
			}
			
			function toggleComments()
			{
				if (document.all.chkNewVersion.checked == true)
				{
					document.all.txtVersionComments.disabled = false;	
					document.all.txtVersionComments.className = '';	
				}
				else
				{
					document.all.txtVersionComments.value = '';
					document.all.txtVersionComments.disabled = true;
					document.all.txtVersionComments.className = 'DisabledTextbox';	
				}
			}

		</script>
	</body>
</HTML>

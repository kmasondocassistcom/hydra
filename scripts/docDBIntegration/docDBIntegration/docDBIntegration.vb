


Public Class docDBIntegration

#Region " Declarations "

    Private Logging As New EventLog
    Private intInterval As Integer
    Private strWebServiceURL As String
    Private strPreSharedKey As String
    Private dsJobInfo As DataSet
    Public Const WSTIMEOUT As Integer = 120000
    Public Const SYSTEMSHAREDKEY As String = "vxiRQkm1hFFECZUgwQgD38QPGfbggIwnca8y6t0uYuiD7e4wQul31tqohZZvyXWm"
    Private sAccountID As String
#End Region

    Protected Overrides Sub OnStart(ByVal args() As String)
        ' Add code here to start your service. This method should set things
        ' in motion so your service can do its work.
        Try
            LogToFile("docAssist Integration service starting.")

            intInterval = CInt(ReadSetting("IntervalSecs"))
            Timer.Interval = intInterval
            LogToFile("Service interval set to " & intInterval.ToString & " seconds.")
            intInterval += intInterval * 1000

            strWebServiceURL = ReadSetting("ServiceURL")
            LogToFile("Service URL set to " & strWebServiceURL)

            strPreSharedKey = ReadSetting("PreSharedKey")
            LogToFile("Service preshared key set to " & strPreSharedKey)

            '#If DEBUG Then
            'Timer.Enabled = False
            'ProcessIntegration()
            'Exit Sub
            '#Else
            Timer.Enabled = True
            '#End If

        Catch ex As Exception
            LogToFile(ex.Message & vbCr & ex.StackTrace)
            'LogToDocAssist()
        End Try
    End Sub


    Private Sub ProcessIntegration()
        'Connect to webservice and validate presharedkey

        Try
            'Authenticate
            If ConnectWithPreShare() = False Then
                Throw New Exception("Service was unable to connect.")
            End If

            'Get Pending Job Info
            dsJobInfo = PendingJobInfo()

            Dim iCount As Integer = LogPendingJobs(dsJobInfo)

            If iCount > 0 Then
                ProcessPendingJobs(dsJobInfo)
            Else
                LogToFile("No jobs to process.")
            End If



        Catch ex As Exception
            LogToFile(ex.Message)
        Finally
            'Log that we are done.
            LogToFile("Service task completed." & vbCrLf & vbCrLf)
        End Try



    End Sub

    Private Sub ProcessPendingJobs(ByRef dsJobInfo As DataSet)
        Try
            'Process each job one at a time
            Dim dr As DataRow
            For Each dr In dsJobInfo.Tables(0).Rows
                Try
                    LogToFile("Process started for: " & dr("JobName") & "[" & dr("JobID").ToString & ":" & dr("JobDetailID").ToString & "] ")
                    LogToFile("Sync Direction: " & "[" & dr("Direction") & "]")
                    Select Case UCase(dr("JobType"))
                        Case "LOOKUPSYNC"
                            ProcessLookupSyncEntry(dr)

                        Case "ATTRIBUTESYNC"
                            ProcessAttributeSyncEntry(dr)
                        Case "DISTRIBUTION"
                            ProcessDistributionSyncEntry(dr)

                    End Select

                Catch ex As Exception
                    LogToFile(dr("JobName") & "[" & dr("JobID").ToString & ":" & dr("JobDetailID").ToString & "] " & ex.Message, True)
                End Try

            Next
        Catch ex As Exception
            LogToFile(ex.Message, True)
        End Try


    End Sub


    Private Sub ProcessAttributeSyncEntry(ByVal dr As DataRow)

        Try
            Select Case UCase(dr("Direction"))
                Case "OUT"
                    ProcessAttributeSyncHost(dr)
                Case "IN"
                    ProcessAttributeSyncDA(dr)
                    'Case "BOTH"
                    '    ProcessAttributeSyncHost(dr)
                '    ProcessAttributeSyncDA(dr)

            End Select

        Catch ex As Exception
            LogToFile(ex.Message, True)
        End Try

    End Sub

    Private Sub ProcessDistributionSyncEntry(ByVal dr As DataRow)

        Try
            Select Case UCase(dr("Direction"))
                Case "OUT"
                    ProcessDistributionSyncHost(dr)
                Case "IN"
                    ProcessDistributionSyncDA(dr)
                    'Case "BOTH"
                    '    ProcessAttributeSyncHost(dr)
                    '    ProcessAttributeSyncDA(dr)

            End Select

        Catch ex As Exception
            LogToFile(ex.Message, True)
        End Try

    End Sub

    Private Sub ProcessLookupSyncEntry(ByVal dr As DataRow)

        Try
            Select Case UCase(dr("Direction"))
                Case "OUT"
                    ProcessLookupSyncHost(dr)
                Case "IN"
                    ProcessLookupSyncDA(dr)
                    'Case "BOTH"
                    '    ProcessLookupSyncHost(dr)
                    '   ProcessLookupSyncDA(dr)

            End Select

        Catch ex As Exception
            LogToFile(ex.Message, True)
        End Try

    End Sub

   
    Private Sub ProcessLookupSyncHost(ByVal dr As DataRow)
        'Sync Data out from docAssist to host
        'First get data from docAssist

        Try
            Dim ws As New WSdocDBIntegration.docDBIntegration

            LogToFile("Retrieving lookup list " & "[" & dr("FilterAttributeID") & "]")
            ws.Url = strWebServiceURL
            ws.Timeout = WSTIMEOUT
            Dim dsSQLInstructions As DataSet

            'Get Host data and send to webservice
            'Build SQL Query for Remote system

            'Check for existance of DSN
            If IsDSNInstalled(dr("ODBCName")) = False Then
                Throw New Exception("ODBC data source: " & dr("ODBCName") & " does not exists.")
            Else
                LogToFile("ODBC data source: " & dr("ODBCName") & " verified.")
            End If

            Dim sHostQuery As String = BuildLookupQuery(dr) 'Build Query

            If sHostQuery <> "" Then
                LogToFile("Host query: " & sHostQuery)
            Else
                Throw New Exception("Unable to build query from data object definition. [" & sHostQuery & "]")
            End If


            Dim dsHostData As DataSet = HostDataGet(dr("ODBCName"), dr("UID"), dr("PWD"), dr("CommandType"), sHostQuery)

            If dsHostData.Tables.Count > 0 Then
                LogToFile("Host query returned (" & dsHostData.Tables(0).Rows.Count.ToString & ") row(s)")
            Else
                Throw New Exception("Host did not return any rows.")
            End If

            If dsHostData.Tables.Count > 0 Then
                LogToFile("(" & dsHostData.Tables(0).Rows.Count.ToString & ") row(s) retrieved from host. [" & sHostQuery & "]")
            Else
                Throw New Exception("No lookup rows retrieved.")
            End If

            dsSQLInstructions = ws.LookupDataHostChangesGet(SYSTEMSHAREDKEY, sAccountID, dr("JobID"), dr("JobDetailID"), dsHostData)



            If dsSQLInstructions.Tables.Count > 0 Then
                If dsSQLInstructions.Tables(0).Rows.Count > 0 Then
                    LogToFile("[" & dsSQLInstructions.Tables(0).Rows.Count.ToString & "]" & "differences to process.")
                    LookupReconcileHost(dr("ODBCName"), dr("UID"), dr("PWD"), dr("CommandType"), dr, dsSQLInstructions.Tables(0))
                Else
                    'Log no changes
                    LogToFile("No differences to process.")
                End If
            Else
                'Log no changes
                LogToFile("No differences to process.")
            End If


        Catch ex As Exception
            Throw ex
        End Try


    End Sub

    Private Function IsDSNInstalled(ByVal DSNNameToCheck As String) As Boolean

        Dim ReturnValue As Short
        Dim DSNName As String
        Dim DriverName As String
        Dim DSNNameLen As Short
        Dim DriverNameLen As Short
        Dim SQLEnv As Integer 'handle to the environment
        Dim blnExists As Boolean = False


        If SQLAllocEnv(SQLEnv) <> -1 Then
            Do Until ReturnValue <> SQL_SUCCESS
                DSNName = Space(1024)
                DriverName = Space(1024)
                ReturnValue = SQLDataSources(SQLEnv, SQL_FETCH_NEXT, DSNName, 1024, DSNNameLen, DriverName, 1024, DriverNameLen)
                DSNName = DSNName.Substring(0, DSNNameLen)
                DriverName = DriverName.Substring(0, DriverNameLen)

                If DSNName = DSNNameToCheck Then blnExists = True

                If DSNName <> Space(DSNNameLen) Then
                    System.Diagnostics.Debug.WriteLine(DSNName)
                    System.Diagnostics.Debug.WriteLine(DriverName)
                End If
            Loop
        End If

        Return blnExists

    End Function


    Private Function LookupReconcileHost(ByVal ODBCName As String, ByVal UID As String, ByVal Password As String, ByVal CommandType As Integer, ByVal dr As DataRow, ByVal dtSQL As DataTable) As Boolean

        Dim ODBCConn As Data.Odbc.OdbcConnection
        ODBCConn = ODBC.ODBCConnection(ODBCName, UID, Password)
        'Dim daSql As Data.Odbc.OdbcDataAdapter

        'Dim odbcDataTable As New DataTable

        'SP= 4
        'Text = 1
        '100 = Oracle function 'i.e. select APPS.XX_DOCASSIST_DELIV_DOCS_COA('1924073') FROM DUAL



        If CommandType = "1" Then



            Dim drSQL As DataRow
            For Each drSQL In dtSQL.Rows
                Dim odbcmd As New Data.Odbc.OdbcCommand(drSQL("SQL"), ODBCConn)
                Try
                    odbcmd.ExecuteNonQuery()
                    LogToFile("SQL success: " & drSQL("SQL"))

                Catch ex As Exception
                    LogToFile("SQL: " & drSQL("SQL") & " " & ex.Message, True)
                End Try
            Next
        End If


        If CommandType = "4" Then 'SP
        End If

        Return True


    End Function

    Private Function DistributionReconcileHost(ByVal ODBCName As String, ByVal UID As String, ByVal Password As String, ByVal CommandType As Integer, ByVal dr As DataRow, ByVal dsSQL As DataSet) As Boolean

        Dim ODBCConn As Data.Odbc.OdbcConnection
        ODBCConn = ODBC.ODBCConnection(ODBCName, UID, Password)
        'Dim daSql As Data.Odbc.OdbcDataAdapter

        'Dim odbcDataTable As New DataTable

        'SP= 4
        'Text = 1
        '100 = Oracle function 'i.e. select APPS.XX_DOCASSIST_DELIV_DOCS_COA('1924073') FROM DUAL

        Dim dtSQLDelete As DataTable
        dtSQLDelete = dsSQL.Tables(0)

        Dim dtSQLInsert As DataTable
        dtSQLInsert = dsSQL.Tables(1)



        If CommandType = "1" Then

            Dim drSQLDel As DataRow
            For Each drSQLDel In dtSQLDelete.Rows
                Dim iRowsAffected As Integer = 0
                Dim odbcmd As New Data.Odbc.OdbcCommand(drSQLDel("SQL"), ODBCConn)
                Try

                    'Test for non-existence of rows
                    Dim blnRowsExist As Boolean

                    Dim sSQLCheck As String
                    sSQLCheck = drSQLDel("SQL").ToString.Replace("DELETE ", "SELECT * ")

                    Dim iANDLocation As Integer = InStr(sSQLCheck, "AND", CompareMethod.Text)
                    sSQLCheck = sSQLCheck.Substring(0, iANDLocation - 1)

                    Dim odbcmdCheck As New Data.Odbc.OdbcCommand(sSQLCheck, ODBCConn)
                    iRowsAffected = odbcmdCheck.ExecuteNonQuery()
                    LogToFile("Existence check: " & drSQLDel("SQL") & " -" & iRowsAffected.ToString & " rows(s) affected.")

                    If iRowsAffected = 0 Then
                        blnRowsExist = False
                    Else
                        blnRowsExist = True
                    End If

                    iRowsAffected = 0
                    iRowsAffected = odbcmd.ExecuteNonQuery()
                    LogToFile("SQL success: " & drSQLDel("SQL") & " -" & iRowsAffected.ToString & " rows(s) affected.")

                    If iRowsAffected > 0 Or blnRowsExist = False Then
                        Dim dvInsert As New DataView(dtSQLInsert)
                        dvInsert.RowFilter = "ImageID = '" & drSQLDel("ImageID").ToString & "'"

                        Dim dvrInsert As DataRowView

                        For Each dvrInsert In dvInsert
                            Dim odbcmdInsert As New Data.Odbc.OdbcCommand(dvrInsert("SQL"), ODBCConn)
                            iRowsAffected = odbcmdInsert.ExecuteNonQuery()
                            LogToFile("SQL success: " & dvrInsert("SQL") & " -" & iRowsAffected.ToString & " rows(s) affected.")
                        Next

                    End If

                Catch ex As Exception
                    LogToFile("SQL: " & drSQLDel("SQL") & " " & ex.Message, True)
                End Try
            Next
        End If


        If CommandType = "4" Then 'SP
        End If

        Return True


    End Function

    Private Function HostDataGet(ByVal ODBCName As String, ByVal UID As String, ByVal Password As String, ByVal CommandType As Integer, ByVal SQLStatement As String) As DataSet

        Dim ODBCConn As Data.Odbc.OdbcConnection

        ODBCConn = ODBC.ODBCConnection(ODBCName, UID, Password)


        Dim odbcmd As New Data.Odbc.OdbcCommand(SQLStatement, ODBCConn)
        Dim odbcDataTable As New DataTable


        'SP= 4
        'Text = 1
        '100 = Oracle function 'i.e. select APPS.XX_DOCASSIST_DELIV_DOCS_COA('1924073') FROM DUAL



        If CommandType = "1" Then
            odbcmd.CommandType = Data.CommandType.Text


            ODBC.PopulateTable(odbcDataTable, String.Format(SQLStatement).ToString, Nothing, ODBCConn)

        End If

        If CommandType = "4" Then 'SP

            SQLStatement = "{call " + SQLStatement + " (?) }"
            odbcmd.CommandText = SQLStatement
            odbcmd.CommandType = Data.CommandType.StoredProcedure
            Dim da As New Data.Odbc.OdbcDataAdapter(odbcmd)
            odbcmd.CommandTimeout = 120
            da.Fill(odbcDataTable)
        End If

        Dim ds As New DataSet
        ds.Tables.Add(odbcDataTable)
        Return ds


    End Function

    Private Function BuildLookupQuery(ByVal dr As DataRow) As String

        Try
            'Build Query to execute on host
            Dim sSQL As String
            sSQL = ""
            If dr("HostValueColumnName") = "" Or dr("ObjectName") = "" Then
                Return ""
            End If
            If dr("HostValueColumnName") <> "" Then
                sSQL = "SELECT " & dr("HostValueColumnName")
            End If
            If dr("HostValueDescColumnName") <> "" Then
                sSQL += ", " & dr("HostValueDescColumnName")
            End If
            sSQL += " FROM " & dr("ObjectName")

            If dr("ObjectRowFilter") <> "" Then
                sSQL += " WHERE " & dr("ObjectRowFilter")
            End If

            Return sSQL


        Catch ex As Exception

            Throw ex

        End Try


    End Function

 
    Private Function BuildAttributeQuery(ByVal dr As DataRow, Optional ByVal NoWhere As Boolean = False) As String

        Try
            'Build Query to execute on host
            Dim dvColMappings As New DataView
            If dsJobInfo.Tables.Count > 1 Then
                dvColMappings = New DataView(dsJobInfo.Tables(1))
                dvColMappings.RowFilter = "JobDetailID = " & dr("JobDetailID")


                If dvColMappings.Count <= 0 Then
                    Return ""
                End If
            Else
                Return ""
            End If

            Dim sSQL As String
            sSQL = ""
            If dr("ObjectName") = "" Then
                Return ""
            End If

            sSQL = "SELECT "
            Dim iTotalRows As Integer = dvColMappings.Count
            Dim x As Integer

            For x = 0 To dvColMappings.Count - 1
                sSQL += dvColMappings.Item(x).Row("ColumnName")
                If x < iTotalRows - 1 Then
                    sSQL += ", "
                Else
                    sSQL += " "
                End If


            Next


            sSQL += " FROM " & dr("ObjectName")

            If NoWhere = True Then
                Return sSQL
            End If


            'Build sync range
            If dr("DateCompareColumnName") <> "" Then
                If dr("SyncRangeMins") > 0 Then
                    'Get server time 
                    Dim dtCurrentDateTime As DateTime = System.DateTime.Now
                    LogToFile("Sync range minutes: " & dr("SyncRangeMins"))
                    'Apply sync minutes
                    dtCurrentDateTime = DateAdd(DateInterval.Minute, dr("SyncRangeMins") * -1, dtCurrentDateTime)
                    Dim strCurrentDateTime As String = dtCurrentDateTime.Year.ToString & "-" & dtCurrentDateTime.Month & "-" & dtCurrentDateTime.Day & " " & dtCurrentDateTime.Hour & ":" & dtCurrentDateTime.Minute & ":" & dtCurrentDateTime.Second
                    sSQL += " WHERE " & dr("DateCompareColumnName") & " >= '" & strCurrentDateTime & "'"

                Else 'Use date last sync
                    Dim dtServerTime As DateTime = dr("CurrentServerTime")
                    Dim DiffFromGMT As Integer = DateDiff(DateInterval.Hour, System.DateTime.Now(), dtServerTime)
                    LogToFile("Sync range minutes: " & dr("SyncRangeMins") & " Time zone difference is " & DiffFromGMT.ToString)
                    'Apply diff from GMT to server time
                    dtServerTime = DateAdd(DateInterval.Hour, DiffFromGMT, dr("LastSyncDate"))
                    Dim strCurrentDateTime As String = dtServerTime.Year.ToString & "-" & dtServerTime.Month & "-" & dtServerTime.Day & " " & dtServerTime.Hour & ":" & dtServerTime.Minute & ":" & dtServerTime.Second

                    sSQL += " WHERE " & dr("DateCompareColumnName") & " >= '" & strCurrentDateTime & "'"

                End If
            End If

            Return sSQL


        Catch ex As Exception

            Throw ex

        End Try


    End Function

    Private Sub ProcessDistributionSyncHost(ByVal dr As DataRow)
        'Sync Data out from host to docAssist


        Try
            Dim ws As New WSdocDBIntegration.docDBIntegration

            LogToFile("Retrieving distribution data.") ' & "[" & dr("FilterAttributeID") & "]")
            ws.Url = strWebServiceURL
            ws.Timeout = WSTIMEOUT
            Dim dsSQLInstructions As DataSet

            'Get Host data and send to webservice
            'Build SQL Query for Remote system

            'Check for existance of DSN
            If IsDSNInstalled(dr("ODBCName")) = False Then
                Throw New Exception("ODBC data source: " & dr("ODBCName") & " does not exists.")
            Else
                LogToFile("ODBC data source: " & dr("ODBCName") & " verified.")
            End If

            Dim sHostQuery As String = BuildDistributionQuery(dr) 'Build Query

            If sHostQuery <> "" Then
                LogToFile("Host query: " & sHostQuery)
            Else
                Throw New Exception("Unable to build query from data object definition. [" & sHostQuery & "]")
            End If

            Dim dsHostData As DataSet = HostDataGet(dr("ODBCName"), dr("UID"), dr("PWD"), dr("CommandType"), sHostQuery)

            If dsHostData.Tables.Count > 0 Then
                LogToFile("Host query returned (" & dsHostData.Tables(0).Rows.Count.ToString & ") row(s)")
            Else
                Throw New Exception("Host did not return any rows.")
            End If

            If dsHostData.Tables.Count > 0 Then
                LogToFile("(" & dsHostData.Tables(0).Rows.Count.ToString & ") row(s) retrieved from host. [" & sHostQuery & "]")
            Else
                Throw New Exception("No rows retrieved.")
            End If

            dsSQLInstructions = ws.DistributionHostChangesGet(SYSTEMSHAREDKEY, sAccountID, dr("JobID"), dr("JobDetailID"), dsHostData)

            If dsSQLInstructions.Tables.Count > 0 Then
                If dsSQLInstructions.Tables(0).Rows.Count > 0 Then
                    LogToFile("[" & dsSQLInstructions.Tables(0).Rows.Count.ToString & "]" & "differences to process.")
                    LogToFile("[" & dsSQLInstructions.Tables(1).Rows.Count.ToString & "]" & "detail records to process.")
                    DistributionReconcileHost(dr("ODBCName"), dr("UID"), dr("PWD"), dr("CommandType"), dr, dsSQLInstructions)
                Else
                    'Log no changes
                    LogToFile("No differences to process.")
                End If
            Else
                'Log no changes
                LogToFile("No differences to process.")
            End If





        Catch ex As Exception
            Throw ex
        End Try



    End Sub
    Private Sub ProcessAttributeSyncHost(ByVal dr As DataRow)
        'Sync attributes out from docAssist to host
        Try
            Dim ws As New WSdocDBIntegration.docDBIntegration

            LogToFile("Retrieving attribute changes from host.")
            ws.Url = strWebServiceURL
            ws.Timeout = WSTIMEOUT
            Dim dsSQLInstructions As DataSet

            'Check for existance of DSN
            If IsDSNInstalled(dr("ODBCName")) = False Then
                Throw New Exception("ODBC data source: " & dr("ODBCName") & " does not exists.")
            Else
                LogToFile("ODBC data source: " & dr("ODBCName") & " verified.")
            End If

            Dim sHostQuery As String = BuildAttributeQuery(dr, True) 'Build Query

            If sHostQuery <> "" Then
                LogToFile("Host query: " & sHostQuery)
            Else
                Throw New Exception("Unable to build query from data object definition. [" & sHostQuery & "]")
            End If


            Dim dsHostData As DataSet = HostDataGet(dr("ODBCName"), dr("UID"), dr("PWD"), dr("CommandType"), sHostQuery)

            If dsHostData.Tables.Count > 0 Then
                LogToFile("Host query returned (" & dsHostData.Tables(0).Rows.Count.ToString & ") row(s)")
            Else
                Throw New Exception("Host did not return any rows.")
            End If

            If dsHostData.Tables.Count > 0 Then
                LogToFile("(" & dsHostData.Tables(0).Rows.Count.ToString & ") row(s) retrieved from host. [" & sHostQuery & "]")
            Else
                Throw New Exception("No rows retrieved.")
            End If


            dsSQLInstructions = ws.AttributeDataHostChangesGet(SYSTEMSHAREDKEY, sAccountID, dr("JobID"), dr("JobDetailID"), dsHostData)


            If dsSQLInstructions.Tables.Count > 0 Then
                If dsSQLInstructions.Tables(0).Rows.Count > 0 Then
                    LogToFile("[" & dsSQLInstructions.Tables(0).Rows.Count.ToString & "]" & "differences to process.")
                    LookupReconcileHost(dr("ODBCName"), dr("UID"), dr("PWD"), dr("CommandType"), dr, dsSQLInstructions.Tables(0))
                Else
                    'Log no changes
                    LogToFile("No differences to process.")
                End If
            Else
                'Log no changes
                LogToFile("No differences to process.")
            End If

        Catch ex As Exception
            Throw ex
        End Try



    End Sub

    Private Function BuildDistributionQuery(ByVal dr As DataRow, Optional ByVal NoWhere As Boolean = False) As String

        Try
            'Build Query to execute on host
            Dim dvMappings As New DataView
            If dsJobInfo.Tables.Count > 2 Then
                dvMappings = New DataView(dsJobInfo.Tables(2))
                dvMappings.RowFilter = "JobDetailID = " & dr("JobDetailID")


                If dvMappings.Count <> 1 Then
                    Return ""
                End If
            Else
                Return ""
            End If

            Dim sSQL As String
            sSQL = ""
            If dr("ObjectName") = "" Then
                Return ""
            End If

            sSQL = "SELECT "
            If dvMappings.Item(0)("KeyColumnName") <> "" Then sSQL += dvMappings.Item(0)("KeyColumnName") & ", "
            If dvMappings.Item(0)("Col1ValueColumnName") <> "" Then sSQL += dvMappings.Item(0)("Col1ValueColumnName") & ", "
            If dvMappings.Item(0)("Col1DescColumnName") <> "" Then sSQL += dvMappings.Item(0)("Col1DescColumnName") & ", "
            If dvMappings.Item(0)("Col2ValueColumnName") <> "" Then sSQL += dvMappings.Item(0)("Col2ValueColumnName") & ", "
            If dvMappings.Item(0)("AmountColumnName") <> "" Then sSQL += dvMappings.Item(0)("AmountColumnName") & ", "
            If dvMappings.Item(0)("MemoColumnName") <> "" Then sSQL += dvMappings.Item(0)("MemoColumnName") & ", "


            'remove trailing comma
            sSQL = sSQL.TrimEnd
            sSQL = sSQL.Substring(0, sSQL.Length - 1)

            sSQL += " FROM " & dr("ObjectName")

            If NoWhere = True Then
                Return sSQL
            End If


            If dr("DateCompareColumnName") <> "" Then
                'Build sync range
                If dr("SyncRangeMins") > 0 Then
                    'Get server time 
                    Dim dtCurrentDateTime As DateTime = System.DateTime.Now
                    LogToFile("Sync range minutes: " & dr("SyncRangeMins"))
                    'Apply sync minutes
                    dtCurrentDateTime = DateAdd(DateInterval.Minute, dr("SyncRangeMins") * -1, dtCurrentDateTime)
                    Dim strCurrentDateTime As String = dtCurrentDateTime.Year.ToString & "-" & dtCurrentDateTime.Month & "-" & dtCurrentDateTime.Day & " " & dtCurrentDateTime.Hour & ":" & dtCurrentDateTime.Minute & ":" & dtCurrentDateTime.Second
                    sSQL += " WHERE " & dr("DateCompareColumnName") & " >= '" & strCurrentDateTime & "'"

                Else 'Use date last sync
                    Dim dtServerTime As DateTime = dr("CurrentServerTime")
                    Dim DiffFromGMT As Integer = DateDiff(DateInterval.Hour, System.DateTime.Now(), dtServerTime)
                    LogToFile("Sync range minutes: " & dr("SyncRangeMins") & " Time zone difference is " & DiffFromGMT.ToString)
                    'Apply diff from GMT to server time
                    dtServerTime = DateAdd(DateInterval.Hour, DiffFromGMT, dr("LastSyncDate"))
                    Dim strCurrentDateTime As String = dtServerTime.Year.ToString & "-" & dtServerTime.Month & "-" & dtServerTime.Day & " " & dtServerTime.Hour & ":" & dtServerTime.Minute & ":" & dtServerTime.Second

                    sSQL += " WHERE " & dr("DateCompareColumnName") & " >= '" & strCurrentDateTime & "'"

                End If
            End If


            Return sSQL


        Catch ex As Exception

            Throw ex

        End Try


    End Function

    Private Function CompareTables(ByVal dtSource As DataTable, ByRef dtTarget As DataTable, ByVal drJob As DataRow) As DataTable


        Dim sColumnName1 As String
        Dim sColumnName2 As String

        If drJob("Direction") = "Out" Then
            sColumnName1 = drJob("HostValueColumnName")
            sColumnName2 = drJob("HostValueDescColumnName")
        Else
            sColumnName1 = "Value"
            sColumnName2 = "ValueDescription"
        End If


        Dim dtDifferences As New DataTable
        dtDifferences.Columns.Add(New DataColumn(sColumnName1, GetType(System.String), Nothing, MappingType.Element))
        If sColumnName2 <> "" Then dtDifferences.Columns.Add(New DataColumn(sColumnName2, GetType(System.String), Nothing, MappingType.Element))
        dtDifferences.Columns.Add(New DataColumn("Action", GetType(System.String), Nothing, MappingType.Element))


        'First find missing records on host to add

        Dim drDA As DataRow
        For Each drDA In dtSource.Rows
            'Look for docAssist row in host table
            Dim dv As New DataView(dtTarget)
            dv.RowFilter = sColumnName1 & "= " & "'" & drDA(sColumnName1).ToString.Trim & "'"

            If dv.Count <= 0 Then 'not found


                Dim x As Integer = 1
                If drJob("HostAddNewRows") = True Then
                    'Add row to target
                    Dim drAdd As DataRow
                    drAdd = dtDifferences.NewRow
                    drAdd(sColumnName1) = drDA(sColumnName1)
                    If sColumnName2 <> "" Then drAdd(sColumnName2) = drDA(sColumnName2)
                    drAdd("Action") = "A"
                    dtDifferences.Rows.Add(drAdd)
                End If
            Else 'Found Match
                If drJob("HostUpdateRows") = True And sColumnName2 <> "" Then

                    If dv.Item(0)(sColumnName2).ToString.Trim <> drDA(sColumnName2).ToString.Trim Then

                        'Update Description on host
                        'Add row to target
                        Dim drAdd As DataRow
                        drAdd = dtDifferences.NewRow
                        drAdd(sColumnName1) = drDA(sColumnName1)
                        If sColumnName2 <> "" Then drAdd(sColumnName2) = drDA(sColumnName2)
                        drAdd("Action") = "U"
                        dtDifferences.Rows.Add(drAdd)

                    End If
                End If
            End If
        Next



        If drJob("HostRemoveRows") = True Then
            'Loop thru and add 

            For Each drDA In dtTarget.Rows
                Dim dv As New DataView(dtSource)
                dv.RowFilter = sColumnName1 & "= " & "'" & drDA(sColumnName1).ToString.Trim & "'"
                If dv.Count <= 0 Then
                    Dim drAdd As DataRow
                    drAdd = dtDifferences.NewRow
                    drAdd(sColumnName1) = drDA(sColumnName1)
                    If sColumnName2 <> "" Then drAdd(sColumnName2) = drDA(sColumnName2)
                    drAdd("Action") = "R"
                    dtDifferences.Rows.Add(drAdd)
                End If
            Next

        End If



        Return dtDifferences




    End Function

    Private Sub ProcessAttributeSyncDA(ByVal dr As DataRow)
        'Sync attributes out from host to docAssist
        Try
            Dim ws As New WSdocDBIntegration.docDBIntegration

            LogToFile("Retrieving attribute changes from host.")
            ws.Url = strWebServiceURL
            ws.Timeout = WSTIMEOUT
            Dim dsSQLInstructions As DataSet

            'Check for existance of DSN
            If IsDSNInstalled(dr("ODBCName")) = False Then
                Throw New Exception("ODBC data source: " & dr("ODBCName") & " does not exists.")
            Else
                LogToFile("ODBC data source: " & dr("ODBCName") & " verified.")
            End If

            'Dim sHostQuery As String = BuildAttributeQuery(dr) 'Build Query

            'Get UTC Time from system
            Dim UTCTime As System.DateTime = System.DateTime.UtcNow
            Dim sHostQuery As String = ws.BuildAttributeQuery(SYSTEMSHAREDKEY, sAccountID, dr("JobID"), dr("JobDetailID"), System.DateTime.Now, UTCTime)

            If sHostQuery <> "" Then
                LogToFile("Host query: " & sHostQuery)
            Else
                Throw New Exception("Unable to build query from data object definition. [" & sHostQuery & "]")
            End If


            Dim dsHostData As DataSet = HostDataGet(dr("ODBCName"), dr("UID"), dr("PWD"), dr("CommandType"), sHostQuery)

            If dsHostData.Tables.Count > 0 Then
                LogToFile("Host query returned (" & dsHostData.Tables(0).Rows.Count.ToString & ") row(s)")
            Else
                Throw New Exception("Host did not return any rows.")
            End If

            If dsHostData.Tables.Count > 0 Then
                LogToFile("(" & dsHostData.Tables(0).Rows.Count.ToString & ") row(s) retrieved from host. [" & sHostQuery & "]")
            Else
                Throw New Exception("No rows retrieved.")
            End If


            dsSQLInstructions = ws.AttributeDatadocAssistChangesSet(SYSTEMSHAREDKEY, sAccountID, dr("JobID"), dr("JobDetailID"), dsHostData)

            'Log dsReconciled table that is returned from web service
            LogToFile("Data updated in docAssist.")
            Dim sColumnList As String = ""
            Dim sDataList As String = ""

            Dim drRow As DataRow
            Dim dc As DataColumn
            Dim iTableNum As Integer = 1
            Dim dtLog As DataTable
            For Each dtLog In dsSQLInstructions.Tables

                For Each dc In dtLog.Columns

                    sColumnList += dc.ColumnName & ","
                Next
                If iTableNum = 1 Then LogToFile(dtLog.Rows.Count.ToString & " row(s) added to docAssist")
                If iTableNum = 2 Then LogToFile(dtLog.Rows.Count.ToString & " row(s) changed to docAssist")
                If iTableNum = 3 Then LogToFile(dtLog.Rows.Count.ToString & " row(s) removed from docAssist")
                If dtLog.Rows.Count <= 0 Then LogToFile("No rows affected.")

                LogToFile(sColumnList)


                For Each drRow In dtLog.Rows
                    sDataList = ""
                    For Each dc In dtLog.Columns
                        sDataList += drRow(dc.ColumnName) & ","
                    Next
                    LogToFile(sDataList)
                Next
            Next

        Catch ex As Exception

            Throw ex

        End Try


    End Sub

    Private Sub ProcessLookupSyncDA(ByVal dr As DataRow)
        'Sync Data out from host to docAssist


        Try
            Dim ws As New WSdocDBIntegration.docDBIntegration

            LogToFile("Retrieving lookup list " & "[" & dr("FilterAttributeID") & "]")
            ws.Url = strWebServiceURL
            ws.Timeout = WSTIMEOUT
            Dim dsSQLInstructions As DataSet

            'Get Host data and send to webservice
            'Build SQL Query for Remote system

            'Check for existance of DSN
            If IsDSNInstalled(dr("ODBCName")) = False Then
                Throw New Exception("ODBC data source: " & dr("ODBCName") & " does not exists.")
            Else
                LogToFile("ODBC data source: " & dr("ODBCName") & " verified.")
            End If

            Dim sHostQuery As String = BuildLookupQuery(dr) 'Build Query

            If sHostQuery <> "" Then
                LogToFile("Host query: " & sHostQuery)
            Else
                Throw New Exception("Unable to build query from data object definition. [" & sHostQuery & "]")
            End If


            Dim dsHostData As DataSet = HostDataGet(dr("ODBCName"), dr("UID"), dr("PWD"), dr("CommandType"), sHostQuery)

            If dsHostData.Tables.Count > 0 Then
                LogToFile("Host query returned (" & dsHostData.Tables(0).Rows.Count.ToString & ") row(s)")
            Else
                Throw New Exception("Host did not return any rows.")
            End If

            If dsHostData.Tables.Count > 0 Then
                LogToFile("(" & dsHostData.Tables(0).Rows.Count.ToString & ") row(s) retrieved from host. [" & sHostQuery & "]")
            Else
                Throw New Exception("No rows retrieved.")
            End If

            dsSQLInstructions = ws.LookupDatadocAssistChangesSet(SYSTEMSHAREDKEY, sAccountID, dr("JobID"), dr("JobDetailID"), dsHostData)



            If dsSQLInstructions.Tables.Count > 0 Then
                If dsSQLInstructions.Tables(0).Rows.Count > 0 Then
                    LogToFile("[" & dsSQLInstructions.Tables(0).Rows.Count.ToString & "]" & "differences to process.")
                    Dim drSQL As DataRow
                    For Each drSQL In dsSQLInstructions.Tables(0).Rows
                        LogToFile(drSQL("Result"), drSQL("Error"))
                    Next
                Else
                    'Log no changes
                    LogToFile("No differences to process.")
                End If
            Else
                'Log no changes
                LogToFile("No differences to process.")
            End If



        Catch ex As Exception
            Throw ex
        End Try



    End Sub

    Private Function LogPendingJobs(ByRef ds As DataSet) As Integer
        Dim iCountPendingJobs As Integer = 0
        If ds.Tables.Count >= 1 Then
            If ds.Tables(0).Rows.Count = 0 Then
                LogToFile("No pending jobs to process.")
            Else
                LogToFile("Job list retrieved.")
            End If
            Dim dr As DataRow
            For Each dr In ds.Tables(0).Rows
                iCountPendingJobs += 1
                LogToFile(dr("JobName") & "[" & dr("JobID").ToString & ":" & dr("JobDetailID").ToString & "]")
            Next

            Return iCountPendingJobs

        Else
            LogToFile("No data returned.", True)
        End If

    End Function

    Private Function PendingJobInfo() As DataSet
        Try
            Dim ws As New WSdocDBIntegration.docDBIntegration

            LogToFile("Attempting to authenticate for job information against " & strWebServiceURL & " with preshared key of " & strPreSharedKey.ToString)
            ws.Url = strWebServiceURL
            ws.Timeout = WSTIMEOUT

            Return ws.PendingJobsGet(SYSTEMSHAREDKEY, sAccountID)

        Catch ex As System.Web.Services.Protocols.SoapException
            LogToFile(ex.Message, True)
            Throw New Exception("Error during PendingJobInfo.")

        End Try

    End Function
    Private Function ConnectWithPreShare() As Boolean
        'Connect to webservice and validate presharedkey
        Dim ws As New WSdocDBIntegration.docDBIntegration

        Try
            LogToFile("Attempting to authenticate against " & strWebServiceURL & " with preshared key of " & strPreSharedKey.ToString)
            ws.Url = strWebServiceURL
            ws.Timeout = WSTIMEOUT

            Dim sReturn As String = ws.ValidateAccount(SYSTEMSHAREDKEY, strPreSharedKey)

            If IsGuid(sReturn) = False Then
                Throw New Exception(sReturn)
            Else
                sAccountID = sReturn
                LogToFile("Authenticated with " & sReturn.ToString)
            End If
            Return True

        Catch ex As Exception
            LogToFile(ex.Message, True)
            Return False

        End Try

    End Function
    Public Function ReadSetting(ByVal strSettingValue As String, Optional ByVal strFileName As String = Nothing) As String
        If strFileName Is Nothing Then strFileName = AppDomain.CurrentDomain.BaseDirectory & "Settings.xml"
        Dim strReturn As String = Nothing
        Dim streamFile As New System.IO.FileStream(strFileName, IO.FileMode.Open)
        Dim xmlReader As New System.Xml.XmlTextReader(streamFile)
        While xmlReader.Read
            If xmlReader.NodeType = Xml.XmlNodeType.Element Then
                If xmlReader.Name.Equals(strSettingValue) Then
                    strReturn = xmlReader.ReadElementString
                End If
            End If

        End While
        xmlReader.Close()
        streamFile.Close()

        xmlReader = Nothing
        streamFile = Nothing

        Return strReturn
    End Function

    Private Function IsGuid(ByVal Value As String) As Boolean
        Try
            Dim g As New Guid(Value)
            Return True
        Catch
            Return False
        End Try
    End Function


    Protected Overrides Sub OnStop()
        ' Add code here to perform any tear-down necessary to stop your service.
    End Sub

    Private Sub LogToFile(ByVal msg As String, Optional ByVal blnError As Boolean = False)
        Dim spath As String = AppDomain.CurrentDomain.BaseDirectory & "Logs"
        If System.IO.Directory.Exists(spath) = False Then
            System.IO.Directory.CreateDirectory(spath)
            LogToFile("Log directory created (" & spath & ")")
        End If
        Dim filePath As String = AppDomain.CurrentDomain.BaseDirectory & "Logs\" & System.DateTime.Today.Year.ToString & System.DateTime.Today.Month.ToString & System.DateTime.Today.Day.ToString & ".log"
        Dim fs As IO.FileStream = New IO.FileStream(filePath, IO.FileMode.Append)
        Dim sw As IO.StreamWriter = New IO.StreamWriter(fs)
        Dim strLogLine As String
        If blnError = False Then
            strLogLine = System.DateTime.Now.ToLongTimeString & ": " & msg
        Else
            strLogLine = System.DateTime.Now.ToLongTimeString & ": " & "*ERROR* " & msg
        End If

        sw.WriteLine(strLogLine)
        sw.Flush()
        sw.Close()
        fs.Close()
    End Sub

    Private Sub Timer_Elapsed(ByVal sender As Object, ByVal e As System.Timers.ElapsedEventArgs) Handles Timer.Elapsed
        Timer.Enabled = False

        Try

            ProcessIntegration()
            GC.Collect()

        Catch ex As Exception
            Logging.WriteEntry(ex.Message & vbCr & ex.StackTrace)
        End Try

        Timer.Interval = intInterval
        Timer.Enabled = True

    End Sub


    Private Sub ProcessDistributionSyncDA(ByVal dr As DataRow)
        'Sync Data out from host to docAssist


        Try
            Dim ws As New WSdocDBIntegration.docDBIntegration

            LogToFile("Retrieving distribution data.") ' & "[" & dr("FilterAttributeID") & "]")
            ws.Url = strWebServiceURL
            ws.Timeout = WSTIMEOUT
            Dim dsSQLInstructions As DataSet

            'Get Host data and send to webservice
            'Build SQL Query for Remote system

            'Check for existance of DSN
            If IsDSNInstalled(dr("ODBCName")) = False Then
                Throw New Exception("ODBC data source: " & dr("ODBCName") & " does not exists.")
            Else
                LogToFile("ODBC data source: " & dr("ODBCName") & " verified.")
            End If

            Dim sHostQuery As String = BuildDistributionQuery(dr) 'Build Query

            If sHostQuery <> "" Then
                LogToFile("Host query: " & sHostQuery)
            Else
                Throw New Exception("Unable to build query from data object definition. [" & sHostQuery & "]")
            End If

            Dim dsHostData As DataSet = HostDataGet(dr("ODBCName"), dr("UID"), dr("PWD"), dr("CommandType"), sHostQuery)

            If dsHostData.Tables.Count > 0 Then
                LogToFile("Host query returned (" & dsHostData.Tables(0).Rows.Count.ToString & ") row(s)")
            Else
                Throw New Exception("Host did not return any rows.")
            End If

            If dsHostData.Tables.Count > 0 Then
                LogToFile("(" & dsHostData.Tables(0).Rows.Count.ToString & ") row(s) retrieved from host. [" & sHostQuery & "]")
            Else
                Throw New Exception("No rows retrieved.")
            End If

            dsSQLInstructions = ws.DistributiondocAssistChangesSet(SYSTEMSHAREDKEY, sAccountID, dr("JobID"), dr("JobDetailID"), dsHostData)


            'Log dsReconciled table that is returned from web service
            LogToFile("Data updated in docAssist.")
            Dim sColumnList As String = ""
            Dim sDataList As String = ""

            Dim drRow As DataRow
            Dim dc As DataColumn
            Dim iTableNum As Integer = 1
            Dim dtLog As DataTable
            For Each dtLog In dsSQLInstructions.Tables

                For Each dc In dtLog.Columns

                    sColumnList += dc.ColumnName & ","
                Next
                If iTableNum = 1 Then LogToFile(dtLog.Rows.Count.ToString & " row(s) distributions added to docAssist")

                If dtLog.Rows.Count <= 0 Then LogToFile("No rows affected.")

                LogToFile(sColumnList)


                For Each drRow In dtLog.Rows
                    For Each dc In dtLog.Columns
                        sDataList += drRow(dc.ColumnName) & ","
                    Next
                    LogToFile(sDataList)
                Next
            Next


        Catch ex As Exception
            Throw ex
        End Try



    End Sub
End Class

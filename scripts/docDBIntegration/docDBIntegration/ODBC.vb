Imports System.Data.OleDb
Imports System.Data.Odbc
Public Module ODBC

    Public Declare Function SQLDataSources Lib "ODBC32.DLL" (ByVal henv As Integer, ByVal fDirection As Short, ByVal szDSN As String, ByVal cbDSNMax As Short, ByRef pcbDSN As Short, ByVal szDescription As String, ByVal cbDescriptionMax As Short, ByRef pcbDescription As Short) As Short
    Public Declare Function SQLAllocEnv Lib "ODBC32.DLL" (ByRef env As Integer) As Short
    Public Declare Function SQLConfigDataSource Lib "ODBCCP32.DLL" (ByVal hwndParent As Integer, _
    ByVal ByValfRequest As Integer, ByVal lpszDriver As String, ByVal lpszAttributes As String) As Integer

    Public Const SQL_SUCCESS As Integer = 0
    Public Const SQL_FETCH_NEXT As Integer = 1
    Public Const ODBC_ADD_DSN As Short = 1 ' Add user data source
    Public Const ODBC_CONFIG_DSN As Short = 2 ' Configure (edit) data source
    Public Const ODBC_REMOVE_DSN As Short = 3 ' Remove data source
    Public Const ODBC_ADD_SYS_DSN As Short = 4 'Add system data source
    Public Const vbAPINull As Integer = 0 ' NULL Pointer


    <System.Diagnostics.DebuggerStepThrough()> _
    Friend Function ODBCConnection(ByVal strDSN As String, Optional ByVal srtUsername As String = "", Optional ByVal strPassword As String = "") As Data.Odbc.OdbcConnection
        Dim rValue As New OdbcConnection
        Try
            rValue.ConnectionTimeout = 100
            rValue.ConnectionString = "DSN=" & strDSN
            If srtUsername <> "" Then rValue.ConnectionString += "; UID=" & srtUsername
            If strPassword <> "" Then rValue.ConnectionString += "; PWD=" & strPassword

            rValue.Open()
        Catch ex As OdbcException
            Throw New Exception(ex.Message & vbCrLf & rValue.ConnectionString)
        End Try
        Return rValue
    End Function

    Public Function GetSqlReader(ByVal strSql As String, ByVal strDSN As String) As Data.Odbc.OdbcDataReader

        Dim sqlCmd As New Data.Odbc.OdbcCommand(strSql, ODBCConnection(strDSN))
        Dim rValue As Data.Odbc.OdbcDataReader = sqlCmd.ExecuteReader

        Return rValue

    End Function

    Public Function GetScalarResult(ByVal strSql As String, ByVal strDSN As String) As String

        Dim dbCmd As New Data.Odbc.OdbcCommand(strSql)
        Dim dbConn As Data.Odbc.OdbcConnection = ODBCConnection(strDSN)
        dbCmd.Connection = dbConn

        Dim rValue As String = dbCmd.ExecuteScalar
        dbCmd.Dispose()
        dbConn.Close()
        dbConn.Dispose()

        dbCmd = Nothing
        dbConn = Nothing

        Return rValue

    End Function

    '<System.Diagnostics.DebuggerStepThrough()> _
    Public Sub PopulateTable(ByRef dt As DataTable, ByRef strSql As String, Optional ByVal strDSN As String = Nothing, Optional ByRef ODBCConn As Data.Odbc.OdbcConnection = Nothing)

        Dim dbCmd As New Data.Odbc.OdbcCommand(strSql)
        Dim dbConn As Data.Odbc.OdbcConnection
        If Not strDSN Is Nothing Then
            dbConn = ODBCConnection(strDSN)
        ElseIf Not ODBCConn Is Nothing Then
            dbConn = ODBCConn
        End If
        dbCmd.Connection = dbConn
        dbCmd.CommandTimeout = 120
        Dim dbDa As New Data.Odbc.OdbcDataAdapter(dbCmd)
        dbDa.Fill(dt)



    End Sub



End Module



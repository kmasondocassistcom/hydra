Imports Accucentric.docAssist.Data.Images

Partial Class PersonalInfo
    Inherits System.Web.UI.Page

    Private mobjUser As Accucentric.docAssist.Web.Security.User
    Private mconSqlImage As SqlClient.SqlConnection

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Not Page.IsPostBack Then
            Dim dtTimeZones As New DataTable
            Dim sqlCon As New SqlClient.SqlConnection

            Try

                If Request.Cookies("docAssist") Is Nothing Then
                    'Cookie not found, kick user out
                    Response.Redirect("Default.aspx")
                    Exit Sub
                End If

                Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
                Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))

                sqlCon = Functions.BuildConnection(Me.mobjUser)

                Dim objAdmin As New Accucentric.docAssist.Data.Images.Admin(sqlCon)
                Dim dt As New DataTable

                dt = objAdmin.SMPersonalInfoGet(Me.mobjUser.UserId.ToString, Me.mobjUser.TimeDiffFromGMT)

                If dt.Rows.Count > 0 Then
                    lblName.Text = dt.Rows(0)("FirstName") & Space(1) & dt.Rows(0)("LastName")
                    lblEmail.Text = dt.Rows(0)("EmailAddress")
                    lblNumber.Text = dt.Rows(0)("ContactNo")

                    Try
                        lblLogin.Text = CDate(dt.Rows(0)("LastLogin")).ToLongDateString & Space(1) & CDate(dt.Rows(0)("LastLogin")).ToLongTimeString
                    Catch ex As Exception

                    End Try

                End If

            Catch ex As Exception

            Finally

                If sqlCon.State = ConnectionState.Open Then
                    sqlCon.Close()
                    sqlCon.Dispose()
                End If

            End Try

        End If
    End Sub

End Class

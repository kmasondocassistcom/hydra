<%@ Page Language="vb" AutoEventWireup="false" Codebehind="SetupMail.aspx.vb" Inherits="docAssistWeb.SetupMail"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title></title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="css/core.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body>
		<form id="frmSetupMail" method="post" runat="server">
			<table width="100%">
				<tr>
					<td>
						<label>Email Address</label><br>
						<asp:TextBox id="txtPrefix" runat="server"></asp:TextBox>
						<asp:Label id="lblAlias" runat="server"></asp:Label>&nbsp;&nbsp;<!--<a id="link">Send message</a>-->
					</td>
				</tr>
				<tr>
					<td>
						<label>Document Type</label><br>
						<asp:DropDownList id="DocType" runat="server" Width="250px"></asp:DropDownList></td>
				</tr>
				<tr>
					<td height="23">
						<asp:Label id="lblError" runat="server" CssClass="errorLabel"></asp:Label></td>
				</tr>
				<tr>
					<td align="right">
						<asp:Literal id="lit" runat="server"></asp:Literal><button type="button" class="btn primary" id="SaveSettings" runat="server"><span><span>Save 
									Email Settings</span></span></button></td>
				</tr>
			</table>
		</form>
		<script src="scripts/jquery.js"></script>
		<script>
			$(document).ready(function() {
				$('#txtPrefix').focus();
			});
		</script>
	</body>
</HTML>

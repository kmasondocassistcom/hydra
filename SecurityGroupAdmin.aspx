<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="Controls/MenuBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="docHeader" Src="Controls/docHeader.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="SecurityGroupAdmin.aspx.vb"
    Inherits="docAssistWeb.SecurityGroups" %>

<%@ Register TagPrefix="uc1" TagName="TopRight" Src="Controls/TopRight.ascx" %>
<%@ Register TagPrefix="uc1" TagName="AdminNav" Src="Controls/AdminNav.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
   <title>docAssist - Security Groups</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <base target="_self">
    <meta http-equiv="cache-control" content="no-cache">
    <link href="Styles.css" type="text/css" rel="stylesheet">
    <link href="css/jquery-ui.css" rel="Stylesheet" />

    <script type="text/javascript" src="scripts/jquery-1.4.2.min.js"></script>

    <script type="text/javascript" src="scripts/jquery-ui.js"></script>

    <script type="text/javascript" src="scripts/jquery.blockUI.js"></script>

    <style type="text/css">
        .ui-widget
        {
            font-size: 11px !important;
        }
        .ui-state-error-text
        {
            margin-left: 10px;
        }
    </style>

    <script type="text/javascript" language="javascript">

        
        $("#divSAdminGroup").ready(function() {

        $("#divSAdminGroup").dialog({
                autoOpen: false,
                modal: true,
                minHeight: 100,
                height: 'auto',
                width: 'auto',
                resizable: false,
                open: function(event, ui) {
                    $(this).parent().appendTo("#divSAdminGPDlgContainer");
                }
            });

        });


        function closeDialog() {
            //Could cause an infinite loop because of "on close handling"
            $("#divSAdminGroup").dialog('close');
        }

        function openDialog(title, linkID) {


            var pos = $("#" + linkID).position();

            var top = pos.top;

            var left = pos.left + $("#" + linkID).width() + 10;


            $("#divSAdminGroup").dialog("option", "title", title)

            $("#divSAdminGroup").dialog("option", "position", [left, top]);

            $("#divSAdminGroup").dialog('open');

        }



        function openDialogAndBlock(title, linkID) {
            openDialog(title, linkID);

            //block it to clean out the data
            $("#divSAdminGroup").block({
                message: '<img src="Images/progress.gif" />',
                css: { border: '0px' },
                fadeIn: 0,
                //fadeOut: 0,
                overlayCSS: { backgroundColor: '#ffffff', opacity: 1 }
            });

        }


        function unblockDialog() {
           
            $("#divSAdminGroup").unblock();
        }

       
    </script>

</head>
<body>
    <form id="Form1" method="post" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div id="divSAdminGPDlgContainer">
        <div id="divSAdminGroup" style="display: none">
            <asp:UpdatePanel ID="upnlSAGPEdit" runat="server">
                <ContentTemplate>
                    <asp:PlaceHolder ID="phrSAGPEdit" runat="server">
                        <table height="800" cellspacing="0" cellpadding="0" width="900" border="0">
                            <tr>
                                <td valign="top">
                                    <table height="100%" cellspacing="0" cellpadding="0" width="100%" border="0">
                                        <tr>
                                            <td valign="top" height="100%">
                                                <div class="Container">
                                                    <div class="north">
                                                        <div class="east">
                                                            <div class="south">
                                                                <div class="west">
                                                                    <div class="ne">
                                                                        <div class="se">
                                                                            <div class="sw">
                                                                                <div class="nw">
                                                                                    <table id="surround" height="100%" cellspacing="0" cellpadding="0" width="95%" border="0"
                                                                                        class="PageContent">
                                                                                        <tr>
                                                                                            <td align="right">
                                                                                                <table class="PageContent" cellspacing="0" cellpadding="0" width="100%" border="0">
                                                                                                    <tr>
                                                                                                        <td nowrap colspan="2">
                                                                                                            <span class="Heading">Group:</span>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td nowrap>
                                                                                                            <img height="1" src="spacer.gif" width="7">
                                                                                                        </td>
                                                                                                        <td nowrap width="100%">
                                                                                                            <span class="Text" id="GroupNameLabel" runat="server">
                                                                                                                <img height="1" src="spacer.gif" width="31">Group Name:&nbsp;</span><asp:TextBox
                                                                                                                    ID="txtGroupName" runat="server" Width="300px" Font-Names="Trebuchet MS" Font-Size="8pt"
                                                                                                                    MaxLength="45"></asp:TextBox>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td nowrap>
                                                                                                        </td>
                                                                                                        <td nowrap>
                                                                                                            <span class="Text" id="GroupDescLabel" runat="server">Group Description: </span>
                                                                                                            <asp:TextBox ID="txtGroupDesc" runat="server" Width="300px" Font-Names="Trebuchet MS"
                                                                                                                Font-Size="8pt" MaxLength="100"></asp:TextBox>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td nowrap align="left" colspan="2">
                                                                                                            <span class="Heading">Rights:</span>
                                                                                                            <table id="Tabs" cellspacing="0" cellpadding="0" border="0" runat="server" width="100%">
                                                                                                                <tr style="font-family: 'Trebuchet MS'">
                                                                                                                    <td class="selectedTab" id="tdDocs" onclick="showDocs();" align="center" width="150">
                                                                                                                        Documents
                                                                                                                    </td>
                                                                                                                    <td class="unselectedTab">
                                                                                                                        <img height="1" src="spacer.gif" width="5">
                                                                                                                    </td>
                                                                                                                    <td class="unselectedTab" id="tdAttributes" onclick="showAttributes();" nowrap align="center"
                                                                                                                        width="120">
                                                                                                                        Attributes
                                                                                                                    </td>
                                                                                                                    <td class="unselectedTab">
                                                                                                                        <img height="1" src="spacer.gif" width="5">
                                                                                                                    </td>
                                                                                                                    <td class="unselectedTab" id="tdLookups" onclick="showLookups();" nowrap align="center"
                                                                                                                        style="display: none" width="120">
                                                                                                                        Lookups
                                                                                                                    </td>
                                                                                                                    <td class="unselectedTab" style="border-bottom: #6382ad 1px solid" width="650">
                                                                                                                        &nbsp;
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                            <span id="documentRights">
                                                                                                                <table class="SearchGrid" style="border-right: lightgrey thin solid; border-left: lightgrey thin solid;
                                                                                                                    border-top-style: none; border-bottom: lightgrey thin" bordercolor="gainsboro"
                                                                                                                    cellspacing="0" cellpadding="0" border="1">
                                                                                                                    <tr>
                                                                                                                        <td valign="middle" align="center" width="195">
                                                                                                                            <b>&nbsp;Document</b>
                                                                                                                        </td>
                                                                                                                        <td valign="bottom" width="280">
                                                                                                                            <b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Access<br>
                                                                                                                            </b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a id="NoAccessAll" href="javascript:NoAccessAll()">No
                                                                                                                                Access</a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a id="ReadOnlyAll" href="javascript:ReadOnlyAll()">
                                                                                                                                    Read Only</a>&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;<a id="ChangeAll" href="javascript:ChangeAll()">Change</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                                                            <a id="DeleteAll" href="javascript:DeleteAll()">Delete </a>
                                                                                                                        </td>
                                                                                                                        <td valign="bottom" align="center" width="61">
                                                                                                                            <b>&nbsp;Creator Only<br>
                                                                                                                            </b><a id="CreatorAll" href="javascript:CreatorAll()">All</a>
                                                                                                                        </td>
                                                                                                                        <td valign="bottom" align="center" width="62">
                                                                                                                            <b>&nbsp;Current Version<br>
                                                                                                                            </b><a id="CurrentAll" href="javascript:CurrentAll()">All</a>
                                                                                                                        </td>
                                                                                                                        <td valign="bottom" align="center" width="62">
                                                                                                                            <b>&nbsp;Print<br>
                                                                                                                            </b><a id="PrintAll" href="javascript:PrintAll()">All</a>
                                                                                                                        </td>
                                                                                                                        <td valign="bottom" align="center" width="62">
                                                                                                                            <b>&nbsp;E-Mail &amp; Download<br>
                                                                                                                            </b><a id="MailAll" href="javascript:MailAll()">All</a>
                                                                                                                        </td>
                                                                                                                        <td valign="bottom" align="center" width="68">
                                                                                                                            <b>&nbsp;Manage Annotations<br>
                                                                                                                            </b><a id="AnnotationsAll" href="javascript:AnnotationsAll()">All</a>
                                                                                                                        </td>
                                                                                                                        <td valign="bottom" align="center" width="64" colspan="2" style="display: none">
                                                                                                                            <b>&nbsp;Manage Redactions<br>
                                                                                                                            </b><a id="RedactionsAll" href="javascript:RedactionsAll()">All</a>
                                                                                                                        </td>
                                                                                                                        <td width="18">
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                                <div class="SmartFolderGrids" style="border-right: silver thin solid; overflow: auto;
                                                                                                                    border-left: silver thin solid; border-bottom: silver thin solid; height: 200px">
                                                                                                                    <asp:DataGrid ID="dgGroupPermissions" runat="server" Width="100%" AutoGenerateColumns="False"
                                                                                                                        CssClass="SearchGrid" ShowHeader="False">
                                                                                                                        <AlternatingItemStyle BackColor="White"></AlternatingItemStyle>
                                                                                                                        <ItemStyle BackColor="Silver"></ItemStyle>
                                                                                                                        <HeaderStyle Font-Bold="True"></HeaderStyle>
                                                                                                                        <Columns>
                                                                                                                            <asp:TemplateColumn HeaderText="Document">
                                                                                                                                <ItemStyle Width="180px"></ItemStyle>
                                                                                                                                <ItemTemplate>
                                                                                                                                    <asp:Label ID="lblDocumentName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DocumentName") %>'>
                                                                                                                                    </asp:Label>
                                                                                                                                    <asp:Label ID="lblDocumentId" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.Sec_Value") %>'>
                                                                                                                                    </asp:Label>
                                                                                                                                </ItemTemplate>
                                                                                                                            </asp:TemplateColumn>
                                                                                                                            <asp:TemplateColumn HeaderText="Access">
                                                                                                                                <ItemStyle HorizontalAlign="Center" Width="290px"></ItemStyle>
                                                                                                                                <ItemTemplate>
                                                                                                                                    <asp:Label ID="lblSecLevel" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.Sec_Level") %>'>
                                                                                                                                    </asp:Label>
                                                                                                                                    <asp:RadioButton ID="rdoNoAccess" runat="server" GroupName="Access" Text="No Access">
                                                                                                                                    </asp:RadioButton>
                                                                                                                                    <asp:RadioButton ID="rdoReadOnly" runat="server" GroupName="Access" Text="Read Only">
                                                                                                                                    </asp:RadioButton>
                                                                                                                                    <asp:RadioButton ID="rdoChange" runat="server" GroupName="Access" Text="Change">
                                                                                                                                    </asp:RadioButton>
                                                                                                                                    <asp:RadioButton ID="rdoDelete" runat="server" GroupName="Access" Text="Delete">
                                                                                                                                    </asp:RadioButton>
                                                                                                                                </ItemTemplate>
                                                                                                                            </asp:TemplateColumn>
                                                                                                                            <asp:TemplateColumn HeaderText="Creator Only">
                                                                                                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                                                                                <ItemStyle HorizontalAlign="Center" Width="65px"></ItemStyle>
                                                                                                                                <ItemTemplate>
                                                                                                                                    <asp:CheckBox ID="chkCreatorOnly" runat="server" Checked='<%# DataBinder.Eval(Container, "DataItem.CreatorOnly") %>'>
                                                                                                                                    </asp:CheckBox>
                                                                                                                                </ItemTemplate>
                                                                                                                            </asp:TemplateColumn>
                                                                                                                            <asp:TemplateColumn HeaderText="Current Version Only">
                                                                                                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                                                                                <ItemStyle HorizontalAlign="Center" Width="65px"></ItemStyle>
                                                                                                                                <ItemTemplate>
                                                                                                                                    <asp:CheckBox ID="chkCurrentVersionOnly" runat="server" Checked='<%# DataBinder.Eval(Container, "DataItem.VersionOnly") %>'>
                                                                                                                                    </asp:CheckBox>
                                                                                                                                </ItemTemplate>
                                                                                                                            </asp:TemplateColumn>
                                                                                                                            <asp:TemplateColumn HeaderText="Print">
                                                                                                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                                                                                <ItemStyle HorizontalAlign="Center" Width="65px"></ItemStyle>
                                                                                                                                <ItemTemplate>
                                                                                                                                    <asp:CheckBox ID="chkPrint" runat="server" Checked='<%# DataBinder.Eval(Container, "DataItem.Print") %>'>
                                                                                                                                    </asp:CheckBox>
                                                                                                                                </ItemTemplate>
                                                                                                                            </asp:TemplateColumn>
                                                                                                                            <asp:TemplateColumn HeaderText="E-Mail/Export">
                                                                                                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                                                                                <ItemStyle HorizontalAlign="Center" Width="65px"></ItemStyle>
                                                                                                                                <ItemTemplate>
                                                                                                                                    <asp:CheckBox ID="chkEmailExport" runat="server" Checked='<%# DataBinder.Eval(Container, "DataItem.Export") %>'>
                                                                                                                                    </asp:CheckBox>
                                                                                                                                </ItemTemplate>
                                                                                                                            </asp:TemplateColumn>
                                                                                                                            <asp:TemplateColumn HeaderText="Manage Annotations">
                                                                                                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                                                                                <ItemStyle HorizontalAlign="Center" Width="70px"></ItemStyle>
                                                                                                                                <ItemTemplate>
                                                                                                                                    <asp:CheckBox Checked='<%# DataBinder.Eval(Container, "DataItem.Annotate") %>' ID="chkAnnotations"
                                                                                                                                        runat="server"></asp:CheckBox>
                                                                                                                                </ItemTemplate>
                                                                                                                            </asp:TemplateColumn>
                                                                                                                            <asp:TemplateColumn HeaderText="Manage Redactions" Visible="False">
                                                                                                                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                                                                                                                <ItemStyle HorizontalAlign="Center" Width="65px"></ItemStyle>
                                                                                                                                <ItemTemplate>
                                                                                                                                    <asp:CheckBox ID="chkRedactions" runat="server" Checked='<%# DataBinder.Eval(Container, "DataItem.Redact") %>'>
                                                                                                                                    </asp:CheckBox>
                                                                                                                                </ItemTemplate>
                                                                                                                            </asp:TemplateColumn>
                                                                                                                        </Columns>
                                                                                                                    </asp:DataGrid></div>
                                                                                                            </span><span id="spanAttributes" style="border-right: lightgrey thin solid; display: none;
                                                                                                                border-left: lightgrey thin solid; width: 100%; border-bottom: lightgrey thin;
                                                                                                                height: 252px">
                                                                                                                <table class="SearchGrid" cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                                                                    <tr>
                                                                                                                        <td width="100%" align="right">
                                                                                                                            <asp:CheckBox ID="rdoAdvancedPerms" runat="server" Text="Enable advanced attribute permissions">
                                                                                                                            </asp:CheckBox>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                                <table class="SearchGrid" style="border-right: lightgrey thin solid; border-left: lightgrey thin solid;
                                                                                                                    border-bottom: lightgrey thin" bordercolor="gainsboro" cellspacing="0" cellpadding="0"
                                                                                                                    border="0">
                                                                                                                    <tr>
                                                                                                                        <td valign="middle" width="404" align="center">
                                                                                                                            <b>&nbsp;Document/Attribute</b>
                                                                                                                        </td>
                                                                                                                        <td valign="bottom" align="center" width="405" colspan="2">
                                                                                                                            <b>Access Level<br>
                                                                                                                            </b><a id="A1" href="javascript:attributeNoAccessAll()">None</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                                                            <a id="A2" href="javascript:attributeReadOnlyAll()">Read Only</a>&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                                                            &nbsp;<a id="A3" href="javascript:attributeChangeAll()">Change</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                                                            <a id="A4" href="javascript:attributeDeleteAll()">Delete </a>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                                <div style="overflow: auto; height: 220px">
                                                                                                                    <asp:DataGrid ID="dgAttributes" runat="server" ShowHeader="False" CssClass="SearchGrid"
                                                                                                                        AutoGenerateColumns="False" Width="100%">
                                                                                                                        <SelectedItemStyle Wrap="False"></SelectedItemStyle>
                                                                                                                        <AlternatingItemStyle Wrap="False" BackColor="White"></AlternatingItemStyle>
                                                                                                                        <ItemStyle Wrap="False" BackColor="Silver"></ItemStyle>
                                                                                                                        <HeaderStyle Font-Bold="True"></HeaderStyle>
                                                                                                                        <Columns>
                                                                                                                            <asp:TemplateColumn HeaderText="Document/Attribute">
                                                                                                                                <ItemStyle Width="50%"></ItemStyle>
                                                                                                                                <ItemTemplate>
                                                                                                                                    <asp:Label ID="lblDocName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DocumentName") %>'
                                                                                                                                        Font-Bold="True">
                                                                                                                                    </asp:Label>
                                                                                                                                    <asp:Label ID="lblAttributeName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AttributeName") %>'>
                                                                                                                                    </asp:Label>
                                                                                                                                    <asp:Label ID="lblAttributeId" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AttributeId") %>'
                                                                                                                                        Visible="False">
                                                                                                                                    </asp:Label>
                                                                                                                                    <asp:Label ID="lblDocumentId" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DocumentId") %>'
                                                                                                                                        Visible="False">
                                                                                                                                    </asp:Label>
                                                                                                                                </ItemTemplate>
                                                                                                                            </asp:TemplateColumn>
                                                                                                                            <asp:TemplateColumn HeaderText="Access">
                                                                                                                                <ItemStyle HorizontalAlign="Center" Width="800px"></ItemStyle>
                                                                                                                                <ItemTemplate>
                                                                                                                                    <asp:DropDownList ID="ddlAccess" runat="server" Width="150px" SelectedIndex='<%# DataBinder.Eval(Container, "DataItem.AccessLevel") %>'>
                                                                                                                                        <asp:ListItem Value="0">None</asp:ListItem>
                                                                                                                                        <asp:ListItem Value="1">Read Only</asp:ListItem>
                                                                                                                                        <asp:ListItem Value="2">Change</asp:ListItem>
                                                                                                                                        <asp:ListItem Value="3">Delete</asp:ListItem>
                                                                                                                                    </asp:DropDownList>
                                                                                                                                </ItemTemplate>
                                                                                                                            </asp:TemplateColumn>
                                                                                                                        </Columns>
                                                                                                                    </asp:DataGrid></div>
                                                                                                            </span><span id="spanLookups" style="border-right: lightgrey thin solid; display: none;
                                                                                                                border-left: lightgrey thin solid; width: 100%; border-bottom: lightgrey thin;
                                                                                                                height: 252px">
                                                                                                                <table class="SearchGrid" id="Table2" style="border-right: lightgrey thin solid;
                                                                                                                    border-left: lightgrey thin solid; border-bottom: lightgrey thin" bordercolor="gainsboro"
                                                                                                                    cellspacing="0" cellpadding="0" border="0">
                                                                                                                    <tr>
                                                                                                                        <td valign="middle" align="center" width="404">
                                                                                                                            <b>&nbsp;Lookup</b>
                                                                                                                        </td>
                                                                                                                        <td valign="bottom" align="center" width="405" colspan="2">
                                                                                                                            <b>Access Level<br>
                                                                                                                            </b><a id="A5" href="javascript:lookupNoAccessAll()">Read Only</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                                                            <a id="A6" href="javascript:lookupReadOnlyAll()">Add</a>&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                                                            &nbsp;<a id="A7" href="javascript:lookupChangeAll()">Change</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                                                            <a id="A8" href="javascript:lookupDeleteAll()">Delete </a>
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                                <asp:DataGrid ID="dgLookups" runat="server" Width="100%" ShowHeader="False" CssClass="SearchGrid"
                                                                                                                    AutoGenerateColumns="False">
                                                                                                                    <SelectedItemStyle Wrap="False"></SelectedItemStyle>
                                                                                                                    <AlternatingItemStyle Wrap="False" BackColor="White"></AlternatingItemStyle>
                                                                                                                    <ItemStyle Wrap="False" BackColor="Silver"></ItemStyle>
                                                                                                                    <HeaderStyle Font-Bold="True"></HeaderStyle>
                                                                                                                    <Columns>
                                                                                                                        <asp:TemplateColumn HeaderText="Document/Attribute">
                                                                                                                            <ItemStyle Width="50%"></ItemStyle>
                                                                                                                            <ItemTemplate>
                                                                                                                                <asp:Label ID="lblLookupAttributeId" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AttributeId") %>'
                                                                                                                                    Font-Bold="True" Visible="False">
                                                                                                                                </asp:Label>
                                                                                                                                <asp:Label ID="lblLookupName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AttributeName") %>'>
                                                                                                                                </asp:Label>
                                                                                                                            </ItemTemplate>
                                                                                                                        </asp:TemplateColumn>
                                                                                                                        <asp:TemplateColumn HeaderText="Access">
                                                                                                                            <ItemStyle HorizontalAlign="Center" Width="800px"></ItemStyle>
                                                                                                                            <ItemTemplate>
                                                                                                                                <asp:DropDownList ID="ddlLookup" runat="server" Width="150px" SelectedIndex='<%# DataBinder.Eval(Container, "DataItem.AccessLevel") %>'>
                                                                                                                                    <asp:ListItem Value="0">Read Only</asp:ListItem>
                                                                                                                                    <asp:ListItem Value="1">Add</asp:ListItem>
                                                                                                                                    <asp:ListItem Value="2">Change</asp:ListItem>
                                                                                                                                    <asp:ListItem Value="3">Delete</asp:ListItem>
                                                                                                                                </asp:DropDownList>
                                                                                                                            </ItemTemplate>
                                                                                                                        </asp:TemplateColumn>
                                                                                                                    </Columns>
                                                                                                                </asp:DataGrid>
                                                                                                            </span>
                                                                                                            <br>
                                                                                                            <span class="Heading">Users:</span>
                                                                                                            <br>
                                                                                                            <table>
                                                                                                                <tr>
                                                                                                                    <td>
                                                                                                                        <table class="PageContent" id="Table1" height="100%" cellspacing="0" cellpadding="0"
                                                                                                                            border="0">
                                                                                                                            <tr>
                                                                                                                                <td width="50%" height="15">
                                                                                                                                    &nbsp;&nbsp;<span class="Text">Available Users:&nbsp;</span>
                                                                                                                                </td>
                                                                                                                                <td width="25" height="15" rowspan="1">
                                                                                                                                </td>
                                                                                                                                <td width="50%" height="15">
                                                                                                                                    &nbsp;<span class="Text">Assigned Users:&nbsp;</span>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td valign="top" width="50%" height="100%">
                                                                                                                                    <asp:ListBox ID="lstAvailable" runat="server" Width="215px" Height="180px" SelectionMode="Multiple">
                                                                                                                                    </asp:ListBox>
                                                                                                                                </td>
                                                                                                                                <td valign="top" align="center" width="25">
                                                                                                                                    <br>
                                                                                                                                    <br>
                                                                                                                                    <asp:ImageButton ID="btnAssignAll" runat="server" ImageUrl="Images/addall.gif"></asp:ImageButton><br>
                                                                                                                                    <br>
                                                                                                                                    <asp:ImageButton ID="btnAdd" runat="server" ImageUrl="Images/addone.gif"></asp:ImageButton><br>
                                                                                                                                    <br>
                                                                                                                                    <asp:ImageButton ID="btnRemove" runat="server" ImageUrl="Images/removeone.gif"></asp:ImageButton><br>
                                                                                                                                    <br>
                                                                                                                                    <asp:ImageButton ID="btnRemoveAll" runat="server" ImageUrl="Images/removeall.gif">
                                                                                                                                    </asp:ImageButton>
                                                                                                                                </td>
                                                                                                                                <td valign="top" width="50%">
                                                                                                                                    &nbsp;
                                                                                                                                    <asp:ListBox ID="lstAssigned" runat="server" Width="215px" Height="180px" SelectionMode="Multiple">
                                                                                                                                    </asp:ListBox>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </table>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                                <span class="Text" id="ErrorMessage" runat="server"></span>&nbsp;&nbsp;
                                                                                                <a href="#" onclick="closeDialog()">
                                                                                                <img src="Images/smallred_cancel.gif" alt ="Cancel" border="0"></a>&nbsp; &nbsp;
                                                                                                    <asp:ImageButton ID="btnSave"
                                                                                                        runat="server" ImageUrl="Images/smallred_save.gif" ToolTip="Save"></asp:ImageButton>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </asp:PlaceHolder>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <table height="100%" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td width="100%" colspan="6">
                <uc1:docHeader ID="docHeader" runat="server"></uc1:docHeader>
            </td>
            <td width="100%">
            </td>
        </tr>
        <tr valign="middle" height="100%">
            <td class="Navigation1" style="border-right: #cccccc thin solid; width: 171px" valign="top"
                height="100%">
                <uc1:AdminNav ID="AdminNav1" runat="server"></uc1:AdminNav>
            </td>
            <td>
                <img src="spacer.gif" height="1" width="25">
            </td>
            <td valign="top" width="100%">
                <br/>
                
                <asp:UpdatePanel ID="upnlUsers" UpdateMode="Conditional" runat="server">
                        <ContentTemplate>
                 <table id="Table3" cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr align="right">
                        <td class="PageContent" align="left" width="100%">
                          <asp:LinkButton ID="btnAddGroup" OnClientClick="openDialogAndBlock('Add Group','btnAddGroup')"
                                     Text="Add Group"  align="left" border="0" runat="server" />  
                        </td>
                    </tr>
                </table>
                    <br />                       
                <asp:DataGrid ID="dgGroups" runat="server" AutoGenerateColumns="False" CssClass="PageContent"
                    Width="100%">
                    <SelectedItemStyle Height="20px"></SelectedItemStyle>
                    <AlternatingItemStyle CssClass="TableAlternateItem" BackColor="#E0E0E0"></AlternatingItemStyle>
                    <ItemStyle Height="20px"></ItemStyle>
                    <HeaderStyle Font-Bold="True" Height="20px" CssClass="TableHeader"></HeaderStyle>
                    <Columns>
                        <asp:TemplateColumn>
                            <HeaderStyle Width="85px"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblGroupId" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.GroupId") %>'>
                                </asp:Label>
                                <asp:Label ID="lblGroupAllowDelete" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.AllowDelete") %>'>
                                </asp:Label>&nbsp;
                                
                                <asp:LinkButton ID="lnkEditGroup" runat="server" ToolTip="Edit Group" CommandName="Edit"
                                                            CommandArgument='<%# DataBinder.Eval(Container, "DataItem.GroupId") %>'>
                                                        <img src="Images/editIcon.gif" border="0">
                                                        </asp:LinkButton>
                                                        &nbsp;
                                                        <asp:LinkButton ID="lnkCopyGroup" runat="server" ToolTip="Copy Group" CommandName="Copy"
                                                            CommandArgument='<%# DataBinder.Eval(Container, "DataItem.GroupId") %>'>
                                                        <img src="Images/listingcopy.gif" border="0">
                                                        </asp:LinkButton>&nbsp;
                               
                                <asp:LinkButton ID="lnkDeleteGroup" runat="server" ToolTip="Remove Group" CommandName="Delete"     CommandArgument='<%# DataBinder.Eval(Container, "DataItem.GroupId") %>'>
											<img src="./Images/trash.gif" border="0"></asp:LinkButton><img height="1" src="spacer.gif"
                                                width="10">
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="GroupName" HeaderText="Group"></asp:BoundColumn>
                    </Columns>
                </asp:DataGrid>
               <asp:LinkButton ID="btnRefreshGrid"  CausesValidation="false" Style="display: none"
                                            runat="server">LinkButton</asp:LinkButton>
                </ContentTemplate> 
                </asp:UpdatePanel>
                <asp:Label ID="lblNoGroups" runat="server" Width="100%" Visible="False"></asp:Label>
            </td>
            <td>
                <img src="spacer.gif" height="1" width="25">
            </td>
            <tr>
                <td colspan="5">
                    <uc1:TopRight ID="docFooter" runat="server"></uc1:TopRight>
                </td>
                <td>
                </td>
            </tr>
    </table>
    <asp:UpdatePanel ID="upnlJsRunner" UpdateMode="Always" runat="server">
        <ContentTemplate>
            <asp:PlaceHolder ID="phrJsRunner" runat="server"></asp:PlaceHolder>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>

    <script type="text/javascript" language="javascript">

        function EditGroup(groupid) {
            var nXpos = (document.body.clientWidth - 400) / 2;
            var nYpos = (document.body.clientHeight - 200) / 2;
            //window.open('SecurityGroupEdit.aspx?ID='+groupid,'EditGroup','height=715,width=895,resizable=no,status=no,toolbar=no,top='+nYpos+',left='+nXpos)
            window.open('SecurityGroupEdit.aspx?ID=' + groupid, 'EditGroup', 'height=700,width=895,resizable=no,status=no,toolbar=no')

        }

        function CopyGroup(groupid) {
            var nXpos = (document.body.clientWidth - 400) / 2;
            var nYpos = (document.body.clientHeight - 200) / 2;
            //window.open('SecurityGroupEdit.aspx?ID='+groupid+'&Mode=Copy','EditGroup','height=715,width=895,resizable=no,status=no,toolbar=no,top='+nYpos+',left='+nXpos)
            window.open('SecurityGroupEdit.aspx?ID=' + groupid + '&Mode=Copy', 'EditGroup', 'height=700,width=895,resizable=no,status=no,toolbar=no')

        }

        function GroupDeleteError() {
            alert('Cannot delete this group.');
        }
			
    

    function NameError() {
        document.all.GroupNameLabel.className = 'ErrorText';
    }

    function CloseWindow() {
        opener.location.reload();
        window.close();
    }

    function ignoreEvent() {
        if (event.keyCode == 13) {
            event.cancelBubble = true;
            event.returnValue = false;
        }
    }

    function showDocs() {
        var documentRights = document.getElementById('documentRights');
        if (documentRights != undefined) { documentRights.style.display = 'block'; }
        var spanAttributes = document.getElementById('spanAttributes');
        if (spanAttributes != undefined) { spanAttributes.style.display = 'none'; }
        var spanLookups = document.getElementById('spanLookups');
        if (spanLookups != undefined) { spanLookups.style.display = 'none'; }
        if (document.getElementById('tdDocs') != undefined) { document.getElementById('tdDocs').className = 'selectedTab'; }
        if (document.getElementById('tdAttributes') != undefined) { document.getElementById('tdAttributes').className = 'unselectedTab'; }
        if (document.getElementById('tdLookups') != undefined) { document.getElementById('tdLookups').className = 'unselectedTab'; }
    }

    function showAttributes() {

        var documentRights = document.getElementById('documentRights');
        if (documentRights != undefined) { documentRights.style.display = 'none'; }
        var spanAttributes = document.getElementById('spanAttributes');
        if (spanAttributes != undefined) { spanAttributes.style.display = 'block'; }
        var spanLookups = document.getElementById('spanLookups');
        if (spanLookups != undefined) { spanLookups.style.display = 'none'; }
        document.getElementById('tdDocs').className = 'unselectedTab';
        document.getElementById('tdAttributes').className = 'selectedTab';
        if (document.getElementById('tdLookups') != undefined) { document.getElementById('tdLookups').className = 'unselectedTab'; }
    }

    function showLookups() {
        var documentRights = document.getElementById('documentRights');
        if (documentRights != undefined) { documentRights.style.display = 'none'; }
        var spanAttributes = document.getElementById('spanAttributes');
        if (spanAttributes != undefined) { spanAttributes.style.display = 'none'; }
        var spanLookups = document.getElementById('spanLookups');
        if (spanLookups != undefined) { spanLookups.style.display = 'block'; }
        if (document.getElementById('tdDocs') != undefined) { document.getElementById('tdDocs').className = 'unselectedTab'; }
        if (document.getElementById('tdAttributes') != undefined) { document.getElementById('tdAttributes').className = 'unselectedTab'; }
        if (document.getElementById('tdLookups') != undefined) { document.getElementById('tdLookups').className = 'selectedTab'; }
    }

    var allowMassAction = false;
    function setEnable() {
        chk = document.getElementById('rdoAdvancedPerms');
        if (chk.checked) {
            allowMassAction = true;
            enable();
        }
        else {
            allowMassAction = false;
            disable();
        }
    }
			
		</script>
</body>
</html>

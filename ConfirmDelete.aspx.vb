Partial Class _ConfirmDelete
    Inherits System.Web.UI.Page

    Private mobjUser As Accucentric.docAssist.Web.Security.User
    Private mconSqlImage As SqlClient.SqlConnection

    Private mcookieImage As cookieImage
    Private mcookieSearch As cookieSearch

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region


    Friend ReadOnly Property PageControls() As System.Web.UI.ControlCollection
        Get
            If Me.FindControl("Form1") Is Nothing Then
                Throw New Exception("Form1 not found.")
            Else
                Return Me.FindControl("Form1").Controls
            End If
        End Get
    End Property

    Private Sub DeleteDocument()

        Dim conSqlImages As SqlClient.SqlConnection = Functions.BuildConnection(Me.mobjUser)

        Try

            Dim cmdSql As SqlClient.SqlCommand
            '
            ' Get the image id
            Dim intImageId As Integer
            cmdSql = New SqlClient.SqlCommand("SELECT ImageId FROM ImageVersion WHERE VersionId = @VersionId", conSqlImages)
            cmdSql.Parameters.Add("@VersionId", txtVersionId.Text)
            conSqlImages.Open()
            intImageId = cmdSql.ExecuteScalar()

            ' Delete the image
            cmdSql = New SqlClient.SqlCommand("acsImageDelete", conSqlImages)
            cmdSql.CommandType = CommandType.StoredProcedure
            cmdSql.Parameters.Add("@ImageId", intImageId)
            cmdSql.Parameters.Add("@UserId", Me.mobjUser.ImagesUserId)
            cmdSql.ExecuteNonQuery()

            'Update cached results
            Functions.UpdateCachedResults(intImageId, Me)

            ' Clear the cookies
            If Me.mcookieSearch.VersionID > 0 Then
                Me.mcookieSearch.Expire()
            End If

            ' Send the browser back to the search screen
            Dim sbResult As New System.Text.StringBuilder
            sbResult.Append("<SCRIPT language=""javascript"">")
            'sbResult.Append("alert(""Document deleted successfully."");")
            sbResult.Append("var search='search';window.returnValue=search;window.close();")
            sbResult.Append("</SCRIPT>" & vbCrLf)

            litResult.Text = sbResult.ToString()

        Catch exSql As SqlClient.SqlException

            Functions.LogException(mobjUser.UserId.ToString, mobjUser.AccountId.ToString, "", "Delete document: " & exSql.Message, exSql.StackTrace, Server.MachineName, "", Request.UrlReferrer.ToString, Functions.BuildMasterConnection, True)

#If DEBUG Then
            lblErrorMessage.Text = exSql.Message
#Else
            lblErrorMessage.Text = "An error occurred while trying to delete the requested document."
#End If
            lblErrorMessage.Visible = True
        Catch ex As Exception

            Functions.LogException(mobjUser.UserId.ToString, mobjUser.AccountId.ToString, "", "Delete document: " & ex.Message, ex.StackTrace, Server.MachineName, "", Request.UrlReferrer.ToString, Functions.BuildMasterConnection, True)

#If DEBUG Then
            lblErrorMessage.Text = ex.Message & "<BR>" & ex.StackTrace
#Else
            lblErrorMessage.Text = "An error occurred, if this continues please contact your account coordinator."
#End If
            lblErrorMessage.Visible = True
        Finally
            If Not conSqlImages.State = ConnectionState.Closed Then conSqlImages.Close()
        End Try

    End Sub

    Private Sub DeleteRange(ByVal intBegin As Integer, ByVal intEnd As Integer)
        Dim bolValue As Boolean
        '
        ' Get the version
        '
        Dim conSqlImages As SqlClient.SqlConnection = Functions.BuildConnection(Me.mobjUser)
        Try

            conSqlImages.Open()
            'Dim objSave As New Accucentric.docAssist.Data.ImageSave(Me.mobjUser.ImagesUserId, conSqlImages)

            '
            ' Get the current version
            Dim intVersionId As Integer = txtVersionId.Text
            Dim dsVersion As Accucentric.docAssist.Data.dsVersionControl = Session(intVersionId & "VersionControl")
            Dim intImageId As Integer = CType(dsVersion.Images.Rows(0), Accucentric.docAssist.Data.dsVersionControl.ImagesRow).ImageID
            '
            'Determine if the entire image will be deleted
            If intBegin = 1 And intEnd >= dsVersion.ImageVersion.Rows(0).Item("SeqTotal") Then
                DeleteDocument()
                Exit Sub
            End If



            ' Delete version from database
            '
            Dim dvImageVersionDetail As New DataView(dsVersion.ImageVersionDetail, Nothing, "SeqId", DataViewRowState.CurrentRows)
            Dim drImageVersionDetail As Accucentric.docAssist.Data.dsVersionControl.ImageVersionDetailRow
            Dim intPageNoPrior As Integer, dvr As DataRowView
            '
            ' Loop through deleting the records
            '
            For Each dvr In dvImageVersionDetail
                drImageVersionDetail = dvr.Row
                If drImageVersionDetail.SeqID < intBegin Then
                    intPageNoPrior = drImageVersionDetail.SeqID
                ElseIf drImageVersionDetail.SeqID >= intBegin And drImageVersionDetail.SeqID <= intEnd Then
                    drImageVersionDetail.SeqID = 0
                    '
                    ' Check for imported image, and determine to delete the import file
                    If drImageVersionDetail.DeleteFile Then
                        ' Check for other pages
                        Dim dv As New DataView(dsVersion.ImageVersionDetail, "ImportFileName='" & drImageVersionDetail.ImportFileName & "'", Nothing, DataViewRowState.CurrentRows)
                        If dv.Count = 1 And System.IO.File.Exists(drImageVersionDetail.ImportFileName) Then
                            ' Delete the file
                            System.IO.File.Delete(drImageVersionDetail.ImportFileName)
                        End If
                    End If
                    drImageVersionDetail.SeqID = 0
                    drImageVersionDetail.Delete()
                Else
                    drImageVersionDetail.SeqID = intPageNoPrior + 1
                    intPageNoPrior = drImageVersionDetail.SeqID
                End If
            Next
            '
            ' Get the new page count
            Dim dvImages As New DataView(dsVersion.ImageVersionDetail, Nothing, Nothing, DataViewRowState.CurrentRows)
            CType(dsVersion.ImageVersion.Rows(0), Accucentric.docAssist.Data.dsVersionControl.ImageVersionRow).SeqTotal = dvImages.Count

            '
            ' Update the database
            Dim objSave As New Accucentric.docAssist.Data.ImageSave(GetImagesUserId(conSqlImages, Me.mobjUser), conSqlImages)
            Dim iNewVersionID As Integer = objSave.UpdateImage(dsVersion, , , True)
            If iNewVersionID > 0 Then
                Dim objEncrypt As Encryption.Encryption

                ' Send confirmation back to the user
                Dim sbResult As New System.Text.StringBuilder
                sbResult.Append("<SCRIPT language=""javascript"">")
                sbResult.Append("window.returnValue='" & iNewVersionID.ToString & "';window.close();")
                sbResult.Append("</SCRIPT>" & vbCrLf)

                ' Set the page to the current page
                Me.mcookieSearch.VersionID = iNewVersionID
                'Me.mcookieImage.PageNo = txtPageNo.Text
                Me.mcookieImage.PageNo = 1
                Response.Flush()

                litResult.Text = sbResult.ToString()

                Session(intVersionId & "VersionControl") = dsVersion

            End If


        Catch exSql As SqlClient.SqlException
#If DEBUG Then
            lblErrorMessage.Text = exSql.Message
#Else
            lblErrorMessage.Text = "An error occurred while trying to delete the requested document."
#End If
            lblErrorMessage.Visible = True
        Catch ex As Exception
#If DEBUG Then
            lblErrorMessage.Text = ex.Message & "<BR>" & ex.StackTrace
#Else
            lblErrorMessage.Text = "An error occurred, if this continues please contact your account coordinator."
#End If
            lblErrorMessage.Visible = True
        Finally
            If Not conSqlImages.State = ConnectionState.Closed Then conSqlImages.Close()
        End Try

    End Sub

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        '
        ' Build the user object
        '
        Try
            Me.mobjUser = BuildUserObject(Request.Cookies("docAssist"))
            Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)

            'Response.CacheControl = "no-cache"

        Catch ex As Exception
            If ex.Message = "User is not logged on" Then
                Response.Redirect("default.aspx")
            End If
        End Try

        btnGoDelete.Attributes.Add("OnClick", "return Validate();")
        Me.mcookieImage = New cookieImage(Me)
        Me.mcookieSearch = New cookieSearch(Me)

        If Not Page.IsPostBack Then
            Try
                If Request.RequestType = "GET" Then
                    txtVersionId.Text = Request.QueryString("VersionId")
                    txtPageNo.Text = Request.QueryString("PageNo")
                    lblPageNo.Text = "(Page No. " & CInt(Request.QueryString("PageNo")).ToString("#,###") & ")"
                    hidMaxPages.Value = Request.QueryString("PageCount")
                Else
                    Throw New Exception("Error loading image.")
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End If

    End Sub

    Private Sub btnGoDelete_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnGoDelete.Click

        If rdoCurrent.Checked Then
            DeleteRange(txtPageNo.Text, txtPageNo.Text)
        ElseIf rdoDocument.Checked Then
            DeleteDocument()
        ElseIf rdoRange.Checked Then
            DeleteRange(txtFrom.Value, txtTo.Value)
        End If

        Me.mcookieSearch.PageNo = 1
        Me.mcookieImage.PageNo = 1

    End Sub

    Private Sub btnGoDelete_Command(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.CommandEventArgs) Handles btnGoDelete.Command

    End Sub
End Class

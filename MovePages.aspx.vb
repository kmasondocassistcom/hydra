Imports Accucentric.docAssist

Partial Class MovePages
    Inherits System.Web.UI.Page

    Private mintVersionId As Integer
    Private mobjUser As Accucentric.docAssist.Web.Security.User
    Private mconSqlImage As SqlClient.SqlConnection

    Private Const MAX_COLS As Integer = 5
    Private Const MAX_ROWS As Integer = 5500
    Private Const MAX_HEIGHT As Integer = 90
    Private Const MAX_WIDTH As Integer = 81

    Private mcookieImage As cookieImage
    Private mcookieSearch As cookieSearch

    Protected WithEvents lblThumbs As System.Web.UI.WebControls.Label

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Response.Expires = 0
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        ' Build the user object
        Try
            Me.mobjUser = BuildUserObject(Request.Cookies("docAssist"))
            Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
        Catch ex As Exception
            If ex.Message = "User is not logged on" Then
                Response.Redirect("default.aspx")
            End If
        End Try

        ' Initialize the page
        Me.mcookieImage = New cookieImage(Me)
        Me.mcookieSearch = New cookieSearch(Me)

        Try

            If Not Page.IsPostBack Then

                If Me.mcookieSearch.VersionID < 1 Then Me.mcookieSearch.VersionID = Request.QueryString("VID")

                If Me.mcookieSearch.VersionID > 0 Then
                    Me.mintVersionId = Me.mcookieSearch.VersionID
                    LoadThumbnails()
                Else
                    Throw New Exception("Error receiving image information.")
                End If

            End If

        Catch ex As Exception

        End Try

    End Sub

    Private Sub LoadThumbnails(Optional ByVal intStartPage As Integer = 1)

        ' Open connection
        If Not Me.mconSqlImage.State = ConnectionState.Open Then
            Me.mconSqlImage.Open()
        End If

        ' Build the thumbnail files
        Dim dsVersion As Accucentric.docAssist.Data.dsVersionControl = Session(Me.mcookieSearch.VersionID & "VersionControl")
        Dim dsImages As New dsThumbnails
        Dim dtImages As dsThumbnails.ImagesDataTable = dsImages.Images
        Dim drImages As dsThumbnails.ImagesRow

        Dim sb As New System.Text.StringBuilder

        ' Loop through each page of the version
        Dim sbQuery As New System.Text.StringBuilder
        sbQuery.Append("SeqId >= ")
        sbQuery.Append(intStartPage.ToString)
        sbQuery.Append(" and SeqId < ")
        sbQuery.Append(intStartPage + (MAX_COLS * MAX_ROWS))

        Dim dv As New DataView(dsVersion.ImageVersionDetail, sbQuery.ToString, "SeqId", DataViewRowState.CurrentRows)
        Dim intPageCount As Integer = CType(dsVersion.ImageVersion.Rows(0), Accucentric.docAssist.Data.dsVersionControl.ImageVersionRow).SeqTotal

        Dim strHostname As String = Request.Url.AbsoluteUri.Substring(0, Request.Url.AbsoluteUri.Length - Request.Url.AbsolutePath.Length)
#If DEBUG Then
        strHostname += "/docAssistWeb1"
#End If
        System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.Empty.ToString, "startPage = " & intStartPage & ";endPage = " & intStartPage + MAX_ROWS - 1 & ";versionId = " & Me.mcookieSearch.VersionID & ";quality = " & "1" & ";accountId = '" & Me.mobjUser.AccountId.ToString & "';pageCount = '" & intPageCount & "';hostname = '" & strHostname & "';", True)

    End Sub

    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnOk.Click

        Try

            Dim dsVersion As Accucentric.docAssist.Data.dsVersionControl = Session(Me.mcookieSearch.VersionID & "VersionControl")

            Dim pageCount As Integer = dsVersion.ImageVersionDetail.Count

            'Clear pages
            For X As Integer = 0 To pageCount - 1
                dsVersion.ImageVersionDetail.Item(X).SeqID = CInt(dsVersion.ImageVersionDetail.Item(X).SeqID * -1)
            Next

            Dim items() As String = NewOrder.Value.Split(",")

            'Reorder pages
            For X As Integer = 0 To pageCount - 1
                Dim objFind As Object
                Dim intImageDetId As Integer
                objFind = items(x).Split("-")(1)
                dsVersion.ImageVersionDetail.PrimaryKey = New DataColumn() {dsVersion.ImageVersionDetail.Columns("ImageDetId")}
                Dim drVersionImageDetail As Data.dsVersionControl.ImageVersionDetailRow = dsVersion.ImageVersionDetail.Rows.Find(objFind)
                'dsVersion.ImageVersionDetail.Item(X).SeqID = CInt(items(x))
                drVersionImageDetail.SeqID = x + 1
            Next

            System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.Empty.ToString, "window.returnValue='" & NewOrder.Value & "';window.close();", True)

        Catch ex As Exception

        End Try

    End Sub

End Class
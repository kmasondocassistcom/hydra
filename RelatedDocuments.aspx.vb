Imports Accucentric.docAssist.Data.Images

Partial Class RelatedDocuments
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private mobjUser As Accucentric.docAssist.Web.Security.User
    Private mconSqlImage As New SqlClient.SqlConnection

    Private Enum RELATED_DOCUMENTS
        DocumentCounts = 0
        Documents = 1
        Attributes = 2
    End Enum

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
        Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
        Me.mconSqlImage.Open()

        Dim objUtil As New SystemUtilities(Me.mconSqlImage)
        Dim sb As New System.Text.StringBuilder

        Try

            If IsNumeric(Request.QueryString("ImageId")) Then

                Dim dsRelatedDocuments As DataSet = objUtil.GetRelatedDocuments(Request.QueryString("ImageId"), Me.mobjUser.ImagesUserId)

                If dsRelatedDocuments.Tables.Count > 0 Then

                    If dsRelatedDocuments.Tables(RELATED_DOCUMENTS.Documents).Rows.Count > 0 Then

                        sb.Append("<table width=100% class=relatedDocs cellspacing=0>")

                        Dim divCounter As Integer = 1

                        For Each docType As DataRow In dsRelatedDocuments.Tables(RELATED_DOCUMENTS.DocumentCounts).Rows

                            Dim divSection As String = "section" & divCounter

                            sb.Append("<tr><td class=docType colspan=5>" & docType("DocumentName") & " (" & docType("DocumentCount") & ")</td></tr>")

                            Dim docs As New DataView(dsRelatedDocuments.Tables(RELATED_DOCUMENTS.Documents))
                            docs.RowFilter = "DocumentID='" & docType("DocumentID") & "'"

                            Dim x As Integer = 0

                            sb.Append("<div style=""DISPLAY:none"" id=" & divSection & ">")

                            For Each document As DataRowView In docs

                                If x Mod 2 = 0 Then
                                    sb.Append("<a href=""#"" onclick=""parent.window.location.href='Viewer.aspx?VID=" & document("CurrentVersionID") & "&Ref=Related'""><tr class=docRowEven><td width=15 nowrap></td><td nowrap>")
                                Else
                                    sb.Append("<a href=""#"" onclick=""parent.window.location.href='Viewer.aspx?VID=" & document("CurrentVersionID") & "&Ref=Related'""><tr class=docRow><td width=15 nowrap></td><td nowrap>")
                                End If

                                Select Case document("ImageType")
                                    Case ImageTypes.SCAN
                                        sb.Append("<img src=Images/folders/scan_preview.gif>")
                                    Case ImageTypes.ATTACHMENT

                                        Dim strIconPath As String = "images/icons/" & System.IO.Path.GetExtension(document("Filename")).ToUpper.Replace(".", "") & ".gif"
                                        If System.IO.File.Exists(Server.MapPath(strIconPath)) Then
                                            sb.Append("<img height=16 width=16 src=" & strIconPath & ">")
                                        Else
                                            sb.Append("<img src=Images/file.gif>")
                                        End If

                                End Select

                                sb.Append(Space(2) & document("ImageID") & "</td>" & Space(2))

                                sb.Append("<td width=7 nowrap></td><td width=100%>" & document("Filename") & Space(1))

                                Dim attributes As New DataView(dsRelatedDocuments.Tables(RELATED_DOCUMENTS.Attributes))
                                attributes.RowFilter = "ImageID='" & document("ImageID") & "'"

                                sb.Append(BuildAttributes(attributes))

                                sb.Append("</td></tr>")

                                x += 1

                            Next

                            sb.Append("</div>")

                            divCounter += 1

                        Next

                        sb.Append("</table>")

                        lblDocs.Text = sb.ToString

                    Else

                        lblDocs.Text = "No related documents were found."

                    End If

                End If

            Else

                lblDocs.Text = "Invalid document number. This document may have been deleted or processed by a system service."

            End If

        Catch ex As Exception

            lblDocs.Text = "An error has occured while loading related documents. Our support staff has been notified of the error."

            Dim conMaster As SqlClient.SqlConnection
            conMaster = Functions.BuildMasterConnection
            Functions.LogException(mobjUser.UserId.ToString, mobjUser.AccountId.ToString, "", "Related documents error: " & ex.Message, ex.StackTrace, Server.MachineName, "", Request.UrlReferrer.ToString, conMaster, True)

        Finally

            If Me.mconSqlImage.State <> ConnectionState.Closed Then
                Me.mconSqlImage.Close()
                Me.mconSqlImage.Dispose()
            End If

        End Try

    End Sub

    Private Function BuildAttributes(ByVal dv As DataView) As String

        If dv.Count = 0 Then Return ""

        Dim sbAttributesTable As New System.Text.StringBuilder

        sbAttributesTable.Append("<TABLE class=attributes>")

        Dim counter As Integer = 0

        Dim intColumns As Integer = 2

        For x As Integer = 0 To dv.Count - 1

            If dv(x)("AttributeValue") <> "" Then

                If counter Mod 3 = 0 Then sbAttributesTable.Append("<tr>")

                For currentAttribute As Integer = x To x + (intColumns - 1)

                    If currentAttribute <= dv.Count - 1 Then

                        Dim AttributeValue As String = dv(currentAttribute)("AttributeValue")
                        Dim AttributeName As String = dv(currentAttribute)("AttributeName")
                        'Dim AttributeId As Integer = dv(currentAttribute)("AttributeId")

                        Select Case dv(currentAttribute)("AttributeDataType")
                            Case "Date"
                                AttributeValue = PadDate(CDate(AttributeValue).ToShortDateString)
                        End Select


                        'TODO: " causes script error when inside attribute value

                        'Dim searchAttributeValue As String = AttributeValue

                        'If searchAttributeValue.IndexOf("'") > -1 Or searchAttributeValue.IndexOf("""") > -1 Then
                        '    searchAttributeValue = searchAttributeValue.Replace("'", "\' ")
                        'End If

                        'sbAttributesTable.Append("<TD nowrap onclick=""showSearch(this);"" onmouseout=""hideDrop(this);"" onmouseover=&#34; showDrop(this," & AttributeId & ",'" & searchAttributeValue & "'); &#34;>" & AttributeName & ": " & AttributeValue & "</TD>")
                        'cellId += 1

                        If dv(currentAttribute)("CommonMatch") Then
                            sbAttributesTable.Append("<TD nowrap class=match>" & AttributeName & ": " & AttributeValue & "</TD>")
                        Else
                            sbAttributesTable.Append("<TD nowrap>" & AttributeName & ": " & AttributeValue & "</TD>")
                        End If
                        'sbAttributesTable.Append("<TD nowrap id=t" & cellId & " onmouseover=""showDrop('t" & cellId & "');"">" & AttributeName & ": " & AttributeValue & "</TD>")
                        'cellId += 1

                        x = currentAttribute

                    Else

                        Exit For

                    End If

                Next

                If counter Mod 3 = 0 Then sbAttributesTable.Append("</tr>")

            End If

        Next

        sbAttributesTable.Append("</TABLE>")

        Return sbAttributesTable.ToString

    End Function

End Class

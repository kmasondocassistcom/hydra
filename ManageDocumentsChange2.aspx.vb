Partial Class ManageDocumentsChange2
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region



    Private mobjUser As Accucentric.docAssist.Web.Security.User
    Private mconSqlImage As SqlClient.SqlConnection

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Not Page.IsPostBack Then
            txtDocumentName.Text = Session("mAdminDocumentName")
            SetVersionMode(Session("mAdminDocumentID"))

        End If

    End Sub
  

    Private Function SetVersionMode(ByVal iDocumentID As Integer) As Integer
        Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
        Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))

        Me.mobjUser.AccountId = guidAccountId
        Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
        Me.mconSqlImage.Open()

        Dim cmdSql As SqlClient.SqlCommand

        Dim dtsql As New DataTable
        cmdSql = New SqlClient.SqlCommand("acsAdminDocumentVersionModeGet", Me.mconSqlImage)
        cmdSql.CommandType = CommandType.StoredProcedure
        cmdSql.Parameters.Add("@DocumentID", iDocumentID)
        Dim dasql As New SqlClient.SqlDataAdapter(cmdSql)
        dasql.Fill(dtsql)


        If dtsql.Rows.Count > 0 Then
            ddlVersion.SelectedValue = dtsql.Rows(0).Item(0)
        Else
            ddlVersion.SelectedValue = "0"
        End If

    End Function


    'Private Function GetDocumentQuality(ByVal iDocumentID As Integer) As Integer
    '    Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
    '    Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))

    '    Me.mobjUser.AccountId = guidAccountId
    '    Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
    '    Me.mconSqlImage.Open()

    '    Dim cmdSql As SqlClient.SqlCommand

    '    Dim dtsql As New DataTable
    '    cmdSql = New SqlClient.SqlCommand("acsAdminDocumentQualityGet", Me.mconSqlImage)
    '    cmdSql.CommandType = CommandType.StoredProcedure
    '    cmdSql.Parameters.Add("@DocumentID", iDocumentID)
    '    Dim dasql As New SqlClient.SqlDataAdapter(cmdSql)
    '    dasql.Fill(dtsql)


    '    If dtsql.Rows.Count > 0 Then
    '        cmbQuality.SelectedValue = dtsql.Rows(0).Item("Quality")
    '    End If



    'End Function

    'Private Sub SelectFoldersAssigned(ByVal dtsql As DataTable)

    '    Dim dr As DataRow

    '    lstFolders.Items.Clear()
    '    For Each dr In dtsql.Rows
    '        Dim lstitem As New System.Web.UI.WebControls.ListItem

    '        lstitem.Value = dr("FolderID")
    '        lstitem.Text = dr("FolderName")
    '        lstitem.Selected = dr("Selected")
    '        lstFolders.Items.Add(lstitem)
    '    Next


    'End Sub

    Private Sub lnkFinish_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If DocumentExists(txtDocumentName.Text) And (txtDocumentName.Text.ToUpper <> Session("mAdminDocumentName").ToString.ToUpper) Then
            lblError.Text = "Document " & txtDocumentName.Text & " already exists."
            Exit Sub
        End If

        Session("VersionMode") = ddlVersion.SelectedValue
        'If Session("FoldersSelected").rows.count <= 0 Then
        '    lblError.Text = "You must select at least one category."
        '    Exit Sub
        'End If

        lblError.Text = ""
        Session("mAdminDocumentName") = txtDocumentName.Text
        Response.Redirect("ManageDocumentsAttributesChange.aspx")

    End Sub

    'Private Function FoldersSelected() As DataTable
    '    Dim dtFolders As New DataTable
    '    Dim dcID As New DataColumn("FolderID", GetType(System.Int64), Nothing)
    '    dtFolders.Columns.Add(dcID)


    '    Dim iCount As Integer = lstFolders.Items.Count
    '    Dim x


    '    For x = 0 To iCount - 1
    '        If lstFolders.Items(x).Selected = True Then
    '            Dim dr As DataRow
    '            dr = dtFolders.NewRow
    '            dr("FolderID") = lstFolders.Items(x).Value
    '            dtFolders.Rows.Add(dr)
    '        End If

    '    Next

    '    Return dtFolders

    'End Function


    Private Function DocumentExists(ByVal strDocumentName As String) As Boolean
        Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
        Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))

        Me.mobjUser.AccountId = guidAccountId
        Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
        Me.mconSqlImage.Open()

        Dim cmdSql As SqlClient.SqlCommand

        Dim dtsql As New DataTable
        cmdSql = New SqlClient.SqlCommand("acsAdminDocumentCheck", Me.mconSqlImage)
        cmdSql.CommandType = CommandType.StoredProcedure
        cmdSql.Parameters.Add("@DocumentName", strDocumentName)
        Dim dasql As New SqlClient.SqlDataAdapter(cmdSql)
        dasql.Fill(dtsql)


        If dtsql.Rows.Count > 0 Then
            Return True
        Else
            Return False

        End If
    End Function

    Private Sub btnNext_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnNext.Click
        If DocumentExists(txtDocumentName.Text) And txtDocumentName.Text <> Session("mAdminDocumentName") Then
            lblError.Text = "Document " & txtDocumentName.Text & " already exists."
            Exit Sub
        End If

        Session("VersionMode") = ddlVersion.SelectedValue
        

        lblError.Text = ""
        Session("mAdminDocumentName") = txtDocumentName.Text
        Response.Redirect("ManageDocumentsAttributesChange.aspx")

    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("ManageDocuments.aspx")

    End Sub
End Class

<%@ Register TagPrefix="uc1" TagName="TopRight" Src="Controls/TopRight.ascx" %>
<%@ Register TagPrefix="uc1" TagName="docHeader" Src="Controls/docHeader.ascx" %>
<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="Controls/MenuBar.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="DocumentStatus.aspx.vb" Inherits="docAssistWeb.DocumentStatus"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
  <HEAD>
		<title>DocumentStatus</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
  </HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<TABLE height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TBODY>
					<TR>
						<TD width="100%" colSpan="5">
							<uc1:docHeader id="docHeader" runat="server"></uc1:docHeader></TD>
					</TR>
					<TR vAlign="middle" height="100%">
						<TD class="Navigation1" height="100%"></TD>
						<TD vAlign="top" align="center" height="100%"><BR>
							<table cellSpacing="0" cellPadding="0" width="100%" border="0">
								<tr>
									<td><IMG height="1" src="dummy.gif" width="35"></td>
								</tr>
							</table>
							<table height="100%" cellSpacing="0" cellPadding="0" width="100%">
					</TR>
					<tr height="50%">
					</tr>
					<tr height="15" width="100%">
						<td align="right"></td>
						<td><img src="dummy.gif" height="1" width="900"></td>
						<td></td>
					</tr>
					<tr height="15" width="100%">
						<td style="HEIGHT: 31px"></td>
						<TD style="HEIGHT: 31px" vAlign="middle">
							<asp:Label id="Label1" runat="server" Font-Size="8.25pt" Font-Names="Verdana" Height="3px">Document:</asp:Label>
							<asp:DropDownList id="ddlDocument" runat="server" Font-Size="8.25pt" Font-Names="Verdana" Width="160px"></asp:DropDownList>&nbsp;
							<asp:Label id="Label2" runat="server" Font-Size="8.25pt" Font-Names="Verdana">Status:</asp:Label>
							<asp:DropDownList id="ddlStatus" runat="server" Font-Size="8.25pt" Font-Names="Verdana" Width="88px">
<asp:ListItem Value="0">Pending</asp:ListItem>
<asp:ListItem Value="1">Approved</asp:ListItem>
<asp:ListItem Value="3">Cancelled</asp:ListItem>
							</asp:DropDownList>&nbsp;
							<asp:Label id="Label3" runat="server" Font-Size="8.25pt" Font-Names="Verdana">From:</asp:Label>
							<asp:TextBox id="txtFrom" runat="server" Font-Size="8.25pt" Font-Names="Verdana" Width="120px">9/1/2005</asp:TextBox>&nbsp;<A 
            id=anchor1xx onclick="" name=anchor1xx ?><IMG 
            src="Images/calendar.gif" border=0></A><asp:Label ID="Label4" runat="server" Font-Names="Verdana" Font-Size="8.25pt">Through:</asp:Label>
							<asp:TextBox id="txtThrough" runat="server" Font-Size="8.25pt" Font-Names="Verdana" Width="120px">10/13/2005</asp:TextBox>&nbsp;
							<asp:Button id="Button1" runat="server" Text="Search"></asp:Button></TD>
						<TD width="50%" style="HEIGHT: 31px"></TD>
					<TR height="15" width="100%">
						<td style="HEIGHT: 21px" width="50%"></td>
						<td style="HEIGHT: 21px">
<asp:Label id=lblResults runat="server" Font-Names="Verdana" Font-Size="8.25pt"></asp:Label></td>
						<td style="HEIGHT: 21px" width="50%"></td>
					</TR>
					<tr height="15" width="100%">
						<td style="WIDTH: 393px" width="393"></td>
						<td style="HEIGHT: 17px"></td>
						<td width="50%"></td>
					</tr>
					<tr height="15" width="100%">
						<td style="WIDTH: 393px" width="393"></td>
						<td></td>
						<td width="50%"></td>
					</tr>
					<tr height="15" width="100%">
						<td style="WIDTH: 393px" width="395"></td></TD>
						<td style="HEIGHT: 17px"></td>
						<td width="50%"></td>
					</tr>
					<tr height="15" width="100%">
						<td style="WIDTH: 395px" width="395"></td>
						<td align="right"></td>
						<td width="50"></td>
					</tr>
					<tr height="100%">
						<td></td>
					</tr>
				</tr>
			</TABLE>
			<P><BR>
				<BR>
			</P></TD>
			<TD class="RightContent" height="100%">&nbsp;</TD></TR>
			<TR>
				<TD class="PageFooter" vAlign="bottom" bgColor="#cccccc" colSpan="3">
					<uc1:TopRight id="TopRight1" runat="server"></uc1:TopRight></TD>
			</TR>
			<TR height="1">
				<TD style="BACKGROUND-COLOR: #959394" width="20"><IMG height="1" src="images/spacer.gif" width="20"></TD>
				<TD style="BACKGROUND-COLOR: #959394" width="100%"><IMG height="1" src="images/spacer.gif" width="100%"></TD>
				<TD style="BACKGROUND-COLOR: #959394" width="20"><IMG height="1" src="images/spacer.gif" width="20"></TD>
			</TR></TBODY></TABLE>
		</form>

		<script language="javascript" src="CalendarPopup.js"></script>

		<script language="javascript">
		
	
			document.all.docHeader_btnWorkflow.src = "./Images/toolbar_workflow_grey.gif";
			
			</script>
	</body>
</HTML>

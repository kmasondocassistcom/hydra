Partial Class ManageAttributeChange2
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private mobjUser As Accucentric.docAssist.Web.Security.User
    Private mconSqlImage As SqlClient.SqlConnection

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        txtLength.Attributes.Add("onkeypress", "catchSubmit();return nb_numericOnly(event)")
        txtAttributeName.Attributes("onkeypress") = "catchSubmit();"
        txtFormat.Attributes("onkeypress") = "catchSubmit();"

        If Not Page.IsPostBack Then

            Dim lstItem As New ListItem
            lstItem.Text = "AlphaNumeric"
            lstItem.Value = "String"
            cmbDataType.Items.Add(lstItem)
            cmbDataType.Items.Add("Numeric")
            cmbDataType.Items.Add("Date")

            'lstItem = New ListItem
            'lstItem.Text = "List"
            'lstItem.Value = "ListView"
            'cmbDataType.Items.Add(lstItem)

            txtAttributeName.MaxLength = 40

            LoadAttributeValues()

            btnFinish.ImageUrl = "Images/btn_finish.gif"
            
        End If

    End Sub

    Private Function LoadAttributeValues()

        Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
        Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))

        Me.mobjUser.AccountId = guidAccountId
        Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
        Me.mconSqlImage.Open()

        Dim cmdSql As SqlClient.SqlCommand

        Dim dtsql As New DataTable
        cmdSql = New SqlClient.SqlCommand("acsAdminAttributeValues", Me.mconSqlImage)
        cmdSql.CommandType = CommandType.StoredProcedure
        cmdSql.Parameters.Add("@AttributeID", Session("mAdminAttributeID"))
        Dim dasql As New SqlClient.SqlDataAdapter(cmdSql)
        dasql.Fill(dtsql)


        If dtsql.Rows.Count > 0 Then

            If dtsql.Rows(0).Item("NumCols") = 1 Then
                rdoSingle.Checked = True
            Else
                rdoMultiple.Checked = True
            End If

            txtAttributeName.Text = dtsql.Rows(0).Item("AttributeName")
            If dtsql.Rows(0).Item("AttributeDataType") = "ListView" Then
                cmbDataType.SelectedIndex = 3
            Else
                cmbDataType.SelectedValue = dtsql.Rows(0).Item("AttributeDataType")
            End If
            If dtsql.Rows(0).Item("Length") = 0 Then txtLength.Text = "" Else txtLength.Text = dtsql.Rows(0).Item("Length")
            txtFormat.Text = dtsql.Rows(0).Item("DataFormat")

        End If

        If cmbDataType.SelectedItem.Text = "List" Then
            rdoMultiple.Visible = True
            rdoSingle.Visible = True
        Else
            rdoMultiple.Visible = False
            rdoSingle.Visible = False
        End If

        rdoMultiple.Visible = False
        rdoSingle.Visible = False

        If cmbDataType.SelectedItem.Text = "List" Or cmbDataType.SelectedItem.Text = "Date" Then
            txtLength.Visible = False
            txtFormat.Visible = False
            lblFormat.Visible = False
            lblLength.Visible = False
            txtLength.Text = ""
            txtFormat.Text = ""
        Else
            txtLength.Visible = True
            txtFormat.Visible = True
            lblFormat.Visible = True
            lblLength.Visible = True
        End If

    End Function

    Private Sub cmbDataType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbDataType.SelectedIndexChanged

        If cmbDataType.SelectedItem.Text = "List" Or cmbDataType.SelectedItem.Text = "Date" Then
            lblFormat.Visible = False
            lblLength.Visible = False
            txtFormat.Visible = False
            txtLength.Visible = False
            btnFinish.ImageUrl = "Images/btn_nextadmin.gif"

            If cmbDataType.SelectedItem.Text = "List" Then
                rdoMultiple.Visible = True
                rdoSingle.Visible = True
            Else
                rdoMultiple.Visible = False
                rdoSingle.Visible = False
            End If


        Else
            lblFormat.Visible = True
            lblLength.Visible = True
            txtFormat.Visible = True
            txtLength.Visible = True
            btnFinish.ImageUrl = "Images/btn_finish.gif"

            rdoMultiple.Visible = False
            rdoSingle.Visible = False

        End If

    End Sub

    Private Sub lnkFinish_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        If txtAttributeName.Text = "" Then
            lblError.Text = "Attribute name is required."
            Exit Sub
        End If


        If AttributeNameExists() And txtAttributeName.Text <> Session("mAdminAttributeName") Then
            lblError.Text = "Attribute name already exists."
            Exit Sub
        Else

            If cmbDataType.SelectedValue = "List" Then
                'Response.Cookies("Admin")("AttributeName") = txtAttributeName.Text
                'Response.Cookies("Admin")("AttributeLength") = txtLength.Text
                'Response.Cookies("Admin")("AttributeFormat") = txtFormat.Text
                Session("mAdminAttributeName") = txtAttributeName.Text
                Session("mAdminAttributeLength") = txtLength.Text
                Session("mAdminAttributeFormat") = txtFormat.Text
                Response.Redirect("ManageAttributesChangeList.aspx")
            Else
                SaveAttributeChange()
                Response.Redirect("ManageAttributes.aspx")
            End If
        End If

    End Sub

    Private Function AttributeNameExists() As Boolean
        Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
        Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))

        Me.mobjUser.AccountId = guidAccountId
        Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
        Me.mconSqlImage.Open()

        Dim cmdSql As SqlClient.SqlCommand

        Dim dtsql As New DataTable
        cmdSql = New SqlClient.SqlCommand("acsAdminAttributeCheck", Me.mconSqlImage)
        cmdSql.CommandType = CommandType.StoredProcedure
        cmdSql.Parameters.Add("@AttributeName", txtAttributeName.Text)
        Dim dasql As New SqlClient.SqlDataAdapter(cmdSql)
        dasql.Fill(dtsql)


        If dtsql.Rows.Count > 0 Then
            Return True
        Else
            Return False

        End If

    End Function

    Private Function SaveAttributeChange() As Boolean

        Dim cmdSql As SqlClient.SqlCommand
        Dim dtsql As New DataTable
        cmdSql = New SqlClient.SqlCommand("acsAdminAttributeChange", Me.mconSqlImage)
        cmdSql.CommandType = CommandType.StoredProcedure
        cmdSql.Parameters.Add("@AttributeID", Session("mAdminAttributeID"))
        cmdSql.Parameters.Add("@AttributeName", txtAttributeName.Text)
        cmdSql.Parameters.Add("@AttributeDataType", cmbDataType.SelectedValue)
        cmdSql.Parameters.Add("@DataFormat", txtFormat.Text)
        cmdSql.Parameters.Add("@Length", txtLength.Text)

        If rdoSingle.Checked Then
            cmdSql.Parameters.Add("@NumCols", 1)
        Else
            cmdSql.Parameters.Add("@NumCols", 2)
        End If

        cmdSql.ExecuteNonQuery()

    End Function

    Private Sub btnFinish_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFinish.Click

        If txtAttributeName.Text = "" Then
            lblError.Text = "Attribute name is required."
            Exit Sub
        End If

        If AttributeNameExists() And (txtAttributeName.Text.ToUpper <> Session("mAdminAttributeName").ToString.ToUpper) Then
            lblError.Text = "Attribute name already exists."
            Exit Sub
        End If

        If cmbDataType.SelectedItem.Text <> "Date" And cmbDataType.SelectedItem.Text <> "List" Then

            If cmbDataType.SelectedValue <> "List" Then
                If txtLength.Text <> "" Then
                    If txtLength.Text < 1 Or txtLength.Text > 75 Then
                        lblError.Text = "Invalid length. Please enter a number between 1 and 75."
                        Exit Sub
                    End If
                Else
                    lblError.Text = "Invalid length. Please enter a number between 1 and 75."
                    Exit Sub
                End If

            End If
        End If

        If cmbDataType.SelectedItem.Text = "List" Then
            'Session("mAdminAttributeName") = txtAttributeName.Text
            'Session("mAdminAttributeLength") = txtLength.Text
            'Session("mAdminAttributeFormat") = ""
            'Response.Redirect("ManageAttributesChangeList.aspx")
            SaveAttributeChange()
            Response.Redirect("ManageAttributes.aspx")
        Else
            SaveAttributeChange()
            Response.Redirect("ManageAttributes.aspx")
        End If

    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("ManageAttributes.aspx")
    End Sub
End Class

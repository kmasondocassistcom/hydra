<%@ Register TagPrefix="cc1" Namespace="OptGroupLists" Assembly="OptGroupLists" %>

<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="WorkflowReview.aspx.vb"
    Inherits="docAssistWeb.WorkflowReview" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title></title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <link href="Styles.css" type="text/css" rel="stylesheet">
</head>
<body bottommargin="0" leftmargin="0" topmargin="0" rightmargin="0">
    <form id="Form1" method="post" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="updatePanel" runat="server">
        <ContentTemplate>
            <table class="PageContent" id="tblSubmit" height="5" cellspacing="0" cellpadding="0"
                width="240" border="0">
                <tr>
                    <td height="35">
                    </td>
                    <td valign="middle">
                        <asp:Label ID="lblWorkflow" runat="server" Font-Bold="True"></asp:Label><br>
                        <asp:Label ID="lblQueueName" runat="server" Visible="True"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td valign="bottom">
                        <b>Select a review&nbsp;action:</b><br>
                        <cc1:OptGroupDDL ID="ddlAction" runat="server" Width="100%" AutoPostBack="True">
                        </cc1:OptGroupDDL>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td valign="top">
                        <strong>
                            <asp:Label ID="lblQueue" runat="server" Font-Bold="True" Font-Size="8.25pt" Font-Names="Verdana">Next Queue:</asp:Label><asp:Label
                                ID="lblAction" runat="server"></asp:Label></strong><br>
                        <cc1:OptGroupDDL ID="ddlQueue" runat="server" Width="100%" Enabled="False" AutoPostBack="True">
                        </cc1:OptGroupDDL>
                    </td>
                </tr>
                <tr>
                    <td style="height: 32px">
                    </td>
                    <td style="height: 32px">
                        <p>
                            <b>
                                <asp:CheckBox ID="chkUrgent" runat="server" AutoPostBack="True" Enabled="False" Text="Urgent"
                                    Height="4px"></asp:CheckBox><br>
                            </b>
                            <asp:Label ID="lblNotes" runat="server" Font-Bold="True" Font-Size="8.25pt" Font-Names="Verdana">Notes:</asp:Label></p>
                    </td>
                </tr>
                <tr valign="top" height="100%">
                    <td>
                    </td>
                    <td>
                        <asp:TextBox ID="txtNotes" runat="server" Width="100%" Enabled="False" Height="55px"
                            TextMode="MultiLine" AutoPostBack="True"></asp:TextBox>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:Literal ID="litAction" runat="server"></asp:Literal><asp:Literal ID="litQueue"
        runat="server"></asp:Literal><asp:Literal ID="litUrgent" runat="server"></asp:Literal>
    <asp:Literal ID="litRoute" runat="server"></asp:Literal>

    <script src="scripts/cookies.js" language="javascript">
			var resultbln = readCookie("results");
			if (resultbln == "1") { window.history.back(); }
    </script>

    <script language="javascript">
			function PassWorkflowNote()
			{
				parent.frames["frameAttributes"].WorkflowNoteSet(document.all.txtNotes.value);
			}
    </script>

    </form>
</body>
</html>

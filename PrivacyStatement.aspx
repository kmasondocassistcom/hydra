<%@ Page Language="vb" AutoEventWireup="false" Codebehind="PrivacyStatement.aspx.vb" Inherits="docAssistWeb.PrivacyStatement"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML xmlns:o>
	<HEAD>
		<title>Privacy & Security</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body scroll="yes">
		<form id="Privacy" method="post" runat="server">
			<table cellSpacing="0" cellPadding="0" width="560" align="left" border="0">
				<tr>
					<td colspan="3" align="right"><IMG src="Images/doc_header_logo.gif"></td>
				</tr>
				<tr>
					<td><img src="spacer.gif" height="1" width="3"></td>
					<td width="100%">
						<font face="Verdana" size="1">
							<P></P>
							<P></P>
							<STRONG>Privacy and Security</STRONG>
							<BR>
							<BR>
							<STRONG>docAssist Privacy Policy<BR>
							</STRONG>We respect the privacy of anyone who uses our docAssist online 
							applications (docAssist.com).<BR>
							docAssist gathers important information from our visitors and customers. We 
							publish this Privacy Policy to communicate how we gather and use information 
							about you, how we protect it, and how you can participate in its protection. 
							docAssist provides its service to you subject to the docAssist Terms of Service 
							and this Privacy Policy, which shall be deemed part of the Terms of Service. We 
							reserve the right to change this Privacy Policy. If we change it, we will 
							provide notification of the change at least thirty (30) business days before 
							the change takes effect and include directions for how you can respond to the 
							change.
							<BR>
							<BR>
							<STRONG>Collected Information<BR>
							</STRONG>We require people who register to use docAssist online applications 
							(the "Services") to give us contact information. Depending on the services 
							chosen, we ask for contact and account information, such as name, company name, 
							address, phone number, e-mail address, password and the number of users in the 
							organization who will be using the Services, in addition to financial and 
							billing information, such as billing name, and address. When you express 
							interest in additional information, when you register for the Services, or 
							after you become a user, we may ask for additional personal information, such 
							as title, department name, fax number or additional company information, such 
							as annual revenues, number of employees or industry. You can opt out of 
							providing additional information by not entering it when asked.
							<P></P>
							<P>docAssist does not distribute or share customer e-mail addresses, except as may 
								be required by law.</P>
							<P>docAssist uses the information we collect to set up the Services for individuals 
								and their organizations. We may also use the information to contact customers 
								and prospects to further discuss their interest in our company, the Services we 
								provide and ways we can improve them, and to send information such as 
								announcements of promotions and events regarding our company or partners. We 
								may also e-mail a customer newsletter and updates about the Services or the 
								company. You can opt out of receiving these communications by replying to them 
								at the time they are distributed or at any time by e-mailing <A href="mailto:support@docassist.com">
									support@docassist.com</A>.</P>
							<P>All financial and billing information that we collect through our docAssist Web 
								site and the Services is used solely to bill for the Services. docAssist does 
								not use this billing information for marketing or promotional purposes. Except 
								as we explicitly state at the time we request information, or as provided for 
								in the docAssist Terms of Service, we do not disclose to any third party the 
								information provided.</P>
							<P>In the event that docAssist is billed via credit card, docAssist uses a 
								third-party intermediary to manage credit card processing. This intermediary is 
								solely a link in the distribution chain and is not permitted to store, retain, 
								or use the information provided, except for the sole purpose of credit card 
								processing.</P>
							<P>Other third parties, such as content providers, may provide information on a 
								docAssist Web site, but they are not permitted to collect any information, nor 
								does docAssist share any personally identifiable user information with these 
								parties.</P>
							<P>docAssist customers use docAssist online applications to host data and 
								information. docAssist will not review, share, distribute, print, or reference 
								any such data except as provided in the docAssist Terms of Service, or as may 
								be required by law. We will view or access individual records only with your 
								permission (for example, to resolve a problem or support issue). Of course, 
								users of the Services are responsible for maintaining the confidentiality and 
								security of their user registration and passwords.</P>
							<P>docAssist may also collect certain information from visitors to a docAssist Web 
								site and customers of the Services, such as Internet addresses. This 
								information is logged to help diagnose technical problems and to administer the 
								docAssist Web site and Services in order to constantly improve quality. We may 
								also track and analyze non-identifying and aggregate usage and volume 
								statistical information from visitors and customers.</P>
							<P><STRONG>Cookies</STRONG><BR>
								Cookies are files Web browsers place on a computer's hard drive. They are used 
								to help us authenticate clients and provide timesaving shortcuts and 
								preferences. docAssist uses a persistent encrypted cookie, if the client 
								requests, to save and retrieve individual authentication information as well as 
								other per-client device preferences. docAssist issues a mandatory session 
								cookie to each user of the Services only to record encrypted authentication 
								information for the duration of a specific session. If you reject this cookie, 
								access to and usage of the Services will be denied.
							</P>
							<P><STRONG>Security</STRONG><BR>
								docAssist has security measures in place for the Services to help protect 
								against the loss, misuse and alteration of the data under docAssistís control. 
								When the Services are accessed using Microsoft Internet Explorer versions 6.0 
								or higher, Secure Socket Layer (SSL) technology protects information using both 
								server authentication and data encryption to help ensure data is safe, secure 
								and available only to you. docAssist also implements an advanced security 
								method based on dynamic data and encoded session identifications and hosts the 
								Services in a secure server environment using a firewall and other advanced 
								technology to prevent interference or access from outside intruders. Finally, 
								docAssist requires unique user names and passwords that must be entered each 
								time a customer logs on. These safeguards help prevent unauthorized access, 
								maintain data accuracy and ensure the appropriate use of data.
								<BR>
								<SPAN class="blacksubhead1"><STRONG>
										<BR>
										<BR>
										Correcting and Updating Your Information</STRONG></SPAN><BR>
								To update or change registration information, to update billing information, or 
								to discontinue the Services, e-mail <A href="mailto:support@docassist.com"><SPAN style="LINE-HEIGHT: 130%; mso-ansi-font-size: 9.0pt; mso-bidi-font-size: 9.0pt">
									support@docassist.com</A>.</SPAN>
							</P>
							<P style="LINE-HEIGHT: 130%"><STRONG><SPAN class="blacksubhead1">Additional Information</SPAN><BR>
								</STRONG>Questions regarding this Privacy Policy or the security practices of 
								the Services should be directed to docAssistís Security Administrator by 
								e-mailing <A href="mailto:security@docassist.com">security@docassist.com</A>
							. </font></P>
					</td>
					<td><img src="spacer.gif" height="1" width="3"></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>

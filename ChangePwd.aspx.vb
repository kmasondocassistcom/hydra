Partial Class ChangePwd
    Inherits System.Web.UI.Page

    Dim objUser As Accucentric.docAssist.Web.Security.User
    Dim strUserEmail As String


#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try
            If Request.Cookies("docAssist") Is Nothing Then Response.Redirect("default.aspx")

            objUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
            strUserEmail = objUser.EMailAddress

            lblReq.Text = Functions.PasswordStrongValidationMessage

        Catch ex As Exception

        End Try

    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click

        Dim objEncrypt As New Encryption.Encryption
        Dim strPassword As String
        Dim strConnection As String = ConfigurationSettings.AppSettings("Connection")
        Dim conSql As New SqlClient.SqlConnection(Functions.ConnectionString())

        lblMsg.Text = ""

        If txtCurrent.Text.Length = 0 Or txtNewPass.Text.Length = 0 Or txtConfirmPass.Text.Length = 0 Then
            lblMsg.Text = "You must enter text into all the password fields."
            Exit Sub
        End If

        Try

            If conSql.State <> ConnectionState.Open Then conSql.Open()

            Dim cmdSql As New SqlClient.SqlCommand("SELECT password FROM Users WHERE EmailAddress = @EMailAddress", conSql)
            cmdSql.Parameters.Add("@EMailAddress", strUserEmail)

            strPassword = objEncrypt.Decrypt(cmdSql.ExecuteScalar)

            'Check if current password is correct
            If txtCurrent.Text <> strPassword Then
                lblMsg.Text = "Invalid current password."
                Exit Sub
            End If

            'Check if passwords match
            If txtNewPass.Text <> txtConfirmPass.Text Then
                lblMsg.Text = "New passwords must match."
                Exit Sub
            End If

            If Functions.PasswordStrongValidation(txtConfirmPass.Text) = False Then
                lblMsg.Text = Functions.PasswordStrongValidationMessage
                Exit Sub
            End If

            'Check if password is different than new one
            If txtCurrent.Text = txtNewPass.Text Then
                lblMsg.Text = "New password must be different than current password."
                Exit Sub
            End If

            Functions.ChangePassword(Me.objUser.UserId.ToString, Me.objUser.AccountId.ToString, objEncrypt.Encrypt(txtNewPass.Text), conSql)

            lblMsg.Text = "Password successfully changed."

        Catch ex As Exception

#If DEBUG Then
            lblMsg.Text = ex.Message
#Else
            lblMsg.Text = "An error has occured while changing your password. If the problem continues please contact support."
#End If

        Finally

            If conSql.State <> ConnectionState.Closed Then
                conSql.Close()
                conSql.Dispose()
            End If

        End Try

    End Sub

End Class

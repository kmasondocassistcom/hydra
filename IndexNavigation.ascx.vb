Imports Accucentric.docAssist
Imports Accucentric.docAssist.Data.Images

Partial Class IndexNavigation
    Inherits System.Web.UI.UserControl

    Private mintVersionId As Integer
    Private mobjUser As Accucentric.docAssist.Web.Security.User
    Protected WithEvents anchorPrint As System.Web.UI.HtmlControls.HtmlAnchor

    Private mconSqlImage As SqlClient.SqlConnection

    Private mcookieSearch As cookieSearch
    Protected WithEvents imgNotes As System.Web.UI.HtmlControls.HtmlAnchor
    Protected WithEvents A1 As System.Web.UI.HtmlControls.HtmlAnchor
    Private mcookieImage As cookieImage

    Protected WithEvents tdMag As System.Web.UI.HtmlControls.HtmlTableCell

    Private CurrentRow As Integer

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Public Function DocumentSecurityLevel(ByVal VersionId As Integer) As Web.Security.SecurityLevel
        Return Functions.DocumentSecurityLevel(VersionId, Me.mobjUser, Me.mconSqlImage)
    End Function

    Public Function DocumentSecurityLevelByVersionId(ByVal intVersionId As Integer) As Web.Security.SecurityLevel
        Return Functions.DocumentSecurityLevelByVersionId(Session("intDocumentId"), Me.mobjUser, Me.mconSqlImage)
    End Function

    Public Function DocumentEmailExportSecurityLevel(ByVal intDocumentId As Integer) As Web.Security.SecurityLevelEmailExport
        Return Functions.EmailExportSecurityLevel(intDocumentId, Me.mobjUser, Me.mconSqlImage)
    End Function

    Public Function DocumentPrintSecurityLevel(ByVal intDocumentId As Integer) As Web.Security.SecurityLevelPrint
        Return Functions.PrintSecurityLevel(intDocumentId, Me.mobjUser, Me.mconSqlImage)
    End Function

    Private Sub DisableNextButton()

        If CurrentRow = Session("intTotalDocuments") Then
            btnNext.ImageUrl = "Images/results_next_off.gif"
            btnNext.Enabled = False
        Else
            btnNext.ImageUrl = "Images/results_next.gif"
            btnNext.Enabled = True
        End If

    End Sub

    Private Sub DisablePreviousButton()

        If CurrentRow <= 1 Then
            btnPrevious.ImageUrl = "Images/results_previous_off.gif"
            btnPrevious.Enabled = False
        Else
            btnPrevious.ImageUrl = "Images/results_previous.gif"
            btnPrevious.Enabled = True
        End If

    End Sub


    Private Sub InitializeViewer(ByVal intVersionId As Integer)
        '
        ' Get the version
        '
        Dim dtVersion As New Data.Images.ImageVersion
        dtVersion.GetAllVersionsByVersionId(intVersionId, Me.mconSqlImage)
        If dtVersion.Rows.Count > 0 Then
            ddlVersions.DataSource = dtVersion
            ddlVersions.DataValueField = dtVersion.VersionId.ColumnName
            ddlVersions.DataTextField = dtVersion.VersionNum.ColumnName
            ddlVersions.DataBind()
            Dim dsVersions As Accucentric.docAssist.Data.dsVersionControl = Session(intVersionId & "VersionControl")
            ddlVersions.SelectedValue = CType(dsVersions.ImageVersion.Rows(0), Accucentric.docAssist.Data.dsVersionControl.ImageVersionRow).VersionID

            If dtVersion.Rows.Count > 1 Then
                ddlVersions.Enabled = True
            Else
                ddlVersions.Enabled = False
            End If

            ddlVersions.Attributes("onchange") = "var d = new Date();var random = d.getHours()+d.getMinutes()+d.getSeconds()+d.getMilliseconds();document.location.href='Viewer.aspx?VID='+this.options[this.selectedIndex].value+'&'+random;"
            ddlQuality.Attributes("onchange") = "loadQuality(this.options[this.selectedIndex].value);"

            litLatestVID.Text = "<script>var latestVersionId = " & ddlVersions.Items(ddlVersions.Items.Count - 1).Value & ";</script>"

        End If

    End Sub

    Private Function AttachmentMode()

        Try
            Dim col As New Collection
            col.Add(tdDocumentText)
            col.Add(tdFirst)
            col.Add(tdLast)
            col.Add(tdNext)
            col.Add(tdPrevious)
            col.Add(tdZoom)
            col.Add(tdMag)
            col.Add(tdZoomIn)
            col.Add(tdZoomOut)
            col.Add(tdHand)
            col.Add(tdFitBest)
            col.Add(tdFitWidth)
            col.Add(tdFitHeight)
            col.Add(tdPrint)
            col.Add(tdSpace1)
            col.Add(tdSpace2)
            col.Add(tdSpace3)
            col.Add(tdPagesVersion)

            tdDocumentText.Attributes.Add("onclick", "textPopup(0);")

            For Each ctrl As System.Web.UI.HtmlControls.HtmlControl In col
                If Not ctrl Is Nothing Then
                    ctrl.Attributes("style") = "DISPLAY: none"
                    ctrl.Visible = False
                End If
            Next

        Catch ex As Exception

        End Try


    End Function

    Private Function PDFMode()

        Try
            Dim col As New Collection
            'col.Add(tdDocumentText)
            'col.Add(tdFirst)
            'col.Add(tdLast)
            'col.Add(tdNext)
            'col.Add(tdPrevious)
            'col.Add(tdZoom)
            'col.Add(tdMag)
            'col.Add(tdZoomIn)
            'col.Add(tdZoomOut)
            'col.Add(tdHand)
            'col.Add(tdFitBest)
            'col.Add(tdFitWidth)
            'col.Add(tdFitHeight)
            col.Add(tdMail)
            col.Add(tdPrint)
            col.Add(tdSpace1)
            col.Add(tdSpace2)
            col.Add(tdSpace3)

            Label3.Visible = False
            ddlVersions.Visible = False
            Label1.Visible = False
            ddlQuality.Visible = False

            tdDocumentText.Attributes.Add("onclick", "textPopup(1);")

            For Each ctrl As System.Web.UI.HtmlControls.HtmlControl In col
                If Not ctrl Is Nothing Then
                    ctrl.Attributes("style") = "DISPLAY: none"
                    ctrl.Visible = False
                End If
            Next

        Catch ex As Exception

        End Try


    End Function

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try
            Me.mobjUser = BuildUserObject(Request.Cookies("docAssist"))
            Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
            Me.mcookieSearch = New cookieSearch(Me.Page)
            Me.mcookieImage = New cookieImage(Me.Page)
        Catch ex As Exception
            If ex.Message = "User is not logged on" Then
                Response.Redirect("default.aspx")
            End If
        End Try

        If Request.QueryString("WFA") = "1" Then
            Session("Workflow") = True
        Else
            Session("Workflow") = False
        End If

        If Not Page.IsPostBack Then

            AttachmentDelete.Attributes("onclick") = "return confirm('Are you sure you want to delete this attachment?');"

            Dim mSecurityLevel As Web.Security.SecurityLevel
            Dim intVersionId As Integer

            Try

                Select Case CType(Session("FileType"), Functions.FileType)
                    Case Functions.FileType.ATTACHMENT
                        If Request.QueryString("ref") = "pdfviewer" Then
                            PDFMode()
                        Else
                            AttachmentMode()
                        End If
                    Case Else
                        tdDocumentText.Attributes.Add("onclick", "textPopup(0);")
                        tdAttachmentDelete.Visible = False
                End Select

                'Get VersionId of the image to load text from
                If Me.mcookieSearch.VersionID > 0 Then
                    intVersionId = Me.mcookieSearch.VersionID
                    VersionId.Value = intVersionId

                    ' Load the data and save to the session
                    'Dim dsVersionControl As New Accucentric.docAssist.Data.dsVersionControl
                    ''dsVersionControl = Session(intVersionId & "VersionControl")

                    'dsVersionControl = Data.GetVersionControl(intVersionId, Me.mconSqlImage, Me.mobjUser.ImagesUserId)
                    Dim intPageCount As Integer = CType(Parent.FindControl("pageCount"), System.Web.UI.HtmlControls.HtmlInputHidden).Value 'dsVersionControl.ImageDetail.Rows.Count
                    'Session(intVersionId & "VersionControl") = dsVersionControl
                    'Session(intVersionId & "PageCount") = intPageCount
                    'Session(intVersionId & "VersionControl") = dsVersionControl

                    If intPageCount = 1 Then
                        lblPagesOf.Text = " 1 of 1"
                        txtPageNo.Visible = False
                    Else
                        lblPagesOf.Text = " of " & intPageCount.ToString
                    End If

                    litPageCount.Text = "<script>var pageCount = " & intPageCount & ";</script>"

                    Dim intDocumentId As Integer = CType(Parent.FindControl("docId"), System.Web.UI.HtmlControls.HtmlInputHidden).Value 'CType(dsVersionControl.Images.Rows(0), Accucentric.docAssist.Data.dsVersionControl.ImagesRow).DocumentID

                    Dim wfMode As Functions.WorkflowMode
                    Try
                        wfMode = Session("WorkflowMode")
                    Catch ex As Exception

                    End Try

                    'Document notes count
                    Try
                        If Me.mconSqlImage.State <> ConnectionState.Open Then Me.mconSqlImage.Open()
                        Dim cmd As New SqlClient.SqlCommand("SMDocumentNoteInfo", Me.mconSqlImage)
                        cmd.Parameters.Add("@VersionID", Me.mcookieSearch.VersionID)
                        cmd.CommandType = CommandType.StoredProcedure
                        Dim noteCount As Integer = cmd.ExecuteScalar

                        If noteCount > 0 Then
                            litBadge.Text = "<script>badge = " & noteCount & ";</script>"
                        End If

                    Catch ex As Exception
                        Dim conMaster As SqlClient.SqlConnection
                        conMaster = Functions.BuildMasterConnection
                        Functions.LogException(mobjUser.UserId.ToString, mobjUser.AccountId.ToString, "", "Document Note Badge Loader: " & Me.mcookieSearch.VersionID & ex.Message, ex.StackTrace, Server.MachineName, "", Request.UrlReferrer.ToString, conMaster, True)
                    End Try

                    Try

                        'Dim conSqlCompany As New SqlClient.SqlConnection
                        'Try
                        '    'Attachments button
                        '    'companydb(FAAttachmentStatusbyVersionID)
                        '    '@VersionID --returns 1 if attachments exists or 0 if not, use to change the attachment button in the viewer to show presence of attachments

                        '    conSqlCompany = Functions.BuildConnection(Me.mobjUser)
                        '    conSqlCompany.Open()
                        '    Dim cmdSql As New SqlClient.SqlCommand("FAAttachmentStatusbyVersionID", conSqlCompany)
                        '    cmdSql.CommandType = CommandType.StoredProcedure
                        '    cmdSql.Parameters.Add("@VersionID", Me.mcookieSearch.VersionID)

                        '    Dim intAttachmentTotals As Integer = cmdSql.ExecuteScalar

                        '    If cmdSql.ExecuteScalar > 0 Then
                        '        'change image
                        '        'imgAttachments.Src = "Images/paperClipHot.gif"
                        '        imgAttachments.Alt = intAttachmentTotals.ToString & " Attachments"
                        '    End If

                        'Catch ex As Exception
                        '    If conSqlCompany.State <> ConnectionState.Closed Then
                        '        conSqlCompany.Close()
                        '        conSqlCompany.Dispose()
                        '    End If
                        'End Try

                        'SMDocumentNoteInfo()
                        '@VersionID int

                        'Hide save button by default, if it needs to be shown it will be below
                        imgSave.Visible = False
                        SaveCell.Visible = False

                        'Check document level access
                        Select Case DocumentSecurityLevel(Me.mcookieSearch.VersionID)

                            Case Web.Security.Functions.SecurityLevel.NoAccess
                                'Check for workflow
                                tdAttachmentDelete.Visible = False
                                If wfMode = Functions.WorkflowMode.VIEW_ACCESS_REVIEWER_PENDING Or wfMode = Functions.WorkflowMode.ACTION_ACCESS_REVIEWER_PENDING _
                                Or wfMode = Functions.WorkflowMode.REVIEWED Then
                                    imgSave.Visible = True
                                    If wfMode = Functions.WorkflowMode.REVIEWED Then imgSave.Visible = False
                                    SaveCell.Visible = True
                                    btnNext.Visible = False
                                    btnPrevious.Visible = False
                                    NextPrevSpacer.Visible = False
                                    imgAttachments.Visible = False
                                    Response.Cookies("Workflow")("Enabled") = True
                                Else
                                    Response.Redirect("Default.aspx")
                                    Exit Sub
                                End If
                            Case Web.Security.Functions.SecurityLevel.Read
                                'Check for workflow
                                tdAttachmentDelete.Visible = False
                                If wfMode = Functions.WorkflowMode.VIEW_ACCESS_REVIEWER_PENDING Or wfMode = Functions.WorkflowMode.ACTION_ACCESS_REVIEWER_PENDING _
                                Or wfMode = Functions.WorkflowMode.REVIEWED Then
                                    imgSave.Visible = True
                                    SaveCell.Visible = True
                                    btnNext.Visible = False
                                    btnPrevious.Visible = False
                                    NextPrevSpacer.Visible = False
                                    imgAttachments.Visible = True
                                    Response.Cookies("Workflow")("Enabled") = True
                                End If
                            Case Web.Security.Functions.SecurityLevel.Change
                                'Show save
                                imgSave.Visible = True
                                SaveCell.Visible = True
                                imgAttachments.Visible = True
                                tdAttachmentDelete.Visible = False
                            Case Web.Security.Functions.SecurityLevel.Delete
                                'Show save
                                imgSave.Visible = True
                                SaveCell.Visible = True
                                imgAttachments.Visible = True
                                'SaveSpacer.Visible = True
                            Case Web.Security.Functions.SecurityLevel.NoAccess
                                imgSave.Visible = False
                            Case Else
                                'Hide save
                                imgSave.Visible = False
                                SaveCell.Visible = False
                                imgAttachments.Visible = False
                        End Select

                        If Me.mobjUser.EditionID = AccountEdition.SMALL_BUSINESS_EDITION Then
                            tdShare.Visible = True
                        Else
                            tdShare.Visible = False
                        End If

                        'Check e-mail/export permissions
                        Select Case DocumentEmailExportSecurityLevel(intDocumentId)
                            Case Web.Security.Functions.SecurityLevelEmailExport.EmailExportDisabled
                                'Hide e-mail/export button
                                imgMail.Attributes("style") = "DISPLAY: none"
                            Case Web.Security.Functions.SecurityLevelEmailExport.EmailExportEnabled
                                'show e-mail/export button
                                imgMail.Attributes("style") = "DISPLAY: block"
                            Case Else
                                'Hide e-mail/export button
                                imgMail.Attributes("style") = "DISPLAY: none"
                        End Select

                        'Check printing permissions
                        Select Case DocumentPrintSecurityLevel(intDocumentId)
                            Case Web.Security.Functions.SecurityLevelPrint.PrintDisabled
                                'Hide print button
                                imgPrint.Attributes("style") = "DISPLAY: none"
                            Case Web.Security.Functions.SecurityLevelPrint.PrintEnabled
                                'show print button
                                imgPrint.Attributes("style") = "DISPLAY: block"
                            Case Else
                                'Hide print button
                                imgPrint.Attributes("style") = "DISPLAY: none"
                        End Select

                        'Print permissions
                        Dim blnAnnotation As Boolean = False
                        Dim blnRedaction As Boolean = False
                        Functions.DocumentAnnotationSecurityLevel(Me.mobjUser.ImagesUserId, Me.mcookieSearch.VersionID, Me.mconSqlImage, blnAnnotation, blnRedaction)
                        Annotation.Value = CInt(blnAnnotation)
                        Redaction.Value = CInt(blnRedaction)

                    Catch ex As Exception
                        Throw ex
                    Finally
                        If Not Me.mconSqlImage.State = ConnectionState.Closed Then Me.mconSqlImage.Close()
                    End Try
                End If

                'Get variable information
                If Not Page.IsPostBack Then
                    If Me.mcookieSearch.VersionID > 0 Then
                        Me.mintVersionId = Me.mcookieSearch.VersionID
                        'Disable previous and next result if we are on the first result
                        DisableNextButton()
                        DisablePreviousButton()
                    ElseIf Not Me.mcookieImage.ServerFileName Is Nothing _
                            And Not Me.mcookieImage.LocalFileName Is Nothing Then
                        ddlVersions.Visible = False
                        imgMail.Visible = False
                        anchorPrint.Visible = False
                    End If
                    If CType(Session("FileType"), Functions.FileType) = Functions.FileType.SCAN Then InitializeViewer(Me.mintVersionId)

                    ddlVersions.Enabled = Me.mcookieSearch.HideVersionDropDownList

                End If

                Try
                    If Session("HideResultsButton") = True Then
                        BackCell.Visible = False
                        Session("HideResultsButton") = False
                    End If
                Catch ex As Exception

                End Try

                Try
                    'Next/Prev result buttons
                    Dim ResultCount As Integer = Session("intTotalDocuments")
                    Dim tmpDocs As DataTable = Session("dtVersionId")
                    If tmpDocs.Rows.Count > 0 Then
                        Dim dr() As DataRow = tmpDocs.Select("VersionId = '" & Me.mcookieSearch.VersionID & "'")
                        CurrentRow = dr(0)("Row")
                        Session("intCurrentRow") = CurrentRow
                        DisableNextButton()
                        DisablePreviousButton()
                    End If
                Catch ex As Exception

                End Try

            Catch ex As Exception

                If ex.Message = "Version has been deleted." Then
                    Server.Transfer("VersionDeleted.aspx")
                End If

            End Try

        End If

    End Sub

    Private Sub btnNext_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnNext.Click

        Try

            Session("intPrevNextVersionId") = Session("dtVersionId").Rows(Session("intCurrentRow"))("VersionId")
            Session("intCurrentRow") += 1

            Me.mcookieSearch = New cookieSearch(Me.Page)
            Me.mcookieImage = New cookieImage(Me.Page)

            Response.Redirect("Viewer.aspx?VID=" & CInt(Session("intPrevNextVersionId")))

        Catch ex As Exception

        End Try

    End Sub

    Private Sub btnPrevious_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrevious.Click

        Try

            Session("intPrevNextVersionId") = Session("dtVersionId").Rows(Session("intCurrentRow") - 2)("VersionId")
            Session("intCurrentRow") -= 1

            Me.mcookieSearch = New cookieSearch(Me.Page)
            Me.mcookieImage = New cookieImage(Me.Page)

            Response.Redirect("Viewer.aspx?VID=" & CInt(Session("intPrevNextVersionId")))

        Catch ex As Exception

        End Try

    End Sub

    Private Sub CheckVersionID(ByVal Rows As Integer)
        If Rows <= 0 Then
            'TODO: Need to add page that says image is not found... only should happen if versionid in query string is invalid
            Session(Me.mcookieSearch.VersionID & "VersionControl") = Nothing
            Me.mcookieSearch.VersionID = Nothing
            Response.Redirect("Default.aspx")
            Exit Sub
        End If
    End Sub

    Private Sub btnBack_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBack.Click

        If Request.QueryString("Ref") = "Recent" Then
            Response.Redirect("Search.aspx")
            Exit Sub
        End If

        If Request.QueryString("WFA") = "1" Then
            Response.Redirect("WorkflowDashboard.aspx")
        Else
            Session("Back") = True
            Response.Redirect("Search.aspx")
        End If

    End Sub

    Private Sub AttachmentDelete_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles AttachmentDelete.Click

        Try

            Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)

            Dim objAttachments As New FileAttachments(Functions.BuildConnection(Me.mobjUser))

            objAttachments.AttachmentDeleteByVersionId(Me.mcookieSearch.VersionID)

            'Update cached results
            Functions.UpdateCachedResultsByVersionId(Me.mcookieSearch.VersionID, Me.Parent.Page)

            'Return to results
            'btnBack_Click(Nothing, Nothing)
            System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.NewGuid.ToString, "parent.GB_hide();parent.closeHandler('remove', " & Me.mcookieSearch.VersionID & ");", True)

        Catch ex As Exception

            If ex.Message <> "Thread was being aborted." Then Functions.LogException(mobjUser.UserId.ToString, mobjUser.AccountId.ToString, "", "Delete attachment: " & ex.Message, ex.StackTrace, Server.MachineName, "", Request.UrlReferrer.ToString, Functions.BuildMasterConnection, True)

        Finally

            If Me.mconSqlImage.State <> ConnectionState.Closed Then
                Me.mconSqlImage.Close()
                Me.mconSqlImage.Dispose()
            End If

        End Try

    End Sub

    Private Sub lnkBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        If Request.QueryString("Ref") = "Recent" Then
            Response.Redirect("Search.aspx")
            Exit Sub
        End If

        If Request.QueryString("WFA") = "1" Then
            Response.Redirect("WorkflowDashboard.aspx")
        Else
            Session("Back") = True
            Response.Redirect("Search.aspx")
        End If


    End Sub
End Class
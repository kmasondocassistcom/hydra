<%@ Register TagPrefix="uc1" TagName="IndexNavigation" Src="IndexNavigation.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ImageThumbnails.aspx.vb" Inherits="docAssistWeb._ImageThumbnails"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<TITLE>docAssist : Thumbnails</TITLE>
		<META content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<META content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<META content="JavaScript" name="vs_defaultClientScript">
		<META content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<BODY>
		<DIV id="svcThumbs" style="BEHAVIOR: url(webservice.htc)" onresult="buildThumb()"></DIV>
		<FORM id="frmImageThumbnail" runat="server">
			<br>
			<table cellSpacing="0" cellPadding="0" width="100%">
				<tr>
					<td align="center"><asp:label id="lblThumbs" runat="server"></asp:label></td>
				</tr>
			</table>
			<script src="scripts/jquery.js"></script>
			<script src="scripts/imageThumbnails.js"></script>
		</FORM>
	</BODY>
</HTML>
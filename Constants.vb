Public Module Constants

    'Application Timeout
    Friend Const SESSION_TIMEOUT_MINUTES = 240
    Friend Const SESSION_KEEP_SIGNED_IN_DURATION_MINUTES = 240
    Friend Const SESSION_COOKIE = "Timer"
    Friend Const SESSION_COOKIE_KEY = "docAssistTimer"
    Friend Const SINGLE_SESSION_COOKIE = "docAssistSessionTimer"
    Friend Const SINGLE_SESSION_COOKIE_KEY = "Time"

    Friend Const DEFAULT_ATTRIBUTE_LENGTH = 75
    Friend Const DEFAULT_ATTRIBUTE_DATATYPE = "String"
    Friend Const DEFAULT_COMMAND_TIMEOUT = 120
    Friend Const DEFAULT_DATE_FORMAT = "yyyy/MM/dd"
    Friend Const DEFAULT_SESSION_TIMEOUT = 60

    'Web Service constants
    Friend Const DEFAULT_WEBSERVICE_NAMESPACE = "http://my.docassist.com/"

    Friend Const NEW_FOLDER = "New Folder"
    Friend Const RENAME_FOLDER = "Rename Folder"
    Friend Const REMOVE_FOLDER = "Remove Folder"
    Friend Const NEW_SHARE = "New Cabinet"
    Friend Const RENAME_SHARE = "Rename Cabinet"
    Friend Const REMOVE_SHARE = "Remove Cabinet"
    Friend Const SECURITY = "Security"
    Friend Const RENAME_FAVORITE = "Rename Favorite"
    Friend Const REMOVE_FAVORITE = "Remove Favorite"
    Friend Const ADD_TO_FAVORITES = "Add to Favorites"
    Friend Const ADD_FILES = "Add Files"
    Friend Const LOAD_SMARTFOLDER = "Edit SmartFolder"
    Friend Const REMOVE_SMARTFOLDER = "Remove SmartFolder"
    Friend Const FAVORITE_SPLIT = "-FAVSPLIT-"
    Friend Const ATTRIBUTE_ROWS = 3
    Friend Const ATTRIBUTE_COLUMN_WIDTH = 400

    'Tree Images
    Friend Const IMAGE_UNASSIGNED_NODE = "./images/folders/folder.gif"
    Friend Const IMAGE_CABINETS_FOLDERS = "./images/folders/cabinet_folder.gif"
    Friend Const IMAGE_CABINET_ADD = "./images/folders/cabinet_add.gif"
    Friend Const IMAGE_CABINET = "./images/folders/cabinet.gif"
    Friend Const IMAGE_DOCUMENT_TYPES = "./images/folders/doc_types.gif"
    Friend Const IMAGE_ADD_FILES = "./images/folders/add_doc.gif"
    Friend Const IMAGE_DOCUMENT = "./images/folders/doc.gif"
    Friend Const IMAGE_PUBLIC = "./images/folders/public.gif"
    Friend Const IMAGE_PRIVATE = "./images/folders/private.gif"
    Friend Const IMAGE_SMARTFOLDER_LOAD = "./images/folders/smartfolder_load.gif"
    Friend Const IMAGE_SMARTFOLDER = "./images/folders/smartfolder.gif"
    Friend Const IMAGE_CABINET_FOLDER = "./images/folders/cabinet_folder.gif"
    Friend Const IMAGE_FAVORITES_ADD = "./images/folders/favorites.add.gif"
    Friend Const IMAGE_FAVORITES = "./images/folders/favorites.gif"
    Friend Const IMAGE_FOLDER_ADD = "./images/folders/folder.add.gif"
    Friend Const IMAGE_FOLDER = "./images/folders/folder.gif"
    Friend Const IMAGE_FOLDER_OPEN = "./images/folders/folder.o.gif"
    Friend Const IMAGE_RENAME = "./images/folders/rename.gif"
    Friend Const IMAGE_REMOVE = "./images/folders/remove.gif"
    Friend Const IMAGE_SECURITY = "./images/folders/security.gif"
    Friend Const IMAGE_RECYCLE_BIN = "./images/folders/recycle_bin.png"
    Friend Const IMAGE_WORKSPACE = "./images/folders/workspace.png"
    Friend Const IMAGE_VIEW = "./images/folders/viewglass.gif"

    'Toolbar Images
    Friend Const TOOLBAR_IMAGE_REMOVE_OFF = "./images/folders/remove.btn.off.gif"
    Friend Const TOOLBAR_IMAGE_REMOVE_ON = "./images/folders/remove.btn.gif"
    Friend Const TOOLBAR_IMAGE_SECURITY_OFF = "./images/folders/properties.btn.off.gif"
    Friend Const TOOLBAR_IMAGE_SECURITY_ON = "./images/folders/properties.btn.gif"
    Friend Const TOOLBAR_IMAGE_RENAME_OFF = "./images/folders/rename.btn.off.gif"
    Friend Const TOOLBAR_IMAGE_RENAME_ON = "./images/folders/rename.btn.gif"
    Friend Const TOOLBAR_IMAGE_ADD_FAVORITES_OFF = "./images/folders/fav.add.btn.off.gif"
    Friend Const TOOLBAR_IMAGE_ADD_FAVORITES_ON = "./images/folders/fav.add.btn.gif"
    Friend Const TOOLBAR_IMAGE_CABINET_ADD_OFF = "./images/folders/cabinet.add.btn.off.gif"
    Friend Const TOOLBAR_IMAGE_CABINET_ADD_ON = "./images/folders/cabinet.add.btn.gif"
    Friend Const TOOLBAR_IMAGE_FOLDER_ADD_OFF = "./images/folders/folder.add.btn.off.gif"
    Friend Const TOOLBAR_IMAGE_FOLDER_ADD_ON = "./images/folders/folder.add.btn.gif"
    Friend Const TOOLBAR_IMAGE_ADD_FILES_ON = "./images/folders/doc.add.btn.gif"
    Friend Const TOOLBAR_IMAGE_ADD_FILES_OFF = "./images/folders/doc.add.btn.off.gif"

    'File size limits
    Friend Const MASS_DOWNLOAD_SIZE = 52428800 '50 Megs
    Friend Const MASS_EMAIL_SIZE = 9437184 '9 Megs
    Friend Const EMAIL_ATTACHMENT_SIZE = 9437184 '9 Megs 

    Friend Const WORKSPACE_FOLDERID = 2147483647

    Friend Enum TreeTables
        SHARES = 0
        FOLDERS = 1
        FAVORITES = 2
        CATEGORIES = 3
        DOCUMENTS = 4
        SMART_FOLDERS = 5
        UNASSIGNED = 6
    End Enum

    Friend Enum ImageTypes
        SCAN = 1
        DOCUMENT_ATTACHMENT = 2
        ATTACHMENT = 3
        FOLDER = 4
        EMAIL = 5
    End Enum

    Friend Enum UserType
        IMAGING = 0
        WORKFLOW_ONLY = 1
        STANDARD = 2
        READ_ONLY = 3
    End Enum

    Friend Enum AccountEdition
        SMALL_BUSINESS_EDITION = 5
    End Enum

    Public Enum AccountState
        DISABLED = -1
        TRIAL_EXPIRED = -2
        TRIAL_ACTIVE = -3
        PAID = -4
        INVALID_ACCOUNT = -5
    End Enum

    Public Enum DownloadQuality
        G4 = 1
        GREYSCALE = 50
        COLOR = 100
    End Enum

End Module

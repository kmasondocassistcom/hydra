Public Class ImageViewer
    Inherits System.Web.UI.Page

    '    Private mobjUser As Accucentric.docAssist.Web.Security.User
    '    Private mconSqlImage As SqlClient.SqlConnection
    '    Private mcookieSearch As cookieSearch

    '    Protected WithEvents litUnlockPrint As System.Web.UI.WebControls.Literal
    '    Protected WithEvents lblPageCount As System.Web.UI.HtmlControls.HtmlInputHidden
    '    Protected WithEvents litProgress As System.Web.UI.WebControls.Literal
    '    Protected WithEvents AutoDisplay As System.Web.UI.HtmlControls.HtmlInputText
    '    Protected WithEvents litLpkLicense As System.Web.UI.WebControls.Literal
    '    Protected WithEvents hostname As System.Web.UI.HtmlControls.HtmlInputHidden
    '    Protected WithEvents UserView As System.Web.UI.HtmlControls.HtmlInputText
    '    Protected WithEvents lblErrorMessage As System.Web.UI.WebControls.Label
    '    Protected WithEvents frameLoading As System.Web.UI.HtmlControls.HtmlTable
    '    Protected WithEvents iFrameLoading As System.Web.UI.HtmlControls.HtmlGenericControl
    '    Protected WithEvents AccountId As System.Web.UI.HtmlControls.HtmlInputHidden
    '    Protected WithEvents frameConfirm As System.Web.UI.HtmlControls.HtmlTable
    '    Protected WithEvents iframeConfirm As System.Web.UI.HtmlControls.HtmlGenericControl
    '    Protected WithEvents Annotation As System.Web.UI.HtmlControls.HtmlInputHidden
    '    Protected WithEvents Redaction As System.Web.UI.HtmlControls.HtmlInputHidden
    '    Protected WithEvents DownloadQuality As System.Web.UI.HtmlControls.HtmlInputHidden
    '    Protected WithEvents MaxQuality As System.Web.UI.HtmlControls.HtmlInputHidden

    '    Private mcookieImage As cookieImage

    '#Region " Web Form Designer Generated Code "

    '    'This call is required by the Web Form Designer.
    '    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    '    End Sub
    '    Protected WithEvents lnkLoadFile As System.Web.UI.WebControls.LinkButton
    '    Protected WithEvents lblFileNameClient As System.Web.UI.WebControls.Label
    '    Protected WithEvents txtFileNameServer As System.Web.UI.WebControls.TextBox
    '    Protected WithEvents litUnlockImageCtrl As System.Web.UI.WebControls.Literal
    '    Protected WithEvents litScripts As System.Web.UI.WebControls.Literal
    '    Protected WithEvents lblVersionId As System.Web.UI.HtmlControls.HtmlInputHidden
    '    Protected WithEvents lblPageNo As System.Web.UI.HtmlControls.HtmlInputHidden
    '    Protected WithEvents lblFileNameResult As System.Web.UI.HtmlControls.HtmlInputHidden

    '    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    '    'Do not delete or move it.
    '    Private designerPlaceholderDeclaration As System.Object

    '    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
    '        'CODEGEN: This method call is required by the Web Form Designer
    '        'Do not modify it using the code editor.
    '        InitializeComponent()
    '    End Sub

    '#End Region

    '    Private Sub InitializeViewer()

    '        'Dim sb As New System.Text.StringBuilder
    '        'sb.Append("<OBJECT classid=""clsid:5220cb21-c88d-11cf-b347-00aa00a28331"" VIEWASTEXT>")
    '        'sb.Append("<param name=""LPKPath"" value=""ix7.lpk"">")
    '        'sb.Append("</OBJECT>" & vbCrLf)
    '        'litLpkLicense.Text = sb.ToString

    '        'Dim sbUnlockCtrl As New System.Text.StringBuilder
    '        'Dim sbUnlockPrint As New System.Text.StringBuilder

    '        'sbUnlockCtrl.Append("<OBJECT classid=clsid:5220cb21-c88d-11cf-b347-00aa00a28331 VIEWASTEXT>")
    '        'sbUnlockCtrl.Append("<param name=LPKPath value=ix7.lpk>")
    '        'sbUnlockCtrl.Append("</OBJECT>")
    '        'sbUnlockCtrl.Append("<OBJECT id=imageCtrl style=""DISPLAY: none; Z-INDEX: 1; LEFT: 0px; WIDTH: 100%; POSITION: absolute; TOP: 0px"" onfocus=""ActivateEvents();"" height=100% width=100% border=0 classid=clsid:6D3CF4F3-C2F3-46E7-A126-3E53102A6B91 VIEWASTEXT>")
    '        'sbUnlockCtrl.Append("</OBJECT>")

    '        'sbUnlockCtrl.Append(vbCrLf & "<script language=""javascript"">" & vbCrLf)
    '        'sbUnlockCtrl.Append("   Form1.imageCtrl.IntegratorWeb(""")
    '        'sbUnlockCtrl.Append(Functions.UnlockCode(Request))
    '        'sbUnlockCtrl.Append(""");")
    '        'sbUnlockPrint.Append("<script language=""javascript"">" & vbCrLf)
    '        'sbUnlockPrint.Append("   document.all.objPrint.IntegratorWeb(""")
    '        'sbUnlockPrint.Append(Functions.UnlockCode(Request))
    '        'sbUnlockPrint.Append(""");")

    '        AccountId.Value = Me.mobjUser.AccountId.ToString

    '        '
    '        ' Load image client side
    '        '
    '        If lblFileNameClient.Text.Length > 0 Then
    '            Dim strFileName As String
    '            If Not lblFileNameClient.Text.IndexOf("\") > 0 Then
    '                strFileName = "\\" & lblFileNameClient.Text
    '            Else
    '                strFileName = lblFileNameClient.Text.Replace("\", "\\")
    '            End If

    '            'sbUnlockCtrl.Append(vbCrLf & "   Form1.imageCtrl.ProgressEnabled = true;" & vbCrLf)
    '            'sbUnlockCtrl.Append("   Form1.imageCtrl.ProgressPct = 100;" & vbCrLf)
    '            'sbUnlockCtrl.Append("   Form1.imageCtrl.style.display=""block"";" & vbCrLf)
    '            'sbUnlockCtrl.Append("   Form1.imageCtrl.Async=0;" & vbCrLf)
    '            'sbUnlockCtrl.Append("   Form1.imageCtrl.PageNbr=Form1.lblPageNo.value ;" & vbCrLf)

    '            'sbUnlockCtrl.Append("   Form1.imageCtrl.BackColor=""&HE0E2E2"";" & vbCrLf)
    '            'sbUnlockCtrl.Append("   Form1.imageCtrl.BorderType=3;" & vbCrLf)

    '            'sbUnlockCtrl.Append("   Form1.imageCtrl.FileName=""")
    '            'sbUnlockCtrl.Append(strFileName)
    '            'sbUnlockCtrl.Append(""";" & vbCrLf)
    '            'sbUnlockCtrl.Append("   Form1.imageCtrl.AutoSize=4;" & vbCrLf)
    '            'sbUnlockCtrl.Append("   Form1.imageCtrl.ScrollBars=3;" & vbCrLf)
    '            WriteScripts(Server.MapPath("Script2.js"))
    '            '
    '            ' Get the page count
    '            Dim ix As New PegasusImaging.WinForms.ImagXpress7.PICImagXpress
    '            lblPageCount.Value = ix.NumPages(Server.MapPath(txtFileNameServer.Text))
    '        ElseIf CInt(lblVersionId.Value) > 0 Then

    '            ' Get Page No
    '            Dim dsVersion As Accucentric.docAssist.Data.dsVersionControl = Session(Me.mcookieSearch.VersionID & "VersionControl")
    '            lblPageCount.Value = CType(dsVersion.ImageVersion.Rows(0), Accucentric.docAssist.Data.dsVersionControl.ImageVersionRow).SeqTotal
    '        End If

    '        'sbUnlockCtrl.Append("</script>" & vbCrLf)
    '        'sbUnlockPrint.Append("</script>" & vbCrLf)
    '        ''litUnlockImageCtrl.Text = sbUnlockCtrl.ToString
    '        'litUnlockPrint.Text = sbUnlockPrint.ToString

    '    End Sub

    '    Private Sub WriteScripts(ByVal strScriptFileName As String)

    '        Dim sbScripts As New System.Text.StringBuilder
    '        sbScripts.Append("<SCRIPT language=""javascript"">" & vbCrLf)
    '        sbScripts.Append("             var hostname=""")
    '        sbScripts.Append(Request.Url.AbsoluteUri.Substring(0, Request.Url.AbsoluteUri.Length - Request.Url.AbsolutePath.Length))
    '#If DEBUG Then
    '        sbScripts.Append("/docAssistWeb1")
    '#End If
    '        sbScripts.Append(""";" & vbCrLf)
    '        Dim srServiceScript As New System.IO.StreamReader(strScriptFileName, False)
    '        sbScripts.Append(srServiceScript.ReadToEnd)
    '        srServiceScript.Close()

    '        sbScripts.Append(vbCrLf & "</SCRIPT>")
    '        Dim strScript As String = sbScripts.ToString

    '        litScripts.Text = strScript

    '    End Sub

    '    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    '        Response.Cookies("docAssistBack")("Count") = True

    '        '
    '        ' Build the user object
    '        '
    '        Try
    '            Me.mobjUser = BuildUserObject(Request.Cookies("docAssist"))
    '            Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)

    '            'Response.CacheControl = "no-cache"

    '            'Load default user document view
    '            Select Case mobjUser.DefaultDocView
    '                Case 0 'Best Fit
    '                    UserView.Value = "4"
    '                Case 1 ' Fit width
    '                    UserView.Value = "6"
    '                Case 2 'Fit Height
    '                    UserView.Value = "7"
    '                Case Else
    '                    UserView.Value = "4"
    '            End Select

    '            'Get default viewing quality
    '            DownloadQuality.Value = mobjUser.DefaultQuality

    '        Catch ex As Exception
    '            If ex.Message = "User is not logged on" Then
    '                Response.Redirect("Default.aspx")
    '            Else
    '                lblErrorMessage.Text = ex.Message & "<BR>"
    '                lblErrorMessage.Visible = True
    '            End If
    '        End Try
    '        '
    '        ' Initialize the page
    '        '
    '        lblErrorMessage.Text = ""
    '        lblErrorMessage.Visible = False
    '        Me.mcookieSearch = New cookieSearch(Me)
    '        Me.mcookieImage = New cookieImage(Me)

    '        If Not Page.IsPostBack Then

    '            Dim strHostname As String = Request.Url.AbsoluteUri.Substring(0, Request.Url.AbsoluteUri.Length - Request.Url.AbsolutePath.Length)
    '#If DEBUG Then
    '            strHostname += "/docAssistWeb1"
    '#End If
    '            hostname.Value = strHostname

    '            Try
    '                Dim intPageNo As Integer = 1

    '                If Me.mcookieSearch.VersionID > 0 Then
    '                    lblVersionId.Value = Me.mcookieSearch.VersionID.ToString
    '                    Session("intVersionIdTemp") = Me.mcookieSearch.VersionID.ToString
    '                    intPageNo = Me.mcookieImage.PageNo
    '                ElseIf Not Me.mcookieImage.ServerFileName Is Nothing _
    '                        And Not Me.mcookieImage.LocalFileName Is Nothing Then
    '                    txtFileNameServer.Text = Me.mcookieImage.ServerFileName
    '                    lblFileNameClient.Text = Me.mcookieImage.LocalFileName
    '                Else
    '                    Throw New Exception("Error receiving image information.")
    '                End If
    '                lblPageNo.Value = intPageNo
    '                InitializeViewer()
    '                '
    '                ' Check to see if the confirmdelete page was called
    '                Dim pgDelete As _ConfirmDelete
    '                Try
    '                    pgDelete = CType(context.Handler, _ConfirmDelete)
    '                Catch ex As Exception
    '                End Try
    '                If Not pgDelete Is Nothing Then
    '                    Dim ctrls As IEnumerator = pgDelete.PageControls.GetEnumerator
    '                    Dim ctrl As Control
    '                    Do While (ctrls.MoveNext)
    '                        ctrl = CType(ctrls.Current, Control)
    '                        If ctrl.ID = "txtPageNo" Then
    '                            lblPageNo.Value = CType(ctrl, TextBox).Text
    '                        End If
    '                    Loop
    '                End If

    '            Catch ex As Exception
    '                lblErrorMessage.Text = ex.Message & "<BR>"
    '                lblErrorMessage.Visible = True
    '            End Try

    '        End If
    '    End Sub

    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub
End Class

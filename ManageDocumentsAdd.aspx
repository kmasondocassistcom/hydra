<%@ Register TagPrefix="mbclb" Namespace="MetaBuilders.WebControls" Assembly="MetaBuilders.WebControls.CheckedListBox" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ManageDocumentsAdd.aspx.vb" Inherits="docAssistWeb.ManageDocumentsAdd"%>
<%@ Register TagPrefix="uc1" TagName="AdminNav" Src="Controls/AdminNav.ascx" %>
<%@ Register TagPrefix="uc1" TagName="docHeader" Src="Controls/docHeader.ascx" %>
<%@ Register TagPrefix="uc1" TagName="TopRight" Src="Controls/TopRight.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>docAssist - Add Document</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0" MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<TABLE height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TBODY>
					<TR>
						<TD width="100%" colSpan="5"><uc1:docheader id="docHeader" runat="server"></uc1:docheader></TD>
					</TR>
					<TR vAlign="middle" height="100%">
						<TD class="Navigation1" style="BORDER-RIGHT: #cccccc thin solid" vAlign="top" height="100%"><uc1:adminnav id="AdminNav2" runat="server"></uc1:adminnav><IMG height="1" src="dummy.gif" width="120"></TD>
						<TD vAlign="top" align="center" height="100%"><BR>
							<table cellSpacing="0" cellPadding="0" width="100%" border="0">
								<tr>
									<td><IMG height="1" src="dummy.gif" width="35"><IMG src="Images/manage_documents.gif"></td>
								</tr>
							</table>
							<table height="100%" cellSpacing="0" cellPadding="0" width="100%">
					</TR>
					<tr height="50%">
					</tr>
					<tr height="15" width="100%">
						<td></td>
						<td style="WIDTH: 61px"></td>
						<td style="WIDTH: 330px"></td>
						<td></td>
					</tr>
					<tr height="15" width="100%">
						<td></td>
						<td style="WIDTH: 61px" align="left"><asp:label id="Label4" runat="server" Font-Size="8pt" Font-Names="Verdana" Height="5px" Width="104px">Document Name:</asp:label></td>
						<TD style="WIDTH: 330px"><asp:textbox id="txtDocumentName" runat="server" Font-Size="8pt" Font-Names="Verdana" Width="270px"></asp:textbox></TD>
						<TD width="50%"></TD>
					<TR height="15" width="100%" style="VISIBILITY: hidden">
						<td style="HEIGHT: 42px" width="50%"></td>
						<td style="WIDTH: 61px; HEIGHT: 42px" align="left" vAlign="top" noWrap><BR>
							<asp:label id="Label1" runat="server" Font-Size="8pt" Font-Names="Verdana" Height="16px" Width="95px">Version Control:</asp:label>
						</td>
						<td style="WIDTH: 330px; HEIGHT: 42px" vAlign="top"><BR>
							<asp:dropdownlist id="ddlVersion" runat="server" Width="240px" AutoPostBack="True">
								<asp:ListItem Value="1">Enabled</asp:ListItem>
								<asp:ListItem Value="0">Disabled</asp:ListItem>
							</asp:dropdownlist>
						</td>
						<td style="HEIGHT: 42px" width="50%"></td>
					</TR>
					<tr height="15" width="100%">
						<td style="WIDTH: 393px; HEIGHT: 29px" width="393"></td>
						<td style="WIDTH: 61px; HEIGHT: 29px">
							<P align="left">&nbsp;&nbsp;
							</P>
						</td>
						<td style="WIDTH: 330px; HEIGHT: 29px"></td>
						<td style="HEIGHT: 29px" width="50%"></td>
					</tr>
					<tr height="15" width="100%">
						<td style="WIDTH: 393px; HEIGHT: 15px" width="393"></td>
						<td style="WIDTH: 61px; HEIGHT: 15px" vAlign="middle">
							<P align="left">&nbsp;&nbsp;</P>
						</td>
						<td style="WIDTH: 330px; HEIGHT: 15px" align="right"></td>
						<td style="HEIGHT: 15px" width="50%"></td>
					</tr>
					<tr height="15" width="100%">
						<td style="WIDTH: 393px; HEIGHT: 29px" width="395"></td>
						<td style="WIDTH: 61px; HEIGHT: 29px"><BR>
							<P>&nbsp;</P>
						</td>
						<td style="WIDTH: 330px; HEIGHT: 29px">
							<P><asp:label id="lblError" runat="server" Font-Size="8pt" Font-Names="Verdana" Height="2px" Width="100%"
									ForeColor="Red"></asp:label><BR>
								<asp:requiredfieldvalidator id="FolderValidator" runat="server" Font-Size="8pt" Font-Names="Verdana" Height="6px"
									Width="100%" ErrorMessage="Document Name required." ControlToValidate="txtDocumentName"></asp:requiredfieldvalidator></P>
						</td>
						<TD style="HEIGHT: 29px" width="50%"></TD>
					</tr>
					<TR height="15" width="100%">
						<td style="WIDTH: 395px" width="395"></td>
						<td style="WIDTH: 61px"><BR>
							<P></P>
						</td>
						<td style="WIDTH: 330px" align="right"><asp:imagebutton id="imgCancel" runat="server" ImageUrl="Images/btn_canceladmin.gif"></asp:imagebutton>&nbsp;
							<asp:imagebutton id="btnNext" runat="server" ImageUrl="Images/btn_nextadmin.gif"></asp:imagebutton></td>
						<td width="50"></td>
					</TR>
					<tr height="100%">
						<td></td>
					</tr>
				</tr></TABLE>
			<P><BR>
				<BR>
			</P>
			</TD>
			<TD class="RightContent" height="100%">&nbsp;</TD>
			</TR>
			<TR>
				<TD class="PageFooter" vAlign="bottom" bgColor="#cccccc" colSpan="3"><uc1:topright id="TopRight1" runat="server"></uc1:topright></TD>
			</TR>
			<TR height="1">
				<TD style="BACKGROUND-COLOR: #959394" width="20"><IMG height="1" src="images/spacer.gif" width="20"></TD>
				<TD style="BACKGROUND-COLOR: #959394" width="100%"><IMG height="1" src="images/spacer.gif" width="100%"></TD>
				<TD style="BACKGROUND-COLOR: #959394" width="20"><IMG height="1" src="images/spacer.gif" width="20"></TD>
			</TR>
			</TBODY></TABLE>
			<script language="javascript">
		function nb_numericOnly(evt)
			{
				evt = (evt) ? evt : event;
				var charCode = (evt.charCode) ? evt.charCode : ((evt.keyCode) ? evt.keyCode
				: ((evt.which) ? evt.which : 0));
				if (charCode > 31 && (charCode < 48 || charCode >57)) {return false;}
				return true;
			}
			
			function catchSubmit()
			{
				if (event.keyCode == 13)
				{
					event.cancelBubble = true;
					event.returnValue = false;
					//document.all.lnkSearch.click();
					return false;
				}
			}	
			</script>
		</form>
	</body>
</HTML>

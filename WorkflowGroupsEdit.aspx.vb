Partial Class WorkflowGroupsEdit
    Inherits System.Web.UI.Page


    Private intGroupId As Integer
    Private intMode As Integer


    Private pageTitle As New System.Web.UI.HtmlControls.HtmlGenericControl

    Private mobjUser As Accucentric.docAssist.Web.Security.User

    Private dtAssisgnedUsers As New DataTable
    Private dtAvailableUsers As New DataTable

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try
            txtGroupName.Attributes("onkeypress") = "submitForm();"

            intMode = CInt(Request.QueryString("Mode"))
            intGroupId = CInt(Request.QueryString("ID"))

        Catch ex As Exception

        End Try


        'Security
        Try
            Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
            Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))
            Me.mobjUser.AccountId = guidAccountId
        Catch ex As Exception
            Response.Redirect("Default.aspx")
        End Try


        If Page.IsPostBack = False Then


            'Actions 
            Try
                Select Case intMode
                    Case 0
                        'lblMessage.Text = "Enter a name for the new category." 
                        chkEnabled.Checked = True
                        LoadGroup()
                    Case 1
                        'Load category to change.
                        lblMessage.Text = "Make any changes and click save to commit."
                        LoadExistingGroup()

                    Case 2

                End Select

            Catch ex As Exception

            End Try
        End If

        'Mode
        'TODO: Page title

    End Sub


    Private Function LoadExistingGroup()

        Try
            'Load header info
            Dim wfGroups As New Accucentric.docAssist.Data.Images.Workflow(Functions.BuildConnection(Me.mobjUser))
            Dim dtGroupHeader As New DataTable
            dtGroupHeader = wfGroups.WFGroupsGetID(intGroupId)
            If dtGroupHeader.Rows.Count > 0 Then
                txtGroupName.Text = dtGroupHeader.Rows(0)("GroupName")
                chkEnabled.Checked = dtGroupHeader.Rows(0)("GroupEnabled")
            End If

            'Load assigned users
            wfGroups = New Accucentric.docAssist.Data.Images.Workflow(Functions.BuildConnection(Me.mobjUser))
            Dim dtAssisgnedUsers As New DataTable
            dtAssisgnedUsers = wfGroups.WFGroupMembersGetAssignedID(Me.mobjUser.AccountId.ToString, intGroupId)
            lstAssisgned.DataTextField = "UserName"
            lstAssisgned.DataValueField = "UserId"
            lstAssisgned.DataSource = dtAssisgnedUsers
            lstAssisgned.DataBind()

            'Load available users
            wfGroups = New Accucentric.docAssist.Data.Images.Workflow(Functions.BuildConnection(Me.mobjUser))
            Dim dtAvailableUsers As DataTable
            dtAvailableUsers = wfGroups.WFGroupMembersGetAvailableID(Me.mobjUser.AccountId.ToString, intGroupId)
            lstAvailable.DataTextField = "UserName"
            lstAvailable.DataValueField = "UserId"
            lstAvailable.DataSource = dtAvailableUsers
            lstAvailable.DataBind()
        Catch ex As Exception

        End Try

    End Function


    Private Function LoadGroup()

        Try
            Dim wfGroups As New Accucentric.docAssist.Data.Images.Workflow(Functions.BuildConnection(Me.mobjUser))

            Dim dtAssisgnedUsers As New DataTable
            dtAssisgnedUsers = wfGroups.WFGroupMembersGetAssignedID(Me.mobjUser.AccountId.ToString, intGroupId)
            lstAssisgned.DataSource = dtAssisgnedUsers
            lstAssisgned.DataBind()

            wfGroups = New Accucentric.docAssist.Data.Images.Workflow(Functions.BuildConnection(Me.mobjUser))
            Dim dtAvailableUsers As DataTable
            dtAvailableUsers = wfGroups.WFGroupMembersGetAvailableID(Me.mobjUser.AccountId.ToString, intGroupId)

            lstAvailable.DataTextField = "UserName"
            lstAvailable.DataValueField = "UserId"
            lstAvailable.DataSource = dtAvailableUsers
            lstAvailable.DataBind()
        Catch ex As Exception

        End Try

    End Function

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click

        Try
            If txtGroupName.Text.Length = 0 Then
                lblError.Text = "Group Name is required."
                lblError.Visible = True
                'ReturnScripts.Add(lblError)
                Exit Sub
            End If
        Catch ex As Exception

        End Try
        Dim sqlTran As SqlClient.SqlTransaction

        Try

            Dim sqlConImages As New SqlClient.SqlConnection
            sqlConImages = Functions.BuildConnection(Me.mobjUser)

            If sqlConImages.State <> ConnectionState.Open Then sqlConImages.Open()

            sqlTran = sqlConImages.BeginTransaction

            Dim wfGroup As New Accucentric.docAssist.Data.Images.Workflow(sqlConImages, False, sqlTran)

            Select Case intMode
                Case 0 'Add

                    'chkEnabled.Checked = True
                    Dim intGrpId As Integer = wfGroup.WFGroupsInsert(txtGroupName.Text, chkEnabled.Checked)

                    If intGrpId = 0 Then
                        lblError.Text = "Group already exists."
                        lblError.Visible = True
                        'ReturnScripts.Add(lblError)
                    Else
                        For X As Integer = 0 To lstAssisgned.Items.Count - 1
                            wfGroup.WFGroupMembersAssignedInsert(intGrpId, lstAssisgned.Items(X).Value)
                        Next

                        'System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.NewGuid.ToString, "reloadData();", True)
                    End If

                Case 1 'Edit

                    Dim intGrpId As Integer = wfGroup.WFGroupsUpdate(intGroupId, txtGroupName.Text, chkEnabled.Checked)
                    If intGrpId = 0 Then
                        lblError.Text = "Group already exists."
                        lblError.Visible = True
                        'ReturnScripts.Add(lblError)
                    Else

                        'Delete all the users
                        wfGroup.WFGroupMembersAssignedInsert(intGroupId, 0)

                        For X As Integer = 0 To lstAssisgned.Items.Count - 1
                            wfGroup.WFGroupMembersAssignedInsert(intGroupId, lstAssisgned.Items(X).Value)
                        Next
                        'System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.NewGuid.ToString, "reloadData();", True)

                    End If
                Case 2

            End Select

            System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, "Close", "opener.location.reload();window.close();", True)

        Catch ex As Exception
            sqlTran.Rollback()
        Finally
            sqlTran.Commit()
        End Try

    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAdd.Click

        Try
            If lstAvailable.SelectedIndex = -1 Then Exit Sub

            Dim mdcUserId As New DataColumn
            Dim mdcUserName As New DataColumn
            mdcUserId = New DataColumn("UserId", GetType(System.Int32), Nothing, MappingType.Element)
            mdcUserName = New DataColumn("UserName", GetType(System.String), Nothing, MappingType.Element)
            dtAssisgnedUsers.Columns.AddRange(New DataColumn() {mdcUserId, mdcUserName})

            For x As Integer = 0 To lstAssisgned.Items.Count - 1
                Dim dr2 As DataRow
                dr2 = dtAssisgnedUsers.NewRow
                dr2("UserId") = lstAssisgned.Items(x).Value
                dr2("UserName") = lstAssisgned.Items(x).Text
                dtAssisgnedUsers.Rows.Add(dr2)
            Next

            Dim lstItem As New ListItem
            lstItem = lstAvailable.SelectedItem

            Dim intSelectedRow As Integer = lstAvailable.SelectedIndex

            'lstAssisgned.Items.Add(lstItem)
            lstAvailable.Items.Remove(lstItem)

            Dim dr As DataRow
            dr = dtAssisgnedUsers.NewRow

            dr("UserId") = lstItem.Value
            dr("UserName") = lstItem.Text

            dtAssisgnedUsers.Rows.Add(dr)

            lstAssisgned.DataSource = dtAssisgnedUsers
            lstAssisgned.DataTextField = "UserName"
            lstAssisgned.DataValueField = "UserId"
            lstAssisgned.DataBind()

            If lstAvailable.Items.Count > 0 Then
                If intSelectedRow = 0 Then
                    lstAvailable.Items(0).Selected = True
                Else
                    lstAvailable.Items(intSelectedRow - 1).Selected = True
                End If
            End If

        Catch ex As Exception

        Finally
            'ReturnScripts.Add(lstAssisgned)
            'ReturnScripts.Add(lstAvailable)
        End Try

    End Sub

    Private Sub btnRemove_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnRemove.Click

        Try
            If lstAssisgned.SelectedIndex = -1 Then Exit Sub

            'Create table that contains available users
            Dim mdcUserId As New DataColumn
            Dim mdcUserName As New DataColumn
            mdcUserId = New DataColumn("UserId", GetType(System.Int32), Nothing, MappingType.Element)
            mdcUserName = New DataColumn("UserName", GetType(System.String), Nothing, MappingType.Element)
            dtAvailableUsers.Columns.AddRange(New DataColumn() {mdcUserId, mdcUserName})

            'Get current 
            For x As Integer = 0 To lstAvailable.Items.Count - 1
                Dim dr2 As DataRow
                dr2 = dtAvailableUsers.NewRow
                dr2("UserId") = lstAvailable.Items(x).Value
                dr2("UserName") = lstAvailable.Items(x).Text
                dtAvailableUsers.Rows.Add(dr2)
            Next

            Dim lstItem As New ListItem
            lstItem = lstAssisgned.SelectedItem

            Dim intSelectedRow As Integer = lstAssisgned.SelectedIndex

            lstAssisgned.Items.Remove(lstItem)

            Dim dr As DataRow
            dr = dtAvailableUsers.NewRow

            dr("UserId") = lstItem.Value
            dr("UserName") = lstItem.Text

            dtAvailableUsers.Rows.Add(dr)

            lstAvailable.DataSource = dtAvailableUsers
            lstAvailable.DataTextField = "UserName"
            lstAvailable.DataValueField = "UserId"
            lstAvailable.DataBind()

            If lstAssisgned.Items.Count > 0 Then
                If intSelectedRow = 0 Then
                    lstAssisgned.Items(0).Selected = True
                Else
                    lstAssisgned.Items(intSelectedRow - 1).Selected = True
                End If
            End If

        Catch ex As Exception

        Finally
            'ReturnScripts.Add(lstAssisgned)
            'ReturnScripts.Add(lstAvailable)
        End Try

    End Sub
End Class

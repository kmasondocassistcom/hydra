<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Search.aspx.vb" Inherits="docAssistWeb.Search"
    Buffer="True" ValidateRequest="false" %>

<%@ Register TagPrefix="componentart" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<%@ Register TagPrefix="uc1" TagName="docHeader" Src="Controls/docHeader.ascx" %>
<%@ Register TagPrefix="uc1" TagName="TopRight" Src="Controls/TopRight.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>docAssist - Explorer</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <link href="Styles.css" type="text/css" rel="stylesheet">
    <link href="css/newStyle.css" type="text/css" rel="stylesheet">
    <link href="css/jquery.autocomplete.css" type="text/css" rel="stylesheet">
    <meta http-equiv="X-UA-Compatible" content="IE=7">
</head>
<body onkeydown="catchEsc();" onresize="resizeElements();return false;" style="background-position-y: bottom;
    background-image: url(Images/Search/backgroundGrad.gif); background-repeat: repeat-x"
    bottommargin="0" onbeforeunload="checkDirty();" leftmargin="0" topmargin="0"
    onload="resizeElements();return false" rightmargin="0">
    <div id='GB_overlay' style="display: none">
    </div>
    <div id='GB_window' style="display: none; overflow: hidden">
        <div id='GB_caption' style="display: none">
        </div>
        <img id='GB_img' style="display: none" src='./images/close.gif' alt='Close Viewer'></div>
    <form id="frmSearch" onkeydown="catchEsc();" method="post" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="black_overlay" id="fade">
    </div>
    <div id="svcThumbs" style="behavior: url(webservice.htc)">
    </div>
    <div id="svcImages" style="behavior: url(webservice.htc)">
    </div>
    <table height="100%" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td valign="top" nowrap colspan="2">
                <uc1:docHeader ID="docHeader" runat="server"></uc1:docHeader>
            </td>
        </tr>
        <tr align="left">
            <td valign="top" nowrap align="center" width="250" height="100%">
                <table class="PageContent" id="tblTree" cellspacing="0" cellpadding="0" width="275"
                    border="0">
                    <tbody>
                        <tr valign="top" align="left">
                            <td nowrap align="center" width="250">
                                <table id="tblQuick" cellspacing="0" cellpadding="0" width="275" border="0">
                                    <tr>
                                        <td nowrap align="center">
                                            <table class="PageContent" style="border-right: #cccccc 2px solid; border-top: #cccccc 2px solid;
                                                border-left: #cccccc 2px solid; border-bottom: #cccccc 2px solid" cellspacing="2"
                                                cellpadding="2" width="100%" border="0">
                                                <tr>
                                                    <td>
                                                        <table class="PageContent" style="border-right: #eaf0f7 1px solid; border-top: #eaf0f7 1px solid;
                                                            border-left: #eaf0f7 1px solid; border-bottom: #eaf0f7 1px solid" cellspacing="0"
                                                            cellpadding="0" width="100%" border="0">
                                                            <tr bgcolor="#f7f7f7">
                                                                <td>
                                                                    <img height="3" src="spacer.gif" width="1">
                                                                </td>
                                                            </tr>
                                                            <tr bgcolor="#f7f7f7">
                                                                <td nowrap>
                                                                    <table cellspacing="1" cellpadding="1" width="100%" border="0">
                                                                    </table>
                                                                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                                                        <tr>
                                                                            <td nowrap colspan="2">
                                                                                <span class="Heading"><strong><font size="+0">Quick Search</font></strong></span>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" width="100%" colspan="2">
                                                                                <asp:TextBox ID="txtQuickSearch" runat="server" Font-Names="Trebuchet MS" Font-Size="8pt"
                                                                                    Width="130px"></asp:TextBox>&nbsp;<asp:DropDownList ID="ddlQuickSearchType" runat="server"
                                                                                        Font-Names="Trebuchet MS" Font-Size="8pt" Width="113px">
                                                                                        <asp:ListItem Value="0">Documents</asp:ListItem>
                                                                                        <asp:ListItem Value="5">Document Text</asp:ListItem>
                                                                                        <asp:ListItem Value="1">Document No.</asp:ListItem>
                                                                                        <asp:ListItem Value="4">Folder Name</asp:ListItem>
                                                                                        <asp:ListItem Value="2">Filename</asp:ListItem>
                                                                                        <asp:ListItem Value="3">Document Title</asp:ListItem>
                                                                                    </asp:DropDownList>
                                                                            </td>
                                                                            <td width="100%">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td height="20">
                                                                            </td>
                                                                            <td valign="bottom" align="right">
                                                                                <asp:ImageButton ID="btnQuickSearch" runat="server" ImageUrl="Images/btn_search.gif">
                                                                                </asp:ImageButton><img height="1" src="spacer.gif" width="5">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2">
                                                                                <img height="3" src="spacer.gif" width="1">
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                                <table height="5">
                                    <tr>
                                        <td>
                                        </td>
                                    </tr>
                                </table>
                                <table style="border-right: #cccccc 2px solid; border-top: #cccccc 2px solid; border-left: #cccccc 2px solid;
                                    border-bottom: #cccccc 2px solid" cellspacing="2" cellpadding="2" width="275"
                                    border="0">
                                    <tr>
                                        <td bgcolor="#ffffff">
                                            <table width="100%" class="PageContent">
                                                <tr>
                                                    <td>
                                                        <div>
                                                            <img id="newCabinet" alt="New Cabinet" src="./images/folders/cabinet.add.btn.off.gif">
                                                            <img id="newFolder" alt="New Folder" src="./images/folders/folder.add.btn.off.gif">
                                                            <img id="moveFolder" alt="Move Folder" src="./images/folders/move.btn.off.gif">
                                                            <img id="renameItem" alt="Rename" src="./images/folders/rename.btn.off.gif">
                                                            <img id="removeItem" alt="Remove Item" src="./images/folders/remove.btn.off.gif">
                                                            <img id="addFavorite" alt="Add to Favorites" src="./images/folders/fav.add.btn.off.gif">
                                                            <img id="uploadFiles" alt="Upload Files" src="./images/folders/doc.add.btn.off.gif">
                                                            <img id="manageItem" alt="Manage Item" src="./images/folders/properties.btn.off.gif">
                                                            <img id="shareFolder" alt="Share Folder" src="./images/folders/share.btn.off.gif"
                                                                runat="server">
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table class="PageContent" style="border-right: #eaf0f7 1px solid; border-top: #eaf0f7 1px solid;
                                                border-left: #eaf0f7 1px solid; border-bottom: #eaf0f7 1px solid" width="100%">
                                                <tr>
                                                    <td>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="PageContent" id="divTree" style="overflow: auto; width: 255px; height: 300px;
                                                            background-color: #ffffff" runat="server">
                                                            <componentart:TreeView ID="treeSearch" runat="server" Width="100%" EnableViewState="False"
                                                                ExpandSelectedPath="False" ExpandSlide="None" ExpandDuration="0" CollapseDuration="0"
                                                                ClientSideOnNodeSelect="LoadProgress" ContentLoadingImageUrl="./images/spinner.gif"
                                                                ClientSideOnNodeCheckChanged="NodeChecked" ClientSideOnNodeCollapse="Collapse"
                                                                ClientSideOnNodeExpand="Expand" ClientSideOnNodeRename="treeSearch_onNodeRename"
                                                                ItemSpacing="0" CollapseNodeOnSelect="False" ClientScriptLocation="./treeview_scripts"
                                                                AutoScroll="False" LineImagesFolderUrl="images/lines/" ShowLines="True" NodeRowCssClass="TreeFont"
                                                                SelectedNodeCssClass="SelectedNode" HoverNodeCssClass="NodeHover">
                                                            </componentart:TreeView>
                                                            <input id="NodeId" type="hidden" name="NodeId" runat="server"></div>
                                                        </ContentTemplate>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
            </td>
    </table>
    <td valign="top" nowrap align="left" width="90%">
        <!-- Advanced Search table -->
        <table class="PageContent" id="tblAdvancedSearch" style="border-right: #cccccc 2px solid;
            border-top: #cccccc 2px solid; visibility: hidden; border-left: #cccccc 2px solid;
            border-bottom: #cccccc 2px solid" cellspacing="2" cellpadding="2" width="99%"
            border="0" runat="server">
            <tr valign="top">
                <td valign="top">
                    <table class="PageContent" cellspacing="2" cellpadding="2" width="100%" border="0">
                        <tr width="100%">
                            <td width="100%">
                                <span style="font-size: 8pt; font-family: 'Trebuchet MS'"><a onclick="parent.window.landMode();"
                                    href="#">&lt;&lt; Home</a></span>
                            </td>
                        </tr>
                    </table>
                    <table class="PageContent" style="border-right: #eaf0f7 1px solid; border-top: #eaf0f7 1px solid;
                        border-left: #eaf0f7 1px solid; border-bottom: #eaf0f7 1px solid" cellspacing="0"
                        cellpadding="0" width="100%" border="0">
                        <tr>
                            <td bgcolor="#f7f7f7">
                                <asp:UpdatePanel ID="uAdvancedSearch" runat="server">
                                    <ContentTemplate>
                                        <table id="Table1" cellspacing="0" cellpadding="0" width="95%" border="0">
                                            <tr>
                                                <td colspan="1">
                                                    <span class="Heading"><strong><font color="#808080">Advanced Search:</font></strong>
                                                    </span>
                                                    <br>
                                                    <span class="SearchGrid">Select one or more documents types from the tree.</span>
                                                    <br>
                                                </td>
                                                <td align="right">
                                                    <asp:ImageButton ID="lnkSearch" runat="server" ImageUrl="Images/btn_search.gif">
                                                    </asp:ImageButton>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <table id="Table2" cellspacing="0" cellpadding="0" width="100%" border="0">
                                                        <tr>
                                                            <td>
                                                                <img height="1" src="spacer.gif" width="12">
                                                            </td>
                                                            <td nowrap width="100%" colspan="1" rowspan="2">
                                                                <div id="AttributeLoadMessage" style="display: none">
                                                                </div>
                                                                <div id="AttributeGrid">
                                                                    <asp:DataGrid ID="dgSearchAttributes" runat="server" GridLines="None" AutoGenerateColumns="False"
                                                                        ShowHeader="False" BorderStyle="None" CssClass="SearchGrid">
                                                                        <Columns>
                                                                            <asp:TemplateColumn HeaderText="Attribute">
                                                                                <HeaderStyle Width="196px"></HeaderStyle>
                                                                                <ItemTemplate>
                                                                                    <span id="NameSpan" runat="server">
                                                                                        <asp:Label ID="lblAttribute" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AttributeName") %>'>
                                                                                        </asp:Label><img height="1" alt="" src="../images/spacer.gif" width="4"><input id="hiddenAttributeId"
                                                                                            type="hidden" size="1" value='<%# DataBinder.Eval(Container, "DataItem.AttributeId") %>'
                                                                                            name="hiddenAttributeId" runat="server">
                                                                                    </span>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateColumn>
                                                                            <asp:TemplateColumn HeaderText="Value">
                                                                                <HeaderStyle Width="100%"></HeaderStyle>
                                                                                <ItemStyle Wrap="False"></ItemStyle>
                                                                                <ItemTemplate>
                                                                                    <span id="ValueSpan" style="width: 100%" runat="server">
                                                                                        <asp:DropDownList ID="ddlSearchType" runat="server" Width="85px" Font-Size="8pt"
                                                                                            CssClass="SearchGrid" AutoPostBack="True">
                                                                                            <asp:ListItem Value="1">Starts With</asp:ListItem>
                                                                                            <asp:ListItem Value="0">Equals</asp:ListItem>
                                                                                            <asp:ListItem Value="2">Contains</asp:ListItem>
                                                                                            <asp:ListItem Value="5">Between</asp:ListItem>
                                                                                        </asp:DropDownList>
                                                                                        <asp:TextBox ID="txtAttributeValue" runat="server" Width="180px" Font-Size="8pt"
                                                                                            Font-Names="Trebuchet MS" Height="21px"></asp:TextBox>
                                                                                        <asp:TextBox ID="txtBetweenFrom" runat="server" Width="78px" Font-Size="8pt" Font-Names="Trebuchet MS"
                                                                                            Height="21px"></asp:TextBox><span class="SearchGrid" id="AndLabel" runat="server">&nbsp;and</span>
                                                                                        <asp:TextBox ID="txtBetweenTo" runat="server" Width="78px" Font-Size="8pt" Font-Names="Trebuchet MS"
                                                                                            Height="21px"></asp:TextBox>
                                                                                        <asp:DropDownList ID="ddlAttributeList" runat="server" Width="330px" Font-Size="8pt"
                                                                                            CssClass="SearchGrid" Height="21px" Visible="False">
                                                                                        </asp:DropDownList>
                                                                                        <asp:CheckBox ID="chkBox" runat="server" Font-Size="8pt" CssClass="SearchGrid" Visible="False">
                                                                                        </asp:CheckBox>&nbsp;
                                                                                        <asp:Label ID="lblRowError" runat="server" ForeColor="Red" Visible="False"></asp:Label></span>
                                                                                </ItemTemplate>
                                                                                <FooterTemplate>
                                                                                    &nbsp;&nbsp;
                                                                                </FooterTemplate>
                                                                            </asp:TemplateColumn>
                                                                            <asp:TemplateColumn Visible="False" HeaderText="AttributeId">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblAttributeId" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AttributeId") %>'>
                                                                                    </asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateColumn>
                                                                            <asp:TemplateColumn Visible="False" HeaderText="Format">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblAttributeDataType" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AttributeDataType") %>'>
                                                                                    </asp:Label>
                                                                                    <asp:Label ID="lblLength" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Length") %>'>
                                                                                    </asp:Label>
                                                                                    <asp:Label ID="lblListViewStyle" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ListViewStyle") %>'>
                                                                                    </asp:Label>
                                                                                    <asp:Label ID="lblDataFormat" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DataFormat") %>'>
                                                                                    </asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateColumn>
                                                                            <asp:TemplateColumn HeaderText="Validation">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="Label1" runat="server" Visible="False" ForeColor="Red"></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateColumn>
                                                                        </Columns>
                                                                    </asp:DataGrid>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
 
 
 
                                 <span class="SearchSubHeading">
                                    <img height="1" src="Images/spacer.GIF" width="6"><strong><em><font size="2">Options</font></em></strong></span>
                            </td>
                            </SPAN></tr>
                        <tr>
                            <td colspan="2">
                                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                    <tr valign="middle">
                                        <td class="Text" nowrap align="left" width="50">
                                            <img height="1" src="spacer.gif" width="5">
                                            Folder:
                                        </td>
                                        <td valign="middle" nowrap align="left">
                                            <span id="FolderPath" style="font-size: 8pt; font-family: 'Trebuchet MS'" runat="server">
                                                [No Folder Selected] </NO></span>
                                        </td>
                                        <td width="100%">
                                            <img id="ChooseFolder" style="cursor: hand" onclick="SearchFolderLookup()" alt="Choose Folder"
                                                src="Images/addToFolder_btn.gif" runat="server"><img height="1" src="spacer.gif"
                                                    width="5"><img id="ClearFolder" style="cursor: hand" onclick="document.all.SearchFolderId.value = '';document.all.FolderPath.innerHTML = '[No Folder Selected]';"
                                                        alt="Clear Folder" src="Images/clear.gif" runat="server">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="Text" nowrap align="right" width="90">
                                            &nbsp;
                                        </td>
                                        <td colspan="2">
                                            <asp:CheckBox ID="chkSubfolders" runat="server" CssClass="Text" Text="Include subfolders">
                                            </asp:CheckBox>
                                        </td>
                                    </tr>
                                </table>
                                <table id="Table3" cellspacing="0" cellpadding="0" width="100%" border="0">
                                    <tr>
                                        <td class="Text" nowrap align="left" width="145">
                                            &nbsp; Last Modified/Created:&nbsp;
                                        </td>
                                        <td align="left">
                                            <asp:DropDownList ID="ddlModifiedUser" Font-Names="Trebuchet MS" Font-Size="8pt"
                                                Width="350" runat="server">
                                            </asp:DropDownList>
                                            <font size="2"></font>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" colspan="2">
                                            <span class="Text">
                                                <asp:Label ID="lblError" runat="server" CssClass="Error" ForeColor="Red"></asp:Label><font
                                                    size="2"></font></span>
                                        </td>
                                    </tr>
                                </table>
                                                                   </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="treeSearch" EventName="NodeCheckChanged" />
                                        <asp:AsyncPostBackTrigger ControlID="treeSearch" EventName="NodeSelected" />
                                    </Triggers>
                                </asp:UpdatePanel>

                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </td>
    </tr> </table> </td> </TR>
    <tr>
        <td id="btmrow" nowrap colspan="2">
            <uc1:TopRight ID="docFooter" runat="server"></uc1:TopRight>
        </td>
    </tr>
    </TBODY></TABLE>
    <asp:UpdatePanel ID="uResults" runat="server">
        <ContentTemplate>
            <!-- results -->
            <div id="divResults" style="border-right: lightgrey 1px solid; border-top: lightgrey 1px solid;
                left: 0px; visibility: hidden; overflow: hidden; border-left: lightgrey 1px solid;
                width: 0px; border-bottom: lightgrey 1px solid; position: absolute; top: 0px;
                height: 0px; background-color: transparent">
                <tr height="100%">
                    <td bgcolor="#f7f7f7" colspan="3">
                        <table class="PageContent" cellspacing="2" cellpadding="2" width="100%" border="0">
                            <tr style="overflow: hidden; height: 20px" width="100%">
                                <td nowrap>
                                    <span style="font-size: 8pt; font-family: 'Trebuchet MS'"><a onclick=";parent.window.landMode();document.getElementById('btnHome').click();"
                                        href="#">&lt;&lt; Home</a></span>
                                </td>
                                <td style="overflow: hidden" nowrap align="right" width="100%" height="20">
                                    <img height="1" src="spacer.gif" width="15"><span class="Text" id="SearchCriteria"
                                        style="overflow: hidden" runat="server"></span>
                                </td>
                            </tr>
                        </table>
                        <table class="resultGradient" height="25" cellspacing="0" cellpadding="0" width="100%"
                            border="0">
                            <tr style="border-top: #ddd 1px solid; font-weight: bold; font-size: 8pt; font-family: 'Trebuchet MS'" height="25"
                                width="100%">
                                <td class="PageContent" id="resultHeader" valign="top" nowrap align="left">
                                    <img height="1" src="spacer.gif" width="7">
                                    <span id="BackCell"><a href="#">
                                        <img id="btnBack" style="cursor: hand" onclick="Back()" src="Images/back.btn.gif"
                                            border="0" runat="server"></a></span>
                                </td>
                                <td nowrap align="left">
                                    <span class="FolderName" id="FolderHeading" runat="server"></span>
                                </td>
                                <td id="MassCell" align="right" width="100%" style="vertical-align:bottom;">
                                    <span id="MassIcons">
                                        <img style="cursor: hand" onclick="if (confirm('Delete selected documents?')) { document.getElementById('btnDeleteCheck').click(); } else { return false; }"
                                            alt="Delete" src="Images/Search/redbar_icons/delete_red_menu_bar.gif" border="0">
                                        <asp:ImageButton ID="btnDeleteCheck" runat="server" Width="0px" ImageUrl="Images/Search/redbar_icons/delete_red_menu_bar.gif"
                                            ToolTip="Delete" Height="0px"></asp:ImageButton><asp:ImageButton ID="btnMoveCheck"
                                                runat="server" Width="0px" ImageUrl="Images/Search/redbar_icons/move_red_menu_bar.gif"
                                                ToolTip="Move" Height="0px"></asp:ImageButton>&nbsp;<img style="cursor: hand" onclick="getMoveFolder();"
                                                    alt="Move" src="Images/Search/redbar_icons/move_red_menu_bar.gif" border="0">&nbsp;<asp:ImageButton
                                                        ID="btnEmailCheck" runat="server" ImageUrl="Images/Search/redbar_icons/email_red_menu_bar.gif"
                                                        ToolTip="E-Mail" BackColor="Transparent"></asp:ImageButton>&nbsp;<asp:ImageButton ID="btnMassDownload"
                                                            runat="server" Width="16px" ImageUrl="Images/Search/redbar_icons/download_red_menu_bar.gif"
                                                            ToolTip="Download" Height="16px"></asp:ImageButton><span onclick="showDownloadOptions();">&nbsp;<img
                                                                style="cursor: hand" alt="Download Options" src="Images/cog.png">
                                                            </span>&nbsp;<asp:ImageButton ID="btnStaple" runat="server" Width="16px" ImageUrl="Images/Search/staple.gif"
                                                                ToolTip="Staple Documents" Height="16px"></asp:ImageButton></span><span id="divRestore"
                                                                    style="visibility: hidden"><asp:Button ID="btnRestore" runat="server" Width="110px"
                                                                        Height="20px" Text="Restore Documents"></asp:Button><input style="width: 110px; height: 20px"
                                                                            onclick="emptyRecycleBin();" type="button" value="Empty Recycle Bin"></span>
                                    &nbsp;
                                </td>
                            </tr>
                        </table>
                        <table height="20" cellspacing="0" cellpadding="0" width="100%" border="0">
                            <tr style="border-top: #ddd 1px solid; font-weight: bold; font-size: 8pt; background-image: url(./Images/headerbar.gif);
                                color: black; background-repeat: repeat-x; font-family: 'Arial'" width="100%">
                                <td valign="middle" align="left" width="30">
                                    <img height="1" src="spacer.gif" width="5"><asp:CheckBox ID="chkMassActions" runat="server">
                                    </asp:CheckBox>
                                </td>
                                <td valign="middle" align="left" width="100%">
                                    &nbsp; Document/Folder/Filename
                                </td>
                                <td valign="middle" nowrap align="left" width="100%">
                                    <img height="1" src="spacer.gif" width="7">Document Number/Document Text/Date Modified
                                </td>
                            </tr>
                        </table>
                        <div id="divInnerResults" style="border-top-width: 1px; border-left-width: 1px; border-left-color: lightgrey;
                            visibility: visible; border-bottom-width: 1px; border-bottom-color: lightgrey;
                            overflow: auto; width: 100%; border-top-color: lightgrey; height: 200px; background-color: #f7f7f7;
                            border-right-width: 1px; border-right-color: lightgrey">
                            <span class="PageContent" id="NoResults" runat="server"></span>
                            <asp:DataGrid ID="dgResults" runat="server" Width="100%" AutoGenerateColumns="False"
                                ShowHeader="False" BorderStyle="None" CssClass="PageContent" CellPadding="0"
                                ItemStyle-BackColor="#ffffff">
                                <AlternatingItemStyle BorderStyle="None" BackColor="#E0E0E0"></AlternatingItemStyle>
                                <ItemStyle BorderStyle="None" BackColor="White"></ItemStyle>
                                <Columns>
                                    <asp:TemplateColumn>
                                        <HeaderStyle Width="100%"></HeaderStyle>
                                        <ItemTemplate>
                                            <table class="ResultAttributes" width="100%" id="ResultRow" runat="server" cellpadding="0"
                                                cellspacing="0">
                                                <tr>
                                                    <td nowrap width="15" align="center" valign="top">
                                                        <asp:CheckBox ID="chkMass" runat="server"></asp:CheckBox><br>
                                                    </td>
                                                    <td valign="top">
                                                        <asp:Image AlternateText="View" ID="imgView" ImageUrl="Images/Search/results/view.gif"
                                                            runat="server"></asp:Image><br>
                                                        <img src="Images/Search/results/download.gif" alt="Download" onclick="download(<%# DataBinder.Eval(Container, "DataItem.CurrentVersionId") %>,<%# DataBinder.Eval(Container, "DataItem.ImageType") %>,'<%# DataBinder.Eval(Container, "DataItem.Filename") %>');" />
                                                        <%--<asp:ImageButton AlternateText="Download" CommandName="Download" ID="imgDownload"
                                                            ImageUrl="Images/Search/results/download.gif" runat="server"></asp:ImageButton>--%>
                                                    </td>
                                                    <td nowrap width="22" align="center" valign="top">
                                                        <asp:Image ID="imgIcon" runat="server" ImageUrl="Images/folders/scan_preview.gif"
                                                            Height="16" Width="16"></asp:Image>
                                                    </td>
                                                    <td nowrap width="100%" align="left" valign="top">
                                                        <%--<asp:LinkButton ID="lnkFile" runat="server" Visible="False" CommandName="Download"></asp:LinkButton>--%>
                                                        <b><%# DataBinder.Eval(Container, "DataItem.Filename") %></b>
                                                        <asp:Label ID="DocInfo" runat="server" Visible="False"></asp:Label>
                                                    </td>
                                                    <td nowrap width="40" valign="top">
                                                        <asp:Label ID="DocNo" runat="server" Visible="False"></asp:Label>
                                                    </td>
                                                    <td nowrap width="45" valign="top" style="display: none">
                                                        <asp:Label ID="Hits" runat="server" Visible="False"></asp:Label>
                                                    </td>
                                                    <td nowrap width="30" valign="top" style="display: none">
                                                        <asp:Label ID="Rel" runat="server" Visible="False"></asp:Label>
                                                    </td>
                                                    <td nowrap width="160" align="right" valign="top">
                                                        <asp:Label ID="Date" runat="server" Visible="False"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="3">
                                                    </td>
                                                    <td colspan="5" width="100%">
                                                        <asp:Label ID="Att" runat="server" Visible="False"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                            <asp:Label ID="ImageType" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ImageType") %>'
                                                Visible="False">
                                            </asp:Label>
                                            <asp:Label ID="ImageId" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ImageId") %>'
                                                Visible="False">
                                            </asp:Label>
                                            <asp:Label ID="VersionId" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CurrentVersionId") %>'
                                                Visible="False">
                                            </asp:Label>
                                            <asp:Label ID="Title" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DocumentTitle") %>'
                                                Visible="False">
                                            </asp:Label>
                                            <asp:Label ID="Pages" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CurrentPageCount") %>'
                                                Visible="False">
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                        </div>
                        <table class="PageContent" height="20" cellspacing="0" cellpadding="0" width="100%"
                            border="0">
                            <tr style="border-top: #ddd 1px solid; font-weight: bold; font-size: 8pt; background-image: url(./Images/headerbar.gif);
                                color: black; background-repeat: repeat-x; font-family: 'Trebuchet MS'" width="100%">
                                <td valign="top" align="left" width="100%">
                                    <img height="1" src="spacer.gif" width="7">
                                </td>
                                <td valign="top" nowrap align="left">
                                    <img height="1" src="spacer.gif" width="7">
                                    <asp:Label ID="lblPages" runat="server" ForeColor="Black" BackColor="Transparent"
                                        Visible="True"></asp:Label>
                                </td>
                                <td valign="top" nowrap align="right" width="100%">
                                    <asp:Label ID="lblSummary" CssClass="Text" runat="server" BackColor="Transparent"
                                        Visible="True"></asp:Label><img height="1" src="spacer.gif" width="7">
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                </TABLE></TABLE>
            </div>
            <div id="hiddenObjects" style="display: none">
                <asp:ImageButton ID="btnBrowseFolder" runat="server" Width="0px" Height="0px"></asp:ImageButton><asp:ImageButton
                    ID="btnRename" runat="server" Width="0px" Height="0px"></asp:ImageButton><asp:ImageButton
                        ID="btnQuickSort" runat="server" Width="0px" Height="0px"></asp:ImageButton><asp:ImageButton
                            ID="btnAdvancedSort" runat="server" Width="0px" Height="0px"></asp:ImageButton><asp:ImageButton
                                ID="btnFolderSort" runat="server" Width="0px" Height="0px"></asp:ImageButton><asp:ImageButton
                                    ID="btnDownload" runat="server" Width="0px" Height="0px"></asp:ImageButton><input
                                        id="UpdateFlag" type="hidden" name="UpdateFlag" runat="server">
                <input id="LastScope" type="hidden" name="LastScope" runat="server">
                <input id="BrowseFolder" type="hidden" name="BrowseFolder" runat="server">
                <input id="SearchType" type="hidden" name="SearchType" runat="server">
                <input id="Sort" type="hidden" name="Sort" runat="server">
                <input id="SortDirection" type="hidden" name="SortDirection" runat="server"><input
                    id="SortAttributeId" type="hidden" runat="server">
                <input id="PageNo" type="hidden" value="1" name="PageNo" runat="server">
                <input id="BackMethod" type="hidden" name="BackMethod" runat="server">
                <input id="BackValue" type="hidden" name="BackValue" runat="server">
                <input id="NodeText" type="hidden" name="NodeText" runat="server">
                <asp:ImageButton ID="btnQuickPaging" runat="server" Width="0px" Height="0px"></asp:ImageButton><input
                    id="DocList" type="hidden" name="DocList" runat="server">
                <input id="LoadSearch" type="hidden" name="LoadSearch" runat="server">
                <input id="NewText" type="hidden" name="NewText" runat="server">
                <input id="UpFolder" type="hidden" name="UpFolder" runat="server"><input id="NoDocMode"
                    type="hidden" name="UpFolder" runat="server"><input id="ParentFolder" type="hidden"
                        name="UpFolder" runat="server">
                <asp:ImageButton ID="btnNoAccess" runat="server" Width="0px" Height="0px"></asp:ImageButton><asp:ImageButton
                    ID="btnCabinetSort" runat="server" Width="0px" Height="0px"></asp:ImageButton><asp:ImageButton
                        ID="btnMassDelete" runat="server" Width="0px" Height="0px"></asp:ImageButton><asp:ImageButton
                            ID="btnDeleteUnselect" runat="server" Width="0px" Height="0px"></asp:ImageButton><asp:ImageButton
                                ID="btnDeleteShade" runat="server" Width="0px" Height="0px"></asp:ImageButton><asp:ImageButton
                                    ID="btnMoveShade" runat="server" Width="0px" Height="0px"></asp:ImageButton><asp:ImageButton
                                        ID="btnMassMove" runat="server" Width="0px" Height="0px">
                </asp:ImageButton><asp:ImageButton ID="btnDownloadCheck" runat="server" Width="0px"
                    Height="0px"></asp:ImageButton><asp:ImageButton ID="btnDownloadUnselect" runat="server"
                        Width="0px" Height="0px"></asp:ImageButton><asp:ImageButton ID="btnDownloadShade"
                            runat="server" Width="0px" Height="0px"></asp:ImageButton><asp:ImageButton ID="btnEmailUnselect"
                                runat="server" Width="0px" Height="0px"></asp:ImageButton><asp:ImageButton ID="btnPrintAttachUnselect"
                                    runat="server" Width="0px" Height="0px"></asp:ImageButton><asp:ImageButton ID="btnPrintAttachShade"
                                        runat="server" Width="0px" Height="0px"></asp:ImageButton><asp:ImageButton ID="btnPrintCheck"
                                            runat="server" Width="0px" Height="0px"></asp:ImageButton><asp:ImageButton ID="btnPrintShade"
                                                runat="server" Width="0px" Height="0px"></asp:ImageButton><asp:ImageButton ID="LoadAttributes"
                                                    runat="server" Width="0px" Height="0px"></asp:ImageButton><asp:ImageButton ID="btnBrowseCabinet"
                                                        runat="server" Width="0px" Height="0px">
                </asp:ImageButton><asp:ImageButton ID="btnUpdateRecent" runat="server" Width="0px"
                    Height="0px"></asp:ImageButton><asp:ImageButton ID="btnTrackExpandCollapse" runat="server"
                        Width="0px" Height="0px"></asp:ImageButton><asp:ImageButton ID="btnPrintUnselect"
                            runat="server" Width="0px" Height="0px"></asp:ImageButton><asp:ImageButton ID="btnEmailShade"
                                runat="server" Width="0px" Height="0px"></asp:ImageButton><input id="MoveFolder"
                                    type="hidden" name="MoveFolder" runat="server"><input id="TrackAction" type="hidden"
                                        name="TrackAction" runat="server"><input id="TrackNode" type="hidden" name="TrackNode"
                                            runat="server">
                <input id="SearchFolderId" type="hidden" name="SearchFolderId" runat="server"><input
                    id="NodeValue" type="hidden" runat="server"><input id="SearchDocuments" type="hidden"
                        runat="server"><input id="CurrentPage" type="hidden" name="Hidden1" runat="server"><input
                            id="FolderName" type="hidden" runat="server"><input id="MoveToFolder" type="hidden"
                                runat="server">
                <asp:ImageButton ID="btnRecycleCheck" runat="server" Width="0px" Height="0px"></asp:ImageButton><asp:ImageButton
                    ID="btnHome" runat="server" Width="0px" Height="0px"></asp:ImageButton><asp:ImageButton
                        ID="btnSort" runat="server" Width="0px" Height="0px"></asp:ImageButton><asp:ImageButton
                            ID="btnPaging" runat="server" Width="0px" Height="0px"></asp:ImageButton>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="treeSearch" EventName="NodeSelected" />
            <asp:AsyncPostBackTrigger ControlID="lnkSearch" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnQuickSearch" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnEmailCheck" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnMassDownload" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
    <table id="frameLoading" style="z-index: 12; left: -400px; visibility: hidden; position: absolute;
        top: -150px" bordercolor="#bd0000" cellspacing="0" cellpadding="0" border="0"
        runat="server">
        <tr style="border-top: lightgrey 1px solid">
            <td valign="top" width="100%" colspan="3">
                <iframe id="iFrameLoading" name="iFrameLoading" src="LoadImage.htm" frameborder="no"
                    width="350" scrolling="no" height="80" runat="server"></iframe>
                <div id="svcPrint" style="behavior: url(webservice.htc)" onresult="Print()">
                </div>
            </td>
        </tr>
    </table>
    <iframe id="frameAdd" style="border-right: black thin; border-top: black thin; left: 0px;
        visibility: hidden; overflow: auto; border-left: black thin; width: 0px; border-bottom: black thin;
        position: absolute; top: 0px; height: 0px; background-color: transparent" src="dummy.htm"
        frameborder="no"></iframe>
    <iframe id="framePreview" style="border-right: #bd0000 thin solid; border-top: #bd0000 thin solid;
        visibility: hidden; overflow: auto; border-left: #bd0000 thin solid; width: 150px;
        border-bottom: #bd0000 thin solid; position: absolute; top: 0px; height: 190px"
        marginwidth="0" marginheight="0" src="dummy.htm" frameborder="no" scrolling="no">
    </iframe>
    <!-- default landing/home page -->
    <div id="frameLanding" style="left: -2000px; width: 900px; position: absolute; top: -2000px;
        height: 900px; background-color: transparent" scrolling="auto" frameborder="no">
        <table style="background-color: transparent" cellspacing="0" cellpadding="0" width="100%"
            align="left" border="0">
            <tr style="background-color: transparent">
                <td id="pagecontent" style="background-color: transparent">
                    <td id="girl" style="background-color: transparent" valign="top">
                        <div id="coretext" style="background-color: transparent">
                            <table class="alert" id="tblAlert" style="background-color: transparent" cellspacing="5"
                                cellpadding="5" width="100%" border="0" runat="server">
                                <tr bgcolor="#d8e4ef">
                                    <td id="tree" width="50%">
                                        Account Alert<span id="AlertMessage" runat="server"></span>
                                    </td>
                                </tr>
                            </table>
                            <table class="select" style="background-color: transparent" cellspacing="2" cellpadding="0"
                                width="100%" border="0">
                                <tr>
                                    <td id="borders" valign="top" width="50%">
                                        <table style="background-color: transparent" cellspacing="5" cellpadding="5" width="100%"
                                            border="0">
                                            <tr>
                                                <th valign="top" align="left">
                                                    Quick Start
                                                </th>
                                            </tr>
                                            <tr>
                                                <td id="tree" width="50%" bgcolor="#f7f7f7">
                                                    <table cellspacing="4" cellpadding="4" width="100%" border="0">
                                                        <tr style="font-size: 11px; cursor: hand" onclick="saveCookie('UploadFolderId',0,30);addMode('0');resetTimeout();">
                                                            <td align="center" width="32" bgcolor="#eaf0f7">
                                                                <img height="32" src="landing_page/images/import.btn.gif" width="32">
                                                            </td>
                                                            <td class="RecentHover" bgcolor="#eaf0f7">
                                                                <a href="#"><strong>Add New Files</strong></a><br>
                                                                Upload files from your computer into docAssist.
                                                            </td>
                                                        </tr>
                                                        <tr id="trScan" style="font-size: 11px; cursor: hand" runat="server">
                                                            <td align="center" width="32" bgcolor="#d8e4ef">
                                                                <img height="32" src="landing_page/images/scan.btn.gif" width="32">
                                                            </td>
                                                            <td class="RecentHover" bgcolor="#d8e4ef">
                                                                <a href="#"><strong><span>Scan Documents</span><br>
                                                                </strong></a>Scan paper documents using your desktop scanner.
                                                            </td>
                                                        </tr>
                                                        <tr id="trAdvancedSearch" style="font-size: 11px; cursor: hand" onclick="parent.window.advancedSearch();"
                                                            runat="server">
                                                            <td align="center" width="32" bgcolor="#eaf0f7">
                                                                <img height="32" src="landing_page/images/search.btn.gif" width="32">
                                                            </td>
                                                            <td class="RecentHover" bgcolor="#eaf0f7">
                                                                <a href="#"><strong>Advanced Search</strong></a><br>
                                                                Search for documents by type or attribute.
                                                            </td>
                                                        </tr>
                                                        <tr id="trUsers" style="font-size: 11px; cursor: hand" onclick="SetUser();" runat="server">
                                                            <td align="center" width="32" bgcolor="#d8e4ef">
                                                                <img height="32" src="landing_page/images/users.btn.gif" width="32">
                                                            </td>
                                                            <td class="RecentHover" bgcolor="#d8e4ef">
                                                                <a href="#"><strong>Add New Users</strong></a><br>
                                                                Create and manage new user accounts.
                                                            </td>
                                                        </tr>
                                                        <tr id="trGroups" style="font-size: 11px; cursor: hand" onclick="AddGroup();" runat="server">
                                                            <td align="center" width="32" bgcolor="#eaf0f7">
                                                                <img height="32" src="landing_page/images/groups.btn.gif" width="32">
                                                            </td>
                                                            <td class="RecentHover" bgcolor="#eaf0f7">
                                                                <a href="#"><strong>Add New Groups</strong></a>
                                                                <br>
                                                                Create and manage new user group security.
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td id="borders" valign="top" width="50%">
                                        <table style="background-color: transparent" height="100%" cellspacing="5" cellpadding="5"
                                            width="100%" border="0">
                                            <tr>
                                                <th valign="top" align="left" bgcolor="#f7f7f7">
                                                    Recent Documents
                                                </th>
                                            </tr>
                                            <tr>
                                                <td valign="top" width="50%">
                                                    <span id="NoRecent" style="font-size: 8pt; font-family: 'Trebuchet MS'" runat="server">
                                                    </span>
                                                    <asp:DataGrid ID="dgRecentItems" runat="server" Width="100%" EnableViewState="False"
                                                        GridLines="None" AutoGenerateColumns="False" ShowHeader="False" CssClass="RecentHover"
                                                        CellSpacing="0" CaptionAlign="Left" HorizontalAlign="Left">
                                                        <AlternatingItemStyle HorizontalAlign="Left" BackColor="White"></AlternatingItemStyle>
                                                        <ItemStyle HorizontalAlign="Left" Height="0px" VerticalAlign="Top" BackColor="#EAF0F7">
                                                        </ItemStyle>
                                                        <Columns>
                                                            <asp:TemplateColumn HeaderText="Icon">
                                                                <HeaderStyle></HeaderStyle>
                                                                <ItemStyle Width="25" HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="RecentKey" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.RecentKey") %>'
                                                                        Visible="False">
                                                                    </asp:Label>
                                                                    <asp:Label ID="Value" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Value") %>'
                                                                        Visible="False">
                                                                    </asp:Label>
                                                                    <asp:Label ID="KeyValue" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.KeyValue") %>'
                                                                        Visible="False">
                                                                    </asp:Label>
                                                                    <asp:Label ID="Filename" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Filename") %>'
                                                                        Visible="False">
                                                                    </asp:Label>
                                                                    <asp:Label ID="ImageType" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ImageType") %>'
                                                                        Visible="False">
                                                                    </asp:Label>
                                                                    <asp:Image ID="imgIcon" runat="server" Width="16px" Height="16px"></asp:Image>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="Description">
                                                                <HeaderStyle></HeaderStyle>
                                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
                                                                <ItemTemplate>
                                                                    <a href="#">
                                                                        <asp:Label ID="lblDesc" runat="server" Font-Names="Trebuchet MS" Font-Size="8pt"></asp:Label></a>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                        </Columns>
                                                    </asp:DataGrid>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <span style="font-size: 8pt; font-family: 'Trebuchet MS'">Have more questions? Click
                                "Help" above.</span>
                        </div>
                    </td>
            </tr>
        </table>
        </TD></TR></TABLE>
    </div>
    <!-- download options dialog -->
    <div id="downloadOptions" style="border-right: #bd0000 thin solid; border-top: #bd0000 thin solid;
        left: -150px; border-left: #bd0000 thin solid; width: 0px; border-bottom: #bd0000 thin solid;
        position: absolute; top: -150px; height: 0px; background-color: #ffffff">
        <table class="SearchGrid" cellspacing="0" cellpadding="0" width="100%">
            <tr>
                <td>
                    Download documents as:
                </td>
                <td align="right">
                    <img style="cursor: hand" onclick="hideDownloadOptions();" src="Images/list.close.gif"
                        border="0">
                </td>
            </tr>
            <tr>
                <td align="left" colspan="2">
                    <asp:RadioButton ID="rdoPdf" runat="server" Text="PDF" GroupName="DownloadAs" Checked="True">
                    </asp:RadioButton><asp:RadioButton ID="rdoTiff" runat="server" Text="TIFF" GroupName="DownloadAs">
                    </asp:RadioButton>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:CheckBox ID="chkSplitPages" runat="server" Text="Download as single pages">
                    </asp:CheckBox>
                </td>
            </tr>
        </table>
    </div>
    <!-- sort popup menu -->
    <div class="SearchPopup" style="width: 0px; height: 0px">
        <table id="AttributePopup" onmouseover="showSearch(lastCell);" style="display: none"
            onmouseout="hideSearch();" height="0" cellspacing="0" cellpadding="0" width="0">
            <tr id="similarSearch">
                <td>
                    Search Similar
                </td>
            </tr>
            <tr id="aSort">
                <td>
                    Sort Ascending
                </td>
            </tr>
            <tr id="dSort">
                <td>
                    Sort Descending
                </td>
            </tr>
        </table>
    </div>
    <!-- recycle bin dialog -->
    <div class="lb_popup" id="binDialog">
        <table height="100%" cellspacing="0" cellpadding="0" width="100%">
            <tr>
                <td colspan="2">
                    <span class="popup_caption" id="InfoMsg">Empty Recycle Bin</span>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <img height="5" src="spacer.gif" width="1">
                </td>
            </tr>
            <tr>
                <td style="height: 21px">
                    <img height="1" src="spacer.gif" width="25">
                </td>
                <td style="height: 21px" width="100%">
                    <asp:RadioButton ID="rdoRecyleAll" runat="server" Text="All documents" GroupName="RecycleBin"
                        Checked="True"></asp:RadioButton>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <img height="5" src="spacer.gif" width="1">
                </td>
            </tr>
            <tr>
                <td width="25">
                </td>
                <td>
                    <asp:RadioButton ID="rdoRecycleDate" runat="server" Text="Documents older than" GroupName="RecycleBin">
                    </asp:RadioButton><asp:TextBox ID="recycleTime" runat="server"></asp:TextBox><input
                        id="emptyMode" type="hidden" value="0" runat="server">
                </td>
            </tr>
            <tr>
                <td align="right" colspan="2">
                    <img height="5" src="spacer.gif" width="1">
                    <asp:Label ID="lblRecycleError" runat="server" CssClass="error" Visible="False"></asp:Label>
                </td>
            </tr>
            <tr>
                <td valign="bottom" align="right" colspan="2" height="100%">
                    <img style="cursor: hand" onclick="hideRecycleBin();" src="./Images/btn_cancel.gif"
                        border="0">&nbsp;&nbsp;<img style="cursor: hand" onclick="recycleCheck();" src="./Images/button_ok.gif"
                            border="0">
                </td>
            </tr>
        </table>
    </div>
    <!-- input dialog -->
    <div class="lb" id="inputBox" style="display: none">
        <table height="100%" cellspacing="0" cellpadding="0" width="100%">
            <tr class="title">
                <td>
                    <table>
                        <tr>
                            <td width="100%" class="title">
                                <div id="title">
                                </div>
                            </td>
                            <td align="right" valign="middle">
                                <span id="closer" class="closeButton" onclick="hideInput();">close</span>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <img src="spacer.gif" height="5" width="1">
                </td>
            </tr>
            <tr>
                <td valign="middle">
                    <table>
                        <tr>
                            <td>
                                <img src="spacer.gif" height="1" width="10">
                            </td>
                            <td>
                                <div id="label">
                                </div>
                            </td>
                            <td>
                                <input type="text" id="inputText" style="width: 235px">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <img src="spacer.gif" height="5" width="1">
                </td>
            </tr>
            <tr>
                <td height="100%" align="right">
                    <table>
                        <tr>
                            <td>
                                <span id="okButton" class="button" onclick="hideInput();"></span>
                            </td>
                            <td>
                                <img src="spacer.gif" height="1" width="5">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <div style="display: none">
        <asp:ImageButton ID="download" runat="server" Height="0px" Width="0px" />
        <input type="hidden" id="dlID" runat="server" />
        <input type="hidden" id="filenm" runat="server" /></div>
    <script language="javascript" src="scripts/cookies.js"></script>

    <script language="javascript" src="scripts/searchToolbar.js"></script>

    <script language="javascript" src="scripts/tooltip.js"></script>

    <script language="javascript" src="scripts/jquery.js"></script>

    <script language="javascript" src="scripts/jquery.autocomplete.js"></script>

    <script language="javascript" src="scripts/search.js"></script>

    <script language="javascript">

        landMode();
        //hideInput();

        var treeEnabled = true;

        var loadmsg = '<b> (loading...)</b>';
        var tagLoad = false;
        var accessDenied = false;
        var uploading = false;
        var hideLandingPage = false;
        var uploadingInProgress = false;
        var checked = false;
        var menuState;
        var clientTimeout = 60; //client timeout while browsing/searching
        var searchInProgress = false;
        var clientTimer = 0;
        var hostname;

        var mode;

        if (document.all.LoadSearch.value == 1) { resultsMode(); stopProgressDisplay(); }

        function resetTimeout() {

        }

        //pin menu down first time or when cookie is lost
        if (readCookie('docAssistQuickMenu') == '') {
            saveCookie('docAssistQuickMenu', 0, 365);
        }

        disableCabinet(); disableRename(); disableDelete(); disableSecurity(); disableAdd(); disableFavorite(); disableFolder(); disableMove();

        function SetUser(UserId) {
            var d = new Date();
            var time = d.getHours() + d.getMinutes() + d.getSeconds() + d.getMilliseconds();
            if (smallBus) {
                var settings = "help:no;status:yes;resizable:no;dialogHeight=215px;dialogWidth=656px";
            } else {
                var settings = "help:no;status:yes;resizable:no;dialogHeight=437px;dialogWidth=656px";
            }
            var returnValue = window.showModalDialog('UserEdit.aspx?Id=0&Mode=NewEdit&Time=' + time, null, settings);
            if (returnValue == "1") { window.location.replace('UserAdmin.aspx'); }
        }

        function AddGroup() {
            var nXpos = (document.body.clientWidth - 400) / 2;
            var nYpos = (document.body.clientHeight - 200) / 2;
            window.open('SecurityGroupEdit.aspx?ID=0&Ref=Explorer', 'EditGroup', 'height=700,width=895,resizable=no,status=no,toolbar=no')
        }

        function quickSearch(resetpage) {
            resetResultPage();
        }

        function enableTree() { }
        function disableToolbar() { }
        function disableTree() { }

        function FolderPermissions(folderid) {
            var d = new Date();
            var random = d.getHours() + d.getMinutes() + d.getSeconds() + d.getMilliseconds();
            window.showModalDialog('FolderPermissions.aspx?Id=' + folderid + '&' + random, 'FolderPermissions', 'resizable:yes;status:no;dialogHeight:635px;dialogWidth:675px;scroll:yes')
        }

        function SharePermissions(shareid) {
            var d = new Date();
            var random = d.getHours() + d.getMinutes() + d.getSeconds() + d.getMilliseconds();
            window.showModalDialog('SharePermissions.aspx?Id=' + shareid + '&' + random, 'SharePermissions', 'resizable:yes;status:no;dialogHeight:625px;dialogWidth:675px;scroll:yes')
        }

        function landMode() {
            mode = 'land';

            var tblAdvancedSearch = document.getElementById('tblAdvancedSearch');
            if (tblAdvancedSearch != undefined) { tblAdvancedSearch.style.visibility = 'hidden'; }

            var divResults = document.getElementById('divResults');
            divResults.style.display = 'none';

            var frameAdd = document.getElementById('frameAdd');
            frameAdd.style.display = 'none';

            var frameLanding = document.getElementById('frameLanding');
            frameLanding.style.display = 'block';
            var height = document.body.clientHeight;
            var width = document.body.clientWidth;
            frameLanding.style.top = 94;
            frameLanding.style.left = 285;
            frameLanding.style.width = width - 300;
            frameLanding.style.height = height - 132;
            frameLanding.style.visibility = 'visible';
        }

        function resultsMode() {
            try {
                mode = 'results';

                hideLandingPage = true;
                uploading = false;

                var tblAdvancedSearch = document.getElementById('tblAdvancedSearch');
                if (tblAdvancedSearch != undefined) { tblAdvancedSearch.style.visibility = 'hidden'; tblAdvancedSearch.style.display = 'none'; }

                var frameLanding = document.getElementById('frameLanding');
                if (frameLanding != undefined) {
                    frameLanding.style.display = 'none';
                }

                var divResults = document.getElementById('divResults');

                var tblQuickSearch = document.getElementById('tblQuickSearch');

                var height = document.body.clientHeight;
                var width = document.body.clientWidth;

                divResults.style.top = 94;
                divResults.style.left = 285;
                divResults.style.width = width - 300;
                divResults.style.height = height - 130;
                divResults.style.display = 'block';
                divResults.style.visibility = 'visible';

                var frameAdd = document.getElementById('frameAdd');
                if (frameAdd != undefined) {
                    var height = document.body.clientHeight;
                    var width = document.body.clientWidth;

                    frameAdd.style.top = 103;
                    frameAdd.style.left = 290;
                    frameAdd.style.width = width - 300;
                    frameAdd.style.height = height - 139;
                    frameAdd.style.display = 'none';
                    frameAdd.style.visibility = 'hidden';
                }
            }
            catch (e) {
                //alert(e.message);
            }
        }

        function searchMode() {
            mode = 'search';

            hideLandingPage = true;
            uploading = false;

            var frameLanding = document.getElementById('frameLanding');
            if (frameLanding != undefined) {
                frameLanding.style.visibility = 'hidden';
                frameLanding.style.top = -150;
                frameLanding.style.left = -150;
                frameLanding.style.height = 0;
                frameLanding.style.width = 0;
            }

            var tblAdvancedSearch = document.getElementById('tblAdvancedSearch');

            if (tblAdvancedSearch != undefined) {
                tblAdvancedSearch.style.display = 'block';
                tblAdvancedSearch.style.visibility = 'visible';
            }

            var divResults = document.getElementById('divResults');
            if (divResults != undefined) {
                divResults.style.visibility = 'hidden';
                divResults.style.top = 0;
                divResults.style.left = 0;
                divResults.style.height = 0;
                divResults.style.width = 0;
            }

            var frameAdd = document.getElementById('frameAdd');
            if (frameAdd != undefined) {
                frameAdd.style.visibility = 'hidden';
                frameAdd.style.top = 0;
                frameAdd.style.left = 0;
                frameAdd.style.height = 0;
                frameAdd.style.width = 0;
            }

            var frameLanding = document.getElementById('frameLanding');
            if (frameLanding != undefined) {
                frameLanding.style.display = 'none';
            }

        }

        function submitForm() {
            if (event.keyCode == 13) {
                startProgressDisplay();
                event.cancelBubble = true;
                event.returnValue = false;
                document.all.lnkSearch.click();
            }
        }

        function catchEsc() {
            if (event.keyCode == 27) {
                event.cancelBubble = true;
                event.returnValue = false;
                return false;
                EditModeNode(document.getElementById('NodeId').value);
            }
        }


        function checkClientTimeout() {
            return true;



            if (searchInProgress == true) {
                searchInProgress = false;
                stopProgressDisplay();
                //enableTree();
                document.getElementById('btnBrowseFolder').click();
                startProgressDisplay();
            }
        }

        function submitQuickForm() {
            if (event.keyCode == 13) {
                document.all.btnQuickSearch.click();
                event.cancelBubble = true;
                event.returnValue = false;
            }
        }

        function quickSearch() {
            var searchval = document.getElementById('txtQuickSearch').value.replace(/^\s+|\s+$/, '');
            if (searchval.length > 0) {
                clearResults();
                resetResultPage();
                startProgressDisplay();
            } else {
                event.cancelBubble = true;
                event.returnValue = false;
            }
        }

        function disable() {
            resetResultPage();
            startProgressDisplay();
        }



        function addMode(id) {
            //showAdd();
            id = readCookie('UploadFolderId');
            uploading = true;

            var tblAdvancedSearch = document.getElementById('tblAdvancedSearch');
            if (tblAdvancedSearch != undefined) { tblAdvancedSearch.style.visibility = 'hidden'; }

            var divResults = document.getElementById('divResults');
            if (divResults != undefined) {
                divResults.style.visibility = 'hidden';
                divResults.style.top = 0;
                divResults.style.left = 0;
                divResults.style.height = 0;
                divResults.style.width = 0;
            }

            var frameAdd = document.getElementById('frameAdd');
            if (frameAdd != undefined) {
                var height = document.body.clientHeight;
                var width = document.body.clientWidth;

                frameAdd.style.top = 95;
                frameAdd.style.left = 285;
                frameAdd.style.width = 700;
                frameAdd.style.height = 500;
                frameAdd.style.visibility = 'visible';
                frameAdd.style.display = 'block';
                frameAdd.src = 'UploadFiles.aspx?FolderId=' + id;
                mode = 'add';

                var frameLanding = document.getElementById('frameLanding');
                if (frameLanding != undefined) {
                    frameLanding.style.visibility = 'hidden';
                    frameLanding.style.top = -150;
                    frameLanding.style.left = -150;
                    frameLanding.style.height = 0;
                    frameLanding.style.width = 0;
                }

                //resizeElements();

            }

        }

        function loadAdvanced(id) {
            var frameAdd = document.getElementById('frameAdd');
            frameAdd.src = 'Upload.aspx?FolderId=' + id;
        }

        function resetResultPage() {
            document.getElementById('CurrentPage').value = '1';
        }

        function browseCabinet(id) {
            document.getElementById('NodeId').value = id;
        }

        function roll(obj, highlightcolor, textcolor) {
            obj.style.backgroundColor = highlightcolor;
            obj.style.color = textcolor;
        }

        function Back() {
            var BackMethod = document.getElementById('BackMethod');
            switch (BackMethod.value) {
                case "A":
                    searchMode();
                    break;
                case "F":
                    browseFolder(document.all.UpFolder.value);
                    break;
                default:
                    break;
            }
        }

        function focusField(id) {
            try {
                var txtAttribute = document.getElementById(id);
                if (txtAttribute != undefined) { txtAttribute.focus(); txtAttribute.focus(); txtAttribute.focus(); }
            }
            catch (err) {

            }
        }

        var timer

        function noDocumentsMode() {
            var tblAdvancedSearch = document.getElementById('tblAdvancedSearch');
            if (tblAdvancedSearch != undefined) { tblAdvancedSearch.style.visibility = 'hidden'; }
        }


        function uploadOn() {
            uploading = true;
            disableTree();
        }

        function uploadOff() {
            uploading = false;
            enableTree();
        }

        var height;
        var width;

        function massAction(val) {
            switch (val) {
                case "1":
                    document.getElementById('btnDeleteCheck').click();
                    break;
                case "2":
                    document.getElementById('btnMoveCheck').click();
                    break;
                case "3":
                    document.getElementById('btnDownloadCheck').click();
                    break;
                case "4":
                    document.getElementById('btnEmailCheck').click();
                    break;
                case "5":
                    document.getElementById('btnPrintCheck').click();
                    break;
                default:
                    break;
            }

            document.getElementById('ddlMass').options[0].selected = true;
        }

        function shade(rowid, check) {
            if (document.getElementById(check).checked == true) {
                document.getElementById(rowid).className = 'selected';
            }
            else {
                document.getElementById(rowid).className = 'ResultAttributes';
            }
            event.cancelBubble = true;
        }

        function shadeError(rowid) {
            document.getElementById(rowid).className = 'selectederror';
        }

        function FolderLookup() {
            var val = window.showModalDialog('MoveTree.aspx?', 'FolderLookup', 'resizable:no;status:no;dialogHeight:625px;dialogWidth:250px;scroll:no');

            if (val != undefined) {
                document.getElementById('MoveFolder').value = val;
                document.getElementById('btnMassMove').click();
            }
        }

        function massEmail(scan) {
            var d = new Date();
            var random = d.getHours() + d.getMinutes() + d.getSeconds() + d.getMilliseconds();
            var settings = "resizable:yes;status:no;dialogHeight:535px;dialogWidth:830px;scroll:no";
            window.showModalDialog('Mail.aspx?Mode=' + random + '&Scan=' + scan, 'MassMail', settings);
        }

        var PrintId;

        function massPrint(versions) {
            //document.getElementById('svcPrint').useService('http://localhost/docAssistWeb1' + "/docAssistImages.asmx?wsdl", "PrintService");
            //PrintId = document.getElementById('svcPrint').PrintService.callService("BuildMassPrint", versions); // Call the service
        }

        function Print() {
            if (event.result.error) {
                //Pull the error information from the event.result.errorDetail properties
                var xfaultcode = event.result.errorDetail.code;
                var xfaultstring = event.result.errorDetail.string;
                var xfaultsoap = event.result.errorDetail.raw;

                // Add code to handle specific codes here
                alert('An error has occured while printing this document. If the problem continues please contact support.');
            }
            else if ((!event.result.error) && (PrintId == event.result.id)) {
                var file = event.result.value;

                var imageCtrl = document.getElementById('imageCtrl');
                imageCtrl.PageNbr = 1;
                imageCtrl.FileName = file;

                var intLoop
                while (!imageCtrl.Idle) {
                    intLoop++;
                }

                document.all.objPrint.StartPrintDoc();

                for (intPage = 1; intPage <= imageCtrl.Pages; intPage++) {
                    document.all.objPrint.NewPage();
                    imageCtrl.PageNbr = intPage;
                    imageCtrl.FileName = file;

                    // Wait for image to load
                    while (!imageCtrl.Idle) {
                        intLoop++;
                    }

                    document.all.objPrint.hDib = document.all.imageCtrl.hDib;
                    document.all.objPrint.PrintDib(0, 0, document.all.objPrint.PWidth, document.all.objPrint.PHeight, 0, 0, 0, 0, true);
                }

                document.all.objPrint.EndPrintDoc();
                imageCtrl.PageNbr = intPage;

            }
        }

        function checkDirty() {
            if (uploadingInProgress) {
                event.returnValue = "All current uploads will be cancelled.";
            }
        }

        function Collapse(sender, eventArgs) {
            if ((sender.ID == 'MYFAVNODE') || (sender.ID == 'DOCNODE') || (sender.ID == 'CabsFolders') || (sender.ID == 'Views')) {
                document.getElementById('TrackAction').value = 'COLLAPSE';
                document.getElementById('TrackNode').value = sender.ID;
                document.getElementById('btnTrackExpandCollapse').click();
            }
            return true;
        }

        function Expand(sender, eventArgs) {
            if ((sender.ID == 'MYFAVNODE') || (sender.ID == 'DOCNODE') || (sender.ID == 'CabsFolders') || (sender.ID == 'Views')) {
                document.getElementById('TrackAction').value = 'EXPAND';
                document.getElementById('TrackNode').value = sender.ID;
                document.getElementById('btnTrackExpandCollapse').click();
            }
            //disableTree();
            return true;
        }

        function configAttribute(value, attributevalue, betweenfonr, betweento, label) {

            var txtAtttributeValue = document.getElementById(attributevalue);
            var txtBetweenFrom = document.getElementById(betweenfonr);
            var txtBetweenTo = document.getElementById(betweento);
            var lblBetween = document.getElementById(label);

            switch (value) {
                case '5':
                    txtAtttributeValue.style.display = 'none';
                    txtBetweenFrom.style.display = 'inline';
                    txtBetweenTo.style.display = 'inline';
                    lblBetween.style.innerHTML = 'and';
                    lblBetween.style.display = 'inline';
                    break;
                default:
                    txtAtttributeValue.style.display = 'inline';
                    txtBetweenFrom.style.display = 'none';
                    txtBetweenTo.style.display = 'none';
                    lblBetween.style.display = 'none';
                    break;
            }
        }

        function togQuickMenu() {
            if (document.getElementById('quickMenu').style.display == 'block') {
                var menu = document.getElementById('quickMenu');
                document.getElementById('quickMenuIcon').src = 'Images/Search/quickMenu/expand.gif';
                menu.style.display = 'none';
                saveCookie('docAssistQuickMenu', 0, 365);
                menuState = 'hidden';
                resizeElements();
            }
            else {
                var menu = document.getElementById('quickMenu');
                //document.getElementById('quickMenuIcon').src = 'Images/Search/quickMenu/contract.gif';
                menu.style.display = 'block';
                saveCookie('docAssistQuickMenu', 1, 365);
                menuState = 'visible';
                resizeElements();
            }
        }

        function showMenu() {
            if (menuState != 'visible') {
                var menu = document.getElementById('quickMenu');
                menu.style.position = 'absolute';
                menu.style.display = 'block';
                menuState = 'visible';
            }
            else {
                hideMenu();
            }
        }

        function hideMenu() {
            var menu = document.getElementById('quickMenu');
            menu.style.display = 'none';
            menu.style.position = 'relative';
            menuState = 'hidden';
        }

        var firstDoc;
        function advancedSearch() {

            var node = window['treeSearch'].findNodeById('DOCNODE');
            node.Expand();

            var newnode = window['treeSearch'].findNodeById('D' + firstDoc);
            newnode.SetProperty('Checked', 'true');
            newnode.SetProperty('Selected', 'true');
            newnode.SaveState();
            window['treeSearch'].Render();

            document.getElementById('DocList').value = newnode.ID.replace('D', '');

            //loadAttributes(firstDoc);
            searchMode();

        }

        function updateRecent() {
            document.getElementById('btnUpdateRecent').click();
        }


        function SearchFolderLookup() {
            var val = window.showModalDialog('Tree.aspx?', 'FolderLookup', 'resizable:no;status:no;dialogHeight:625px;dialogWidth:250px;scroll:no');
            if (val != undefined) {
                varArray = val.split('�');
                document.all.SearchFolderId.value = varArray[0].replace('W', '');
                document.all.FolderPath.innerHTML = varArray[1];
            }
        }


        function showDownloadOptions() {
            downloadOptions.style.height = 65;
            downloadOptions.style.width = 160;
            downloadOptions.style.left = event.clientX;
            downloadOptions.style.top = event.clientY;
            downloadOptions.style.display = 'block';
        }

        function hideDownloadOptions() {
            downloadOptions.style.height = 0;
            downloadOptions.style.width = 0;
            downloadOptions.style.left = -150;
            downloadOptions.style.top = -150;
            downloadOptions.style.display = 'none';
        }

        function setValue(val) {
            document.all.NodeValue.value = val;
        }

        function clearResults() {
            document.getElementById('divInnerResults').style.visibility = 'hidden';
        }

        function showResults() {
            document.getElementById('divInnerResults').style.visibility = 'visible';
            document.getElementById('chkMassActions').checked = false;
        }

        function getMoveFolder() {
            var val = window.showModalDialog('MoveTree.aspx?', 'FolderLookup', 'resizable:no;status:no;dialogHeight:625px;dialogWidth:250px;scroll:no');
            if (val != undefined) {
                document.getElementById('MoveToFolder').value = val;
                document.getElementById('btnMoveCheck').click();
            }
        }

        function showAttributes() {
            document.getElementById('AttributeLoadMessage').style.display = 'none';
            document.getElementById('AttributeGrid').style.display = 'block';
        }

        function hideAttributes() {
            document.getElementById('AttributeGrid').style.display = 'none';
            document.getElementById('AttributeLoadMessage').style.display = 'block';
        }

        function browseFolder(id) {
            var SearchType = document.getElementById('SearchType');
            SearchType.value = 'F';
            resetResultPage();
            clearResults();
            document.getElementById('NodeId').value = id;
            startProgressDisplay();
            var BrowseFolder = document.getElementById('BrowseFolder');
            if (BrowseFolder != undefined) {
                BrowseFolder.value = id;

                if (id == 2147483647) {
                    document.getElementById('SortDirection').value = 'ASC';
                }

                //document.all.btnBrowseFolder.click();
                $('#btnBrowseFolder').click();
            }
        }

        function download(vid, imageType, file) {
            event.cancelBubble = true;
            $('#dlID').val(vid + ',' + imageType + ',' + file);
            $('#download').click();
        }

    </script>
    </form>
</body>
</html>

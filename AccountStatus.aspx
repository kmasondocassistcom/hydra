<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="./Controls/MenuBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="docHeader" Src="./Controls/docHeader.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="AccountStatus.aspx.vb" Inherits="docAssistWeb.AccountStatus" %>
<%@ Register TagPrefix="uc1" TagName="TopRight" Src="./Controls/TopRight.ascx" %>
<%@ Register TagPrefix="uc1" TagName="AdminNav" Src="./Controls/AdminNav.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<TITLE>docAssist - Account Status</TITLE>
		<META content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<META content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<META content="JavaScript" name="vs_defaultClientScript">
		<META content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<META http-equiv="cache-control" content="no-cache">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<BODY>
		<FORM id="Form1" method="post" runat="server">
			<TABLE height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD width="100%" colSpan="6">
						<uc1:docHeader id="docHeader" runat="server"></uc1:docHeader></TD>
					<TD width="100%"></TD>
				</TR>
				<TR vAlign="middle" height="100%">
					<TD class="Navigation1" style="BORDER-RIGHT: #cccccc thin solid; WIDTH: 171px" vAlign="top"
						height="100%"><UC1:ADMINNAV id="AdminNav1" runat="server"></UC1:ADMINNAV></TD>
					<td><img src="spacer.gif" height="1" width="25"></td>
					<TD vAlign="top" width="100%"><BR>
						<table cellSpacing="0" cellPadding="0" width="100%" border="0" class="PageContent">
							<TR align="left">
								<td class="Text" align="left" width="100%">
									<asp:Label id="lblStatus" runat="server"></asp:Label>
								</td>
							</TR>
							<TR align="right">
								<td class="Text" align="right" width="100%">
									<asp:Label id="lblError" runat="server" Visible="False" CssClass="ErrorText"></asp:Label>
								</td>
							</TR>
						</table>
					</TD>
					<TD><img src="spacer.gif" height="1" width="25"></TD>
				<tr>
					<td colSpan="5"><uc1:topright id="docFooter" runat="server"></uc1:topright></td>
					<td></td>
				</tr>
			</TABLE>
		</FORM>
	</BODY>
</HTML>

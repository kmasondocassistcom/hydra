Imports System
Imports System.IO
Imports System.Text
Imports System.Web.UI

Namespace ViewState

    Public Class PersistViewStateToFileSystem
        Inherits System.Web.UI.Page
        ' the extension is to protect users from sniffing in on view state via a simple
        ' HTTP request
        Private Const FilePathFormat As String = "~/tmp/{0}.vs.resource"
        Private Const ViewStateHiddenFieldName As String = "__ViewStateGuid"

        ' creates a new instance of a GUID for the current request
        Private pViewStateFilePath As Guid = Guid.NewGuid()

        Public ReadOnly Property ViewStateFilePath() As String
            Get
                Return MapPath([String].Format(FilePathFormat, pViewStateFilePath.ToString()))
            End Get
        End Property

        Protected Overloads Overrides Sub SavePageStateToPersistenceMedium(ByVal viewState As Object)

            Dim los As New LosFormatter
            Dim writer As New StringWriter

            ' save the view state to disk
            los.Serialize(writer, viewState)

            Functions.SaveSessionState(pViewStateFilePath.ToString, writer.ToString)

            ' saves the view state GUID to a hidden field
            Page.RegisterHiddenField(ViewStateHiddenFieldName, pViewStateFilePath.ToString)

        End Sub

        Protected Overloads Overrides Function LoadPageStateFromPersistenceMedium() As Object

            Dim los As New LosFormatter

            Dim vsString As String = Request.Form(ViewStateHiddenFieldName)

            Dim viewStateString As String = Functions.LoadSessionState(vsString)
            Return los.Deserialize(viewStateString)
            
        End Function

    End Class

End Namespace
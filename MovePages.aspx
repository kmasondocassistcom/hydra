<%@ Page Language="vb" AutoEventWireup="false" Codebehind="MovePages.aspx.vb" Inherits="docAssistWeb.MovePages"%>
<%@ Register TagPrefix="uc1" TagName="IndexNavigation" Src="IndexNavigation.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<TITLE>docAssist - Thumbnails</TITLE>
		<META content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<META content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<META content="JavaScript" name="vs_defaultClientScript">
		<META content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
		<style type="text/css">
			#boxes img { padding:3px; }
		</style>
		<script src=scripts/jquery.js></script>
		<script src=scripts/jquery-ui.js></script>
		<script src=scripts/movePages.js></script>
	</HEAD>
	<BODY class="Thumbnails">
		<FORM id="frmImageThumbnail" runat="server">
			<br>
			<table cellSpacing="0" cellPadding="0" width="700" height="500" border="0">
				<tr>
					<td class="PageContent" width="25"></td>
					<td colspan="2" class="PageContent">To change the order of pages drag and drop them 
						to the desired location. <span id="progress"></span></td>
				</tr>
				<tr>
					<td colspan="3" height="10"></td>
				</tr>
				<tr>
					<td width="25"></td>
					<td valign="top" align="left" height="100%">
						<div style="OVERFLOW: auto; WIDTH: 660px; HEIGHT: 500px">
																<div id="boxes">
								</div>
											</div> </td>
			<td width="25"></td>
			</tr>
			<tr>
				<td colspan="3" align="right">
					<input type="hidden" id="NewOrder" runat="server">
					<asp:ImageButton id="btnOk" runat="server" ImageUrl="Images/btn_small_ok.gif"></asp:ImageButton></td>
			</tr>
			<tr><td><img src=spacer.gif height=15 width=1></td></tr>
			</table>
		</FORM>
	</BODY>
</HTML>

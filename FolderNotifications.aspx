<%@ Page Language="vb" AutoEventWireup="false" Codebehind="FolderNotifications.aspx.vb" Inherits="docAssistWeb.FolderNotifications"%>
<%@ Register TagPrefix="uc1" TagName="docHeader" Src="Controls/docHeader.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<TITLE>docAssist</TITLE>
		<META content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<META content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<META content="JavaScript" name="vs_defaultClientScript">
		<META content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<BODY class="Prefs">
		<FORM id="Form1" method="post" runat="server">
			<br>
			<table cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
				<tr>
					<td width="20" rowSpan="6"></td>
					<td class="PrefHeader">
						Folder&nbsp;Notifications
					</td>
				</tr>
				<tr>
					<td align="left"><IMG height="1" src="dummy.gif" width="17">
						<asp:LinkButton id="lnkAdd" runat="server" Font-Size="8pt" Font-Names="Trebuchet MS" ForeColor="#BD0000">Add Notification</asp:LinkButton><IMG height="5" src="dummy.gif" width="1">
					</td>
				</tr>
				<tr>
					<td>
						<table class="Prefs" cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
							<tr>
								<td width="20"><IMG height="1" src="dummy.gif" width="20"></td>
								<td class="Text" noWrap align="left" width="100%"><span class="Text" id="NoRows" runat="server"></span><asp:datagrid id="dgAlerts" runat="server" Font-Names="Trebuchet MS" Font-Size="9pt" AutoGenerateColumns="False"
										Width="100%">
										<AlternatingItemStyle BackColor="WhiteSmoke"></AlternatingItemStyle>
										<HeaderStyle Font-Size="10pt" Font-Bold="True" BackColor="Gainsboro"></HeaderStyle>
										<Columns>
											<asp:TemplateColumn>
												<HeaderStyle Width="30px"></HeaderStyle>
												<ItemStyle Wrap="False"></ItemStyle>
												<ItemTemplate>
													<asp:ImageButton id="btnDelete" runat="server" ImageUrl="Images/trash.gif" CommandName="Del"></asp:ImageButton>
											<asp:Label id="AlertId" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SubscriptionID") %>' Visible="False">
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn DataField="Action" HeaderText="Action">
												<HeaderStyle Wrap="False"></HeaderStyle>
												<ItemStyle Wrap="False"></ItemStyle>
											
											</asp:BoundColumn>
											<asp:BoundColumn DataField="FolderName" HeaderText="Folder Name">
												<HeaderStyle Wrap="False"></HeaderStyle>
												<ItemStyle Wrap="False"></ItemStyle>
											
											</asp:BoundColumn>
											<asp:BoundColumn DataField="FolderPath" HeaderText="Folder Path">
												<HeaderStyle Wrap="False"></HeaderStyle>
												<ItemStyle Wrap="False"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="Details" HeaderText="Details">
												<HeaderStyle Wrap="False"></HeaderStyle>
												<ItemStyle Wrap="False"></ItemStyle>
											</asp:BoundColumn>
										</Columns>
									</asp:datagrid></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</FORM>
		<script language="javascript">
		
			function disableRealTime()
			{
				document.getElementById('rdoRealTime').disabled = true;
				document.getElementById('rdoRealTime').checked = false;
				document.getElementById('rdoDaily').checked = true;
			}

			function enableRealTime()
			{
				document.getElementById('rdoRealTime').disabled = false;
			}
			
			function disableWeekly()
			{
				document.getElementById('Weekly').disabled = true;
			}
		
		</script>
	</BODY>
</HTML>

<%@ Control Language="vb" AutoEventWireup="false" Codebehind="SearchAttributesGrid.ascx.vb" Inherits="docAssistWeb.SearchAttributesGrid" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<asp:DataGrid id="dgSearchAttributes" runat="server" AutoGenerateColumns="False" CssClass="dgSearchAttributes">
	<Columns>
		<asp:TemplateColumn HeaderText="Attribute">
			<EditItemTemplate>
				<asp:Label id=lblAttributeValue runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AttributeName") %>'>
				</asp:Label>
			</EditItemTemplate>
		</asp:TemplateColumn>
		<asp:TemplateColumn HeaderText="Search Value">
			<EditItemTemplate>
				<asp:TextBox id=txtAttributeValue runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AttributeValue") %>' Width="250px">
				</asp:TextBox>
				<asp:Calendar id="calAttributeValue" runat="server" Font-Size="8pt">
					<SelectorStyle Font-Size="8pt"></SelectorStyle>
					<DayHeaderStyle Font-Bold="True" ForeColor="White" BackColor="Black"></DayHeaderStyle>
					<SelectedDayStyle Font-Size="10pt"></SelectedDayStyle>
					<WeekendDayStyle Font-Size="8pt" BackColor="Silver"></WeekendDayStyle>
				</asp:Calendar>
			</EditItemTemplate>
		</asp:TemplateColumn>
		<asp:TemplateColumn HeaderText="DataType"></asp:TemplateColumn>
		<asp:TemplateColumn HeaderText="DataFormat"></asp:TemplateColumn>
	</Columns>
</asp:DataGrid>

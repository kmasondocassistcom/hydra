Partial Class ManageMapAddDetailMap
    Inherits System.Web.UI.Page

    Private mobjUser As Accucentric.docAssist.Web.Security.User
    Private mconSqlImage As SqlClient.SqlConnection

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        txtFolder.Attributes("onmouseover") = "Tip(this.value);"
        txtFolder.Attributes("onkeypress") = "catchSubmit();"
        txtFolderValue.Attributes("onkeypress") = "catchSubmit();"

        If Not Page.IsPostBack Then

            lblCurrent.Text = Session("mAdminIntegrationPublisherName") & " / " & _
            Session("mAdminIntegrationProductName") & " / " & _
            Session("mAdminIntegrationVersion") & " / " & _
            Session("mAdminIntegrationFormName")
            lblDocument.Text = Session("mAdminIntegrationDocumentName")
            PopulateAttributesList()
            PopulateAppFieldsList()

            ddlAppFolder.DataSource = cmbAppFields.DataSource
            ddlAppFolder.DataTextField = cmbAppFields.DataTextField
            ddlAppFolder.DataValueField = cmbAppFields.DataValueField
            ddlAppFolder.DataBind()
            ddlAppFolder.Items.Add(New ListItem("", "Nothing"))
            ddlAppFolder.SelectedValue = "Nothing"

            If Session("mAdminMapTable") = "INTEGRATION" Then
                FolderMappings.Attributes("style") = "DISPLAY: none"
                btnRemoveFolderMapping.Visible = False
                chkInteractive.Visible = True
                rdoAppend.Visible = False
                rdoReplace.Visible = False
            Else
                rdoAppend.Visible = True
                rdoReplace.Visible = True
                chkInteractive.Visible = False
                chkInteractive.Checked = False
            End If

        End If

    End Sub


    Private Sub PopulateAttributesList()

        Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
        Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))

        Me.mobjUser.AccountId = guidAccountId
        Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
        Me.mconSqlImage.Open()

        Dim cmdSql As SqlClient.SqlCommand

        Dim dtsql As New DataTable
        cmdSql = New SqlClient.SqlCommand("acsAdminAttributesByDocumentId", Me.mconSqlImage)
        cmdSql.CommandType = CommandType.StoredProcedure
        cmdSql.Parameters.Add("@DocumentID", Session("mAdminIntegrationDocumentID"))
        Dim dasql As New SqlClient.SqlDataAdapter(cmdSql)
        dasql.Fill(dtsql)

        If dtsql.Rows.Count <= 0 Then
            'TODO: No Cabinets to change
            cmbAttributes.Items.Clear()
        Else
            cmbAttributes.DataTextField = "AttributeName"
            cmbAttributes.DataValueField = "AttributeID"
            cmbAttributes.DataSource = dtsql
            cmbAttributes.DataBind()

        End If

    End Sub



    Private Sub PopulateAppFieldsList()

        Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
        Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))

        Me.mobjUser.AccountId = guidAccountId
        Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
        Me.mconSqlImage.Open()

        Dim cmdSql As SqlClient.SqlCommand

        Dim dtsql As New DataTable
        cmdSql = New SqlClient.SqlCommand("acsAdminIntegrationFields", Me.mconSqlImage)
        cmdSql.CommandType = CommandType.StoredProcedure
        cmdSql.Parameters.Add("@FormID", Session("mAdminIntegrationFormID"))
        Dim dasql As New SqlClient.SqlDataAdapter(cmdSql)
        dasql.Fill(dtsql)

        If dtsql.Rows.Count <= 0 Then
            'TODO: No Cabinets to change
            cmbAppFields.Items.Clear()
        Else
            cmbAppFields.DataTextField = "FieldDesc"
            cmbAppFields.DataValueField = "FieldID"
            cmbAppFields.DataSource = dtsql
            cmbAppFields.DataBind()

        End If

    End Sub

    Private Sub ImageButton2_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)

        If cmbAppFields.SelectedValue = "" Or cmbAttributes.SelectedValue = "" Then Exit Sub
        If MappingExists() = True Then
            lblError.Text = "Form Field/Attribute combination already used in mapping."
            Exit Sub
        End If

        Dim lstNewItem As New ListItem
        lstNewItem.Text = cmbAppFields.SelectedItem.Text & " - " & cmbAttributes.SelectedItem.Text
        lstNewItem.Value = cmbAppFields.SelectedValue & ";" & cmbAttributes.SelectedValue
        lstMappings.Items.Add(lstNewItem)
        lblError.Text = ""
    End Sub

    Private Function MappingExists() As Boolean

        Dim lstItemCheck As ListItem
        For Each lstItemCheck In lstMappings.Items

            If chkInteractive.Checked = False Then
                If lstItemCheck.Value.Split(";")(0) = cmbAppFields.SelectedValue Then
                    Return True
                End If
            Else
                If lstItemCheck.Value = "0" & ";" & cmbAttributes.SelectedValue Then
                    Return True
                End If
            End If

        Next
    End Function


    Private Sub ImageButton1_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)

        If lstMappings.Items.Count <= 0 Then
            lblError.Text = "At least one mapping must be defined to save."
            Exit Sub
        End If

        If Session("mAdminMapTable") = "INTEGRATION" Then
            SaveMappings()
        End If

        If Session("mAdminMapTable") = "CAPTURE" Then
            SaveCaptureMap()
        End If


        Response.Redirect("ManageIntegration.aspx")


    End Sub

    Private Function SaveMappings() As Boolean

        Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
        Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))

        Me.mobjUser.AccountId = guidAccountId
        Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
        Me.mconSqlImage.Open()
        Dim cmdSql As SqlClient.SqlCommand
        Dim dtsql As New DataTable


        cmdSql = New SqlClient.SqlCommand("acsAdminIntegrationMapClear", Me.mconSqlImage)
        cmdSql.CommandType = CommandType.StoredProcedure
        cmdSql.Parameters.Add("@DocumentID", Session("mAdminIntegrationDocumentID"))
        cmdSql.Parameters.Add("@FormID", Session("mAdminIntegrationFormID"))
        cmdSql.ExecuteNonQuery()


        cmdSql = New SqlClient.SqlCommand("acsAdminIntegrationMapAdd", Me.mconSqlImage)
        cmdSql.CommandType = CommandType.StoredProcedure
        cmdSql.Parameters.Add("@DocumentID", Session("mAdminIntegrationDocumentID"))
        cmdSql.Parameters.Add("@FormID", Session("mAdminIntegrationFormID"))
        cmdSql.ExecuteNonQuery()

        SaveMappingDetail()

        Return True

    End Function

    Private Sub SaveMappingDetail()

        Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
        Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))

        Me.mobjUser.AccountId = guidAccountId
        Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
        Me.mconSqlImage.Open()

        Dim cmdSql As SqlClient.SqlCommand
        Dim sArray As String()

        Dim icount As Integer = lstMappings.Items.Count
        Dim x As Integer

        For x = 0 To icount - 1
            sArray = Split(lstMappings.Items(x).Value, ";")

            Dim iSequence As Integer
            If sArray(0) = "0" Then
                iSequence = 0
            Else
                iSequence = x + 1
            End If


            cmdSql = New SqlClient.SqlCommand("acsAdminIntegrationMapDetailAdd", Me.mconSqlImage)
            cmdSql.CommandType = CommandType.StoredProcedure
            cmdSql.Parameters.Add("@FormID", Session("mAdminIntegrationFormID"))
            cmdSql.Parameters.Add("@DocumentID", Session("mAdminIntegrationDocumentID"))
            cmdSql.Parameters.Add("@Sequence", iSequence)
            cmdSql.Parameters.Add("@AttributeID", sArray(1))
            cmdSql.Parameters.Add("@FieldID", sArray(0))
            cmdSql.Parameters.Add("@SearchType", "0")
            If sArray(0) = "0" Then
                cmdSql.Parameters.Add("@Selectable", "1")
            Else
                cmdSql.Parameters.Add("@Selectable", "0")
            End If
            cmdSql.ExecuteNonQuery()

        Next


    End Sub


    Private Function SaveCaptureMap() As Boolean

        Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
        Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))

        Me.mobjUser.AccountId = guidAccountId
        Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
        Me.mconSqlImage.Open()
        Dim cmdSql As SqlClient.SqlCommand
        Dim dtsql As New DataTable


        cmdSql = New SqlClient.SqlCommand("acsAdminCaptureMapClear", Me.mconSqlImage)
        cmdSql.CommandType = CommandType.StoredProcedure
        cmdSql.Parameters.Add("@DocumentID", Session("mAdminIntegrationDocumentID"))
        cmdSql.Parameters.Add("@FormID", Session("mAdminIntegrationFormID"))
        cmdSql.ExecuteNonQuery()


        cmdSql = New SqlClient.SqlCommand("acsAdminCaptureMapAdd", Me.mconSqlImage)
        cmdSql.CommandType = CommandType.StoredProcedure
        cmdSql.Parameters.Add("@DocumentID", Session("mAdminIntegrationDocumentID"))
        cmdSql.Parameters.Add("@FormID", Session("mAdminIntegrationFormID"))
        cmdSql.Parameters.Add("@AttributeCaptureMode", IIf(rdoAppend.Checked, 0, 1))
        cmdSql.ExecuteNonQuery()

        SaveCaptureMapDetail()

        Return True

    End Function


    Private Sub SaveCaptureMapDetail()

        Try

            Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
            Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))

            Me.mobjUser.AccountId = guidAccountId
            Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
            Me.mconSqlImage.Open()

            Dim cmdSql As SqlClient.SqlCommand
            Dim sArray As String()

            Dim icount As Integer = lstMappings.Items.Count
            Dim x As Integer
            Dim iDocumentID As Integer = Session("mAdminIntegrationDocumentID")

            For x = 0 To icount - 1

                sArray = Split(lstMappings.Items(x).Value, ";")


                cmdSql = New SqlClient.SqlCommand("acsAdminCaptureMapDetailAdd", Me.mconSqlImage)
                cmdSql.CommandType = CommandType.StoredProcedure
                cmdSql.Parameters.Add("@FormID", Session("mAdminIntegrationFormID"))
                cmdSql.Parameters.Add("@AttributeID", sArray(1))
                cmdSql.Parameters.Add("@FieldID", sArray(0))
                cmdSql.Parameters.Add("@DocumentID", iDocumentID)
                cmdSql.Parameters.Add("@Sequence", x + 1)
                cmdSql.ExecuteNonQuery()

            Next

            'cmdSql.Parameters.Clear()
            'cmdSql.Dispose()

            If ddlAppFolder.SelectedValue <> "Nothing" Then
                Dim cmdSqlDetail As New SqlClient.SqlCommand("acsAdminCaptureMapDetailAdd", Me.mconSqlImage)
                cmdSqlDetail.CommandType = CommandType.StoredProcedure
                'cmdSqlDetail.Parameters.Add("@AttributeID", 11)
                cmdSqlDetail.Parameters.Add("@FormID", Session("mAdminIntegrationFormID"))
                cmdSqlDetail.Parameters.Add("@FieldID", ddlAppFolder.SelectedValue)
                cmdSqlDetail.Parameters.Add("@DocumentID", iDocumentID)
                cmdSqlDetail.Parameters.Add("@FolderMap", True)
                cmdSqlDetail.Parameters.Add("@Sequence", x + 1)
                cmdSqlDetail.ExecuteNonQuery()
            End If

            For Each listItem As ListItem In lstAppFolder.Items

                cmdSql.Parameters.Clear()
                cmdSql = New SqlClient.SqlCommand("acsAdminCaptureFolderMappingAdd", Me.mconSqlImage)
                cmdSql.CommandType = CommandType.StoredProcedure
                cmdSql.Parameters.Add("@FormID", Session("mAdminIntegrationFormID"))
                cmdSql.Parameters.Add("@FieldID", CInt(listItem.Value.Split("�")(0)))
                cmdSql.Parameters.Add("@DocumentID", iDocumentID)
                cmdSql.Parameters.Add("@CaptureValue", listItem.Value.Split("�")(2))
                cmdSql.Parameters.Add("@FolderID", CInt(listItem.Value.Split("�")(1)))
                cmdSql.ExecuteNonQuery()

            Next

        Catch ex As Exception

            Throw New Exception("An error occured while saving this integration profile.")

        Finally


        End Try

    End Sub

    Private Sub btnNext_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnNext.Click

        If lstMappings.Items.Count <= 0 Then
            lblError.Text = "At least one mapping must be defined to save."
            Exit Sub
        End If

        If Session("mAdminMapTable") = "INTEGRATION" Then
            SaveMappings()
        End If

        If Session("mAdminMapTable") = "CAPTURE" Then
            SaveCaptureMap()
        End If

        Response.Redirect("ManageIntegration.aspx")

    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAdd.Click

        If (cmbAppFields.SelectedValue = "" And chkInteractive.Checked = False) Or cmbAttributes.SelectedValue = "" Then Exit Sub
        If MappingExists() = True Then
            lblError.Text = "Field already used in mapping."
            Exit Sub
        End If

        Dim lstNewItem As New ListItem
        If chkInteractive.Checked = False Then
            lstNewItem.Text = cmbAppFields.SelectedItem.Text & " - " & cmbAttributes.SelectedItem.Text
        Else
            lstNewItem.Text = "[Interactive Selection]" & " - " & cmbAttributes.SelectedItem.Text
        End If


        If Session("mAdminMapTable") = "INTEGRATION" Then
            lstNewItem.Value = cmbAppFields.SelectedValue & ";" & cmbAttributes.SelectedValue & ";" & IIf(chkInteractive.Checked, "1", "0")
            If chkInteractive.Checked = True Then
                lstNewItem.Text = "[Interactive Selection]" & " - " & cmbAttributes.SelectedItem.Text
                lstNewItem.Value = "0" & ";" & cmbAttributes.SelectedValue
            Else
                lstNewItem.Text = cmbAppFields.SelectedItem.Text & " - " & cmbAttributes.SelectedItem.Text
                lstNewItem.Value = cmbAppFields.SelectedValue & ";" & cmbAttributes.SelectedValue
            End If

        Else
            lstNewItem.Value = cmbAppFields.SelectedValue & ";" & cmbAttributes.SelectedValue
            lstNewItem.Text = cmbAppFields.SelectedItem.Text & " - " & cmbAttributes.SelectedItem.Text
        End If


        lstMappings.Items.Add(lstNewItem)
        lblError.Text = ""

    End Sub

    Private Sub btnMoveUp_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnMoveUp.Click
        If lstMappings.Items.Count = 0 Then Exit Sub

        Dim i As Integer = lstMappings.SelectedIndex

        If i > -1 Then
            Dim listItem As System.Web.UI.WebControls.ListItem = lstMappings.SelectedItem
            lstMappings.Items.RemoveAt(i)
            lstMappings.Items.Insert(i - 1, listItem)
            lstMappings.SelectedIndex = i - 1
            lblError.Text = ""
        End If
    End Sub

    Private Sub btnMoveDown_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnMoveDown.Click
        If lstMappings.Items.Count = 0 Then Exit Sub

        Dim i As Integer = lstMappings.SelectedIndex

        If i > -1 Then
            Dim listItem As System.Web.UI.WebControls.ListItem = lstMappings.SelectedItem
            lstMappings.Items.RemoveAt(i)
            lstMappings.Items.Insert(i + 1, listItem)
            lstMappings.SelectedIndex = i + 1
            lblError.Text = ""
        End If
    End Sub

    Private Sub btnRemove_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnRemove.Click

        If lstMappings.Items.Count = 0 Then Exit Sub
        Dim i As Integer = lstMappings.SelectedIndex
        lstMappings.Items.RemoveAt(i)

    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("ManageIntegration.aspx")
    End Sub

    Private Sub chkInteractive_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkInteractive.CheckedChanged

        If chkInteractive.Checked = True Then
            cmbAppFields.Enabled = False
        Else
            cmbAppFields.Enabled = True
        End If

    End Sub

    Private Sub btnAddAppFolder_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddAppFolder.Click
        lstAppFolder.Items.Add(New ListItem(txtFolder.Text & " - " & txtFolderValue.Text, ddlAppFolder.SelectedValue & "�" & FolderId.Value & "�" & txtFolderValue.Text))
    End Sub

    Private Sub btnRemoveFolderMapping_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnRemoveFolderMapping.Click
        lstAppFolder.Items.Remove(lstAppFolder.SelectedItem)
    End Sub

End Class
Imports Accucentric.docAssist.Data.Images

Partial Class FolderNotifications
    Inherits System.Web.UI.Page

    Dim mobjUser As Accucentric.docAssist.Web.Security.User
    Dim mconFolderNotifications As New SqlClient.SqlConnection
    Dim mconSqlCompany As New SqlClient.SqlConnection

    Protected WithEvents txtDaily As eWorld.UI.TimePicker
    Protected WithEvents txtWeekly As eWorld.UI.TimePicker
    Protected WithEvents Radiobutton1 As System.Web.UI.WebControls.RadioButton
    Protected WithEvents Radiobutton2 As System.Web.UI.WebControls.RadioButton

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try
            Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
        Catch ex As Exception
            Response.Redirect("Default.aspx")
            Exit Sub
        End Try

        Try

            If Not Page.IsPostBack Then

                Me.mconFolderNotifications = Functions.BuildConnection(Functions.ConnectionType.Notifications_Folder)
                Me.mconSqlCompany = Functions.BuildConnection(Me.mobjUser)

                Dim objFa As New FolderAlerts(Me.mconSqlCompany, False)
                Dim objWorkflow As New Workflow(Me.mconFolderNotifications, False)

                Dim sqlCmd As New SqlClient.SqlCommand
                Dim strFoldersTable As String = System.Guid.NewGuid().ToString().Replace("-", "")
                Dim sqlDa As New SqlClient.SqlDataAdapter

                Dim sb As New System.Text.StringBuilder

                Dim dtQueues As New DataTable
                dtQueues = objFa.FAFolderNotificationListGet()
                sb = New System.Text.StringBuilder
                sb.Append("CREATE TABLE ##" & strFoldersTable.ToString & " (FolderId int NULL, FolderName varchar(255) NULL, FolderPath varchar(2000) NULL)" & vbCrLf)
                For Each dr As DataRow In dtQueues.Rows
                    sb.Append("INSERT INTO ##" & strFoldersTable & " (FolderId, FolderName, FolderPath) VALUES (" & CStr(dr("FolderId")).Replace("'", "''") & ", '" & CStr(dr("FolderName")).Replace("'", "''").ToString.Replace("'", "") & "', '" & dr("FolderPath").ToString.Replace("'", "") & "')" & vbCrLf)
                Next
                sqlCmd = New SqlClient.SqlCommand(sb.ToString, Me.mconFolderNotifications)
                Dim sqlBld As New SqlClient.SqlCommandBuilder
                sqlCmd.ExecuteNonQuery()

                Dim dt As New DataTable
                Dim fa As New FolderAlerts(Me.mconFolderNotifications)
                dt = fa.FolderGetSubscriberFolderAlertSMTP(Me.mobjUser.UserId.ToString, Me.mobjUser.AccountId.ToString, Me.mobjUser.TimeDiffFromGMT, "##" & strFoldersTable)

                If dt.Rows.Count = 0 Then
                    NoRows.InnerHtml = "No alerts have been defined for your account. Click the Add Notification link to setup a notification."
                    dgAlerts.Visible = False
                Else
                    dgAlerts.Visible = True
                    dgAlerts.DataSource = dt
                    dgAlerts.DataBind()
                End If

            End If

        Catch ex As Exception

        Finally

            If Me.mconFolderNotifications.State <> ConnectionState.Closed Then
                Me.mconFolderNotifications.Close()
                Me.mconFolderNotifications.Dispose()
            End If

            If Me.mconSqlCompany.State <> ConnectionState.Closed Then
                Me.mconSqlCompany.Close()
                Me.mconSqlCompany.Dispose()
            End If

        End Try

    End Sub

    Private Sub dgAlerts_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgAlerts.ItemDataBound

        Try
            Dim lblType As Label = e.Item.FindControl("lblType")
            Dim lblAlertType As Label = e.Item.FindControl("lblAlertType")
            Dim btnDelete As ImageButton = e.Item.FindControl("btnDelete")

            btnDelete.Attributes("onclick") = "return confirm('Are you sure you want to remove this alert?');"

          
        Catch ex As Exception

        End Try


    End Sub


    Private Sub lnkAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkAdd.Click
        Response.Redirect("FolderNotificationsAdd.aspx")
    End Sub

    Private Sub dgAlerts_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgAlerts.ItemCommand
        Try
            Select Case e.CommandName
                Case "Del"

                    Try
                        Me.mconFolderNotifications = Functions.BuildConnection(Functions.ConnectionType.Notifications_Folder)

                        Dim objFa As New FolderAlerts(Me.mconFolderNotifications)

                        Dim lblAlertId As Label = e.Item.FindControl("AlertId")
                        objFa.FolderDeleteAlert(lblAlertId.Text)

                        e.Item.Visible = False

                        Dim intVisibleItems As Integer = 0
                        For Each dgi As DataGridItem In dgAlerts.Items
                            If dgi.Visible Then intVisibleItems += 1
                        Next

                        If intVisibleItems = 0 Then
                            NoRows.InnerHtml = "No alerts have been defined for your account. Click the Add Notification link to setup a notification."
                            dgAlerts.Visible = False
                            NoRows.Visible = True
                        End If

                    Catch ex As Exception

                    Finally

                        If Me.mconSqlCompany.State <> ConnectionState.Closed Then
                            Me.mconSqlCompany.Close()
                            Me.mconSqlCompany.Dispose()
                        End If

                    End Try
            End Select
        Catch ex As Exception

        End Try
    End Sub
End Class
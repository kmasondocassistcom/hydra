
Imports Accucentric.docAssist
Imports Accucentric.docAssist.Data.Images

Public Class SecurityGroups
    Inherits System.Web.UI.Page

    Private mobjUser As Web.Security.User
    Private mconSqlImage As SqlClient.SqlConnection

    'Protected WithEvents dgGroups As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents lblNoGroups As System.Web.UI.WebControls.Label

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try

            If Not Page.IsPostBack Then

                If Request.Cookies("docAssist") Is Nothing Then
                    Response.Redirect("Default.aspx")
                    Exit Sub
                End If

                Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
                Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))

                Me.mobjUser.AccountId = guidAccountId
                Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)

                If Not (Me.mobjUser.SecAdmin = True Or Me.mobjUser.SysAdmin = True) Then
                    'ReturnScripts.Add("alert('Access denied.');window.location.href='Default.aspx';")
                    Exit Sub
                End If

                'Groups
                dgGroups.DataSource = BuildGroups()
                dgGroups.DataBind()



                'If dgGroups.Items.Count = 0 Then
                '    lblNoGroups.Text = "You must add a group before you can add queues."
                '    lblNoGroups.Visible = True
                '    dgGroups.Visible = False
                'Else
                '    lblNoGroups.Visible = False
                '    dgGroups.Visible = True
                'End If

            End If

        Catch ex As Exception
            If ex.Message = "DualLogin" Then
                Response.Cookies("docAssist")("DualLogin") = True
                Server.Transfer("Default.aspx")
            Else
                Response.Redirect("Default.aspx")
                Response.Flush()
                Response.End()
            End If

        Finally

            'ReturnControls.Add(dgGroups)
            'ReturnControls.Add(lblNoGroups)

        End Try

    End Sub

    Private Function BuildGroups() As DataTable

        Try
            Dim objGroups As New Security(Functions.BuildConnection(Me.mobjUser))
            Return objGroups.GroupsGet()
        Catch ex As Exception

        End Try

    End Function

    Private Sub dgGroups_DeleteCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgGroups.DeleteCommand

        Try

            Dim dgRowItem As DataGridItem = e.Item
            Dim intGroupId As Integer = CType(dgRowItem.FindControl("lblGroupId"), Label).Text
            Dim blnAllowDelete As Boolean = CType(dgRowItem.FindControl("lblGroupAllowDelete"), Label).Text

            If blnAllowDelete = True Then

                Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
                Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))

                Dim objGroups As New Security(Functions.BuildConnection(Me.mobjUser))
                objGroups.GroupRemoveById(intGroupId)

                'Groups
                dgGroups.DataSource = BuildGroups()
                dgGroups.DataBind()

            Else

                'ReturnScripts.Add("GroupDeleteError();")

            End If

        Catch ex As Exception

        Finally

            'ReturnControls.Add(dgGroups)

        End Try

    End Sub

    Private Sub dgGroups_ItemDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgGroups.ItemDataBound

        Try
            Dim lnkDeleteGroup As LinkButton = CType(e.Item.FindControl("lnkDeleteGroup"), LinkButton)
            If Not lnkDeleteGroup Is Nothing Then lnkDeleteGroup.Attributes("onclick") = "return confirm('Delete the \'" & e.Item.Cells(1).Text.Replace("'", "\'") & "\' group?');"

            Dim lblGroupId As Label = CType(e.Item.FindControl("lblGroupId"), Label)
            If Not lblGroupId Is Nothing Then
                e.Item.Cells(1).Attributes("onclick") = "EditGroup(" & lblGroupId.Text & ");"
                e.Item.Cells(1).Style("CURSOR") = "hand"
            End If

        Catch ex As Exception

        End Try

    End Sub

    Private Sub dgGroups_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgGroups.SelectedIndexChanged

    End Sub
End Class
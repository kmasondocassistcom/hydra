<%@ Page Language="vb" AutoEventWireup="false" Codebehind="InstallIndex.aspx.vb" Inherits="docAssistWeb.InstallIndex" EnableSessionState="False" enableViewState="True" %>
<%@ Register TagPrefix="uc1" TagName="docHeader" Src="Controls/docHeader.ascx" %>
<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="Controls/MenuBar.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>docAssist - Update Components</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="css/core.css" type="text/css" rel="stylesheet">
		<style type="text/css">
		BODY
			{
				background-repeat: repeat-x;
				background-image: url(images/background.gif);
			}
		</style>
	</HEAD>
	<body class="welcome">
		<form id="Form1" method="post" runat="server">
			<table width="300" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="20">&nbsp;</td>
					<td><img src="images/logoBug.gif" alt="docAssist" width="185" height="89"></td>
				</tr>
			</table>
			<table width="100%">
				<tr>
					<td align="center">
						<table width="700">
							<tr>
								<td colspan="2">
									<h1>docAssist Scanner Components Required</h1>
									<h2>Please follow these 4 easy steps to install the required scanning components.</h2>
									<ol id="step">
										<li>
											<p>Click <a href=Downloads/docAssistIndexer.exe>here</a> to download the 
												required components.</p>
										</li>
										<li>
											<p>Once the file has downloaded you will be prompted with a window similar to this 
												one:<br>
												<br>
												<img src="images/indexerInstall.jpg" alt="Sample Security Warning Dialog">
										</li>
										<li>
											<p>Click "Run" to start the installation. The wizard will guide you through the 
												installation process.</p>
										</li>
										<li>
											<p>Once the installation has completed you must close <b>ALL</b> web browsers.</p>
										</li>
										<li><p>Open a new web browser and continue to the docAssist Scan module.</p></li>
									</ol>
								</td>
							</tr>
							<tr>
								<td width="45"><img src="Images/icon_sets/info.png"></td>
								<td>
									If you experience any difficulty installing these componenents, please contact <a href="Support.aspx">
										technical support</a> or email <a href="mailto:support@docassist.com">support@docassist.com</a>. 
									You can also reach technical support directly at 877.399.1100.
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			</TD></TR></TABLE>

			<!--
			<table width="300" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="20">&nbsp;</td>
					<td><img src="images/logoBug.gif" alt="docAssist" width="185" height="89"></td>
				</tr>
			</table>
			<table width="100%" border="0" align="left" cellpadding="0" cellspacing="0">

				<tr>
					<td id="pagecontent"><table width="100%" border="0" align="left" cellpadding="10" cellspacing="0">
							<tr>
								<td colspan="3" valign="top"><div id="coretext">
										<h1>Install Required docAssist™ Components</h1>
									</div>
								</td>
							</tr>
							<tr>
								<td width="25%" valign="top"><table width="100%" border="0" cellpadding="5" cellspacing="5" class="content">
										<caption colspan="4">
											Step 1 - Install <a name="compatibility" id="compatibility"></a>
										</caption>
										<tr>
											<th width="100%">
												<strong>Click the 'Install Now'&nbsp;button below.</strong>
											</th>
										</tr>
										<tr>
											<td><div align="center"><a href=Downloads/docAssistIndexer.exe><img border=0 src="images/installnow.gif" alt="Install Now" width="120" height="58" id="imgInstall"></a></div>
											</td>
										</tr>
									</table>
									<p>&nbsp;</p>
								</td>
								<td valign="top"><table width="100%" border="0" cellpadding="5" cellspacing="5" class="content">
										<caption colspan="4">
											Step 2 - Click Run <a name="compatibility" id="compatibility"></a>
										</caption>
										<tr>
											<th width="100%">
												<p><strong>Security Warning Dialog and Confirmation- Click "Run" </strong>
												</p>
												<ol>
													<li>
														<div align="left">A window will open asking what you want to do with a file called 
															"docAssistComponents.exe".&nbsp;<strong>Click 'Run'</strong>.</div>
													<li>
														<div align="left">You might see a confirmation message, asking if you're sure you 
															want to run this software. <strong>Click 'Run' again</strong>.</div>
													</li>
												</ol>
											</th>
										</tr>
										<tr>
											<td><div align="center">
													<p align="center"><img src="images/indexerInstall.jpg" alt="Sample Security Warning Dialog"></p>
												</div>
											</td>
										</tr>
									</table>
								</td>
								<td width="25%" valign="top"><table width="100%" height="100%" border="0" cellpadding="5" cellspacing="5" class="content">
										<caption colspan="4">
											Step 3 - Close Your Browser <a name="compatibility" id="compatibility"></a>
										</caption>
										<tr>
											<th width="100%">
												<strong class="price">IMPORTANT - Close All Browser Windows </strong>
											</th>
										</tr>
										<tr>
											<td><ol>
													<li>
													After the installation has finished...
													<li>
														<strong class="price">Close ALL browser windows</strong> and return to 
														https://my.docassist.com to login again.
													</li>
												</ol>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
									<td colspan="3" valign="top"><div align="center">If you encounter any difficulties 
									installing these componenents, please contact technical support:<br>
									<a href="Support.aspx">docAssist Support</a> or 
										email <a href="mailto:support@docassist.com">support@docassist.com</a> or 
									call 877.399.1100.
								</div>
							</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>-->
		</form>
				<script src="scripts/jquery.js" type="text/javascript"></script>
		<script language="javascript">
			$(document).ready(function(){
				$("#step li").each(function (i) {
					i = i+1;
					$(this).addClass("item"+i);
			});
			 
				$("#number li").each(function (i) {
					i = i+1;
					$(this).addClass("item"+i);
			});
			 
				$("#commentlist li").each(function (i) {
					i = i+1;
					$(this).prepend('<span class="commentnumber"> #'+i+'</span>');
			});
			 
			});
		</script>

	</body>
</HTML>

<%@ Page Language="vb" AutoEventWireup="false" Codebehind="UploadFiles.aspx.vb" Inherits="docAssistWeb.UploadFiles"%>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>docAssist - Add Attachments</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
		<base target="_self">
	</HEAD>
	<body style="BACKGROUND-COLOR: transparent" bottomMargin="0" leftMargin="0" topMargin="0"
		rightMargin="0">
		<form class="PageContent" id="Form1" method="post" runat="server">
			<table class="menu" style="BACKGROUND-COLOR: transparent" cellSpacing="2" cellPadding="2"
				width="100%" border="0">
				<tr width="100%">
					<td width="100%"><span style="FONT-SIZE: 8pt; FONT-FAMILY: 'Trebuchet MS'"><A onclick="parent.window.landMode();" href="#">&lt;&lt; 
								Home</A></span></td>
				</tr>
			</table>
			<TABLE id="Table1" cellSpacing="0" cellPadding="0" width="550" border="0"> <!-- Parent grey container -->
				<TR>
					<TD vAlign="top">
						<DIV class="Container">
							<DIV class="north">
								<DIV class="east">
									<DIV class="south">
										<DIV class="west">
											<DIV class="ne">
												<DIV class="se">
													<DIV class="sw">
														<DIV class="nw">
															<TABLE id="tblMain" cellSpacing="0" cellPadding="0" width="550" border="0">
																<TR>
																	<TD noWrap align="left"><SPAN class="Heading">Add Files</SPAN></TD>
																	<td noWrap align="right"><span id="ModeClick" onclick="toggleMode();" style="FONT-SIZE:8pt; CURSOR:hand; COLOR:#bd0000; FONT-FAMILY:'Trebuchet MS'; TEXT-DECORATION:underline">Bulk 
																			upload mode</span></td>
																</TR>
																<tr>
																	<td style="VERTICAL-ALIGN: super" vAlign="top" align="left" colSpan="2"><span class="FolderPath" id="FolderPath" runat="server">[No 
																			Folder Selected]</NO></span> <IMG id="ChooseFolder" style="CURSOR: hand" onclick="FolderLookup()" alt="Choose Folder"
																			src="Images/addToFolder_btn.gif" runat="server"> <IMG id="ClearFolder" style="CURSOR: hand" onclick="document.all.FolderId.value = '';document.all.FolderPath.innerHTML = '[No Folder Selected]';document.getElementById('btnUpdateSession').click();"
																			alt="Clear Folder" src="Images/clear.gif" runat="server">
																		<br>
																		<div id="UploadBoxes" style="OVERFLOW: auto; HEIGHT: 150px">
																			<TABLE id="formBlockInput" cellSpacing="2" cellPadding="0" border="0">
																				<TR class="formHead">
																					<TD><span class="Text">Choose file to upload:</span></TD>
																					<TD><span class="Text">Title:</span></TD>
																					<td><span class="Text">Description:</span></td>
																					<td><span class="Text">Comments:</span></td>
																					<td><span class="Text">Tags:</span></td>
																					<td><A id="AddRow" onclick="addRow(this); return false" href="#"><IMG src="Images/bot23.gif" border="0"></A></td>
																				</TR>
																				<TR vAlign="top">
																					<TD><INPUT id="txtFilename" style="FONT-SIZE: 8pt; FONT-FAMILY: 'Trebuchet MS'" type="file"
																							size="20" name="txtFilename"></TD>
																					<TD><asp:textbox id="txtTitle" runat="server" Font-Names="Trebuchet MS" Font-Size="8pt" Width="100px"></asp:textbox></TD>
																					<td width="15"><asp:textbox id="txtDescription" runat="server" Font-Names="Trebuchet MS" Font-Size="8pt" Width="100px"></asp:textbox></td>
																					<td width="15" style="display:none"><asp:textbox id="txtComments" runat="server" Font-Names="Trebuchet MS" Font-Size="8pt" Width="100px"></asp:textbox></td>
																					<td width="15"><asp:textbox id="txtTags" runat="server" Font-Names="Trebuchet MS" Font-Size="8pt" Width="100px"></asp:textbox></td>
																					<td vAlign="bottom" width="15"><A onclick="remRow(this); return false" href="#"><IMG src="Images/circle_minus.gif" border="0"></A></td>
																				</TR>
																			</TABLE>
																		</div>
																		<span id="MultiUpload" style="DISPLAY: none">
																			<OBJECT id="fileUpload" codeBase="" height="365" width="575" align="left" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000"
																				VIEWASTEXT>
																				<PARAM NAME="_cx" VALUE="15214">
																				<PARAM NAME="_cy" VALUE="9657">
																				<PARAM NAME="FlashVars" VALUE="">
																				<PARAM NAME="Movie" VALUE="flashApplets/FlashFileUpload.swf">
																				<PARAM NAME="Src" VALUE="flashApplets/FlashFileUpload.swf">
																				<PARAM NAME="WMode" VALUE="Transparent">
																				<PARAM NAME="Play" VALUE="0">
																				<PARAM NAME="Loop" VALUE="-1">
																				<PARAM NAME="Quality" VALUE="High">
																				<PARAM NAME="SAlign" VALUE="LT">
																				<PARAM NAME="Menu" VALUE="-1">
																				<PARAM NAME="Base" VALUE="">
																				<PARAM NAME="AllowScriptAccess" VALUE="sameDomain">
																				<PARAM NAME="Scale" VALUE="NoScale">
																				<PARAM NAME="DeviceFont" VALUE="0">
																				<PARAM NAME="EmbedMovie" VALUE="0">
																				<PARAM NAME="BGColor" VALUE="">
																				<PARAM NAME="SWRemote" VALUE="">
																				<PARAM NAME="MovieData" VALUE="">
																				<PARAM NAME="SeamlessTabbing" VALUE="1">
																				<PARAM NAME="Profile" VALUE="0">
																				<PARAM NAME="ProfileAddress" VALUE="">
																				<PARAM NAME="ProfilePort" VALUE="0">
																				<PARAM NAME="AllowNetworking" VALUE="all">
																				<PARAM NAME="AllowFullScreen" VALUE="false">
																				<embed 
            src="flashApplets/FlashFileUpload.swf"                         
            FlashVars='uploadPage=MultipleUpload2.aspx<%=GetFlashVars()%>&completeFunction=UploadComplete()' 
                                    quality="high" wmode="transparent" 
            width="575"             height="365"             name="fileUpload" 
            align="left"             allowScriptAccess="sameDomain"              
                       type="application/x-shockwave-flash"                      
               pluginspage="http://www.macromedia.com/go/getflashplayer" />
																			</OBJECT>
																			<asp:linkbutton id="LinkButton1" onclick="LinkButton1_Click" runat="server"></asp:linkbutton></span>
																		<span id="AdvancedCheck">
																			<asp:checkbox id="chkAdvanced" runat="server" Text="Assign attributes &amp; workflow" CssClass="Text"></asp:checkbox></span>&nbsp;&nbsp;&nbsp;&nbsp;
																		
																		<asp:ScriptManager ID="ScriptManager1" runat="server">
                                                                        </asp:ScriptManager>
                                                                        <telerik:RadProgressManager ID="RadProgressManager" Runat="server" />
                                                                        <telerik:RadProgressArea ID="radProgress" runat="server" Skin="Windows7">
<Localization Uploaded="Uploaded"></Localization>
                                                                        </telerik:RadProgressArea>
																	</td>
																</tr>
																<tr>
																	<td align="right" colSpan="2"><asp:literal id="litEnable" runat="server"></asp:literal><asp:literal id="litRedirect" runat="server"></asp:literal><asp:label id="lblError" runat="server" CssClass="ErrorText"></asp:label>&nbsp;
																		<asp:imagebutton id="SetCancel" runat="server" Width="0" Height="0"></asp:imagebutton>&nbsp;<span id="CancelButton" style="CURSOR: hand" onclick="CancelUpload();"><IMG src="Images/btn_cancelupload.gif"></span>
																		<asp:imagebutton id="btnCancel" runat="server" Visible="False" ImageUrl="Images/smallred_cancel.gif"></asp:imagebutton>&nbsp;
																		<asp:ImageButton id="btnUpdateSession" runat="server" Width="0px" Height="0px"></asp:ImageButton>
																		<asp:imagebutton id="btnSave" runat="server" ImageUrl="Images/smallred_save.gif"></asp:imagebutton></td>
																</tr>
															</TABLE>
														</DIV>
													</DIV>
												</DIV>
											</DIV>
										</DIV>
									</DIV>
								</DIV>
							</DIV>
						</DIV>
					</TD>
				</TR>
			</TABLE>
			</TD></TR></TABLE><input id="FolderId" type="hidden" runat="server" NAME="FolderId">
			<INPUT id="NoFolderAccess" type="hidden" name="Hidden1" runat="server">
		</form>
		<script language="javascript" src="scripts/upload.js"></script>
		<script language="javascript">
		
			parent.killTimer();
			
			document.all.AddRow.click();
			document.all.AddRow.click();
			document.all.AddRow.click();

			document.getElementById('CancelButton').style.visibility = 'hidden';

			function RadProgressManagerOnClientProgressStarted()
			{
				<%= radProgress.ClientID %>.Show();
				window.parent.uploadingInProgress = true;
				window.parent.disableTree();
				document.getElementById('CancelButton').style.visibility = 'visible';
				ModeClick.disabled = true;
				document.getElementById('btnSave').style.display = 'none';
			}

			function CancelUpload()
			{
				if (confirm('The current upload will be aborted. Are you sure?'))
				{
					document.getElementById('SetCancel').click();
					<%= radProgress.ClientID %>.CancelRequest();
					window.parent.enableTree();
				}
			}

			function FolderLookup()
			{
				var val = window.showModalDialog('Tree.aspx?','FolderLookup','resizable:no;status:no;dialogHeight:625px;dialogWidth:250px;scroll:no');
				if (val != undefined)
				{
					varArray = val.split('�');
					document.all.FolderId.value = varArray[0].replace('W','');
					document.all.FolderPath.innerHTML = varArray[1];
					document.getElementById('btnUpdateSession').click();
				}
				
			}
			
			function checkRights()
			{
				var NoFolderAccess = document.getElementById('NoFolderAccess');
				var FolderId = document.getElementById('FolderId');
				
				if (NoFolderAccess.value == 0)
				{
					if ((FolderId.value == '') || (FolderId.value == '0'))
					{
						alert('You must choose a folder before saving a file.');
						event.cancelBubble=true;
						event.returnValue = false;
						//return false;
					}
					else
					{
						return true;
					}
				}
				else
				{
					return true;
				}
			}
			
			function toggleMode()
			{
				if (window.parent.uploadingInProgress) { return true; }
			
				var ModeClick = document.getElementById('ModeClick');
				if (ModeClick.innerText == 'Bulk upload mode')
				{
					ModeClick.innerText = 'Single file mode';
					MultiUpload.style.display = 'block';
					UploadBoxes.style.display = 'none';
					document.getElementById('AdvancedCheck').style.display = 'none';
					document.getElementById('btnSave').style.display = 'none';
				}
				else
				{
					ModeClick.innerText = 'Bulk upload mode';
					MultiUpload.style.display = 'none';
					UploadBoxes.style.display = 'block';
					document.getElementById('AdvancedCheck').style.display = 'block';
					document.getElementById('btnSave').style.display = 'block';
				}
			}
			
			function startPoll(){
			    window.parent.uploadOn();
			    document.all.AdvancedCheck.style.display='none';
			    document.all.UploadBoxes.style.display='none';
			    //<%= RadProgressManager.ClientID %>.startProgressPolling();
			    var status = $find("<%= RadProgressManager.ClientID %>");
			    status.startProgressPolling();
			}
			
		</script>
	</body>
</HTML>

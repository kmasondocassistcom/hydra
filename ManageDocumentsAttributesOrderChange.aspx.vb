Partial Class ManageDocumentsAttributesOrderChange
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region



    Private mobjUser As Accucentric.docAssist.Web.Security.User
    Private mconSqlImage As SqlClient.SqlConnection
    Dim dtAttributesSelected As DataTable

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        'Put user code to initialize the page here
        If Not Page.IsPostBack = True Then
            txtDocumentName.Text = Session("mAdminDocumentName")
            dtAttributesSelected = Session("AttributesSelected")
            lstAttributes.DataTextField = "AttributeName"
            lstAttributes.DataValueField = "AttributeID"
            lstAttributes.DataSource = dtAttributesSelected
            lstAttributes.DataBind()
            'SetAttributesRequired()
        End If


    End Sub

    Private Sub SetAttributesRequired()

        Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
        Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))

        Me.mobjUser.AccountId = guidAccountId
        Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
        Me.mconSqlImage.Open()

        Dim lstItem As System.Web.UI.WebControls.ListItem

        For Each lstItem In lstAttributes.Items

            Dim cmdSql As SqlClient.SqlCommand
            Dim dtsql As New DataTable
            cmdSql = New SqlClient.SqlCommand("acsAdminAttributeRequired", Me.mconSqlImage)
            cmdSql.CommandType = CommandType.StoredProcedure
            cmdSql.Parameters.Add("@DocumentID", Session("mAdminDocumentID"))
            cmdSql.Parameters.Add("@AttributeID", lstItem.Value)
            Dim da As New SqlClient.SqlDataAdapter(cmdSql)
            da.Fill(dtsql)
            If dtsql.Rows.Count > 0 Then
                If dtsql.Rows(0).Item(0) = True Then
                    lstItem.Selected = True
                End If
            End If
            dtsql.Clear()

        Next






    End Sub


    Private Function UpdateDocument(ByVal strDocumentName As String) As Boolean
        Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
        Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))

        Me.mobjUser.AccountId = guidAccountId
        Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
        Me.mconSqlImage.Open()


        Dim cmdSql As SqlClient.SqlCommand
        Dim dtsql As New DataTable
        cmdSql = New SqlClient.SqlCommand("acsAdminDocumentChange", Me.mconSqlImage)
        cmdSql.CommandType = CommandType.StoredProcedure
        cmdSql.Parameters.Add("@DocumentID", Session("mAdminDocumentID"))
        cmdSql.Parameters.Add("@NewDocumentName", strDocumentName)
        cmdSql.ExecuteNonQuery()

    End Function



    Private Function UpdateDocumentFolders() As Boolean
        Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
        Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))

        Me.mobjUser.AccountId = guidAccountId
        Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
        Me.mconSqlImage.Open()

        Dim cmdSql As SqlClient.SqlCommand
        Dim dr As DataRow

        'Clear any existing document locations
        cmdSql = New SqlClient.SqlCommand("acsAdminDocumentLocationsClear", Me.mconSqlImage)
        cmdSql.CommandType = CommandType.StoredProcedure
        cmdSql.Parameters.Add("@DocumentID", Session("mAdminDocumentID"))
        cmdSql.ExecuteNonQuery()


        For Each dr In Session("FoldersSelected").rows

            cmdSql = New SqlClient.SqlCommand("acsAdminDocumentLocationsAdd", Me.mconSqlImage)
            cmdSql.CommandType = CommandType.StoredProcedure
            cmdSql.Parameters.Add("@FolderID", dr("FolderID"))
            cmdSql.Parameters.Add("@DocumentID", Session("mAdminDocumentID"))
            cmdSql.ExecuteNonQuery()

        Next


    End Function


    Private Sub UpdateDocumentAttributes(ByVal iNewDocumentID As Integer)
        Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
        Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))

        Me.mobjUser.AccountId = guidAccountId
        Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
        Me.mconSqlImage.Open()


        Dim cmdSql As SqlClient.SqlCommand

        'Clear any existing document attributes
        cmdSql = New SqlClient.SqlCommand("acsAdminDocumentAttributesClear", Me.mconSqlImage)
        cmdSql.CommandType = CommandType.StoredProcedure
        cmdSql.Parameters.Add("@DocumentID", iNewDocumentID)
        cmdSql.ExecuteNonQuery()

        Dim icount As Integer = lstAttributes.Items.Count
        Dim x As Integer

        For x = 0 To icount - 1
            cmdSql = New SqlClient.SqlCommand("acsAdminDocumentAttributesAdd", Me.mconSqlImage)
            cmdSql.CommandType = CommandType.StoredProcedure
            cmdSql.Parameters.Add("@DocumentID", iNewDocumentID)
            cmdSql.Parameters.Add("@AttributeID", lstAttributes.Items(x).Value)
            cmdSql.Parameters.Add("@AttributeDisplayOrder", x + 1)
            cmdSql.Parameters.Add("@Required", lstAttributes.Items(x).Selected)
            cmdSql.ExecuteNonQuery()
        Next

    End Sub

    Private Function DocumentExists(ByVal strDocumentName As String) As Boolean
        Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
        Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))

        Me.mobjUser.AccountId = guidAccountId
        Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
        Me.mconSqlImage.Open()

        Dim cmdSql As SqlClient.SqlCommand

        Dim dtsql As New DataTable
        cmdSql = New SqlClient.SqlCommand("acsAdminDocumentCheck", Me.mconSqlImage)
        cmdSql.CommandType = CommandType.StoredProcedure
        cmdSql.Parameters.Add("@DocumentName", strDocumentName)
        Dim dasql As New SqlClient.SqlDataAdapter(cmdSql)
        dasql.Fill(dtsql)


        If dtsql.Rows.Count > 0 Then
            Return True
        Else
            Return False

        End If
    End Function

    Private Sub lnkFinish_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub


    Private Sub btnFinish_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFinish.Click
        Session("SelectedAttributes") = AttributesSelected()
        Response.Redirect("ManageDocumentsAttributesRequiredChange.aspx")
    End Sub



    Private Function AttributesSelected() As DataTable
        Dim dtAttributes As New DataTable
        Dim dcID As New DataColumn("AttributeID", GetType(System.Int64), Nothing)
        Dim dcValue As New DataColumn("AttributeName", GetType(System.String), Nothing)
        Dim dcOrder As New DataColumn("DisplayOrder", GetType(System.Int64), Nothing)
        dtAttributes.Columns.Add(dcID)
        dtAttributes.Columns.Add(dcValue)
        dtAttributes.Columns.Add(dcOrder)


        Dim iCount As Integer = lstAttributes.Items.Count
        Dim x


        For x = 0 To iCount - 1

            Dim dr As DataRow
            dr = dtAttributes.NewRow
            dr("AttributeID") = lstAttributes.Items(x).Value
            dr("AttributeName") = lstAttributes.Items(x).Text
            dr("DisplayOrder") = x + 1
            dtAttributes.Rows.Add(dr)


        Next

        Return dtAttributes

    End Function

    Private Sub LinkButton2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub btnMoveUp_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnMoveUp.Click
        If lstAttributes.Items.Count = 0 Then Exit Sub

        Dim i As Integer = lstAttributes.SelectedIndex

        If i > 0 Then
            Dim listItem As System.Web.UI.WebControls.ListItem = lstAttributes.SelectedItem
            lstAttributes.Items.RemoveAt(i)
            lstAttributes.Items.Insert(i - 1, listItem)
            lstAttributes.SelectedIndex = i - 1
            lblError.Text = ""
        End If
    End Sub

    Private Sub btnMoveDown_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnMoveDown.Click
        If lstAttributes.Items.Count = 0 Then Exit Sub

        Dim i As Integer = lstAttributes.SelectedIndex

        If i < lstAttributes.Items.Count - 1 Then
            Dim listItem As System.Web.UI.WebControls.ListItem = lstAttributes.SelectedItem
            lstAttributes.Items.RemoveAt(i)
            lstAttributes.Items.Insert(i + 1, listItem)
            lstAttributes.SelectedIndex = i + 1
            lblError.Text = ""
        End If

    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("ManageDocuments.aspx")

    End Sub
End Class

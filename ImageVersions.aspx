<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ImageVersions.aspx.vb" Inherits="docAssistWeb.ImageVersions"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<TITLE>docAssist - Document History</TITLE>
		<META content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<META content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<META content="JavaScript" name="vs_defaultClientScript">
		<META content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<BODY>
		<FORM id="Form1" method="post" runat="server">
			<TABLE class="Transparent" height="100%" cellSpacing="0" cellPadding="0" width="95%" align="center"
				border="0">
				<TBODY>
					<TR height="2">
						<TD><BR>
							<IMG src="Images/document_history.gif">
						</TD>
					</TR>
					<TR>
						<TD>&nbsp;</TD>
					</TR>
					<TR bgColor="#000000">
						<TD style="COLOR: #ffffff"><B>Current Version</B></TD>
					</TR>
					<TR>
						<TD style="BORDER-RIGHT: #000000 1px solid; BORDER-TOP: #000000 1px solid; BORDER-LEFT: #000000 1px solid; BORDER-BOTTOM: #000000 1px solid">
							<TABLE class="Transparent" cellSpacing="0" cellPadding="1" width="100%" border="0">
								<TBODY>
									<TR>
										<TD style="HEIGHT: 11px">Date Created:</TD>
										<TD style="HEIGHT: 11px"><ASP:LABEL id="lblAddedDateTime" Runat="server"></ASP:LABEL></TD>
									</TR>
									<TR bgColor="#cccccc">
										<TD>Created by User:</TD>
										<TD><ASP:LABEL id="lblAddedUser" Runat="server"></ASP:LABEL></TD>
									</TR>
									<TR>
										<TD id="tdPageCount" runat="server">Page Count:</TD>
										<TD id="tdPageCountValue" runat="server"><ASP:LABEL id="lblPageCount" Runat="server"></ASP:LABEL></TD>
									</TR>
									<TR bgColor="#cccccc">
										<TD id="lastMod" runat="server">Last Modified Date:</TD>
										<TD id="lastModValue" runat="server"><ASP:LABEL id="lblModifiedDateTime" Runat="server"></ASP:LABEL></TD>
									</TR>
									<TR>
										<TD id="lastUser" runat="server">Last Modified User:</TD>
										<TD id="lastUserValue" runat="server"><ASP:LABEL id="lblModifiedUser" Runat="server"></ASP:LABEL></TD>
									</TR>
								</TBODY>
							</TABLE>
						</TD>
					</TR>
					<TR>
						<TD><BR>
						</TD>
					</TR>
					<TR bgColor="#000000">
						<TD style="COLOR: #ffffff"><B>Version History</B></TD>
					</TR>
					<TR>
						<TD><ASP:DATAGRID id="dgVersions" runat="server" WIDTH="100%" BORDERWIDTH="1px" BORDERSTYLE="None"
								GRIDLINES="Vertical" AutoGenerateColumns="False" CSSCLASS="Transparent" CELLPADDING="2" BORDERCOLOR="Black">
								<AlternatingItemStyle BackColor="#CCCCCC"></AlternatingItemStyle>
								<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#D00000"></HeaderStyle>
								<Columns>
									<asp:TemplateColumn HeaderText="Version No.">
										<HeaderStyle HorizontalAlign="Center" Width="75px" BackColor="#999999"></HeaderStyle>
										<ItemStyle HorizontalAlign="Right" VerticalAlign="Top"></ItemStyle>
										<ItemTemplate>
											<ASP:LABEL id=lblVersionNum runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.VersionNum") %>'>
											</ASP:LABEL><IMG HEIGHT="1" ALT="" SRC="images/spacer.gif" WIDTH="8">
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Modified User">
										<HeaderStyle Width="220px" BackColor="#999999"></HeaderStyle>
										<ItemTemplate>
											<ASP:LABEL id=lblHistoryModifiedUser runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ModifiedUserName") %>'>
											</ASP:LABEL>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Modified Date">
										<HeaderStyle Width="144px" BackColor="#999999"></HeaderStyle>
										<ItemTemplate>
											<ASP:LABEL id=lblHistoryModifiedDate runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ModifiedDate") %>'>
											</ASP:LABEL>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Page Count">
										<HeaderStyle HorizontalAlign="Center" Width="75px" BackColor="#999999"></HeaderStyle>
										<ItemStyle HorizontalAlign="Right" VerticalAlign="Top"></ItemStyle>
										<ItemTemplate>
											<ASP:LABEL id=lblHistoryPageCount runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SeqTotal") %>'>
											</ASP:LABEL>
										</ItemTemplate>
									</asp:TemplateColumn>
								</Columns>
							</ASP:DATAGRID><BR>
						</TD>
					</TR>
					<TR id="AuditHeader" bgColor="#000000" runat="server">
						<TD style="COLOR: #ffffff">
							<P><B>Document Access Audit</B></P>
						</TD>
					</TR>
					<TR height="100%">
						<TD vAlign="top" height="150">
							<div style="OVERFLOW: auto; WIDTH: 100%; HEIGHT: 200px"><asp:datagrid id="dgAudit" runat="server" WIDTH="100%" BORDERWIDTH="1px" BORDERSTYLE="Solid" GRIDLINES="Vertical"
									AutoGenerateColumns="False" CELLPADDING="2" BORDERCOLOR="Black" ShowHeader="False" Font-Size="8.25pt" Font-Names="Verdana">
									<AlternatingItemStyle BackColor="#CCCCCC"></AlternatingItemStyle>
									<ItemStyle BorderWidth="0px"></ItemStyle>
									<HeaderStyle Wrap="False" Height="15px"></HeaderStyle>
									<Columns>
										<asp:TemplateColumn HeaderText="Date">
											<ItemStyle VerticalAlign="Top"></ItemStyle>
											<ItemTemplate>
												<asp:ImageButton id="Toggle" runat="server" ImageUrl="Images/plus.gif" CommandName="Drill"></asp:ImageButton>
												<asp:Label id=lblDate runat="server" Visible="True" Text='<%# DataBinder.Eval(Container, "DataItem.RequestDate") %>'>
												</asp:Label><BR>
												<asp:Label id="details" runat="server" Visible="False"></asp:Label>
											</ItemTemplate>
											<EditItemTemplate>
												<asp:TextBox id="TextBox1" runat="server"></asp:TextBox>
											</EditItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="User">
											<ItemStyle VerticalAlign="Top"></ItemStyle>
											<ItemTemplate>
												<asp:Label id=lblWebUserId runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.WebUserId") %>'>
												</asp:Label>
												<asp:Label id="lblUser" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.UserName") %>' Visible="True">
												</asp:Label>
											</ItemTemplate>
											<EditItemTemplate>
												<asp:TextBox runat="server"></asp:TextBox>
											</EditItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="E-Mail Address">
											<ItemStyle VerticalAlign="Top"></ItemStyle>
											<ItemTemplate>
												<asp:Label id="Label1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Emailaddress") %>' Visible="True">
												</asp:Label>
											</ItemTemplate>
											<EditItemTemplate>
												<asp:TextBox runat="server"></asp:TextBox>
											</EditItemTemplate>
										</asp:TemplateColumn>
									</Columns>
								</asp:datagrid></div>
						</TD>
					</TR>
					<tr height="100%">
						<td></td>
					</tr>
					<TR>
						<TD align="right"><asp:imagebutton id="btnOk" runat="server" ImageUrl="Images/button_ok.gif"></asp:imagebutton></TD>
					</TR>
					<TR>
						<TD><IMG height="4" src="spacer.gif" width="1"></TD>
					</TR>
				</TBODY>
			</TABLE>
		</FORM>
	</BODY>
</HTML>

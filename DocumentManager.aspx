<%@ Page Language="vb" AutoEventWireup="false" Codebehind="DocumentManager.aspx.vb" Inherits="docAssistWeb.DocumentManager"%>
<%@ Register TagPrefix="uc1" TagName="docHeader" Src="Controls/docHeader.ascx" %>
<%@ Register TagPrefix="uc1" TagName="TopRight" Src="Controls/TopRight.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>docAssist - Scan</title>
		<META content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<META content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<META content="JavaScript" name="vs_defaultClientScript">
		<META content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<BODY onresize="checkSize();" bottomMargin="0" leftMargin="0" topMargin="0" onload="Init();"
		onunload="ExitCode();" MS_POSITIONING="GridLayout">
		<FORM id="Form1" method="post" runat="server">
			<TABLE height="100%" cellSpacing="0" cellPadding="0" width="100%">
				<TR>
					<TD><uc1:docheader id="docHeader" runat="server"></uc1:docheader></TD>
				</TR>
				<TR>
					<TD height="100%">
						<script language="javascript" src="scripts/embedScan.js"></script>
						<SCRIPT language="JScript">
						
							document.all.LoadMessage.style.display = 'none';
						
							function document.all.manager::ShowStatusDialog(msg)
							{
								startProgressDisplay(msg);
							}

							function document.all.manager::HideStatusDialog()
							{
								stopProgressDisplay();
							}

							function document.all.manager::SaveComplete(a)
							{
								switch(a)
								{
									case "FOLDERLOOKUP":
										var val = window.showModalDialog('Tree.aspx?','FolderLookup','resizable:no;status:no;dialogHeight:625px;dialogWidth:250px;scroll:no');
										if (val != undefined)
										{
											varArray = val.split('�');
											document.all.manager.FolderId = varArray[0].replace('W','');
											document.all.manager.FolderName = varArray[1];
										}
										break;
									case "END":
										alert('An error has occured. Please try again. If the problem continues contact support.');
										document.location.replace("Search.aspx");
										break;
									default:
										var path=document.location.href.substring(0,document.location.href.lastIndexOf("/")+1);
										parent.location.replace('Search.aspx?ref=addpage&loadversion=' + a);
										break;
								}
							}

							function document.all.manager::BrowseAttribute(attributeId)
							{
								browseAttribute(attributeId);
							}
							
						</SCRIPT>
						<input id="uid" type="hidden" name="uid" runat="server"> <input id="aid" type="hidden" name="aid" runat="server">
						<input id="url" type="hidden" name="url" runat="server"> <input id="filenm" type="hidden" name="filenm" runat="server">
						<input id="loadoption" type="hidden" value="0" name="loadoption" runat="server"><input id="VersionId" type="hidden" name="VersionId" runat="server">
						<input id="pagecount" type="hidden" name="pagecount" runat="server"> <input id="purl" type="hidden" value="0" name="purl" runat="server">
						<TABLE id="frameLoading" cellSpacing="0" cellPadding="0" border="1" runat="server" style="Z-INDEX: 99; POSITION: absolute">
							<TR>
								<TD vAlign="top" width="100%" colSpan="3">
									<IFRAME id="iFrameLoading" name="iFrameLoading" src="LoadIndex.htm" frameBorder="no" width="360"
										scrolling="no" height="115" runat="server"></IFRAME>
								</TD>
							</TR>
						</TABLE>
						<script language="javascript">
						
		document.all.docHeader_imgLauncher.src = "./Images/manager_grey.gif";
		document.all.docHeader_managerLink.href = "#";

		stopProgressDisplay();

		function startProgressDisplay(msg)
		{
			document.all.frameLoading.style.display = "block";
			document.all.frameLoading.style.visibility = "visible";
			var nXpos = (document.body.clientWidth - 360) / 2; 
			var nYpos = (document.body.clientHeight - 105) / 2;
			document.all.frameLoading.style.left = nXpos;
			document.all.frameLoading.style.top = nYpos;
			document.all.frameLoading.style.zIndex = 99;
			parent.frames['iFrameLoading'].setMessage(msg);
		}			

		function stopProgressDisplay()
		{
			document.all.frameLoading.style.display = "none";
			document.all.frameLoading.style.visibility = "hidden";
		}
		
		checkSize();

		function Init()
		{
			try
			{
				var loadfile = document.all.filenm.value;

				switch(document.all.loadoption.value)
				{
					case "0":
						//regular load
						document.all.manager.Config(document.all.uid.value);
						break;
					case "1":
						//append page(s)
						document.all.manager.FileName(loadfile);
						break;
					case "2":
						//print driver
						document.all.manager.Print(document.all.purl.value, document.all.uid.value);
						break;
					case "3":
						//index from integration
						document.all.manager.Config(document.all.uid.value);
						document.all.manager.IndexFromIntegration(document.all.filenm.value);
				}
			}								
			catch(errmsg)
			{
				window.location = "InstallIndex.aspx";
			}	
		}
		
		function CheckDirty()
		{
			if (document.all.manager.Isdirty == true)
			{
				event.returnValue = "All your changes will be lost. You must save them before leaving the page.";
			}
		}
		
		function ExitCode()
		{
			document.all.manager.SaveState();
		}
		
		function checkSize()
		{
			try
			{
				var myWidth = 0, myHeight = 0;
				myWidth = document.body.clientWidth;
				myHeight = document.body.clientHeight;
				
				var newHeight = document.body.clientHeight - 110;
				var newWidth = document.body.clientWidth;
				
				if (newHeight < 490) { newHeight = 490; }
				if (newWidth < 880) { newWidth = 880; }
				
				document.all.manager.style.position = 'absolute';
				document.all.manager.style.top = 90;
				document.all.manager.style.left = 0;
				document.all.manager.style.height = newHeight;
				document.all.manager.style.width = newWidth;

			}
			catch(ex)
			{
					
			}
		}
		
						function browseAttribute(attributeId)
						{
							var d = new Date();
							var random = d.getHours()+d.getMinutes()+d.getSeconds()+d.getMilliseconds();
							var folderId = document.all.manager.FolderId;
							if (folderId == '') { folderId = 0; }
							if (folderId == undefined) { folderId = 0; }
							var documentId = document.all.manager.DocumentId;
							var ret = window.showModalDialog('ListLookup.aspx?Id='+attributeId+'&DocumentId='+documentId+'&FolderID='+folderId+'&'+random,'SharePermissions','resizable:yes;status:no;dialogHeight:550px;dialogWidth:450px;scroll:no');
							
							if (ret != undefined)
							{ 
								document.all.manager.ListAttributeValue = ret.split('�')[0];
							}
						}
		
		
						</script>
					</TD>
				</TR>
				<tr>
					<td><uc1:topright id="docFooter" runat="server"></uc1:topright></td>
				</tr>
			</TABLE>
		</FORM>
	</BODY>
</HTML>

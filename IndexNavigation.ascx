<%@ Control Language="vb" AutoEventWireup="false" Codebehind="IndexNavigation.ascx.vb" Inherits="docAssistWeb.IndexNavigation" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<span class="badge" id="badge" style="DISPLAY:none" onclick="Notes();"></span>
<TABLE class="Navigation1" cellSpacing="0" cellPadding="0" width="100%" border="0">
	<tr class="hand">
		<td style="DISPLAY: none" vAlign="middle"><IMG id="imgNavigation" src="images/navigation.gif" border="0"></td>
		<TD class="PageContent" id="BackCell" style="DISPLAY: none" vAlign="middle" noWrap runat="server"><asp:imagebutton id="btnBack" runat="server" ImageUrl="Images/dummyback.bmp"></asp:imagebutton></TD>
		<TD id="tdDocumentText" vAlign="middle" runat="server"><A id="A2" href="#" runat="server"><IMG id="ShowText" alt="View Document Text" src="Images/fulltext.gif" border="0" runat="server"></A></TD>
		<TD id="tdDocumentHistory" vAlign="middle" runat="server"><A onclick="javascript:docHistory();" href="#"><IMG id="imgHistory" alt="View Document History" src="Images/doc_hist.gif" border="0"
					runat="server"><!-- this was img1 --></A></TD>
		<TD vAlign="middle"><IMG id="Notes" onclick="Notes();" height="22" alt="View Document Notes" src="Images/doc_notes.gif"
				width="25" border="0" runat="server"></TD>
		<TD id="tdSpace1" vAlign="middle" runat="server"><IMG id="NextPrevSpacer" style="BACKGROUND-COLOR: gray" src="images/spacer.gif" width="2"
				runat="server"></TD>
		<TD vAlign="middle"><ASP:IMAGEBUTTON id="btnPrevious" runat="server" ImageUrl="Images/results_previous.gif" ToolTip="Previous Result"></ASP:IMAGEBUTTON></TD>
		<TD vAlign="middle"><ASP:IMAGEBUTTON id="btnNext" runat="server" ImageUrl="Images/results_next.gif" ToolTip="Next Result"></ASP:IMAGEBUTTON></TD>
		<TD id="tdSpace2" vAlign="middle" runat="server"><IMG style="BACKGROUND-COLOR: gray" src="images/spacer.gif" width="2"></TD>
		<TD id="tdFirst" vAlign="middle" runat="server"><IMG id="imgTop" onclick="Top();" alt="First" src="images/bot25off.gif"></TD>
		<TD id="tdPrevious" vAlign="middle" runat="server"><IMG id="imgPrevious" onclick="Previous();" alt="Previous" src="images/bot02off.gif"></TD>
		<TD id="tdNext" vAlign="middle" runat="server"><IMG id="imgNext" onclick="Next();" alt="Next" src="images\bot03off.gif" border="0"></TD>
		<TD id="tdLast" vAlign="middle" runat="server"><IMG id="imgBottom" onclick="Bottom();" alt="Last" src="images\bot26off.gif" border="0"></TD>
		<TD vAlign="middle"><IMG style="BACKGROUND-COLOR: gray" src="images/spacer.gif" width="2"></TD>
		<TD id="tdZoom" vAlign="middle" runat="server"><IMG class="hand" id="imgZoom" onclick="zoomSelection();" alt="Zoom Window" src="images\bot06.gif"
				border="0"></TD>
		<TD id="tdZoomIn" vAlign="middle" runat="server"><IMG class="hand" id="imgZoomIn" onclick="zoomIn();" alt="Zoom In" src="images\bot07.gif"
				border="0"></TD>
		<TD id="tdZoomOut" vAlign="middle" runat="server"><IMG class="hand" id="imgZoomOut" onclick="zoomOut();" alt="Zoom Out" src="images\bot08.gif"
				border="0"></TD>
		<TD vAlign="middle"><A onmouseover="hover(this);" onclick="ZoomMode(4);" onmouseout="nohover(this);"></A></TD>
		<TD id="tdHand" vAlign="middle" runat="server"><IMG class="hand" id="imgHand" onclick="pan();" alt="Pan" src="images\bot09.gif" border="0"></TD>
		<TD id="tdSpace3" vAlign="middle" runat="server"><IMG style="BACKGROUND-COLOR: gray" src="images/spacer.gif" width="2"></TD>
		<TD id="tdFitBest" vAlign="middle" runat="server"><IMG id="imgFitBest" onclick="bestFit();" alt="Fit Best" src="Images/bot11.gif" border="0"></TD>
		<TD id="tdFitWidth" vAlign="middle" runat="server"><IMG id="imgFitWidth" onclick="fitWidth();" alt="Fit Width" src="images/bot12.gif" border="0"></TD>
		<TD id="tdFitHeight" vAlign="middle" runat="server"><IMG id="imgFitHeight" onclick="fitHeight();" alt="Fit Height" src="images/bot13.gif"
				border="0"></TD>
		<TD id="tdPrint" style="WIDTH: 6px" vAlign="middle" runat="server"><IMG id="imgPrint" onclick="Print();" alt="Print" src="images\bot18off.gif" border="0"
				runat="server"></TD>
		<TD id="tdMail" vAlign="middle" runat="server"><IMG id="imgMail" onclick="eMail();" alt="E-Mail/Download" src="images\bot19off.gif"
				runat="server"></TD>
		<TD id="tdAttach" style="DISPLAY: none" vAlign="middle" runat="server"><A id="lnkAttachments" onmouseover="hover(this);" onclick="Attachments();" onmouseout="nohover(this);"
				runat="server"><IMG id="imgAttachments" height="22" alt="File Attachments" src="Images/paperClip.gif"
					width="25" runat="server"></A></TD>
		<TD id="tdAttachmentDelete" vAlign="middle" noWrap runat="server"><asp:imagebutton id="AttachmentDelete" runat="server" ImageUrl="Images/delete.gif" ToolTip="Delete File(s)"></asp:imagebutton></TD>
		<td id="tdShare" vAlign="middle" runat="server"><IMG src="images/buttons/share.gif" alt="Share Document" onclick="shareDocument();"></td>
		<td style="display:none"><a href="#" onclick="clickMode();">Magic</a></td>
		<TD id="tdPagesVersion" vAlign="middle" noWrap runat="server">&nbsp;
			<asp:label id="Label1" runat="server" Font-Names="Verdana" Font-Size="7pt">Version:</asp:label><ASP:DROPDOWNLIST id="ddlVersions" runat="server" Font-Size="7" Font-Name="Verdana" Enabled="False"></ASP:DROPDOWNLIST>&nbsp;
			<asp:label id="Label2" runat="server" Font-Names="Verdana" Font-Size="7pt">Page:</asp:label><INPUT onkeypress="onEnter();return numericOnly();" id="txtPageNo" style="TEXT-ALIGN: right; WIDTH: 30px; FONT-FAMILY: Verdana; HEIGHT: 17px; FONT-SIZE: 7pt"
				maxLength="3" onchange="onEnter();return false;" size="1" value="1" runat="server">
			<asp:label id="lblPagesOf" runat="server" Font-Names="Verdana" Font-Size="7pt"></asp:label><INPUT style="WIDTH: 27px; DISPLAY: none; HEIGHT: 15px" size="1">&nbsp;
			<asp:label id="Label3" runat="server" Font-Names="Verdana" Font-Size="7pt">Quality:</asp:label><ASP:DROPDOWNLIST id="ddlQuality" runat="server" Font-Size="7" Font-Name="Verdana"></ASP:DROPDOWNLIST></TD>
		<TD width="100%"><IMG height="1" src="images/spacer.gif" width="100%"></TD>
		<TD id="SaveCell" vAlign="middle" align="right" runat="server"><A onclick="trackAnnotations();" href="#" runat="server"><IMG id="imgSave" onmouseover="document.frames['frameAttributes'].removeFocus()" onclick="clickUpload()"
					alt="Save" src="Images/save_small.off.gif" border="0" runat="server"></A>
			<IMG height="1" src="spacer.gif" width="3"></TD>
	</tr>
	<tr>
		<td><IMG height="3" src="spacer.gif" width="1"></td>
	</tr>
</TABLE>
<script src="scripts/jquery.js"></script>
<SCRIPT language="javascript">
		var badge = 0;
		 
		$(document).ready(function() {
			if (badge != 0){
				var pos = $("#navIndex_Notes").position();
				$("#badge").html(badge).css('position','absolute').css('top', 1).css('left', pos.left + 15).show();
			}
		});

		var blnReload = 0;

		function eMail()
		{
			TrackAnnoationsNoImageLoad();
			document.body.style.cursor = "wait";
			var d = new Date();
			var random = d.getHours()+d.getMinutes()+d.getSeconds()+d.getMilliseconds();
			var settings = "resizable:yes;status:no;dialogHeight:535px;dialogWidth:830px;scroll:no";
			window.showModalDialog('Mail.aspx?ID='+random,'Mail',settings);
			document.body.style.cursor = "deafult";
		}

		function Attachments()
		{
			var d = new Date();
			var random = d.getHours()+d.getMinutes()+d.getSeconds()+d.getMilliseconds();
			var settings = "resizable:no;status:no;dialogHeight:705px;dialogWidth:956px;scroll:no";
			//window.showModalDialog('FileAttachments.aspx?ID='+random,'Mail',settings);
			window.open('FileAttachments.aspx?ID='+random,"Attachments", "height=670,resizable=no,scrollbars=no,status=no,toolbar=no,width=956");
		}
		
		function LaunchManager()
		{
			var file = document.all.manager.AddPage();
			if (file != "")
			{
				location.replace(location.href.replace("Index.aspx", "DocumentManager.aspx?File=" + file + "&ID=" + document.all.navIndex_VersionId.value + "&TPC=" + PageCount()));
			}
		}

		function numericOnly(evt)
		{
			evt = (evt) ? evt : event;
			var charCode = (evt.charCode) ? evt.charCode : ((evt.keyCode) ? evt.keyCode
			: ((evt.which) ? evt.which : 0));
			if (charCode > 31 && (charCode < 48 || charCode >57)) {return false;}
			return true;
		}

		function onEnter()
		{
			if(event.keyCode==13)
			{
				GoToPage(Form1.navIndex_txtPageNo.value);
				event.cancelBubble = true;
				event.returnValue = false;
			}
			
			if(event.keyCode==0)
			{
				GoToPage(Form1.navIndex_txtPageNo.value);
				event.cancelBubble = true;
				event.returnValue = false;
			}
		}

		function saveCookie(name,value,days) 
		{ 
			if (!days) expires = "" 
			else{ 
				var date = new Date(); 
				date.setTime(date.getTime()+(days*24*60*60*1000)); 
				var expires = "; expires="+ date.toGMTString() 
				} 
			document.cookie=name+"="+ 
			escape(value)+expires+"; path=/" 
		} 
		
		function clearCookie(name) 
		{
			if (!1) expires = "" 
			else{ 
				var date = new Date(); 
				date.setTime(date.getTime()+ 
				(1*24*60*60*1000)) 
				var expires = "; expires="+ 
				date.toGMTString() 
				} 
			document.cookie=name+"="+ 
			escape("")+expires+"; path=/" 
		}
		
		function readCookie(name) 
		{ 
			name+="=" 
			var ca = document.cookie.split(';') 
			for(var i in ca) 
				{ 
				var c = ca[i]; 
				while (c.charAt(0)==' ') 
				c = c.substring(1,c.length) 
				if (c.indexOf(name) == 0) 
				return unescape(c.substring(name.length,c.length)) 
				} 
			return '' 
		} 				

		function back()
		{
			window.history.back();
		}
		
		function docHistory()
		{
			var url;
			window.open("ImageVersions.aspx","Text", "height=550,resizable=no,scrollbars=no,status=no,toolbar=no,width=600");
			return false;
		}
		
		function Notes()
		{
			var url;
			window.open("DocumentNotes.aspx","DocumentNotes", "height=600,resizable=no,scrollbars=no,status=no,toolbar=no,width=540");
			return false;
		}

		function postbackCount()
		{
			var count = Number(readCookie("PostbackCount"));
			saveCookie("PostbackCount",count+1,1);
		}

		function saveCookie(name,value,days) 
		{ 
			if (!days) expires = "" 
			else{ 
				var date = new Date(); 
				date.setTime(date.getTime()+ 
				(days*24*60*60*1000)) 
				var expires = "; expires="+ 
				date.toGMTString() 
				} 
			document.cookie=name+"="+ 
			escape(value)+expires+"; path=/" 
		} 
		
		function clearCookie(name) 
		{
			if (!1) expires = "" 
			else{ 
				var date = new Date(); 
				date.setTime(date.getTime()+ 
				(1*24*60*60*1000)) 
				var expires = "; expires="+ 
				date.toGMTString() 
				} 
			document.cookie=name+"="+ 
			escape("")+expires+"; path=/" 
		}
		
		function readCookie(name) 
		{ 
			name+="=" 
			var ca = document.cookie.split(';') 
			for(var i in ca) 
				{ 
				var c = ca[i]; 
				while (c.charAt(0)==' ') 
				c = c.substring(1,c.length) 
				if (c.indexOf(name) == 0) 
				return unescape(c.substring(name.length,c.length)) 
				} 
			return '' 
		}
		
		function clickUpload()
		{
			parent.clickUpload();
		} 				
		
		function Back()
		{
			saveCookie("results","1",1);
			window.history.back();
		}
		
		function back()
		{
			document.all.btnBack.click();
		}

</SCRIPT>
<script language="javascript" src="IndexNavigation.js"></script>
<div id="hiddenItems" style="DISPLAY: none"><ASP:LITERAL id="litReload" runat="server"></ASP:LITERAL><asp:literal id="litPostbackCount" runat="server"></asp:literal><asp:literal id="litBack" runat="server"></asp:literal><INPUT id="VersionId" type="hidden" name="VersionId" runat="server">
	<INPUT id="PostbackCount" style="DISPLAY: none" readOnly value="1" name="PostbackCount"
		runat="server"> <INPUT id="uid" type="hidden" name="uid" runat="server"> <INPUT id="aid" type="hidden" name="aid" runat="server">
	<INPUT id="url" type="hidden" name="url" runat="server"> <input id="Annotation" type="hidden" name="Annotation" runat="server">
	<input id="Redaction" type="hidden" name="Redaction" runat="server"><asp:textbox id="txtWorkFlow" runat="server" Width="1" Height="1" Visible="true"></asp:textbox>
	<asp:Literal id="litBadge" runat="server"></asp:Literal>
	<asp:Literal id="litPageCount" runat="server"></asp:Literal>
	<asp:Literal id="litLatestVID" runat="server"></asp:Literal></div>

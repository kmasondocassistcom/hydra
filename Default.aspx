<%@ Page Language="vb" validateRequest="false" AutoEventWireup="True" Codebehind="Default.aspx.vb" Inherits="docAssistWeb._Default" EnableSessionState="True" enableViewState="True" smartNavigation="False"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD runat=server>
		<TITLE>docAssist - creating a paperless world.</TITLE>
		<meta name="robots" content="noindex,nofollow">
		<META content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<META content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<META content="JavaScript" name="vs_defaultClientScript">
		<META content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="css/login.css" type="text/css" rel="stylesheet">
		<LINK href="favicon.ico" rel="shortcut icon">
		<style type="text/css">BODY { BACKGROUND-IMAGE: url(../images/spacer.gif); MARGIN:0px; }
	.padImage { MARGIN: 0px 0px 0px 10px }
	.style1 { COLOR: #999999 }
	#searchbox { BORDER-BOTTOM: #cccccc 2px solid; BORDER-LEFT: #cccccc 2px solid; BORDER-TOP: #cccccc 2px solid; BORDER-RIGHT: #cccccc 2px solid }
	.style3 { FONT-SIZE: 12px; FONT-WEIGHT: bold }
	.style4 { FONT-SIZE: 12px }
		</style>
	</HEAD>
	<BODY class="welcome" onload="FocusCheck();checkCookies();GetTimeDiff();">
		<FORM id="Form1" onsubmit="disable();" method="post" runat="server">

			<table cellSpacing="0" cellPadding="0" align="center">
				<tr>
					<td colSpan="3"><IMG src="images/newbanner.png" id="imgLogin" runat="server"></td>
				</tr>
				<tr>
					<td vAlign="top" align="left" width=50></td>
					<td vAlign="top" align="left" width="547" style="padding-left:99px;">
						<table cellSpacing="5" cellPadding="0" width="100%">
							<tr>
								<td vAlign="top" colspan="3">
									<IMG height="17" src="images/login-customer-label.gif" width="130">
								</td>
							</tr>
							<tr>
								<td width="55"></td>
								<td>
									<div align="right"><IMG id="imgEmail" height="12" src="images/login-email-label.gif" width="39" runat="server"></div>
								</td>
								<td><ASP:TEXTBOX id="txtEMailAddress" tabIndex="1" runat="server" Width="200px" MaxLength="100" Font-Names="Verdana"
										Font-Size="8pt" BorderStyle="Solid" BorderWidth="1px" BorderColor="DarkGray"></ASP:TEXTBOX>
									<DIV align="left"><asp:label id="lblWelcome" runat="server" Width="300px" Font-Names="Verdana" Font-Size="8.25pt"
											Visible="False"></asp:label></DIV>
								</td>
								<td vAlign="top" align="left" rowSpan="4">
									<P><A class="loginLink" href="RequestPwd.aspx">forgot your password?</A></P>
									<P><A class="loginLink" id="supportLink" href="Support.aspx" runat="server">technical 
											support</A></P>
									<img src="dummy.gif" height="1" width="149">
								</td>
							</tr>
							<tr>
								<td></td>
								<td>
									<div align="right"><IMG id="imgPass" height="13" src="images/login-pass-label.gif" width="61" runat="server"></div>
								</td>
								<td><ASP:TEXTBOX id="txtPassword" tabIndex="2" runat="server" Width="200px" MaxLength="500" Font-Names="Verdana"
										Font-Size="8pt" BorderStyle="Solid" BorderWidth="1px" BorderColor="DarkGray" EnableViewState="False"
										TextMode="Password">&lt;/td&gt;
							&lt;/tr&gt;
							</ASP:TEXTBOX>
							<tr>
								<td colspan="3" align="right">
									<ASP:LABEL id="lblErrorMessage" runat="server" Font-Size="9pt" Font-Names="Verdana" Visible="False"
										ForeColor="#BD0000" Font-Bold="True"></ASP:LABEL></td>
								<td></td>
							</tr>
							<tr>
								<td></td>
								<td align="right" colSpan="2"><ASP:CHECKBOX id="chkRemember" tabIndex="3" Runat="server" Width="200px" TEXT="Keep me signed in."
										CssClass="welcome"></ASP:CHECKBOX>&nbsp;
									<BR>
									<ASP:IMAGEBUTTON id="btnSignIn" tabIndex="4" runat="server" ImageUrl="Images/btn_sign_in.gif"></ASP:IMAGEBUTTON></td>
							</tr>
						</table>
					</td>
					<td vAlign="top" width="187"><a href="http://www.docassist.com"><IMG style="padding-left:20px;" height="173" src="images/not-a-customer.jpg" width="178" border="0" id="imgNotCust"
								runat="server"></a></td>
				</tr>

				<tr class="welcome">

					<td style="FONT-SIZE: 7pt" vAlign="top" align="center" colSpan="3" id="footrLinks" runat="server"><a href="http://www.docassist.com/about-docAssist.asp">home</a>
						| <a href="http://www.docassist.com/about-docAssist.asp">about us</a> | <a href="http://www.docassist.com/ecm-solutions/document-management.asp">
							solutions</a> | <a href="http://www.docassist.com/docassist-modules/cloud-platform.asp">
							modules</a> | <a href="http://www.docassist.com/contact-us.asp">contact us</a>
						<br>
						� copyright docAssist 2010</td>
				</tr>
			</table>
			<div style="DISPLAY: none"><BR>
				<asp:dropdownlist id="ddlAccounts" runat="server" Width="216px" Font-Names="Verdana" Font-Size="8.25pt"
					Visible="False"></asp:dropdownlist><INPUT id="ObjectCheck" type="hidden" name="ObjectCheck" runat="server"><INPUT id="txtTimeDiff" type="hidden" name="ObjectCheck" runat="server">
				<asp:imagebutton id="btnSignOut" runat="server" Width="0px" Height="0px"></asp:imagebutton><asp:literal id="litMonitor" runat="server"></asp:literal>
				<ASP:LITERAL id="litRedirect" Runat="server"></ASP:LITERAL>
			</div>
		</FORM>
		<SCRIPT language="javascript">
	if (document.getElementById('txtPassword')) { document.getElementById('txtPassword').focus(); }
	var submitted = false
	function GetTimeDiff(){var now = new Date();document.all.txtTimeDiff.value = now.getTimezoneOffset()}
	function FocusCheck()
	{
		if (document.all.txtEMailAddress != undefined)
		{
			if (document.all.txtEMailAddress.value == "")
			{
				document.all.txtEMailAddress.focus();
			} else
			{
				document.all.txtPassword.focus();
			}
		} 
	}
	function disable()
	{
		if(submitted == false)
		{
		submitted = true;
		
		document.body.style.cursor = "wait";
		}
		else
		{
		document.all.imgSend.disabled = true;
		}
	}
	function checkCookies()
	{
		document.cookie = 'test'; 
		if (document.cookie.indexOf('test') > -1)
		{ 
			var enabled = true;
		}
		else
		{ 
			document.location = "../ErrorMsg.aspx?Id=04gLk6OK5gJvuU5a7Z9SBrYyoaEX0El2EKxFJA1/ZBpjeiJs34cqw+cZkroHpPsSEKBtiVPu7qFr1O9S/5zj6g5At6ei425Ku8mjAaUVVaN3SAs7LWrMnygY3OKhrzjNS7nv1rVW/lPTVFsEhZaTiHUn7zbNlBIeQ3nC31Q6g3BsqV7VOQkWEerm/3I9GZl7";
		}
	}
	function Logout()
	{
		document.all.btnSignOut.click();
	}
	
		</SCRIPT>
	</BODY>
</HTML>

<%--<%@ Register TagPrefix="uc1" TagName="IndexNavigation" Src="IndexNavigation.ascx" %>
<%@ Register TagPrefix="uc1" TagName="AnnotationToolbar" Src="Controls/AnnotationToolbar.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="Atalasoft.Imaging.WebControls" Assembly="Atalasoft.dotImage.WebControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="NewIndex.aspx.vb" Inherits="docAssistWeb.NewIndex" EnableSessionState="True" enableViewState="True"%>
<%@ Register TagPrefix="uc1" TagName="docHeader" Src="Controls/docHeader.ascx" %>
<%@ Register TagPrefix="uc1" TagName="TopRight" Src="Controls/TopRight.ascx" %>
<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="Controls/MenuBar.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<TITLE>docAssist</TITLE>
		<META content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<META content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<META content="JavaScript" name="vs_defaultClientScript">
		<META content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<BODY id="page_body" runat="server">
		<FORM id="Form1" method="post" runat="server">
			<DIV id="svc" style="BEHAVIOR: url(webservice.htc)" onresult="onWSresult()"></DIV>
			<DIV id="svcRotate" style="BEHAVIOR: url(webservice.htc)" onresult="rotateComplete()"></DIV>
			<table border="0" width="100%" height="100%" cellpadding="0" cellspacing="0">
				<tr>
					<td>
						<UC1:DOCHEADER id="docHeader" runat="server"></UC1:DOCHEADER>
					</td>
				</tr>
				<tr>
					<td>
						<UC1:INDEXNAVIGATION id="navIndex" runat="server"></UC1:INDEXNAVIGATION>
					</td>
				</tr>
				<tr>
					<td height="100%">
						<table border="0" width="100%" height="100%" cellpadding="0" cellspacing="0">
							<tr>
								<td><img src="spacer.gif" height="1" width="3"></td>
								<td rowspan="2" id="tdThumbs">
									<TABLE id="tblThumbnails" borderColor="#cccccc" height="100%" cellSpacing="0" cellPadding="0"
										width="200" border="1" runat="server">
										<TR height="14">
											<TD style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: black; FONT-FAMILY: Arial" align="left"
												bgColor="#cccccc"><IMG src="Images/bar_horiz_thumbnails.gif"></TD>
											<td align="right" bgColor="#cccccc"><A href="#"><IMG onclick="HideThumbs();" src="Images/collapse.gif" border="0"></A></td>
										</TR>
										<tr>
											<td width="100%" colSpan="2" height="100%">
												<iframe id="iFrameThumbnails" style="BORDER-RIGHT: thin; PADDING-RIGHT: 0px; BORDER-TOP: thin; PADDING-LEFT: 0px; VISIBILITY: visible; PADDING-BOTTOM: 0px; BORDER-LEFT: thin; PADDING-TOP: 0px; BORDER-BOTTOM: thin"
													src="dummy.htm" frameBorder="no" width="100%" scrolling="auto" height="100%"></iframe>
											</td>
										</tr>
									</TABLE>
								</td>
								<td rowspan="2"><img src="spacer.gif" height="1" width="3"></td>
								<td rowspan="2" width="100%" vAlign="top"><cc1:webimageviewer id="objViewer" runat="server" height="100%" width="100%" BorderColor="Black" BorderWidth="1px"
										BackColor="#CCCCCC"></cc1:webimageviewer></td>
								<td rowspan="2"><img src="spacer.gif" height="1" width="3"></td>
								<td rowspan="2" id="tdAnnotations" valign="top">
									<uc1:annotationtoolbar id="objAnnotation" runat="server"></uc1:annotationtoolbar>
								</td>
								<td rowspan="2"><img src="spacer.gif" height="1" width="3"></td>
								<td rowspan="2" id="tdAttributes">
									<TABLE id="tblAttributes" width="270" borderColor="#cccccc" height="100%" cellSpacing="0"
										cellPadding="0" border="1">
										<TR height="14">
											<TD bgColor="#cccccc"><IMG src="Images/nav_doc_attributes.gif"></TD>
										</TR>
										<TR vAlign="top">
											<TD vAlign="top" align="left">
												<IFRAME id="frameAttributes" style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px; PADDING-BOTTOM: 0px; PADDING-TOP: 0px"
													frameBorder="no" width="100%" height="100%" runat="server" src="Attributes.aspx"></IFRAME>
											</TD>
										</TR>
									</TABLE>
									<TABLE id="tblWorkflow" borderColor="#cccccc" cellSpacing="0" cellPadding="0" border="2"
										runat="server">
										<TR height="14">
											<TD style="FONT-WEIGHT: bold; FONT-SIZE: 10pt; COLOR: black; FONT-FAMILY: Arial" align="left"
												bgColor="#cccccc" colSpan="3"><IMG src="Images/nav_wfcon.gif"></TD>
										</TR>
										<TR borderColor="#ffffff">
											<TD style="HEIGHT: 205px" vAlign="top" width="100%" colSpan="3">
												<IFRAME id="frameWorkflow" style="DISPLAY: block" name="iWorkflow" src="WorkflowSubmit.aspx"
													frameBorder="no" width="100%" scrolling="no" height="100%" runat="server"></IFRAME>
												<IFRAME id="frameWorkflowHistory" style="VISIBILITY: hidden" name="frameWorkflowHistory"
													src="WorkflowHistory.aspx" frameBorder="no" width="100%" scrolling="yes" height="100%"
													runat="server"></IFRAME>
											</TD>
										</TR>
										<tr id="WorkflowTabs">
											<td id="Td1" vAlign="baseline" borderColor="white" align="left" colSpan="1" runat="server"><A onclick="LoadReview();" href="#"><IMG id="imgReview" height="20" src="Images/imgReview.gif" width="50" border="0"></A><A onclick="LoadHistory();" href="#"><IMG id="imgHistory" height="20" src="Images/imgHistoryOff.gif" width="50" border="0"></A></td>
										</tr>
									</TABLE>
								</td>
							</tr>
							<tr>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<UC1:TOPRIGHT id="TopRight1" runat="server"></UC1:TOPRIGHT>
					</td>
				</tr>
			</table>
			<DIV id="IndexItems" style="DISPLAY: none" runat="server">
				<INPUT id="GoHistory" type="hidden" value="0"> <input id="Popup" type="hidden" value="0" name="Popup" runat="server">
				<input id="ImageType" type="hidden" runat="server"><input id="VersionId" type="hidden" runat="server">
				<input id="hostname" type="hidden" runat="server">
				<SCRIPT language="javascript" event="OnChange" for="GoHistory">
				GoBack();
				</SCRIPT>
				<ASP:LITERAL id="litStartService" runat="server"></ASP:LITERAL><ASP:LITERAL id="litShowViewer" runat="server"></ASP:LITERAL><INPUT id="ShowWorkflow" type="hidden" runat="server"><INPUT id="pageNo" type="hidden" runat="server">
				<INPUT id="accountId" type="hidden" runat="server"> <INPUT id="DownloadQuality" type="hidden" runat="server">
				<ASP:LITERAL id="litHideProgress" runat="server"></ASP:LITERAL><ASP:LITERAL id="litJavascript" runat="server"></ASP:LITERAL><input id="mode" type="hidden" runat="server"><input id="routeid" type="hidden" runat="server"><ASP:LABEL id="lblAttributeError" runat="server" Visible="False" ForeColor="Red"></ASP:LABEL>
				<ASP:LITERAL id="litServices" runat="server"></ASP:LITERAL>
				<ASP:LABEL id="lblDocumentId" runat="server" Visible="False"></ASP:LABEL>
				<INPUT id="Coordinates" type="hidden"> <INPUT id="txtShowImage" type="hidden" name="txtShowImage" runat="server">
				<asp:textbox id="txtUser" runat="server" Height="0px" Width="0px"></asp:textbox>
				<asp:imagebutton id="btnWf" runat="server" Height="0px" Width="0px"></asp:imagebutton><IMG id="ThumbsBar" style="LEFT: 0px; VISIBILITY: hidden; CURSOR: hand; POSITION: absolute; TOP: 124px"
					onclick="ShowThumbs();" src="Images/bar_vert_thumbnails.gif" border="0">
				<script language="javascript" src="scripts/zeppelin.js"></script>
				<script language="javascript" src="scripts/cookies.js"></script>
				<script language="javascript">

			LoadThumbs();
			ShowThumbs();
			
			//show thumbs first time or when cookie is lost
			if (readCookie('docAssistThumbs') == '')
			{
				saveCookie('docAssistThumbs', 1, 365);	
			}				
							
			var showDist = false;
		
			var resultbln = readCookie("results");
			if (resultbln == "1") { window.history.back(); }
		
			var hideMode = 0;
			var hideWF = false;
			
			var lastPageDelete = readCookie("LastPageDeleted");
			if (lastPageDelete == "1")
			{
				saveCookie("LastPageDeleted","0",1);
				GoToPage(1);
			}

			buttons();
				
			var showResults = document.all.docHeader_ShowResults.value;
			
			if (showResults == "0")
			{
				document.all.btnResult.style.display = "none";
			}
			else
			{
				document.all.btnResult.style.display = "block";
			}	 

			var thumbSetting = readCookie('docAssistThumbs');
			if (thumbSetting == 0)
			{
				HideThumbs();
			}
			else
			{
				ShowThumbs();
			}		

			function LoadThumbs()
			{
				var url = document.all.iFrameThumbnails.src;
				if (url.indexOf('ImageThumbnails.aspx') == -1)
				{
					document.all.iFrameThumbnails.src = 'ImageThumbnails.aspx?';
				}
			}

				function saveCookie(name,value,days) 
				{ 
					if (!days) expires = "" 
					else{ 
						var date = new Date(); 
						date.setTime(date.getTime()+ 
						(days*24*60*60*1000)) 
						var expires = "; expires="+ 
						date.toGMTString() 
						} 
					document.cookie=name+"="+ 
					escape(value)+expires+"; path=/" 
				} 

				function HideThumbs()
				{
					document.all.ThumbsBar.style.visibility = 'visible';
					document.all.tblThumbnails.style.display = 'none';
					hideMode = 1;
					saveCookie('docAssistThumbs',0,365);
					ResizeControls();
				}
				
				function ShowThumbs()
				{
					//tab on left
					document.all.ThumbsBar.style.visibility = 'hidden';
					document.all.tblThumbnails.style.display = 'block';
					hideMode = 0;
					saveCookie('docAssistThumbs',1,365);
					ResizeControls();
				}

				
				function ResizeControls()
				{
					
				
				}
			
			function PageContentScroll(setting)
			{
				document.all.frameImage.scrolling = setting;
				document.all.frameImage.frameBorder = setting;
			}
			
			function GoBack()
			{
				if(Form1.GoHistory.value == "1")
					history.back();
			}

			function buttons()
			{
			  	document.all.docHeader_btnIndex.style.display = "INLINE";
				document.all.docHeader_btnIndex.src = "./Images/toolbar_viewer_grey.gif";	
			}
			
			function readCookie(name) 
			{ 
				name+="=" 
				var ca = document.cookie.split(';') 
				for(var i in ca) 
					{ 
					var c = ca[i]; 
					while (c.charAt(0)==' ') 
					c = c.substring(1,c.length) 
					if (c.indexOf(name) == 0) 
					return unescape(c.substring(name.length,c.length)) 
					} 
				return '' 
			} 

			function LoadReview()
			{
				document.all.frameWorkflow.style.visibility = 'visible';
				document.all.frameWorkflow.style.display = 'block';
				document.all.frameWorkflowHistory.style.display = 'none';	
				document.all.imgReview.src = 'Images/imgReview.gif';
				document.all.imgHistory.src = 'Images/imgHistoryOff.gif';
			}
			
			function LoadHistory()
			{
				document.all.frameWorkflowHistory.style.visibility = 'visible';
				document.all.frameWorkflowHistory.style.display = 'block';
				document.all.frameWorkflow.style.display = 'none';			
				document.all.imgReview.src = 'Images/imgReviewOff.gif';
				document.all.imgHistory.src = 'Images/imgHistory.gif';
			}

			function SaveHistory()
			{
				document.all.frameWorkflowHistory.style.visibility = 'visible';
				document.all.frameWorkflowHistory.style.display = 'block';
				document.all.frameWorkflow.style.display = 'none';
				document.all.frameWorkflowHistory.src='dummy.htm';
				document.all.frameWorkflowHistory.src='WorkflowHistory.aspx';
				document.all.WorkflowTabs.style.display = 'none';
			}
			
			function clickUpload()
			{
				var frameImage = document.getElementById('frameImage');
				if (frameImage.src == "ViewAttachments.aspx")
				{
					parent.frames['frameImage'].clickSave();
				}
			}
			
			function showWorkflowPane(mode, routeIdnum)
			{
				document.getElementById('mode').value = mode;
				document.getElementById('routeid').value = routeIdnum;
				document.getElementById('btnWf').click();
			}
			
			function hideWorkflowPane()
			{
				document.getElementById('ShowWorkflow').value = '0';
				document.all.frameWorkflow.style.visibility = 'hidden';
				document.all.tblWorkflow.style.visibility = 'hidden';
				ResizeControls();
				var wfTabs = document.getElementById('WorkflowTabs');
				if (wfTabs) { wfTabs.style.display = 'none'; }
			}

			function loadDistribution(id)
			{
				if (document.getElementById('frmDistribution').src == 'dummy.htm')
				{
					document.getElementById('frmDistribution').src = 'Distribution.aspx?ID='+id;
				}
			}

			function showDistribution(fromButton)
			{
				
				showDist = true;
				
				var frmDistribution = document.getElementById('frmDistribution');
				var frameImage = document.getElementById('frameImage');
				
				//move up viewer
				if (parent.frames["frameAttributes"].getGl() == true)
				{
					if (frameImage != undefined)
					{
						var viewerHeight = (frameImage.style.height.replace('px','') * 1);
						frameImage.style.height = viewerHeight - 250;
					}
				
					//position distribution div
					var distTop = (frameImage.style.top.replace('px','') * 1) + (frameImage.style.height.replace('px','') * 1);
					frmDistribution.style.top = distTop + 2;
					frmDistribution.style.left = frameImage.style.left;
					frmDistribution.style.width = (frameImage.style.width.replace('px','') * 1) - 2;
					frmDistribution.style.height = 245;
					frmDistribution.style.display = 'block';
					parent.frames["frameAttributes"].setGlEnabled(false);
				}
			}
			
			function hideDistribution()
			{
				showDist = false;
				
				var frmDistribution = document.getElementById('frmDistribution');
				frmDistribution.style.display = 'none';
				
				//move up viewer
				var frameImage = document.getElementById('frameImage');
				if (frameImage != undefined)
				{
					var viewerHeight = (frameImage.style.height.replace('px','') * 1);
					frameImage.style.height = viewerHeight + 250;
					parent.frames["frameAttributes"].setGlEnabled(true);
				}
					
				ResizeControls();

			}

			function reloadThumbs()
			{
				parent.frames["iFrameThumbnails"].reloadThumbs();
			}
			
			function pinWorkflowPane()
			{
				hideWF = true;
				document.all.tblWorkflow.style.display = 'none';
				document.getElementById('ShowWorkflow').value = '0';
				var wfBar = document.getElementById('WfBar');
				wfBar.style.display = 'block';
				positionWf();
				ResizeControls();
			}
			
			function unpinWorkflowPane()
			{
				hideWF = false;
				document.all.tblWorkflow.style.display = 'block';
				document.getElementById('ShowWorkflow').value = '1';
				ResizeControls();
				var wfBar = document.getElementById('WfBar');
				wfBar.style.display = 'none';
			}
			
			function positionWf()
			{
				myWidth = document.body.clientWidth;
				myHeight = document.body.clientHeight;
				var wfBar = document.getElementById('WfBar');
				wfBar.style.top = myHeight - 38;
				wfBar.style.left = myWidth - 136;
			}

				</script>
				<INPUT id="pageCount" type="hidden" runat="server" NAME="pageCount">
				<INPUT id="pageNumber" type="hidden" runat="server" NAME="Hidden1">
			</DIV>
		</FORM>
	</BODY>
</HTML>
--%>
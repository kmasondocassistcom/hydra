<%@ Register TagPrefix="uc1" TagName="docHeader" Src="Controls/docHeader.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ChangePwd.aspx.vb" Inherits="docAssistWeb.ChangePwd"%>
<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="Controls/MenuBar.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
  <HEAD>
		<TITLE>docAssist - Change Password</TITLE>
		<META content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<META content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<META content="JavaScript" name="vs_defaultClientScript">
		<META content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
  </HEAD>
	<BODY class="Prefs">
		<FORM id="Form1" method="post" runat="server">
			<br>
			<table cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
				<tr>
					<td rowspan="6" width="20"></td>
					<td class="PrefHeader">
						Change My Password
					</td>
				</tr>
				<tr>
					<td>
						
							<img src="dummy.gif" height="5" width="1">&nbsp;<BR>
							<IMG height="1" src="dummy.gif" width="60">
							<asp:Label id="lblReq" runat="server" CssClass=Text></asp:Label><BR><br>
					</td>
				</tr>
				<tr>
					<td>
						<table cellSpacing="0" cellPadding="0" width="100%" align="center" border="0" class="Prefs">
							<tr>
								<td width="20" rowspan="3"></td>
								<td noWrap class="LabelName" align="right">Current Password:</td>
								<td>&nbsp;
									<asp:textbox id="txtCurrent" runat="server" TextMode="Password" Width="248px" MaxLength="15"
										CssClass="Text"></asp:textbox></td>
							</tr>
							<tr>
								<td width="160" noWrap class="LabelName" align="right">New Password:</td>
								<td>&nbsp;
									<asp:textbox id="txtNewPass" runat="server" TextMode="Password" Width="248px" MaxLength="15"
										CssClass="Text"></asp:textbox></td>
							</tr>
							<tr>
								<td width="160" noWrap class="LabelName" align="right">Confirm Password:</td>
								<td>&nbsp;
									<asp:textbox id="txtConfirmPass" runat="server" TextMode="Password" Width="248px" MaxLength="15"
										CssClass="Text"></asp:textbox>&nbsp;
									<asp:label id="lblMsg" runat="server" ForeColor="Red" Font-Names="Verdana" Font-Size="XX-Small"></asp:label></td>
							</tr>
						</table>
						<BR>
						<IMG height="1" src="spacer.gif" width="375">
						<asp:imagebutton id="btnSave" runat="server" ImageUrl="Images/btn_save.gif"></asp:imagebutton>
					</td>
				</tr>
			</table>
		</FORM>
	</BODY>
</HTML>

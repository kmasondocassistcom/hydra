<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ODBCSources.aspx.vb" Inherits="docAssistWeb.ODBCSources"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Choose ODBC Source</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body class="Prefs">
		<form id="Form1" method="post" runat="server">
			<table width="250">
				<tr>
					<td align="right">
						<asp:LinkButton id="lnkDelete" runat="server" CssClass="PageContent">Delete</asp:LinkButton>
						<asp:LinkButton id="lnkEdit" runat="server" CssClass="PageContent">Edit</asp:LinkButton>
					</td>
				</tr>
				<tr>
					<td>
						<asp:ListBox id="SourcesList" runat="server" Width="100%" Height="150px" AutoPostBack="True"></asp:ListBox>
					</td>
				</tr>
				<tr>
					<td align="right">
						<asp:ImageButton id="Save" runat="server" ImageUrl="Images/btn_small_ok.gif"></asp:ImageButton></td>
				</tr>
			</table>
			<script language="javascript">
				
				function editODBCSource(ID)
				{
					var ODBC = document.getElementById('ODBC');
					var val = window.showModalDialog('CreateODBCSource.aspx?ID='+ID,'EditODBCSource','resizable:no;status:no;dialogHeight:200px;dialogWidth:350px;scroll:no');
					if (val != undefined)
					{
						document.getElementById('btnrefresh').click();
					}
				}			
			
			</script>
			<asp:ImageButton id="btnrefresh" runat="server" Height="0px" Width="0px"></asp:ImageButton>
		</form>
	</body>
</HTML>

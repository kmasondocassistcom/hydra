Imports Accucentric.docAssist
Imports Accucentric.docAssist.Data
Imports System.Text.RegularExpressions
Imports Accucentric.docAssist.Data.Images
Imports Atalasoft.Imaging
Imports Atalasoft.Imaging.Codec.Pdf

Partial Class _Mail
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'Protected WithEvents tblTopOptions As System.Web.UI.HtmlControls.HtmlTable
    'Protected WithEvents tblBottomOptions As System.Web.UI.HtmlControls.HtmlTable
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Dim intPageCount As Integer
    Dim strFromName As String
    Dim strFromEmail As String


    Private mcookieSearch As cookieSearch
    Private mobjUser As Accucentric.docAssist.Web.Security.User
    Private mconSqlImage As SqlClient.SqlConnection
    Private blnAnnotation As Boolean = False
    Private blnRedaction As Boolean = False

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Response.Expires = 0
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        Try
            Me.mobjUser = BuildUserObject(Request.Cookies("docAssist"))
            Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
            Me.mcookieSearch = New cookieSearch(Me.Page)
            Me.mconSqlImage.Open()

            If Me.mcookieSearch.VersionID = -1 Then
                Me.mcookieSearch.VersionID = Request.QueryString("VID")
            End If

        Catch ex As Exception
            If ex.Message = "User is not logged on" Then
                'TODO: Close window
                Response.Redirect("Default.aspx")
            End If
        End Try

        'Load autocomplete Email addresses

        Dim addressBook As New Email.AddressBook(Me.mconSqlImage, False)
        Dim list As New DataTable
        list = addressBook.ListAddresses(Me.mobjUser.ImagesUserId, "%")

        Dim sb As New System.Text.StringBuilder
        sb.Append("<script>to.plugins['autocomplete'].setValues([")
        Dim count As Integer = 1
        For Each dr As DataRow In list.Rows
            sb.Append("['" & dr("EmailAddress") & "', '" & dr("EmailAddress") & "', '" & dr("EmailAddress") & "']")
            If count < list.Rows.Count Then sb.Append(",")
            count += 1
        Next
        sb.Append("]);</script>")

        sb.Append("<script>cc.plugins['autocomplete'].setValues([")
        count = 1
        For Each dr As DataRow In list.Rows
            sb.Append("['" & dr("EmailAddress") & "', '" & dr("EmailAddress") & "', '" & dr("EmailAddress") & "']")
            If count < list.Rows.Count Then sb.Append(",")
            count += 1
        Next
        sb.Append("]);</script>")

        sb.Append("<script>bcc.plugins['autocomplete'].setValues([")
        count = 1
        For Each dr As DataRow In list.Rows
            sb.Append("['" & dr("EmailAddress") & "', '" & dr("EmailAddress") & "', '" & dr("EmailAddress") & "']")
            If count < list.Rows.Count Then sb.Append(",")
            count += 1
        Next
        sb.Append("]);</script>")

        Page.RegisterStartupScript("preload", sb.ToString)


        'These are here to catch the user pressing enter on any of these textboxes.
        'txtTo.Attributes("onkeypress") = "catchSubmit();"
        'txtCc.Attributes("onkeypress") = "catchSubmit();"
        'txtBcc.Attributes("onkeypress") = "catchSubmit();"
        'txtSubject.Attributes("onkeypress") = "catchSubmit();"
        'txtRangeTo.Attributes("onkeypress") = "catchSubmit();"
        'txtRangeFrom.Attributes("onkeypress") = "catchSubmit();"

        Try
            strFromName = Me.mobjUser.UserName
            strFromEmail = Me.mobjUser.EMailAddress
            'txtFrom.Text = strFromName & " [" & strFromEmail & "]"
            txtRangeFrom.Attributes.Add("OnKeypress", "return nb_numericOnly(event);")
            txtRangeTo.Attributes.Add("OnKeypress", "return nb_numericOnly(event);")
        Catch ex As Exception

        End Try

        If Not IsNothing(Request.QueryString("Scan")) Then
            deliveryOptions.Visible = False
            If Request.QueryString("Scan") = "true" Then
                combine.Visible = True
            Else
                combine.Visible = False
            End If
        Else


        End If

        If Not IsNothing(Request.QueryString("Mode")) Then
            'Mass email
            combine.Visible = True
            pageOptions.Visible = False
            annotations.Visible = False
            btnDownload.Visible = False
        Else

            combine.Visible = False

            Select Case CType(Session("FileType"), Functions.FileType)
                Case Functions.FileType.ATTACHMENT
                    pageOptions.Visible = False
                    annotations.Visible = False
                    'idMailChoice.InnerText = ""
                    'rdoLink.Visible = False
                    'rdoAttach.Visible = False
                    btnDownload.Visible = False
            End Select

            Try
                'Annotation security
                DocumentAnnotationSecurityLevel(Me.mobjUser.ImagesUserId, Me.mcookieSearch.VersionID, Me.mconSqlImage, blnAnnotation, blnRedaction)

                If blnAnnotation = False Then
                    annotations.Visible = False
                End If

                If blnRedaction = False Then
                    'chkRedactions.Visible = False
                    annotations.Visible = False
                End If

            Catch ex As Exception

            End Try


            If Not Page.IsPostBack Then

                Dim daImages As New docAssistImages
                Dim Request As HttpRequest = Context.Request
                Dim intVersionId As Integer

                'Get VersionId of the image to e-mail
                intVersionId = Me.mcookieSearch.VersionID
                txtRangeTo.Text = daImages.PageCount(intVersionId)

                Request = Nothing
                daImages = Nothing

            Else
                page_body.Attributes("onload") = "javascript:if(document.all.rdoAllPages.checked){rdoPages_click(document.all.rdoAllPages);}else{rdoPages_click(document.all.rdoRange);}"
            End If

        End If

    End Sub

    Private Sub imgCancel_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Response.Write("<script language='javascript'>window.close();</script>")
    End Sub

    Private Function ValidatePage(Optional ByVal blnExport As Boolean = False) As Boolean

        Try

            If blnExport = False Then

                'Check e-mail addresses.
                Dim intCounter As Integer
                Dim strRegEx As String = "^\w+([-+.]\w+)*@\w+([-.]\w+)*\.([a-zA-Z]{2,4})$"
                Dim strToList() As String
                Dim strCcList() As String
                Dim strBccList() As String

                If txtTo.Text.Trim = "" And txtCc.Text.Trim = "" And txtBcc.Text.Trim = "" Then
                    lblPageError.Text = "Please enter an e-mail address."
                    Return False
                End If

                strToList = txtTo.Text.Split(",")
                strCcList = txtCc.Text.Split(",")
                strBccList = txtBcc.Text.Split(",")

                For intCounter = 0 To (strToList.Length - 1)
                    If strToList.Length = 0 Or strToList(intCounter).Trim = "" Then Exit For
                    If Regex.IsMatch(strToList(intCounter).Trim, strRegEx) Then
                    Else
                        lblPageError.Text = "Invalid 'To:' address."
                        Return False
                    End If
                Next

                For intCounter = 0 To (strCcList.Length - 1)
                    If strCcList.Length = 0 Or strCcList(intCounter).Trim = "" Then Exit For
                    If Regex.IsMatch(strCcList(intCounter).Trim, strRegEx) Then
                    Else
                        lblPageError.Text = "Invalid 'Cc:' address."
                        Return False
                    End If
                Next

                For intCounter = 0 To (strBccList.Length - 1)
                    If strBccList.Length = 0 Or strBccList(intCounter).Trim = "" Then Exit For
                    If Regex.IsMatch(strBccList(intCounter).Trim, strRegEx) Then
                    Else
                        lblPageError.Text = "Invalid 'Bcc:' address."
                        Return False
                    End If
                Next

            End If

            'Check page ranges.
            If rdoRange.Checked Then

                Dim dtImages As New docAssistImages
                Dim Request As HttpRequest = Context.Request
                Dim intVersionId As Integer
                Dim intPageCounter As Integer

                'Get VersionId of the current e-mail.
                intVersionId = Me.mcookieSearch.VersionID

                intPageCounter = dtImages.PageCount(intVersionId)

                If (txtRangeTo.Text < 1) Or (txtRangeFrom.Text > intPageCounter) Or (txtRangeTo.Text > intPageCounter) Or _
                (txtRangeFrom.Text > txtRangeTo.Text) Then
                    lblPageError.Text = "Invalid page range."
                    Return False
                Else
                    lblPageError.Text = ""
                    Return True
                End If

                Request = Nothing
                dtImages = Nothing
            Else
                Return True
            End If

        Catch Ex As Exception
            lblPageError.Text = "Unexpected error: " & Ex.Message
            Return False
        End Try

    End Function

    Private Sub btnSend_ServerClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSend.ServerClick

        txtTo.Text = txtTo.Text.Replace(";", ",")
        txtCc.Text = txtCc.Text.Replace(";", ",")
        txtBcc.Text = txtBcc.Text.Replace(";", ",")

        Dim conSql As SqlClient.SqlConnection
        conSql = Functions.BuildConnection(Me.mobjUser)
        conSql.Open()

        If Not IsNothing(Request.QueryString("Mode")) Then

            If ValidatePage() Then

                Try

                    Dim dtMain As New DataTable
                    dtMain = Session("MassEmail")

                    'Put files in a ZIP file
                    Dim mem As New System.IO.MemoryStream
                    Dim zip As New ICSharpCode.SharpZipLib.Zip.ZipOutputStream(mem)
                    Dim zipentry As ICSharpCode.SharpZipLib.Zip.ZipEntry
                    Dim crc As New ICSharpCode.SharpZipLib.Checksums.Crc32

                    Dim dv As New DataView(dtMain)

                    If chkCombine.Checked Then dv.Sort = "ImageType ASC"

                    Dim strCombinedAttach As String = Server.MapPath("tmp/" & System.Guid.NewGuid.ToString.Replace("-", "") & ".pdf")

                    'Combine scans into a single PDF
                    If chkCombine.Checked Then

                        dv.RowFilter = "ImageType='1'"

                        Dim col As New Atalasoft.Imaging.ImageCollection

                        Dim versionId As Integer

                        For Each drvCombine As DataRowView In dv

                            versionId = drvCombine("VersionId")

                            'Build the image
                            Dim cmdSql As New SqlClient.SqlCommand("SMDocumentGetByVersionID", conSql)
                            cmdSql.Parameters.Add("@VersionID", drvCombine("VersionId"))
                            cmdSql.CommandType = CommandType.StoredProcedure
                            Dim dt As New DataTable
                            Dim da As New SqlClient.SqlDataAdapter(cmdSql)
                            da.Fill(dt)

                            For Each dr As DataRow In dt.Rows
                                Dim bytImage As Byte()
                                bytImage = dr("Image")
                                Dim tmpai As New Atalasoft.Imaging.AtalaImage
                                tmpai = tmpai.FromByteArray(bytImage)
                                If tmpai.ColorDepth <> 1 Then tmpai = tmpai.GetChangedPixelFormat(Atalasoft.Imaging.PixelFormat.Pixel1bppIndexed)
                                col.Add(tmpai)
                            Next

                        Next

                        ' Create the PDF.
                        Dim pdf As PdfEncoder = New PdfEncoder

                        ' Set any properties.
                        pdf.JpegQuality = 85
                        pdf.Metadata = New PdfMetadata("docAssist Export", "docAssist", "Document Export", "", "", "docAssist", DateTime.Now, DateTime.Now)

                        ' Make each image fit into an 8.5 x 11 inch page (612 x 792 @ 72 DPI).
                        pdf.SizeMode = PdfPageSizeMode.FitToPage
                        pdf.PageSize = New Size(612, 792)

                        col.Save(strCombinedAttach, pdf, Nothing)
                        col.Dispose()

                        dv.RowFilter = "ImageType <> '1'"

                    End If

                    Dim versionIdAttach As Integer

                    For Each drv As DataRowView In dv

                        versionIdAttach = drv("VersionId")

                        Dim imgByte As Byte()

                        Select Case CType(drv("ImageType"), ImageTypes)

                            Case Constants.ImageTypes.ATTACHMENT

                                'Check for file size before e-mailing
                                If Not rdoLink.Checked Then
                                    Dim intByteCount As Integer = Accucentric.docAssist.Data.Images.Functions.GetSizeBytesByVersionId(drv("VersionId"), conSql)
                                    If intByteCount > EMAIL_ATTACHMENT_SIZE Then
                                        litCancel.Text = "<script language='javascript'>alert('The selected file is too large to e-mail.');</script>"
                                        Exit Sub
                                    End If
                                End If

                                Dim objAttachments As New FileAttachments(Functions.BuildConnection(Me.mobjUser))
                                Dim dt As New DataTable
                                dt = objAttachments.GetFile(drv("VersionId"))
                                imgByte = CType(dt.Rows(0)("Data"), Byte())

                                Dim strFilename As String = drv("DocId") & " - " & CStr(dt.Rows(0)("Filename"))
                                zipentry = New ICSharpCode.SharpZipLib.Zip.ZipEntry(strFilename.Replace("/", "").Replace("\", "").Replace(":", "").Replace("*", "").Replace("?", "").Replace("""", "").Replace("<", "").Replace(">", "").Replace("|", ""))

                                zip.SetLevel(6)
                                zipentry.DateTime = DateTime.Now
                                zipentry.Size = imgByte.Length

                                crc.Reset()
                                crc.Update(imgByte)

                                zip.PutNextEntry(zipentry)
                                zip.Write(imgByte, 0, imgByte.Length)

                                ''Track attachment
                                'Try
                                '    Accucentric.docAssist.Web.Data.Functions.TrackPageRequest(Me.mobjUser.UserId, Me.mobjUser.AccountId, "Web E-Mail", "Out", versionIdAttach, "", Functions.GetMasterConnectionString)
                                'Catch ex As Exception

                                'End Try

                            Case Constants.ImageTypes.SCAN

                                'Check for file size before e-mailing
                                If Not rdoLink.Checked Then
                                    Dim intByteCount As Integer = Accucentric.docAssist.Data.Images.Functions.GetSizeBytesByVersionId(Me.mcookieSearch.VersionID, conSql, IIf(rdoRange.Checked, txtRangeFrom.Text, 0), IIf(rdoRange.Checked, txtRangeTo.Text, 0))
                                    If intByteCount > EMAIL_ATTACHMENT_SIZE Then
                                        litCancel.Text = "<script language='javascript'>alert('The selected file is too large to e-mail.');</script>"
                                        Exit Sub
                                    End If
                                End If

                                'Build the image
                                Dim daImages As New docAssistImages

                                Dim cmdSql As New SqlClient.SqlCommand("SMDocumentGetByVersionID", conSql)
                                cmdSql.Parameters.Add("@VersionID", drv("VersionId"))
                                cmdSql.CommandType = CommandType.StoredProcedure
                                Dim dt As New DataTable
                                Dim da As New SqlClient.SqlDataAdapter(cmdSql)
                                da.Fill(dt)

                                Dim strAttach As String = Server.MapPath("tmp/" & System.Guid.NewGuid.ToString.Replace("-", "") & ".pdf")

                                Dim col As New Atalasoft.Imaging.ImageCollection

                                For Each dr As DataRow In dt.Rows
                                    Dim bytImage As Byte()
                                    bytImage = dr("Image")
                                    Dim tmpai As New Atalasoft.Imaging.AtalaImage
                                    tmpai = tmpai.FromByteArray(bytImage)
                                    If tmpai.ColorDepth <> 1 Then tmpai = tmpai.GetChangedPixelFormat(Atalasoft.Imaging.PixelFormat.Pixel1bppIndexed)
                                    col.Add(tmpai)
                                Next

                                ''Track e-mail
                                'Try
                                '    Accucentric.docAssist.Web.Data.Functions.TrackPageRequest(Me.mobjUser.UserId, Me.mobjUser.AccountId, "Web E-Mail", "Out", versionIdAttach, "", Functions.GetMasterConnectionString)
                                'Catch ex As Exception

                                'End Try

                                ' Create the PDF.
                                Dim pdf As PdfEncoder = New PdfEncoder

                                ' Set any properties.
                                pdf.JpegQuality = 85
                                pdf.Metadata = New PdfMetadata("docAssist Export", "docAssist", "Document Export", "", "", "docAssist", DateTime.Now, DateTime.Now)

                                ' Make each image fit into an 8.5 x 11 inch page (612 x 792 @ 72 DPI).
                                pdf.SizeMode = PdfPageSizeMode.FitToPage
                                pdf.PageSize = New Size(612, 792)

                                col.Save(strAttach, pdf, Nothing)
                                col.Dispose()

                                Dim byteFile As Byte() = GetDataAsByteArray(strAttach)

                                Dim strFilename As String = CStr(drv("DocId")).Replace("<b>", "").Replace("</b>", "") & " - " & IIf(CStr(drv("Title")).Trim = "", "Untitled", drv("Title")) & ".pdf"
                                zipentry = New ICSharpCode.SharpZipLib.Zip.ZipEntry(strFilename.Replace("/", "").Replace("\", "").Replace(":", "").Replace("*", "").Replace("?", "").Replace("""", "").Replace("<", "").Replace(">", "").Replace("|", ""))

                                zip.SetLevel(6)
                                zipentry.DateTime = DateTime.Now
                                zipentry.Size = byteFile.Length

                                crc.Reset()
                                crc.Update(byteFile)

                                zip.PutNextEntry(zipentry)
                                zip.Write(byteFile, 0, byteFile.Length)

                                Try
                                    System.IO.File.Delete(strAttach)
                                Catch ex As Exception

                                End Try

                        End Select

                    Next

                    If chkCombine.Checked Then

                        '
                        Dim Newzipentry As New ICSharpCode.SharpZipLib.Zip.ZipEntry(System.IO.Path.GetFileName(strCombinedAttach.Replace(".tif", ".pdf")))
                        Dim combinedBytes As Byte() = GetDataAsByteArray(strCombinedAttach.Replace(".tif", ".pdf"))

                        zip.SetLevel(6)
                        Newzipentry.DateTime = DateTime.Now
                        Newzipentry.Size = combinedBytes.Length

                        crc.Reset()
                        crc.Update(combinedBytes)

                        zip.PutNextEntry(Newzipentry)
                        zip.Write(combinedBytes, 0, combinedBytes.Length)


                    End If

                    Dim zipBytes As Byte()

                    zip.Finish()
                    zip.Close()

                    zipBytes = mem.ToArray

                    Dim FileTime As DateTime = Now.AddHours(Me.mobjUser.TimeDiffFromGMT)
                    Dim strFullFileName As String = "docAssist_" & (FileTime.ToShortDateString.Replace("/", "-") & Space(1) & FileTime.ToShortTimeString.Replace(":", ".")).Replace(" ", "_") & ".zip"
                    Dim strFullPath As String = Server.MapPath("tmp/" & strFullFileName)

                    If Not GetByteArrayAsFile(strFullPath, zipBytes) Then
                        'Error creating file

                    End If

                    Dim eMail As New SendMail.Smtp
                    Dim strToEmails() As String = txtTo.Text.Split(",")
                    Dim strCcEmails() As String = txtCc.Text.Split(",")
                    Dim strBccEmails() As String = txtBcc.Text.Split(",")
                    Dim strAttachment As String

                    eMail.server = ConfigurationSettings.AppSettings("MailServer")
                    eMail.serverport = ConfigurationSettings.AppSettings("MailServerPort")
                    eMail.from = strFromEmail
                    eMail.displayname = strFromName
                    email.bodyText = txtMessage.Text

                    If System.IO.File.Exists(strFullPath) Then email.attachment.Add(strFullPath)

                    Dim intCounter As Integer

                    Dim addressBook As New Email.AddressBook(conSql, False)

                    If txtTo.Text.Trim.Length > 0 Then
                        For intCounter = 0 To (strToEmails.Length - 1)
                            eMail.to.Add(strToEmails(intCounter).Trim)
                            addressBook.InsertEmailAddress(Me.mobjUser.ImagesUserId, strToEmails(intCounter).Trim)
                        Next
                    End If

                    If txtCc.Text.Trim.Length > 0 Then
                        For intCounter = 0 To (strCcEmails.Length - 1)
                            eMail.cc.Add(strCcEmails(intCounter).Trim)
                            addressBook.InsertEmailAddress(Me.mobjUser.ImagesUserId, strCcEmails(intCounter).Trim)
                        Next
                    End If

                    If txtBcc.Text.Trim.Length > 0 Then
                        For intCounter = 0 To (strBccEmails.Length - 1)
                            eMail.bcc.Add(strBccEmails(intCounter).Trim)
                            addressBook.InsertEmailAddress(Me.mobjUser.ImagesUserId, strBccEmails(intCounter).Trim)
                        Next
                    End If

                    'If chkCopySender.Checked = True Then eMail.bcc.Add(Me.mobjUser.EMailAddress)

                    eMail.subject = txtSubject.Text

                    email.Send()

                    'Track attachment
                    Try
                        Dim sb As New System.Text.StringBuilder
                        For Each toEmail As String In email.to.ToArray
                            sb.Append(toEmail & ";")
                        Next
                        For Each toEmail As String In email.cc.ToArray
                            sb.Append(toEmail & ";")
                        Next
                        For Each toEmail As String In email.bcc.ToArray
                            sb.Append(toEmail & ";")
                        Next

                        Accucentric.docAssist.Web.Data.Functions.TrackPageRequest(Me.mobjUser.UserId, Me.mobjUser.AccountId, "Web Export", "Out", versionIdAttach, "", Functions.GetMasterConnectionString, , , , sb.ToString.Split(";"))
                    Catch ex As Exception

                    End Try

                    Try
                        System.IO.File.Delete(strFullPath)
                    Catch ex As Exception

                    End Try

                    'Close popup
                    Response.Write("<script language='javascript'>window.close();</script>")

                Catch ex As Exception

#If DEBUG Then
                    lblPageError.Text = ex.Message
#Else
                    lblPageError.Text = "An error has occured while e-mailing, please try again."
#End If

                Finally

                    If conSql.State <> ConnectionState.Closed Then
                        conSql.Close()
                        conSql.Dispose()
                    End If

                End Try

            End If

        Else

            If ValidatePage() = True Then

                Try
                    Dim Request As HttpRequest = Context.Request
                    Dim eMail As New SendMail.Smtp

                    Dim intCounter As Integer
                    Dim intVersionId As Integer

                    Dim strToEmails() As String = txtTo.Text.Split(",")
                    Dim strCcEmails() As String = txtCc.Text.Split(",")
                    Dim strBccEmails() As String = txtBcc.Text.Split(",")
                    Dim strAttachment As String

                    'Get VersionId of the image to e-mail
                    intVersionId = Me.mcookieSearch.VersionID

                    'Build the image
                    Dim daImages As New docAssistImages

                    intPageCount = daImages.PageCount(intVersionId)

                    If rdoAllPages.Checked And rdoLink.Checked = False Then 'all pages

                        Select Case CType(Session("FileType"), Functions.FileType)
                            Case Functions.FileType.ATTACHMENT

                                'Check for file size before e-mailing
                                If Not rdoLink.Checked Then
                                    Dim intByteCount As Integer = Accucentric.docAssist.Data.Images.Functions.GetSizeBytesByVersionId(Me.mcookieSearch.VersionID, conSql)
                                    If intByteCount > EMAIL_ATTACHMENT_SIZE Then
                                        litCancel.Text = "<script language='javascript'>alert('The selected file is too large to e-mail.');</script>"
                                        Exit Sub
                                    End If
                                End If

                                'Build file attachment

                                Dim objAttachments As New Data.Images.FileAttachments(Functions.BuildConnection(Me.mobjUser))
                                Dim dt As New DataTable
                                dt = objAttachments.GetFile(Me.mcookieSearch.VersionID)
                                Dim imgByte As Byte() = CType(dt.Rows(0)("Data"), Byte())

                                strAttachment = GetByteArrayAsData(dt.Rows(0)("Filename"), imgByte, Server.MapPath("tmp") & "\")

                            Case Else 'Scanned document

                                'Check for file size before e-mailing
                                If Not rdoLink.Checked Then
                                    Dim intByteCount As Integer = Accucentric.docAssist.Data.Images.Functions.GetSizeBytesByVersionId(Me.mcookieSearch.VersionID, conSql, IIf(rdoRange.Checked, txtRangeFrom.Text, 0), IIf(rdoRange.Checked, txtRangeTo.Text, 0))
                                    If intByteCount > EMAIL_ATTACHMENT_SIZE Then
                                        litCancel.Text = "<script language='javascript'>alert('The selected file is too large to e-mail.');</script>"
                                        Exit Sub
                                    End If
                                End If

                                strAttachment = Functions.ExportPDF(Me.mobjUser, intVersionId, 1, intPageCount, Me, chkAnnotations.Checked)

                        End Select

                    Else 'range
                        If txtRangeTo.Text > intPageCount Then
                            txtRangeTo.Text = intPageCount
                            'lblErrMsg.Text = "Invalid page range"
                            Exit Sub
                        End If

                        If rdoLink.Checked = False Then strAttachment = Functions.ExportPDF(Me.mobjUser, intVersionId, txtRangeFrom.Text, txtRangeTo.Text, Me, chkAnnotations.Checked)

                    End If

                    Dim strAttachmentPath As String
                    If CType(Session("FileType"), Functions.FileType) <> Functions.FileType.ATTACHMENT Then
                        strAttachmentPath = Server.MapPath("tmp/" & strAttachment)
                    Else
                        strAttachmentPath = strAttachment
                    End If

                    eMail.server = ConfigurationSettings.AppSettings("MailServer")
                    eMail.serverport = ConfigurationSettings.AppSettings("MailServerPort")
                    eMail.from = strFromEmail
                    eMail.displayname = strFromName

                    Dim addressBook As New Email.AddressBook(conSql, False)

                    If txtTo.Text.Trim.Length > 0 Then
                        For intCounter = 0 To (strToEmails.Length - 1)
                            eMail.to.Add(strToEmails(intCounter).Trim)
                            addressBook.InsertEmailAddress(Me.mobjUser.ImagesUserId, strToEmails(intCounter).Trim)
                        Next
                    End If

                    If txtCc.Text.Trim.Length > 0 Then
                        For intCounter = 0 To (strCcEmails.Length - 1)
                            eMail.cc.Add(strCcEmails(intCounter).Trim)
                            addressBook.InsertEmailAddress(Me.mobjUser.ImagesUserId, strCcEmails(intCounter).Trim)
                        Next
                    End If

                    If txtBcc.Text.Trim.Length > 0 Then
                        For intCounter = 0 To (strBccEmails.Length - 1)
                            eMail.bcc.Add(strBccEmails(intCounter).Trim)
                            addressBook.InsertEmailAddress(Me.mobjUser.ImagesUserId, strBccEmails(intCounter).Trim)
                        Next
                    End If

                    'If chkCopySender.Checked = True Then eMail.bcc.Add(Me.mobjUser.EMailAddress)

                    eMail.subject = txtSubject.Text

                    If rdoLink.Checked = True Then

                        Dim sb As New System.Text.StringBuilder
                        Dim enc As Encryption.Encryption
                        Dim strRemove = "loaddoc=" & Request.QueryString("loaddoc")
                        Dim strURL = Request.Url.ToString.Replace(Request.Url.Query, "").Replace("Mail.aspx", "Search.aspx?ref=email&loadversion=" & intVersionId)

                        Dim reader As New System.IO.StreamReader(Server.MapPath("./resources/email/sendDocumentAsLink.txt"))
                        Dim emailMessage As String = reader.ReadToEnd
                        reader.Close()

                        emailMessage = emailMessage.Replace("[User]", Me.mobjUser.UserName)
                        emailMessage = emailMessage.Replace("[Message]", txtMessage.Text)
                        emailMessage = emailMessage.Replace("[Link]", strURL)
                        eMail.bodyHtml = True
                        eMail.bodyText = emailMessage

                    Else

                        eMail.bodyText = txtMessage.Text
                        eMail.attachment.Add(strAttachmentPath)

                    End If

                    eMail.Send()

                    'Track e-mail
                    Try
                        Dim sb As New System.Text.StringBuilder
                        For Each toEmail As String In email.to.ToArray
                            sb.Append(toEmail & ";")
                        Next
                        For Each toEmail As String In email.cc.ToArray
                            sb.Append(toEmail & ";")
                        Next
                        For Each toEmail As String In email.bcc.ToArray
                            sb.Append(toEmail & ";")
                        Next
                        Accucentric.docAssist.Web.Data.Functions.TrackPageRequest(Me.mobjUser.UserId, Me.mobjUser.AccountId, "Web E-Mail", "Out", Me.mcookieSearch.VersionID, "", Functions.GetMasterConnectionString, , , , sb.ToString.Split(";"))
                    Catch ex As Exception

                    End Try

                    Try
                        'Delete attachment from temp directory.
                        If System.IO.File.Exists(strAttachmentPath) Then System.IO.File.Delete(strAttachmentPath)
                        System.IO.Directory.Delete(System.IO.Path.GetDirectoryName(strAttachment))
                    Catch ex As Exception

                    End Try

                    eMail = Nothing
                    daImages = Nothing

                    'Close popup
                    Response.Write("<script language='javascript'>window.close();</script>")

                Catch exMail As SendMail.SmtpException
#If DEBUG Then
                    lblPageError.Text = exMail.Message
#Else
                                lblPageError.Text = "An error has occured. Please try again."
#End If

                Catch ex As Exception
#If DEBUG Then
                    lblPageError.Text = ex.Message
#Else
                                lblPageError.Text = "An error has occured. Please try again."
#End If
                End Try

            End If

        End If

    End Sub

    Private Sub btnDownload_ServerClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDownload.ServerClick

        If ValidatePage(True) = True Then

            Dim Request As HttpRequest = Context.Request
            Dim intCounter As Integer
            Dim intVersionId As Integer
            Dim strAttachment As String

            'Get VersionId of the image to e-mail
            intVersionId = Me.mcookieSearch.VersionID

            'Build the image
            Dim daImages As New docAssistImages
            intPageCount = daImages.PageCount(intVersionId)
            If rdoAllPages.Checked Then
                strAttachment = ExportPDF(Me.mobjUser, intVersionId, 1, intPageCount, Me, IIf(chkAnnotations.Checked, True, False))
            Else
                If txtRangeTo.Text > intPageCount Then
                    txtRangeTo.Text = intPageCount
                    Exit Sub
                End If
                strAttachment = ExportPDF(Me.mobjUser, intVersionId, txtRangeFrom.Text, txtRangeTo.Text, Me, chkAnnotations.Checked)
            End If

            'Track attachment
            Try
                For x As Integer = 1 To intPageCount
                    Accucentric.docAssist.Web.Data.Functions.TrackPageRequest(Me.mobjUser.UserId, Me.mobjUser.AccountId, "Web Export", "Out", intVersionId, "", Functions.GetMasterConnectionString)
                Next
            Catch ex As Exception

            End Try

            daImages = Nothing

            'TODO: code for toolbar direct download
            'Dim FileTime As DateTime = Now.AddHours(Me.mobjUser.TimeDiffFromGMT)
            'Dim fs As New IO.FileStream(Server.MapPath("tmp/") & strAttachment, IO.FileMode.Open)
            'Dim br As New IO.BinaryReader(fs)
            'Dim bytes As Byte() = br.ReadBytes(fs.Length)

            'Response.Clear()
            'Response.AddHeader("Content-Disposition", "attachment; filename=" & "docAssist.pdf")
            'Response.AddHeader("Content-Length", bytes.Length)
            'Response.ContentType = "application/octet-stream"
            'Response.BinaryWrite(bytes)
            'Response.End()

            Response.Redirect("Export.aspx?File=" & Request.Url.GetLeftPart(UriPartial.Path).Replace("Mail.aspx", "") & "tmp/" & strAttachment.Replace("\", "/"))

        End If

    End Sub

    Private Sub btnShare_ServerClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnShare.ServerClick

        Dim enc As New Encryption.Encryption
        Dim conSql As New SqlClient.SqlConnection

        Try

            If ValidatePage(True) = True Then

                Dim userID, accountID, versionId, shareDate, startPage, endPage As String

                Dim link As New System.Text.StringBuilder
                link.Append(Me.mobjUser.AccountId.ToString & ";")
                link.Append(Me.mcookieSearch.VersionID & ";")

                If rdoAllPages.Checked Then
                    link.Append("All" & ";")
                    link.Append("All" & ";")
                Else
                    link.Append(txtRangeFrom.Text.ToString & ";")
                    link.Append(txtRangeTo.Text.ToString & ";")
                End If

                'link.Append(txtRangeFrom.Text.ToString & ";")
                'link.Append(txtRangeTo.Text.ToString & ";")
                link.Append(Now.ToString & ";")
                link.Append(Session("FileType"))

                Dim params As New Collection
                params.Add(New EmailParameter("USER", Me.mobjUser.UserName))
                params.Add(New EmailParameter("MESSAGE", "<i>" & txtMessage.Text & "</i>"))

                If Request.Url.ToString.StartsWith("http://dev.docassist") Then
                    params.Add(New EmailParameter("LINK", "http://dev.docassist.com/pub/DownloadDocument.aspx?q=" & enc.Encrypt(link.ToString).Replace("=", "~").Replace("+", "^")))
                ElseIf Request.Url.ToString.StartsWith("http://stage.docassist") Then
                    params.Add(New EmailParameter("LINK", "http://stage.docassist.com/pub/DownloadDocument.aspx?q=" & enc.Encrypt(link.ToString).Replace("=", "~").Replace("+", "^")))
                ElseIf Request.Url.ToString.StartsWith("https://stage.docassist") Then
                    params.Add(New EmailParameter("LINK", "https://stage.docassist.com/pub/DownloadDocument.aspx?q=" & enc.Encrypt(link.ToString).Replace("=", "~").Replace("+", "^")))
                ElseIf Request.Url.ToString.IndexOf("localhost") > 0 Then
                    params.Add(New EmailParameter("LINK", "http://localhost/docAssistWeb1/pub/DownloadDocument.aspx?q=" & enc.Encrypt(link.ToString).Replace("=", "~").Replace("+", "^")))
                ElseIf Request.Url.ToString.StartsWith("https://my.docassist") Then
                    params.Add(New EmailParameter("LINK", "https://my.docassist.com/pub/DownloadDocument.aspx?q=" & enc.Encrypt(link.ToString).Replace("=", "~").Replace("+", "^")))
                End If

                params.Add(New EmailParameter("PROMOLINK", "http://www.docassist.com"))

                conSql = Functions.BuildConnection(Me.mobjUser)
                conSql.Open()

                Dim strToEmails() As String = txtTo.Text.Split(",")
                Dim strCcEmails() As String = txtCc.Text.Split(",")
                Dim strBccEmails() As String = txtBcc.Text.Split(",")

                Dim addressBook As New Email.AddressBook(conSql, False)

                For Each str As String In strToEmails
                    If str <> "" Then addressBook.InsertEmailAddress(Me.mobjUser.ImagesUserId, str)
                Next

                For Each str As String In strCcEmails
                    If str <> "" Then addressBook.InsertEmailAddress(Me.mobjUser.ImagesUserId, str)
                Next

                For Each str As String In strBccEmails
                    If str <> "" Then addressBook.InsertEmailAddress(Me.mobjUser.ImagesUserId, str)
                Next

                Functions.sendEmailMessage(Functions.EmailType.SHARE_DOCUMENT, txtTo.Text, txtCc.Text, txtBcc.Text, params, Me, IIf(txtSubject.Text.Trim.Length = 0, Me.mobjUser.UserName & " wants to share a document with you", txtSubject.Text), "noreply@docassist.com")

                'Close popup
                Response.Write("<script language='javascript'>window.close();</script>")

            End If

        Catch ex As Exception

            Dim conMaster As SqlClient.SqlConnection
            conMaster = Functions.BuildMasterConnection
            Functions.LogException(mobjUser.UserId.ToString, mobjUser.AccountId.ToString, "", "Sending share document: " & ex.Message, ex.StackTrace, Server.MachineName, "", Request.UrlReferrer.ToString, conMaster, True)

        Finally

            If conSql.State <> ConnectionState.Closed Then
                conSql.Close()
                conSql.Dispose()
            End If

        End Try

    End Sub

End Class

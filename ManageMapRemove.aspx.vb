Partial Class ManageMapRemove
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private mobjUser As Accucentric.docAssist.Web.Security.User
    Private mconSqlImage As SqlClient.SqlConnection

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'Put user code to initialize the page here
        If Not Page.IsPostBack Then

            PopulateDocuments()
            If lstDocuments.Items.Count > 0 Then
                lstDocuments.Items(0).Selected = True
            End If
        End If

    End Sub


    Private Sub PopulateDocuments()

        Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
        Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))

        Me.mobjUser.AccountId = guidAccountId
        Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
        Me.mconSqlImage.Open()

        Dim strMode As String
        If Session("mAdminMapTable") = "INTEGRATION" Then strMode = 0 Else strMode = 1

        Dim cmdSql As SqlClient.SqlCommand

        Dim dtsql As New DataTable
        cmdSql = New SqlClient.SqlCommand("acsAdminIntegrationGetDocuments", Me.mconSqlImage)
        cmdSql.CommandType = CommandType.StoredProcedure
        cmdSql.Parameters.Add("@Mode", strMode)
        Dim dasql As New SqlClient.SqlDataAdapter(cmdSql)
        dasql.Fill(dtsql)

        If dtsql.Rows.Count <= 0 Then
            'TODO: No Cabinets to change
            lstDocuments.Items.Clear()
        Else
            lstDocuments.DataTextField = "DocumentName"
            lstDocuments.DataValueField = "UniqueKey"
            lstDocuments.DataSource = dtsql
            lstDocuments.DataBind()
        End If
    End Sub

    Private Sub ImageButton2_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If lstDocuments.Items.Count <= 0 Then Exit Sub

        If Session("mAdminMapTable") = "INTEGRATION" Then
            RemoveMappingIntegration()
        End If

        If Session("mAdminMapTable") = "CAPTURE" Then
            RemoveMappingCapture()
        End If
        Response.Redirect("ManageIntegration.aspx")
    End Sub

    Private Function RemoveMappingCapture() As Boolean

        If lstDocuments.Items.Count <= 0 Then Exit Function
        Dim strKey As String()
        strKey = Split(lstDocuments.SelectedItem.Value, ";")
        Session("mAdminIntegrationFormID") = strKey(0)
        Session("mAdminIntegrationDocumentID") = strKey(1)



        Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
        Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))

        Me.mobjUser.AccountId = guidAccountId
        Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
        Me.mconSqlImage.Open()
        Dim cmdSql As SqlClient.SqlCommand
        Dim dtsql As New DataTable


        cmdSql = New SqlClient.SqlCommand("acsAdminCaptureMapClear", Me.mconSqlImage)
        cmdSql.CommandType = CommandType.StoredProcedure
        cmdSql.Parameters.Add("@DocumentID", Session("mAdminIntegrationDocumentID"))
        cmdSql.Parameters.Add("@FormID", Session("mAdminIntegrationFormID"))
        cmdSql.ExecuteNonQuery()
    End Function

    Private Function RemoveMappingIntegration() As Boolean
        Dim strKey As String()
        strKey = Split(lstDocuments.SelectedItem.Value, ";")
        Session("mAdminIntegrationFormID") = strKey(0)
        Session("mAdminIntegrationDocumentID") = strKey(1)


        Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
        Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))

        Me.mobjUser.AccountId = guidAccountId
        Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
        Me.mconSqlImage.Open()
        Dim cmdSql As SqlClient.SqlCommand
        Dim dtsql As New DataTable


        cmdSql = New SqlClient.SqlCommand("acsAdminIntegrationMapClear", Me.mconSqlImage)
        cmdSql.CommandType = CommandType.StoredProcedure
        cmdSql.Parameters.Add("@DocumentID", Session("mAdminIntegrationDocumentID"))
        cmdSql.Parameters.Add("@FormID", Session("mAdminIntegrationFormID"))
        cmdSql.ExecuteNonQuery()

    End Function


    Private Sub btnNext_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnNext.Click

        If lstDocuments.Items.Count <= 0 Then Exit Sub

        If Session("mAdminMapTable") = "INTEGRATION" Then
            RemoveMappingIntegration()
        End If

        If Session("mAdminMapTable") = "CAPTURE" Then
            RemoveMappingCapture()
        End If
        Response.Redirect("ManageIntegration.aspx")

    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("ManageIntegration.aspx")
    End Sub
End Class

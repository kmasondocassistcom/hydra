Partial Class ManageDocumentsChange
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region


    Private mobjUser As Accucentric.docAssist.Web.Security.User
    Private mconSqlImage As SqlClient.SqlConnection


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Not Page.IsPostBack Then

            PopulateDocumentList()

            If Session("mAdminDocumentMode") = "CHANGE" Then
                'lblMode.Text = "Manage Documents - Change Document"
                lblAction.Text = "Select Document to Change"
                'lnkNext.Text = "Next"
                btnFinish.ImageUrl = "Images/btn_nextadmin.gif"
            End If

            If Session("mAdminDocumentMode") = "REMOVE" Then
                'lblMode.Text = "Manage Documents - Remove Document"
                lblAction.Text = "Select Document to Remove"
                'lnkNext.Text = "Finish"
                btnFinish.ImageUrl = "Images/btn_finish.gif"
            End If

        End If

    End Sub

    Private Sub PopulateDocumentList()
        Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
        Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))

        Me.mobjUser.AccountId = guidAccountId
        Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
        Me.mconSqlImage.Open()

        Dim cmdSql As SqlClient.SqlCommand

        Dim dtsql As New DataTable
        cmdSql = New SqlClient.SqlCommand("acsAdminDocumentList", Me.mconSqlImage)
        cmdSql.CommandType = CommandType.StoredProcedure
        Dim dasql As New SqlClient.SqlDataAdapter(cmdSql)
        dasql.Fill(dtsql)

        If dtsql.Rows.Count <= 0 Then
            'TODO: No Cabinets to change
        Else
            lstDocuments.DataTextField = "DocumentName"
            lstDocuments.DataValueField = "DocumentID"
            lstDocuments.DataSource = dtsql
            lstDocuments.DataBind()
            lstDocuments.Items(0).Selected = True
        End If

    End Sub

    Private Sub lnkNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If lstDocuments.SelectedItem.Value > 0 Then

            If Session("mAdminDocumentMode") = "CHANGE" Then
                Response.Cookies("Admin")("DocumentID") = lstDocuments.SelectedItem.Value
                Response.Cookies("Admin")("DocumentName") = lstDocuments.SelectedItem.Text
                Response.Redirect("ManageDocumentsChange2.aspx")
            End If

            If Session("mAdminDocumentMode") = "REMOVE" Then
                Session("mAdminDocumentID") = lstDocuments.SelectedItem.Value
                Session("mAdminDocumentName") = lstDocuments.SelectedItem.Text
                If CheckDocumentUsed(lstDocuments.SelectedItem.Value) = True Then
                    lblError.Text = "Document " & lstDocuments.SelectedItem.Text & " has indexed documents. Cannot remove document."
                    Exit Sub
                Else
                    lblError.Text = ""
                    RemoveDocument(lstDocuments.SelectedItem.Value)
                    Response.Redirect("ManageDocuments.aspx")
                End If
            End If


        End If
    End Sub


    Private Function CheckDocumentUsed(ByVal iDocumentID As Integer) As Boolean
        Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
        Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))

        Me.mobjUser.AccountId = guidAccountId
        Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
        Me.mconSqlImage.Open()

        Dim cmdSql As SqlClient.SqlCommand

        Dim dtsql As New DataTable
        cmdSql = New SqlClient.SqlCommand("acsAdminDocumentRemoveCheck", Me.mconSqlImage)
        cmdSql.CommandType = CommandType.StoredProcedure
        cmdSql.Parameters.Add("@DocumentID", iDocumentID)
        Dim dasql As New SqlClient.SqlDataAdapter(cmdSql)
        dasql.Fill(dtsql)


        If dtsql.Rows.Count > 0 Then
            Return True
        Else
            Return False

        End If

    End Function

    Private Function RemoveDocument(ByVal strDocumentID As String) As Boolean

        Dim cmdSql As SqlClient.SqlCommand
        Dim dtsql As New DataTable
        cmdSql = New SqlClient.SqlCommand("acsAdminDocumentRemove", Me.mconSqlImage)
        cmdSql.CommandType = CommandType.StoredProcedure
        cmdSql.Parameters.Add("@DocumentID", strDocumentID)
        cmdSql.ExecuteNonQuery()

    End Function

    Private Function DocumentExists(ByVal strDocumentName As String) As Boolean
        Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
        Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))

        Me.mobjUser.AccountId = guidAccountId
        Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
        Me.mconSqlImage.Open()

        Dim cmdSql As SqlClient.SqlCommand

        Dim dtsql As New DataTable
        cmdSql = New SqlClient.SqlCommand("acsAdminDocumentCheck", Me.mconSqlImage)
        cmdSql.CommandType = CommandType.StoredProcedure
        cmdSql.Parameters.Add("@DocumentName", strDocumentName)
        Dim dasql As New SqlClient.SqlDataAdapter(cmdSql)
        dasql.Fill(dtsql)


        If dtsql.Rows.Count > 0 Then
            Return True
        Else
            Return False

        End If
    End Function


    Private Sub btnFinish_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFinish.Click
        If lstDocuments.SelectedItem.Value > 0 Then

            If Session("mAdminDocumentMode") = "CHANGE" Then
                Session("mAdminDocumentID") = lstDocuments.SelectedItem.Value
                Session("mAdminDocumentName") = lstDocuments.SelectedItem.Text
                Response.Redirect("ManageDocumentsChange2.aspx")
            End If

            If Session("mAdminDocumentMode") = "REMOVE" Then
                Session("mAdminDocumentID") = lstDocuments.SelectedItem.Value
                Session("mAdminDocumentName") = lstDocuments.SelectedItem.Text
                If CheckDocumentUsed(lstDocuments.SelectedItem.Value) = True Then
                    lblError.Text = "Document " & lstDocuments.SelectedItem.Text & " has indexed documents. Cannot remove document."
                    Exit Sub
                Else
                    lblError.Text = ""
                    RemoveDocument(lstDocuments.SelectedItem.Value)
                    Response.Redirect("ManageDocuments.aspx")
                End If
            End If


        End If
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("ManageDocuments.aspx")

    End Sub
End Class

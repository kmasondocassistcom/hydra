<%@ Register TagPrefix="rada" Namespace="Telerik.WebControls" Assembly="RadAjax" %>
<%@ Register TagPrefix="radt" Namespace="Telerik.WebControls" Assembly="RadTreeView" %>
<%@ Register TagPrefix="uc1" TagName="TopRight" Src="Controls/TopRight.ascx" %>
<%@ Register TagPrefix="uc1" TagName="docHeader" Src="Controls/docHeader.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="NewSearch.aspx.vb" Inherits="docAssistWeb.NewSearch" buffer="True"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<TITLE>docAssist - Search</TITLE>
		<META content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<META content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<META content="JavaScript" name="vs_defaultClientScript">
		<META content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body onresize="resizeElements();return false;" bottomMargin="0" leftMargin="0" topMargin="0"
		onload="resizeElements();return false;" rightMargin="0">
		<form id="frmSearch" method="post" runat="server">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
					<td vAlign="top" noWrap colSpan="2"><uc1:docheader id="docHeader" runat="server"></uc1:docheader></td>
				</tr>
				<tr align="left">
					<td vAlign="top" width="250" height="100%">
						<!-- Quick search table -->
						<table class="PageContent" id="tblQuickSearch" height="55" cellSpacing="0" cellPadding="0"
							width="100%" border="0">
							<tr vAlign="top" align="left">
								<td width="250">
									<div class="Container">
										<div class="north">
											<div class="east">
												<div class="south">
													<div class="west">
														<div class="ne">
															<div class="se">
																<div class="sw">
																	<div class="nw">
																		<table cellSpacing="0" cellPadding="0" width="100%" border="0">
																			<tr>
																				<td noWrap>
																					<table cellSpacing="0" cellPadding="0" width="100%" border="0">
																						<tr>
																							<td noWrap><span class="Heading">Quick Search:</span></td>
																							<td align="right"></td>
																						</tr>
																					</table>
																					<table cellSpacing="0" cellPadding="0" width="100%" border="0">
																						<tr>
																							<td colSpan="2"><IMG height="1" src="Images/spacer.GIF" width="15"><asp:textbox id="txtQuickSearch" runat="server" Width="100%" Font-Size="8pt" Font-Names="Trebuchet MS"></asp:textbox></td>
																							<td width="100%"></td>
																						</tr>
																						<tr>
																							<td width="100%"><ASP:CHECKBOX id="chkQuickOCRSearch" runat="server" CssClass="Text" AutoPostBack="False" TEXT="Search Document Text"
																									WIDTH="160px"></ASP:CHECKBOX><BR>
																							</td>
																							<td vAlign="bottom" align="right"><ASP:IMAGEBUTTON id="btnQuickSearch" runat="server" ImageUrl="Images/btn_search.gif"></ASP:IMAGEBUTTON></td>
																						</tr>
																					</table>
																				</td>
																			</tr>
																		</table>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</td>
							</tr>
						</table>
						<!-- Tree table -->
						<table class="PageContent" id="tblTree" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<TBODY>
								<tr vAlign="top" align="left">
									<td noWrap width="250">
										<DIV class="Container">
											<DIV class="north">
												<DIV class="east">
													<DIV class="south">
														<DIV class="west">
															<DIV class="ne">
																<DIV class="se">
																	<DIV class="sw">
																		<DIV class="nw">
																			<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																				<TR vAlign="top">
																					<TD noWrap>
																						<TABLE id="tblShare" cellPadding="0" border="0" runat="server">
																							<TR vAlign="top">
																								<TD width="25"><asp:imagebutton id="btnNewShare" runat="server" ImageUrl="Images/folders/share.gif" ToolTip="New Share"></asp:imagebutton></TD>
																								<TD width="25"><asp:imagebutton id="btnShareNewFolder" runat="server" ImageUrl="Images/folders/newfolder.gif" ToolTip="New Folder"></asp:imagebutton></TD>
																								<TD width="25"><A id="Rename" href="#" runat="server"><IMG id="imgRenameShare" onclick="EditModeNode(document.all.NodeId.value);" height="15"
																											src="Images/folders/rename.gif" width="15" border="0" runat="server"></A></TD>
																								<TD width="25"><asp:imagebutton id="btnRemoveFolder" runat="server" ImageUrl="Images/folders/remove.gif" ToolTip="Remove Folder"></asp:imagebutton></TD>
																								<TD width="25"><asp:imagebutton id="btnFolderSecurity" runat="server" ImageUrl="Images/folders/security.gif" ToolTip="Security"></asp:imagebutton></TD>
																								<TD width="25"><asp:imagebutton id="btnAddFavorite" runat="server" ImageUrl="Images/folders/addfavorite.gif" ToolTip="Add to Favorites"></asp:imagebutton></TD>
																								<TD width="25"><asp:imagebutton id="btnRemoveFavorite" runat="server" ImageUrl="Images/folders/remove_favorite.gif"
																										ToolTip="Remove Favorite"></asp:imagebutton></TD>
																							</TR>
																						</TABLE>
																						<DIV class="PageContent" id="divTree" style="OVERFLOW: auto; WIDTH: 245px; HEIGHT: 300px"
																							runat="server"><radt:radtreeview id="radTree" runat="server" AfterClientClick="LoadProgress" AllowNodeEditing="True"
																								LoadingMessagePosition="AfterNodeText" CheckBoxes="True" AutoPostBackOnCheck="True"></radt:radtreeview><BR>
																							<INPUT id="NodeId" type="hidden" runat="server"></DIV>
																					</TD>
																				</TR>
																			</TABLE>
																		</DIV>
																	</DIV>
																</DIV>
															</DIV>
														</DIV>
													</DIV>
												</DIV>
											</DIV>
										</DIV>
									</td>
								</tr>
					</td>
			</table>
			</TD>
			<td vAlign="top" noWrap width="100%">
				<!-- Advanced Search table -->
				<TABLE class="PageContent" id="tblAdvancedSearch" cellSpacing="0" cellPadding="0" width="100%"
					border="0" runat="server">
					<TR vAlign="top">
						<TD vAlign="top">
							<DIV class="Container">
								<DIV class="north">
									<DIV class="east">
										<DIV class="south">
											<DIV class="west">
												<DIV class="ne">
													<DIV class="se">
														<DIV class="sw">
															<DIV class="nw">
																<TABLE id="Table1" cellSpacing="0" cellPadding="0" width="95%" border="0">
																	<TR>
																		<TD colSpan="1"><SPAN class="Heading"><STRONG><FONT color="#808080">Advanced Search:</FONT></STRONG></SPAN>
																		</TD>
																		<TD align="right"><ASP:IMAGEBUTTON id="lnkSearch" runat="server" ImageUrl="Images/btn_search.gif"></ASP:IMAGEBUTTON></TD>
																	</TR>
																	<TR>
																		<TD colSpan="2">
																			<TABLE id="Table2" cellSpacing="0" cellPadding="0" width="100%" border="0">
																				<TR>
																					<TD><IMG height="1" src="spacer.gif" width="12">
																					</TD>
																					<TD noWrap width="100%" colSpan="1" rowSpan="2"><ASP:DATAGRID id="dgSearchAttributes" runat="server" CssClass="SearchGrid" GridLines="None" AutoGenerateColumns="False"
																							ShowHeader="False" BorderStyle="None">
																							<Columns>
																								<asp:TemplateColumn HeaderText="Attribute">
																									<HeaderStyle Width="196px"></HeaderStyle>
																									<ItemTemplate>
																										<asp:Label id=lblAttribute runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AttributeName") %>'>
																										</asp:Label><IMG height="1" alt="" src="../images/spacer.gif" width="4"><INPUT id=hiddenAttributeId type=hidden size=1 value='<%# DataBinder.Eval(Container, "DataItem.AttributeId") %>' name=hiddenAttributeId RUNAT="server">
																									</ItemTemplate>
																								</asp:TemplateColumn>
																								<asp:TemplateColumn HeaderText="Value">
																									<HeaderStyle Width="100%"></HeaderStyle>
																									<ItemTemplate>
																										<asp:Panel id="pnlValue" runat="server">
																											<asp:DropDownList id="ddlSearchType" runat="server" CssClass="SearchGrid" Width="85px" AutoPostBack="True"
																												Font-Size="8pt">
																												<asp:ListItem Value="0">Equals</asp:ListItem>
																												<asp:ListItem Value="1">Starts With</asp:ListItem>
																												<asp:ListItem Value="2">Contains</asp:ListItem>
																												<asp:ListItem Value="5">Between</asp:ListItem>
																											</asp:DropDownList>
																											<asp:TextBox id="txtAttributeValue" runat="server" Width="180px" Font-Size="8pt" Height="21px"
																												Font-Names="Trebuchet MS"></asp:TextBox>
																											<asp:TextBox id="txtBetweenFrom" runat="server" Width="76px" Font-Size="8pt" Height="21px" Font-Names="Trebuchet MS"
																												Visible="False"></asp:TextBox>
																											<asp:Label id="lblBetweenAnd" runat="server" CssClass="SearchGrid" Font-Size="8pt" Visible="False">and</asp:Label>
																											<asp:TextBox id="txtBetweenTo" runat="server" Width="76px" Font-Size="8pt" Height="21px" Font-Names="Trebuchet MS"
																												Visible="False"></asp:TextBox>
																											<ASP:DROPDOWNLIST id="ddlAttributeList" runat="server" CssClass="SearchGrid" Width="330px" Font-Size="8pt"
																												Height="21px" Visible="False"></ASP:DROPDOWNLIST>
																											<ASP:CHECKBOX id="chkBox" runat="server" CssClass="SearchGrid" Font-Size="8pt" Visible="False"></ASP:CHECKBOX>
																											<ASP:LABEL id="lblRowError" runat="server" Visible="False" ForeColor="Red"></ASP:LABEL>
																										</asp:Panel>
																									</ItemTemplate>
																									<FooterTemplate>
																										&nbsp;&nbsp;
																									</FooterTemplate>
																								</asp:TemplateColumn>
																								<asp:TemplateColumn Visible="False" HeaderText="AttributeId">
																									<ItemTemplate>
																										<asp:Label id=lblAttributeId runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AttributeId") %>'>
																										</asp:Label>
																									</ItemTemplate>
																								</asp:TemplateColumn>
																								<asp:TemplateColumn Visible="False" HeaderText="Format">
																									<ItemTemplate>
																										<asp:Label id=lblAttributeDataType runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AttributeDataType") %>'>
																										</asp:Label>
																										<asp:Label id=lblLength runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Length") %>'>
																										</asp:Label>
																										<asp:Label id=lblListViewStyle runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ListViewStyle") %>'>
																										</asp:Label>
																										<asp:Label id=lblDataFormat runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DataFormat") %>'>
																										</asp:Label>
																									</ItemTemplate>
																								</asp:TemplateColumn>
																								<asp:TemplateColumn HeaderText="Validation">
																									<ItemTemplate>
																										<ASP:LABEL id="Label1" runat="server" Visible="False" ForeColor="Red"></ASP:LABEL>
																									</ItemTemplate>
																								</asp:TemplateColumn>
																							</Columns>
																						</ASP:DATAGRID></TD>
																				</TR>
																			</TABLE>
																			<SPAN class="SearchSubHeading"><IMG height="1" src="Images/spacer.GIF" width="6"><STRONG><EM><FONT size="2">Options</FONT></EM></STRONG></SPAN></TD>
																		</SPAN></TR>
																	<TR>
																		<TD colSpan="2">
																			<TABLE id="Table3" cellSpacing="2" cellPadding="0" width="100%" border="0">
																				<TR>
																					<TD noWrap><SPAN class="Text"><FONT size="2"><STRONG><EM><IMG height="1" src="Images/spacer.GIF" width="9"></EM></STRONG>Document 
																								Text: </FONT>
																							<asp:dropdownlist id="ddlFullText" runat="server" BackColor="White">
																								<asp:ListItem Value="0">Contains</asp:ListItem>
																								<asp:ListItem Value="1">Starts With</asp:ListItem>
																								<asp:ListItem Value="2">Equals</asp:ListItem>
																							</asp:dropdownlist><SPAN class="SearchSubHeading"><FONT size="2"><IMG height="1" src="Images/spacer.GIF" width="2"></FONT></SPAN></SPAN></TD>
																					<TD width="100%"><ASP:TEXTBOX id="txtKeywords" Font-Size="8pt" Font-Names="Trebuchet MS" WIDTH="80%" Runat="server"
																							Rows="1" MAXLENGTH="255"></ASP:TEXTBOX><FONT size="2"></FONT></TD>
																				</TR>
																				<TR>
																					<TD><FONT size="2"></FONT></TD>
																					<TD><ASP:DROPDOWNLIST id="ddlModifiedUser" Width="80%" Font-Size="8pt" Font-Names="Trebuchet MS" Runat="server"></ASP:DROPDOWNLIST><FONT size="2"></FONT></TD>
																				</TR>
																				<TR>
																					<TD><FONT size="2"></FONT></TD>
																					<TD><ASP:CHECKBOX id="chkOrphans" runat="server" CssClass="Text" TEXT="Include Un-indexed Documents"
																							WIDTH="200px"></ASP:CHECKBOX><FONT size="2"></FONT></TD>
																				</TR>
																				<TR>
																					<TD align="right" colSpan="2"><SPAN class="Text"><ASP:LABEL id="lblError" runat="server" CssClass="Error" Visible="False" ForeColor="Red"></ASP:LABEL><FONT size="2"></FONT></SPAN></TD>
																				</TR>
																			</TABLE>
																		</TD>
																	</TR>
																</TABLE>
															</DIV>
														</DIV>
													</DIV>
												</DIV>
											</DIV>
										</DIV>
									</DIV>
								</DIV>
							</DIV>
						</TD>
					</TR>
				</TABLE>
				<TABLE class="PageContent" id="tblSmartFolder" height="100%" cellSpacing="0" cellPadding="0"
					width="100%" border="0" runat="server"> <!-- Save SmartFolder table -->
					<TR height="100%">
						<TD vAlign="top" height="100%">
							<DIV class="Container">
								<DIV class="north">
									<DIV class="east">
										<DIV class="south">
											<DIV class="west">
												<DIV class="ne">
													<DIV class="se">
														<DIV class="sw">
															<DIV class="nw">
																<TABLE id="Table8" height="100%" cellSpacing="0" cellPadding="0" width="95%" border="0">
																	<TR>
																		<TD>
																			<TABLE id="Table9" cellSpacing="0" cellPadding="0" width="100%" border="0">
																				<TR>
																					<TD><SPAN class="Heading"><STRONG><FONT color="#808080">SmartFolder:</FONT></STRONG></SPAN></TD>
																					<TD align="right"><STRONG><FONT color="#808080"></FONT></STRONG></TD>
																				</TR>
																			</TABLE>
																			<TABLE id="Table10" cellSpacing="0" cellPadding="0" width="100%" border="0">
																				<TR>
																					<TD noWrap><STRONG><FONT color="#808080"><IMG height="1" src="Images/spacer.GIF" width="10"></FONT></STRONG><SPAN class="Text"><FONT size="2">SmartFolder 
																								Name:</FONT></SPAN>
																					</TD>
																					<TD width="100%"><IMG height="1" src="Images/spacer.GIF" width="10">
																						<asp:textbox id="txtSmartFolderName" runat="server" Width="80%" Font-Size="8pt" Font-Names="Trebuchet MS"
																							MaxLength="35"></asp:textbox></TD>
																				</TR>
																				<TR>
																					<TD align="right" colSpan="2">
																						<TABLE id="Table11" cellSpacing="0" cellPadding="0" width="100%" border="0">
																							<TBODY>
																								<TR>
																									<TD><IMG height="1" src="Images/spacer.GIF" width="40">
																										<asp:radiobutton id="rdoPrivate" runat="server" CssClass="Text" Text="Private" Checked="True" GroupName="SmartFolder"></asp:radiobutton><IMG height="1" src="Images/spacer.GIF" width="5">
																										<asp:radiobutton id="rdoPublic" runat="server" CssClass="Text" Text="Public (Share with Other Users)"
																											GroupName="SmartFolder"></asp:radiobutton></TD>
																					</TD>
																					<TD align="right"><asp:imagebutton id="btnUpdateSmartFolder" runat="server" Width="0px" Height="0px"></asp:imagebutton><ASP:IMAGEBUTTON id="btnSaveSmartFolder" runat="server" ImageUrl="Images/save_small.gif"></ASP:IMAGEBUTTON></TD>
																				</TR>
																			</TABLE>
																			<ASP:LABEL id="lblSmartFolderError" runat="server" CssClass="Error" ForeColor="Red"></ASP:LABEL></TD>
																	</TR>
																</TABLE>
																<IMG height="100%" src="spacer.gif" width="1">
						</TD>
					</TR>
				</TABLE>
				</DIV></DIV></DIV></DIV></DIV></DIV></DIV></DIV></DIV></td>
			</TR></TBODY></TABLE></TD></TR>
			<tr>
				<td colSpan="2"><uc1:topright id="docFooter" runat="server"></uc1:topright><rada:radajaxmanager id="ajaxManager" runat="server">
						<AjaxSettings>
							<rada:AjaxSetting AjaxControlID="btnQuickSearch">
								<UpdatedControls>
									<rada:AjaxUpdatedControl ControlID="dgResults"></rada:AjaxUpdatedControl>
								</UpdatedControls>
							</rada:AjaxSetting>
							<rada:AjaxSetting AjaxControlID="radTree">
								<UpdatedControls>
									<rada:AjaxUpdatedControl ControlID="dgSearchAttributes"></rada:AjaxUpdatedControl>
									<rada:AjaxUpdatedControl ControlID="rdoPrivate"></rada:AjaxUpdatedControl>
									<rada:AjaxUpdatedControl ControlID="rdoPublic"></rada:AjaxUpdatedControl>
									<rada:AjaxUpdatedControl ControlID="dgResults"></rada:AjaxUpdatedControl>
									<rada:AjaxUpdatedControl ControlID="radTree"></rada:AjaxUpdatedControl>
									<rada:AjaxUpdatedControl ControlID="NodeId"></rada:AjaxUpdatedControl>
								</UpdatedControls>
							</rada:AjaxSetting>
							<rada:AjaxSetting AjaxControlID="lnkSearch">
								<UpdatedControls>
									<rada:AjaxUpdatedControl ControlID="dgResults"></rada:AjaxUpdatedControl>
								</UpdatedControls>
							</rada:AjaxSetting>
							<rada:AjaxSetting AjaxControlID="dgSearchAttributes">
								<UpdatedControls>
									<rada:AjaxUpdatedControl ControlID="dgSearchAttributes"></rada:AjaxUpdatedControl>
								</UpdatedControls>
							</rada:AjaxSetting>
							<rada:AjaxSetting AjaxControlID="rdoPrivate">
								<UpdatedControls>
									<rada:AjaxUpdatedControl ControlID="rdoPrivate"></rada:AjaxUpdatedControl>
									<rada:AjaxUpdatedControl ControlID="rdoPublic"></rada:AjaxUpdatedControl>
								</UpdatedControls>
							</rada:AjaxSetting>
							<rada:AjaxSetting AjaxControlID="rdoPublic">
								<UpdatedControls>
									<rada:AjaxUpdatedControl ControlID="rdoPrivate"></rada:AjaxUpdatedControl>
									<rada:AjaxUpdatedControl ControlID="rdoPublic"></rada:AjaxUpdatedControl>
								</UpdatedControls>
							</rada:AjaxSetting>
							<rada:AjaxSetting AjaxControlID="btnSaveSmartFolder">
								<UpdatedControls>
									<rada:AjaxUpdatedControl ControlID="lblSmartFolderError"></rada:AjaxUpdatedControl>
									<rada:AjaxUpdatedControl ControlID="radTree"></rada:AjaxUpdatedControl>
								</UpdatedControls>
							</rada:AjaxSetting>
							<rada:AjaxSetting AjaxControlID="btnBrowseFolder">
								<UpdatedControls>
									<rada:AjaxUpdatedControl ControlID="dgResults"></rada:AjaxUpdatedControl>
								</UpdatedControls>
							</rada:AjaxSetting>
							<rada:AjaxSetting AjaxControlID="radTree">
								<UpdatedControls>
									<rada:AjaxUpdatedControl ControlID="dgSearchAttributes"></rada:AjaxUpdatedControl>
									<rada:AjaxUpdatedControl ControlID="rdoPrivate"></rada:AjaxUpdatedControl>
									<rada:AjaxUpdatedControl ControlID="rdoPublic"></rada:AjaxUpdatedControl>
									<rada:AjaxUpdatedControl ControlID="dgResults"></rada:AjaxUpdatedControl>
									<rada:AjaxUpdatedControl ControlID="radTree"></rada:AjaxUpdatedControl>
									<rada:AjaxUpdatedControl ControlID="NodeId"></rada:AjaxUpdatedControl>
								</UpdatedControls>
							</rada:AjaxSetting>
						</AjaxSettings>
					</rada:radajaxmanager><input id="UpdateFlag" type="hidden" runat="server"><input id="LastScope" type="hidden" runat="server">
					<input id="BrowseFolder" type="hidden" runat="server">
					<asp:imagebutton id="btnBrowseFolder" runat="server" Width="0px" Height="0px"></asp:imagebutton></td>
			</tr>
			</TBODY></TABLE>
			<div id="divResults" style="BORDER-RIGHT: black thin solid; BORDER-TOP: black thin solid; LEFT: 0px; VISIBILITY: hidden; OVERFLOW: auto; BORDER-LEFT: black thin solid; WIDTH: 0px; BORDER-BOTTOM: black thin solid; POSITION: absolute; TOP: 0px; HEIGHT: 0px"><asp:datagrid id="dgResults" runat="server" Width="100%" CssClass="SearchGrid" AutoGenerateColumns="False"
					ShowHeader="False" BorderStyle="None">
					<AlternatingItemStyle BackColor="#E0E0E0"></AlternatingItemStyle>
					<Columns>
						<asp:TemplateColumn>
							<HeaderStyle Width="100%"></HeaderStyle>
							<ItemTemplate>
								<TABLE class="ResultAttributes" cellSpacing="0" cellPadding="0" width="100%" border="0">
									<TR>
										<TD vAlign="middle" noWrap width="30">
											<asp:Image id="imgResultType" runat="server"></asp:Image></TD>
										<TD id="tdHeader" vAlign="top" noWrap width="175">
											<asp:Label id=lblDocumentName runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DocumentName") %>'>
											</asp:Label><BR>
											<asp:Label id=lblSummary runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SeqTotal") %>'>
											</asp:Label>
											<asp:Label id=lblPages runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.SeqTotal") %>'>
											</asp:Label>
											<asp:Label id=lblVersion runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.VersionNum") %>'>
											</asp:Label>
											<asp:Label id=lblModifiedDate runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.ModifiedDate") %>'>
											</asp:Label>
											<asp:Label id=lblVersionId runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.VersionId") %>'>
											</asp:Label>
											<asp:Label id=lblImageType runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.ImageType") %>'>
											</asp:Label>
											<asp:Label id=lblFileName runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.FileName") %>'>
											</asp:Label>
											<asp:Label id=lblDocumentTitle runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.DocumentTitle") %>'>
											</asp:Label>
											<asp:Label id=lblFolderId runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.FolderId") %>'>
											</asp:Label><BR>
											<asp:Label id=lblFolderName runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.FolderName") %>'>
											</asp:Label>
											<asp:Label id="lblModifiedDateFormatted" runat="server" Visible="True"></asp:Label></TD>
										<TD vAlign="top" noWrap><IMG height="1" src="dummy.gif" width="7"></TD>
										<TD id="tdAttributes" vAlign="top" width="100%">
											<asp:Label id="lblAttributes" runat="server"></asp:Label></TD>
									</TR>
								</TABLE>
								<TABLE class="ResultAttributes" cellSpacing="0" cellPadding="0" width="100%" border="0">
									<TR>
										<TD>
											<asp:Label id=lblFolder runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.DocumentTitle") %>'>
											</asp:Label></TD>
									</TR>
								</TABLE>
							</ItemTemplate>
						</asp:TemplateColumn>
					</Columns>
				</asp:datagrid></div>
			<TABLE id="frameLoading" style="Z-INDEX: 12; LEFT: 350px; VISIBILITY: hidden; POSITION: absolute; TOP: -140px"
				borderColor="#bd0000" cellSpacing="0" cellPadding="0" border="0" runat="server">
				<TR borderColor="#ffffff">
					<TD vAlign="top" width="100%" colSpan="3"><IFRAME id="iFrameLoading" name="iFrameLoading" src="LoadImage.htm" frameBorder="no" width="350"
							scrolling="no" height="80" runat="server"></IFRAME>
					</TD>
				</TR>
			</TABLE>
			<iframe id="frameAdd" style="BORDER-RIGHT: black thin; BORDER-TOP: black thin; LEFT: 0px; VISIBILITY: hidden; OVERFLOW: auto; BORDER-LEFT: black thin; WIDTH: 0px; BORDER-BOTTOM: black thin; POSITION: absolute; TOP: 0px; HEIGHT: 0px"
				frameBorder="no"></iframe>
			<script language="javascript">
		
				//page init scripting
				document.all.docHeader_btnSearch.src = "./Images/toolbar_search_grey.gif";
				document.all.txtQuickSearch.focus();
						
				function resizeElements()
				{
					var myHeight = document.body.clientHeight;
					var newHeight = myHeight - 280;
					
					if (newHeight -15 > 0)	{document.all.divTree.style.height = newHeight - 35;}
					
					var divResults = document.getElementById('divResults');
					if (divResults != undefined)
					{
						if ((newHeight + 100) > 0) { divResults.style.height = newHeight + 100; }
					}	
					
					var divResults = document.getElementById('divResults');
					if ((divResults != undefined) & (divResults.style.visibility == 'visible'))
					{
						var height = document.body.clientHeight;
						var width = document.body.clientWidth;
						if (width - 300 > 0){ divResults.style.width = width - 300 };
						if (height - 135 > 0){ divResults.style.height = height - 135 };
					}
				}

				function EditModeNode(text)
				{
					var node = window["radTree"].FindNodeByValue(text);
					if (node != null)
					{
						node.StartEdit();
					}
				}

				function FolderPermissions(folderid)
				{
					var d = new Date();
					var random = d.getHours()+d.getMinutes()+d.getSeconds()+d.getMilliseconds();
					window.showModalDialog('FolderPermissions.aspx?Id='+folderid+'&'+random,'FolderPermissions','resizable:no;status:no;dialogHeight:625px;dialogWidth:655px;scroll:no')
				}

				function SharePermissions(shareid)
				{
					var d = new Date();
					var random = d.getHours()+d.getMinutes()+d.getSeconds()+d.getMilliseconds();
					window.showModalDialog('SharePermissions.aspx?Id='+shareid+'&'+random,'SharePermissions','resizable:no;status:no;dialogHeight:652px;dialogWidth:655px;scroll:no')
				}
				
				function resultsMode()
				{
					var tblAdvancedSearch = document.getElementById('tblAdvancedSearch');
					if (tblAdvancedSearch != undefined){ tblAdvancedSearch.style.visibility = 'hidden'; }

					var tblSmartFolder = document.getElementById('tblSmartFolder');
					if (tblSmartFolder != undefined){ tblSmartFolder.style.visibility = 'hidden'; }

					var divResults = document.getElementById('divResults');
					if (divResults != undefined)
					{

						var tblQuickSearch = document.getElementById('tblQuickSearch');

						var height = document.body.clientHeight;
						var width = document.body.clientWidth;

						divResults.style.top = 103;
						divResults.style.left = 290;
						divResults.style.width = width - 300;
						divResults.style.height = height - 139;
												
						divResults.style.visibility = 'visible';
					}
					
					var frameAdd = document.getElementById('frameAdd');
					if (frameAdd != undefined)
					{

						var tblQuickSearch = document.getElementById('tblQuickSearch');

						var height = document.body.clientHeight;
						var width = document.body.clientWidth;

						frameAdd.style.top = 103;
						frameAdd.style.left = 290;
						frameAdd.style.width = width - 300;
						frameAdd.style.height = height - 139;
												
						frameAdd.style.visibility = 'hidden';
					}
				}
		
				function searchMode()
				{
					var tblAdvancedSearch = document.getElementById('tblAdvancedSearch');
					if (tblAdvancedSearch != undefined){ tblAdvancedSearch.style.visibility = 'visible'; }

					var tblSmartFolder = document.getElementById('tblSmartFolder');
					if (tblSmartFolder != undefined){ tblSmartFolder.style.visibility = 'visible'; }
					
					var divResults = document.getElementById('divResults');
					if (divResults != undefined)
					{
						divResults.style.visibility = 'hidden';
						divResults.style.top = 0;
						divResults.style.left = 0;
						divResults.style.height = 0;
						divResults.style.width = 0;
					} 

					var frameAdd = document.getElementById('frameAdd');
					if (frameAdd != undefined)
					{
						frameAdd.style.visibility = 'hidden';
						frameAdd.style.top = 0;
						frameAdd.style.left = 0;
						frameAdd.style.height = 0;
						frameAdd.style.width = 0;
					} 

				}

				function submitForm()
				{
					if (event.keyCode == 13)
					{
						startProgressDisplay();
						event.cancelBubble = true;
						event.returnValue = false;
						document.all.lnkSearch.click();
						
						for (i = 0; i < frmSearch.length; i++)
						{ 
							var formElement = frmSearch.elements[i]; 
							if (true)
							{ 
								formElement.disabled = true; 
							} 
						} 
					}
				}

				function startProgressDisplay()
				{
					document.all.frameLoading.style.display = "inline";
					document.all.frameLoading.style.visibility = "visible";
					var nXpos = (document.body.clientWidth - 350) / 2; 
					var nYpos = (document.body.clientHeight - 80) / 2;
					document.all.frameLoading.style.top = 200;
					document.all.frameLoading.style.left = nXpos;
					document.all.frameLoading.style.top = nYpos;
					document.all.frameLoading.style.zIndex = 99;
				}			

				function stopProgressDisplay()
				{
					document.all.frameLoading.style.display = "none";
					document.all.frameLoading.style.visibility = "hidden";
					
					for (i = 0; i < frmSearch.length; i++)
					{ 
						var formElement = frmSearch.elements[i]; 
						if (true)
						{ 
							formElement.disabled = false; 
						} 
					} 
				}

				function submitQuickForm()
				{
					if (event.keyCode == 13)
					{
						startProgressDisplay();
						event.cancelBubble = true;
						event.returnValue = false;
						document.all.btnQuickSearch.click();
						
						for (i = 0; i < frmSearch.length; i++)
						{ 
							var formElement = frmSearch.elements[i]; 
							if (true)
							{ 
								formElement.disabled = true; 
							} 
						} 
					}
				}

				function disable()
				{
					document.all.btnQuickSearch.click();
					/*
					for (i = 0; i < frmSearch.length; i++)
					{ 
						var formElement = frmSearch.elements[i]; 
						if (true)
						{
							formElement.disabled = true;
						} 
					}
					*/
					startProgressDisplay();
				}

				function LoadImage(id)
				{
					startProgressDisplay();
					var d = new Date();
					var random = d.getHours()+d.getMinutes()+d.getSeconds()+d.getMilliseconds();
					document.body.style.cursor = "wait";
					window.location.href='Index.aspx?VID='+id+'&'+random;
				}

				function addMode(id)
				{
					
					var tblAdvancedSearch = document.getElementById('tblAdvancedSearch');
					if (tblAdvancedSearch != undefined){ tblAdvancedSearch.style.visibility = 'hidden'; }

					var tblSmartFolder = document.getElementById('tblSmartFolder');
					if (tblSmartFolder != undefined){ tblSmartFolder.style.visibility = 'hidden'; }

					var divResults = document.getElementById('divResults');
					if (divResults != undefined)
					{
						divResults.style.visibility = 'hidden';
						divResults.style.top = 0;
						divResults.style.left = 0;
						divResults.style.height = 0;
						divResults.style.width = 0;
					} 

					var frameAdd = document.getElementById('frameAdd');
					if (frameAdd != undefined)
					{
						var tblQuickSearch = document.getElementById('tblQuickSearch');
						var height = document.body.clientHeight;
						var width = document.body.clientWidth;
						frameAdd.style.top = 95;
						frameAdd.style.left = 290;
						frameAdd.style.width = 700;
						frameAdd.style.height = 500;
						frameAdd.style.visibility = 'visible';
						frameAdd.src = 'UploadFiles.aspx?FolderId='+id;
					}
				}	
				
				function loadAdvanced(id)
				{
					var frameAdd = document.getElementById('frameAdd');
					frameAdd.src = 'Upload.aspx?FolderId='+id;
				}  

				function LoadProgress(node)
				{
					var value = node.Value;
					if (value.substring(0,1) == "W"){ startProgressDisplay(); }
					if (value.substring(0,1) == "F"){ startProgressDisplay(); }
					if (value.substring(0,1) == "Q"){ startProgressDisplay(); }
				}
				
				function browseFolder(id)
				{
					startProgressDisplay();
					var BrowseFolder = document.getElementById('BrowseFolder');
					if (BrowseFolder != undefined)
					{
						BrowseFolder.value = id;
						document.all.btnBrowseFolder.click();
					}
				}
				
			</script>
		</form>
	</body>
</HTML>

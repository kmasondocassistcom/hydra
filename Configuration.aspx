<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="Controls/MenuBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="docHeader" Src="Controls/docHeader.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="Configuration.aspx.vb" Inherits="docAssistWeb._Configuration"%>
<%@ Register TagPrefix="uc1" TagName="AdminNav" Src="Controls/AdminNav.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<TITLE>docAssist</TITLE>
		<META content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<META content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<META content="JavaScript" name="vs_defaultClientScript">
		<META content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<BODY>
		<FORM id="Form1" method="post" runat="server">
			<TABLE height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD colspan="3" width="100%">
						<UC1:DOCHEADER id="docHeader" runat="server"></UC1:DOCHEADER></TD>
				</TR>
				<TR vAlign="middle" height="100%">
					<TD class="Navigation1" vAlign="top" width="180" height="100%">
						<UC1:ADMINNAV id="AdminNav1" runat="server"></UC1:ADMINNAV></TD>
					<TD class="PageContent" vAlign="middle" align="center" height="100%"><P class="Heading"><ASP:LABEL ID="lblPageHeading" CssClass="PageHeading" Runat="server">Configuration Manager</ASP:LABEL></P>
						<TABLE border="0" cellpadding="0" cellspacing="0" class="transparent">
							<TR>
								<TD>Attributes</TD>
							</TR>
							<TR>
								<TD>Documents</TD>
							</TR>
							<TR>
								<TD>Folders</TD>
							</TR>
							<TR>
								<TD>Cabinets</TD>
							</TR>
						</TABLE>
					</TD>
					<TD class="RightContent">&nbsp;</TD>
				</TR>
				<TR height="1">
					<TD style="BACKGROUND-COLOR: #959394" width="180"><IMG height="1" src="images/spacer.gif" width="180"></TD>
					<TD style="BACKGROUND-COLOR: #959394" width="100%"><IMG height="1" src="images/spacer.gif" width="100%"></TD>
					<TD style="BACKGROUND-COLOR: #959394" width="90"><IMG height="1" src="images/spacer.gif" width="90"></TD>
				</TR>
			</TABLE>
		</FORM>
	</BODY>
</HTML>

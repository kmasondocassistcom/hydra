<%@ Register TagPrefix="mbclb" Namespace="MetaBuilders.WebControls" Assembly="MetaBuilders.WebControls.CheckedListBox" %>
<%@ Register TagPrefix="uc1" TagName="TopRight" Src="Controls/TopRight.ascx" %>
<%@ Register TagPrefix="uc1" TagName="AdminNav" Src="Controls/AdminNav.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ManageDocumentsAttributesOrderChange.aspx.vb" Inherits="docAssistWeb.ManageDocumentsAttributesOrderChange"%>
<%@ Register TagPrefix="uc1" TagName="docHeader" Src="Controls/docHeader.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>docAssist - Add Document</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0" MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<TABLE height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TBODY>
					<TR>
						<TD width="100%" colSpan="5"><uc1:docheader id="docHeader" runat="server"></uc1:docheader></TD>
					</TR>
					<TR vAlign="middle" height="100%">
						<TD class="Navigation1" height="100%" vAlign="top">
							<uc1:AdminNav id="AdminNav2" runat="server"></uc1:AdminNav><IMG height="1" src="dummy.gif" width="120"></TD>
						<TD vAlign="top" align="center" height="100%"><BR>
							<table cellSpacing="0" cellPadding="0" width="100%" border="0">
								<tr>
									<td><IMG height="1" src="dummy.gif" width="35" height1><IMG src="Images/manage_documents.gif"></td>
								</tr>
							</table>
							<table height="100%" cellSpacing="0" cellPadding="0" width="100%">
					</TR>
					<tr height="50%">
					</tr>
					<tr height="15" width="100%">
						<td></td>
						<td><asp:label id="Label3" runat="server" Width="121px" Height="16px">Document Name:</asp:label></td>
						<td><asp:textbox id="txtDocumentName" runat="server" Width="270px" Font-Names="Verdana" Font-Size="8pt"
								ReadOnly="True"></asp:textbox></td>
						<td></td>
					</tr>
					<tr height="15" width="100%">
						<td></td>
						<td style="WIDTH: 72px" align="left"></td>
						<TD><BR>
							<asp:label id="Label1" runat="server" Width="144px" Height="8px" Font-Names="Verdana" Font-Size="8pt"> Select Display Order</asp:label><BR>
							<asp:ListBox id="lstAttributes" runat="server" Height="200px" Width="440px"></asp:ListBox></TD>
						<TD width="50%">
							<asp:ImageButton id="btnMoveUp" runat="server" ImageUrl="Images/move_up.gif"></asp:ImageButton><BR>
							<BR>
							<asp:ImageButton id="btnMoveDown" runat="server" ImageUrl="Images/move_down.gif"></asp:ImageButton></TD>
					<TR height="15" width="100%">
						<td style="HEIGHT: 21px" width="50%"></td>
						<td style="WIDTH: 72px; HEIGHT: 21px" align="left">&nbsp;
						</td>
						<td style="HEIGHT: 21px"><asp:label id="lblError" runat="server" Width="100%" Height="4px" ForeColor="Red"></asp:label></td>
						<td style="HEIGHT: 21px" width="50%"></td>
					</TR>
					<tr height="15" width="100%">
						<td style="WIDTH: 393px" width="393"></td>
						<td style="WIDTH: 72px; HEIGHT: 17px">
							<P align="left">&nbsp;&nbsp;
							</P>
						</td>
						<td style="HEIGHT: 17px" align="right"><asp:imagebutton id="btnCancel" runat="server" ImageUrl="Images/btn_canceladmin.gif"></asp:imagebutton>&nbsp;<asp:imagebutton id="btnFinish" runat="server" ImageUrl="Images/btn_nextadmin.gif"></asp:imagebutton></td>
						<td width="50%"></td>
					</tr>
					<tr height="15" width="100%">
						<td style="WIDTH: 393px" width="393"></td>
						<td style="WIDTH: 72px" vAlign="middle">
							<P align="left">&nbsp;&nbsp;</P>
						</td>
						<td></td>
						<td width="50%"></td>
					</tr>
					<tr height="15" width="100%">
						<td style="WIDTH: 393px" width="395"></td>
						<td style="WIDTH: 72px; HEIGHT: 17px"><BR>
						</td>
						<td style="HEIGHT: 17px"></td>
						<td width="50%"></td>
					</tr>
					<tr height="15" width="100%">
						<td style="WIDTH: 395px" width="395"></td>
						<td style="WIDTH: 72px"><BR>
							<P></P>
						</td>
						<td align="right"></td>
						<TD width="50"></TD>
					</tr>
					<TR height="100%">
						<td></td>
					</TR>
				</tr></TABLE>
			<P><BR>
				<BR>
			</P>
			</TD>
			<TD class="RightContent" height="100%">&nbsp;</TD>
			</TR>
			<TR>
				<TD class="PageFooter" vAlign="bottom" bgColor="#cccccc" colSpan="3"><uc1:topright id="docFooter" runat="server"></uc1:topright></TD>
			</TR>
			<TR height="1">
				<TD style="BACKGROUND-COLOR: #959394" width="20"><IMG height="1" src="images/spacer.gif" width="20"></TD>
				<TD style="BACKGROUND-COLOR: #959394" width="100%"><IMG height="1" src="images/spacer.gif" width="100%"></TD>
				<TD style="BACKGROUND-COLOR: #959394" width="20"><IMG height="1" src="images/spacer.gif" width="20"></TD>
			</TR>
			</TBODY></TABLE></form>
	</body>
</HTML>

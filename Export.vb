
''This export function should always be kept out of web services and other classes because
''every time that PdfXpress is created it takes forever! Only create it when we need it.
''Remember to dispose this when you are done with it, PdfXpress is a hog!!!
'' -Alex Barberis 5/10/07 6:23 PM, workin late today... it's Friday, I should be out in my new truck that I bought last night! :)

'Namespace docAssist

'    Public Class docExport

'        Friend WithEvents PdfXpress As New PegasusImaging.WinForms.PdfXpress1.PdfXpress

'        Public Sub New()

'            MyBase.New()

'            PegasusImaging.WinForms.PdfXpress1.Licensing.UnlockControl(352502225, 995605554, 1962979216, 29365)

'            PdfXpress.Initialize()
'            PdfXpress.Licensing.UnlockRuntime(352502225, 995605554, 1962979216, 29365)
'            PdfXpress.Licensing.LicenseEdition = PegasusImaging.WinForms.PdfXpress1.LicenseChoice.ProfessionalEdition

'        End Sub

'        Public Sub Dispose()
'            PdfXpress.Dispose()
'        End Sub

'        Public Function GetExportImage(ByVal mobjUser As Accucentric.docAssist.Web.Security.User, ByVal intVersionID As Integer, ByVal intImageDetId As Integer, ByVal strFileName As String, _
'        ByVal IncludeExportAnnotations As Boolean, ByVal RemoveExportRedactions As Boolean, ByVal Server As System.Web.HttpServerUtility) As String

'            Dim compresseddata As System.IntPtr
'            Dim bolConOpen As Boolean
'            Dim sqlCmd As SqlClient.SqlCommand
'            Dim strReturn As String
'            Dim conSql As SqlClient.SqlConnection = BuildConnection(mobjUser)
'            Try
'                '
'                ' Get the image from the database
'                ' 
'                sqlCmd = New SqlClient.SqlCommand("acsImageDetail", conSql)
'                sqlCmd.CommandType = CommandType.StoredProcedure
'                sqlCmd.Parameters.Add("@ImageDetId", intImageDetId)

'                If Not conSql.State = ConnectionState.Open Then
'                    bolConOpen = True
'                    conSql.Open()
'                End If

'                Dim dr As SqlClient.SqlDataReader = sqlCmd.ExecuteReader(CommandBehavior.SingleRow)
'                Dim bytImage As Byte()

'                'Read the image
'                If dr.Read() Then
'                    bytImage = dr("Image")
'                End If
'                dr.Close()

'                'If we got the image, then rotate it and save the file
'                Dim ix As New ImagXpr7.ImagXpressClass
'                Dim nx As New NOTEXP70Lib.NotateXpressClass

'                PdfXpress.Initialize()
'                Dim outputDocument As PegasusImaging.WinForms.PdfXpress1.Document = New PegasusImaging.WinForms.PdfXpress1.Document

'                ' Create the output page
'                Dim pageOptions As PegasusImaging.WinForms.PdfXpress1.PageOptions
'                pageOptions = New PegasusImaging.WinForms.PdfXpress1.PageOptions

'                If strFileName = "" Then
'                    outputDocument.CreatePage(-1, pageOptions)
'                Else
'                    outputDocument = New PegasusImaging.WinForms.PdfXpress1.Document(Server.MapPath(strFileName))
'                    outputDocument.CreatePage(outputDocument.PageCount - 1, pageOptions)
'                End If

'                ' Create the location of the image on the page
'                Dim destinationX As Double = 0.25 * 72.0 ' Quarter inch margins around image
'                Dim destinationY As Double = 0.25 * 72.0
'                Dim destinationWidth As Double = pageOptions.MediaWidth - 0.5 * 72.0
'                Dim destinationHeight As Double = pageOptions.MediaHeight - 0.5 * 72.0

'                ' Define the image fit
'                Dim destinationFit As PegasusImaging.WinForms.PdfXpress1.ImageFitSettings
'                destinationFit = New PegasusImaging.WinForms.PdfXpress1.ImageFitSettings
'                destinationFit = destinationFit Or PegasusImaging.WinForms.PdfXpress1.ImageFitSettings.Shrink

'                ' Add the image to the output page
'                If Not bytImage Is Nothing Then

'                    If bytImage.Length > 0 Then
'                        If Not strFileName Is Nothing Then
'                            strReturn = strFileName
'                            If System.IO.File.Exists(Server.MapPath(strFileName)) Then
'                                ix.SaveMultiPage = True
'                            End If
'                        Else
'                            strReturn = "tmp\" & DateAdd(DateInterval.Hour, mobjUser.TimeDiffFromGMT, System.DateTime.Now).ToShortDateString.Replace("/", "") & "-" & System.DateTime.Now.Hour.ToString & System.DateTime.Now.Minute.ToString & System.DateTime.Now.Second.ToString & System.DateTime.Now.Millisecond.ToString & ".pdf"
'                        End If

'                        ix.LoadBlob(bytImage, bytImage.Length)

'                        'Apply annotations to the page if they exists
'                        Dim bytAnnotation As Byte()

'                        If IncludeExportAnnotations = True Or RemoveExportRedactions = False Then
'                            sqlCmd = New SqlClient.SqlCommand("acsImageAnnotation", conSql)
'                            sqlCmd.CommandType = CommandType.StoredProcedure
'                            sqlCmd.Parameters.Add("@VersionID", intVersionID)
'                            sqlCmd.Parameters.Add("@ImageDetId", intImageDetId)

'                            If Not conSql.State = ConnectionState.Open Then
'                                bolConOpen = True
'                                conSql.Open()
'                            End If

'                            Dim drAnn As SqlClient.SqlDataReader = sqlCmd.ExecuteReader(CommandBehavior.SingleRow)

'                            ' Read the image
'                            If drAnn.Read() Then
'                                bytAnnotation = drAnn("Annotation")
'                            End If
'                            drAnn.Close()

'                        End If

'                        Try
'                            Dim intAnnLayer As Integer
'                            Dim intRedLayer As Integer

'                            nx.InterfaceConnect(ix)
'                            nx.CreateLayer()
'                            nx.CreateLayer()

'                            If Not bytAnnotation Is Nothing Then

'                                If RemoveExportRedactions = False Then
'                                    'brand redaction
'                                    nx.TIFFAnnotationType = NOTEXP70Lib.TIFFAnnType.NXP_Compatible
'                                    nx.SetAnnFromVariant(bytAnnotation)
'                                    intAnnLayer = nx.GetFirstLayer()
'                                    intRedLayer = nx.GetNextLayer()
'                                    nx.DeleteLayer(intAnnLayer)
'                                    nx.SetActive(intRedLayer, True)

'                                    'Check if there any any redactions to brand
'                                    Dim intNumRedactions As Integer = nx.PrgGetItemCount(intRedLayer)
'                                    If intNumRedactions > 0 Then
'                                        For X As Integer = 1 To nx.PrgGetItemCount(intRedLayer)

'                                            Dim intSeqId As Integer
'                                            If X = 1 Then
'                                                intSeqId = nx.PrgGetFirstItem(intRedLayer)
'                                            Else
'                                                intSeqId = nx.PrgGetNextItem(intRedLayer)
'                                            End If

'                                            nx.PrgSetItemBackstyle(intRedLayer, intSeqId, NOTEXP70Lib.AnnBackstyle.NXP_OPAQUE)
'                                            nx.PrgSetItemColor(intRedLayer, intSeqId, NOTEXP70Lib.annotationATTRIBUTE.NXP_FillColor, Convert.ToUInt32(ColorTranslator.ToOle(System.Drawing.Color.Black)))
'                                        Next

'                                        nx.Brand(24)

'                                    End If
'                                End If

'                                If IncludeExportAnnotations = True Then
'                                    'Apply annotations
'                                    nx.TIFFAnnotationType = NOTEXP70Lib.TIFFAnnType.NXP_Compatible
'                                    nx.SetAnnFromVariant(bytAnnotation)
'                                    intAnnLayer = nx.GetFirstLayer()
'                                    intRedLayer = nx.GetNextLayer()
'                                    nx.DeleteLayer(intRedLayer)

'                                    Dim intNumAnnotations As Integer = nx.PrgGetItemCount(intAnnLayer)
'                                    If intNumAnnotations > 0 Then
'                                        nx.Brand(24) 'brand color annotations
'                                    End If

'                                End If

'                            End If

'                        Catch ex As Exception

'                        End Try

'                    End If

'                    'Get annotated image into byte array
'                    Dim tmpFile As String = Server.MapPath(strReturn).Replace(".pdf", ".tif")
'                    ix.SaveMultiPage = False
'                    ix.SaveFileName = tmpFile
'                    ix.SaveFileType = ImagXpr7.enumSaveFileType.FT_TIFF_LZW
'                    ix.SaveTIFFCompression = ImagXpr7.enumSaveTIFFCompression.TIFF_LZW
'                    ix.ColorDepth(8, ImagXpr7.enumPalette.IPAL_Optimized, ImagXpr7.enumDithered.DI_None)
'                    ix.SaveFile()

'                    bytImage = Functions.GetDataAsByteArray(tmpFile)

'                    outputDocument.AddImage(outputDocument.PageCount - 1, destinationX, destinationY, destinationWidth, destinationHeight, destinationFit, bytImage, 0)

'                    Try
'                        System.IO.File.Delete(tmpFile)
'                    Catch ex As Exception

'                    End Try

'                    Dim saveOptions As New PegasusImaging.WinForms.PdfXpress1.SaveOptions
'                    saveOptions.Filename = Server.MapPath(strReturn)
'                    saveOptions.Overwrite = True
'                    outputDocument.Save(saveOptions)
'                    outputDocument.Dispose()

'                    nx = Nothing

'                    '
'                    ' Check to make sure the file saved
'                    If ix.ImagError <> 0 Then
'                        Throw New Exception(ix.ImagError)
'                    End If
'                    '
'                    ' Destroy the ImageXpress control
'                    ix = Nothing
'                End If


'            Catch ex As Exception
'                strReturn = Nothing
'                Throw ex
'            Finally
'                If bolConOpen And (Not conSql.State = ConnectionState.Closed) Then
'                    conSql.Close()
'                End If
'            End Try

'            Return strReturn
'        End Function

'    End Class
'End Namespace


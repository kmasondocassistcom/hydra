Imports Accucentric.docAssist

Partial Class ThumbnailPreview
    Inherits System.Web.UI.Page

    Private mintVersionId As Integer
    Private mobjUser As Accucentric.docAssist.Web.Security.User
    Private mconSqlImage As SqlClient.SqlConnection
  
    Private Const MAX_COLS As Integer = 1
    Private Const MAX_ROWS As Integer = 20
    Private Const MAX_WIDTH As Integer = 162
    Private Const MAX_HEIGHT As Integer = 180

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Response.Expires = 60

        Try
            Me.mobjUser = BuildUserObject(Request.Cookies("docAssist"))
            Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
        Catch ex As Exception
            If ex.Message = "User is not logged on" Then
                Response.Redirect("default.aspx")
            End If
        End Try

        If Not Page.IsPostBack Then

            Dim ws As New docAssistImages

            Dim sb As New System.Text.StringBuilder
            sb.Append("<img id=thumb src=")
            sb.Append("." & ws.BuildAtalaThumbnail(CInt(Request.QueryString("Id")), 1, 1, Me.mobjUser.AccountId.ToString)(0))
            sb.Append(">")

            Response.Write(sb.ToString)

            System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.Empty.ToString, "document.write('" & sb.ToString & "');parent.window.sizePreview(180,180)", True)

        End If

    End Sub

End Class
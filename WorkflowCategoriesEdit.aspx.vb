Partial Class WorkflowCategoriesEdit
    Inherits System.Web.UI.Page

    Private intCategoryId As Integer = 0
    Private intMode As Integer

    Private pageTitle As New System.Web.UI.HtmlControls.HtmlGenericControl

    Private mobjUser As Accucentric.docAssist.Web.Security.User
    Private wfCategory As Accucentric.docAssist.Data.Images.Workflow

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        txtCategory.Attributes("onkeypress") = "submitForm();"

        intMode = CInt(Request.QueryString("Mode"))
        intCategoryId = CInt(Request.QueryString("ID"))

        'Security
        Try
            Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
            Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))
            Me.mobjUser.AccountId = guidAccountId
        Catch ex As Exception
            Response.Redirect("Default.aspx")
        End Try

        If Page.IsPostBack = False Then

            'Actions 
            Try

                Select Case intMode
                    Case 0
                        lblMessage.Text = "Enter a name for the new category."
                    Case 1
                        'Load category to change.
                        lblMessage.Text = "Make any changes and click save to commit."

                        Dim dtCategory As New DataTable
                        wfCategory = New Accucentric.docAssist.Data.Images.Workflow(Functions.BuildConnection(Me.mobjUser))
                        dtCategory = wfCategory.WFCategoryGetID(intCategoryId)

                        If dtCategory.Rows.Count > 0 Then
                            txtCategory.Text = dtCategory.Rows(0)("CategoryName")
                        End If
                    Case 2

                End Select

            Catch ex As Exception

            End Try
        End If

        'Mode
        'TODO: Page title

    End Sub

    Private Function LoadCategory(ByVal intCategoryId) As String

    End Function

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click

        Try
            If Len(txtCategory.Text.Trim) > 0 Then
                Select Case intMode
                    Case 0
                        wfCategory = New Accucentric.docAssist.Data.Images.Workflow(Functions.BuildConnection(Me.mobjUser))
                        Dim intResult As Integer = wfCategory.WFCategoryInsert(txtCategory.Text)
                        If intResult = 0 Then
                            lblError.Text = "Category already exists."
                            lblError.Visible = True
                        Else
                            litReload.Text = "<script language='javascript'>opener.location.reload();window.close();</script>"
                        End If
                    Case 1
                        wfCategory = New Accucentric.docAssist.Data.Images.Workflow(Functions.BuildConnection(Me.mobjUser))
                        Dim intResult As Integer = wfCategory.WFCategoryUpdate(intCategoryId, txtCategory.Text)
                        If intResult = 0 Then
                            lblError.Text = "Category already exists."
                            lblError.Visible = True
                        Else
                            litReload.Text = "<script language='javascript'>opener.location.reload();window.close();</script>"
                        End If
                    Case 2

                End Select
            Else
                lblError.Text = "Category Name is required."
                lblError.Visible = True
            End If
        Catch ex As Exception

        End Try

    End Sub
End Class

Partial Class MenuBar
    Inherits System.Web.UI.UserControl

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here

        'lnkSignOut.Visible = False

        If Not Request.Cookies("docAssist") Is Nothing Then
            If Request.Cookies("docAssist")("UserId").Trim.Length = 36 Then
                'lnkSignOut.Visible = True
            End If
        End If

    End Sub

    Private Sub lnkSignOut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        Response.Cookies("docAssist")("UserId") = ""
        Response.Cookies("docAssist")("AccountId") = ""

        Server.Transfer("Default.aspx")

    End Sub

    Private Sub lnkOptions_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)


    End Sub

    Private Sub lnkIndex_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim cookieImage As New cookieImage(Me.Page)
        Dim cookieSearch As New cookieSearch(Me.Page)
        cookieImage.Expire()
        cookieSearch.Expire()

        Response.Redirect("Index.aspx")
    End Sub

    Private Sub lnkSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    End Sub

    Private Sub lnkScan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Response.Redirect("Scan.aspx")
    End Sub
End Class

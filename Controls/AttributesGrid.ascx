<%@ Control Language="vb" AutoEventWireup="false" Codebehind="AttributesGrid.ascx.vb" Inherits="docAssistWeb.AttributesGrid" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<TABLE cellSpacing="0" cellPadding="0" border="0">
	<TR>
		<TD class="pageContent" vAlign="top" align="left">Document Type:
			<asp:dropdownlist id="ddlDocuments" Width="150px" AutoPostBack="True" runat="server"></asp:dropdownlist><asp:imagebutton id="btnAddAttribute" Visible="False" runat="server" ImageUrl="../Images/bot23.gif"></asp:imagebutton><BR>
			<asp:datagrid id="dgAttributes" Width="50px" runat="server" AutoGenerateColumns="False" ShowHeader="False">
				<Columns>
					<asp:TemplateColumn HeaderText="Attribute">
						<HeaderStyle Width="200px"></HeaderStyle>
						<ItemStyle Wrap="False"></ItemStyle>
						<ItemTemplate>
							<asp:Label id=lblAttributeId runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.AttributeId") %>'>
							</asp:Label>
							<asp:Label id=lblAttributeDataType runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.AttributeDataType") %>'>
							</asp:Label>
							<asp:Label id=lblLength runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.Length") %>'>
							</asp:Label>
							<asp:DropDownList id="ddlAttribute" runat="server" AutoPostBack="True" Width="100px"></asp:DropDownList>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Value">
						<HeaderStyle Width="200px"></HeaderStyle>
						<ItemStyle Wrap="False"></ItemStyle>
						<ItemTemplate>
							<asp:TextBox id="txtAttributeValue" runat="server" Visible="False" Width=100></asp:TextBox>
							<asp:TextBox id="txtAttributeDate" runat="server" Visible="False" Width=100></asp:TextBox>
						</ItemTemplate>
					</asp:TemplateColumn>
				</Columns>
			</asp:datagrid></TD>
	</TR>
</TABLE>

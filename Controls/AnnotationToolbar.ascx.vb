Imports Microsoft.VisualBasic
Imports System
Imports System.IO
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Web
Imports System.Web.SessionState
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls

Imports System.Runtime.Serialization
Imports System.Reflection

Imports Atalasoft.Imaging
Imports Atalasoft.Imaging.Codec
Imports Atalasoft.Imaging.Codec.Tiff
Imports Atalasoft.Imaging.Drawing
Imports Atalasoft.Imaging.ImageProcessing
Imports Atalasoft.Imaging.ImageProcessing.Document
Imports Atalasoft.Imaging.ImageProcessing.Transforms
Imports Atalasoft.Imaging.WebControls
Imports Atalasoft.Imaging.WebControls.Annotations

Imports Atalasoft.Annotate
Imports Atalasoft.Annotate.UI
Imports Atalasoft.Annotate.Renderer

Partial Class AnnotationToolbar
    Inherits System.Web.UI.UserControl

    Private mobjUser As Accucentric.docAssist.Web.Security.User
    Private mconSqlImage As SqlClient.SqlConnection
    Private mcookieSearch As cookieSearch

    Protected WithEvents tblNavigation As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents tdDelete As System.Web.UI.HtmlControls.HtmlTableCell
    Protected WithEvents HideAnnotations As System.Web.UI.HtmlControls.HtmlTableRow

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents SaveCell As System.Web.UI.HtmlControls.HtmlTableCell

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try
            Me.mobjUser = BuildUserObject(Request.Cookies("docAssist"))
            Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
            Me.mcookieSearch = New cookieSearch(Me.Page)
            Me.mconSqlImage.Open()
        Catch ex As Exception
            If ex.Message = "User is not logged on" Then
                Response.Redirect("Default.aspx")
            End If
        End Try

        If Not Page.IsPostBack Then

            Try

                Dim intVersionId As Integer = mcookieSearch.VersionID

                Dim intDocumentId As Integer = CType(Parent.FindControl("pageCount"), System.Web.UI.HtmlControls.HtmlInputHidden).Value

                Dim wfMode As Functions.WorkflowMode
                Try
                    wfMode = Session("WorkflowMode")
                Catch ex As Exception

                End Try

                HideAddPagesButton()

                Select Case Functions.DocumentSecurityLevel(Me.mcookieSearch.VersionID, Me.mobjUser, Me.mconSqlImage)
                    Case Accucentric.docAssist.Web.Security.Functions.SecurityLevel.NoAccess
                        If Not (wfMode = Functions.WorkflowMode.VIEW_ACCESS_REVIEWER_PENDING Or wfMode = Functions.WorkflowMode.ACTION_ACCESS_REVIEWER_PENDING _
                        Or wfMode = Functions.WorkflowMode.REVIEWED) Then
                            Response.Redirect("Default.aspx")
                            Exit Sub
                        End If
                    Case Accucentric.docAssist.Web.Security.Functions.SecurityLevel.Read
                        'If Not (wfMode = Functions.WorkflowMode.VIEW_ACCESS_REVIEWER_PENDING Or wfMode = Functions.WorkflowMode.ACTION_ACCESS_REVIEWER_PENDING _
                        'Or wfMode = Functions.WorkflowMode.REVIEWED) Then
                        'Hide delete from toolbar
                        Delete.Attributes("style") = "DISPLAY: none"
                        AddPages.Attributes("style") = "DISPLAY: none"
                        Split.Attributes("style") = "DISPLAY: none"
                        MovePages.Visible = False
                        imgFlip.Visible = False
                        imgRotateL.Visible = False
                        imgRotateR.Visible = False
                        imgAdd.Visible = False
                        'End If
                    Case Accucentric.docAssist.Web.Security.Functions.SecurityLevel.Change
                        'Hide delete from toolbar
                        Delete.Attributes("style") = "DISPLAY: none"
                        MovePages.Visible = True
                        ShowAddPagesButton()
                    Case Accucentric.docAssist.Web.Security.Functions.SecurityLevel.Delete
                        'Show delete button toolbar
                        Delete.Attributes("style") = "DISPLAY: block"
                        MovePages.Visible = True
                        ShowAddPagesButton()
                    Case Else
                        'Hide delete from toolbar
                        Delete.Attributes("style") = "DISPLAY: none"
                        SaveCell.Visible = False
                        Split.Attributes("style") = "DISPLAY: none"
                        MovePages.Visible = False

                End Select

                'Select Case CType(Session("FileType"), Functions.FileType)
                '    Case Functions.FileType.ATTACHMENT
                '        tdSplit.Visible = False
                '    Case Else
                'End Select

            Catch ex As Exception

                Dim conMaster As SqlClient.SqlConnection
                conMaster = Functions.BuildMasterConnection
                Functions.LogException(mobjUser.UserId.ToString, mobjUser.AccountId.ToString, "", "Annotation toolbar load: " & ex.Message, ex.StackTrace, Server.MachineName, "", Request.UrlReferrer.ToString, conMaster, True)

            End Try

            Try

                Dim viewer As Atalasoft.Imaging.WebControls.Annotations.WebAnnotationViewer = CType(sender.parent.parent.findcontrol("objViewer"), Atalasoft.Imaging.WebControls.Annotations.WebAnnotationViewer)

                'Annotation security
                Dim blnAnnotation As Boolean = False
                Dim blnRedaction As Boolean = False
                DocumentAnnotationSecurityLevel(Me.mobjUser.ImagesUserId, Me.mcookieSearch.VersionID, Me.mconSqlImage, blnAnnotation, blnRedaction)

                If blnAnnotation = False Then
                    viewer.InteractMode = WebAnnotationInteractMode.None
                    StickyNote.Attributes("style") = "DISPLAY: none"
                    DrawingTool.Attributes("style") = "DISPLAY: none"
                    TextTool.Attributes("style") = "DISPLAY: none"
                    Highlighter.Attributes("style") = "DISPLAY: none"
                    Stamp.Attributes("style") = "DISPLAY: none"
                    ClearAnnotations.Attributes("style") = "DISPLAY: none"
                    HideAnnotations.Attributes("style") = "DISPLAY: none"
                End If

                If blnRedaction = False Then
                    viewer.InteractMode = WebAnnotationInteractMode.None
                    Redaction.Attributes("style") = "DISPLAY: none"
                    SelectRedactions.Attributes("style") = "DISPLAY: none"
                    Redaction.Attributes("style") = "DISPLAY: none"
                    RedactionHeader.Attributes("style") = "DISPLAY: none"
                End If

                'Account level annotation/redaction setting
                If Functions.ModuleSetting(Functions.ModuleCode.ANNOTATION, Me.mobjUser) = False Then
                    viewer.InteractMode = WebAnnotationInteractMode.None
                    StickyNote.Attributes("style") = "DISPLAY: none"
                    DrawingTool.Attributes("style") = "DISPLAY: none"
                    TextTool.Attributes("style") = "DISPLAY: none"
                    Highlighter.Attributes("style") = "DISPLAY: none"
                    Stamp.Attributes("style") = "DISPLAY: none"
                    ClearAnnotations.Attributes("style") = "DISPLAY: none"
                    HideAnnotations.Attributes("style") = "DISPLAY: none"
                End If

                If Functions.ModuleSetting(Functions.ModuleCode.REDACTION, Me.mobjUser) = False Then
                    viewer.InteractMode = WebAnnotationInteractMode.None
                    Redaction.Attributes("style") = "DISPLAY: none"
                    SelectRedactions.Attributes("style") = "DISPLAY: none"
                    Redaction.Attributes("style") = "DISPLAY: none"
                    RedactionHeader.Attributes("style") = "DISPLAY: none"
                End If

            Catch ex As Exception

                Dim conMaster As SqlClient.SqlConnection
                conMaster = Functions.BuildMasterConnection
                Functions.LogException(mobjUser.UserId.ToString, mobjUser.AccountId.ToString, "", "Annotation toolbar load (part 2): " & ex.Message, ex.StackTrace, Server.MachineName, "", Request.UrlReferrer.ToString, conMaster, True)

            Finally

                Me.mconSqlImage.Close()
                Me.mconSqlImage.Dispose()

            End Try

        End If


    End Sub

    Private Sub imgAdd_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgAdd.Click

        Try

            Dim intPageCount As Integer = CType(Session(Me.mcookieSearch.VersionID & "VersionControl"), Accucentric.docAssist.Data.dsVersionControl).ImageDetail.Rows.Count

            Dim objDecrypt As New Encryption.Encryption

            Dim sqlConMaster As New SqlClient.SqlConnection
            sqlConMaster.ConnectionString = objDecrypt.Decrypt(ConfigurationSettings.AppSettings("Connection"))
            sqlConMaster.Open()
            Dim cmdSqlWebSvc As New SqlClient.SqlCommand("acsOptionValuebyKey", sqlConMaster)
            cmdSqlWebSvc.Parameters.Add("@OptionKey", "DocMgrWebService")
            cmdSqlWebSvc.CommandTimeout = DEFAULT_COMMAND_TIMEOUT
            cmdSqlWebSvc.CommandType = CommandType.StoredProcedure
            Dim strWebServiceUrl As String = cmdSqlWebSvc.ExecuteScalar
            sqlConMaster.Close()
            sqlConMaster.Dispose()

            Dim sbIndexer As New System.Text.StringBuilder
            sbIndexer.Append(Now().ToString & ";")
            sbIndexer.Append(Me.mobjUser.UserId.ToString & ";")
            sbIndexer.Append(Me.mobjUser.AccountId.ToString & ";")
            sbIndexer.Append(Me.mcookieSearch.VersionID & ";")
            sbIndexer.Append(intPageCount & ";")
            sbIndexer.Append(strWebServiceUrl)

            Dim objEnc As New Encryption.Encryption
            Dim strIndexerQueryString As String = objEnc.Encrypt(sbIndexer.ToString)

            'Response.Redirect("DocumentManager.aspx?File=" & strIndexerQueryString.Replace("+", "~").Replace("/", "�"))
            'CType(Me.Parent.Page, zumiControls.zumiPage).ReturnScripts.Add("")
            System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.Empty.ToString, "parent.location.replace('" & "DocumentManager.aspx?File=" & strIndexerQueryString.Replace("+", "~").Replace("/", "�") & "');", True)

        Catch ex As Exception

            Dim strError As String = ex.Message

        End Try

    End Sub

    Private Sub ShowAddPagesButton()
        If Functions.ModuleSetting(Functions.ModuleCode.SCAN_V2, Me.mobjUser) Then
            addPgs.Visible = True
            addPgs.Attributes("onclick") = "add();"
            imgAdd.Visible = False
        Else
            imgAdd.Visible = True
            addPgs.Visible = False
        End If
    End Sub

    Private Sub HideAddPagesButton()
        imgAdd.Visible = False
        addPgs.Visible = False
    End Sub

End Class

Imports Accucentric.docAssist.Data.Images

Partial Class WorkflowPane
    Inherits System.Web.UI.UserControl

    Private mintDocumentId As Integer
    Private mintUserId As Integer
    Private mobjUser As Accucentric.docAssist.Web.Security.User
    Private mconSqlImage As New SqlClient.SqlConnection

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Enum WorkflowTables
        WORKFLOWS = 1
        QUEUES = 2
    End Enum

    Public Property DocumentId() As Integer
        Get
            Return Session("mintDocumentId")
        End Get
        Set(ByVal Value As Integer)
            Session("mintDocumentId") = Value
        End Set
    End Property

    Public Property UserId() As Integer
        Get
            Return Session("mintUserId")
        End Get
        Set(ByVal Value As Integer)
            Session("mintUserId") = Value
        End Set
    End Property

    Public Sub LoadWorkflows()

        Try

            Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
            Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))
            Me.mobjUser.AccountId = guidAccountId

            Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
            Me.mconSqlImage.Open()

            Dim objWorkflow As New Workflow(Me.mconSqlImage)

            Dim ds As New DataSet

            ds = objWorkflow.WFSubmitterWorkflowsGet(Me.mobjUser.ImagesUserId, , Me.DocumentId)

            Try

                ddlWorkflow.Items.Clear()

                ddlWorkflow.Items.Add(New ListItem("Select a workflow...", "-2"))

                For Each dr As DataRow In ds.Tables(0).Rows
                    Dim intCategoryId As Integer = dr("CategoryId")
                    Dim strCategoryName As String = dr("CategoryName")
                    Dim dvFilter As New DataView(ds.Tables(1))
                    dvFilter.RowFilter = "CategoryId=" & intCategoryId

                    ddlWorkflow.Items.Add(New ListItem(strCategoryName, "OPTGROUP"))
                    For X As Integer = 0 To dvFilter.Count - 1
                        ddlWorkflow.Items.Add(New ListItem(dvFilter(X)("WorkflowName"), dvFilter(X)("WorkflowId")))
                    Next
                    ddlWorkflow.Items.Add(New ListItem(strCategoryName, "/OPTGROUP"))

                Next

                ddlWorkflow.SelectedValue = -2

            Catch ex As Exception

            End Try

        Catch ex As Exception

        Finally

            If Me.mconSqlImage.State <> ConnectionState.Closed Then
                Me.mconSqlImage.Close()
                Me.mconSqlImage.Dispose()
            End If

        End Try

    End Sub

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try

            Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
            Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))
            Me.mobjUser.AccountId = guidAccountId

        Catch ex As Exception

        End Try

    End Sub

    Private Sub ddlWorkflow_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlWorkflow.SelectedIndexChanged

        Try

            If ddlWorkflow.SelectedValue = "-2" Then
                ddlQueues.Enabled = False
                txtNotes.Enabled = False
                chkUrgent.Enabled = False
                Exit Sub
            End If

            Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
            Me.mconSqlImage.Open()

            Dim objWorkflow As New Workflow(Me.mconSqlImage)

            Dim ds As New DataSet
            ds = objWorkflow.WFSubmitterWorkflowsGet(Me.mobjUser.ImagesUserId, , Me.DocumentId)

            If ds.Tables(0).Rows.Count > 0 Then

                Dim dvQueues As New DataView(ds.Tables(WorkflowTables.QUEUES))

                dvQueues.RowFilter = "WorkflowId=" & ddlWorkflow.SelectedValue

                ddlQueues.DataTextField = "QueueCode"
                ddlQueues.DataValueField = "QueueId"
                ddlQueues.DataSource = dvQueues
                ddlQueues.DataBind()

                ddlQueues.Enabled = True
                txtNotes.Enabled = True
                chkUrgent.Enabled = True

            End If

        Catch ex As Exception

        Finally

            If Me.mconSqlImage.State <> ConnectionState.Closed Then
                Me.mconSqlImage.Close()
                Me.mconSqlImage.Dispose()
            End If

        End Try

    End Sub

End Class
Partial Class docHeader
    Inherits System.Web.UI.UserControl

    Private mobjUser As Accucentric.docAssist.Web.Security.User

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtClientWizardVersion As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtWizardVersion2 As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtCmdLine As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnResult As System.Web.UI.WebControls.ImageButton
    Protected WithEvents litUnlockImageCtrl As System.Web.UI.WebControls.Literal
    Protected WithEvents litLpkLicense As System.Web.UI.WebControls.Literal
    Protected WithEvents lblUserName As System.Web.UI.WebControls.Label
    Protected WithEvents lblDate As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        managerLink.HRef = "../DocumentManager.aspx"

        If Not Page.IsPostBack Then

            Response.Write("<!--Server:" & Server.MachineName & "-->")

            Response.Cookies(SESSION_COOKIE_KEY).Expires = Now.AddDays(7)

            btnWorkflow.Attributes("onclick") = "NavPage(""WorkflowDashboard"");"
            btnOptions.Attributes("onclick") = "NavPage(""UserAdmin"");"

            Try
                'Setup logo for each environment
                headerLogo.Src = "../Images/logoBug-blue.gif"
                If Request.Url.ToString.StartsWith("http://dev.docassist") Then
                    headerLogo.Src = "../Images/logoBug_dev-blue.gif"
                ElseIf Request.Url.ToString.StartsWith("http://stage.docassist") Then
                    headerLogo.Src = "../Images/logoBug_stage-blue.gif"
                ElseIf Request.Url.ToString.StartsWith("https://stage.docassist") Then
                    headerLogo.Src = "../Images/logoBug_stage-blue.gif"
                ElseIf Request.Url.ToString.IndexOf("localhost") > 0 Then
                    headerLogo.Src = "../Images/logoBug_dev-blue.gif"
                Else
                    headerLogo.Src = "../Images/logoBug-blue.gif"
                End If

                'Private branding
                If Not IsNothing(Session("CustomLogo")) Then
                    Dim customFileName = Session("CustomLogo")
                    headerLogo.Src = "../Images/custom/" & customFileName
                End If

                headerLogo.Attributes("onclick") = "NavPage(""Search"");"
                headerLogo.Attributes("style") = "CURSOR:hand;"

            Catch ex As Exception
                headerLogo.Src = "../Images/logoBug-blue.gif"
            End Try

            Try

                If Request.Cookies("docAssist") Is Nothing Then

                    If Not IsNothing(Request.Cookies(SESSION_COOKIE_KEY)) Then
                        If Now < Request.Cookies(SESSION_COOKIE_KEY)(SESSION_COOKIE) Then
                            Functions.SignOut(Me.Page, False)
                            Response.Redirect("Default.aspx?ErrId=" & ErrorCodes.SESSION_TIMEOUT)
                            Exit Sub
                        End If
                    Else
                        Response.Redirect("Default.aspx")
                        Exit Sub
                    End If

                End If

                If IsNothing(Request.Cookies(SINGLE_SESSION_COOKIE)) Then
                    Response.Cookies(SINGLE_SESSION_COOKIE)(SINGLE_SESSION_COOKIE_KEY) = True
                    Request.Cookies.Add(Request.Cookies(SINGLE_SESSION_COOKIE))
                End If

                'Check for session timeout
                If Not IsNothing(Request.Cookies(SESSION_COOKIE_KEY)) Then

                    If Request.Cookies(SESSION_COOKIE_KEY)(SESSION_COOKIE) = "" Then Response.Cookies(SESSION_COOKIE_KEY)(SESSION_COOKIE) = Now.AddDays(-1)

                    If IsNothing(Request.Cookies(SESSION_COOKIE_KEY)(SESSION_COOKIE)) Then
                        Dim newCookie As New HttpCookie(SESSION_COOKIE_KEY)
                        Response.Cookies(SESSION_COOKIE_KEY)(SESSION_COOKIE) = Now.AddDays(-1)
                        Request.Cookies.Add(newCookie)
                    End If

                    If Now < Request.Cookies(SESSION_COOKIE_KEY)(SESSION_COOKIE) Then

                        Response.Cookies(SESSION_COOKIE_KEY)(SESSION_COOKIE) = Now.AddMinutes(Constants.SESSION_TIMEOUT_MINUTES)
                        Response.Cookies(SESSION_COOKIE_KEY).Expires = Now.AddDays(7)

                        If Request.Cookies(SESSION_COOKIE_KEY)("RememberLogin") = True Then
                            Response.Cookies(SESSION_COOKIE_KEY)("RememberLogin") = True
                            Dim cookieDocAssist As System.Web.HttpCookie = Request.Cookies("docAssist")
                            If Not cookieDocAssist Is Nothing Then cookieDocAssist.Expires = Now().AddMinutes(SESSION_KEEP_SIGNED_IN_DURATION_MINUTES)

                            'Response.Cookies(SINGLE_SESSION_COOKIE)(SINGLE_SESSION_COOKIE_KEY) = True
                            'Response.Cookies(SINGLE_SESSION_COOKIE).Expires = Now.AddYears(-10)

                        Else
                            Response.Cookies(SESSION_COOKIE_KEY).Expires = Now.AddDays(7)
                            Response.Cookies(SESSION_COOKIE_KEY)("RememberLogin") = False
                        End If

                    Else
                        'Expired session
                        Response.Cookies(SESSION_COOKIE_KEY).Expires = Now.AddDays(7)
                        Functions.SignOut(Me.Page, False)
                        Response.Redirect("Default.aspx?ErrId=" & ErrorCodes.SESSION_TIMEOUT)
                        Exit Sub
                    End If

                    Request.Cookies.Add(Request.Cookies(SESSION_COOKIE_KEY))

                Else
                    'Session is nothing
                    Response.Cookies(SESSION_COOKIE_KEY).Expires = Now.AddDays(7)
                    Functions.SignOut(Me.Page, False)
                    Response.Redirect("Default.aspx?ErrId=" & ErrorCodes.SESSION_TIMEOUT)
                    Exit Sub
                End If

            Catch ex As Exception

            End Try

            Try

                If Request.Cookies("docAssist") Is Nothing Then
                    Response.Redirect("Default.aspx")
                    Exit Sub
                End If

                Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
                Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))
                Me.mobjUser.AccountId = guidAccountId

                'Lightbox windows
               
                If Functions.ModuleSetting(Functions.ModuleCode.SCAN_V2, Me.mobjUser) Then

                    Dim sqlConMaster As New SqlClient.SqlConnection
                    Dim scanURL, mtomURL, saveURL, buttonLink As String

                    Try

                        sqlConMaster = Functions.BuildMasterConnection()

                        Dim cmdSqlWebSvc As New SqlClient.SqlCommand("acsOptionValuebyKey", sqlConMaster)
                        cmdSqlWebSvc.Parameters.Add("@OptionKey", "CLICKONCESCANURL")
                        cmdSqlWebSvc.CommandType = CommandType.StoredProcedure
                        scanURL = cmdSqlWebSvc.ExecuteScalar
                        cmdSqlWebSvc.Parameters.Clear()
                        cmdSqlWebSvc.Parameters.Add("@OptionKey", "CLICKONCEMTOMURL")
                        mtomURL = cmdSqlWebSvc.ExecuteScalar
                        cmdSqlWebSvc.Parameters.Clear()
                        cmdSqlWebSvc.Parameters.Add("@OptionKey", "CLICKONCESAVEURL")
                        saveURL = cmdSqlWebSvc.ExecuteScalar

                        Dim qualitySetting As String
                        Dim conCompany As SqlClient.SqlConnection
                        conCompany = Functions.BuildConnection(Me.mobjUser)
                        conCompany.Open()

                        cmdSqlWebSvc = New SqlClient.SqlCommand("SMScanQualityGet", conCompany)
                        cmdSqlWebSvc.Parameters.Clear()
                        cmdSqlWebSvc.CommandType = CommandType.StoredProcedure
                        Dim qSet As New SqlClient.SqlParameter
                        qSet.ParameterName = "@Quality"
                        qSet.Direction = ParameterDirection.Output
                        qSet.DbType = DbType.Int32
                        cmdSqlWebSvc.Parameters.Add(qSet)
                        cmdSqlWebSvc.ExecuteNonQuery()
                        qualitySetting = qSet.Value

                        Dim sb As New System.Text.StringBuilder
                        sb.Append("<Param><AccountID>" & Me.mobjUser.AccountId.ToString & "</AccountID>")
                        sb.Append("<AccountDesc>" & Me.mobjUser.AccountDesc.ToString & "</AccountDesc>")
                        sb.Append("<UserID>" & Me.mobjUser.UserId.ToString & "</UserID>")
                        sb.Append("<LoginName>" & Me.mobjUser.EMailAddress.ToString & "</LoginName>")
                        sb.Append("<ScanWS>" & mtomURL & "</ScanWS>")
                        sb.Append("<SaveWS>" & saveURL & "</SaveWS>")
                        sb.Append("<Quality>" & qualitySetting & "</Quality>")
                        sb.Append("<Date>" & Now.Date.ToString & "</Date></Param>")

                        buttonLink = scanURL & "?Param=" & HttpUtility.UrlEncode(Encryption.Encryption.Encrypt(sb.ToString))

                        conCompany.Close()
                        conCompany.Dispose()

                    Catch ex As Exception

                    Finally

                        If sqlConMaster.State <> ConnectionState.Closed Then
                            sqlConMaster.Close()
                            sqlConMaster.Dispose()
                        End If

                    End Try

                    btnScan.Visible = True
                    managerLink.Visible = False
                    btnScan.HRef = buttonLink

                    'Check for expired trial
                    Dim accountStatus As AccountState = Functions.GetAccountStatus(Me.mobjUser.AccountId.ToString)
                    Select Case accountStatus
                        Case Constants.AccountState.PAID
                            upgradeLink.Visible = False
                        Case Constants.AccountState.TRIAL_ACTIVE
                            upgradeLink.HRef = ConfigurationSettings.AppSettings("UpgradeURL") & "?ref=button&m=" & accountStatus & "&id=" & Me.mobjUser.AccountId.ToString
                            upgradeLink.Visible = True
                        Case Constants.AccountState.DISABLED
                            Response.Redirect(ConfigurationSettings.AppSettings("UpgradeURL") & "?ref=disabledAccount&m=" & accountStatus & "&id=" & Me.mobjUser.AccountId.ToString, True)
                            Exit Sub
                        Case Constants.AccountState.TRIAL_EXPIRED
                            Response.Redirect(ConfigurationSettings.AppSettings("UpgradeURL") & "?ref=expiredAccount&m=" & accountStatus & "&id=" & Me.mobjUser.AccountId.ToString, True)
                            Exit Sub
                    End Select

                Else

                    
                    btnScan.Visible = False
                    managerLink.Visible = True

                    upgradeLink.Visible = False

                    If Session("blnExpired") <> True Then
                        'Check to see if password is expired
                        If Session("glbnPasswordExpired") = True Then
                            Session("blnExpired") = True
                            'Response.Redirect("PasswordExpired.aspx")
                            litTerms.Text = "<script>$(document).ready(function() { setTimeout('changePassword();', 700); });</script>"
                            Exit Sub
                        End If
                    End If

                    If Session("Terms") = "1" Then
                        litTerms.Text = "<script>$(document).ready(function() { setTimeout('showTerms();', 700); });</script>"
                    End If

                End If

                'Help
                If Me.mobjUser.EditionID = AccountEdition.SMALL_BUSINESS_EDITION Then
                    helpLink.Attributes("onclick") = "sbeHelp();"
                Else
                    helpLink.Attributes("onclick") = "window.open('help/docAssist4Help.htm','Help','height=600,resizable=yes,status=no,toolbar=no,scrollbars=yes,width=720');"
                End If

                txtUserId.Value = Me.mobjUser.UserId.ToString
                txtAccountId.Value = Me.mobjUser.AccountId.ToString
                WebServiceUrl.Value = Session("strWebServiceUrl")

                If Session("blnShowResultsTab") = False Then
                    ShowResults.Value = "0"
                Else
                    ShowResults.Value = "1"
                End If

                Dim dtAccess As New DataTable
                dtAccess = Functions.UserAccess(Functions.BuildConnection(mobjUser), mobjUser.UserId, mobjUser.AccountId)

                btnWorkflow.Visible = dtAccess.Rows(0)("Workflow")
                btnSearch.Visible = dtAccess.Rows(0)("Search")
                imgLauncher.Visible = dtAccess.Rows(0)("Index")

                If dtAccess.Rows(0)("Index") And Functions.ModuleSetting(Functions.ModuleCode.SCAN_V2, Me.mobjUser) Then
                    Dim sqlConMaster As New SqlClient.SqlConnection
                    Dim scanURL, mtomURL, saveURL, buttonLink As String

                    Try

                        sqlConMaster = Functions.BuildMasterConnection()

                        Dim cmdSqlWebSvc As New SqlClient.SqlCommand("acsOptionValuebyKey", sqlConMaster)
                        cmdSqlWebSvc.Parameters.Add("@OptionKey", "CLICKONCESCANURL")
                        cmdSqlWebSvc.CommandType = CommandType.StoredProcedure
                        scanURL = cmdSqlWebSvc.ExecuteScalar
                        cmdSqlWebSvc.Parameters.Clear()
                        cmdSqlWebSvc.Parameters.Add("@OptionKey", "CLICKONCEMTOMURL")
                        mtomURL = cmdSqlWebSvc.ExecuteScalar
                        cmdSqlWebSvc.Parameters.Clear()
                        cmdSqlWebSvc.Parameters.Add("@OptionKey", "CLICKONCESAVEURL")
                        saveURL = cmdSqlWebSvc.ExecuteScalar

                        Dim qualitySetting As String
                        Dim conCompany As SqlClient.SqlConnection
                        conCompany = Functions.BuildConnection(Me.mobjUser)
                        conCompany.Open()

                        cmdSqlWebSvc = New SqlClient.SqlCommand("SMScanQualityGet", conCompany)
                        cmdSqlWebSvc.Parameters.Clear()
                        cmdSqlWebSvc.CommandType = CommandType.StoredProcedure
                        Dim qSet As New SqlClient.SqlParameter
                        qSet.ParameterName = "@Quality"
                        qSet.Direction = ParameterDirection.Output
                        qSet.DbType = DbType.Int32
                        cmdSqlWebSvc.Parameters.Add(qSet)
                        cmdSqlWebSvc.ExecuteNonQuery()
                        qualitySetting = qSet.Value

                        Dim sb As New System.Text.StringBuilder
                        sb.Append("<Param><AccountID>" & Me.mobjUser.AccountId.ToString & "</AccountID>")
                        sb.Append("<AccountDesc>" & Me.mobjUser.AccountDesc.ToString & "</AccountDesc>")
                        sb.Append("<UserID>" & Me.mobjUser.UserId.ToString & "</UserID>")
                        sb.Append("<LoginName>" & Me.mobjUser.EMailAddress.ToString & "</LoginName>")
                        sb.Append("<ScanWS>" & mtomURL & "</ScanWS>")
                        sb.Append("<SaveWS>" & saveURL & "</SaveWS>")
                        sb.Append("<Quality>" & qualitySetting & "</Quality>")
                        sb.Append("<Date>" & Now.Date.ToString & "</Date></Param>")

                        buttonLink = scanURL & "?Param=" & HttpUtility.UrlEncode(Encryption.Encryption.Encrypt(sb.ToString))

                        conCompany.Close()
                        conCompany.Dispose()

                    Catch ex As Exception

                    Finally

                        If sqlConMaster.State <> ConnectionState.Closed Then
                            sqlConMaster.Close()
                            sqlConMaster.Dispose()
                        End If

                    End Try

                    btnScan.Visible = True
                    managerLink.Visible = False
                    btnScan.HRef = buttonLink
                Else

                    btnScan.Visible = False
                    managerLink.Visible = True

                End If

                If Functions.ModuleSetting(Functions.ModuleCode.USER_ADMIN, Me.mobjUser) = True Then
                    If mobjUser.SecAdmin Or mobjUser.SysAdmin Or mobjUser.SyncAdmin Then
                        btnOptions.Visible = True
                    Else
                        btnOptions.Visible = False
                    End If
                Else
                    btnOptions.Visible = False
                End If

                If mobjUser.SyncAdmin And Not mobjUser.SecAdmin And Not mobjUser.SysAdmin Then
                    btnOptions.Attributes("onclick") = "NavPage(""Synchs"");"
                End If

            Catch ex As Exception

                If ex.Message = "DualLogin" Then
                    Response.Cookies("docAssist")("DualLogin") = True
                    Server.Transfer("Default.aspx")
                Else
                    If ex.Message = "User is not logged on" Then
                        Response.Redirect("Default.aspx")
                    End If

                    If Not ex.Message = "Thread was being aborted." Then
                        Response.Redirect("Default.aspx")
                        Response.Flush()
                        Response.End()
                    End If

                End If

            End Try

            Try

                If Session("TrialActive") = 1 Then
                    If Session("TrialDaysRemaining") > 1 Then
                        lblTrial.Text = "Trial account will expire in " & Session("TrialDaysRemaining") & " days."
                    Else
                        lblTrial.Text = "Trial account will expire in " & Session("TrialDaysRemaining") & " day."
                    End If
                    lblTrial.Visible = True
                End If

                'Check if we need to launch the text monitor
                If Session("strIntegrationCommandLine") <> "" Then

                    'Account level integration setting
                    If Functions.ModuleSetting(Functions.ModuleCode.APP_INTEGRATION, Me.mobjUser) Then
                        litLauncher.Text = "<script language='javascript'>try{var objLaunch = new ActiveXObject('docAssistLauncher.UserCtl');objLaunch.MainCall(""" & CStr(Session("strIntegrationCommandLine")).Replace("'", "\'") & """);}catch(ex){}</script>"
                        Session("strIntegrationCommandLine") = ""
                    End If

                End If

                'Account level workflow
                If Functions.ModuleSetting(Functions.ModuleCode.WORKFLOW, Me.mobjUser) = False Then
                    btnWorkflow.Visible = False
                    btnWorkflow.Enabled = False
                End If

            Catch ex As Exception

            End Try

        End If

    End Sub

    Private Sub btnIndex_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim cookieSearch As New cookieSearch(Me.Page)
        cookieSearch.VersionID = 0
        Response.Redirect("Index.aspx")
    End Sub

    Private Sub tbnSignOut_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles tbnSignOut.Click

        Try

            Dim aCookie As HttpCookie
            Dim i As Integer
            Dim cookieName As String
            Dim limit As Integer = Request.Cookies.Count - 1
            For i = 0 To limit
                If Request.Cookies(i).Name = "docAssistSave" Then i += 1
                If Request.Cookies(i).Name = "docAssistQuickMenu" Then i += 1
                If Request.Cookies(i).Name = "docAssistThumbs" Then i += 1
                cookieName = Request.Cookies(i).Name
                aCookie = New HttpCookie(cookieName)
                aCookie.Expires = DateTime.Now.AddDays(-1)
                Response.Cookies.Add(aCookie)
            Next

            If IsNothing(Session("CustomLogoId")) Then
                Dim cookieSearch As New cookieSearch(Me.Page)
                cookieSearch.Expire()
                Response.Redirect("Default.aspx")
            Else
                Dim strLogoId As String = Session("CustomLogoId")
                Dim cookieSearch As New cookieSearch(Me.Page)
                cookieSearch.Expire()
                Response.Redirect("Default.aspx?LID=" & strLogoId)
            End If

        Catch ex As Exception

        End Try

    End Sub

    Private Function ButtonClick()
        Response.Cookies("docAssist")("ButtonClick") = "True"
    End Function

    Private Sub ImageButton1_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Response.Cookies("docAssistBack")("Results") = True
        Response.Redirect("Index.aspx")
    End Sub

    Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Session("SavedSearch") = Nothing
        Response.Redirect("Search.aspx")
    End Sub
End Class

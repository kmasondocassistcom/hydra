Partial Class PrefNav
    Inherits System.Web.UI.UserControl

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents ConfigHeader As System.Web.UI.HtmlControls.HtmlAnchor
    Protected WithEvents ConfigurationLinks As System.Web.UI.HtmlControls.HtmlTable

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try

            If Not Page.IsPostBack Then

                Dim mobjUser As New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
                Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))
                mobjUser.AccountId = guidAccountId

                Dim nodeWF As ComponentArt.Web.UI.TreeViewNode = treeNav.FindNodeById("WFAlerts")
                Dim nodeNotify As ComponentArt.Web.UI.TreeViewNode = treeNav.FindNodeById("FolderAlerts")
                nodeWF.Visible = False
                nodeNotify.Visible = False

                If Functions.ModuleSetting(Functions.ModuleCode.WORKFLOW, mobjUser) = True Then
                    nodeWF.Visible = True
                End If

                If Functions.ModuleSetting(Functions.ModuleCode.FOLDER_NOTIFICATIONS, mobjUser) = True Then
                    nodeNotify.Visible = True
                End If

                If nodeNotify.Visible = False And nodeWF.Visible = False Then treeNav.FindNodeById("Notifications").Visible = False

            End If

        Catch ex As Exception

        End Try

    End Sub

End Class
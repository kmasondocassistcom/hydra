<%@ Page Language="vb" AutoEventWireup="false" Codebehind="StapleDocs.aspx.vb" Inherits="docAssistWeb.StapleDocs"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>docAssist - Stapler</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
		<base target="_self">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server" class="PageContent">
			<table width="200">
				<tr>
					<td>
						<span class="PageContent">All indexing data will be replaced by the first document. 
							This includes document type, title, tag, description, and attributes.
							<br>
						</span>
						<div style="OVERFLOW: auto; WIDTH: 195px; HEIGHT: 160px">
							<asp:DataGrid id="dgDocs" runat="server" AutoGenerateColumns="False" Width="100%" Font-Names="Trebuchet MS"
								Font-Size="8pt">
								<HeaderStyle Font-Bold="True"></HeaderStyle>
								<Columns>
									<asp:TemplateColumn HeaderText="Document No.">
										<ItemTemplate>
											<asp:Label id=lblDocNo runat="server" Font-Bold="False" Visible="True" Text='<%# DataBinder.Eval(Container, "DataItem.ImageID") %>'>
											</asp:Label>
											<asp:Label id="lblVersionId" runat="server" Font-Bold="False" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.VersionId") %>'>
											</asp:Label>
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="Order">
										<ItemTemplate>
											<asp:LinkButton id="lnkUp" runat="server">Up</asp:LinkButton>&nbsp;
											<asp:LinkButton id="lnkDown" runat="server">Down</asp:LinkButton>
										</ItemTemplate>
									</asp:TemplateColumn>
								</Columns>
							</asp:DataGrid></div>
					</td>
				</tr>
				<tr>
					<td align="right">
						<asp:Button id="btnStaple" runat="server" Text="Staple"></asp:Button>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>

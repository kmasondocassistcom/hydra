<%@ Register TagPrefix="uc1" TagName="IndexNavigation" Src="IndexNavigation.ascx" %>
<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="Controls/MenuBar.ascx" %>
<%@ Register TagPrefix="cc2" Namespace="Atalasoft.Imaging.WebControls.Annotations"
    Assembly="Atalasoft.dotImage.WebControls.Annotations" %>
<%@ Register TagPrefix="cc1" Namespace="Atalasoft.Imaging.WebControls" Assembly="Atalasoft.dotImage.WebControls" %>
<%@ Register TagPrefix="uc1" TagName="AnnotationToolbar" Src="Controls/AnnotationToolbar.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Viewer.aspx.vb" Inherits="docAssistWeb.Viewer.Viewer"
    EnableSessionState="True" EnableViewState="True" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>docAssist</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <link href="Styles.css" type="text/css" rel="stylesheet">
    <link href="css/jquery-ui.css" type="text/css" rel="stylesheet">
    <link href="css/greybox.css" type="text/css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" media="print" href="css/print.css">
    <style type="text/css">
        .badge
        {
            overflow: hidden;
            color: #fff;
            font-family: Arial, Helvetica, sans-serif;
            font-size: 7px;
            font-weight: bold;
            background-color: #DC000B;
            padding: 1px 4px 2px 3px;
            position: absolute;
            cursor: hand;
        }
    </style>
</head>
<body id="page_body" runat="server" onresize="size();">
    <form id="Form1" method="post" runat="server">
    <div id='GB_overlay' style="display: none">
    </div>
    <div id='GB_window_viewer' style="display: none; overflow: hidden">
        <!--<div id='GB_caption' style="DISPLAY:none"></div>-->
        <img id='GB_img' style="display: none" src='./images/close-black.gif' alt='Close Viewer'></div>
    <div class="black_overlay" id="fade">
    </div>
    <div id="rotateDialog" class="rotate_popup">
        <table cellpadding="0" cellspacing="0" width="100%" height="100%">
            <tr>
                <td colspan="2">
                    <span id="InfoMsg" class="popup_caption"></span>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <img src="spacer.gif" height="5" width="1">
                </td>
            </tr>
            <tr>
                <td>
                    <img src="spacer.gif" height="1" width="25">
                </td>
                <td width="100%">
                    <asp:RadioButton ID="rdoCurrent" Checked="True" runat="server" Text="Current Page"
                        GroupName="Rotate"></asp:RadioButton>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <img src="spacer.gif" height="5" width="1">
                </td>
            </tr>
            <tr>
                <td width="25">
                </td>
                <td>
                    <asp:RadioButton ID="rdoAllPages" runat="server" Text="All Pages" GroupName="Rotate">
                    </asp:RadioButton>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <img src="spacer.gif" height="5" width="1">
                </td>
            </tr>
            <tr>
                <td colspan="2" valign="bottom" align="right" height="100%">
                    <img style="cursor: hand" src="./Images/btn_cancel.gif" border="0" onclick="hideRotate();">&nbsp;&nbsp;<img
                        style="cursor: hand" src="./Images/button_ok.gif" border="0" onclick="commitRotation();">
                </td>
            </tr>
        </table>
    </div>
    <div id="relatedDialog" class="lb_related">
        <table cellpadding="0" cellspacing="0" width="100%" height="100%">
            <tr class="titlebar">
                <td>
                    Related Documents
                </td>
                <td align="right">
                    <a href="#" onclick="hideRelated();">Close</a>
                </td>
            </tr>
            <tr>
                <td height="100%" colspan="2">
                    <span id="rel"></span>
                </td>
            </tr>
        </table>
    </div>
    <div id="svc" style="behavior: url(webservice.htc)" onresult="onWSresult()">
    </div>
    <div id="svcRotate" style="behavior: url(webservice.htc)" onresult="rotateComplete()">
    </div>
    <img id="ThumbsBar" style="left: 0px; visibility: hidden; cursor: hand; position: absolute;
        top: 124px" onclick="ShowThumbs();" src="Images/bar_vert_thumbnails.gif" border="0">
    <table height="100%" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td>
                <uc1:IndexNavigation ID="navIndex" runat="server"></uc1:IndexNavigation>
            </td>
        </tr>
        <tr>
            <td height="100%">
                <table height="100%" cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td>
                            <img height="1" src="spacer.gif" width="3">
                        </td>
                        <td id="tdThumbs" rowspan="2">
                            <table id="tblThumbnails" bordercolor="#cccccc" height="100%" cellspacing="0" cellpadding="0"
                                width="200" border="1" runat="server">
                                <tr height="14">
                                    <td style="font-weight: bold; font-size: 10pt; color: black; font-family: Arial"
                                        align="left" bgcolor="#cccccc">
                                        <img src="Images/bar_horiz_thumbnails.gif">
                                    </td>
                                    <td align="right" bgcolor="#cccccc">
                                        <a href="#">
                                            <img onclick="HideThumbs();" src="Images/collapse.gif" border="0"></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="100%" colspan="2" height="100%">
                                        <iframe id="iFrameThumbnails" style="border-right: thin; padding-right: 0px; border-top: thin;
                                            padding-left: 0px; visibility: visible; padding-bottom: 0px; border-left: thin;
                                            padding-top: 0px; border-bottom: thin" src="dummy.htm" frameborder="no" width="100%"
                                            scrolling="auto" height="100%"></iframe>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td rowspan="2">
                            <img height="1" src="spacer.gif" width="3">
                        </td>
                        <td valign="top" width="100%" rowspan="2" id="annoCell">
                            <div id="msgMagic" class="redbar">
                                Click on document values to index them. Need help? <u>Show me how</u></div>
                            <div id="indexMenu">
                                <ul id="indexAttributes" class="menuitems">
                                </ul>
                            </div>
                            <iframe height="100%" width="100%" id="frameFile" runat="server" frameborder="no">
                            </iframe>
                            <div height="100%" width="100%" id="frameEmail" runat="server" style="padding:10px;border:solid 2px lightgrey;overflow:auto;">TEST
                            </div>
                            <cc2:WebAnnotationViewer ID="objViewer" runat="server" BorderColor="Black" BorderWidth="1px"
                                BackColor="#E0E0E0" Height="100%" Width="100%" Centered="True" EnableViewState="False"
                                TileSize="1024, 1024"></cc2:WebAnnotationViewer>
                            <div id="GL" style="height: 130px;">
                                <table width="100%" class="PageContent">
                                    <tr>
                                        <td>
                                            GL/Account
                                        </td>
                                        <td>
                                            Description
                                        </td>
                                        <td>
                                            Amount
                                        </td>
                                        <td>
                                            Memo
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <select id="glacct1" style="width: 120px;" onchange="document.getElementById('desc1').value = document.getElementById('glacct1').value;">
                                                <option value="1">000-4441-0998</option>
                                                <option value="2">000-4441-0448</option>
                                                <option value="3">000-5541-0998</option>
                                            </select>
                                        </td>
                                        <td>
                                            <select id="desc1" style="width: 120px;" onchange="document.getElementById('glacct1').value = document.getElementById('desc1').value;">
                                                <option value="1">SALES</option>
                                                <option value="2">TAXES</option>
                                                <option value="3">DEPREC.</option>
                                            </select>
                                        </td>
                                        <td>
                                            <input id="amt1" type="text" style="width: 120px;" onblur="totalGL();" />
                                        </td>
                                        <td width="100%">
                                            <input type="text" style="width: 100%;" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <select id="glacct2" style="width: 120px;" onchange="document.getElementById('desc2').value = document.getElementById('glacct2').value;">
                                                <option value="1">000-4441-0998</option>
                                                <option value="2">000-4441-0448</option>
                                                <option value="3">000-5541-0998</option>
                                            </select>
                                        </td>
                                        <td>
                                            <select id="desc2" style="width: 120px;" onchange="document.getElementById('glacct2').value = document.getElementById('desc2').value;">
                                                <option value="1">SALES</option>
                                                <option value="2">TAXES</option>
                                                <option value="3">DEPREC.</option>
                                            </select>
                                        </td>
                                        <td>
                                            <input id="amt2" type="text" style="width: 120px;" onblur="totalGL();" />
                                        </td>
                                        <td width="100%">
                                            <input type="text" style="width: 100%;" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <select id="glacct3" style="width: 120px;" onchange="document.getElementById('desc3').value = document.getElementById('glacct3').value;">
                                                <option value="1">000-4441-0998</option>
                                                <option value="2">000-4441-0448</option>
                                                <option value="3">000-5541-0998</option>
                                            </select>
                                        </td>
                                        <td>
                                            <select id="desc3" style="width: 120px;" onchange="document.getElementById('glacct3').value = document.getElementById('desc3').value;">
                                                <option value="1">SALES</option>
                                                <option value="2">TAXES</option>
                                                <option value="3">DEPREC.</option>
                                            </select>
                                        </td>
                                        <td>
                                            <input id="amt3" type="text" style="width: 120px;" onblur="totalGL();" />
                                        </td>
                                        <td width="100%">
                                            <input type="text" style="width: 100%;" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <select id="glacct4" style="width: 120px;" onchange="document.getElementById('desc4').value = document.getElementById('glacct4').value;">
                                                <option value="1">000-4441-0998</option>
                                                <option value="2">000-4441-0448</option>
                                                <option value="3">000-5541-0998</option>
                                            </select>
                                        </td>
                                        <td>
                                            <select id="desc4" style="width: 120px;" onchange="document.getElementById('glacct4').value = document.getElementById('desc4').value;">
                                                <option value="1">SALES</option>
                                                <option value="2">TAXES</option>
                                                <option value="3">DEPREC.</option>
                                            </select>
                                        </td>
                                        <td>
                                            <input id="amt4" type="text" style="width: 120px;" onblur="totalGL();" />
                                        </td>
                                        <td width="100%">
                                            <input type="text" style="width: 100%;" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" align="right">
                                            <div id="glTotal">
                                                Total: $0.00</div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                        <td rowspan="2">
                            <img height="1" src="spacer.gif" width="3">
                        </td>
                        <td id="tdAnnotations" valign="top" rowspan="2" runat="server">
                            <uc1:AnnotationToolbar ID="objAnnotation" runat="server"></uc1:AnnotationToolbar>
                        </td>
                        <td rowspan="2">
                            <img height="1" src="spacer.gif" width="3">
                        </td>
                        <td id="tdAttributes" rowspan="2">
                            <table cellpadding="0" cellspacing="0" border="0" height="100%">
                                <tr height="100%">
                                    <td valign="top" height="100%">
                                        <table id="tblAttributes" bordercolor="#cccccc" cellspacing="0" cellpadding="0" width="270"
                                            border="1" height="100%">
                                            <tr height="14">
                                                <td bgcolor="#cccccc">
                                                    <img src="Images/nav_doc_attributes.gif">
                                                </td>
                                            </tr>
                                            <tr valign="top">
                                                <td valign="top" align="left">
                                                    <iframe id="frameAttributes" style="padding-right: 0px; padding-left: 0px; padding-bottom: 0px;
                                                        padding-top: 0px" src="ViewerAttributes.aspx" frameborder="no" width="100%" height="100%"
                                                        runat="server"></iframe>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr height="100%">
                                    <td>
                                        <table id="tblWorkflow" bordercolor="#cccccc" cellspacing="0" cellpadding="0" border="2"
                                            runat="server" height="205">
                                            <tr height="14">
                                                <td style="font-weight: bold; font-size: 10pt; color: black; font-family: Arial"
                                                    align="left" bgcolor="#cccccc" colspan="3">
                                                    <img src="Images/nav_wfcon.gif">
                                                </td>
                                            </tr>
                                            <tr bordercolor="#ffffff">
                                                <td style="height: 205px" valign="top" width="100%" colspan="3">
                                                    <iframe id="frameWorkflow" style="display: block" name="iWorkflow" src="WorkflowSubmit.aspx"
                                                        frameborder="no" width="100%" scrolling="no" height="100%" runat="server"></iframe>
                                                    <iframe id="frameWorkflowHistory" style="visibility: hidden" name="frameWorkflowHistory"
                                                        src="dummy.htm" frameborder="no" width="100%" scrolling="yes" height="100%" runat="server">
                                                    </iframe>
                                                </td>
                                            </tr>
                                            <tr id="WorkflowTabs">
                                                <td id="Td1" valign="baseline" bordercolor="white" align="left" colspan="1" runat="server">
                                                    <a onclick="LoadReview();" href="#">
                                                        <img id="imgReview" height="20" src="Images/imgReview.gif" width="50" border="0"></a><a
                                                            onclick="LoadHistory();" href="#"><img id="imgHistory" height="20" src="Images/imgHistoryOff.gif"
                                                                width="50" border="0"></a>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <div id="IndexItems" style="display: none" runat="server">
        <input id="GoHistory" type="hidden" value="0">
        <input id="Popup" type="hidden" value="0" name="Popup" runat="server">
        <input id="ImageType" type="hidden" name="ImageType" runat="server"><input id="VersionId"
            type="hidden" name="VersionId" runat="server">
        <input id="hostname" type="hidden" name="hostname" runat="server">

        <script language="javascript" event="OnChange" for="GoHistory">
				GoBack();
        </script>

        <asp:Literal ID="litStartService" runat="server"></asp:Literal><asp:Literal ID="litShowViewer"
            runat="server"></asp:Literal><input id="ShowWorkflow" type="hidden" name="ShowWorkflow"
                runat="server"><input id="pageNo" type="hidden" name="pageNo" runat="server">
        <input id="accountId" type="hidden" name="accountId" runat="server">
        <input id="DownloadQuality" type="hidden" name="DownloadQuality" runat="server">
        <asp:Literal ID="litHideProgress" runat="server"></asp:Literal><asp:Literal ID="litJavascript"
            runat="server"></asp:Literal><input id="mode" type="hidden" name="mode" runat="server"><input
                id="routeid" type="hidden" name="routeid" runat="server"><asp:Label ID="lblAttributeError"
                    runat="server" ForeColor="Red" Visible="False"></asp:Label>
        <asp:Literal ID="litServices" runat="server"></asp:Literal><asp:Label ID="lblDocumentId"
            runat="server" Visible="False"></asp:Label><input id="Coordinates" type="hidden">
        <input id="txtShowImage" type="hidden" name="txtShowImage" runat="server">
        <asp:TextBox ID="txtUser" runat="server" Height="0px" Width="0px"></asp:TextBox><asp:ImageButton
            ID="btnWf" runat="server" Height="0px" Width="0px"></asp:ImageButton>

        <script src="scripts/cookies.js"></script>

        <script src="scripts/jquery.js"></script>

        <script src="scripts/jquery-ui.js"></script>

        <script src="scripts/jquery.numeric.pack.js"></script>

        <script src="scripts/zeppelin.js"></script>

        <script src="scripts/magic.js"></script>

        <script src="scripts/greybox.js"></script>

        <script src="scripts/lightboxDialog.js"></script>

        <script>

            //show thumbs first time or when cookie is lost
            if (readCookie('docAssistThumbs') == '') {
                saveCookie('docAssistThumbs', 1, 365);
            }

            var showDist = false;

            var resultbln = readCookie("results");
            if (resultbln == "1") { window.history.back(); }

            var hideMode = 0;
            var hideWF = false;

            var lastPageDelete = readCookie("LastPageDeleted");
            if (lastPageDelete == "1") {
                saveCookie("LastPageDeleted", "0", 1);
                GoToPage(1);
            }

            buttons();

            //var showResults = document.all.docHeader_ShowResults.value;

            //if (showResults == "0")	{
            /*document.all.btnResult.style.display = "none";*/
            //}
            //else {
            //	document.all.btnResult.style.display = "block";
            //}	 

            var thumbSetting = readCookie('docAssistThumbs');
            if (thumbSetting == 0) {
                HideThumbs();
            }
            else {
                ShowThumbs();
            }

            function HideThumbs() {
                document.all.ThumbsBar.style.visibility = 'visible';
                document.all.ThumbsBar.style.top = '124px';
                document.all.ThumbsBar.style.left = '0px';
                document.all.ThumbsBar.style.display = 'block';
                document.all.tblThumbnails.style.display = 'none';
                hideMode = 1;
                saveCookie('docAssistThumbs', 0, 365);
            }

            function ShowThumbs() {
                try {
                    //tab on left
                    document.all.ThumbsBar.style.visibility = 'hidden';
                    document.all.tblThumbnails.style.display = 'block';
                    hideMode = 0;
                    saveCookie('docAssistThumbs', 1, 365);
                }
                catch (ex) { }
            }

            function GoBack() {
                if (Form1.GoHistory.value == "1")
                    history.back();
            }

            function buttons() {
                //document.all.docHeader_btnIndex.style.display = "INLINE";
                //document.all.docHeader_btnIndex.src = "./Images/toolbar_viewer_grey.gif";	
            }

            function LoadReview() {
                document.all.frameWorkflow.style.visibility = 'visible';
                document.all.frameWorkflow.style.display = 'block';
                document.all.frameWorkflowHistory.style.display = 'none';
                document.all.imgReview.src = 'Images/imgReview.gif';
                document.all.imgHistory.src = 'Images/imgHistoryOff.gif';
                return false;
            }

            function LoadHistory() {
                document.all.frameWorkflowHistory.style.visibility = 'visible';
                document.all.frameWorkflowHistory.style.display = 'block';
                if (document.all.frameWorkflowHistory.src != 'WorkflowHistory.aspx') { document.all.frameWorkflowHistory.src = 'WorkflowHistory.aspx'; }
                document.all.frameWorkflow.style.display = 'none';
                document.all.imgReview.src = 'Images/imgReviewOff.gif';
                document.all.imgHistory.src = 'Images/imgHistory.gif';
                return false;
            }

            function SaveHistory() {
                document.all.frameWorkflowHistory.style.visibility = 'visible';
                document.all.frameWorkflowHistory.style.display = 'block';
                document.all.frameWorkflow.style.display = 'none';
                document.all.frameWorkflowHistory.src = 'dummy.htm';
                document.all.frameWorkflowHistory.src = 'WorkflowHistory.aspx';
                document.all.WorkflowTabs.style.display = 'none';
            }

            function clickUpload() {
                if ($("#frameFile").length > 0) {
                    var frameFile = document.all.frameFile;
                    if (frameFile.src == "ViewAttachments.aspx") {
                        document.frames['frameFile'].clickSave();
                    }
                }
                /*			
                var frameFile = document.all.frameFile;
                if (frameFile.src == "ViewAttachments.aspx")
                {
                document.frames['frameFile'].clickSave();
                }
                */
            }

            function showWorkflowPane(mode, routeIdnum) {

                //                document.getElementById('mode').value = mode;
                //                document.getElementById('routeid').value = routeIdnum;
                //                document.getElementById('btnWf').click();

                switch (mode) {
                    case 0:
                        $('#tblWorkflow').hide();
                        $('#frameWorkflow').attr('src', 'dummy.htm');
                        break;
                    case 1:
                        $('#tblWorkflow, #frameWorkflow').show();
                        $('#ShowWorkflow').val('1');
                        $('#WorkflowTabs').hide();
                        $('#frameWorkflow').attr('src', 'WorkflowSubmit.aspx?Mode=1').attr('scrolling', 'no');
                        break;
                    case 10:
                        $('#ShowWorkflow').val('1');
                        $('#tblWorkflow, #frameWorkflow').show();
                        $('#WorkflowTabs').hide();
                        $('#frameWorkflow').attr('src', 'WorkflowHistory.aspx').attr('scrolling', 'auto');
                        break;
                    case 20:
                        $('#ShowWorkflow').val('1');
                        $('#tblWorkflow, #frameWorkflow').show();
                        $('#WorkflowTabs').show();
                        $('#frameWorkflow').attr('src', 'WorkflowReview.aspx?Mode=20&Route=' + routeIdnum).attr('scrolling', 'no');
                        break;
                    case 2:
                        $('#ShowWorkflow').val('1');
                        $('#tblWorkflow, #frameWorkflow').show();
                        $('#WorkflowTabs').hide();
                        $('#frameWorkflow').attr('src', 'WorkflowHistory.aspx').attr('scrolling', 'auto');
                        break;
                    case 30:
                        $('#ShowWorkflow').val('1');
                        $('#tblWorkflow, #frameWorkflow').show();
                        $('#WorkflowTabs').hide();
                        $('#frameWorkflow').attr('src', 'WorkflowHistory.aspx').attr('scrolling', 'auto');
                        break;
                    case 3:
                        $('#ShowWorkflow').val('1');
                        $('#tblWorkflow, #frameWorkflow').show();
                        $('#frameWorkflow').attr('src', 'WorkflowSubmit.aspx?Mode=1').attr('scrolling', 'no');
                        break;
                    default:
                        $('#tblWorkflow, #frameWorkflow').hide();
                        $('#frameWorkflow').attr('src', 'dummy.htm');
                        break;
                }

            }

            function hideWorkflowPane() {
                $('#ShowWorkflow').val('0');
                //document.getElementById('ShowWorkflow').value = '0';
                $('#tblWorkflow').hide();
                $('#frameWorkflow').hide();
                $('#WorkflowTabs').hide();
                //document.all.frameWorkflow.style.visibility = 'hidden';
                //                document.all.tblWorkflow.style.visibility = 'hidden';
                //                ResizeControls();
                ////                var wfTabs = document.getElementById('WorkflowTabs');
                //                if (wfTabs) { wfTabs.style.display = 'none'; }
                //            
            }

            function loadDistribution(id) {
                if (document.getElementById('frmDistribution').src == 'dummy.htm') {
                    document.getElementById('frmDistribution').src = 'Distribution.aspx?ID=' + id;
                }
            }

            function showDistribution(fromButton) {

                showDist = true;

                var frmDistribution = document.getElementById('frmDistribution');
                var frameImage = document.getElementById('frameImage');

                //move up viewer
                if (parent.frames["frameAttributes"].getGl() == true) {
                    if (frameImage != undefined) {
                        var viewerHeight = (frameImage.style.height.replace('px', '') * 1);
                        frameImage.style.height = viewerHeight - 250;
                    }

                    //position distribution div
                    var distTop = (frameImage.style.top.replace('px', '') * 1) + (frameImage.style.height.replace('px', '') * 1);
                    frmDistribution.style.top = distTop + 2;
                    frmDistribution.style.left = frameImage.style.left;
                    frmDistribution.style.width = (frameImage.style.width.replace('px', '') * 1) - 2;
                    frmDistribution.style.height = 245;
                    frmDistribution.style.display = 'block';
                    parent.frames["frameAttributes"].setGlEnabled(false);
                }
            }

            function hideDistribution() {
                showDist = false;

                var frmDistribution = document.getElementById('frmDistribution');
                frmDistribution.style.display = 'none';

                //move up viewer
                var frameImage = document.getElementById('frameImage');
                if (frameImage != undefined) {
                    var viewerHeight = (frameImage.style.height.replace('px', '') * 1);
                    frameImage.style.height = viewerHeight + 250;
                    parent.frames["frameAttributes"].setGlEnabled(true);
                }

                ResizeControls();

            }

            function reloadThumbs() {
                parent.frames["iFrameThumbnails"].reloadThumbs();
            }

            function pinWorkflowPane() {
                hideWF = true;
                document.all.tblWorkflow.style.display = 'none';
                document.getElementById('ShowWorkflow').value = '0';
                var wfBar = document.getElementById('WfBar');
                wfBar.style.display = 'block';
                positionWf();
                ResizeControls();
            }

            function unpinWorkflowPane() {
                hideWF = false;
                document.all.tblWorkflow.style.display = 'block';
                document.getElementById('ShowWorkflow').value = '1';
                ResizeControls();
                var wfBar = document.getElementById('WfBar');
                wfBar.style.display = 'none';
            }

            function positionWf() {
                myWidth = document.body.clientWidth;
                myHeight = document.body.clientHeight;
                var wfBar = document.getElementById('WfBar');
                wfBar.style.top = myHeight - 38;
                wfBar.style.left = myWidth - 136;
            }

            function ResizeControls() { }

            function PageContentScroll(setting) {
                document.all.frameImage.scrolling = setting;
                document.all.frameImage.frameBorder = setting;
            }

            function reloadActivate() {
                removeFlag = false;
            }


            function showRotate(msg) {
                //var fade = document.getElementById('fade');
                var rotateDialog = document.getElementById('rotateDialog');

                //fade.style.display='block';
                //fade.style.width = '100%';
                //fade.style.height = '100%';

                rotateDialog.style.width = '350px';
                rotateDialog.style.height = '135px';
                var nXpos = (document.body.clientWidth - rotateDialog.style.width.replace('px', '')) / 2;
                var nYpos = (document.body.clientHeight - rotateDialog.style.height.replace('px', '')) / 2;
                rotateDialog.style.left = nXpos;
                rotateDialog.style.top = nYpos;

                document.getElementById('rotateDialog').style.display = 'block';
                document.getElementById('InfoMsg').innerHTML = msg;
            }

            function hideRotate() {
                try {
                    var rotateDialog = document.getElementById('rotateDialog');
                    rotateDialog.style.display = 'none';
                    //var fade = document.getElementById('fade');
                    //fade.style.display='none';
                } catch (Ex)
		{ }
            }

            function CheckNotDirty() {
                /*
                var dirty = document.all.hiddenDirty.value;
                if((dirty == "1") && (document.getElementById('ReadOnlyUser').value == 0))
                {
                event.returnValue = 'All your changes will be lost. You must save them before leaving the page.';
                }
                */
            }
        </script>

        <input id="docId" type="hidden" name="docId" runat="server">
        <input id="pageCount" type="hidden" name="pageCount" runat="server">
        <input id="pageNumber" type="hidden" value="1" runat="server" name="pageNumber">
    </div>
    <!-- Context Menu -->
    <div id="AnnoContextMenu" class="PageContent" style="display: none">
        <table width="40" style="border-right: #000000 thin solid; border-top: #000000 thin solid;
            border-left: #000000 thin solid; border-bottom: #000000 thin solid; background-color: white">
            <tr>
                <td>
                    <a href="#" onclick="return CallFromContextMenu(DeleteAnno);">Delete</a>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>

Partial Class AdminWorkflowStructure
    Inherits System.Web.UI.Page


#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblStructure As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private mobjUser As Accucentric.docAssist.Web.Security.User

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim sqlConn As New SqlClient.SqlConnection

        Try

            If Request.Cookies("docAssist") Is Nothing Then Response.Redirect("Default.aspx")
            If Request.Cookies("docAssist") Is Nothing Then
                Response.Redirect("Default.aspx")
                Exit Sub
            End If

            Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
            Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))

            'This is the parameter that points to the current account.
            'TODO: Test this with multi account login.
            Me.mobjUser.AccountId = guidAccountId

            sqlConn = Functions.BuildConnection(Me.mobjUser)
            sqlConn.Open()

            Dim sqlCmd As New SqlClient.SqlCommand("acsAdminDisplayWFStructure")
            sqlCmd.Connection = sqlConn
            sqlCmd.CommandType = CommandType.StoredProcedure
            sqlCmd.CommandTimeout = DEFAULT_COMMAND_TIMEOUT
            Dim daSql As New SqlClient.SqlDataAdapter(sqlCmd)
            Dim dsStructure As New DataSet
            daSql.Fill(dsStructure)

            sqlConn.Close()
            sqlConn.Dispose()

            Dim sbTable As New System.Text.StringBuilder

            sbTable.Append("<TABLE border=""0"" width=""100%"" height=""100%"" cellpadding=""0"" cellspacing=""0"" style=""FONT-SIZE: 8.25pt; FONT-FAMILY: Verdana""><TR><TD style=""BACKGROUND-COLOR: #959394""><b>Role</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</TD><TD width=""100%"" style=""BACKGROUND-COLOR: #959394""><b>Description</b></TD></TR>")

            Dim dr As DataRow
            Dim strLastRole As String

            Dim intCounter As Integer = 0

            For Each dr In dsStructure.Tables(0).Rows

                If dr("RoleDescription") <> strLastRole Then
                    If intCounter > 0 Then sbTable.Append("<TR height=""12""><TD></TD></TR>")
                    sbTable.Append("<TR><TD style=""BACKGROUND-COLOR: #cccccc"">" & dr("RoleDescription") & "</TD><TD style=""BACKGROUND-COLOR: #cccccc"">" & dr("RouteDescription") & "</TD></TR>")
                Else
                    sbTable.Append("<TR><TD></TD><TD>" & dr("RouteDescription") & "</TD></TR>")
                End If
                strLastRole = dr("RoleDescription")
                intCounter += 1
            Next

            sbTable.Append("<tr><td><BR><BR></td></tr>")
            sbTable.Append("<TR><TD style=""BACKGROUND-COLOR: #959394""><b>Route</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</TD><TD width=""100%"" style=""BACKGROUND-COLOR: #959394""><b>Users</b></TD></TR>")


            Dim dr2 As DataRow
            Dim strLast As String
            Dim intRowCounter As Integer = 0
            For Each dr2 In dsStructure.Tables(1).Rows

                If dr2("RouteDescription") <> strLast Then
                    If intRowCounter > 0 Then sbTable.Append("<TR height=""12""><TD></TD></TR>")
                    sbTable.Append("<TR><TD style=""BACKGROUND-COLOR: #cccccc"">" & dr2("RouteDescription") & "</TD><TD style=""BACKGROUND-COLOR: #cccccc"">" & dr2("UserName") & "</TD></TR>")
                Else
                    sbTable.Append("<TR><TD></TD><TD>" & dr2("UserName") & "</TD></TR>")

                End If
                strLast = dr2("RouteDescription")
                intRowCounter += 1
            Next

            sbTable.Append("<TR height=""100%""><TD height=""100%""></TD></TABLE>")

            lblTable.Text = sbTable.ToString

        Catch ex As Exception
            If sqlConn.State <> ConnectionState.Closed Then sqlConn.Close()
            sqlConn.Dispose()
        End Try





    End Sub

End Class

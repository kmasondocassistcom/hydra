<%@ Register TagPrefix="uc1" TagName="AdminNav" Src="Controls/AdminNav.ascx" %>
<%@ Register TagPrefix="uc1" TagName="TopRight" Src="Controls/TopRight.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="UserAdmin.aspx.vb" Inherits="docAssistWeb._UserAdmin"%>
<%@ Register TagPrefix="uc1" TagName="docHeader" Src="Controls/docHeader.ascx" %>
<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="Controls/MenuBar.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<TITLE>docAssist - User Admin</TITLE>
		<META content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<META content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<META content="JavaScript" name="vs_defaultClientScript">
		<META content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<META http-equiv="cache-control" content="no-cache">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<BODY>
		<FORM id="Form1" method="post" runat="server">
			<DIV id="svcUser" style="BEHAVIOR: url(webservice.htc)" onresult="onWSUserResult()"></DIV>
			<INPUT id="hostname" type="hidden" runat="server">
			<TABLE height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD width="100%" colSpan="3"><UC1:DOCHEADER id="docHeader" runat="server"></UC1:DOCHEADER></TD>
				</TR>
				<TR vAlign="middle" height="100%">
					<TD class="Navigation1" style="BORDER-RIGHT: #cccccc thin solid; WIDTH: 171px" vAlign="top"
						height="100%"><UC1:ADMINNAV id="AdminNav1" runat="server"></UC1:ADMINNAV><IMG height="1" src="dummy.gif" width="120"></TD>
					<TD class="PageContent" vAlign="top" align="center" width="100%" height="100%">
						<P><BR>
							<table cellSpacing="0" cellPadding="0" width="100%" border="0">
								<tr align="right">
									<TD></TD>
									<td width="50%"><A class="Link" style="TEXT-ALIGN: right" href="javascript:SetUser(0);"><IMG id="imgAddUser" alt="Add User" src="Images/add_user.gif" align="left" border="0"
												runat="server"></A></td>
									<TD></TD>
								</tr>
								<tr>
									<TD>
										<P><asp:image id="Image1" runat="server" Height="1px" Width="20px"></asp:image></P>
										<P>&nbsp;</P>
									</TD>
									<td width="100%"><IMG height="1" src="dummy.gif" width="20"><BR>
										<ASP:DATAGRID id="dgUsers" runat="server" Width="100%" AUTOGENERATECOLUMNS="False" CSSCLASS="transparent">
											<AlternatingItemStyle CssClass="TableAlternateItem"></AlternatingItemStyle>
											<HeaderStyle CssClass="TableHeader"></HeaderStyle>
											<Columns>
												<asp:TemplateColumn>
													<HeaderStyle Width="75px"></HeaderStyle>
													<ItemTemplate>
														<asp:Label id=lblWebUserId runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.WebUserId") %>'>
														</asp:Label>
														<asp:Label id=lblImageUserId runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.ImageUserId") %>'>
														</asp:Label>&nbsp;
														<asp:HyperLink id=lnkShowUser runat="server" NavigateUrl='<%# "javascript:SetUser(" + CStr(DataBinder.Eval(Container, "DataItem.ImageUserId")) + ");" %>' ToolTip="Edit User">
															<img src="Images/editIcon.gif" border="0"></asp:HyperLink>
														<asp:HyperLink id="lnkCopyUser" runat="server" NavigateUrl='<%# "javascript:CopyUser(" + CStr(DataBinder.Eval(Container, "DataItem.ImageUserId")) + ");" %>' ToolTip="Copy User">
															<img src="Images/listingcopy.gif" border="0"></asp:HyperLink>&nbsp;
														<ASP:LINKBUTTON id="lnkRemove" runat="server" ToolTip="Remove User" CommandName="Delete">
															<img src="Images/trash.gif" border="0"></ASP:LINKBUTTON>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:BoundColumn DataField="UserName" ReadOnly="True" HeaderText="User Name"></asp:BoundColumn>
												<asp:BoundColumn DataField="EMailAddress" HeaderText="E-Mail Address"></asp:BoundColumn>
												<asp:BoundColumn DataField="UserType" HeaderText="User Type"></asp:BoundColumn>
												<asp:TemplateColumn HeaderText="Enabled">
													<HeaderStyle HorizontalAlign="Center" Width="72px"></HeaderStyle>
													<ItemStyle HorizontalAlign="Center" VerticalAlign="Top"></ItemStyle>
													<ItemTemplate>
														<asp:CheckBox id=chkEnabled runat="server" Checked='<%# DataBinder.Eval(Container, "DataItem.Enabled") %>' Enabled="False">
														</asp:CheckBox>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="Security Admin">
													<HeaderStyle HorizontalAlign="Center" Width="72px"></HeaderStyle>
													<ItemStyle HorizontalAlign="Center"></ItemStyle>
													<ItemTemplate>
														<asp:CheckBox id=chkSecAdmin runat="server" Checked='<%# DataBinder.Eval(Container, "DataItem.SecAdmin") %>' Enabled="False">
														</asp:CheckBox>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="System Admin">
													<HeaderStyle HorizontalAlign="Center" Width="72px"></HeaderStyle>
													<ItemStyle HorizontalAlign="Center"></ItemStyle>
													<ItemTemplate>
														<asp:CheckBox id="Checkbox1" runat="server" Enabled="False" Checked='<%# DataBinder.Eval(Container, "DataItem.SysAdmin") %>'>
														</asp:CheckBox>
													</ItemTemplate>
												</asp:TemplateColumn>
											</Columns>
										</ASP:DATAGRID><ASP:LABEL id="lblErrorMessage" runat="server" ForeColor="Red" Visible="False"></ASP:LABEL></td>
									<TD><asp:image id="Image2" runat="server" Height="1px" Width="20px"></asp:image></TD>
								</tr>
							</table>
							<ASP:LITERAL id="litScripts" runat="server"></ASP:LITERAL></P>
					</TD>
				</TR>
				<tr>
					<td colSpan="2"><uc1:topright id="docFooter" runat="server"></uc1:topright></td>
				</tr>
			</TABLE>
			<script language="javascript">

	var smbiz = false;
	
	if (document.all.docHeader_btnSearch != undefined) { document.all.docHeader_btnSearch.src = "./Images/toolbar_search_white.gif"; }
	if (document.all.docHeader_btnIndex != undefined) { document.all.docHeader_btnIndex.src = "./Images/toolbar_index_white.gif";}
	if (document.all.docHeader_btnOptions != undefined) { document.all.docHeader_btnOptions.src = "./Images/toolbar_options_grey.gif"; }
	if (document.all.docHeader_btnWorkflow != undefined){ document.all.docHeader_btnWorkflow.src = "./Images/toolbar_workflow_white.gif"; }

	function SetUser(UserId){
			var d = new Date();
			var time = d.getHours()+d.getMinutes()+d.getSeconds()+d.getMilliseconds();
			if (smbiz){
				var settings = "help:no;status:yes;resizable:no;dialogHeight=215px;dialogWidth=666px";
			}else{
				var settings = "help:no;status:yes;resizable:no;dialogHeight=432px;dialogWidth=666px";
			}
			var returnValue = window.showModalDialog('UserEdit.aspx?Id='+UserId+'&Mode=NewEdit&Time='+time,null,settings);
			if (returnValue == "1") { window.location.replace('UserAdmin.aspx'); }
	}

	function CopyUser(UserId){
		var d = new Date();
		var time = d.getHours()+d.getMinutes()+d.getSeconds()+d.getMilliseconds();
		var settings = "help:no;status:yes;resizable:no;dialogHeight=432px;dialogWidth=666px";
		var returnValue = window.showModalDialog('UserEdit.aspx?Id='+UserId+'&Mode=Copy&Time='+time,null,settings);
		if (returnValue == "1") { window.location.replace('UserAdmin.aspx'); }
	}

			</script>
		</FORM>
	</BODY>
</HTML>

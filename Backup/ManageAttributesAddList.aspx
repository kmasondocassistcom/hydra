<%@ Register TagPrefix="uc1" TagName="TopRight" Src="Controls/TopRight.ascx" %>
<%@ Register TagPrefix="uc1" TagName="docHeader" Src="Controls/docHeader.ascx" %>
<%@ Register TagPrefix="uc1" TagName="AdminNav" Src="Controls/AdminNav.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ManageAttributesAddList.aspx.vb" Inherits="docAssistWeb.ManageAttributesAddList"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>docAssist - Manage Attributes</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<TABLE height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TBODY>
					<TR>
						<TD width="100%" colSpan="5">
							<uc1:docHeader id="docHeader" runat="server"></uc1:docHeader>
						</TD>
					</TR>
					<TR vAlign="middle" height="100%">
						<TD class="Navigation1" height="100%" style="BORDER-RIGHT: #cccccc thin solid" vAlign="top">
							<uc1:AdminNav id="AdminNav2" runat="server"></uc1:AdminNav><IMG height="1" src="dummy.gif" width="120"></TD>
						<TD vAlign="top" align="center" height="100%"><BR>
							<table cellSpacing="0" cellPadding="0" width="100%" border="0">
								<tr>
									<td><IMG height="1" src="dummy.gif" width="35"><IMG src="Images/manage_attributes.gif"></td>
								</tr>
							</table>
							<table height="100%" cellSpacing="0" cellPadding="0" width="100%">
					</TR>
					<tr height="50%">
					</tr>
					<tr height="15" width="100%">
						<td style="HEIGHT: 17px"></td>
						<td style="HEIGHT: 17px"></td>
						<td style="WIDTH: 456px; HEIGHT: 17px">
							<P><asp:label id="lblAttributeName" runat="server" Height="24px" Width="480px" Font-Bold="True"
									Font-Names="Verdana" Font-Size="8.25pt"></asp:label>
								<asp:label id="Label1" runat="server" Font-Size="8.25pt" Font-Names="Verdana" Width="480px"
									Height="24px">Please enter the list of items to display when this attribute is selected.</asp:label></P>
						</td>
						<td style="HEIGHT: 17px"></td>
					</tr>
					<tr height="15" width="100%">
						<td style="HEIGHT: 26px"></td>
						<td style="WIDTH: 72px; HEIGHT: 26px" align="left"></td>
						<TD style="WIDTH: 456px; HEIGHT: 26px">
							<P>
								<asp:textbox id="txtListAdd" runat="server" Height="20px" Width="270px" Font-Names="Verdana"
									Font-Size="8.25pt"></asp:textbox>
								<asp:ImageButton id="btnAdd" runat="server" ImageUrl="Images/add_item.gif"></asp:ImageButton></P>
						</TD>
						<TD width="50%" style="HEIGHT: 26px"></TD>
					<TR height="15" width="100%">
						<td style="HEIGHT: 21px" width="50%"></td>
						<td style="WIDTH: 72px; HEIGHT: 21px" align="left">&nbsp;
						</td>
						<td style="WIDTH: 456px; HEIGHT: 21px">&nbsp;</td>
						<td style="HEIGHT: 21px" width="50%"></td>
					</TR>
					<tr height="15" width="100%">
						<td style="WIDTH: 393px" width="393"></td>
						<td style="WIDTH: 72px; HEIGHT: 17px">
							<P align="left">&nbsp;&nbsp;
							</P>
						</td>
						<td style="WIDTH: 456px; HEIGHT: 17px" vAlign="top"><asp:listbox id="lstItems" runat="server" Height="200px" Width="440px" Font-Names="Verdana" Font-Size="8.25pt"></asp:listbox></td>
						<td width="50%">
							<asp:ImageButton id="btnMoveUp" runat="server" ImageUrl="Images/move_up.gif"></asp:ImageButton><BR>
							<BR>
							<asp:ImageButton id="btnRemove" runat="server" ImageUrl="Images/remove_item.gif"></asp:ImageButton><BR>
							<BR>
							<asp:ImageButton id="btnMoveDown" runat="server" ImageUrl="Images/move_down.gif"></asp:ImageButton></td>
					</tr>
					<tr height="15" width="100%">
						<td style="WIDTH: 393px" width="393"></td>
						<td style="WIDTH: 72px" vAlign="middle">
							<P align="left">&nbsp;&nbsp;</P>
						</td>
						<td align="left" style="WIDTH: 456px"><asp:label id="lblError" runat="server" Height="15px" Width="384px" ForeColor="Red" Font-Names="Verdana"
								Font-Size="8.25pt"></asp:label></td>
						<td width="50%"></td>
					</tr>
					<tr height="15" width="100%">
						<td style="WIDTH: 393px" width="395"></td>
						<td style="WIDTH: 72px; HEIGHT: 17px"><BR>
						</td>
						<td style="WIDTH: 456px; HEIGHT: 17px" align="right"><asp:imagebutton id="btnCancel" runat="server" ImageUrl="Images/btn_canceladmin.gif"></asp:imagebutton>&nbsp;<asp:imagebutton id="btnFinish" runat="server" ImageUrl="Images/btn_finish.gif"></asp:imagebutton></td>
						<td width="50%"></td>
					</tr>
					<tr height="15" width="100%">
						<td style="WIDTH: 395px" width="395"></td>
						<td style="WIDTH: 72px"><BR>
							<P>&nbsp;</P>
						</td>
						<td align="right" style="WIDTH: 456px"></td>
						<td width="50"></td>
					</tr>
					<tr height="100%">
						<td></td>
					</tr>
				</tr>
			</TABLE>
			<P><BR>
				<BR>
			</P>
			</TD>
			<TD class="RightContent" height="100%">&nbsp;</TD>
			</TR>
			<TR>
				<TD class="PageFooter" vAlign="bottom" bgColor="#cccccc" colSpan="3">
					<uc1:TopRight id="docFooter" runat="server"></uc1:TopRight></TD>
			</TR>
			<TR height="1">
				<TD style="BACKGROUND-COLOR: #959394" width="20"><IMG height="1" src="images/spacer.gif" width="20"></TD>
				<TD style="BACKGROUND-COLOR: #959394" width="100%"><IMG height="1" src="images/spacer.gif" width="100%"></TD>
				<TD style="BACKGROUND-COLOR: #959394" width="20"><IMG height="1" src="images/spacer.gif" width="20"></TD>
			</TR>
			</TBODY></TABLE>
			<script language="javascript">
				function submitForm()
				{
					if (event.keyCode == 13)
					{
						event.cancelBubble = true;
						event.returnValue = false;
						document.all.btnAdd.click();
						document.all.txtListAdd.focus();
					}
				}
			</script>
		</form>
	</body>
</HTML>

Imports ComponentArt.Web.UI
Imports Accucentric.docAssist
Imports Accucentric.docAssist.Data.Images

Partial Class MoveFolder
    Inherits System.Web.UI.Page
    
    Dim mconSqlImage As SqlClient.SqlConnection
    Dim mobjUser As Accucentric.docAssist.Web.Security.User
    Dim contextMenus As New ArrayList

    Protected WithEvents litEdit As System.Web.UI.WebControls.Literal

    Private Const FAVORITE_SPLIT = "�"

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Response.Expires = 0
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        Try

            Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
            Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))
            Me.mobjUser.AccountId = guidAccountId
            Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)

            BuildTree()

        Catch ex As Exception

            Response.Write(ex.Message)

        Finally
            If Me.mconSqlImage.State <> ConnectionState.Closed Then
                Me.mconSqlImage.Close()
                Me.mconSqlImage.Dispose()
            End If
        End Try

    End Sub

    Private Sub BuildTree()

        If Not Page.IsPostBack Then

            Dim firstDoc As TreeViewNode

            Dim objFolders As New Folders(Me.mconSqlImage, False)
            Dim ds As New DataSet
            ds = objFolders.FoldersGet(Me.mobjUser.ImagesUserId)

            'Favorites
            'Dim FavoritesNode As New TreeViewNode
            'FavoritesNode.Text = " My Favorites"
            'FavoritesNode.ID = "MYFAVNODE"
            'FavoritesNode.ImageUrl = IMAGE_FAVORITES
            'FavoritesNode.Expanded = True
            'FavoritesNode.ShowCheckBox = False
            'treeSearch.Nodes.Add(FavoritesNode)

            'Dim strFavMenuName As String = "FavoritesMenu"

            'Dim cmFav As New RadTreeViewContextMenu.ContextMenu
            'cmFav.Name = strFavMenuName
            'cmFav.Width = 150
            'contextMenus.Add(cmFav)

            'Dim mnuNewFav As New RadTreeViewContextMenu.ContextMenuItem(RENAME_FAVORITE)
            'mnuNewFav.Image = IMAGE_RENAME
            'mnuNewFav.PostBack = False
            'cmFav.Items.Add(mnuNewFav)

            'mnuNewFav = New RadTreeViewContextMenu.ContextMenuItem(REMOVE_FAVORITE)
            'mnuNewFav.Image = IMAGE_REMOVE
            'mnuNewFav.PostBack = True
            'cmFav.Items.Add(mnuNewFav)

            'For Each drFavorite As DataRow In ds.Tables(TreeTables.FAVORITES).Rows

            '    Dim favFolder As New TreeViewNode
            '    favFolder.Text = drFavorite("FavoriteName")
            '    favFolder.ID = "F" & drFavorite("FavoriteId") & FAVORITE_SPLIT & drFavorite("FolderID")
            '    favFolder.ImageUrl = IMAGE_FOLDER
            '    favFolder.ToolTip = drFavorite("FolderPath")
            '    favFolder.AutoPostBackOnSelect = True
            '    'favFolder.PostBack = True
            '    'favFolder.Checkable = False

            '    'Dim favParentNode As New RadTreeNode
            '    'favParentNode = radTree.FindNodeByValue("MYFAVNODE")
            '    FavoritesNode.Nodes.Add(favFolder)

            'Next

            '    'Categories
            '    'Dim catNode As New RadTreeNode("Document Categories", "CATNODE")
            '    'catNode.ImageUrl = IMAGE_FAVORITES_ADD
            '    'catNode.Expanded = True
            '    'catNode.Checkable = False
            '    'radTree.Nodes.Add(catNode)

            '    'For Each drCategory As DataRow In ds.Tables(TreeTables.CATEGORIES).Rows

            '    '    Dim catFolder As New RadTreeNode(drCategory("FolderName"), "C" & drCategory("FolderId"))
            '    '    catFolder.ImageUrl = IMAGE_FOLDER
            '    '    catFolder.PostBack = True
            '    '    catFolder.Checkable = False

            '    '    Dim catParentNode As New RadTreeNode
            '    '    catParentNode = radTree.FindNodeByValue("CATNODE")
            '    '    catParentNode.Nodes.Add(catFolder)

            '    'Next

            'If ds.Tables(TreeTables.DOCUMENTS).Rows.Count > 0 Then

            '    'Documents
            '    Dim docNode As New TreeViewNode
            '    docNode.Text = " Document Types"
            '    docNode.ID = "DOCNODE"

            '    docNode.ImageUrl = IMAGE_DOCUMENT_TYPES
            '    docNode.Expanded = True
            '    docNode.ShowCheckBox = False
            '    'docNode.PostBack = True
            '    'docNode.EditEnabled = False
            '    treeSearch.Nodes.Add(docNode)

            '    'All documents node
            '    Dim docAllDocuments As New TreeViewNode
            '    docAllDocuments.Text = "All Documents"
            '    docAllDocuments.ID = "AllDocs"
            '    docAllDocuments.ShowCheckBox = True
            '    docAllDocuments.AutoPostBackOnSelect = True
            '    docAllDocuments.AutoPostBackOnCheckChanged = True
            '    docAllDocuments.Checked = False
            '    docAllDocuments.EditingEnabled = False
            '    docNode.Nodes.Add(docAllDocuments)

            '    Dim X As Integer = 1

            '    For Each drDocument As DataRow In ds.Tables(TreeTables.DOCUMENTS).Rows

            '        If X = 1 Then Session("firstDocumentNodeValue") = "D" & drDocument("DocumentId")

            '        Dim docFolder As New TreeViewNode '(drDocument("DocumentName"), "D" & drDocument("DocumentId"))
            '        docFolder.Text = drDocument("DocumentName")
            '        docFolder.ID = "D" & drDocument("DocumentId")
            '        docFolder.ImageUrl = IMAGE_DOCUMENT
            '        docFolder.AutoPostBackOnSelect = True
            '        docFolder.ShowCheckBox = True
            '        docFolder.AutoPostBackOnCheckChanged = True
            '        docFolder.Checked = False
            '        docFolder.EditingEnabled = False

            '        If X = 1 Then
            '            docFolder.Checked = True
            '            firstDoc = docFolder
            '        End If

            '        docNode.Nodes.Add(docFolder)

            '        X += 1

            '    Next

            'End If

            ''Smart Folders
            'Dim sfNode As New TreeViewNode
            'sfNode.Text = "SmartFolders"
            'sfNode.ID = "SmartFolders"
            'sfNode.ImageUrl = IMAGE_SMARTFOLDER
            'sfNode.Expanded = True
            'sfNode.AutoPostBackOnSelect = True
            'treeSearch.Nodes.Add(sfNode)

            'Dim privateNode As New TreeViewNode
            'privateNode.Text = "Private"
            'privateNode.ID = "PrivateSmartFolders"
            'privateNode.Expanded = True
            'privateNode.ImageUrl = IMAGE_PRIVATE
            'privateNode.AutoPostBackOnSelect = True
            'sfNode.Nodes.Add(privateNode)

            'Dim publicNode As New TreeViewNode
            'publicNode.Text = "Public"
            'publicNode.ID = "PublicSmartFolders"
            'publicNode.Expanded = True
            'publicNode.ImageUrl = IMAGE_PUBLIC
            'publicNode.AutoPostBackOnSelect = True
            'sfNode.Nodes.Add(publicNode)


            'For Each drSmartFolder As DataRow In ds.Tables(TreeTables.SMART_FOLDERS).Rows

            '    'Dim strSmartFolderMenuName As String = System.Guid.NewGuid.ToString.Replace("-", "")

            '    'Dim cmSmartFolders As New RadTreeViewContextMenu.ContextMenu
            '    'cmSmartFolders.Name = strSmartFolderMenuName
            '    'cmSmartFolders.Width = 150
            '    'contextMenus.Add(cmSmartFolders)

            '    'If drSmartFolder("CanManage") = 1 Then
            '    '    Dim mnuSmartFolder As New RadTreeViewContextMenu.ContextMenuItem(LOAD_SMARTFOLDER)
            '    '    mnuSmartFolder.Image = IMAGE_SMARTFOLDER_LOAD
            '    '    mnuSmartFolder.PostBack = True
            '    '    cmSmartFolders.Items.Add(mnuSmartFolder)

            '    '    mnuSmartFolder = New RadTreeViewContextMenu.ContextMenuItem(REMOVE_SMARTFOLDER)
            '    '    mnuSmartFolder.Image = IMAGE_REMOVE
            '    '    mnuSmartFolder.PostBack = True
            '    '    cmSmartFolders.Items.Add(mnuSmartFolder)
            '    'End If

            '    Dim smFolder As New TreeViewNode '(drSmartFolder("FolderName"), "Q" & drSmartFolder("FolderId"))
            '    smFolder.Text = drSmartFolder("FolderName")
            '    smFolder.ID = "Q" & drSmartFolder("FolderId")

            '    'If drSmartFolder("CanManage") = 1 Then
            '    '    smFolder.EditEnabled = False
            '    'Else
            '    '    smFolder.EditEnabled = False
            '    'End If

            '    smFolder.AutoPostBackOnSelect = True
            '    smFolder.Checked = False
            '    smFolder.ImageUrl = IMAGE_SMARTFOLDER

            '    If drSmartFolder("Scope") = "G" Then
            '        publicNode.Nodes.Add(smFolder)
            '    Else
            '        privateNode.Nodes.Add(smFolder)
            '    End If

            '    'NodeCollection.Add(smFolder)

            'Next


            'Dim CatFolderNode As New RadTreeNode("Cabinets & Folders", "CabsFolders")

            Dim CatFolderNode As New TreeViewNode
            CatFolderNode.Text = "Cabinets & Folders"
            CatFolderNode.ID = "CabsFolders"
            CatFolderNode.AutoPostBackOnSelect = True
            CatFolderNode.Expanded = True

            'CatFolderNode.Expanded = True
            'CatFolderNode.EditEnabled = False
            'CatFolderNode.Checkable = False
            CatFolderNode.ImageUrl = IMAGE_CABINETS_FOLDERS
            'CatFolderNode.PostBack = True

            If Me.mobjUser.SysAdmin Then
                ''Build share context menus
                'Dim cm As New RadTreeViewContextMenu.ContextMenu
                'cm.Name = "NewShareMenu"
                'cm.Width = 150
                'contextMenus.Add(cm)

                ''New share
                'Dim mnuNewShare As New RadTreeViewContextMenu.ContextMenuItem(NEW_SHARE)
                'mnuNewShare.Image = IMAGE_CABINET_ADD
                'mnuNewShare.PostBack = True
                'cm.Items.Add(mnuNewShare)

                'CatFolderNode.ContextMenuName = "NewShareMenu"

            End If

            treeSearch.Nodes.Add(CatFolderNode)

            For Each drShare As DataRow In ds.Tables(TreeTables.SHARES).Rows

                'Dim strMenuName As String = System.Guid.NewGuid.ToString.Replace("-", "")

                ''Build share context menus
                'Dim cm As New RadTreeViewContextMenu.ContextMenu
                'cm.Name = strMenuName
                'cm.Width = 150
                'contextMenus.Add(cm)

                ''New share
                'If drShare("CanAddShare") = True Then
                '    Dim mnuNewShare As New RadTreeViewContextMenu.ContextMenuItem(NEW_SHARE)
                '    mnuNewShare.Image = IMAGE_CABINET_ADD
                '    mnuNewShare.PostBack = True
                '    cm.Items.Add(mnuNewShare)
                'End If

                ''New folder
                'If drShare("CanAddFolder") = True Then
                '    Dim mnuShareNewFolder As New RadTreeViewContextMenu.ContextMenuItem(NEW_FOLDER)
                '    mnuShareNewFolder.Image = IMAGE_FOLDER_ADD
                '    mnuShareNewFolder.PostBack = True
                '    cm.Items.Add(mnuShareNewFolder)
                'End If

                ''Rename Share
                'If drShare("CanRenameShare") = True Then
                '    Dim mnuRename As New RadTreeViewContextMenu.ContextMenuItem(RENAME_SHARE)
                '    mnuRename.Image = IMAGE_RENAME
                '    mnuRename.PostBack = False
                '    cm.Items.Add(mnuRename)
                'End If

                ''Remove Share
                'If drShare("CanDeleteShare") = True Then
                '    Dim mnuRemove As New RadTreeViewContextMenu.ContextMenuItem(REMOVE_SHARE)
                '    mnuRemove.Image = IMAGE_REMOVE
                '    mnuRemove.PostBack = True
                '    cm.Items.Add(mnuRemove)
                'End If

                ''Security
                'If drShare("CanChangeSecurity") = True Then
                '    Dim mnuSecurity As New RadTreeViewContextMenu.ContextMenuItem(SECURITY)
                '    mnuSecurity.Image = IMAGE_SECURITY
                '    mnuSecurity.PostBack = True
                '    cm.Items.Add(mnuSecurity)
                'End If

                'Build share

                Dim Share As New TreeViewNode
                Share.Text = drShare("ShareName")
                Share.ID = "S" & drShare("ShareId")
                'Dim Share As New RadTreeNode(drShare("ShareName"), "S" & drShare("ShareId"))
                Share.ImageUrl = IMAGE_CABINET
                Share.AutoPostBackOnSelect = True
                Share.AutoPostBackOnRename = False

                If drShare("HasChildFolder") = True Then
                    Share.ContentCallbackUrl = "XmlGenerator.aspx?Ref=Popup&NodeId=" & Share.ID
                End If

                If drShare("ExpandShare") = True Then
                    Share.Expanded = True
                End If

                'Share.PostBack = True
                'Share.AutoPostBackOnSelect = True
                'Share.ContextMenuName = strMenuName

                'If drShare("CanRenameShare") = True Then
                '    Share.EditEnabled = False
                'Else
                '    Share.EditEnabled = False
                'End If

                'Share.Checkable = False

                'Add share to tree
                'radTree.Nodes.Add(Share)
                CatFolderNode.Nodes.Add(Share)
                'colNodes.Add(Share)

            Next

            'Build share children folders
            For Each drChild As DataRow In ds.Tables(TreeTables.FOLDERS).Rows

                Dim intParentFolderId As Integer = drChild("ParentFolderId")
                Dim intFolderLevel As Integer = drChild("FolderLevel")

                'Find parent
                Dim ParentNode As New TreeViewNode
                If intFolderLevel = 1 Then
                    ParentNode = treeSearch.FindNodeById("S" & intParentFolderId.ToString)

                    'ParentNode = CatFolderNode.Nodes("S" & intParentFolderId.ToString)
                    'ParentNode = radTree.FindNodeByValue("S" & intParentFolderId.ToString)
                Else
                    'ParentNode = NodeCollection.FindNodeByValue("W" & intParentFolderId.ToString)
                    ParentNode = treeSearch.FindNodeById("W" & intParentFolderId.ToString)
                End If

                'Add child to parent
                If Not IsNothing(ParentNode) Then

                    'Dim strFolderMenuName As String = System.Guid.NewGuid.ToString.Replace("-", "")

                    ''Folder menus
                    'Dim cmFolder As New RadTreeViewContextMenu.ContextMenu
                    'cmFolder.Name = strFolderMenuName
                    'cmFolder.Width = 150
                    'contextMenus.Add(cmFolder)

                    ''New folder
                    'If drChild("CanAddFolder") = True Then
                    '    Dim mnuNewFolder As New RadTreeViewContextMenu.ContextMenuItem(NEW_FOLDER)
                    '    mnuNewFolder.Image = IMAGE_FOLDER_ADD
                    '    mnuNewFolder.PostBack = True
                    '    cmFolder.Items.Add(mnuNewFolder)
                    'End If

                    ''Rename folder
                    'If drChild("CanRenameFolder") = True Then
                    '    Dim mnuRenameFolder As New RadTreeViewContextMenu.ContextMenuItem(RENAME_FOLDER)
                    '    mnuRenameFolder.Image = IMAGE_RENAME
                    '    mnuRenameFolder.PostBack = False
                    '    cmFolder.Items.Add(mnuRenameFolder)
                    'End If

                    ''Remove folder
                    'If drChild("CanDeleteFolder") = True Then
                    '    Dim mnuRemoveFolder As New RadTreeViewContextMenu.ContextMenuItem(REMOVE_FOLDER)
                    '    mnuRemoveFolder.Image = IMAGE_REMOVE
                    '    mnuRemoveFolder.PostBack = True
                    '    cmFolder.Items.Add(mnuRemoveFolder)
                    'End If

                    ''Add files
                    'If drChild("ReadOnly") = False Then
                    '    Dim mnuAddFiles As New RadTreeViewContextMenu.ContextMenuItem(ADD_FILES)
                    '    mnuAddFiles.Image = IMAGE_ADD_FILES
                    '    mnuAddFiles.PostBack = True
                    '    cmFolder.Items.Add(mnuAddFiles)
                    'End If

                    ''Security
                    'If drChild("CanChangeSecurity") = True Then
                    '    Dim mnuSecurityFolder As New RadTreeViewContextMenu.ContextMenuItem(SECURITY)
                    '    mnuSecurityFolder.Image = IMAGE_SECURITY
                    '    mnuSecurityFolder.PostBack = True
                    '    cmFolder.Items.Add(mnuSecurityFolder)
                    'End If

                    ''Add to favorites
                    'If drChild("ReadOnly") = False Then
                    '    Dim mnuAddToFavorites As New RadTreeViewContextMenu.ContextMenuItem(ADD_TO_FAVORITES)
                    '    mnuAddToFavorites.Image = IMAGE_FAVORITES_ADD
                    '    mnuAddToFavorites.PostBack = True
                    '    cmFolder.Items.Add(mnuAddToFavorites)
                    'End If

                    'Dim ChildNode As New RadTreeNode(drChild("FolderName"), "W" & drChild("FolderId"))

                    Dim ChildNode As New TreeViewNode
                    ChildNode.Text = drChild("FolderName")
                    ChildNode.ID = "W" & drChild("FolderId")
                    ChildNode.AutoPostBackOnSelect = True
                    ChildNode.ImageUrl = IMAGE_FOLDER
                    ChildNode.AutoPostBackOnRename = False
                    ChildNode.EditingEnabled = False
                    ChildNode.ExpandedImageUrl = IMAGE_FOLDER_OPEN
                    'ChildNode.Expanded = True
                    'ChildNode.ContextMenuName = strFolderMenuName

                    'If drChild("CanRenameFolder") = True Then
                    '    ChildNode.EditEnabled = False
                    'Else
                    '    ChildNode.EditEnabled = False
                    'End If

                    'ChildNode.PostBack = True
                    'ChildNode.Checkable = False

                    If drChild("HasChildFolder") = True Then
                        'ChildNode.ExpandMode = ExpandMode.ServerSideCallBack
                        ChildNode.ContentCallbackUrl = "XmlGenerator.aspx?Ref=Popup&NodeId=" & ChildNode.ID
                        ChildNode.Expanded = False
                    End If

                    If drChild("ExpandFolder") = True Then
                        ChildNode.Expanded = True
                    End If

                    If drChild("CanViewFolderOnly") = True Then
                        'ChildNode.Expanded = True
                        ChildNode.AutoPostBackOnSelect = False
                        ChildNode.ClientSideCommand = "alert('Access denied.');stopProgressDisplay();"
                    End If

                    'NodeCollection.Add(ChildNode)
                    'colNodes.Add(ChildNode)

                    ParentNode.Nodes.Add(ChildNode)

                End If

            Next

            '    NodeCollection = Nothing

            '    radTree.ContextMenus = contextMenus
            '    radTree.AllowNodeEditing = True

            '    DisableToolbar()

            'Else

            '    contextMenus = radTree.ContextMenus

            'treeSearch_NodeCheckChanged(Nothing, Nothing)

        End If

        'Try
        '    If Not Page.IsPostBack Then radTree_NodeCheck(Nothing, Nothing)
        'Catch ex As Exception

        'End Try

    End Sub

    Private Sub btnMove_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnMove.Click

        Dim conSql As New SqlClient.SqlConnection

        Try

            conSql = Functions.BuildConnection(Me.mobjUser)

            Dim dt As New DataTable
            Dim objFolders As New Accucentric.docAssist.Data.Images.Folders(conSql, False)

            If FolderId.Value.StartsWith("S") Then

                dt = objFolders.FolderCabinetPermissions(Me.mobjUser.ImagesUserId, CInt(FolderId.Value.Replace("S", "")) * -1)

                If dt.Rows.Count = 0 Then
                    System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType(), "btnMove", "alert('The destination cabinet may have been removed or modified. Please check and try again.');", True)
                    Exit Sub
                Else
                    If dt.Rows(0)("CanAddFolder") = "1" Then
                        Select Case objFolders.MoveFolder(mobjUser.ImagesUserId, Request.QueryString(0).Replace("W", ""), CInt(FolderId.Value.Replace("S", "")) * -1)
                            Case 1
                                System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType(), "btnMove", "window.returnValue='" & FolderId.Value & "';window.close();", True)
                            Case -1
                                System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType(), "btnMove", "alert('The destination cabinet may have been moved or removed. Please check and try again.');", True)
                            Case -2
                                System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType(), "btnMove", "alert('You cannot move a folder to one of its subfolders.');", True)
                        End Select
                    Else
                        System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType(), "btnMove", "alert('You do not have access to this cabinet. Please select another cabinet. If you need access please contact your administrator.');", True)
                    End If
                End If

            Else

                dt = objFolders.FolderCabinetPermissions(Me.mobjUser.ImagesUserId, FolderId.Value.Replace("W", ""))

                If dt.Rows.Count = 0 Then
                    System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType(), "btnMove", "alert('The destination folder may have been removed or modified. Please check and try again.');", True)
                    Exit Sub
                Else
                    If dt.Rows(0)("CanAddFilesToFolder") = "1" Then
                        Select Case objFolders.MoveFolder(mobjUser.ImagesUserId, Request.QueryString(0).Replace("W", ""), FolderId.Value.Replace("W", ""))
                            Case 1
                                System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType(), "btnMove", "window.returnValue='" & FolderId.Value & "';window.close();", True)
                            Case -1
                                System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType(), "btnMove", "alert('The destination folder may have been moved or removed. Please check and try again.');", True)
                            Case -2
                                System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType(), "btnMove", "alert('You cannot move a folder to one of its subfolders.');", True)
                        End Select
                    Else
                        System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType(), "btnMove", "alert('You do not have access to this folder. Please select another cabinet. If you need access please contact your administrator.');", True)
                    End If
                End If

            End If

        Catch ex As Exception

            Dim f = 9

        Finally

            If conSql.State <> ConnectionState.Closed Then
                conSql.Close()
                conSql.Dispose()
            End If

        End Try

    End Sub

End Class
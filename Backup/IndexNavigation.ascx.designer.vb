﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'     Runtime Version:2.0.50727.3082
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On



Partial Public Class IndexNavigation

    '''<summary>
    '''BackCell control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents BackCell As Global.System.Web.UI.HtmlControls.HtmlTableCell

    '''<summary>
    '''btnBack control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnBack As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''tdDocumentText control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tdDocumentText As Global.System.Web.UI.HtmlControls.HtmlTableCell

    '''<summary>
    '''A2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents A2 As Global.System.Web.UI.HtmlControls.HtmlAnchor

    '''<summary>
    '''ShowText control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ShowText As Global.System.Web.UI.HtmlControls.HtmlImage

    '''<summary>
    '''tdDocumentHistory control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tdDocumentHistory As Global.System.Web.UI.HtmlControls.HtmlTableCell

    '''<summary>
    '''imgHistory control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents imgHistory As Global.System.Web.UI.HtmlControls.HtmlImage

    '''<summary>
    '''Notes control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Notes As Global.System.Web.UI.HtmlControls.HtmlImage

    '''<summary>
    '''tdSpace1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tdSpace1 As Global.System.Web.UI.HtmlControls.HtmlTableCell

    '''<summary>
    '''NextPrevSpacer control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents NextPrevSpacer As Global.System.Web.UI.HtmlControls.HtmlImage

    '''<summary>
    '''btnPrevious control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnPrevious As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''btnNext control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnNext As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''tdSpace2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tdSpace2 As Global.System.Web.UI.HtmlControls.HtmlTableCell

    '''<summary>
    '''tdFirst control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tdFirst As Global.System.Web.UI.HtmlControls.HtmlTableCell

    '''<summary>
    '''tdPrevious control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tdPrevious As Global.System.Web.UI.HtmlControls.HtmlTableCell

    '''<summary>
    '''tdNext control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tdNext As Global.System.Web.UI.HtmlControls.HtmlTableCell

    '''<summary>
    '''tdLast control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tdLast As Global.System.Web.UI.HtmlControls.HtmlTableCell

    '''<summary>
    '''tdZoom control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tdZoom As Global.System.Web.UI.HtmlControls.HtmlTableCell

    '''<summary>
    '''tdZoomIn control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tdZoomIn As Global.System.Web.UI.HtmlControls.HtmlTableCell

    '''<summary>
    '''tdZoomOut control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tdZoomOut As Global.System.Web.UI.HtmlControls.HtmlTableCell

    '''<summary>
    '''tdHand control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tdHand As Global.System.Web.UI.HtmlControls.HtmlTableCell

    '''<summary>
    '''tdSpace3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tdSpace3 As Global.System.Web.UI.HtmlControls.HtmlTableCell

    '''<summary>
    '''tdFitBest control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tdFitBest As Global.System.Web.UI.HtmlControls.HtmlTableCell

    '''<summary>
    '''tdFitWidth control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tdFitWidth As Global.System.Web.UI.HtmlControls.HtmlTableCell

    '''<summary>
    '''tdFitHeight control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tdFitHeight As Global.System.Web.UI.HtmlControls.HtmlTableCell

    '''<summary>
    '''tdPrint control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tdPrint As Global.System.Web.UI.HtmlControls.HtmlTableCell

    '''<summary>
    '''imgPrint control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents imgPrint As Global.System.Web.UI.HtmlControls.HtmlImage

    '''<summary>
    '''tdMail control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tdMail As Global.System.Web.UI.HtmlControls.HtmlTableCell

    '''<summary>
    '''imgMail control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents imgMail As Global.System.Web.UI.HtmlControls.HtmlImage

    '''<summary>
    '''tdAttach control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tdAttach As Global.System.Web.UI.HtmlControls.HtmlTableCell

    '''<summary>
    '''lnkAttachments control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkAttachments As Global.System.Web.UI.HtmlControls.HtmlAnchor

    '''<summary>
    '''imgAttachments control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents imgAttachments As Global.System.Web.UI.HtmlControls.HtmlImage

    '''<summary>
    '''tdAttachmentDelete control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tdAttachmentDelete As Global.System.Web.UI.HtmlControls.HtmlTableCell

    '''<summary>
    '''AttachmentDelete control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents AttachmentDelete As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''tdShare control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tdShare As Global.System.Web.UI.HtmlControls.HtmlTableCell

    '''<summary>
    '''tdPagesVersion control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tdPagesVersion As Global.System.Web.UI.HtmlControls.HtmlTableCell

    '''<summary>
    '''Label1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Label1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''ddlVersions control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlVersions As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Label2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Label2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''txtPageNo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtPageNo As Global.System.Web.UI.HtmlControls.HtmlInputText

    '''<summary>
    '''lblPagesOf control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPagesOf As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Label3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Label3 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''ddlQuality control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlQuality As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''SaveCell control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents SaveCell As Global.System.Web.UI.HtmlControls.HtmlTableCell

    '''<summary>
    '''imgSave control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents imgSave As Global.System.Web.UI.HtmlControls.HtmlImage

    '''<summary>
    '''litReload control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents litReload As Global.System.Web.UI.WebControls.Literal

    '''<summary>
    '''litPostbackCount control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents litPostbackCount As Global.System.Web.UI.WebControls.Literal

    '''<summary>
    '''litBack control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents litBack As Global.System.Web.UI.WebControls.Literal

    '''<summary>
    '''VersionId control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents VersionId As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''PostbackCount control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents PostbackCount As Global.System.Web.UI.HtmlControls.HtmlInputText

    '''<summary>
    '''uid control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents uid As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''aid control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents aid As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''url control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents url As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''Annotation control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Annotation As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''Redaction control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Redaction As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''txtWorkFlow control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtWorkFlow As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''litBadge control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents litBadge As Global.System.Web.UI.WebControls.Literal

    '''<summary>
    '''litPageCount control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents litPageCount As Global.System.Web.UI.WebControls.Literal

    '''<summary>
    '''litLatestVID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents litLatestVID As Global.System.Web.UI.WebControls.Literal
End Class

<%@ Page Language="vb" AutoEventWireup="false" Codebehind="Contact.aspx.vb" Inherits="docAssistWeb.COntact"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Contact Us</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<table cellSpacing="0" cellPadding="0" width="350" align="left" border="0">
				<tr>
					<td colspan="3" align="right"><IMG src="Images/doc_header_logo.gif"></td>
				</tr>
				<tr>
					<td><img src="spacer.gif" height="1" width="3"><img src="spacer.gif" height="1" width="15"></td>
					<td width="100%">
						<font face="Verdana" size="1">
							<P></P>
							<P></P>
							<P>
								<STRONG>Contact Information</STRONG>&nbsp;
							</P>
							<P><STRONG>Corporate Headquarters<BR>
								</STRONG>12555 Orange Drive<BR>
								Suite 101<BR>
								Davie, FL 33330<BR>
								USA <FONT face="Verdana" size="1">
									<BR>
									<BR>
									<STRONG>Questions? Comments?<BR>
									</STRONG><A href="mailto:info@docassist.com">info@docassist.com</A><BR>
									<BR>
									<STRONG>Web Site Contact<BR>
									</STRONG><A href="mailto:webmaster@docassist.com">webmaster@docassist.com</A><BR>
							</P>
						</font></FONT>
					</td>
					<td><img src="spacer.gif" height="1" width="3"></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>

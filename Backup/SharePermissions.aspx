<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="SharePermissions.aspx.vb"
    Inherits="docAssistWeb.SharePermissions" ValidateRequest="false" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>Cabinet Permissions</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <link href="Styles.css" type="text/css" rel="stylesheet">
    <base target="_self">
</head>
<body bottommargin="0" leftmargin="0" topmargin="0" rightmargin="0">
    <form id="Form1" method="post" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table height="650" cellspacing="0" cellpadding="0" width="650" border="0">
                <tr>
                    <td valign="top">
                        <table height="100%" cellspacing="0" cellpadding="0" width="100%" border="0">
                            <tr>
                                <td valign="top" height="100%">
                                    <div class="Container">
                                        <div class="north">
                                            <div class="east">
                                                <div class="south">
                                                    <div class="west">
                                                        <div class="ne">
                                                            <div class="se">
                                                                <div class="sw">
                                                                    <div class="nw">
                                                                        <table id="surround" height="100%" cellspacing="0" cellpadding="0" width="95%" border="0">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td align="right">
                                                                                        <table class="PageContent" cellspacing="0" cellpadding="0" width="100%" border="0">
                                                                                            <tr>
                                                                                                <td nowrap colspan="2">
                                                                                                    <span class="Heading" id="ScreenMode" runat="server">Cabinet Permissions</span>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td nowrap width="100%">
                                                                                                    <span class="Text" id="GroupNameLabel" runat="server"><span class="Text" id="GroupDescLabel"
                                                                                                        runat="server">
                                                                                                        <asp:Label ID="lblName" runat="server" Font-Size="10pt" Font-Italic="True" Font-Bold="True"></asp:Label></span></span>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td nowrap>
                                                                                                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                                                                                        <tr>
                                                                                                            <td align="left">
                                                                                                                <div style="overflow: auto; width: 100%; height: 300px">
                                                                                                                    <asp:DataGrid ID="dgPermissions" runat="server" BorderWidth="0px" CssClass="SearchGrid"
                                                                                                                        AutoGenerateColumns="False" Width="100%">
                                                                                                                        <AlternatingItemStyle BackColor="#E0E0E0"></AlternatingItemStyle>
                                                                                                                        <HeaderStyle Font-Bold="True"></HeaderStyle>
                                                                                                                        <Columns>
                                                                                                                            <asp:TemplateColumn HeaderText="Group">
                                                                                                                                <HeaderStyle Width="350px"></HeaderStyle>
                                                                                                                                <ItemTemplate>
                                                                                                                                    <asp:Label ID="lblGroupId" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.GroupId") %>'
                                                                                                                                        Visible="False">
                                                                                                                                    </asp:Label>
                                                                                                                                    <asp:Label ID="lblFileLevel" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FileLevel") %>'
                                                                                                                                        Visible="False">
                                                                                                                                    </asp:Label>
                                                                                                                                    <asp:Label ID="lblFolderLevel" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FolderLevel") %>'
                                                                                                                                        Visible="False">
                                                                                                                                    </asp:Label>
                                                                                                                                    <asp:Label ID="lblGroupName" runat="server" Width="100%" Text='<%# DataBinder.Eval(Container, "DataItem.GroupName") %>'>
                                                                                                                                    </asp:Label>
                                                                                                                                </ItemTemplate>
                                                                                                                            </asp:TemplateColumn>
                                                                                                                            <asp:TemplateColumn HeaderText="Document Permissions">
                                                                                                                                <HeaderStyle Width="160px"></HeaderStyle>
                                                                                                                                <ItemTemplate>
                                                                                                                                    <asp:DropDownList ID="ddlFilePermissions" runat="server" Width="150px">
                                                                                                                                        <asp:ListItem Value="0">None</asp:ListItem>
                                                                                                                                        <asp:ListItem Value="1">Read</asp:ListItem>
                                                                                                                                        <asp:ListItem Value="2">Add</asp:ListItem>
                                                                                                                                        <asp:ListItem Value="3">Change</asp:ListItem>
                                                                                                                                        <asp:ListItem Value="4">Delete</asp:ListItem>
                                                                                                                                    </asp:DropDownList>
                                                                                                                                </ItemTemplate>
                                                                                                                            </asp:TemplateColumn>
                                                                                                                            <asp:TemplateColumn HeaderText="Folder Permissions">
                                                                                                                                <HeaderStyle Width="160px"></HeaderStyle>
                                                                                                                                <ItemTemplate>
                                                                                                                                    <asp:DropDownList ID="ddlFolderPermissons" runat="server" Width="150px">
                                                                                                                                        <asp:ListItem Value="0">None</asp:ListItem>
                                                                                                                                        <asp:ListItem Value="1">List</asp:ListItem>
                                                                                                                                        <asp:ListItem Value="2">Add</asp:ListItem>
                                                                                                                                        <asp:ListItem Value="3">Change</asp:ListItem>
                                                                                                                                        <asp:ListItem Value="4">Delete</asp:ListItem>
                                                                                                                                    </asp:DropDownList>
                                                                                                                                </ItemTemplate>
                                                                                                                            </asp:TemplateColumn>
                                                                                                                        </Columns>
                                                                                                                    </asp:DataGrid></div>
                                                                                                                <span class="Text" id="Span1" runat="server">
                                                                                                                    <br>
                                                                                                                    &nbsp;&nbsp;&nbsp;&nbsp; Cabinet Administrators:</span><br>
                                                                                                                <table class="PageContent" height="150" cellspacing="0" cellpadding="0" width="100%"
                                                                                                                    border="0">
                                                                                                                    <tr>
                                                                                                                        <td width="50%">
                                                                                                                        </td>
                                                                                                                        <td>
                                                                                                                            <p>
                                                                                                                                <span class="Text">Available:<br>
                                                                                                                                </span>
                                                                                                                                <asp:ListBox ID="lstAvailable" runat="server" Width="258px" Height="140px"></asp:ListBox>
                                                                                                                            </p>
                                                                                                                        </td>
                                                                                                                        <td>
                                                                                                                            <asp:ImageButton ID="btnAssignAll" runat="server" ImageUrl="Images/addall.gif"></asp:ImageButton><br>
                                                                                                                            <img height="3" src="spacer.gif" width="1">
                                                                                                                            <asp:ImageButton ID="btnAdd" runat="server" ImageUrl="Images/addone.gif"></asp:ImageButton><br>
                                                                                                                            <img height="3" src="spacer.gif" width="1">
                                                                                                                            <asp:ImageButton ID="btnRemove" runat="server" ImageUrl="Images/removeone.gif"></asp:ImageButton><br>
                                                                                                                            <img height="3" src="spacer.gif" width="1">
                                                                                                                            <asp:ImageButton ID="btnRemoveAll" runat="server" ImageUrl="Images/removeall.gif">
                                                                                                                            </asp:ImageButton>
                                                                                                                        </td>
                                                                                                                        <td>
                                                                                                                            <span class="Text">Assigned:</span><br>
                                                                                                                            <asp:ListBox ID="lstAssigned" runat="server" Width="258px" Height="140px"></asp:ListBox>
                                                                                                                        </td>
                                                                                                                        <td width="50%">
                                                                                                                        </td>
                                                                                                                    </tr>
                                                                                                                </table>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                        <table class="PageContent" cellspacing="0" cellpadding="0" width="100%" border="0">
                                                                                            <tr>
                                                                                                <td align="left">
                                                                                                </td>
                                                                                                <td nowrap width="180">
                                                                                                    <a href="javascript:window.close();">
                                                                                                        <img src="Images/smallred_cancel.gif" border="0"></a> &nbsp;<asp:ImageButton ID="btnSave"
                                                                                                            runat="server" ImageUrl="Images/smallred_save.gif" ToolTip="Save"></asp:ImageButton>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                    <div>
                                                                                    </div>
                                                                                    <div>
                                                                                    </div>
                                                                                    <div>
                                                                                    </div>
                                                                                    <div>
                                                                                    </div>
                                                                                    <div>
                                                                                    </div>
                                                                                    <div>
                                                                                    </div>
                                                                                    <div>
                                                                                    </div>
                                                                                    <div>
                                                                                    </div>
                                                                                    <div>
                                                                                    </div>
                                </td>
                            </tr>
                        </table>
                        </DIV></DIV></DIV></DIV></DIV></DIV></DIV></DIV></DIV>
                    </td>
                </tr>
            </table>
         </ContentTemplate>
    </asp:UpdatePanel>           </form> </TD></TR></TBODY></TABLE>

</body>
</html>

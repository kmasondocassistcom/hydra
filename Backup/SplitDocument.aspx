<%@ Page Language="vb" AutoEventWireup="false" Codebehind="SplitDocument.aspx.vb" Inherits="docAssistWeb.SplitDocument"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<TITLE>Split Document</TITLE>
		<META content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<META content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<META content="JavaScript" name="vs_defaultClientScript">
		<META content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
		<base target="_self">
	</HEAD>
	<BODY>
		<FORM id="Form1" method="post" runat="server">
			<TABLE class="PageContent" cellSpacing="0" cellPadding="0" width="100%">
				<TBODY>
					<TR>
						<TD align="left">
							<TABLE class="PageContent" border="0" width="100%" style="FONT-SIZE: 9pt; FONT-FAMILY: 'Trebuchet MS'">
								<TR>
									<td width="80" noWrap>&nbsp;&nbsp;&nbsp;&nbsp; Start Page</td>
									<TD colSpan="2" width="100%" nowrap>
										<asp:TextBox id="txtStart" runat="server" Width="40px"></asp:TextBox></TD>
								</TR>
								<TR>
									<td noWrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;End Page</td>
									<TD colSpan="2" width="100%" nowrap>
										<asp:TextBox id="txtEnd" runat="server" Width="40px"></asp:TextBox></TD>
								</TR>
								<TR>
									<td></td>
									<TD width="100%" nowrap>
										<asp:CheckBox id="chkInherit" runat="server" Text="Inherit attributes"></asp:CheckBox></TD>
									</SPAN>
								</TR>
								<TR>
									<td></td>
									<TD colSpan="1" align="right" width="100%" vAlign="top"><SPAN id="Validate" style="COLOR: red">
										</SPAN>
										<BR>
										<A onclick="javascript:window.close()" href="#"><IMG src="Images/smallred_cancel.gif" border="0"></A>
										<ASP:IMAGEBUTTON id="btnSplit" Runat="server" ImageUrl="Images/btn_split.gif" AlternateText="Delete"
											Width="80px" Height="17px"></ASP:IMAGEBUTTON></TD>
								</TR>
								<TR>
									<td colspan="2" align="right">
										<input type="hidden" id="PageCount" runat="server">
										<asp:Label id="lblError" runat="server" ForeColor="Red"></asp:Label></td>
								</TR>
							</TABLE>
							<SCRIPT language="javascript">
							
								function NumericOnly(evt)
								{
									evt = (evt) ? evt : event;
									var charCode = (evt.charCode) ? evt.charCode : ((evt.keyCode) ? evt.keyCode
									: ((evt.which) ? evt.which : 0));
									if (charCode > 31 && (charCode < 48 || charCode >57)) {return false;}
									return true;
								}
								
							</SCRIPT>
						</TD>
					</TR>
				</TBODY>
			</TABLE>
		</FORM>
	</BODY>
</HTML>

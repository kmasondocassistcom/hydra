<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ConfirmDelete.aspx.vb" Inherits="docAssistWeb._ConfirmDelete"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<TITLE>Delete Pages</TITLE>
		<META content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<META content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<META content="JavaScript" name="vs_defaultClientScript">
		<META content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
		<base target="_self">
	</HEAD>
	<BODY onload="javascript:if(document.all.rdoRange.checked){Form1.txtFrom.disabled=false;Form1.txtTo.disabled=false;}">
		<FORM id="Form1" method="post" runat="server">
			<TABLE class="PageContent" cellSpacing="0" cellPadding="0" width="100%">
				<TBODY>
					<TR>
						<TD align="left">&nbsp; <img src="Images/remove_pages.gif"> <INPUT id="hidMaxPages" type="hidden" name="hidMaxPages" runat="server">
							<TABLE class="PageContent" border="0" width="100%">
								<TR>
									<TD colSpan="2" width="100%"><INPUT id="rdoCurrent" onclick="radio_click(this);" type="radio" CHECKED value="rdoCurrent"
											runat="server"> Delete Current Page
										<ASP:LABEL id="lblPageNo" Runat="server"></ASP:LABEL></TD>
								</TR>
								<TR>
									<TD colSpan="2" width="100%"><INPUT id="rdoDocument" onclick="radio_click(this);" type="radio" value="rdoDocument" runat="server">
										Delete Entire Document</TD>
								</TR>
								<TR>
									<TD width="100%"><INPUT id="rdoRange" onclick="radio_click(this);" type="radio" value="rdoRange" runat="server">
										Delete Range <INPUT class="InputBox" onkeypress="return NumericOnly(event);" id="txtFrom" disabled type="text"
											maxLength="5" size="4" name="txtFrom" Runat="server">&nbsp;to <INPUT class="InputBox" onkeypress="return NumericOnly(event);" id="txtTo" disabled type="text"
											maxLength="5" size="4" name="txtTo" runat="server"></TD>
									</SPAN>
								</TR>
								<TR>
									<TD colSpan="1" align="right" width="100%" vAlign="top"><SPAN id="Validate" style="COLOR: red">
											<ASP:LABEL id="lblErrorMessage" Runat="server" Visible="False" FORECOLOR="Red"></ASP:LABEL></SPAN><BR>
										<ASP:LITERAL id="litResult" runat="server"></ASP:LITERAL><A onclick="javascript:window.close()" href="#"><IMG src="Images/btn_cancel.gif" border="0"></A>
										<ASP:IMAGEBUTTON id="btnGoDelete" Runat="server" ImageUrl="Images/btn_delete.gif" AlternateText="Delete"></ASP:IMAGEBUTTON></TD>
								</TR>
							</TABLE>
							<SCRIPT language="javascript">
							
							function radio_click(obj)
							{
								Form1.rdoDocument.checked = false;
								Form1.rdoRange.checked = false;
								Form1.rdoCurrent.checked = false;
								Form1.txtFrom.disabled = true;
								Form1.txtTo.disabled = true;
								Form1.txtFrom.className = "InputBox" ;
								Form1.txtTo.className = "InputBox";

								obj.checked = true;
								
								if(obj.id == "rdoRange")
								{
									Form1.txtFrom.disabled = false;
									Form1.txtTo.disabled = false;
								} else
								{
									Form1.txtFrom.value = "";
									Form1.txtTo.value = "";
									document.all.Validate.innerHTML = "";
								}
								return false;
							}
							
							function Validate()
							{
								var returnValue = false;
								if(Form1.rdoRange.checked)
								{
									if(Form1.txtFrom.value.length != 0 && Form1.txtTo.value.length != 0)
									{
										var From = Math.abs(Form1.txtFrom.value);
										var To = Math.abs(Form1.txtTo.value);
										var Max = Math.abs(Form1.hidMaxPages.value);
										
										try
										{
											if(From == 1 && To == Max)
											{
												document.all.Validate.innerHTML = "Please use \"Delete Document\" to remove the entire document.";
												Form1.txtFrom.className = "InputBoxError";
												Form1.txtFrom.select();
												Form1.txtFrom.focus();
												Form1.txtTo.className = "InputBoxError";
												returnValue = false;
											}
											else
											{
												if ((From > To || To > Max || From > Max))
												{
													if(From > To)
													{
														document.all.Validate.innerHTML = "Invalid page range.";
														//Form1.txtFrom.style.backgroundColor = "#D00000";
														//Form1.txtFrom.style.color = "#ffffff";
														Form1.txtFrom.select();
														Form1.txtFrom.focus();
														//Form1.txtTo.style.backgroundColor = "#D00000";
														//Form1.txtTo.style.color = "#ffffff";
													}
													else
													{
														returnValue = true;
													}
													if((To > Max || From > Max) && returnValue == true)
													{
														document.all.Validate.innerHTML = "Invalid page range specified.";
														//Form1.txtTo.style.backgroundColor = "#D00000";
														//Form1.txtTo.style.color = "#ffffff";
														Form1.txtTo.select();
														Form1.txtTo.focus();
														returnValue = false;
													}

												} 
												else
												{
												
													returnValue = true;
												}
											}
										}
										catch(err)
										{
											document.all.Validate.innerHTML("Error occurred while validating");
											returnValue = false;
										}
									}
									else
									{
										document.all.Validate.innerHTML = "Please complete the page range.";
										if(Form1.txtTo.value.length == 0 )
											Form1.txtTo.className="InputBoxError";
										if(Form1.txtFrom.value.length == 0)
											Form1.txtFrom.className="InputBoxError";
										returnValue = false;
									}
								}
								else
								{
									returnValue = true;
								}
								if(returnValue)
								{
									returnValue = confirm("Are you sure?");
								}
								return returnValue;	
							}

							function NumericOnly(evt)
							{
								evt = (evt) ? evt : event;
								var charCode = (evt.charCode) ? evt.charCode : ((evt.keyCode) ? evt.keyCode
								: ((evt.which) ? evt.which : 0));
								if (charCode > 31 && (charCode < 48 || charCode >57)) {return false;}
								return true;
							}
							
							</SCRIPT>
							<ASP:TEXTBOX id="txtVersionId" runat="server" Visible="False"></ASP:TEXTBOX><ASP:TEXTBOX id="txtPageNo" runat="server" Visible="False"></ASP:TEXTBOX></TD>
					</TR>
				</TBODY>
			</TABLE>
		</FORM>
	</BODY>
</HTML>

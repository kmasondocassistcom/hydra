<%@ Page Language="vb" AutoEventWireup="false" Codebehind="landing.aspx.vb" Inherits="docAssistWeb.landing"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title></title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="textStyles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body style="BACKGROUND-POSITION-Y: bottom; BACKGROUND-IMAGE: url(http://localhost/docAssistWeb1/Images/Search/backgroundGrad.gif); BACKGROUND-REPEAT: repeat-x" bottomMargin="0" leftMargin="0" topMargin="0"
		rightMargin="0">
		<form class="welcome" id="Form1" method="post" runat="server">
			<table cellSpacing="0" cellPadding="0" width="100%" align="left" border="0">
				<!-- Start Page Body Content -->
				<tr>
					<td id="pagecontent">
					<td id="girl" vAlign="top">
						<div id="coretext">
							<table class="alert" id="tblAlert" cellSpacing="5" cellPadding="5" width="100%" border="0"
								runat="server">
								<tr>
									<th vAlign="top" align="left" bgColor="#d8e4ef">
										Account Alert
									</th>
								</tr>
								<tr>
									<td id="tree" width="50%"><strong><span id="AlertMessage" runat="server"></span></strong></td>
								</tr>
							</table>
							<table class="select" cellSpacing="2" cellPadding="0" width="100%" border="0">
								<tr>
									<td id="borders" vAlign="top" width="50%">
										<table cellSpacing="5" cellPadding="5" width="100%" border="0">
											<tr>
												<th vAlign="top" align="left" bgColor="#d8e4ef">
													QuickStart Menu
												</th>
											</tr>
											<tr>
												<td id="tree" width="50%" bgColor="#f7f7f7">
													<table cellSpacing="4" cellPadding="4" width="100%" border="0">
														<tr style="FONT-SIZE: 11px">
															<td align="center" width="32" bgColor="#eaf0f7"><IMG height="32" src="images/import.btn.gif" width="32"></td>
															<td bgColor="#eaf0f7"><A class="quicklink" href="#"><strong>Add New Files</strong></A><br>
																Upload files from your PC into docAssist.</td>
														</tr>
														<tr id="trScan" style="FONT-SIZE: 11px" runat="server">
															<td align="center" width="32" bgColor="#d8e4ef"><IMG height="32" src="images/scan.btn.gif" width="32"></td>
															<td bgColor="#d8e4ef"><A onclick="parent.location.href='../DocumentManager.aspx';" href="#"><strong><span class="quicklink">Scan 
																			Documents</span><br>
																	</strong></A>Scan paper documents and view them in docAssist.
															</td>
														</tr>
														<tr style="FONT-SIZE: 11px">
															<td align="center" width="32" bgColor="#eaf0f7"><IMG height="32" src="images/search.btn.gif" width="32"></td>
															<td bgColor="#eaf0f7"><A class="quicklink" href="#" onclick="parent.window.advancedSearch();">Advanced Search</A><br>
																Search by document type or narrow your search using this interactive tool.
															</td>
														</tr>
														<tr id="trUsers" style="FONT-SIZE: 11px" runat="server">
															<td align="center" width="32" bgColor="#d8e4ef"><IMG height="32" src="images/users.btn.gif" width="32"></td>
															<td bgColor="#d8e4ef"><A onclick="SetUser();" href="#">Add New Users</A><br>
																Create and mange new user accounts.
															</td>
														</tr>
														<tr id="trGroups" style="FONT-SIZE: 11px" runat="server">
															<td align="center" width="32" bgColor="#eaf0f7"><IMG height="32" src="images/groups.btn.gif" width="32"></td>
															<td bgColor="#eaf0f7"><A class="quicklink" onclick="AddGroup();" href="#">Add New 
																	Groups</A>
																<br>
																Create and mange new user group security.
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
									<td id="borders" vAlign="top" width="50%">
										<table height="1100%" cellSpacing="5" cellPadding="5" width="100%" border="0">
											<tr>
												<th vAlign="top" align="left" bgColor="#f7f7f7">
													Recent Items</th></tr>
											<tr>
												<td vAlign="top" width="50%"><asp:datagrid id="dgRecentItems" runat="server" Width="100%" AutoGenerateColumns="False" ShowHeader="False">
														<Columns>
															<asp:TemplateColumn HeaderText="Icon">
																<HeaderStyle Width="30px"></HeaderStyle>
																<ItemTemplate>
																	<asp:Label id=RecentKey runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.RecentKey") %>' Visible="False">
																	</asp:Label>
																	<asp:Label id=Value runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Value") %>' Visible="False">
																	</asp:Label>
																	<asp:Label id=KeyValue runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.KeyValue") %>' Visible="False">
																	</asp:Label>
																	<asp:Image id="imgIcon" runat="server"></asp:Image>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn HeaderText="Description">
																<HeaderStyle Width="100%"></HeaderStyle>
																<ItemTemplate>
																		<asp:Label id="lblDesc" runat="server"></asp:Label>
																</ItemTemplate>
															</asp:TemplateColumn>
														</Columns>
													</asp:datagrid></td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
							<p>Have more questions? Click "Help" above.
							</p>
						</div>
					</td>
				</tr>
			</table>
			</TD></TR></TABLE>
			<script language="javascript">
	
				function SetUser()
				{
					var d = new Date();
					var time = d.getHours()+d.getMinutes()+d.getSeconds()+d.getMilliseconds();
					var settings = "help:no;status:yes;resizable:no;dialogHeight=422px;dialogWidth=656px";
					var returnValue = window.showModalDialog('../UserEdit.aspx?Id=0&Mode=NewEdit&Time='+time,null,settings);
					if (returnValue == "1") { window.location.replace('UserAdmin.aspx'); }
				}
				
				function AddGroup()
				{
				var nXpos = (document.body.clientWidth - 400) / 2; 
				var nYpos = (document.body.clientHeight - 200) / 2;
				window.open('../SecurityGroupEdit.aspx?ID=0','EditGroup','height=700,width=895,resizable=no,status=no,toolbar=no')
				//window.showModalDialog('../SecurityGroupEdit.aspx?ID=0',null,'help:no;status:no;resizable:no;dialogHeight=650px;dialogWidth=925px')
				}
				
				function quickSearch(val)
				{
					window.parent.tagSearch(val);
				}
				
			</script>
		</form>
	</body>
</HTML>

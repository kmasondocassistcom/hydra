<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ManageMapRemove.aspx.vb" Inherits="docAssistWeb.ManageMapRemove"%>
<%@ Register TagPrefix="uc1" TagName="AdminNav" Src="Controls/AdminNav.ascx" %>
<%@ Register TagPrefix="uc1" TagName="docHeader" Src="Controls/docHeader.ascx" %>
<%@ Register TagPrefix="uc1" TagName="TopRight" Src="Controls/TopRight.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>docAssist - Manage Integration</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<TABLE height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TBODY>
					<TR>
						<TD width="100%" colSpan="5">
							<uc1:docHeader id="docHeader" runat="server"></uc1:docHeader>
						</TD>
					</TR>
					<TR vAlign="middle" height="100%">
						<TD class="Navigation1" height="100%" style="BORDER-RIGHT: #cccccc thin solid" vAlign="top"><IMG height="1" src="dummy.gif" width="120" DESIGNTIMEDRAGDROP="101">
							<uc1:AdminNav id="AdminNav1" runat="server"></uc1:AdminNav></TD>
						<TD vAlign="top" align="center" height="100%"><BR>
							<table cellSpacing="0" cellPadding="0" width="100%" border="0">
								<tr>
									<td><IMG height="1" src="dummy.gif" width="35"><IMG src="Images/manage_roles.gif"></td>
								</tr>
							</table>
							<table height="100%" cellSpacing="0" cellPadding="0" width="100%">
					</TR>
					<tr height="50%">
					</tr>
					<tr height="15" width="100%">
						<td></td>
						<td></td>
						<td>
							<asp:Label id="Label1" runat="server" Width="384px" Height="16px" Font-Size="8.25pt" Font-Names="Verdana">Select document integration to remove:</asp:Label><BR>
							<asp:ListBox id="lstDocuments" runat="server" Width="440px" Height="220px" Font-Size="8.25pt"
								Font-Names="Verdana"></asp:ListBox></td>
						<td></td>
					</tr>
					<tr height="15" width="100%">
						<td></td>
						<td style="WIDTH: 72px" align="left"></td>
						<TD align="right">
							<P><asp:imagebutton id="btnCancel" runat="server" ImageUrl="Images/btn_canceladmin.gif"></asp:imagebutton>&nbsp;<asp:imagebutton id="btnNext" runat="server" ImageUrl="Images/btn_finish.gif"></asp:imagebutton></P>
						</TD>
						<TD width="50%"></TD>
					<TR height="15" width="100%">
						<td style="HEIGHT: 21px" width="50%"></td>
						<td style="WIDTH: 72px; HEIGHT: 21px" align="left">&nbsp;
						</td>
						<td style="HEIGHT: 21px"></td>
						<td style="HEIGHT: 21px" width="50%"></td>
					</TR>
					<tr height="15" width="100%">
						<td style="WIDTH: 393px" width="393"></td>
						<td style="WIDTH: 72px; HEIGHT: 17px">
							<P align="left">&nbsp;&nbsp;
							</P>
						</td>
						<td style="HEIGHT: 17px"></td>
						<td width="50%"></td>
					</tr>
					<tr height="15" width="100%">
						<td style="WIDTH: 393px" width="393"></td>
						<td style="WIDTH: 72px" vAlign="middle">
							<P align="left">&nbsp;&nbsp;</P>
						</td>
						<td align="right"></td>
						<td width="50%"></td>
					</tr>
					<tr height="15" width="100%">
						<td style="WIDTH: 393px" width="395"></td>
						<td style="WIDTH: 72px; HEIGHT: 17px"><BR>
						</td>
						<td style="HEIGHT: 17px"></td>
						<td width="50%"></td>
					</tr>
					<tr height="15" width="100%">
						<td style="WIDTH: 395px" width="395"></td>
						<td style="WIDTH: 72px"><BR>
							<P>&nbsp;</P>
						</td>
						<td align="right"></td>
						<td width="50"></td>
					</tr>
					<tr height="100%">
						<td></td>
					</tr>
				</tr>
			</TABLE>
			<P><BR>
				<BR>
			</P>
			</TD>
			<TD class="RightContent" height="100%">&nbsp;</TD>
			</TR>
			<TR>
				<TD class="PageFooter" vAlign="bottom" bgColor="#cccccc" colSpan="3">
					<uc1:TopRight id="docFooter" runat="server"></uc1:TopRight></TD>
			</TR>
			<TR height="1">
				<TD style="BACKGROUND-COLOR: #959394" width="20"><IMG height="1" src="images/spacer.gif" width="20"></TD>
				<TD style="BACKGROUND-COLOR: #959394" width="100%"><IMG height="1" src="images/spacer.gif" width="100%"></TD>
				<TD style="BACKGROUND-COLOR: #959394" width="20"><IMG height="1" src="images/spacer.gif" width="20"></TD>
			</TR>
			</TBODY></TABLE>
		</form>
	</body>
</HTML>

<%@ Page Language="vb" AutoEventWireup="false" Codebehind="CreateODBCSource.aspx.vb" Inherits="docAssistWeb.CreateODBCSource"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Create ODBC Source</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body class="Prefs">
		<form id="Form1" method="post" runat="server">
			<table width="350">
				<tr bgcolor="#f5f5f5">
					<td class="LabelName">ODBC Name:</td>
					<td class="Text">
						<asp:TextBox id="ODBCName" runat="server" Width="150px"></asp:TextBox>
						<asp:RequiredFieldValidator id="valRange" runat="server" ErrorMessage="*" ControlToValidate="ODBCName" CssClass="Error"></asp:RequiredFieldValidator></td>
				</tr>
				<tr>
					<td class="LabelName">Object Name:</td>
					<td class="Text">
						<asp:TextBox id="ObjectName" runat="server" Width="150px"></asp:TextBox>
						<asp:RequiredFieldValidator id="valObjectName" runat="server" ErrorMessage="*" ControlToValidate="ObjectName"
							CssClass="Error"></asp:RequiredFieldValidator></td>
				</tr>
				<tr bgcolor="#f5f5f5">
					<td class="LabelName">User ID:</td>
					<td class="Text">
						<asp:TextBox id="UserID" runat="server" Width="150px"></asp:TextBox>
						<asp:RequiredFieldValidator id="valUserId" runat="server" ErrorMessage="*" ControlToValidate="UserID" CssClass="Error"></asp:RequiredFieldValidator></td>
				</tr>
				<tr>
					<td class="LabelName">Password:</td>
					<td class="Text">
						<asp:TextBox id="Password" runat="server" Width="150px" TextMode="Password"></asp:TextBox>
						<asp:RequiredFieldValidator id="RequiredFieldValidator1" runat="server" ErrorMessage="*" ControlToValidate="Password"
							CssClass="Error"></asp:RequiredFieldValidator></td>
				</tr>
				<tr bgcolor="#f5f5f5">
					<td class="LabelName">Confirm Password:</td>
					<td class="Text">
						<asp:TextBox id="ConfirmPassword" runat="server" Width="150px" TextMode="Password"></asp:TextBox>
						<asp:RequiredFieldValidator id="RequiredFieldValidator2" runat="server" ErrorMessage="*" ControlToValidate="ConfirmPassword"
							CssClass="Error"></asp:RequiredFieldValidator></td>
				</tr>
				<tr>
					<td colspan="2" align="right">
						<asp:Label id="Err" runat="server" CssClass="Error" Visible="False" Font-Size="8pt" ForeColor="Red"></asp:Label>
					</td>
				</tr>
				<tr>
					<td colspan="2" align="right">
						<asp:ImageButton id="Save" runat="server" ImageUrl="Images/save_small.gif"></asp:ImageButton>
					</td>
				</tr>
			</table>
			<INPUT id="hidObjectId" type="hidden" runat="server">
		</form>
	</body>
</HTML>

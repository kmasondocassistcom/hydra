<%@ Page Language="vb" AutoEventWireup="false" Codebehind="DocumentText.aspx.vb" Inherits="docAssistWeb.DocumentText" enableViewState="False"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>docAssist - Document Text</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="css/core.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body>
		<form id="DocumentText" method="post" runat="server">
			<table width="100%">
				<tr nowrap>
					<td nowrap align="right"><asp:textbox id="SearchText" runat="server" Width="150px"></asp:textbox>&nbsp;
						<form action="#">
							<button type="button" class="btn primary" id="Search" runat="server"><span><span>Search</span></span></button>
						</form>
					</td>
				</tr>
			</table>
			<div id="tabs" style="MARGIN-LEFT: 15px; WIDTH: 480px; HEIGHT: 100%">
				<asp:label id="lblSummary" runat="server"></asp:label>
			</div>
		</form>
		<script language="javascript">
			function keyPressed(){
				if (event.keyCode == 13)
				{
					event.cancelBubble = true;
					event.returnValue = false;
					document.all.Search.click();
				}
			}
		</script>
	</body>
</HTML>

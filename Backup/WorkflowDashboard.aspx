<%@ Register TagPrefix="uc1" TagName="docHeader" Src="Controls/docHeader.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="WorkflowDashboard.aspx.vb"
    Inherits="docAssistWeb.WorkflowDashboard" EnableViewState="True" %>

<%@ Register TagPrefix="uc1" TagName="TopRight" Src="Controls/TopRight.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>docAssist - Workflow Dashboard</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <meta http-equiv="cache-control" content="no-cache">
    <link href="css/workflowDash.css" type="text/css" rel="stylesheet">
    <script src="scripts/infoBar.js" type="text/javascript"></script>
</head>
<body bottommargin="0" leftmargin="0" topmargin="0" rightmargin="0">
    <div id='GB_overlay' style="display: none">
    </div>
    <div id='GB_window' style="display: none; overflow: hidden">
        <div id='GB_caption' style="display: none">
        </div>
        <img id='GB_img' style="display: none" src='./images/close.gif' alt='Close Viewer'></div>
    <div id="service" style="behavior: url(webservice.htc)">
    </div>
    <form id="frmWorkflowDashboard" method="post" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <table cellspacing="0" cellpadding="0" width="100%" border="0" height="100%">
        <tr>
            <td>
                <uc1:docHeader ID="docHeader" runat="server"></uc1:docHeader>
            </td>
        </tr>
        <tr height="100%">
            <td valign="top" height="100%">
                <table cellpadding="0" cellspacing="0" width="100%" border="0" height="100%">
                    <tr>
                        <td align="center">
                            <table class="workflowDashboardMenu" height="100%" cellspacing="0" cellpadding="3"
                                width="99%" border="0">
                                <tr>
                                    <td class="selectedTab" id="current" style="cursor: hand" onclick="currentViewTog();"
                                        nowrap align="center" bgcolor="#f4f8fb">
                                        &nbsp;&nbsp;Current Workflows&nbsp;&nbsp;
                                    </td>
                                    <td class="unselectedTab" id="history" style="cursor: hand" onclick="historyView();"
                                        nowrap align="center" bgcolor="#d8e4ef">
                                        &nbsp;Historical Workflows
                                    </td>
                                    <td class="unselectedTab" width="100%" bgcolor="#d8e4ef">
                                        &nbsp;
                                    </td>
                                    <td class="unselectedTab" nowrap align="right" bgcolor="#d8e4ef" colspan="2">
                                        <span id="temp"></span><span id="toggle" runat="server">
                                            <img id="togImage" style="cursor: hand" onclick="togCollapse();" height="16" src="Images/workflow_dashboard/wf_minus.gif"
                                                width="17" border="0">&nbsp;<span id="Collapse" style="cursor: hand" onclick="togCollapse();">Collapse
                                                    All</span>&nbsp;</span>
                                    </td>
                                </tr>
                                <tr id="HistoryBar" align="right" bgcolor="#f4f8fb" style="display: none">
                                    <td nowrap align="left" width="100%" colspan="4">
                                        Workflow:
                                        <asp:DropDownList ID="ddlWorkflow" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                        &nbsp;&nbsp;&nbsp;Action:
                                        <asp:DropDownList ID="ddlAction" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                        <br>
                                        &nbsp;&nbsp;&nbsp;&nbsp; Queue:
                                        <asp:DropDownList ID="ddlQueue" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                        <td valign="bottom" nowrap align="left">
                                            <input id="rdoFixed" onclick="togRange(true);togFixed(false);" type="radio" checked
                                                name="grp" runat="server" value="rdoFixed">
                                            <asp:DropDownList ID="ddlFixed" runat="server">
                                                <asp:ListItem Value="0">today</asp:ListItem>
                                                <asp:ListItem Value="1">yesterday</asp:ListItem>
                                                <asp:ListItem Value="2">last 7 days</asp:ListItem>
                                                <asp:ListItem Value="3">last 30 days</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label><br>
                                            <input id="rdoRange" onclick="togRange(false);togFixed(true);" type="radio" name="grp"
                                                runat="server" value="rdoRange">
                                            <asp:DropDownList ID="FromMonth" runat="server">
                                                <asp:ListItem Value="01">Jan</asp:ListItem>
                                                <asp:ListItem Value="02">Feb</asp:ListItem>
                                                <asp:ListItem Value="03">Mar</asp:ListItem>
                                                <asp:ListItem Value="04">Apr</asp:ListItem>
                                                <asp:ListItem Value="05">May</asp:ListItem>
                                                <asp:ListItem Value="06">Jun</asp:ListItem>
                                                <asp:ListItem Value="07">Jul</asp:ListItem>
                                                <asp:ListItem Value="08">Aug</asp:ListItem>
                                                <asp:ListItem Value="09">Sep</asp:ListItem>
                                                <asp:ListItem Value="10">Oct</asp:ListItem>
                                                <asp:ListItem Value="11">Nov</asp:ListItem>
                                                <asp:ListItem Value="12">Dec</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:DropDownList ID="FromDay" runat="server">
                                                <asp:ListItem Value="01">01</asp:ListItem>
                                                <asp:ListItem Value="02">02</asp:ListItem>
                                                <asp:ListItem Value="03">03</asp:ListItem>
                                                <asp:ListItem Value="04">04</asp:ListItem>
                                                <asp:ListItem Value="05">05</asp:ListItem>
                                                <asp:ListItem Value="06">06</asp:ListItem>
                                                <asp:ListItem Value="07">07</asp:ListItem>
                                                <asp:ListItem Value="08">08</asp:ListItem>
                                                <asp:ListItem Value="09">09</asp:ListItem>
                                                <asp:ListItem Value="10">10</asp:ListItem>
                                                <asp:ListItem Value="11">11</asp:ListItem>
                                                <asp:ListItem Value="12">12</asp:ListItem>
                                                <asp:ListItem Value="13">13</asp:ListItem>
                                                <asp:ListItem Value="14">14</asp:ListItem>
                                                <asp:ListItem Value="15">15</asp:ListItem>
                                                <asp:ListItem Value="16">16</asp:ListItem>
                                                <asp:ListItem Value="17">17</asp:ListItem>
                                                <asp:ListItem Value="18">18</asp:ListItem>
                                                <asp:ListItem Value="19">19</asp:ListItem>
                                                <asp:ListItem Value="20">20</asp:ListItem>
                                                <asp:ListItem Value="21">21</asp:ListItem>
                                                <asp:ListItem Value="22">22</asp:ListItem>
                                                <asp:ListItem Value="23">23</asp:ListItem>
                                                <asp:ListItem Value="24">24</asp:ListItem>
                                                <asp:ListItem Value="25">25</asp:ListItem>
                                                <asp:ListItem Value="26">26</asp:ListItem>
                                                <asp:ListItem Value="27">27</asp:ListItem>
                                                <asp:ListItem Value="28">28</asp:ListItem>
                                                <asp:ListItem Value="29">29</asp:ListItem>
                                                <asp:ListItem Value="30">30</asp:ListItem>
                                                <asp:ListItem Value="31">31</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:DropDownList ID="FromYear" runat="server">
                                            </asp:DropDownList>
                                            &nbsp;to
                                            <asp:DropDownList ID="ToMonth" runat="server">
                                                <asp:ListItem Value="01">Jan</asp:ListItem>
                                                <asp:ListItem Value="02">Feb</asp:ListItem>
                                                <asp:ListItem Value="03">Mar</asp:ListItem>
                                                <asp:ListItem Value="04">Apr</asp:ListItem>
                                                <asp:ListItem Value="05">May</asp:ListItem>
                                                <asp:ListItem Value="06">Jun</asp:ListItem>
                                                <asp:ListItem Value="07">Jul</asp:ListItem>
                                                <asp:ListItem Value="08">Aug</asp:ListItem>
                                                <asp:ListItem Value="09">Sep</asp:ListItem>
                                                <asp:ListItem Value="10">Oct</asp:ListItem>
                                                <asp:ListItem Value="11">Nov</asp:ListItem>
                                                <asp:ListItem Value="12">Dec</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:DropDownList ID="ToDay" runat="server">
                                                <asp:ListItem Value="01">01</asp:ListItem>
                                                <asp:ListItem Value="02">02</asp:ListItem>
                                                <asp:ListItem Value="03">03</asp:ListItem>
                                                <asp:ListItem Value="04">04</asp:ListItem>
                                                <asp:ListItem Value="05">05</asp:ListItem>
                                                <asp:ListItem Value="06">06</asp:ListItem>
                                                <asp:ListItem Value="07">07</asp:ListItem>
                                                <asp:ListItem Value="08">08</asp:ListItem>
                                                <asp:ListItem Value="09">09</asp:ListItem>
                                                <asp:ListItem Value="10">10</asp:ListItem>
                                                <asp:ListItem Value="11">11</asp:ListItem>
                                                <asp:ListItem Value="12">12</asp:ListItem>
                                                <asp:ListItem Value="13">13</asp:ListItem>
                                                <asp:ListItem Value="14">14</asp:ListItem>
                                                <asp:ListItem Value="15">15</asp:ListItem>
                                                <asp:ListItem Value="16">16</asp:ListItem>
                                                <asp:ListItem Value="17">17</asp:ListItem>
                                                <asp:ListItem Value="18">18</asp:ListItem>
                                                <asp:ListItem Value="19">19</asp:ListItem>
                                                <asp:ListItem Value="20">20</asp:ListItem>
                                                <asp:ListItem Value="21">21</asp:ListItem>
                                                <asp:ListItem Value="22">22</asp:ListItem>
                                                <asp:ListItem Value="23">23</asp:ListItem>
                                                <asp:ListItem Value="24">24</asp:ListItem>
                                                <asp:ListItem Value="25">25</asp:ListItem>
                                                <asp:ListItem Value="26">26</asp:ListItem>
                                                <asp:ListItem Value="27">27</asp:ListItem>
                                                <asp:ListItem Value="28">28</asp:ListItem>
                                                <asp:ListItem Value="29">29</asp:ListItem>
                                                <asp:ListItem Value="30">30</asp:ListItem>
                                                <asp:ListItem Value="31">31</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:DropDownList ID="ToYear" runat="server">
                                            </asp:DropDownList>
                                            <asp:Button ID="btnView" runat="server" Width="28px" Height="22px" Text="Go"></asp:Button>
                                        </td>
                                </tr>
                                <tr id="HistoryBar2" style="display: none">
                                    <td nowrap bgcolor="#f4f8fb" colspan="4">
                                        <span id="View" runat="server"></span>
                                    </td>
                                    <td valign="bottom" nowrap bgcolor="#f4f8fb">
                                    </td>
                                </tr>
                                <tr style="display: none">
                                    <td valign="bottom" colspan="5" height="100%">
                                        <asp:Button ID="btnMassApproval" runat="server" Width="80px" Height="22px" Text="Submit">
                                        </asp:Button><br>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr height="100%">
                        <td align="center" valign="top">
                            <div id="wfBody" style="overflow: auto; width: 99%">
                                <table width="100%" id="tblSort" runat="server">
                                    <tr>
                                        <td nowrap>
                                            <asp:Label ID="lblPending" runat="server" CssClass="heading"></asp:Label>
                                        </td>
                                        <td align="right" class="heading" valign="bottom">
                                            Sort by:<asp:DropDownList ID="ddlSort" runat="server" AutoPostBack="True">
                                                <asp:ListItem Value="ImageID">Document No.</asp:ListItem>
                                                <asp:ListItem Value="Title">Title</asp:ListItem>
                                                <asp:ListItem Value="Description">Description</asp:ListItem>
                                                <asp:ListItem Value="DocumentName">Document Type</asp:ListItem>
                                                <asp:ListItem Value="ActionDateTime" Selected="True">Posted/Pending</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                                <asp:UpdateProgress ID="updateProgress" runat="server">
                                    <ProgressTemplate>
                                        <img src="./Images/ajaxIcons/bigLoad.gif" />
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                                <asp:UpdatePanel ID="uResults" runat="server">
                                    <ContentTemplate>
                                        <asp:Label ID="lblResults" runat="server" Width="100%"></asp:Label>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="btnCurrentView" EventName="Click" />
                                        <asp:AsyncPostBackTrigger ControlID="ddlSort" EventName="SelectedIndexChanged" />
                                        <asp:AsyncPostBackTrigger ControlID="btnView" EventName="Click" />
                                    </Triggers>
                                </asp:UpdatePanel>
                                <asp:ImageButton ID="btnReload" runat="server" Height="0px" Width="0px"></asp:ImageButton>
                                <asp:Literal ID="litQueues" runat="server"></asp:Literal><asp:Literal ID="litInfo"
                                    runat="server"></asp:Literal>
                                <asp:Literal ID="Collapse" runat="server"></asp:Literal>
                                <asp:Literal ID="Expand" runat="server"></asp:Literal>
                                <asp:Literal ID="Scripting" runat="server"></asp:Literal>
                            </div>
                        </td>
                    </tr>
                </table>
                <asp:Button ID="btnCurrentView" runat="server"></asp:Button>
            </td>
        </tr>
        <tr>
            <td>
                <uc1:TopRight ID="docFooter" runat="server"></uc1:TopRight>
                <input id="hiddenApprovals" type="hidden" runat="server" name="hiddenApprovals">
                <div style="display: none">
                </div>
            </td>
        </tr>
    </table>
    </form>

    <script src="scripts/cookies.js"></script>

    <script src="scripts/jquery.js"></script>

    <script src="scripts/workflowDashboard.js"></script>

    <script language="javascript">
    </script>

</body>
</html>

Imports System.IO
Imports System.Web.SessionState
Imports System.Runtime.Serialization
Imports System.Runtime.Serialization.SerializationBinder
Imports System.Reflection

Namespace _Index

    Partial Class _Index
        Inherits System.Web.UI.Page

#Region "Declarations"

        Private mobjUser As Accucentric.docAssist.Web.Security.User
        Private mcookieSearch As cookieSearch

        Private mconSqlImage As SqlClient.SqlConnection

        Protected WithEvents frameAttributes As System.Web.UI.HtmlControls.HtmlGenericControl
        Protected WithEvents txtFileNameServer As System.Web.UI.WebControls.TextBox
        Protected WithEvents lblFileNameClient As System.Web.UI.WebControls.Label
        Protected WithEvents lblDocumentId As System.Web.UI.WebControls.Label
        Protected WithEvents litScripts As System.Web.UI.WebControls.Literal
        Protected WithEvents litServices As System.Web.UI.WebControls.Literal
        Protected WithEvents lblVersionId As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents lblPageNo As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents lblFileNameResult As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents litStartService As System.Web.UI.WebControls.Literal
        Protected WithEvents lblAttributeError As System.Web.UI.WebControls.Label
        Protected WithEvents txtShowImage As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents litShowViewer As System.Web.UI.WebControls.Literal
        Protected WithEvents litHideProgress As System.Web.UI.WebControls.Literal
        Protected WithEvents litJavascript As System.Web.UI.WebControls.Literal
        Protected WithEvents tblWorkflow As System.Web.UI.HtmlControls.HtmlTable
        Protected WithEvents frameWorkflow As System.Web.UI.HtmlControls.HtmlGenericControl
        Protected WithEvents WorkflowTabs As System.Web.UI.HtmlControls.HtmlTableRow
        Protected WithEvents navIndex As IndexNavigation
        Protected WithEvents IndexItems As System.Web.UI.HtmlControls.HtmlGenericControl
        Protected WithEvents txtUserId As System.Web.UI.WebControls.Label
        Protected WithEvents txtUser As System.Web.UI.WebControls.TextBox
        Protected WithEvents Popup As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents tabThumbnails As System.Web.UI.HtmlControls.HtmlImage
        Protected WithEvents Td1 As System.Web.UI.HtmlControls.HtmlTableCell
        Protected WithEvents frameWorkflowHistory As System.Web.UI.HtmlControls.HtmlGenericControl
        Protected WithEvents ImageType As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents VersionId As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents ShowWorkflow As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents dgDistribution As System.Web.UI.WebControls.DataGrid
        Protected WithEvents btnWf As System.Web.UI.WebControls.ImageButton
        Protected WithEvents mode As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents routeid As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents hostname As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents Hidden1 As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents pageNo As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents accountId As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents Hidden2 As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents DownloadQuality As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents objViewer As Atalasoft.Imaging.WebControls.Annotations.WebAnnotationViewer
        Protected WithEvents pageCount As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents docId As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents pageNumber As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents Annotation As Atalasoft.Imaging.WebControls.Annotations.WebAnnotationViewer
        Protected WithEvents frameFile As System.Web.UI.HtmlControls.HtmlGenericControl
        Protected WithEvents tdAnnotations As System.Web.UI.HtmlControls.HtmlTableCell
        Protected WithEvents rdoCurrent As System.Web.UI.WebControls.RadioButton
        Protected WithEvents rdoAllPages As System.Web.UI.WebControls.RadioButton
        Protected WithEvents tblThumbnails As System.Web.UI.HtmlControls.HtmlTable

#End Region

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents Literal1 As System.Web.UI.WebControls.Literal
        Protected WithEvents litUnlockImageCtrl As System.Web.UI.WebControls.Literal

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Response.Redirect("Search.aspx" & Request.Url.Query & "&loadversion=" & Request.QueryString("VID"))
        End Sub

    End Class

End Namespace
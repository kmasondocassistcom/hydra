Partial Class AttributeNavigation
    Inherits System.Web.UI.UserControl

    Dim objUser As Accucentric.docAssist.Web.Security.User

    Dim intResultsPerPage As Integer
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents A1 As System.Web.UI.HtmlControls.HtmlAnchor
    'Protected WithEvents BackToResults As System.Web.UI.HtmlControls.HtmlImage

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Public Event SaveEvent(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)

    Friend ReadOnly Property SaveButton() As ImageButton
        Get
            Return btnSave
        End Get
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Hide Previous/Next result buttons if our cookie isn't populated. This means we don't have a saved search.

        btnNext.Attributes.Add("onClick", "document.all.moveNext.value=1")
        btnPrevious.Attributes.Add("onClick", "document.all.moveNext.value=1")
        'BackToResults.Attributes.Add("onClick", "document.all.moveNext.value=2")

        If Not Request.Cookies("docAssistImage") Is Nothing Then
            If Request.Cookies("docAssistImage")("SearchPostition") = Nothing Then
                btnNext.Attributes("style") = "DISPLAY: none"
                btnPrevious.Attributes("style") = "DISPLAY: none"
            Else
                Dim objcookie As New cookieSearch(Me.Page)
                objUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))

                'Disable previous result if we are on the first result
                Dim intCurrentResult As Integer = ((objcookie.PageNo - 1) * objUser.DefaultResultCount) + objcookie.Row

                If intCurrentResult = 1 Then
                    'Disable previous button
                    btnPrevious.ImageUrl = "Images/results_previous_off.gif"
                End If

                If intCurrentResult = objcookie.TotalResults Then
                    'Disable next button
                    btnNext.ImageUrl = "Images/results_next_off.gif"
                End If

            End If

            If Not Page.IsPostBack Then

                If Request.Cookies("docAssistImage")("HideBackToResults") = "1" Then
                    'Hide return to results button because user has been automatically sent here
                    lnkResults.Visible = False
                End If

                If Not Request.Cookies("docAssistImage")("serverFileName") Is Nothing _
                            And Not Request.Cookies("docAssistImage")("localFileName") Is Nothing Then
                    lnkVersionWindow.Visible = False
                End If
            End If
        Else
            AttributeNavigation.Visible = False
        End If
    End Sub

    Private Sub btnNext_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnNext.Click

        Dim objcookie As New cookieSearch(Me.Page)
        Dim dvResults As New DataView

        objUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))

        Dim intCurrentResult As Integer = ((objcookie.PageNo - 1) * objUser.DefaultResultCount) + objcookie.Row

        If intCurrentResult < objcookie.TotalResults Then
            'Check if we are on the first result of the current page and change the number of the page if we have to
            If objcookie.Row = objUser.DefaultResultCount Then
                objcookie.PageNo = objcookie.PageNo + 1
                Functions.GetSearchResults(objcookie, objUser, dvResults)
                'take max per page result row as the new result
                objcookie.VersionID = dvResults(objUser.DefaultResultCount + 1)("VersionId")
                Session(objcookie.VersionID & "VersionControl") = Accucentric.docAssist.Data.GetVersionControl(objcookie.VersionID, Accucentric.docAssist.Web.Security.BuildConnection(objUser))

                'Response.Cookies("docAssistImage")("VersionID") = dvResults.Table.Rows(objUser.DefaultResultCount).Item("VersionId")
                'Server.Transfer("Index.aspx", False)
                Page.Response.Write("<SCRIPT language=javascript>parent.window.location = 'Index.aspx';</SCRIPT>")
                Page.Response.Flush()
            Else
                'objcookie.PageNo = objcookie.PageNo - 1
                Functions.GetSearchResults(objcookie, objUser, dvResults)

                objcookie.Row = objcookie.Row + 1
                objcookie.VersionID = dvResults(objcookie.Row - 1)("VersionId")

                Session(objcookie.VersionID & "VersionControl") = Accucentric.docAssist.Data.GetVersionControl(objcookie.VersionID, Accucentric.docAssist.Web.Security.BuildConnection(objUser))
                'objcookie.VersionID = dvResults(5)("VersionId")

                'Response.Cookies("docAssistImage")("VersionID") = dvResults(objcookie.Row - 1)("VersionId")
                'Server.Transfer("Index.aspx", False)
                'Response.Redirect("Index.aspx") 
                Page.Response.Write("<SCRIPT language=javascript>parent.window.location = 'Index.aspx';</SCRIPT>")
                Page.Response.Flush()

            End If

        End If


    End Sub

    Private Sub btnPrevious_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrevious.Click

        Dim objcookie As New cookieSearch(Me.Page)
        Dim dvResults As New DataView

        objUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))

        Dim intCurrentResult As Integer = ((objcookie.PageNo - 1) * objUser.DefaultResultCount) + objcookie.Row

        If intCurrentResult > 1 Then
            'Check if we are on the first result of the current page and change the number of the page if we have to
            If objcookie.Row = 1 And objcookie.PageNo > 1 Then
                objcookie.PageNo = objcookie.PageNo - 1
                Functions.GetSearchResults(objcookie, objUser, dvResults)
                'take max per page result row as the new result
                objcookie.VersionID = dvResults(objUser.DefaultResultCount - 1)("VersionId")
                'Response.Cookies("docAssistImage")("VersionID") = dvResults.Table.Rows(objUser.DefaultResultCount).Item("VersionId")
                'Server.Transfer("Index.aspx", False)
                Page.Response.Write("<SCRIPT language=javascript>parent.window.location = 'Index.aspx';</SCRIPT>")
                Page.Response.Flush()
            Else
                'objcookie.PageNo = objcookie.PageNo - 1
                Functions.GetSearchResults(objcookie, objUser, dvResults)

                objcookie.VersionID = dvResults(objcookie.Row - 2)("VersionId")
                objcookie.Row = objcookie.Row - 1
                'objcookie.VersionID = dvResults(5)("VersionId")

                'Response.Cookies("docAssistImage")("VersionID") = dvResults(objcookie.Row - 1)("VersionId")
                'Server.Transfer("Index.aspx", False)
                'Response.Redirect("Index.aspx")
                Page.Response.Write("<SCRIPT language=javascript>parent.window.location = 'Index.aspx';</SCRIPT>")
                Page.Response.Flush()
            End If

        End If
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        RaiseEvent SaveEvent(sender, e)
    End Sub
End Class

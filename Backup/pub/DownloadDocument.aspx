<%@ Page Language="vb" AutoEventWireup="false" Codebehind="DownloadDocument.aspx.vb" Inherits="docAssistWeb.DownloadDocument"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>docAssist - Download Document</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="../css/core.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body>
		<form id="frmDownloadDocument" method="post" runat="server">
			<table width="100%">
				<tr>
					<td align="center">
						<table width="600">
							<tr>
								<td><img src="../Images/docAssist.jpg"></td>
							</tr>
							<tr>
								<td><br>
									<asp:Label id="lblMessage" runat="server"></asp:Label>
								</td>
							</tr>
							<tr>
								<td align="center">
									<BUTTON class="btn large" id="btnDownload" type="button" runat="server"><SPAN><SPAN>Download 
												Document</SPAN></SPAN></BUTTON>
								</td>
							</tr>
							<tr>
								<td>
									<p><br>
										Thousands of users trust docAssist with their documents everyday. docAssist 
										allows you to scan your documents and store files for easy access from anywhere 
										in the world using your web browser. <a href="http://www.docassist.com">Learn 
											more... </a>
									</p>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>

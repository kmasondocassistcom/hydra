Imports Atalasoft.Imaging
Imports Atalasoft.Imaging.ImageProcessing
Imports Atalasoft.Annotate
Imports Atalasoft.Annotate.UI
Imports Atalasoft.Annotate.Renderer
Imports Atalasoft.Imaging.Codec.Tiff
Imports Atalasoft.Imaging.Codec
Imports Accucentric.docAssist.Data.Images

Partial Class DownloadDocument
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private conMaster As New SqlClient.SqlConnection

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Response.Expires = 0
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        Dim conCompany As New SqlClient.SqlConnection

        Try

            Dim accountID, id, type, emailaddress, key As String

            Dim enc As New Encryption.Encryption

            Dim items() As String = enc.Decrypt(Request.QueryString(0).ToString.Replace("~", "=").Replace("^", "+")).Split(";")

            accountID = items(0)
            id = items(1)
            type = items(2)
            key = items(3)
            emailaddress = items(4)

            Dim AccountDesc, DBServerDNS, DBName As String
            Dim enabled As Boolean

            Functions.getAccountInfo(accountID, AccountDesc, DBServerDNS, DBName, enabled)

            Dim strMasterDbPassword As String
            strMasterDbPassword = ConfigurationSettings.AppSettings("MasterPW")

            Dim conString As String = Functions.BuildConnectionString(DBServerDNS, DBName, "bluprint", enc.Decrypt(strMasterDbPassword))

            conCompany.ConnectionString = conString
            conCompany.Open()

            Dim cmd As New SqlClient.SqlCommand("SMSharePermVerify", conCompany)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@AccountID", accountID)
            cmd.Parameters.Add("@ShareType", type)
            cmd.Parameters.Add("@ShareKey", id)
            cmd.Parameters.Add("@ShareOption", "")
            cmd.Parameters.Add("@EmailAddress", emailaddress)
            cmd.Parameters.Add("@ShareGrantKey", key)

            Dim res As New SqlClient.SqlParameter("@Result", SqlDbType.Int)
            res.Direction = ParameterDirection.Output
            cmd.Parameters.Add(res)
            cmd.ExecuteNonQuery()

            Select Case res.Value
                Case -1 'Disabled or expired trial account
                    Dim sbE As New System.Text.StringBuilder
                    sbE.Append("<h1>This document is currently unavailable.</h1>")
                    sbE.Append("<p>Please contact the sender of this message for more information on why this file is unavailable.</p>")
                    btnDownload.Visible = False
                    lblMessage.Text = sbE.ToString
                    Exit Sub
                Case -2 'Document is no longer available.
                    Dim sbE As New System.Text.StringBuilder
                    sbE.Append("<h1>This document is currently unavailable.</h1>")
                    sbE.Append("<p>Please contact the sender of this message for more information on why this file is unavailable.</p>")
                    btnDownload.Visible = False
                    lblMessage.Text = sbE.ToString
                    Exit Sub
            End Select

            'Everything OK
            Dim sb As New System.Text.StringBuilder
            sb.Append("<h2>Your shared document is ready to be downloaded.</h2>")
            sb.Append("<p>To access this document click the 'Download Document' button below. If you need this file again in the future you can simply visit this link again.</p>")
            btnDownload.Visible = True
            lblMessage.Text = sb.ToString

        Catch ex As Exception

            Dim conMaster As SqlClient.SqlConnection
            conMaster = Functions.BuildMasterConnection
            Functions.LogException("", "", "", "Public share document error: " & ex.Message, ex.StackTrace, Server.MachineName, "", Request.UrlReferrer.ToString, conMaster, True)

        Finally

            If conMaster.State <> ConnectionState.Closed Then
                conMaster.Close()
                conMaster.Dispose()
            End If

            If conCompany.State <> ConnectionState.Closed Then
                conCompany.Close()
                conCompany.Dispose()
            End If

        End Try

    End Sub

    Private Sub btnDownload_ServerClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDownload.ServerClick

        Dim conCompany As New SqlClient.SqlConnection

        Try

            Dim accountID, id, type, emailaddress, key, fileType As String

            Dim enc As New Encryption.Encryption

            Dim items() As String = enc.Decrypt(Request.QueryString(0).ToString.Replace("~", "=").Replace("^", "+")).Split(";")

            accountID = items(0)
            id = items(1)
            type = items(2)
            key = items(3)
            emailaddress = items(4)
            fileType = items(5)

            Dim AccountDesc, DBServerDNS, DBName As String
            Dim enabled As Boolean

            Functions.getAccountInfo(accountID, AccountDesc, DBServerDNS, DBName, enabled)

            If enabled = False Then
                'TODO: Display 
                Exit Sub
            End If

            Dim strMasterDbPassword As String
            strMasterDbPassword = ConfigurationSettings.AppSettings("MasterPW")

            Dim conString As String = Functions.BuildConnectionString(DBServerDNS, DBName, "bluprint", enc.Decrypt(strMasterDbPassword))

            conCompany.ConnectionString = conString
            conCompany.Open()

            Select Case CType(fileType, ImageTypes)
                Case Constants.ImageTypes.ATTACHMENT

                    Dim bytes() As Byte

                    Dim objAttachments As New FileAttachments(conCompany)
                    Dim dt As New DataTable
                    dt = objAttachments.GetFile(id)
                    bytes = CType(dt.Rows(0)("Data"), Byte())

                    Dim strFilename As String = dt.Rows(0)("Filename")

                    Response.Clear()
                    Response.AddHeader("Content-Disposition", "attachment; filename=" & strFilename)
                    Response.AddHeader("Content-Length", bytes.Length)
                    Response.ContentType = "application/octet-stream"
                    Response.BinaryWrite(bytes)
                    Response.End()


                Case Constants.ImageTypes.SCAN

                    'Get the images from the database
                    Dim sqlCmd As New SqlClient.SqlCommand("acsImageDetailRange", conCompany)
                    sqlCmd.CommandType = CommandType.StoredProcedure
                    sqlCmd.Parameters.Add("@VersionID", id)
                    sqlCmd.Parameters.Add("@PageStart", 1)
                    sqlCmd.Parameters.Add("@PageEnd", 999999)

                    Dim dt As New DataTable
                    Dim da As New SqlClient.SqlDataAdapter(sqlCmd)
                    da.Fill(dt)

                    Dim col As New Atalasoft.Imaging.ImageCollection

                    For Each dr As DataRow In dt.Rows
                        Dim bytImage As Byte()
                        bytImage = dr("Image")
                        Dim tmpai As New Atalasoft.Imaging.AtalaImage
                        tmpai = tmpai.FromByteArray(bytImage)
                        tmpai.Resolution = New Atalasoft.Imaging.Dpi(150, 150, Atalasoft.Imaging.ResolutionUnit.DotsPerInch)
                        col.Add(tmpai)
                    Next

                    ' Create the PDF.
                    Dim pdf As New Atalasoft.Imaging.Codec.Pdf.PdfEncoder

                    ' Set any properties.
                    pdf.JpegQuality = 100
                    pdf.Metadata = New Atalasoft.Imaging.Codec.Pdf.PdfMetadata("docAssist Export", "docAssist", "Document Export", "", "", "docAssist", DateTime.Now, DateTime.Now)

                    ' Make each image fit into an 8.5 x 11 inch page (612 x 792 @ 72 DPI).
                    pdf.SizeMode = 1
                    pdf.PageSize = New Size(612, 792)

                    Dim bytes As Byte() = col.ToByteArray(pdf, Nothing)

                    Response.Clear()
                    Response.AddHeader("Content-Disposition", "attachment; filename=" & "docAssist_Document.pdf")
                    Response.AddHeader("Content-Length", bytes.Length)
                    Response.ContentType = "application/octet-stream"
                    Response.BinaryWrite(bytes)
                    Response.End()

            End Select

        Catch ex As Exception

            Dim conMaster As SqlClient.SqlConnection
            conMaster = Functions.BuildMasterConnection
            Functions.LogException("", "", "", "Public share document error: " & ex.Message, ex.StackTrace, Server.MachineName, "", Request.UrlReferrer.ToString, conMaster, True)

        Finally

            If conMaster.State <> ConnectionState.Closed Then
                conMaster.Close()
                conMaster.Dispose()
            End If

            If conCompany.State <> ConnectionState.Closed Then
                conCompany.Close()
                conCompany.Dispose()
            End If

        End Try

    End Sub

End Class

Imports Accucentric.docAssist

Partial Class ImageVersions
    Inherits System.Web.UI.Page

    Private mobjUser As Accucentric.docAssist.Web.Security.User
    Private mconSqlImage As SqlClient.SqlConnection
    Private mcookieSearch As cookieSearch

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        btnOk.Attributes("onclick") = "window.close();"

        Try
            Me.mobjUser = BuildUserObject(Request.Cookies("docAssist"))
            Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
            Me.mcookieSearch = New cookieSearch(Me.Page)
        Catch ex As Exception
            Response.Write("<script language='javascript'>alert('Your session has expired. Please login again before opening this window.');window.close();</script>")
        End Try

        Select Case CType(Session("FileType"), Functions.FileType)
            Case Functions.FileType.ATTACHMENT
                tdPageCount.Visible = False
                tdPageCountValue.Visible = False
                lastMod.BgColor = "#ffffff"
                lastModValue.BgColor = "#ffffff"
                lastUser.BgColor = "#cccccc"
                lastUserValue.BgColor = "#cccccc"
        End Select

        If Not Page.IsPostBack Then

            If Me.mcookieSearch.VersionID = -1 Then
                Me.mcookieSearch.VersionID = Request.QueryString("VID")
            End If

            Dim dsVersionControl As Accucentric.docAssist.Data.dsVersionControl = Session(Me.mcookieSearch.VersionID & "VersionControl")
            Dim dsVersions As New dsImageVersions

            Try
                '
                ' Populate the Version History DataGrid
                '
                Dim intImageId As Integer = CType(dsVersionControl.Images.Rows(0), Data.dsVersionControl.ImagesRow).ImageID
                Dim intVersionId As Integer = CType(dsVersionControl.ImageVersion.Rows(0), Data.dsVersionControl.ImageVersionRow).VersionID
                Dim da As SqlClient.SqlDataAdapter
                '
                ' Populate the docAssistWebImages Add and Modified Users Table
                Dim cmdSql As New SqlClient.SqlCommand("SELECT * FROM viewWebUsers", Me.mconSqlImage)
                da = New SqlClient.SqlDataAdapter(cmdSql)
                If Not Me.mconSqlImage.State = ConnectionState.Open Then Me.mconSqlImage.Open()
                da.Fill(dsVersions.viewWebUsersAdd)
                da.Fill(dsVersions.viewWebUsersModified)
                '
                ' Populate the ImageVersions table
                cmdSql = New SqlClient.SqlCommand("acsImageVersionsAllByImageId", Me.mconSqlImage)
                cmdSql.CommandType = CommandType.StoredProcedure
                cmdSql.Parameters.Add("@ImageId", intImageId)
                da = New SqlClient.SqlDataAdapter(cmdSql)
                da.Fill(dsVersions.ImageVersion)
                '
                ' Adust the time
                For Each drIv As dsImageVersions.ImageVersionRow In dsVersions.ImageVersion.Rows
                    drIv.ModifiedDate = drIv.ModifiedDate.AddHours(Me.mobjUser.TimeDiffFromGMT)
                Next

                dgVersions.DataSource = dsVersions.ImageVersion
                dgVersions.DataBind()
                '
                ' Populate the current version info
                lblAddedDateTime.Text = CType(dsVersionControl.ImageVersion.Rows(0), Data.dsVersionControl.ImageVersionRow).AddDate.AddHours(Me.mobjUser.TimeDiffFromGMT)
                lblPageCount.Text = CType(dsVersionControl.ImageVersion.Rows(0), Data.dsVersionControl.ImageVersionRow).SeqTotal.ToString("#,###")
                lblModifiedDateTime.Text = CType(dsVersionControl.ImageVersion.Rows(0), Data.dsVersionControl.ImageVersionRow).ModifiedDate.AddHours(Me.mobjUser.TimeDiffFromGMT)
                ' Get the user account names
                Dim drUser As dsImageVersions.ImageVersionRow = dsVersions.ImageVersion.Rows.Find(intVersionId)
                If Not drUser Is Nothing Then
                    lblAddedUser.Text = drUser.AddUserName
                    lblModifiedUser.Text = drUser.ModifiedUserName
                End If

                If Me.mobjUser.SysAdmin = True Then
                    'Get/Show detailed audit information
                    Dim dtAuditSummary As New DataTable
                    cmdSql = New SqlClient.SqlCommand("ImageVersionsAuditDetailByVersionId", Me.mconSqlImage)
                    cmdSql.CommandType = CommandType.StoredProcedure
                    cmdSql.Parameters.Add("@AccountID", Me.mobjUser.AccountId)
                    cmdSql.Parameters.Add("@VersionId", Me.mcookieSearch.VersionID)
                    cmdSql.Parameters.Add("@DiffFromGMT", Me.mobjUser.TimeDiffFromGMT)
                    da = New SqlClient.SqlDataAdapter(cmdSql)
                    da.Fill(dtAuditSummary)
                    dgAudit.DataSource = dtAuditSummary
                    dgAudit.DataBind()
                    AuditHeader.Visible = True
                    dgAudit.Visible = True
                Else
                    AuditHeader.Visible = False
                    dgAudit.Visible = False
                End If

            Catch exSql As SqlClient.SqlException
                Throw New Exception(exSql.Message)
            Catch ex As Exception
                Throw ex
            Finally
                If Not Me.mconSqlImage.State = ConnectionState.Closed Then Me.mconSqlImage.Close()
            End Try
        End If

    End Sub

    Private Sub dgAudit_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgAudit.ItemDataBound

        Try
            Dim lblDate As Label = CType(e.Item.FindControl("lblDate"), Label)
            If Not lblDate Is Nothing Then
                Dim dtDate As DateTime = CDate(lblDate.Text)
                lblDate.Text = dtDate.ToShortDateString
            End If
        Catch ex As Exception

        End Try

    End Sub

    Private Sub dgAudit_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgAudit.ItemCommand

        Try

            Select Case e.CommandName
                Case "Drill"

                    Dim conSqlCompany As New SqlClient.SqlConnection
                    Dim sb As New System.Text.StringBuilder

                    Dim details As New Label
                    Dim imageButton As New ImageButton
                    Try

                        imageButton = e.Item.FindControl("Toggle")

                        If imageButton.ImageUrl = "Images/plus.gif" Then

                            conSqlCompany = Functions.BuildConnection(Me.mobjUser)
                            Dim objViewer As New Data.Images.Viewer(conSqlCompany)
                            Dim ds As New DataSet
                            ds = objViewer.HistoryDetailDrill(Me.mobjUser.AccountId.ToString, CType(e.Item.FindControl("lblWebUserId"), Label).Text, Me.mcookieSearch.VersionID, CDate(CType(e.Item.FindControl("lbldate"), Label).Text).Date, Me.mobjUser.TimeDiffFromGMT)

                            Dim dt As DataTable = ds.Tables(0)
                            Dim emails As DataTable = ds.Tables(1)
                            If dt.Rows.Count = 0 Then
                                sb.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & "Details unavailable.")
                            End If

                            For Each dr As DataRow In dt.Rows
                                sb.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & CDate(dr("RequestDateTime")).ToShortTimeString & " - " & dr("SourceDescription") & "<BR>")
                                If dr("SourceDescription") = "Email" Then
                                    Dim dv As New DataView(emails)
                                    dv.RowFilter = "RequestID = '" & dr("RequestId") & "'"
                                    For Each item As DataRowView In dv
                                        sb.Append(item("EMailAddress") & "<br>")
                                    Next
                                End If
                            Next

                            details = e.Item.FindControl("details")
                            details.Text = sb.ToString
                            details.Visible = True

                            imageButton.ImageUrl = "Images/minus.gif"

                        Else

                            details = e.Item.FindControl("details")
                            details.Text = ""
                            details.Visible = False

                            imageButton.ImageUrl = "Images/plus.gif"

                        End If


                    Catch ex As Exception

                    Finally

                        If conSqlCompany.State <> ConnectionState.Closed Then
                            conSqlCompany.Close()
                            conSqlCompany.Dispose()
                        End If

                    End Try



            End Select

        Catch ex As Exception

        End Try

    End Sub

End Class

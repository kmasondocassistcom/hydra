Imports System.Data.SqlClient

Partial Class ScanConfig
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim mSessionID As String
        Dim intReturn As Integer = 0
        Dim consql As New SqlClient.SqlConnection
        Dim dsReturn As New DataSet("Config")
        Dim da As New SqlClient.SqlDataAdapter
        Dim objEncryption As New Encryption.Encryption

        Try
            'Get the users current sessionid
            Dim consqluser As New SqlConnection

            consqluser.ConnectionString = objEncryption.Decrypt(ConfigurationSettings.AppSettings("Connection"))
            If Not consqluser.State = ConnectionState.Open Then consqluser.Open()
            Dim dtUser As New DataTable
            Dim cmdUSql As New SqlClient.SqlCommand("SELECT SessionID FROM USERS WHERE UserID = @UserID", consqluser)
            cmdUSql.Parameters.Add("@UserID", Request.QueryString("userId"))
            cmdUSql.CommandTimeout = 120
            Dim daSql As New SqlClient.SqlDataAdapter(cmdUSql)
            daSql.Fill(dtUser)

            If dtUser.Rows.Count <= 0 Then
                Return
                Exit Sub
            Else
                mSessionID = dtUser.Rows(0).Item("SessionID")
            End If

            Dim mobjUser As Accucentric.docAssist.Web.Security.User = BuildUserObject(mSessionID, Request.QueryString("accountId"))

            If Not mobjUser Is Nothing Then
                consql = BuildConnection(mobjUser)
                Dim cmdSql As New SqlClient.SqlCommand("SMPanelConfig", consql)
                cmdSql.CommandType = CommandType.StoredProcedure
                Dim guidSessionID As New System.Guid(Request.QueryString("userId"))
                cmdSql.Parameters.Add("@UserID", guidSessionID)
                cmdSql.Parameters.Add("@AccountID", mobjUser.AccountId)
                consql.Open()
                da = New SqlClient.SqlDataAdapter(cmdSql)
                da.Fill(dsReturn)
                cmdSql.Dispose()
            End If

            '' Add account settings table
            'Dim dtAccountSettings As New DataTable("AccountSettings")
            'dtAccountSettings.Columns.Add(New DataColumn("DocTypeAttributes", GetType(System.Boolean), Nothing, MappingType.Element))
            'dtAccountSettings.Columns.Add(New DataColumn("Annotation", GetType(System.Boolean), Nothing, MappingType.Element))
            'dtAccountSettings.Columns.Add(New DataColumn("Redaction", GetType(System.Boolean), Nothing, MappingType.Element))
            'dtAccountSettings.Columns.Add(New DataColumn("Templates", GetType(System.Boolean), Nothing, MappingType.Element))
            'dtAccountSettings.Columns.Add(New DataColumn("Integration", GetType(System.Boolean), Nothing, MappingType.Element))

            'Dim drSettings As DataRow
            'drSettings = dtAccountSettings.NewRow
            'drSettings("DocTypeAttributes") = Functions.ModuleSetting(Functions.ModuleCode.DOCUMENT_TYPES_ATTRIBUTES, Me.mobjUser)
            'drSettings("Annotation") = Functions.ModuleSetting(Functions.ModuleCode.ANNOTATION, Me.mobjUser)
            'drSettings("Redaction") = Functions.ModuleSetting(Functions.ModuleCode.REDACTION, Me.mobjUser)
            'drSettings("Templates") = Functions.ModuleSetting(Functions.ModuleCode.TEMPLATES, Me.mobjUser)
            'drSettings("Integration") = Functions.ModuleSetting(Functions.ModuleCode.APP_INTEGRATION, Me.mobjUser)
            'dtAccountSettings.Rows.Add(drSettings)

            'dsReturn.Tables.Add(dtAccountSettings)

            consql.Dispose()

            dsReturn.Tables(0).TableName = "Documents"
            dsReturn.Tables(1).TableName = "Attributes"
            dsReturn.Tables(2).TableName = "Categories"
            dsReturn.Tables(3).TableName = "Workflows"
            dsReturn.Tables(4).TableName = "Queues"
            dsReturn.Tables(5).TableName = "Groups"
            dsReturn.Tables(6).TableName = "IntegrationHeader"
            dsReturn.Tables(7).TableName = "IntegrationDetail"
            dsReturn.Tables(8).TableName = "FolderMappings"
            dsReturn.Tables(9).TableName = "Preferences"

            Dim xml As New System.IO.MemoryStream
            dsReturn.WriteXml(xml)

            Response.Cache.SetNoStore()
            Response.ContentType = "application/xml"
            Response.Write(System.Text.ASCIIEncoding.ASCII.GetString(xml.GetBuffer()))
            'Response.End()

        Catch ex As Exception

            Throw New System.Web.Services.Protocols.SoapException("Unable to login user!. " & ex.Message, System.Xml.XmlQualifiedName.Empty, "text")

        End Try

    End Sub

End Class

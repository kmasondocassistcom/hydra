﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Uploader.aspx.vb" Inherits="docAssistWeb.Uploader" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>

    <script type="text/javascript" src="scripts/upload/swfupload.js"></script>

    <script type="text/javascript" src="scripts/upload/handlers.js"></script>

    <script type="text/javascript" src="scripts/upload/upload.js"></script>

    <script>
        var swfu;

        window.onload = function() {
            var settings_object = {
                upload_url: "Upload.php",
                flash_url: "http://localhost/docAssistWeb1/upload/swfupload.swf", // Relative to this file
                file_size_limit: "20 MB",
                button_placeholder_id: "uploadPanel"
            };

            swfu = new SWFUpload(settings_object);
        };    
    </script>

    <form id="form1" runat="server">
    <div>
        <div id="uploadPanel">
        </div>
    </div>
    </form>
</body>
</html>

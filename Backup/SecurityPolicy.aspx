<%@ Register TagPrefix="uc1" TagName="AdminNav" Src="Controls/AdminNav.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="SecurityPolicy.aspx.vb" Inherits="docAssistWeb.SecurityPolicy"%>
<%@ Register TagPrefix="uc1" TagName="docHeader" Src="Controls/docHeader.ascx" %>
<%@ Register TagPrefix="uc1" TagName="TopRight" Src="Controls/TopRight.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>docAssist - Manage Documents</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0" MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<TABLE height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TBODY>
					<TR>
						<TD width="100%" colSpan="3"><uc1:docheader id="docHeader" runat="server"></uc1:docheader></TD>
					</TR>
					<TR vAlign="middle" height="100%">
						<TD class="Navigation1" style="BORDER-RIGHT: #cccccc thin solid" vAlign="top" height="100%"><uc1:adminnav id="AdminNav2" runat="server"></uc1:adminnav><IMG height="1" src="dummy.gif" width="120"></TD>
						<TD class="PageContent" vAlign="top" align="center" height="100%"><br>
							<br>
							<table class="PageContent" cellPadding="2" width="100%" border="0">
								<tr>
									<td width="25"></td>
									<td><h2>Password Policy</h2></td>
								</tr>
								<tr>
									<td></td>
									<td align="right">
										<table class="PageContent" cellPadding="2" width="100%" border="0">
											<tr>
												<td width="15"></td>
												<td><asp:radiobutton id="rdoNeverExpire" runat="server" GroupName="PasswordExpiration" Text="User passwords never expire."></asp:radiobutton><BR>
													<asp:radiobutton id="rdoExpireInterval" runat="server" GroupName="PasswordExpiration" Text="Users must reset their password every"></asp:radiobutton>&nbsp;
													<asp:textbox id="txtDays" runat="server" Width="24px"></asp:textbox>&nbsp;days.</td>
											</tr>
										</table>
										<TABLE class="PageContent" id="Table1" cellPadding="0" width="100%" border="0">
											<TR>
												<TD width="300"></TD>
												<TD><asp:imagebutton id="btnSaveSettings" runat="server" ImageUrl="Images/save_small.gif"></asp:imagebutton></TD>
											</TR>
										</TABLE>
										<TABLE class="PageContent" id="Table2" cellPadding="0" width="100%" border="0">
											<TR>
												<TD width="300"></TD>
												<TD>
													<asp:Label id="lblError" runat="server" ForeColor="Red"></asp:Label></TD>
											</TR>
										</TABLE>
									</td>
								</tr>
							</table>
		</form>
		</TD>
		<TD class="RightContent" height="100%">&nbsp;</TD>
		</TR>
		<TR>
			<TD class="PageFooter" vAlign="bottom" bgColor="#cccccc" colSpan="3"><uc1:topright id="docFooter" runat="server"></uc1:topright></TD>
		</TR>
		<TR height="1">
			<TD style="BACKGROUND-COLOR: #959394" width="20"><IMG height="1" src="images/spacer.gif" width="20"></TD>
			<TD style="BACKGROUND-COLOR: #959394" width="100%"><IMG height="1" src="images/spacer.gif" width="100%"></TD>
			<TD style="BACKGROUND-COLOR: #959394" width="20"><IMG height="1" src="images/spacer.gif" width="20"></TD>
		</TR>
		</TBODY></TABLE>
	</body>
</HTML>

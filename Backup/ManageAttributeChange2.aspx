<%@ Register TagPrefix="uc1" TagName="TopRight" Src="Controls/TopRight.ascx" %>
<%@ Register TagPrefix="uc1" TagName="docHeader" Src="Controls/docHeader.ascx" %>
<%@ Register TagPrefix="uc1" TagName="AdminNav" Src="Controls/AdminNav.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ManageAttributeChange2.aspx.vb" Inherits="docAssistWeb.ManageAttributeChange2"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>docAssist - Manage Attributes</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<TABLE height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TBODY>
					<TR>
						<TD width="100%" colSpan="5">
							<uc1:docHeader id="docHeader" runat="server"></uc1:docHeader>
						</TD>
					</TR>
					<TR vAlign="middle" height="100%">
						<TD class="Navigation1" height="100%" style="BORDER-RIGHT: #cccccc thin solid" vAlign="top">
							<uc1:AdminNav id="AdminNav1" runat="server"></uc1:AdminNav><IMG height="1" src="dummy.gif" width="120"></TD>
						<TD vAlign="top" align="center" height="100%"><BR>
							<table cellSpacing="0" cellPadding="0" width="100%" border="0">
								<tr>
									<td><IMG height="1" src="dummy.gif" width="35"><IMG src="Images/manage_attributes.gif"></td>
								</tr>
							</table>
							<table height="100%" cellSpacing="0" cellPadding="0" width="100%">
					</TR>
					<tr height="50%">
					</tr>
					<tr height="15" width="100%">
						<td style="HEIGHT: 16px"></td>
						<td style="HEIGHT: 16px">
							<asp:label id="Label1" runat="server" Height="5px" Width="98px" Font-Size="8.25pt" Font-Names="Verdana">Attribute Name:</asp:label></td>
						<td style="HEIGHT: 16px">
							<asp:textbox id="txtAttributeName" runat="server" Height="18px" Width="270px" Font-Size="8.25pt"
								Font-Names="Verdana"></asp:textbox></td>
						<td style="HEIGHT: 16px"></td>
					</tr>
					<tr height="15" width="100%">
						<td style="HEIGHT: 4px"></td>
						<td style="WIDTH: 72px; HEIGHT: 4px" align="left">
							<asp:label id="Label2" runat="server" Height="5px" Width="72px" Font-Size="8.25pt" Font-Names="Verdana">Data Type:</asp:label></td>
						<TD style="HEIGHT: 4px">
							<P>
								<asp:dropdownlist id="cmbDataType" runat="server" Height="24px" Width="270px" AutoPostBack="True"
									Font-Size="8.25pt" Font-Names="Verdana"></asp:dropdownlist><BR>
								<asp:RadioButton id="rdoSingle" runat="server" Checked="True" GroupName="ListType" Visible="False"
									Text="Single Column"></asp:RadioButton>
								<asp:RadioButton id="rdoMultiple" runat="server" GroupName="ListType" Visible="False" Text="Multi Column"></asp:RadioButton></P>
						</TD>
						<TD width="50%" style="HEIGHT: 4px"></TD>
					<TR height="15" width="100%" style="display:none">
						<td style="HEIGHT: 21px" width="50%"></td>
						<td style="WIDTH: 72px; HEIGHT: 21px" align="left">
							<asp:label id="lblLength" runat="server" Height="5px" Width="72px" Font-Size="8.25pt" Font-Names="Verdana">Length:</asp:label>
						</td>
						<td style="HEIGHT: 21px">
							<asp:textbox id="txtLength" runat="server" Width="270px" Font-Size="8.25pt" Font-Names="Verdana">75</asp:textbox></td>
						<td style="HEIGHT: 21px" width="50%"></td>
					</TR>
					<tr height="15" width="100%" style="DISPLAY: none">
						<td style="WIDTH: 393px" width="393"></td>
						<td style="WIDTH: 72px; HEIGHT: 17px">
							<P align="left">
								<asp:label id="lblFormat" runat="server" Height="5px" Width="72px" Font-Size="8.25pt" Font-Names="Verdana">Format:</asp:label>
							</P>
						</td>
						<td style="HEIGHT: 17px">
							<asp:TextBox id="txtFormat" runat="server" Width="270px" Font-Size="8.25pt" Font-Names="Verdana"></asp:TextBox></td>
						<td width="50%"></td>
					</tr>
					<tr height="15" width="100%">
						<td style="WIDTH: 393px" width="393"></td>
						<td style="WIDTH: 72px" vAlign="middle">
							<P align="left">&nbsp;&nbsp;</P>
						</td>
						<td align="left">
							<asp:Label id="lblError" runat="server" Height="24px" Width="100%" ForeColor="Red" Font-Size="8.25pt"
								Font-Names="Verdana"></asp:Label></td>
						<td width="50%"></td>
					</tr>
					<tr height="15" width="100%">
						<td style="WIDTH: 393px; HEIGHT: 24px" width="395"></td>
						<td style="WIDTH: 72px; HEIGHT: 24px"><BR>
						</td>
						<td style="HEIGHT: 24px" align="right"><asp:imagebutton id="btnCancel" runat="server" ImageUrl="Images/btn_canceladmin.gif"></asp:imagebutton>&nbsp;<asp:imagebutton id="btnFinish" runat="server" ImageUrl="Images/btn_finish.gif"></asp:imagebutton></td>
						<td width="50%" style="HEIGHT: 24px"></td>
					</tr>
					<tr height="15" width="100%">
						<td style="WIDTH: 395px" width="395"></td>
						<td style="WIDTH: 72px"><BR>
							<P>&nbsp;</P>
						</td>
						<td align="right"></td>
						<td width="50"></td>
					</tr>
					<tr height="100%">
						<td></td>
					</tr>
				</tr>
			</TABLE>
			<P><BR>
				<BR>
			</P>
			</TD>
			<TD class="RightContent" height="100%">&nbsp;</TD>
			</TR>
			<TR>
				<TD class="PageFooter" vAlign="bottom" bgColor="#cccccc" colSpan="3">
					<uc1:TopRight id="docFooter" runat="server"></uc1:TopRight></TD>
			</TR>
			<TR height="1">
				<TD style="BACKGROUND-COLOR: #959394" width="20"><IMG height="1" src="images/spacer.gif" width="20"></TD>
				<TD style="BACKGROUND-COLOR: #959394" width="100%"><IMG height="1" src="images/spacer.gif" width="100%"></TD>
				<TD style="BACKGROUND-COLOR: #959394" width="20"><IMG height="1" src="images/spacer.gif" width="20"></TD>
			</TR>
			</TBODY></TABLE>
		</form>
		<script language="javascript">
		function nb_numericOnly(evt)
			{
				evt = (evt) ? evt : event;
				var charCode = (evt.charCode) ? evt.charCode : ((evt.keyCode) ? evt.keyCode
				: ((evt.which) ? evt.which : 0));
				if (charCode > 31 && (charCode < 48 || charCode >57)) {return false;}
				return true;
			}
			
			function catchSubmit()
			{
				if (event.keyCode == 13)
				{
					event.cancelBubble = true;
					event.returnValue = false;
					//document.all.lnkSearch.click();
					return false;
				}
			}	
		</script>
	</body>
</HTML>

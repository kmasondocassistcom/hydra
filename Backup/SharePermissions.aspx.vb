Imports Telerik.WebControls
Imports Accucentric.docAssist
Imports Accucentric.docAssist.Data.Images

Partial Class SharePermissions
    Inherits System.Web.UI.Page
    
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Dim mconSqlImage As SqlClient.SqlConnection
    Dim mobjUser As Accucentric.docAssist.Web.Security.User
    Private strId As String
    
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try

            Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
            Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))
            Me.mobjUser.AccountId = guidAccountId

            strId = Request.QueryString("Id")

            strId = strId.Replace("S", "")
            ScreenMode.InnerHtml = "Cabinet Permissions"
            
            If Not Page.IsPostBack Then

                Dim objFolders As New Folders(Functions.BuildConnection(Me.mobjUser))
                Dim ds As New DataSet

                ds = objFolders.SecuritySharesGet(strId)
                lblName.Text = ds.Tables(0).Rows(0)("ShareName")

                lstAvailable.DataValueField = "GroupId"
                lstAvailable.DataTextField = "GroupName"
                lstAssigned.DataValueField = "GroupId"
                lstAssigned.DataTextField = "GroupName"

                lstAvailable.DataSource = ds.Tables(2)
                lstAvailable.DataBind()
                lstAssigned.DataSource = ds.Tables(3)
                lstAssigned.DataBind()

                dgPermissions.DataSource = ds.Tables(1)
                dgPermissions.DataBind()

            End If

        Catch ex As Exception

        End Try

    End Sub

    Private Sub dgPermissions_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgPermissions.ItemDataBound

        Try

            Dim ddlFilePermissions As DropDownList = CType(e.Item.FindControl("ddlFilePermissions"), DropDownList)
            Dim ddlFolderPermissons As DropDownList = CType(e.Item.FindControl("ddlFolderPermissons"), DropDownList)
            Dim lblFileLevel As Label = CType(e.Item.FindControl("lblFileLevel"), Label)
            Dim lblFolderLevel As Label = CType(e.Item.FindControl("lblFolderLevel"), Label)

            If Not IsNothing(ddlFilePermissions) Then
                ddlFilePermissions.SelectedValue = lblFileLevel.Text
                ddlFolderPermissons.SelectedValue = lblFolderLevel.Text
            End If

        Catch ex As Exception

        End Try

    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click

        Dim conSql As New SqlClient.SqlConnection
        Dim sqlTran As SqlClient.SqlTransaction

        Try

            'Build temp table
            Dim dtPermissions As New DataTable
            dtPermissions.Columns.Add(New DataColumn("FolderId"))
            dtPermissions.Columns.Add(New DataColumn("GroupId"))
            dtPermissions.Columns.Add(New DataColumn("SecValue"))
            dtPermissions.Columns.Add(New DataColumn("SecLevel"))

            For Each dgRow As DataGridItem In dgPermissions.Items

                Dim ddlFilePermissions As DropDownList = CType(dgRow.FindControl("ddlFilePermissions"), DropDownList)
                Dim ddlFolderPermissons As DropDownList = CType(dgRow.FindControl("ddlFolderPermissons"), DropDownList)
                Dim lblGroupId As Label = CType(dgRow.FindControl("lblGroupId"), Label)

                Dim dr As DataRow = dtPermissions.NewRow
                dr("FolderId") = strId * -1
                dr("GroupId") = lblGroupId.Text
                dr("SecValue") = CInt(Functions.FolderSecurity.FILES)
                dr("SecLevel") = ddlFilePermissions.SelectedValue
                dtPermissions.Rows.Add(dr)

                dr = dtPermissions.NewRow
                dr("FolderId") = strId * -1
                dr("GroupId") = lblGroupId.Text
                dr("SecValue") = CInt(Functions.FolderSecurity.FOLDER)
                dr("SecLevel") = ddlFolderPermissons.SelectedValue
                dtPermissions.Rows.Add(dr)

            Next

            For Each lstItem As ListItem In lstAssigned.Items
                Dim dr As DataRow = dtPermissions.NewRow
                dr("FolderId") = strId * -1
                dr("GroupId") = lstItem.Value
                dr("SecValue") = CInt(Functions.FolderSecurity.SHARE_ADMIN)
                dr("SecLevel") = 1
                dtPermissions.Rows.Add(dr)
            Next

            conSql = Functions.BuildConnection(Me.mobjUser)
            conSql.Open()

            Dim tempTableName As String = "##" & System.Guid.NewGuid.ToString.Replace("-", "")
            Dim sqlCmd As New SqlClient.SqlCommand("CREATE TABLE " & tempTableName & " (FolderID int NULL, GroupID int NULL, SecValue int NULL, SecLevel int NULL)", conSql)
            sqlCmd.ExecuteNonQuery()

            sqlCmd = New SqlClient.SqlCommand("SELECT * FROM " & tempTableName, conSql)
            Dim sqlDa As New SqlClient.SqlDataAdapter(sqlCmd)
            Dim sqlBld As New SqlClient.SqlCommandBuilder(sqlDa)

            sqlDa.Update(dtPermissions)

            sqlTran = conSql.BeginTransaction

            Dim objFolders As New Folders(conSql, False, sqlTran)

            Dim intReturn As Integer = objFolders.SecurityFolderInsert(tempTableName)

            sqlTran.Commit()

            System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType(), "close", "window.close();", True)

        Catch ex As Exception
            sqlTran.Rollback()
        Finally
            If conSql.State <> ConnectionState.Closed Then
                conSql.Close()
                conSql.Dispose()
            End If
        End Try

    End Sub

    Private Sub btnAssignAll_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAssignAll.Click
        Try
            For Each lstItem As ListItem In lstAvailable.Items
                Dim NewItem As New ListItem(lstItem.Text, lstItem.Value)
                lstAssigned.Items.Add(NewItem)
            Next

            lstAvailable.Items.Clear()

        Catch ex As Exception

        Finally

            'ReturnControls.Add(lstAssigned)
            'ReturnControls.Add(lstAvailable)

        End Try
    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAdd.Click

        Dim intAvailableItem As Integer = lstAvailable.SelectedIndex

        Dim dtMove As New DataTable
        dtMove.Columns.Add(New DataColumn("UserId", GetType(System.Int32), Nothing, MappingType.Element))
        dtMove.Columns.Add(New DataColumn("UserName", GetType(System.String), Nothing, MappingType.Element))

        Dim dtKeep As New DataTable
        dtKeep.Columns.Add(New DataColumn("UserId", GetType(System.Int32), Nothing, MappingType.Element))
        dtKeep.Columns.Add(New DataColumn("UserName", GetType(System.String), Nothing, MappingType.Element))

        Try
            For X As Integer = 0 To lstAvailable.Items.Count - 1
                If lstAvailable.Items(X).Selected = True Then
                    Dim dr As DataRow
                    dr = dtMove.NewRow
                    dr("UserId") = lstAvailable.Items(X).Value
                    dr("UserName") = lstAvailable.Items(X).Text
                    dtMove.Rows.Add(dr)
                Else
                    Dim dr As DataRow
                    dr = dtKeep.NewRow
                    dr("UserId") = lstAvailable.Items(X).Value
                    dr("UserName") = lstAvailable.Items(X).Text
                    dtKeep.Rows.Add(dr)
                End If
            Next

            For Each lstItem As ListItem In lstAssigned.Items
                Dim dr As DataRow
                dr = dtMove.NewRow
                dr("UserId") = lstItem.Value
                dr("UserName") = lstItem.Text
                dtMove.Rows.Add(dr)
            Next

            lstAvailable.Items.Clear()
            lstAssigned.Items.Clear()

            lstAvailable.DataSource = dtKeep
            lstAvailable.DataValueField = "UserId"
            lstAvailable.DataTextField = "UserName"
            lstAvailable.DataBind()

            lstAssigned.DataSource = dtMove
            lstAssigned.DataValueField = "UserId"
            lstAssigned.DataTextField = "UserName"
            lstAssigned.DataBind()

            'If lstAssigned.Items.Count > 0 Then
            '    If intSelectedItem > 0 Then
            '        lstAssigned.Items(intSelectedItem - 1).Selected = True
            '    Else
            '        lstAssigned.Items(0).Selected = True
            '    End If

            'End If

            If lstAvailable.Items.Count > 0 Then
                If intAvailableItem > 0 Then
                    lstAvailable.Items(intAvailableItem - 1).Selected = True
                Else
                    lstAvailable.Items(0).Selected = True
                End If
            End If

        Catch ex As Exception

        Finally

            'ReturnControls.Add(lstAssigned)
            'ReturnControls.Add(lstAvailable)

        End Try
    End Sub

    Private Sub btnRemove_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnRemove.Click
        Dim dtMove As New DataTable
        dtMove.Columns.Add(New DataColumn("UserId", GetType(System.Int32), Nothing, MappingType.Element))
        dtMove.Columns.Add(New DataColumn("UserName", GetType(System.String), Nothing, MappingType.Element))

        Dim dtKeep As New DataTable
        dtKeep.Columns.Add(New DataColumn("UserId", GetType(System.Int32), Nothing, MappingType.Element))
        dtKeep.Columns.Add(New DataColumn("UserName", GetType(System.String), Nothing, MappingType.Element))

        Try
            For X As Integer = 0 To lstAssigned.Items.Count - 1
                If lstAssigned.Items(X).Selected = True Then
                    Dim dr As DataRow
                    dr = dtMove.NewRow
                    dr("UserId") = lstAssigned.Items(X).Value
                    dr("UserName") = lstAssigned.Items(X).Text
                    dtMove.Rows.Add(dr)
                Else
                    Dim dr As DataRow
                    dr = dtKeep.NewRow
                    dr("UserId") = lstAssigned.Items(X).Value
                    dr("UserName") = lstAssigned.Items(X).Text
                    dtKeep.Rows.Add(dr)
                End If
            Next

            For Each lstItem As ListItem In lstAvailable.Items
                Dim dr As DataRow
                dr = dtMove.NewRow
                dr("UserId") = lstItem.Value
                dr("UserName") = lstItem.Text
                dtMove.Rows.Add(dr)
            Next

            lstAvailable.Items.Clear()
            lstAssigned.Items.Clear()

            lstAvailable.DataSource = dtMove
            lstAvailable.DataValueField = "UserId"
            lstAvailable.DataTextField = "UserName"
            lstAvailable.DataBind()

            lstAssigned.DataSource = dtKeep
            lstAssigned.DataValueField = "UserId"
            lstAssigned.DataTextField = "UserName"
            lstAssigned.DataBind()

            If lstAssigned.Items.Count > 0 Then
                lstAssigned.Items(0).Selected = True
            End If

            If lstAvailable.Items.Count > 0 Then
                lstAvailable.Items(0).Selected = True
            End If

        Catch ex As Exception

        Finally

            'ReturnControls.Add(lstAssigned)
            'ReturnControls.Add(lstAvailable)

        End Try

    End Sub

    Private Sub btnRemoveAll_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnRemoveAll.Click

        Try
            For Each lstItem As ListItem In lstAssigned.Items
                Dim NewItem As New ListItem(lstItem.Text, lstItem.Value)
                lstAvailable.Items.Add(NewItem)
            Next

            lstAssigned.Items.Clear()

        Catch ex As Exception

        Finally

            'ReturnControls.Add(lstAssigned)
            'ReturnControls.Add(lstAvailable)

        End Try

    End Sub
End Class
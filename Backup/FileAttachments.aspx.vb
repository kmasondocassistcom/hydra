Imports Accucentric.docAssist
Imports Accucentric.docAssist.Data.Images

Partial Class FileAttachment
    Inherits System.Web.UI.Page

    Dim msqlConCompany As SqlClient.SqlConnection
    Dim mobjUser As Accucentric.docAssist.Web.Security.User
    Dim mcookieSearch As cookieSearch

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try

            Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
            Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))
            Me.mobjUser.AccountId = guidAccountId
            Me.msqlConCompany = Functions.BuildConnection(Me.mobjUser)
            Me.mcookieSearch = New cookieSearch(Me)

            If Not Page.IsPostBack Then
                Dim objAttachments As New FileAttachments(Functions.BuildConnection(Me.mobjUser))
                Dim dt As New DataTable
                dt = objAttachments.AttachmentsByVersionId(Me.mcookieSearch.VersionID, Me.mobjUser.TimeDiffFromGMT)
                Dim dv As New DataView(dt)
                dv.Sort = "Filename ASC"
                Response.Cookies("docAssistSort")("AttachmentSortOrder") = "ASC"
                dgFileAttachments.DataSource = dv
                dgFileAttachments.DataBind()
            End If

        Catch ex As Exception

        End Try

    End Sub

    Private Sub dgFileAttachments_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgFileAttachments.ItemDataBound

        Try

            Dim lblFileName As Label = CType(e.Item.FindControl("lblFileName"), Label)
            Dim lblAttachmentId As Label = CType(e.Item.FindControl("lblAttachmentId"), Label)
            Dim lblSize As Label = CType(e.Item.FindControl("lblSize"), Label)
            Dim lblModifiedDate As Label = CType(e.Item.FindControl("lblModifiedDate"), Label)
            Dim FileIcon As System.Web.UI.HtmlControls.HtmlImage = CType(e.Item.FindControl("FileIcon"), System.Web.UI.HtmlControls.HtmlImage)
            Dim lblLocked As Label = CType(e.Item.FindControl("lblLocked"), Label)
            Dim imgLock As System.Web.UI.HtmlControls.HtmlImage = CType(e.Item.FindControl("imgLock"), System.Web.UI.HtmlControls.HtmlImage)
            Dim lblCheckOutUser As Label = CType(e.Item.FindControl("lblCheckOutUser"), Label)
            Dim lnkDelete As LinkButton = e.Item.FindControl("lnkDelete")

            If Not IsNothing(lblFileName) Then

                'e.Item.Cells(0).Attributes("onclick") = "LoadAttachment(" & lblAttachmentId.Text & ");"
                'e.Item.Cells(0).Style("CURSOR") = "hand"
                e.Item.Cells(1).Attributes("onclick") = "LoadAttachment(" & lblAttachmentId.Text & ");"
                e.Item.Cells(1).Style("CURSOR") = "hand"
                e.Item.Cells(2).Attributes("onclick") = "LoadAttachment(" & lblAttachmentId.Text & ");"
                e.Item.Cells(2).Style("CURSOR") = "hand"
                e.Item.Cells(3).Attributes("onclick") = "LoadAttachment(" & lblAttachmentId.Text & ");"
                e.Item.Cells(3).Style("CURSOR") = "hand"
                e.Item.Cells(4).Attributes("onclick") = "LoadAttachment(" & lblAttachmentId.Text & ");"
                e.Item.Cells(4).Style("CURSOR") = "hand"
                e.Item.Cells(5).Attributes("onclick") = "LoadAttachment(" & lblAttachmentId.Text & ");"
                e.Item.Cells(5).Style("CURSOR") = "hand"

                Dim strIconPath As String = "images/icons/" & System.IO.Path.GetExtension(lblFileName.Text).ToUpper.Replace(".", "") & ".gif"
                If System.IO.File.Exists(Server.MapPath(strIconPath)) Then
                    FileIcon.Src = strIconPath
                End If
                lblSize.Text = GetBytesAsMegs(lblSize.Text)
                lblModifiedDate.Text = CDate(lblModifiedDate.Text).ToShortDateString & " " & CDate(lblModifiedDate.Text).ToShortTimeString

                If lblLocked.Text = "True" Then
                    imgLock.Visible = True
                    imgLock.Alt = "Checked out by " & lblCheckOutUser.Text
                Else
                    imgLock.Visible = False
                End If

                lnkDelete.Attributes("onclick") = "return confirm('Delete the file \'" & lblFileName.Text.Replace("'", "\'") & "\'?');"

            End If

        Catch ex As Exception

        End Try

    End Sub

    Private Sub btnAddAttachment_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddAttachment.Click
        Response.Redirect("AddFiles.aspx")
    End Sub

    Private Sub dgFileAttachments_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgFileAttachments.ItemCommand

        Select Case e.CommandName
            Case "Delete"

                Try
                    Dim lblAttachmentId As Label = CType(e.Item.FindControl("lblAttachmentId"), Label)

                    If Not IsNothing(lblAttachmentId) Then
                        Me.msqlConCompany = Functions.BuildConnection(Me.mobjUser)
                        Dim objAttachments As New FileAttachments(Me.msqlConCompany, False)
                        Dim intReturn As Integer = objAttachments.AttachmentDeleteByVersionId(lblattachmentid.Text)

                        Dim dt As New DataTable
                        dt = objAttachments.AttachmentsByVersionId(Me.mcookieSearch.VersionID, Me.mobjUser.TimeDiffFromGMT)
                        Dim dv As New DataView(dt)
                        dv.Sort = "Filename ASC"
                        Response.Cookies("docAssistSort")("AttachmentSortOrder") = "ASC"
                        dgFileAttachments.DataSource = dv
                        dgFileAttachments.DataBind()

                    End If

                Catch ex As Exception

                Finally

                    If Me.msqlConCompany.State <> ConnectionState.Closed Then
                        Me.msqlConCompany.Close()
                        Me.msqlConCompany.Dispose()
                    End If

                End Try


            Case "Select"
                Dim lblAttachmentId As Label = CType(e.Item.FindControl("lblAttachmentId"), Label)
                If Not IsNothing(lblAttachmentId) Then
                    Response.Redirect("EditViewAttachments.aspx?AID=" & lblAttachmentId.Text)
                End If
            Case "Download"
                Dim lblAttachmentId As Label = CType(e.Item.FindControl("lblAttachmentId"), Label)
                If Not IsNothing(lblAttachmentId) Then
                    Dim objAttachments As New FileAttachments(Functions.BuildConnection(Me.mobjUser))
                    Dim dt As New DataTable
                    dt = objAttachments.GetFile(lblAttachmentId.Text)
                    Dim imgByte As Byte() = CType(dt.Rows(0)("Data"), Byte())
                    Response.Clear()
                    Response.AddHeader("Content-Disposition", "attachment; filename=" & dt.Rows(0)("Filename"))
                    Response.AddHeader("Content-Length", imgByte.Length)
                    Response.ContentType = "application/octet-stream"
                    Response.BinaryWrite(imgByte)

                    'Track attachment
                    Try
                        Accucentric.docAssist.Web.Data.Functions.TrackPageRequest(Me.mobjUser.UserId, Me.mobjUser.AccountId, "Attachment View", "In", Me.mcookieSearch.VersionID.ToString, lblAttachmentId.Text.ToString, Functions.GetMasterConnectionString)
                    Catch ex As Exception

                    End Try

                    Response.End()

                End If

            Case "DownloadZip"
                Dim lblAttachmentId As Label = CType(e.Item.FindControl("lblAttachmentId"), Label)
                If Not IsNothing(lblAttachmentId) Then
                    Dim objAttachments As New FileAttachments(Functions.BuildConnection(Me.mobjUser))
                    Dim dt As New DataTable
                    dt = objAttachments.GetFile(lblAttachmentId.Text)
                    Dim imgByte As Byte() = CType(dt.Rows(0)("Data"), Byte())

                    Dim zippedBytes As Byte() '= Functions.CompressBytesAsZipFile(imgbyte, dt.Rows(0)("Filename"))

                    If CStr(dt.Rows(0)("Filename")).ToUpper.EndsWith(".ZIP") Then
                        zippedBytes = imgbyte
                    Else
                        zippedBytes = Functions.CompressBytesAsZipFile(imgbyte, dt.Rows(0)("Filename"))
                    End If

                    Response.Clear()
                    Response.AddHeader("Content-Disposition", "attachment; filename=" & CStr(dt.Rows(0)("Filename")).Replace(System.IO.Path.GetExtension(dt.Rows(0)("Filename")), ".zip"))
                    Response.AddHeader("Content-Length", zippedBytes.Length)
                    Response.ContentType = "application/octet-stream"
                    Response.BinaryWrite(zippedBytes)

                    'Track attachment
                    Try
                        Accucentric.docAssist.Web.Data.Functions.TrackPageRequest(Me.mobjUser.UserId, Me.mobjUser.AccountId, "Attachment View", "In", Me.mcookieSearch.VersionID.ToString, lblAttachmentId.Text.ToString, Functions.GetMasterConnectionString)
                    Catch ex As Exception

                    End Try

                    Response.End()

                End If

        End Select

    End Sub

    Private Sub dgFileAttachments_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgFileAttachments.SortCommand

        Try

            Dim objAttachments As New FileAttachments(Functions.BuildConnection(Me.mobjUser))
            Dim dt As New DataTable
            dt = objAttachments.AttachmentsByVersionId(Me.mcookieSearch.VersionID, Me.mobjUser.TimeDiffFromGMT)
            Dim dv As New DataView(dt)

            If Request.Cookies("docAssistSort")("AttachmentSortOrder") = "ASC" Then
                dv.Sort = e.SortExpression & " DESC"
                Response.Cookies("docAssistSort")("AttachmentSortOrder") = "DESC"
            Else
                dv.Sort = e.SortExpression & " ASC"
                Response.Cookies("docAssistSort")("AttachmentSortOrder") = "ASC"
            End If

            dgFileAttachments.DataSource = dv
            dgFileAttachments.DataBind()

        Catch ex As Exception

        End Try

    End Sub

End Class
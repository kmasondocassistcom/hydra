<%@ Register TagPrefix="uc1" TagName="docHeader" Src="Controls/docHeader.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ManageAtributesChange.aspx.vb" Inherits="docAssistWeb.ManageAtributesChange"%>
<%@ Register TagPrefix="uc1" TagName="AdminNav" Src="Controls/AdminNav.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>docAssist - Manage Attributes</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<TABLE height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TBODY>
					<TR>
						<TD width="100%" colSpan="5">
							<uc1:docHeader id="docHeader" runat="server"></uc1:docHeader></TD>
					</TR>
					<TR vAlign="middle" height="100%">
						<TD class="Navigation1" height="100%" style="BORDER-RIGHT: #cccccc thin solid" vAlign="top">
							<uc1:AdminNav id="AdminNav1" runat="server"></uc1:AdminNav><BR>
							<IMG height="1" src="dummy.gif" width="120"></TD>
						<TD vAlign="top" align="center" height="100%"><BR>
							<table cellSpacing="0" cellPadding="0" width="100%" border="0">
								<tr>
									<td><IMG height="1" src="dummy.gif" width="35"><IMG src="Images/manage_attributes.gif"></td>
								</tr>
							</table>
							<table height="100%" cellSpacing="0" cellPadding="0" width="100%">
					</TR>
					<tr height="50%">
					</tr>
					<tr height="15" width="100%">
						<td style="HEIGHT: 24px"></td>
						<td style="HEIGHT: 24px"></td>
						<td style="HEIGHT: 24px">
							<asp:label id="lblAction" runat="server" Height="8px" Width="100%" Font-Size="8.25pt" Font-Names="Verdana"></asp:label><BR>
							<BR>
							<asp:listbox id="lstAttributes" runat="server" Height="200px" Width="440px" Font-Size="8.25pt"
								Font-Names="Verdana"></asp:listbox></td>
						<td style="HEIGHT: 24px"></td>
					</tr>
					<tr height="15" width="100%">
						<td style="HEIGHT: 31px"></td>
						<td style="WIDTH: 72px; HEIGHT: 31px" align="left"></td>
						<TD style="HEIGHT: 31px"><asp:label id="lblError" runat="server" Height="16px" Width="100%" ForeColor="Red" Font-Size="8.25pt"
								Font-Names="Verdana"></asp:label></TD>
						<TD width="50%" style="HEIGHT: 31px"></TD>
					<TR height="15" width="100%">
						<td style="HEIGHT: 21px" width="50%"></td>
						<td style="WIDTH: 72px; HEIGHT: 21px" align="left">&nbsp;
						</td>
						<td style="HEIGHT: 21px" align="right"><asp:imagebutton id="btnCancel" runat="server" ImageUrl="Images/btn_canceladmin.gif"></asp:imagebutton>&nbsp;<asp:imagebutton id="btnNext" runat="server"></asp:imagebutton></td>
						<td style="HEIGHT: 21px" width="50%"></td>
					</TR>
					<tr height="15" width="100%">
						<td style="WIDTH: 393px" width="393"></td>
						<td style="WIDTH: 72px; HEIGHT: 17px">
							<P align="left">&nbsp;&nbsp;
							</P>
						</td>
						<td style="HEIGHT: 17px" align="right"></td>
						<td width="50%"></td>
					</tr>
					<tr height="15" width="100%">
						<td style="WIDTH: 393px; HEIGHT: 4px" width="393"></td>
						<td style="WIDTH: 72px; HEIGHT: 4px" vAlign="middle">
							<P align="left">&nbsp;&nbsp;</P>
						</td>
						<td align="right" style="HEIGHT: 4px"></td>
						<td width="50%" style="HEIGHT: 4px"></td>
					</tr>
					<tr height="15" width="100%">
						<td style="WIDTH: 393px" width="395"></td>
						<td style="WIDTH: 72px; HEIGHT: 17px"><BR>
						</td>
						<td style="HEIGHT: 17px"></td>
						<td width="50%"></td>
					</tr>
					<tr height="15" width="100%">
						<td style="WIDTH: 395px" width="395"></td>
						<td style="WIDTH: 72px"><BR>
							<P></P>
						</td>
						<td align="right"></td>
						<td width="50"></td>
					</tr>
					<tr height="100%">
						<td></td>
					</tr>
				</tr>
			</TABLE>
		</form>
		</TD></TR></TBODY></TABLE>
	</body>
</HTML>

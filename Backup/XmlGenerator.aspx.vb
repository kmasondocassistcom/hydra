Imports ComponentArt.Web.UI
Imports Accucentric.docAssist.Data.Images

Partial Class XmlGenerator
    Inherits System.Web.UI.Page

    Private mobjUser As Accucentric.docAssist.Web.Security.User
    Private mconSqlWeb As SqlClient.SqlConnection
    Private mconSqlImage As New SqlClient.SqlConnection

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim tree As New TreeView

        Try

            Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
            Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))
            Me.mobjUser.AccountId = guidAccountId
            Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)

            Dim id As String = Request.QueryString("NodeId")

            Dim objFolders As New Folders(Functions.BuildConnection(Me.mobjUser))

            Dim dt As New DataTable
            If id.StartsWith("S") Then
                dt = objFolders.FoldersGetById(Me.mobjUser.ImagesUserId, CInt(id.Replace("S", "")) * -1)
            Else
                dt = objFolders.FoldersGetById(Me.mobjUser.ImagesUserId, id.Replace("W", ""))
            End If

            For Each dr As DataRow In dt.Rows

                Dim node As New TreeViewNode

                node.Text = dr("FolderName")
                node.ID = "W" & dr("FolderId")
                node.ImageUrl = IMAGE_FOLDER

                If Request.QueryString("Ref") = "Popup" Then
                    node.AutoPostBackOnSelect = True
                Else
                    node.AutoPostBackOnSelect = False
                End If

                node.LabelPadding = 3

                If dr("HasChildFolder") = True Then

                    If Request.QueryString("Ref") = "Popup" Then
                        node.ContentCallbackUrl = "XmlGenerator.aspx?Ref=Popup&NodeId=" & node.ID
                    Else
                        node.ContentCallbackUrl = "XmlGenerator.aspx?NodeId=" & node.ID
                    End If

                    node.Expanded = False
                End If

                If Request.QueryString("Ref") = "Popup" Then

                    If dr("CanAddFilesToFolder") = False Then
                        node.ID = "NOACCESS"
                    End If

                Else

                    Dim sbFolderCommand As New System.Text.StringBuilder

                    sbFolderCommand.Append("browseFolder('" & dr("FolderId") & "');")

                    If dr("CanAddFolder") = True Then
                        sbFolderCommand.Append("enableFolder();")
                    Else
                        sbFolderCommand.Append("disableFolder();")
                    End If

                    If dr("CanAddFilesToFolder") = True Then
                        sbFolderCommand.Append("enableAdd();")
                    Else
                        sbFolderCommand.Append("disableAdd();")
                    End If

                    If dr("CanRenameFolder") = True Then
                        sbFolderCommand.Append("enableRename();")
                    Else
                        sbFolderCommand.Append("disableRename();")
                    End If

                    If dr("CanDeleteFolder") = True Then
                        sbFolderCommand.Append("enableDelete('" & node.Text.ToString.Replace("'", "\'") & "');enableMove();")
                    Else
                        sbFolderCommand.Append("disableDelete();")
                    End If

                    If dr("CanChangeSecurity") = True Then
                        sbFolderCommand.Append("enableSecurity();")
                    Else
                        sbFolderCommand.Append("disableSecurity();")
                    End If

                    sbFolderCommand.Append("enableFavorite();disableCabinet();setValue('" & node.ID & "');")

                    node.ClientSideCommand = sbFolderCommand.ToString

                End If

                tree.Nodes.Add(node)

            Next

        Catch ex As Exception

        Finally

        End Try

        Response.Cache.SetNoStore()
        Response.ContentType = "application/xml"
        Response.Write(tree.GetXml())
        Response.End()

    End Sub

End Class
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="FileAttachments.aspx.vb" Inherits="docAssistWeb.FileAttachment"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>docAssist - File Attachments</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<TABLE class="PageContent" id="Table1" height="600" cellSpacing="0" cellPadding="0" width="905"
				border="0"> <!-- Parent grey container -->
				<TR>
					<TD vAlign="top">
						<DIV class="Container">
							<DIV class="north">
								<DIV class="east">
									<DIV class="south">
										<DIV class="west">
											<DIV class="ne">
												<DIV class="se">
													<DIV class="sw">
														<DIV class="nw">
															<TABLE class="PageContent" height="100%" cellSpacing="0" cellPadding="0" border="0" width="905">
																<TR>
																	<TD noWrap align="left"><SPAN class="Heading">File Attachments</SPAN></TD>
																	<td align="right" vAlign="top">
																		<asp:imagebutton id="btnAddAttachment" runat="server" ToolTip="Add Attachment" ImageUrl="Images/addattachment.gif"></asp:imagebutton></td>
																</TR>
															</TABLE>
															<TABLE class="PageContent" id="tblMain" height="100%" cellSpacing="0" cellPadding="0" border="0">
																<tr>
																	<td vAlign="top" align="right"><br>
																		<div style="OVERFLOW: auto; WIDTH: 905px; HEIGHT: 550px" align="left"><asp:datagrid id="dgFileAttachments" runat="server" Width="100%" CssClass="SearchGrid" AutoGenerateColumns="False"
																				AllowSorting="True">
																				<AlternatingItemStyle BackColor="LightGray"></AlternatingItemStyle>
																				<HeaderStyle Font-Bold="True"></HeaderStyle>
																				<Columns>
																					<asp:TemplateColumn>
																						<HeaderStyle Width="65px"></HeaderStyle>
																						<ItemTemplate>
																							<TABLE cellSpacing="0" cellPadding="0" width="100%">
																								<TR>
																									<TD width="10">
																										<ASP:LINKBUTTON id="lnkDownload" runat="server" CommandName="Download" ForeColor="Blue" Font-Underline="True">
																											<IMG id="imgView" alt="Download Attachment" src="Images/paperclip_small.gif" runat="server"
																												border="0"></ASP:LINKBUTTON></TD>
																									<TD width="10">
																										<ASP:LINKBUTTON id="lnkZipped" runat="server" CommandName="DownloadZip" ForeColor="Blue" Font-Underline="True">
																											<IMG id="imgZip" alt="Download Compressed Attachment" src="Images/zip.gif" runat="server"
																												border="0"></ASP:LINKBUTTON></TD>
																									<TD width="10">
																										<ASP:LINKBUTTON id="lnkDelete" runat="server" CommandName="Delete" ForeColor="Blue" Font-Underline="True">
																											<IMG id="imgDelete" alt="Delete Attachment" src="Images/trash.gif" runat="server" border="0"></ASP:LINKBUTTON></TD>
																									<TD width="10"><IMG id="imgLock" alt="Checked Out" src="Images/lock.gif" runat="server"></TD>
																								</TR>
																							</TABLE>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:TemplateColumn SortExpression="FileName" HeaderText="Filename">
																						<HeaderStyle HorizontalAlign="Left" Width="190px"></HeaderStyle>
																						<ItemTemplate>
																							<asp:Label id=lblAttachmentId runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AttachmentVersionId") %>' Visible="False">
																							</asp:Label><IMG id="FileIcon" height="16" src="Images/file.gif" width="16" runat="server">&nbsp;
																							<asp:Label id=lblFileName runat="server" CssClass="Text" Text='<%# DataBinder.Eval(Container, "DataItem.Filename") %>' Visible="True">
																							</asp:Label>&nbsp;
																							<asp:Label id="lblCheckOutUser" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.CheckOutUser") %>'>
																							</asp:Label>
																							<asp:Label id=lblLocked runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CheckOutStatus") %>' Visible="False">
																							</asp:Label>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:TemplateColumn SortExpression="DocumentTitle" HeaderText="Document Title">
																						<HeaderStyle HorizontalAlign="Left" Width="175px"></HeaderStyle>
																						<ItemTemplate>
																							<asp:Label id="lblDocumentTitle" runat="server" Visible=True Text='<%# DataBinder.Eval(Container, "DataItem.DocumentTitle") %>'>
																							</asp:Label>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:TemplateColumn SortExpression="Description" HeaderText="Description">
																						<HeaderStyle HorizontalAlign="Left" Width="220px"></HeaderStyle>
																						<ItemTemplate>
																							<asp:Label id="lblDescription" runat="server" Visible=True Text='<%# DataBinder.Eval(Container, "DataItem.Description") %>'>
																							</asp:Label>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:TemplateColumn SortExpression="VersionNum" HeaderText="Version">
																						<HeaderStyle HorizontalAlign="Left" Width="55px"></HeaderStyle>
																						<ItemStyle HorizontalAlign="Right"></ItemStyle>
																						<ItemTemplate>
																							<asp:Label id="lblVersion" runat="server" Visible=True Text='<%# DataBinder.Eval(Container, "DataItem.VersionNum") %>'>
																							</asp:Label>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:TemplateColumn SortExpression="Size" HeaderText="Size">
																						<HeaderStyle Wrap="False" HorizontalAlign="Right" Width="65px"></HeaderStyle>
																						<ItemStyle HorizontalAlign="Right"></ItemStyle>
																						<ItemTemplate>
																							<asp:Label id="lblSize" runat="server" Visible=True Text='<%# DataBinder.Eval(Container, "DataItem.Size") %>'>
																							</asp:Label>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:TemplateColumn SortExpression="ModifiedDate" HeaderText="Modified Date">
																						<HeaderStyle Wrap="False" HorizontalAlign="Right" Width="120px"></HeaderStyle>
																						<ItemStyle HorizontalAlign="Right"></ItemStyle>
																						<ItemTemplate>
																							<asp:Label id="lblModifiedDate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ModifiedDate") %>'>
																							</asp:Label>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																				</Columns>
																			</asp:datagrid>&nbsp;
																		</div>
																		<BR>
																		<BR>
																	</td>
																</tr>
															</TABLE>
														</DIV>
													</DIV>
												</DIV>
											</DIV>
										</DIV>
									</DIV>
								</DIV>
							</DIV>
						</DIV>
					</TD>
				</TR>
			</TABLE>
			<DIV></DIV>
			<DIV></DIV>
			<DIV></DIV>
			<DIV></DIV>
			<DIV></DIV>
			<DIV></DIV>
			<DIV></DIV>
			<DIV></DIV>
			<DIV></DIV>
			</TD></TR></TABLE></form>
		<script language="javascript">
			function LoadAttachment(id)
			{
				window.location='EditViewAttachments.aspx?AID='+id;
			}
		</script>
	</body>
</HTML>

<%@ Page Language="vb" AutoEventWireup="false" Codebehind="FolderNotificationsAdd.aspx.vb" Inherits="docAssistWeb.FolderNotificationsAdd"%>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Register TagPrefix="uc1" TagName="docHeader" Src="Controls/docHeader.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<TITLE>docAssist</TITLE>
		<META content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<META content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<META content="JavaScript" name="vs_defaultClientScript">
		<META content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<BODY class="Prefs" bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<FORM id="Form1" method="post" runat="server">
			<br>
			<table cellSpacing="0" cellPadding="0" align="left" border="0" width="92%">
				<tr>
					<td width="20" rowSpan="6" height="15"></td>
					<td class="PrefHeader" height="15">
						Add Folder Notification
					</td>
				</tr>
				<tr>
					<td align="right">
						<img src="dummy.gif" height="10" width="1">
					</td>
				</tr>
				<tr valign="top" align="left">
					<td align="left" width="100%">
						<table class="Prefs" cellSpacing="0" cellPadding="0" align="left" border="0" width="100%">
							<tr>
								<td width="20" rowSpan="12"></td>
								<td class="LabelName" noWrap align="right" style="WIDTH: 205px"></td>
								<td class="Text" bgColor="whitesmoke"></td>
							</tr>
							<tr>
								<td class="Text" noWrap align="right" width="205" style="WIDTH: 205px"></td>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td class="LabelName" noWrap align="right" style="WIDTH: 205px">Folder:</td>
								<td class="Text" bgColor="whitesmoke"><IMG height="1" src="dummy.gif" width="2">&nbsp;<IMG id="ChooseFolder" style="CURSOR: hand" onclick="FolderLookup()" alt="Choose Folder"
										src="Images/addToFolder_btn.gif" runat="server">&nbsp;<span id="FolderName">Click 
										to select a folder</span></td>
							</tr>
							<tr>
								<td class="Text" noWrap align="right" width="205" style="WIDTH: 205px"></td>
								<td>&nbsp;</td>
							</tr>
							<tr align="right">
								<td class="LabelName" noWrap align="right" style="WIDTH: 205px" vAlign="top">Alert 
									Type:</td>
								<td class="Text" bgColor="whitesmoke">
									<asp:RadioButton id="rdoAdd" runat="server" Text="Add" Checked="True"></asp:RadioButton>&nbsp;(notification 
									sent&nbsp;when files or documents are added to a folder)<BR>
									<asp:RadioButton id="rdoUpdate" runat="server" Text="Update"></asp:RadioButton>&nbsp;(notification 
									sent when files or documents in a folder are modified)<BR>
									<asp:RadioButton id="rdoBoth" runat="server" Text="Both"></asp:RadioButton></td>
							</tr>
							<tr>
								<td class="Text" noWrap align="right" width="205" style="WIDTH: 205px"></td>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td class="LabelName" vAlign="top" noWrap align="right" width="176" style="WIDTH: 205px">Schedule:</td>
								<td>
									<table cellSpacing="0" cellPadding="0" width="100%" border="0">
										<tr>
											<td bgColor="whitesmoke"><asp:radiobutton id="rdoRealTime" runat="server" GroupName="Schedule" Checked="True" Text="Real-Time"
													CssClass="Text"></asp:radiobutton></td>
										</tr>
										<tr>
											<td class="Text"><asp:radiobutton id="rdoDaily" runat="server" GroupName="Schedule" Text="Daily at"></asp:radiobutton>&nbsp;
												<ew:timepicker id="txtDaily" runat="server" CssClass="Text" CellPadding="5px" NumberOfColumns="1"
													PopupWidth="75px" PopupHeight="220px" MinuteInterval="FifteenMinutes" Width="57px">
													<ClearTimeStyle BackColor="White"></ClearTimeStyle>
													<TimeStyle Font-Size="8pt" Font-Names="Arial" ForeColor="Black" BackColor="White"></TimeStyle>
													<SelectedTimeStyle Font-Size="8pt" Font-Names="Arial" Font-Bold="True" BackColor="White"></SelectedTimeStyle>
												</ew:timepicker></td>
										</tr>
										<TR>
											<td class="Text" bgColor="whitesmoke"><asp:radiobutton id="rdoWeekly" runat="server" GroupName="Schedule" Text="Weekly at"></asp:radiobutton>&nbsp;
												<ew:timepicker id="txtWeekly" runat="server" CssClass="Text" CellPadding="5px" NumberOfColumns="1"
													PopupWidth="75px" PopupHeight="220px" MinuteInterval="FifteenMinutes" Width="57px">
													<ClearTimeStyle BackColor="White"></ClearTimeStyle>
													<TimeStyle Font-Size="8pt" Font-Names="Arial" ForeColor="Black" BackColor="White"></TimeStyle>
													<SelectedTimeStyle Font-Size="8pt" Font-Names="Arial" Font-Bold="True" BackColor="White"></SelectedTimeStyle>
												</ew:timepicker></td>
										</TR>
										<tr>
											<td class="Text" id="Weekly" bgColor="whitesmoke"><IMG height="1" src="dummy.gif" width="10">
												<asp:checkbox id="chkMonday" runat="server" Text="Monday" Font-Size="8pt"></asp:checkbox>&nbsp;<asp:checkbox id="chkTuesday" runat="server" Text="Tuesday" Font-Size="8pt"></asp:checkbox>&nbsp;<asp:checkbox id="chkWednesday" runat="server" Text="Wednesday" Font-Size="8pt"></asp:checkbox>&nbsp;<asp:checkbox id="chkThursday" runat="server" Text="Thursday" Font-Size="8pt"></asp:checkbox>&nbsp;<asp:checkbox id="chkFriday" runat="server" Text="Friday" Font-Size="8pt"></asp:checkbox>&nbsp;<asp:checkbox id="chkSaturday" runat="server" Text="Saturday" Font-Size="8pt"></asp:checkbox>&nbsp;<asp:checkbox id="chkSunday" runat="server" Text="Sunday" Font-Size="8pt"></asp:checkbox>&nbsp;</td>
										</tr>
									</table>
									<BR>
									<asp:Label id="lblError" runat="server" CssClass="ErrorText" Visible="False"></asp:Label>
								</td>
							</tr>
							<tr>
								<td class="Text" noWrap align="right" width="176" style="WIDTH: 176px"></td>
								<td>&nbsp;<IMG height="1" src="dummy.gif" width="350">
									<asp:imagebutton id="btnAdd" runat="server" ImageUrl="Images/btn_save.gif"></asp:imagebutton></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<input type="hidden" id="FolderId" runat="server">
		</FORM>
		<SCRIPT language="javascript">
			
			disableWeekly();
			
			function validateFolder()
			{
				var FolderId = document.getElementById('FolderId');
				if (FolderId.value == '')
				{
					alert('You must select a folder for this notification.');
					event.returnValue = false;
				}
			}
			
			function FolderLookup()
            {
				var FolderName = document.getElementById('FolderName');
				var val = window.showModalDialog('Tree.aspx?','FolderLookup','resizable:no;status:no;dialogHeight:625px;dialogWidth:250px;scroll:no');
				if (val != undefined)
				{
					varArray = val.split('�');
					document.all.FolderId.value = varArray[0].replace('W','');
					document.all.FolderName.innerHTML = varArray[1];
				}
            }
		
			function disableRealTime()
			{
				document.getElementById('rdoRealTime').disabled = true;
				document.getElementById('rdoRealTime').checked = false;
				document.getElementById('rdoDaily').checked = true;
			}

			function enableRealTime()
			{
				document.getElementById('rdoRealTime').disabled = false;
			}
			
			function disableWeekly()
			{
				document.getElementById('Weekly').disabled = true;
			}

			function enableWeekly()
			{
				document.getElementById('Weekly').disabled = false;
			}
			
			function disableNotAdd()
			{
				document.getElementById('rdoUpdate').checked = false;
				document.getElementById('rdoBoth').checked = false;
			}
			
			function disableNotUpdate()
			{
				document.getElementById('rdoAdd').checked = false;
				document.getElementById('rdoBoth').checked = false;
			}
		
			function disableNotBoth()
			{
				document.getElementById('rdoAdd').checked = false;
				document.getElementById('rdoUpdate').checked = false;
			}
		
		</SCRIPT>
	</BODY>
</HTML>

<%@ Page Language="vb" AutoEventWireup="false" Codebehind="RelatedDocuments.aspx.vb" Inherits="docAssistWeb.RelatedDocuments"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Related Documents</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="css/newStyle.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body class="relatedDocs">
		<form id="frmRelated" method="post" runat="server">
			<asp:Label id="lblDocs" runat="server"></asp:Label>
		</form>
	</body>
</HTML>

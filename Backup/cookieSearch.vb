'<System.Diagnostics.DebuggerStepThrough()> _
Friend Class cookieSearch
    Inherits Object

    Private objPage As System.Web.UI.Page

    Friend Sub New(ByRef page As System.Web.UI.Page)

        objPage = page
        Me.SortColumn = 10020
        Me.SortDirection = 1

    End Sub

    Friend Sub Expire()
        objPage.Session.Clear()
        objPage.Session.Abandon()
    End Sub

#Region "Enumerations"

    Friend Enum SearchTypes
        Search = 0
        PendingDocument = 1
        ProcessedDocument = 2
        UpcomingDocument = 3
    End Enum

#End Region

#Region "Friend Properties"

    Friend Property Keywords() As String
        Get
            Return objPage.Session("Keyworkds")
        End Get
        Set(ByVal Value As String)
            objPage.Session("Keyworkds") = Value
        End Set
    End Property

    Friend Property PageNo() As Integer
        Get
            If objPage.Session("PageNo") Is Nothing Then
                Return 1
            Else
                Return CInt(objPage.Session("PageNo"))
            End If
        End Get
        Set(ByVal Value As Integer)
            objPage.Session("PageNo") = Value
        End Set
    End Property

    Friend Property Orphan() As Boolean
        Get
            If objPage.Session("Orphan") Is Nothing Then
                Return False
            Else
                Return CType(objPage.Session("Orphan"), Boolean)
            End If
        End Get
        Set(ByVal Value As Boolean)
            objPage.Session("Orphan") = Value
        End Set
    End Property

    Friend Property Row() As Integer
        Get
            If objPage.Session("Row") Is Nothing Then
                Return 1
            Else
                Return objPage.Session("Row")
            End If
        End Get
        Set(ByVal Value As Integer)
            objPage.Session("Row") = Value
        End Set
    End Property

    Friend Property SearchAdvanced() As String
        Get
            Return objPage.Session("SearchAdvanced")
        End Get
        Set(ByVal Value As String)
            objPage.Session("SearchAdvanced") = Value
        End Set
    End Property

    Friend Property SearchAttributes() As DataSet
        Get
            Return Functions.ConvertStringTodataset(objPage.Session("SearchAttributes"))
        End Get
        Set(ByVal Value As DataSet)
            If Not Value Is Nothing Then
                objPage.Session("SearchAttributes") = Functions.ConvertDatasetToString(Value)
            Else
                objPage.Session("SearchAttributes") = Nothing
            End If
        End Set
    End Property

    Friend Property SearchType() As SearchTypes
        Get
            If objPage.Session("SearchType") Is Nothing Then
                Return SearchTypes.Search
            Else
                Return objPage.Session("SearchType")
            End If
        End Get
        Set(ByVal Value As SearchTypes)
            objPage.Session("SearchType") = Value
        End Set
    End Property

    Friend Property SortColumn() As Integer
        Get
            If objPage.Session("SortColumn") Is Nothing Then
                Return 10020
            Else
                Return objPage.Session("SortColumn")
            End If
        End Get
        Set(ByVal Value As Integer)
            objPage.Session("SortColumn") = Value
        End Set
    End Property

    Friend Property SortDirection() As Integer
        Get
            If objPage.Session("SortDirection") Is Nothing And Me.SortColumn = 10020 Then
                Return 1
            ElseIf objPage.Session("SortDirection") Is Nothing Then
                Return 0
            Else
                Return objPage.Session("SortDirection")
            End If
        End Get
        Set(ByVal Value As Integer)
            objPage.Session("SortDirection") = Value
        End Set
    End Property

    Friend Property TotalResults() As Integer
        Get
            If Not objPage.Session("TotalResults") Is Nothing Then
                Return objPage.Session("TotalResults")
            Else
                Return -1
            End If
        End Get
        Set(ByVal Value As Integer)
            objPage.Session("TotalResults") = Value
        End Set
    End Property

    Friend Property VersionID() As Integer
        Get
            If Not objPage.Session("VersionID") Is Nothing Then
                Return objPage.Session("VersionID")
            Else
                Return -1
            End If
        End Get
        Set(ByVal Value As Integer)
            objPage.Session("VersionID") = Value
        End Set
    End Property

    Friend Property HideVersionDropDownList() As Boolean
        Get
            If objPage.Session("HideVersionDropDownList") Is Nothing Then
                Return False
            Else
                Return CType(objPage.Session("HideVersionDropDownList"), Boolean)
            End If
        End Get
        Set(ByVal Value As Boolean)
            objPage.Session("HideVersionDropDownList") = Value
        End Set
    End Property

#End Region

End Class
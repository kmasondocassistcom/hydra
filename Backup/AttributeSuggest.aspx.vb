Imports Accucentric.docAssist.Data.Images
Partial Class AttributeSuggest
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private mobjUser As Accucentric.docAssist.Web.Security.User
    Private mconSqlImage As New SqlClient.SqlConnection

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try

            Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
            Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))
            Me.mobjUser.AccountId = guidAccountId
            Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)

            Dim objSM As New SystemUtilities(Me.mconSqlImage, True)

            Dim dt As DataTable = objSM.AttributeSuggest(Me.mobjUser.ImagesUserId, Request.QueryString("did"), Request.QueryString("aid"), Request.QueryString("q"))

            Dim sb As New System.Text.StringBuilder
            For Each dr As DataRow In dt.Rows
                sb.Append(dr("AttributeValue") & vbCrLf)
            Next

            Response.Write(sb.ToString)

        Catch ex As Exception

        End Try

    End Sub

End Class

<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ShareItem.aspx.vb" Inherits="docAssistWeb.ShareItem"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Share</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="css/base.css" type="text/css" rel="stylesheet">
		<LINK href="css/buttons.css" type="text/css" rel="stylesheet">
		<script src="scripts/jquery.js"></script>
		<script src="scripts/share.js"></script>
	</HEAD>
	<body>
		<form id="frmShare" method="post" runat="server">
			<div><label id="lEmail" runat="server">E-mail address </label>
				<br>
				<input id="address" type="text" size="50" runat="server"><BUTTON class="btn" id="btnInvite" style="PADDING-LEFT: 10px" type="button" runat="server"><SPAN><SPAN>Invite</SPAN></SPAN></BUTTON>
			</div>
			<div><label>Subject (optional)</label><br>
				<input id="subject" type="text" size="57" runat="server"></div>
			<div style="PADDING-BOTTOM: 20px"><label>Message (optional)</label><br>
				<textarea id="message" style="WIDTH: 420px" rows="3" runat="server"></textarea></div>
			<table width="420">
				<tr>
					<td><label>People with access</label></td>
					<td align="right">
						<div><BUTTON class="btn" id="btnRemove" style="PADDING-LEFT: 10px" type="button" runat="server"><SPAN><SPAN>Remove 
										Selected</SPAN></SPAN></BUTTON></div>
					</td>
				</tr>
			</table>
			<div><asp:listbox id="lstShares" runat="server" Rows="7" Width="420px"></asp:listbox>&nbsp;&nbsp;&nbsp;
			</div>
		</form>
	</body>
</HTML>

<%@ Page Language="vb" AutoEventWireup="false" Codebehind="AddItem.aspx.vb" Inherits="docAssistWeb.AddItem"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title></title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link href="Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<table cellpadding="4" cellspacing="4" width="250" border="0" class="PageContent">
				<tr>
					<td colspan="2">Folder Name:
						<asp:TextBox id="txtFolderName" runat="server" Width="145px"></asp:TextBox>
					</td>
				</tr>
				<tr>
					<td></td>
					<td align="right">
						<asp:Button id="btnCancel" runat="server" Text="Cancel"></asp:Button>&nbsp;
						<asp:Button id="btnOK" runat="server" Text="OK"></asp:Button></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>

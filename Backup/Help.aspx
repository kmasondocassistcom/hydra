<%@ Page Language="vb" AutoEventWireup="false" Codebehind="Help.aspx.vb" Inherits="docAssistWeb.Help"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML xmlns:o>
	<HEAD>
		<title>docAssist Help</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<table cellSpacing="0" cellPadding="0" width="700" align="left" border="0">
				<tr>
					<td align="right" colSpan="3"><IMG src="Images/doc_header_logo.gif"></td>
				</tr>
				<tr>
					<td><IMG height="1" src="spacer.gif" width="3"></td>
					<td width="100%"><font face="Verdana" size="2">
							<P></P>
							<P></P>
							<P style="LINE-HEIGHT: 130%"></P>
							<P style="LINE-HEIGHT: 130%"><b>Search</b></P>
							<P style="FONT-SIZE: 8pt; LINE-HEIGHT: 130%; FONT-FAMILY: Verdana"><EM><STRONG>Basics of <a name="Search">
										</a>Search</STRONG></EM><BR>
								<BR>
								Select one or more documents to search-<BR>
								To perform a search first select the document you wish to search for from the 
								document list box. To search more than one document at the same time hold down 
								the Ctrl Key and click on all the documents you wish to search for.
								<BR>
								<BR>
								Enter search terms- Once you have selected the document(s) you wish to search 
								for, type one or more search values into the appropriate document boxes listed 
								under the SearchCriteria section. To perform wildcard searches use the * 
								asterisk value. You may enter one or more document values to narrow your 
								search. To search for multiple values of the same document index attributes 
								separate with ; semicolon.
								<BR>
								<BR>
								Once you have entered your search values press Enter or click the Search 
								button.<BR>
								<BR>
								<EM><STRONG>Search Examples</STRONG></EM>
								<BR>
								<BR>
							</P>
							<P style="FONT-SIZE: 8pt; LINE-HEIGHT: 130%; FONT-FAMILY: Verdana" align="center">
								<TABLE class="MsoTableGrid" id="Table1" style="BORDER-RIGHT: medium none; BORDER-TOP: medium none; BORDER-LEFT: medium none; BORDER-BOTTOM: medium none; BORDER-COLLAPSE: collapse; mso-border-alt: solid windowtext .5pt; mso-yfti-tbllook: 480; mso-padding-alt: 0in 5.4pt 0in 5.4pt; mso-border-insideh: .5pt solid windowtext; mso-border-insidev: .5pt solid windowtext"
									cellSpacing="0" cellPadding="0" border="1">
									<TR style="mso-yfti-irow: 0; mso-yfti-firstrow: yes">
										<TD style="BORDER-RIGHT: windowtext 1pt solid; PADDING-RIGHT: 5.4pt; BORDER-TOP: windowtext 1pt solid; PADDING-LEFT: 5.4pt; PADDING-BOTTOM: 0in; BORDER-LEFT: windowtext 1pt solid; WIDTH: 88.55pt; PADDING-TOP: 0in; BORDER-BOTTOM: windowtext 1pt solid; mso-border-alt: solid windowtext .5pt"
											vAlign="top" width="118">
											<P class="MsoNormal"><SPAN style="FONT-SIZE: 8pt">15*
													<o:p></o:p></SPAN></P>
										</TD>
										<TD style="BORDER-RIGHT: windowtext 1pt solid; PADDING-RIGHT: 5.4pt; BORDER-TOP: windowtext 1pt solid; PADDING-LEFT: 5.4pt; PADDING-BOTTOM: 0in; BORDER-LEFT: medium none; WIDTH: 354.25pt; PADDING-TOP: 0in; BORDER-BOTTOM: windowtext 1pt solid; mso-border-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt"
											vAlign="top" width="472">
											<P class="MsoNormal"><SPAN style="FONT-SIZE: 8pt">Returns all matches beginning with 
													15
													<o:p></o:p></SPAN></P>
										</TD>
									</TR>
									<TR style="mso-yfti-irow: 1">
										<TD style="BORDER-RIGHT: windowtext 1pt solid; PADDING-RIGHT: 5.4pt; BORDER-TOP: medium none; PADDING-LEFT: 5.4pt; PADDING-BOTTOM: 0in; BORDER-LEFT: windowtext 1pt solid; WIDTH: 88.55pt; PADDING-TOP: 0in; BORDER-BOTTOM: windowtext 1pt solid; mso-border-alt: solid windowtext .5pt; mso-border-top-alt: solid windowtext .5pt"
											vAlign="top" width="118">
											<P class="MsoNormal"><SPAN style="FONT-SIZE: 8pt">*15*
													<o:p></o:p></SPAN></P>
										</TD>
										<TD style="BORDER-RIGHT: windowtext 1pt solid; PADDING-RIGHT: 5.4pt; BORDER-TOP: medium none; PADDING-LEFT: 5.4pt; PADDING-BOTTOM: 0in; BORDER-LEFT: medium none; WIDTH: 354.25pt; PADDING-TOP: 0in; BORDER-BOTTOM: windowtext 1pt solid; mso-border-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; mso-border-top-alt: solid windowtext .5pt"
											vAlign="top" width="472">
											<P class="MsoNormal"><SPAN style="FONT-SIZE: 8pt">Returns all matches containing 15
													<o:p></o:p></SPAN></P>
										</TD>
									</TR>
									<TR style="mso-yfti-irow: 2">
										<TD style="BORDER-RIGHT: windowtext 1pt solid; PADDING-RIGHT: 5.4pt; BORDER-TOP: medium none; PADDING-LEFT: 5.4pt; PADDING-BOTTOM: 0in; BORDER-LEFT: windowtext 1pt solid; WIDTH: 88.55pt; PADDING-TOP: 0in; BORDER-BOTTOM: windowtext 1pt solid; mso-border-alt: solid windowtext .5pt; mso-border-top-alt: solid windowtext .5pt"
											vAlign="top" width="118">
											<P class="MsoNormal"><SPAN style="FONT-SIZE: 8pt">ABC
													<o:p></o:p></SPAN></P>
										</TD>
										<TD style="BORDER-RIGHT: windowtext 1pt solid; PADDING-RIGHT: 5.4pt; BORDER-TOP: medium none; PADDING-LEFT: 5.4pt; PADDING-BOTTOM: 0in; BORDER-LEFT: medium none; WIDTH: 354.25pt; PADDING-TOP: 0in; BORDER-BOTTOM: windowtext 1pt solid; mso-border-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; mso-border-top-alt: solid windowtext .5pt"
											vAlign="top" width="472">
											<P class="MsoNormal"><SPAN style="FONT-SIZE: 8pt">Returns all matches containing 
													exactly ABC
													<o:p></o:p></SPAN></P>
										</TD>
									</TR>
									<TR style="mso-yfti-irow: 3; mso-yfti-lastrow: yes">
										<TD style="BORDER-RIGHT: windowtext 1pt solid; PADDING-RIGHT: 5.4pt; BORDER-TOP: medium none; PADDING-LEFT: 5.4pt; PADDING-BOTTOM: 0in; BORDER-LEFT: windowtext 1pt solid; WIDTH: 88.55pt; PADDING-TOP: 0in; BORDER-BOTTOM: windowtext 1pt solid; mso-border-alt: solid windowtext .5pt; mso-border-top-alt: solid windowtext .5pt"
											vAlign="top" width="118">
											<P class="MsoNormal"><SPAN style="FONT-SIZE: 8pt">ABC;15*
													<o:p></o:p></SPAN></P>
										</TD>
										<TD style="BORDER-RIGHT: windowtext 1pt solid; PADDING-RIGHT: 5.4pt; BORDER-TOP: medium none; PADDING-LEFT: 5.4pt; PADDING-BOTTOM: 0in; BORDER-LEFT: medium none; WIDTH: 354.25pt; PADDING-TOP: 0in; BORDER-BOTTOM: windowtext 1pt solid; mso-border-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; mso-border-top-alt: solid windowtext .5pt"
											vAlign="top" width="472">
											<P class="MsoNormal"><SPAN style="FONT-SIZE: 8pt">Returns all matches containing 
													exactly ABC AND beginning with 15
													<o:p></o:p></SPAN></P>
										</TD>
									</TR>
								</TABLE>
							<p style="FONT-SIZE: 8pt; FONT-FAMILY: verdana" align="left">
								<STRONG><EM>Advanced Searches</EM><BR>
								</STRONG>
								<BR>
								On occasion, you may need to use advanced search to located the document(s) you 
								are attempting to locate. There are several advanced options you can use to 
								locate a document.<BR>
								<BR>
								Full-Text - To perform a full-text search, enter keywords and/or phrases in the 
								box provided. The full-text search has several options to help narrow your 
								search.<BR>
								<BR>
								Match Exact Phrase  If selected, only documents that contain the exact phrase 
								will be returned as a match.<BR>
								<BR>
								Search Within Words - If selected, document containing the keywords searched 
								will be returned if they contain those keywords. For example, a document that 
								contains the word password will return as a match if the keyword word was 
								used and this option was selected.<BR>
								<BR>
								Documents Without Attributes - This search option is used for documents that do 
								not contain any indexing values.<BR>
								<BR>
								Search by Modified/Added User - This search is used to filter search results 
								based on either the user that added the document to the system or last modified 
								the document.
								<BR>
								<BR>
								<BR>
								<B>
									<p style="FONT-SIZE: 9pt; FONT-FAMILY: verdana">
										<a name="Results"></a>
									Search Results</B><BR>
								<BR>
								<EM><STRONG>Basics of Search Results</STRONG>
									<BR>
									<BR>
									<p style="FONT-SIZE: 8pt">
								</EM>All of the matching documents returned by your search are displayed in a 
								grid format. The number of documents matching your search is displayed in the 
								top right corner of the grid. If more results are returned than can be 
								displayed, a page navigation tool is displayed in the lower left of the grid. 
								Click on the page number to navigate to other results. You can adjust the 
								number of rows returned on each page through the preferences icon on the top 
								right of the page.<BR>
								&nbsp;
								<TABLE id="Table2" cellSpacing="0" cellPadding="0" width="100%" border="0" style="FONT-SIZE: 8pt; FONT-FAMILY: verdana">
									<TR>
										<TD style="WIDTH: 41px" align="center">
											<IMG src="Images/bot24.gif"></TD>
										<TD align="left">Viewing a document- You can select any document to view from the 
											display grid. To select a document simply click on the View Icon for the 
											appropriate line or any portion of that row.</TD>
									</TR>
								</TABLE>
								<BR>
								<TABLE id="Table3" style="FONT-SIZE: 8pt; FONT-FAMILY: verdana" cellSpacing="0" cellPadding="0"
									width="100%" border="0">
									<TR>
										<TD style="WIDTH: 41px" align="center">
											<IMG src="Images/thum_on.gif"></TD>
										<TD align="left">Viewing document thumbnails- You can view the entire document in 
											thumbnail format by selecting the&nbsp;thumbnail icon for the appropriate row.</TD>
									</TR>
								</TABLE>
								<BR>
								Sorting Results - You can sort all of the results returned by clicking on the 
								column header of the grid. The first time you select a row it will re-display 
								the items in ascending order, clicking that same column header once more will 
								re-display the lines in descending order.<BR>
								<BR>
							</p>
							<B>
								<p style="FONT-SIZE: 9pt; FONT-FAMILY: verdana"><BR>
								<a name="Viewer"> </a>Viewer </B>
							<P style="FONT-SIZE: 8pt; FONT-FAMILY: verdana">The document viewer page displays 
								both the document and the documents indexing values. Please note that based on 
								your permissions you may or may not have access to certain features described 
								below. To obtain permissions contact your system administrator.<BR>
								<BR>
								<STRONG><EM>Document Basics</EM></STRONG><BR>
								<BR>
								<EM>Navigation Tools<BR>
									<BR>
								</EM>
							</P>
							<P style="FONT-SIZE: 8pt; FONT-FAMILY: verdana">
								<TABLE id="Table5" style="FONT-SIZE: 8pt; FONT-FAMILY: verdana" cellSpacing="0" cellPadding="0"
									width="100%" border="0">
									<TR>
										<TD style="WIDTH: 41px" align="center"><IMG src="Images/bot25.gif"></TD>
										<TD align="left">Go to <STRONG>first</STRONG> page of document.</TD>
									</TR>
									<TR>
										<TD style="WIDTH: 41px" align="center"><IMG src="Images/bot02.gif"></TD>
										<TD align="left">Go to <STRONG>previous</STRONG> page of document.</TD>
									</TR>
									<TR>
										<TD style="WIDTH: 41px" align="center"><IMG src="Images/bot03.gif"></TD>
										<TD align="left">Go to <STRONG>next</STRONG> page of document.</TD>
									</TR>
									<TR>
										<TD style="WIDTH: 41px" align="center"><IMG src="Images/bot26.gif"></TD>
										<TD align="left">Go to <STRONG>last</STRONG> page of document.</TD>
									</TR>
									<TR>
										<TD style="WIDTH: 41px" align="center"><IMG src="Images/back_to_results.gif"></TD>
										<TD align="left">Return to Search Results  Returns you to the last search grid.</TD>
									</TR>
									<TR>
										<TD style="WIDTH: 41px" align="center"><IMG src="Images/doc_hist.gif"></TD>
										<TD align="left">Document History  Displays the revision history for the document 
											displayed.</TD>
									</TR>
									<TR>
										<TD style="WIDTH: 41px" align="center"><IMG src="Images/fulltext.gif"></TD>
										<TD align="left">Document Text  Displays the document text for all pages of the 
											current document.</TD>
									</TR>
									<TR>
										<TD style="WIDTH: 41px" align="center"><IMG src="Images/results_previous.gif"></TD>
										<TD align="left">Previous Result  Opens the previous document from the search 
											results</TD>
									</TR>
									<TR>
										<TD style="WIDTH: 41px" align="center"><IMG src="Images/results_next.gif"></TD>
										<TD align="left">Next Result  Opens the next document from the search results</TD>
									</TR>
									<TR>
										<TD style="WIDTH: 41px" align="center"><IMG src="Images/save.gif"></TD>
										<TD align="left">Save Changes  Click to save attributes and any document changes 
											made.</TD>
									</TR>
								</TABLE>
							</P>
							<P style="FONT-SIZE: 8pt; FONT-FAMILY: verdana"><EM>Viewer Tools<BR>
									<BR>
								</EM>
							</P>
							<P style="FONT-SIZE: 8pt; FONT-FAMILY: verdana">
								<TABLE id="Table4" style="FONT-SIZE: 8pt; FONT-FAMILY: verdana" cellSpacing="0" cellPadding="0"
									width="100%" border="0">
									<TR>
										<TD style="WIDTH: 41px" align="center"><IMG src="Images/bot06.gif"></TD>
										<TD align="left">Zoom area  Click, hold the left mouse button and drag an area on 
											the document to zoom. Relase the mouse button and that area will be displayed.</TD>
									</TR>
									<TR>
										<TD style="WIDTH: 41px" align="center"><IMG src="Images/bot05.gif"></TD>
										<TD align="left">Maginifier  Click and hold the left mouse button and drag across 
											document to magnify a region of the document.</TD>
									</TR>
									<TR>
										<TD style="WIDTH: 41px" align="center"><IMG src="Images/bot07.gif"></TD>
										<TD align="left">Zoom In  Click the document to zoom in incrementally, each click 
											will increase the zoom factor.</TD>
									</TR>
									<TR>
										<TD style="WIDTH: 41px" align="center"><IMG src="Images/bot08.gif"></TD>
										<TD align="left">Zoom Out - Click the document to zoom in incrementally, each click 
											will decrease the zoom factor.</TD>
									</TR>
									<TR>
										<TD style="WIDTH: 41px" align="center"><IMG src="Images/bot10.gif"></TD>
										<TD align="left">Select  Used to highlight a region of the document. Used 
											primarily with the indexing of documents to automate the indexing of values.</TD>
									</TR>
									<TR>
										<TD style="WIDTH: 41px" align="center"><IMG src="Images/bot09.gif"></TD>
										<TD align="left">Pan  Click and hold the left muse button to pan through the 
											document.</TD>
									</TR>
									<TR>
										<TD style="WIDTH: 41px" align="center"><IMG src="Images/fulltext.gif"></TD>
										<TD align="left">Best Fit- Redisplays the document to fit best to the screen size.</TD>
									</TR>
									<TR>
										<TD style="WIDTH: 41px" align="center"><IMG src="Images/bot12.gif"></TD>
										<TD align="left">Fit Width  Redisplays the document to fit the width of the screen 
											size.</TD>
									</TR>
									<TR>
										<TD style="WIDTH: 41px" align="center"><IMG src="Images/bot13.gif"></TD>
										<TD align="left">Fit Height  Redisplays the document to fit the height of the 
											screen size.</TD>
									</TR>
									<TR>
										<TD style="WIDTH: 41px" align="center"><IMG src="Images/bot14.gif"></TD>
										<TD align="left">Rotate Left  Rotates the document counter-clockwise 90 degrees.</TD>
									</TR>
									<TR>
										<TD style="WIDTH: 41px" align="center"><IMG src="Images/bot15.gif"></TD>
										<TD align="left">
											Rotate&nbsp;Right  Rotates the document clockwise 90 degrees.</TD>
									</TR>
									<TR>
										<TD style="WIDTH: 41px" align="center"><IMG src="Images/bot17.gif"></TD>
										<TD align="left">Thumbnail View  Displays the document pages in a thumbnail view.</TD>
									</TR>
									<TR>
										<TD style="WIDTH: 41px" align="center"><IMG src="Images/bot18.gif"></TD>
										<TD align="left">Print  Prompts for printing options.</TD>
									</TR>
									<TR>
										<TD style="WIDTH: 41px" align="center"><IMG src="Images/bot19.gif"></TD>
										<TD align="left">Email/Export  Provides for the email or export of the current 
											document.</TD>
									</TR>
									<TR>
										<TD style="WIDTH: 41px" align="center"><IMG src="Images/delete.gif"></TD>
										<TD align="left">
											Delete  Delete pages of the entire document.</TD>
									</TR>
								</TABLE>
								<BR>
								<EM>Adding/Changing Attributes</EM><BR>
								<BR>
								<TABLE id="Table6" style="FONT-SIZE: 8pt; FONT-FAMILY: verdana" cellSpacing="0" cellPadding="0"
									width="100%" border="0">
									<TR>
										<TD style="WIDTH: 41px" align="center"><IMG src="Images/bot23.gif"></TD>
										<TD align="left">Add Attribute  click to add a new attribute row.</TD>
									</TR>
									<TR>
										<TD style="WIDTH: 41px" align="center"><IMG src="Images/bot24.gif"></TD>
										<TD align="left">Select row  Selects row to perform changes/deletions.</TD>
									</TR>
									<TR>
										<TD style="WIDTH: 41px" align="center"><IMG src="Images/check.gif"></TD>
										<TD align="left">Accept  Click to accept the changes made to the current row.</TD>
									</TR>
									<TR>
										<TD style="WIDTH: 41px" align="center"><IMG src="Images/bot20.gif"></TD>
										<TD align="left">Delete  Click to delete the current row.</TD>
									</TR>
									<TR>
										<TD style="WIDTH: 41px; HEIGHT: 14px" align="center"><IMG src="Images/undo_attribute.gif"></TD>
										<TD align="left" style="HEIGHT: 14px">Undo  Click to undo any changes made to the 
											current row.</TD>
									</TR>
									<TR>
										<TD style="WIDTH: 41px" align="center"><IMG src="Images/ocr.gif"></TD>
										<TD align="left">
											SmartScan  Click to retrieve values from the highlighted region (select tool).</TD>
									</TR>
								</TABLE>
						</font></P></td>
					<td><IMG height="1" src="spacer.gif" width="3"></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>

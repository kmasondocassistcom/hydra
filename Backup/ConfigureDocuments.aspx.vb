Imports Accucentric.docAssist

Partial Class _Documents
    Inherits System.Web.UI.Page

    Private mobjUser As Web.Security.User
    Private mconSqlImage As SqlClient.SqlConnection

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        '
        ' 
        '
        Try
            If Request.Cookies("docAssist") Is Nothing Then
                Response.Redirect("Default.aspx")
                Exit Sub
            End If

            Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
            Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))

            Me.mobjUser.AccountId = guidAccountId
            Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)

        Catch ex As Exception
            Response.Redirect("default.aspx")
            Response.Flush()
            Response.End()
        End Try
        '
        ' Add object events
        '
        btnSave.Attributes("OnClick") = "SelectAllAssigned();"
        lstAssigned.Attributes("OnChange") = "DocumentSelected(this);"
        lstAvailable.Attributes("OnChange") = "DocumentSelected(this);"
        ddlFolders.Attributes("OnChange") = "ChangeFolders();"
        '
        ' Load up the objects
        '
        If Not Page.IsPostBack Then
            Try
                hostname.Value = Request.Url.AbsoluteUri.Substring(0, Request.Url.AbsoluteUri.Length - Request.Url.AbsolutePath.Length)
#If DEBUG Then
                hostname.Value = hostname.Value & "/docAssistWeb1"
#End If
                If Not Session("Attributes") Is Nothing Then Session("Attributes") = Nothing
                If Not Session("Documents") Is Nothing Then Session("Documents") = Nothing
                If Not Session("CabinetFolders") Is Nothing Then Session("CabinetFolders") = Nothing

                Dim dsDocuments As dsConfiguration = GetConfiguration()
                '
                ' Bind the folders
                ddlFolders.DataSource = dsDocuments.CabinetFolders
                ddlFolders.DataTextField = "FolderName"
                ddlFolders.DataValueField = "FolderId"
                ddlFolders.DataBind()

                Session("Documents") = dsDocuments

            Catch ex As Exception
                Throw ex
            Finally
                If Not Me.mconSqlImage.State = ConnectionState.Closed Then Me.mconSqlImage.Close()
            End Try
        End If


    End Sub

    Private Function GetConfiguration() As dsConfiguration

        Dim ds As New dsConfiguration

        'ds.Relations.Remove("CabinetsCabinetFolders")

        Dim cmdSqlDocuments As New SqlClient.SqlCommand("acsDocuments", Me.mconSqlImage)
        Dim cmdSqlFolders As New SqlClient.SqlCommand("SELECT * FROM CabinetFolders ORDER BY FolderName", Me.mconSqlImage)
        Dim cmdSqlDocumentLocations As New SqlClient.SqlCommand("acsDocumentLocations", Me.mconSqlImage)

        cmdSqlDocuments.CommandTimeout = DEFAULT_COMMAND_TIMEOUT
        cmdSqlDocuments.CommandType = CommandType.StoredProcedure
        cmdSqlDocuments.Parameters.Add("@UseSecurity", "0")

        cmdSqlFolders.CommandTimeout = DEFAULT_COMMAND_TIMEOUT

        cmdSqlDocumentLocations.CommandTimeout = DEFAULT_COMMAND_TIMEOUT
        cmdSqlDocumentLocations.CommandType = CommandType.StoredProcedure
        cmdSqlDocumentLocations.Parameters.Add("@UseSecurity", "0")

        If Not Me.mconSqlImage.State = ConnectionState.Open Then Me.mconSqlImage.Open()

        Dim da As New SqlClient.SqlDataAdapter

        da.SelectCommand = cmdSqlDocuments
        da.Fill(ds.Documents)

        da.SelectCommand = cmdSqlFolders
        da.Fill(ds.CabinetFolders)

        da.SelectCommand = cmdSqlDocumentLocations
        da.Fill(ds.DocumentLocations)

        Return ds

    End Function

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click

        Dim dsConfig As dsConfiguration = CType(Session("Documents"), dsConfiguration)
        Dim intFolderId As Integer = ddlFolders.SelectedValue
        '
        ' Update documents
        '
        Dim strNewItems() As String = hiddenNewDocuments.Value.Split(",")
        UpdateDocuments(strNewItems, dsConfig)
        Dim strAssignedItems() As String = hiddenDocumentLocations.Value.Split(",")
        UpdateDocuments(strAssignedItems, dsConfig)
        '
        ' Check for new assigned documents
        '
        Dim strItem As String
        For Each strItem In strAssignedItems
            Dim strValues() As String = strItem.Split(";")
            Dim strFind(2) As String
            strFind(0) = 0
            strFind(1) = intFolderId
            strFind(2) = CInt(strvalues(0))
            Dim dr As dsConfiguration.DocumentLocationsRow = dsConfig.DocumentLocations.Rows.Find(strFind)
            If dr Is Nothing Then
                Dim drLocation As dsConfiguration.DocumentLocationsRow = dsConfig.DocumentLocations.NewRow
                drLocation.CompanyID = 0
                drLocation.DocumentID = CInt(strValues(0))
                drLocation.FolderID = intFolderId
                dsConfig.DocumentLocations.Rows.Add(drLocation)
            End If
        Next
        '
        ' Check for removed documents
        '
        If strNewItems.Length > 1 Or strNewItems(0).Trim.Length > 0 Then
            For Each strItem In strNewItems
                Dim strValues() As String = strItem.Split(";")
                Dim strFind(2) As String
                strFind(0) = 0
                strFind(1) = intFolderId
                strFind(2) = CInt(strvalues(0))
                Dim dr As dsConfiguration.DocumentLocationsRow = dsConfig.DocumentLocations.Rows.Find(strFind)
                If Not dr Is Nothing Then
                    dr.Delete()
                End If
            Next
        End If
        '
        ' Save the changes back to the server
        '
        Dim trnSql As SqlClient.SqlTransaction
        Try
            Me.mconSqlImage.Open()
            trnSql = Me.mconSqlImage.BeginTransaction

            Data.Images.UpdateDocuments(dsConfig.Documents, Me.mconSqlImage, trnSql)
            Data.Images.UpdateDocumentLocations(dsConfig.DocumentLocations, Me.mconSqlImage, trnSql)

            trnSql.Commit()

            dsConfig.WriteXml("c:\temp\dsconfig.xml")

            showMessage.InnerHtml = "Documents were saved succesfully."
        Catch ex As Exception
            trnSql.Rollback()
#If DEBUG Then
            showMessage.InnerHtml = ex.Message & "<BR>" & ex.StackTrace
#Else
            showMessage.InnerHtml = "An error occured during the save.  Your changes have not been saved."
#End If

        Finally
            trnSql.Dispose()
            Session("Documents") = dsConfig
            If Not Me.mconSqlImage.State = ConnectionState.Closed Then Me.mconSqlImage.Close()
        End Try


    End Sub

    Private Function UpdateDocuments(ByVal strItems() As String, ByRef dsConfig As dsConfiguration) As Boolean

        Dim dr As DataRow
        If strItems.Length > 1 Or strItems(0).Trim.Length > 0 Then
            For Each strItem As String In strItems
                Dim strValues() As String = strItem.Split(";")
                Dim drDocument As dsConfiguration.DocumentsRow
                Dim intDocumentId As Integer = CInt(strValues(0))
                If intDocumentId < 0 Then
                    '
                    ' new document
                    drDocument = dsConfig.Documents.NewRow
                    drDocument.DocumentName = strValues(1)
                    dsConfig.Documents.Rows.Add(drDocument)
                Else
                    '
                    ' updated document
                    drDocument = dsConfig.Documents.Rows.Find(strValues(0))
                    If Not drDocument Is Nothing Then
                        If Not drDocument.RowState = DataRowState.Deleted Then
                            If Not drDocument.DocumentName = strValues(1) Then drDocument.DocumentName = strValues(1)
                        End If
                    End If
                End If
            Next
        End If
        Return True
    End Function
End Class

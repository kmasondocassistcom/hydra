Imports Accucentric.docAssist.Data.Images

Partial Class AccountInboundEmailList
    Inherits System.Web.UI.Page

    Dim mobjUser As Accucentric.docAssist.Web.Security.User

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents rdoNeverExpire As System.Web.UI.WebControls.RadioButton
    Protected WithEvents rdoExpireInterval As System.Web.UI.WebControls.RadioButton
    Protected WithEvents txtDays As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnSave As System.Web.UI.WebControls.ImageButton

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try
            mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
            Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))
            mobjUser.AccountId = guidAccountId
        Catch ex As Exception
            Response.Redirect("Default.aspx")
        End Try

        Dim dtEmailList As DataTable

        Try
            Dim sb As New System.Text.StringBuilder

            If Not Page.IsPostBack Then

                Dim objFolders As New Folders(Functions.BuildConnection(Me.mobjUser))
                Dim ds As New DataSet

                ds = objFolders.EmailInboundListGet(Me.mobjUser.AccountId.ToString)
                If ds.Tables.Count > 0 Then
                    dtEmailList = ds.Tables(0)
                    'List out dtEmailList Columns are .... FullAlias(show as emailaddress, make it a link), FolderPath(show as header "Location"),Enabled (checkbox?)
                    sb.Append("<TABLE class=""SearchGrid"" width=""100%"">")

                    sb.Append("<TR>")

                    sb.Append("<TD width=33%><b>E-Mail Address</b>")
                    sb.Append("</TD>")
                 
                    sb.Append("<TD width=33%><b>Folder Path</b>")
                    sb.Append("</TD>")

                    sb.Append("<TD width=33%><b>Status</b>")
                    sb.Append("</TD>")

                    sb.Append("</TR>")

                    Dim x As Integer = 1

                    For Each dr As DataRow In dtEmailList.Rows

                        If x Mod 2 = 0 Then
                            sb.Append("<TR bgcolor=#dcdcdc>")
                        Else
                            sb.Append("<TR>")
                        End If

                        sb.Append("<TD width=33%>")
                        sb.Append("<a href=mailto:" & dr("FullAlias") & ">" & dr("FullAlias") & "</a>")
                        sb.Append("</TD>")

                        sb.Append("<TD width=33%>")
                        sb.Append(dr("FolderPath"))
                        sb.Append("</TD>")

                        sb.Append("<TD width=33%>")
                        Select Case dr("Enabled")
                            Case "True"
                                'sb.Append("<input type=checkbox checked=true>")
                                sb.Append("Enabled")
                            Case "False"
                                'sb.Append("<input type=checkbox checked=false>")
                                sb.Append("Disabled")
                        End Select
                        sb.Append("</TD>")

                        sb.Append("</TR>")

                        x += 1

                    Next

                    sb.Append("</TABLE>")

                Else

                    sb.Append("No inbound e-mail accounts have been configured for this account.")

                End If

            End If

            emailListing.InnerHtml = sb.ToString

        Catch ex As Exception

        Finally

        End Try

    End Sub

End Class
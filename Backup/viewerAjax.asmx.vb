Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Web.Services.Description
Imports System.IO
Imports System.Data.SqlClient
Imports System.Threading

Imports Accucentric.docAssist.Data.Images
Imports Accucentric.docAssist

Imports Atalasoft.Imaging
Imports Atalasoft.Imaging.Codec
Imports Atalasoft.Imaging.Codec.Tiff
Imports Atalasoft.Imaging.Metadata
Imports Atalasoft.Imaging.Drawing
Imports Atalasoft.Imaging.ImageProcessing
Imports Atalasoft.Imaging.ImageProcessing.Document
Imports Atalasoft.Imaging.ImageProcessing.Transforms
Imports Atalasoft.Imaging.WebControls
Imports Atalasoft.Imaging.WebControls.Annotations
Imports Atalasoft.Annotate
Imports Atalasoft.Annotate.UI
Imports Atalasoft.Annotate.Renderer

<System.Web.Services.WebService(Namespace:="http://docassist.com/viewer")> _
Public Class ViewerAJAX
    Inherits System.Web.Services.WebService

#Region " Web Services Designer Generated Code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Web Services Designer.
        InitializeComponent()

        'Add your own initialization code after the InitializeComponent() call

    End Sub

    'Required by the Web Services Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Web Services Designer
    'It can be modified using the Web Services Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        components = New System.ComponentModel.Container
    End Sub

    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        'CODEGEN: This procedure is required by the Web Services Designer
        'Do not modify it using the code editor.
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

#End Region

    Private mobjUser As Accucentric.docAssist.Web.Security.User

    <WebMethod(EnableSession:=True)> _
    Public Function ScanConfig(ByVal mUserID As String, ByVal mAccountID As String, ByVal mAuthenticationKey As String) As DataSet

        Dim mSessionID As String
        Dim intReturn As Integer = 0
        Dim consql As New SqlClient.SqlConnection
        Dim dsReturn As New DataSet("Config")
        Dim da As New SqlClient.SqlDataAdapter
        Dim objEncryption As New Encryption.Encryption

        Try

            'Get the users current sessionid
            Dim consqluser As New SqlConnection

            consqluser.ConnectionString = objEncryption.Decrypt(ConfigurationSettings.AppSettings("Connection"))
            If Not consqluser.State = ConnectionState.Open Then consqluser.Open()
            Dim dtUser As New DataTable
            Dim cmdUSql As New SqlClient.SqlCommand("SELECT SessionID FROM USERS WHERE UserID = @UserID", consqluser)
            cmdUSql.Parameters.Add("@UserID", mUserID)
            cmdUSql.CommandTimeout = 120
            Dim daSql As New SqlClient.SqlDataAdapter(cmdUSql)
            daSql.Fill(dtUser)

            If dtUser.Rows.Count <= 0 Then
                Return Nothing
                Exit Function
            Else
                mSessionID = dtUser.Rows(0).Item("SessionID")
            End If

            Me.mobjUser = BuildUserObject(mSessionID, mAccountID)

            If Not Me.mobjUser Is Nothing Then
                consql = BuildConnection(Me.mobjUser)
                Dim cmdSql As New SqlClient.SqlCommand("SMPanelConfig", consql)
                cmdSql.CommandType = CommandType.StoredProcedure
                Dim guidSessionID As New System.Guid(mUserID)
                cmdSql.Parameters.Add("@UserID", guidSessionID)
                cmdSql.Parameters.Add("@AccountID", Me.mobjUser.AccountId)
                consql.Open()
                da = New SqlClient.SqlDataAdapter(cmdSql)
                da.Fill(dsReturn)
                cmdSql.Dispose()
            End If

            consql.Dispose()

            dsReturn.Tables(0).TableName = "Documents"
            dsReturn.Tables(1).TableName = "Attributes"
            dsReturn.Tables(2).TableName = "Categories"
            dsReturn.Tables(3).TableName = "Workflows"
            dsReturn.Tables(4).TableName = "Queues"
            dsReturn.Tables(5).TableName = "Groups"
            dsReturn.Tables(6).TableName = "IntegrationHeader"
            dsReturn.Tables(7).TableName = "IntegrationDetail"
            dsReturn.Tables(8).TableName = "FolderMappings"
            dsReturn.Tables(9).TableName = "Preferences"

            dsReturn.WriteXml(Server.MapPath("./tmp/") & System.Guid.NewGuid.ToString & ".txt")

            Return dsReturn

        Catch ex As Exception

            Dim conMaster As SqlClient.SqlConnection
            conMaster = Functions.BuildMasterConnection
            Functions.LogException(mobjUser.UserId.ToString, mobjUser.AccountId.ToString, "", "ScanConfig() Error: " & ex.Message, ex.StackTrace, Server.MachineName, "", "", conMaster, True)

        End Try

    End Function

    <WebMethod(EnableSession:=True)> _
    Public Function LoadQueues(ByVal versionId As Integer, ByVal QueueActionId As Integer, ByVal WFRuleAttributeID As Integer, ByVal WFRuleAttributeValue As String) As DataSet

        Dim conCompany As New SqlClient.SqlConnection

        Try

            Dim Request As HttpRequest = Context.Request
            Me.mobjUser = BuildUserObject(Request.Cookies("docAssist"))
            conCompany = Functions.BuildConnection(Me.mobjUser)

            Dim objWf As New Workflow(Functions.BuildConnection(Me.mobjUser))

            Dim dsSubmit As New DataSet
            dsSubmit = objWf.WFReviewerActionsGetID(versionId, Me.mobjUser.ImagesUserId, QueueActionId, WFRuleAttributeID, WFRuleAttributeValue)

            dsSubmit.Tables(0).TableName = "Header"
            dsSubmit.Tables(1).TableName = "QueueDetail"
            dsSubmit.Tables(2).TableName = "Urgent"
            dsSubmit.Tables(3).TableName = "History"

            Return dsSubmit

        Catch ex As Exception

            Dim conMaster As SqlClient.SqlConnection
            conMaster = Functions.BuildMasterConnection
            Functions.LogException(mobjUser.UserId.ToString, mobjUser.AccountId.ToString, "", "LoadQueues() Error: " & ex.Message, ex.StackTrace, Server.MachineName, "", "", conMaster, True)

            Throw ex

        Finally

            If conCompany.State <> ConnectionState.Closed Then
                conCompany.Close()
                conCompany.Dispose()
            End If

        End Try

    End Function

    <WebMethod(EnableSession:=True)> _
    Public Function SaveDocument(ByVal userId As String, ByVal accountId As String, ByVal versionId As Integer, ByVal saveData As String) As DataSet

        Dim conCompany As New SqlClient.SqlConnection
        Dim trnSql As SqlClient.SqlTransaction

        Try

            Dim Request As HttpRequest = Context.Request
            Me.mobjUser = BuildUserObject(Request.Cookies("docAssist"))

            conCompany = Functions.BuildConnection(Me.mobjUser)

            Dim dsVersionControl As Accucentric.docAssist.Data.dsVersionControl
            dsVersionControl = Session(versionId & "VersionControl")

            Dim imageId As Integer = CType(dsVersionControl.Images.Rows(0), Data.dsVersionControl.ImagesRow).ImageID

            Dim indexData As New DataSet("indexData")
            indexData.ReadXml(New StringReader(saveData))

            'Document ID
            dsVersionControl.Images.Rows(0).Item("DocumentID") = indexData.Tables("DocumentData").Rows(0)("DocumentID")

            'Attributes
            Dim dsAttributes As New dsImageAttributes
            Dim drVersionAttribute As Data.dsVersionControl.ImageAttributesRow
            Dim dtAttributes As dsImageAttributes.ImageAttributesDataTable = dsAttributes.ImageAttributes
            Dim drAttributes As dsImageAttributes.ImageAttributesRow
            dtAttributes.PrimaryKey = New DataColumn() {dtAttributes.ImageAttributeIdColumn}

            If Not IsNothing(indexData.Tables("Attributes")) Then
                For Each dr As DataRow In indexData.Tables("Attributes").Rows
                    If dr("ImageAttributeId") = "-1" Then
                        drVersionAttribute = dsVersionControl.ImageAttributes.NewRow() 'oVersionControl.ImageAttributes.NewRow
                        drVersionAttribute.AttributeId = dr("AttributeId")
                        drVersionAttribute.ImageId = imageId
                        drVersionAttribute.AttributeValue = dr("AttributeValue")
                        drVersionAttribute.AttributeName = dr("AttributeName")
                        dsVersionControl.ImageAttributes.Rows.Add(drVersionAttribute)
                    Else
                        Dim row As dsImageAttributes.ImageAttributesRow
                        row = dtAttributes.NewImageAttributesRow
                        row.AttributeId = dr("AttributeId")
                        row.AttributeValue = dr("AttributeValue")
                        row.ImageAttributeId = dr("ImageAttributeId")
                        row.AttributeName = dr("AttributeName")
                        dtAttributes.Rows.Add(row)
                    End If
                Next

                For Each drVersionAttribute In dsVersionControl.ImageAttributes.Rows

                    If drVersionAttribute.ImageAttributeId > -1 Then
                        drAttributes = dtAttributes.Rows.Find(drVersionAttribute.ImageAttributeId)
                        If drAttributes Is Nothing Then
                            'Delete the record if it's been deleted from the grid
                            drVersionAttribute.Delete()
                        Else
                            'Update changed values and ID's
                            If drVersionAttribute.AttributeId <> drAttributes.AttributeId Then drVersionAttribute.AttributeId = drAttributes.AttributeId
                            If drVersionAttribute.AttributeValue <> drAttributes.AttributeValue Then drVersionAttribute.AttributeValue = drAttributes.AttributeValue
                        End If

                    End If

                Next

                ' Add new rows
                Dim dvAttributes As New DataView(dtAttributes, dtAttributes.ImageAttributeIdColumn.ColumnName & "< 0", dtAttributes.ImageAttributeIdColumn.ColumnName, DataViewRowState.CurrentRows)
                Dim dvrAttributes As DataRowView
                For Each dvrAttributes In dvAttributes
                    drAttributes = dvrAttributes.Row
                    drVersionAttribute = dsVersionControl.ImageAttributes.NewRow() 'oVersionControl.ImageAttributes.NewRow
                    drVersionAttribute.AttributeId = drAttributes.AttributeId
                    drVersionAttribute.ImageId = CType(dsVersionControl.Images.Rows(0), Data.dsVersionControl.ImagesRow).ImageID 'CType(oVersionControl.Images.Rows(0), data.Images.ImagesRow).ImageId
                    drVersionAttribute.AttributeValue = drAttributes.AttributeValue
                    dsVersionControl.ImageAttributes.Rows.Add(drVersionAttribute)
                Next
                dvrAttributes = Nothing
                dvAttributes.Dispose()

            Else

                'No attributes passed, clear the table
                dsVersionControl.ImageAttributes.Rows.Clear()

            End If

            Session(versionId & "VersionControl") = dsVersionControl

            'Save transaction
            If conCompany.State <> ConnectionState.Open Then conCompany.Open()
            trnSql = conCompany.BeginTransaction
            Dim objAttachment As New Data.Images.FileAttachments(conCompany, False, trnSql)

            'Folder ID
            Dim folderId As Integer
            If Not IsNothing(indexData.Tables("DocumentData").Rows(0)("FolderID")) Then
                If IsNumeric(indexData.Tables("DocumentData").Rows(0)("FolderID")) Then
                    folderId = indexData.Tables("DocumentData").Rows(0)("FolderID")
                    objAttachment.AttachmentAssignFolder(versionId, folderId, Me.mobjUser.ImagesUserId)
                Else
                    objAttachment.AttachmentAssignFolder(versionId, 0, Me.mobjUser.ImagesUserId)
                End If
            End If

            'Title/desc/tags
            Dim title, description, tags As String
            title = indexData.Tables("DocumentData").Rows(0)("Title")
            description = indexData.Tables("DocumentData").Rows(0)("Description")
            tags = indexData.Tables("DocumentData").Rows(0)("Tags")
            objAttachment.UpdateHeader(versionId, title, description, tags)

            'Workflow

            'if ($("#Workflow").val() != -1){
            '	workflowData = '<WorkflowData>';
            '	workflowData = workflowData + '<WorkflowID>' + $("#Workflow").val() + '</WorkflowID>';	
            '	workflowData = workflowData + '<Mode>' + workflowMode + '</Mode>';	
            '	workflowData = workflowData + '<QueueID>' + $("#ddlQueue").val() + '</QueueID>';	
            '	workflowData = workflowData + '<Urgent>' + urgent + '</Urgent>';	
            '	workflowData = workflowData + '<Notes>' + $("#txtNotes").val() + '</Notes>';	
            '	workflowData = workflowData + '</WorkflowData>';
            '}

            If Not IsNothing(indexData.Tables("WorkflowData")) Then

                Dim wf As New Workflow(conCompany, False, trnSql)
                Dim mode As Functions.WorkflowMode = indexData.Tables("WorkflowData").Rows(0)("Mode")
                Dim workflowId As Integer = indexData.Tables("WorkflowData").Rows(0)("WorkflowID")
                Dim queueId As Integer = indexData.Tables("WorkflowData").Rows(0)("QueueID")
                Dim urgent As Integer = indexData.Tables("WorkflowData").Rows(0)("Urgent")
                Dim notes As String = indexData.Tables("WorkflowData").Rows(0)("Notes")
                Dim currentRouteId As String = indexData.Tables("WorkflowData").Rows(0)("CurrentRouteID")
                Dim status As String = indexData.Tables("WorkflowData").Rows(0)("StatusID")

                Dim result As Integer

                Select Case mode
                    Case Functions.WorkflowMode.AUTHORIZED_SUBMITTER_NOT_SUBMITTED, Functions.WorkflowMode.AUTHORIZED_SUBMITTER_RESUBMIT, Functions.WorkflowMode.AUTHORIZED_SUBMITTER_SUBMITTED
                        'Submit
                        result = wf.WFRouteInsert(versionId, Me.mobjUser.ImagesUserId, workflowId, urgent, queueId, notes)
                    Case Functions.WorkflowMode.ACTION_ACCESS_REVIEWER_PENDING
                        'Review
                        result = wf.WFReview(versionId, currentRouteId, Me.mobjUser.ImagesUserId, urgent, status, queueId, notes)
                End Select

                If result = -1 Then
                    Throw New Exception("Workflow has changed.")
                End If

            End If

            Dim objSave As New Data.ImageSave(Me.mobjUser.ImagesUserId, conCompany, , , , trnSql)

            Dim res As Integer = objSave.UpdateDocument(dsVersionControl, versionId)

            trnSql.Commit()

            'Update name for new attributes
            For Each attributeRow As DataRow In dsVersionControl.ImageAttributes.Rows
                If attributeRow("AttributeName") = "" Then
                    Dim f = 9
                End If
            Next

            'Build HTML that updates the client side
            Dim html As String = Functions.BuildAttributes(New DataView(dsVersionControl.ImageAttributes))

            Dim ds As New DataSet("ReturnData")
            Dim dt As New DataTable("HTML")
            dt.Columns.Add("ResultHTML")
            Dim drSave As DataRow = dt.NewRow
            drSave("ResultHTML") = html
            dt.Rows.Add(drSave)
            ds.Tables.Add(dt)

            Return ds

        Catch ex As Exception

            trnSql.Rollback()

            Dim conMaster As SqlClient.SqlConnection
            conMaster = Functions.BuildMasterConnection
            Functions.LogException(mobjUser.UserId.ToString, mobjUser.AccountId.ToString, "", "SaveDocument() Error: " & ex.Message, ex.StackTrace, Server.MachineName, "", "", conMaster, True)

            Throw ex

        Finally

            If conCompany.State <> ConnectionState.Closed Then
                conCompany.Close()
                conCompany.Dispose()
            End If

        End Try

    End Function

    <WebMethod(EnableSession:=True)> _
    Public Function buildThumbnail(ByVal intVersionId As Integer, ByVal intPage As Integer, ByVal intQuality As Integer, ByVal guidAccountId As String) As DataSet

        Dim strReturn As String
        Dim mconSqlImage As New SqlClient.SqlConnection

        ' Dim dsVersionControl As Data.dsVersionControl = Session(intVersionId & "VersionControl")

        Dim strFileName As String = "Images/NoThumbPreview.jpg"
        Try
            '
            ' Create user objects
            '
            Dim Request As HttpRequest = Context.Request
            Me.mobjUser = BuildUserObject(Request.Cookies("docAssist"))
            Me.mobjUser.AccountId = New System.Guid(guidAccountId)
            mconSqlImage = BuildConnection(Me.mobjUser)

            'Dim objFind(1) As Object
            'Dim intImageDetId As Integer
            'objFind(0) = intVersionId
            'objFind(1) = intPage
            ''dsVersionControl.ImageVersionDetail.PrimaryKey = New DataColumn() {dsVersionControl.ImageVersionDetail.Columns("VersionId"), dsVersionControl.ImageVersionDetail.Columns("SeqId")}

            'Dim drVersionImageDetail As Data.dsVersionControl.ImageVersionDetailRow = dsVersionControl.ImageVersionDetail.Rows.Find(objFind)

            'If drVersionImageDetail Is Nothing Then
            '    Throw New Exception("Unable to locate document thumbnail.")
            'End If

            'intImageDetId = drVersionImageDetail.ImageDetID

            Dim cmdSql As New SqlClient.SqlCommand("SMThumbnailGet")
            cmdSql.Parameters.Add("@VersionId", intVersionId)
            cmdSql.Parameters.Add("@PageNo", intPage)
            cmdSql.CommandType = CommandType.StoredProcedure
            cmdSql.Connection = mconSqlImage
            mconSqlImage.Open()

            Dim da As New SqlDataAdapter(cmdSql)
            Dim dtImage As New DataTable
            da.Fill(dtImage)
            da.Dispose()

            Dim image As Atalasoft.Imaging.AtalaImage

            'Dim ds As New DataSet
            'ds.Tables.Add(dtImage)
            'ds.WriteXml("c:\temp\blob.xml")

            If dtImage.Rows.Count = 0 Then

                Dim f = 99

                'Return strFileName
            Else
                Dim dr As DataRow = dtImage.Rows(0)

                strFileName = "cache/" & System.Guid.NewGuid.ToString & "_tmb.jpg"

                If dr("Thumbnail") Is System.DBNull.Value Then
                    image = Atalasoft.Imaging.AtalaImage.FromByteArray(dr("Image"))
                    image.Resolution = New Atalasoft.Imaging.Dpi(100, 100, image.Resolution.Units)
                    If image.PixelFormat > Atalasoft.Imaging.PixelFormat.Pixel8bppGrayscale Then image = image.GetChangedPixelFormat(Atalasoft.Imaging.PixelFormat.Pixel8bppGrayscale)
                    Dim thumb As New Atalasoft.Imaging.ImageProcessing.Thumbnail(New Size(180, 180))
                    image = thumb.Create(image)
                Else
                    image = Atalasoft.Imaging.AtalaImage.FromByteArray(dr("Thumbnail"))
                End If

                'Dim dynThreshold As New Atalasoft.Imaging.ImageProcessing.Document.DynamicThresholdCommand
                'image = dynThreshold.Apply(image).Image

                Dim enJpeg As New Atalasoft.Imaging.Codec.JpegEncoder
                image.Save(Server.MapPath(strFileName), enJpeg, Nothing)

                Dim dt As New DataTable("Info")
                dt.Columns.Add("filename")
                dt.Columns.Add("height")
                dt.Columns.Add("width")
                Dim dr2 As DataRow
                dr2 = dt.NewRow
                dr2("filename") = "/" & strFileName.Replace("\", "/")
                dr2("height") = image.Height
                dr2("width") = image.Width
                dt.Rows.Add(dr2)

                Dim ds As New DataSet("ThumbInfo")
                ds.Tables.Add(dt)

                Return ds

            End If

        Catch ex As Exception

            Dim conMaster As SqlClient.SqlConnection
            conMaster = Functions.BuildMasterConnection
            Functions.LogException(mobjUser.UserId.ToString, mobjUser.AccountId.ToString, "", "buildThumbnail() Error: " & ex.Message, ex.StackTrace, Server.MachineName, "", "", conMaster, True)

            Throw ex

        Finally

            If Not mconSqlImage Is Nothing Then
                If Not mconSqlImage.State = ConnectionState.Closed Then mconSqlImage.Close()
            End If

        End Try

    End Function

    <WebMethod(EnableSession:=True)> _
    Public Function TrackRotation(ByVal intVersionId As Integer, ByVal intSeqId As Integer, ByVal intRotation As Integer) As Boolean

        Dim bolReturn As Boolean = False
        Dim dsVersionControl As Accucentric.docAssist.Data.dsVersionControl

        Try

            'Load the existing dataset
            If Not Session(intVersionId & "VersionControl") Is Nothing Then
                dsVersionControl = Session(intVersionId & "VersionControl")
            End If

            'Check for the page already existing
            Dim objFind(1) As Object
            objFind(0) = intVersionId
            objFind(1) = intSeqId
            Dim drImageVersionDetail As Data.dsVersionControl.ImageVersionDetailRow = dsVersionControl.ImageVersionDetail.Rows.Find(objFind)

            'Save the image rotation
            If Not drImageVersionDetail Is Nothing Then
                drImageVersionDetail.RotateImage += intRotation
                drImageVersionDetail.Modified = Now()
            End If

            'TODO: Rotate annotations

            'Save the session variable
            Session(intVersionId & "VersionControl") = dsVersionControl

            Return True

        Catch ex As Exception

            Dim conMaster As SqlClient.SqlConnection
            conMaster = Functions.BuildMasterConnection
            Functions.LogException(mobjUser.UserId.ToString, mobjUser.AccountId.ToString, "", "Rotation Error: " & ex.Message, ex.StackTrace, Server.MachineName, "", "", conMaster, True)

            Throw ex

        Finally

        End Try

    End Function

    <WebMethod(EnableSession:=True)> _
    Public Function reorderPages(ByVal versionID As String, ByVal pages As String)

        Try

            Dim dsVersion As Accucentric.docAssist.Data.dsVersionControl = Session(versionID & "VersionControl")

            Dim pageCount As Integer = dsVersion.ImageVersionDetail.Count

            'Clear pages
            For X As Integer = 0 To pageCount - 1
                dsVersion.ImageVersionDetail.Item(X).SeqID = CInt(dsVersion.ImageVersionDetail.Item(X).SeqID * -1)
            Next

            Dim item() As String = pages.Replace(",,", ",").Split(",")
            Dim cleanList As New ArrayList

            'Remove empty items
            For X As Integer = 0 To item.Length - 1
                If item(x) <> "" Then cleanList.Add(item(x))
            Next

            'Reorder pages
            For X As Integer = 0 To cleanList.Count - 1
                dsVersion.ImageVersionDetail.Item(X).SeqID = cleanList(x)
            Next


            'For X As Integer = 0 To pageCount - 1
            '    'dsVersion.ImageVersionDetail.Rows(x).se()
            '    Dim objFind As Object
            '    objFind = x

            '    'Dim intImageDetId As Integer
            '    'objFind = cleanList(x).Split("-")(1)
            '    dsVersion.ImageVersionDetail.PrimaryKey = New DataColumn() {dsVersion.ImageVersionDetail.Columns("SeqID")}
            '    'Dim drVersionImageDetail As Data.dsVersionControl.ImageVersionDetailRow = dsVersion.ImageVersionDetail.Rows.Find(objFind)
            '    ''dsVersion.ImageVersionDetail.Item(X).SeqID = CInt(items(x))
            '    'drVersionImageDetail.SeqID = x
            'Next

            Session(versionID & "VersionControl") = dsVersion

        Catch ex As Exception

        End Try

    End Function

    <WebMethod(EnableSession:=True)> _
    Public Function splitDocument(ByVal versionId As String, ByVal startPage As String, ByVal endPage As String, ByVal inheritAttributes As Boolean) As DataSet

        Dim conCompany As New SqlClient.SqlConnection

        Try

            Dim Request As HttpRequest = Context.Request
            Me.mobjUser = BuildUserObject(Request.Cookies("docAssist"))

            conCompany = Functions.BuildConnection(Me.mobjUser)
            If conCompany.State <> ConnectionState.Open Then conCompany.Open()

            Dim cmdSql As New SqlClient.SqlCommand("SMDocumentSplit", conCompany)
            cmdSql.CommandType = CommandType.StoredProcedure
            cmdSql.Parameters.Add("@UserId", Me.mobjUser.ImagesUserId)
            cmdSql.Parameters.Add("@VersionId", versionId)
            cmdSql.Parameters.Add("@StartSeqID", startPage)
            cmdSql.Parameters.Add("@EndSeqID", endPage)
            cmdSql.Parameters.Add("@InheritAttributes", inheritAttributes)

            Dim vid As New SqlClient.SqlParameter("@NewVersionID", System.Data.SqlDbType.Int)
            vid.Direction = ParameterDirection.Output
            cmdSql.Parameters.Add(vid)

            Dim vidChanged As New SqlClient.SqlParameter("@VersionIDChanged", System.Data.SqlDbType.Int)
            vidChanged.Direction = ParameterDirection.Output
            cmdSql.Parameters.Add(vidChanged)

            cmdSql.ExecuteNonQuery()

            Dim dt As New DataTable("Info")
            dt.Columns.Add("NewVersionID")

            Dim dr2 As DataRow
            dr2 = dt.NewRow
            dr2("NewVersionID") = vid.Value
            dt.Rows.Add(dr2)

            Dim ds As New DataSet("ThumbInfo")
            ds.Tables.Add(dt)

            Return ds

        Catch ex As Exception

            Dim conMaster As SqlClient.SqlConnection
            conMaster = Functions.BuildMasterConnection
            Functions.LogException(mobjUser.UserId.ToString, mobjUser.AccountId.ToString, "", "Split Document: " & ex.Message, ex.StackTrace, Server.MachineName, "", "", conMaster, True)

            Throw ex

        Finally

            If conCompany.State <> ConnectionState.Closed Then
                conCompany.Close()
                conCompany.Dispose()
            End If

        End Try

    End Function

    <WebMethod(EnableSession:=True)> _
    Public Function workflowHistory(ByVal versionId As Integer) As DataSet

        Dim conCompany As New SqlClient.SqlConnection

        Try

            Dim Request As HttpRequest = Context.Request
            Me.mobjUser = BuildUserObject(Request.Cookies("docAssist"))

            conCompany = Functions.BuildConnection(Me.mobjUser)
            If conCompany.State <> ConnectionState.Open Then conCompany.Open()

            Dim objWf As New Workflow(conCompany, False)

            Dim dsHistory As New DataSet("WorkflowHistory")

            dsHistory = objWf.WFRouteDetail(versionId, Me.mobjUser.TimeDiffFromGMT)

            dsHistory.Tables(0).TableName = "Workflow"
            dsHistory.Tables(1).TableName = "Actions"

            'Clean up dates
            'dsHistory.Tables(1).Columns.Add("Date")
            'For Each dr As DataRow In dsHistory.Tables(1).Rows
            '    dr("Date") = CDate(dr("ActionDateTime")).ToShortDateString & " at " & CDate(dr("ActionDateTime")).ToShortTimeString
            'Next

            Return dsHistory

        Catch ex As Exception

            Dim conMaster As SqlClient.SqlConnection
            conMaster = Functions.BuildMasterConnection
            Functions.LogException(mobjUser.UserId.ToString, mobjUser.AccountId.ToString, "", "Load Workflow History: " & ex.Message, ex.StackTrace, Server.MachineName, "", "", conMaster, True)

            Throw ex

        Finally

            If conCompany.State <> ConnectionState.Closed Then
                conCompany.Close()
                conCompany.Dispose()
            End If

        End Try

    End Function

    <WebMethod(EnableSession:=True)> _
    Public Function deletePages(ByVal versionId As String, ByVal startPage As String, ByVal endPage As String) As DataSet

        Dim bolValue As Boolean
        Dim conCompany As New SqlClient.SqlConnection

        Try

            Dim Request As HttpRequest = Context.Request
            Me.mobjUser = BuildUserObject(Request.Cookies("docAssist"))

            conCompany = Functions.BuildConnection(Me.mobjUser)
            If conCompany.State <> ConnectionState.Open Then conCompany.Open()

            'Get the current version
            Dim dsVersion As Accucentric.docAssist.Data.dsVersionControl = Session(versionId & "VersionControl")
            Dim intImageId As Integer = CType(dsVersion.Images.Rows(0), Accucentric.docAssist.Data.dsVersionControl.ImagesRow).ImageID
            '
            'Determine if the entire image will be deleted
            If startPage = 1 And endPage >= dsVersion.ImageVersion.Rows(0).Item("SeqTotal") Then
                DeleteDocument(versionId, conCompany)
                Exit Function
            End If

            ' Delete version from database
            Dim dvImageVersionDetail As New DataView(dsVersion.ImageVersionDetail, Nothing, "SeqId", DataViewRowState.CurrentRows)
            Dim drImageVersionDetail As Accucentric.docAssist.Data.dsVersionControl.ImageVersionDetailRow
            Dim intPageNoPrior As Integer, dvr As DataRowView
            '
            ' Loop through deleting the records
            '
            For Each dvr In dvImageVersionDetail
                drImageVersionDetail = dvr.Row
                If drImageVersionDetail.SeqID < startPage Then
                    intPageNoPrior = drImageVersionDetail.SeqID
                ElseIf drImageVersionDetail.SeqID >= startPage And drImageVersionDetail.SeqID <= endPage Then
                    drImageVersionDetail.SeqID = 0
                    '
                    ' Check for imported image, and determine to delete the import file
                    If drImageVersionDetail.DeleteFile Then
                        ' Check for other pages
                        Dim dv As New DataView(dsVersion.ImageVersionDetail, "ImportFileName='" & drImageVersionDetail.ImportFileName & "'", Nothing, DataViewRowState.CurrentRows)
                        If dv.Count = 1 And System.IO.File.Exists(drImageVersionDetail.ImportFileName) Then
                            ' Delete the file
                            System.IO.File.Delete(drImageVersionDetail.ImportFileName)
                        End If
                    End If
                    drImageVersionDetail.SeqID = 0
                    drImageVersionDetail.Delete()
                Else
                    drImageVersionDetail.SeqID = intPageNoPrior + 1
                    intPageNoPrior = drImageVersionDetail.SeqID
                End If
            Next

            'Get the new page count
            'Dim dvImages As New DataView(dsVersion.ImageVersionDetail, Nothing, Nothing, DataViewRowState.CurrentRows)
            'CType(dsVersion.ImageVersion.Rows(0), Accucentric.docAssist.Data.dsVersionControl.ImageVersionRow).SeqTotal = dvImages.Count

            'Update the database
            Dim objSave As New Accucentric.docAssist.Data.ImageSave(GetImagesUserId(conCompany, Me.mobjUser), conCompany)
            Dim iNewVersionID As Integer = objSave.UpdateImage(dsVersion, , , True)

            If iNewVersionID > 0 Then

                Dim dt As New DataTable("Info")
                dt.Columns.Add("NewVersionID")
                'dt.Columns.Add("PageCount")

                Dim dr2 As DataRow
                dr2 = dt.NewRow
                dr2("NewVersionID") = iNewVersionID
                'dr2("PageCount") = pageCount
                dt.Rows.Add(dr2)

                Dim ds As New DataSet("ThumbInfo")
                ds.Tables.Add(dt)

                Return ds

            End If

        Catch ex As Exception

            Dim conMaster As SqlClient.SqlConnection
            conMaster = Functions.BuildMasterConnection
            Functions.LogException(mobjUser.UserId.ToString, mobjUser.AccountId.ToString, "", "Split Document: " & ex.Message, ex.StackTrace, Server.MachineName, "", "", conMaster, True)

            Throw ex

        Finally

            If conCompany.State <> ConnectionState.Closed Then
                conCompany.Close()
                conCompany.Dispose()
            End If

        End Try


        'Dim conCompany As New SqlClient.SqlConnection

        'Try

        '    Dim Request As HttpRequest = Context.Request
        '    Me.mobjUser = BuildUserObject(Request.Cookies("docAssist"))

        '    conCompany = Functions.BuildConnection(Me.mobjUser)
        '    If conCompany.State <> ConnectionState.Open Then conCompany.Open()

        '    Dim cmdSql As New SqlClient.SqlCommand("SMDocumentSplit", conCompany)
        '    cmdSql.CommandType = CommandType.StoredProcedure
        '    cmdSql.Parameters.Add("@UserId", Me.mobjUser.ImagesUserId)
        '    cmdSql.Parameters.Add("@VersionId", versionId)
        '    cmdSql.Parameters.Add("@StartSeqID", startPage)
        '    cmdSql.Parameters.Add("@EndSeqID", endPage)
        '    cmdSql.Parameters.Add("@InheritAttributes", inheritAttributes)

        '    Dim vid As New SqlClient.SqlParameter("@NewVersionID", System.Data.SqlDbType.Int)
        '    vid.Direction = ParameterDirection.Output
        '    cmdSql.Parameters.Add(vid)

        '    Dim vidChanged As New SqlClient.SqlParameter("@VersionIDChanged", System.Data.SqlDbType.Int)
        '    vidChanged.Direction = ParameterDirection.Output
        '    cmdSql.Parameters.Add(vidChanged)

        '    cmdSql.ExecuteNonQuery()

        '    Dim dt As New DataTable("Info")
        '    dt.Columns.Add("NewVersionID")

        '    Dim dr2 As DataRow
        '    dr2 = dt.NewRow
        '    dr2("NewVersionID") = vid.Value
        '    dt.Rows.Add(dr2)

        '    Dim ds As New DataSet("ThumbInfo")
        '    ds.Tables.Add(dt)

        '    Return ds

        'Catch ex As Exception

        '    Dim conMaster As SqlClient.SqlConnection
        '    conMaster = Functions.BuildMasterConnection
        '    Functions.LogException(mobjUser.UserId.ToString, mobjUser.AccountId.ToString, "", "Split Document: " & ex.Message, ex.StackTrace, Server.MachineName, "", "", conMaster, True)

        '    Throw ex

        'Finally

        '    If conCompany.State <> ConnectionState.Closed Then
        '        conCompany.Close()
        '        conCompany.Dispose()
        '    End If

        'End Try

    End Function

    Private Sub DeleteDocument(ByVal versionId As Integer, ByVal conCompany As SqlClient.SqlConnection)

        Try

            Dim cmdSql As SqlClient.SqlCommand
            '
            ' Get the image id
            Dim intImageId As Integer
            cmdSql = New SqlClient.SqlCommand("SELECT ImageId FROM ImageVersion WHERE VersionId = @VersionId", conCompany)
            cmdSql.Parameters.Add("@VersionId", versionId)
            intImageId = cmdSql.ExecuteScalar()

            ' Delete the image
            cmdSql = New SqlClient.SqlCommand("acsImageDelete", conCompany)
            cmdSql.CommandType = CommandType.StoredProcedure
            cmdSql.Parameters.Add("@ImageId", intImageId)
            cmdSql.Parameters.Add("@UserId", Me.mobjUser.ImagesUserId)
            cmdSql.ExecuteNonQuery()

            'Update cached results
            Dim ds As DataSet = Session("DataSource")
            ds.Tables(0).Rows.Remove(ds.Tables(0).Select("ImageID = " & intImageId)(0))
            Session("DataSource") = ds
            Session("ResultCount") = CInt(Session("ResultCount")) - 1

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    <WebMethod(EnableSession:=True)> _
    Public Function buildMoveThumbnail(ByVal versionId As Integer, ByVal page As Integer) As DataSet

        Dim strReturn As String
        Dim mconSqlImage As New SqlClient.SqlConnection

        Const MAX_HEIGHT As Integer = 90
        Const MAX_WIDTH As Integer = 81

        Dim image As Atalasoft.Imaging.AtalaImage

        Dim dsVersionControl As Data.dsVersionControl = Session(versionId & "VersionControl")
        Dim strFileName As String = "Images/NoThumbPreview.jpg"

        Try
            '
            ' Create user objects
            '
            Dim Request As HttpRequest = Context.Request
            Me.mobjUser = BuildUserObject(Request.Cookies("docAssist"))
            mconSqlImage = BuildConnection(Me.mobjUser)

            Dim objFind(1) As Object
            Dim intImageDetId As Integer
            objFind(0) = versionId
            objFind(1) = page
            dsVersionControl.ImageVersionDetail.PrimaryKey = New DataColumn() {dsVersionControl.ImageVersionDetail.Columns("VersionId"), dsVersionControl.ImageVersionDetail.Columns("SeqId")}

            Dim drVersionImageDetail As Data.dsVersionControl.ImageVersionDetailRow = dsVersionControl.ImageVersionDetail.Rows.Find(objFind)

            If drVersionImageDetail Is Nothing Then
                Throw New Exception("Unable to locate document thumbnail.")
            End If

            intImageDetId = drVersionImageDetail.ImageDetID

            Dim cmdSql As New SqlClient.SqlCommand("SELECT Image FROM ImageDetail WHERE ImageDetID = @ImageDetID")
            cmdSql.Parameters.Add("@ImageDetID", intImageDetId)
            cmdSql.CommandType = CommandType.Text
            cmdSql.Connection = mconSqlImage
            mconSqlImage.Open()

            Dim imageBytes() As Byte = cmdSql.ExecuteScalar

            strFileName = "cache/" & System.Guid.NewGuid.ToString & "_movetmb.jpg"

            image = AtalaImage.FromByteArray(imageBytes)
            image.Resolution = New Atalasoft.Imaging.Dpi(100, 100, image.Resolution.Units)
            If image.PixelFormat > Atalasoft.Imaging.PixelFormat.Pixel8bppGrayscale Then image = image.GetChangedPixelFormat(Atalasoft.Imaging.PixelFormat.Pixel8bppGrayscale)
            Dim thumb As New Atalasoft.Imaging.ImageProcessing.Thumbnail(New Size(90, 90))
            image = thumb.Create(image)

            Dim enJpeg As New Atalasoft.Imaging.Codec.JpegEncoder
            image.Save(Server.MapPath(strFileName), enJpeg, Nothing)

            Dim dt As New DataTable("ThumbInfo")
            dt.Columns.Add("Filename")
            dt.Columns.Add("ImageDetId")

            Dim dr2 As DataRow
            dr2 = dt.NewRow
            dr2("Filename") = "/" & strFileName.Replace("\", "/")
            dr2("ImageDetId") = drVersionImageDetail.SeqID & "-" & drVersionImageDetail.ImageDetID
            dt.Rows.Add(dr2)

            Dim ds As New DataSet("Thumb")
            ds.Tables.Add(dt)

            Return ds

        Catch ex As Exception

            'Dim conMaster As SqlClient.SqlConnection
            'conMaster = Functions.BuildMasterConnection
            'Functions.LogException(mobjUser.UserId.ToString, mobjUser.AccountId.ToString, "", "BuildMoveThumbnail() Error: " & ex.Message, ex.StackTrace, Server.MachineName, "", "", conMaster, True)

        Finally

            image.Dispose()

            If Not mconSqlImage Is Nothing Then
                If Not mconSqlImage.State = ConnectionState.Closed Then mconSqlImage.Close()
            End If

        End Try

    End Function

    <WebMethod(EnableSession:=True)> _
    Public Function BuildAtalaThumbnail(ByVal intVersionId As Integer, ByVal intPage As Integer, ByVal intQuality As Integer, ByVal guidAccountId As String) As String()

        Dim strReturn As String
        Dim mconSqlImage As New SqlClient.SqlConnection

        ' Dim dsVersionControl As Data.dsVersionControl = Session(intVersionId & "VersionControl")

        Dim strFileName As String = "Images/NoThumbPreview.jpg"
        Try
            '
            ' Create user objects
            '
            Dim Request As HttpRequest = Context.Request
            Me.mobjUser = BuildUserObject(Request.Cookies("docAssist"))
            Me.mobjUser.AccountId = New System.Guid(guidAccountId)
            mconSqlImage = BuildConnection(Me.mobjUser)

            'Dim objFind(1) As Object
            'Dim intImageDetId As Integer
            'objFind(0) = intVersionId
            'objFind(1) = intPage
            ''dsVersionControl.ImageVersionDetail.PrimaryKey = New DataColumn() {dsVersionControl.ImageVersionDetail.Columns("VersionId"), dsVersionControl.ImageVersionDetail.Columns("SeqId")}

            'Dim drVersionImageDetail As Data.dsVersionControl.ImageVersionDetailRow = dsVersionControl.ImageVersionDetail.Rows.Find(objFind)

            'If drVersionImageDetail Is Nothing Then
            '    Throw New Exception("Unable to locate document thumbnail.")
            'End If

            'intImageDetId = drVersionImageDetail.ImageDetID

            Dim cmdSql As New SqlClient.SqlCommand("SMThumbnailGet")
            cmdSql.Parameters.Add("@VersionId", intVersionId)
            cmdSql.Parameters.Add("@PageNo", intPage)
            cmdSql.CommandType = CommandType.StoredProcedure
            cmdSql.Connection = mconSqlImage
            mconSqlImage.Open()

            Dim da As New SqlDataAdapter(cmdSql)
            Dim dtImage As New DataTable
            da.Fill(dtImage)
            da.Dispose()

            Dim image As Atalasoft.Imaging.AtalaImage

            'Dim ds As New DataSet
            'ds.Tables.Add(dtImage)
            'ds.WriteXml("c:\temp\blob.xml")

            If dtImage.Rows.Count = 0 Then
                'Return strFileName
            Else
                Dim dr As DataRow = dtImage.Rows(0)

                strFileName = "cache/" & System.Guid.NewGuid.ToString & "_tmb.jpg"

                If dr("Thumbnail") Is System.DBNull.Value Then
                    image = Atalasoft.Imaging.AtalaImage.FromByteArray(dr("Image"))
                    image.Resolution = New Atalasoft.Imaging.Dpi(100, 100, image.Resolution.Units)
                    If image.PixelFormat > Atalasoft.Imaging.PixelFormat.Pixel8bppGrayscale Then image = image.GetChangedPixelFormat(Atalasoft.Imaging.PixelFormat.Pixel8bppGrayscale)
                    Dim thumb As New Atalasoft.Imaging.ImageProcessing.Thumbnail(New Size(180, 180))
                    image = thumb.Create(image)
                Else
                    image = Atalasoft.Imaging.AtalaImage.FromByteArray(dr("Thumbnail"))
                End If

                'Dim dynThreshold As New Atalasoft.Imaging.ImageProcessing.Document.DynamicThresholdCommand
                'image = dynThreshold.Apply(image).Image

                Dim enJpeg As New Atalasoft.Imaging.Codec.JpegEncoder
                image.Save(Server.MapPath(strFileName), enJpeg, Nothing)

            End If

            Dim returns() As String = " , , ,".Split(",")
            returns(0) = "/" & strFileName.Replace("\", "/")
            returns(1) = intPage
            returns(2) = image.Height
            returns(3) = image.Width

            Return returns

        Catch ex As Exception

            'Dim conMaster As SqlClient.SqlConnection
            'conMaster = Functions.BuildMasterConnection
            'Functions.LogException(mobjUser.UserId.ToString, mobjUser.AccountId.ToString, "", "BuildAtalaThumb() Error: " & ex.Message, ex.StackTrace, Server.MachineName, "", "", conMaster, True)

        Finally

            If Not mconSqlImage Is Nothing Then
                If Not mconSqlImage.State = ConnectionState.Closed Then mconSqlImage.Close()
            End If

        End Try

    End Function

    <WebMethod(EnableSession:=True)> _
    Public Function copyToWebClipboard(ByVal AttributeData As String) As Boolean

        Try

            Dim clip As New DataSet("ClipBoardData")
            clip.ReadXml(New StringReader(AttributeData))

            Session("ClipboardData") = clip

            Return True

        Catch ex As Exception

        Finally

        End Try

    End Function

    <WebMethod(EnableSession:=True)> _
    Public Function getWebClipboard() As DataSet

        Try

            Return Session("ClipboardData")

        Catch ex As Exception

        Finally

        End Try

    End Function

    <WebMethod(EnableSession:=True)> _
    Public Function movePages(ByVal versionID As Integer, ByVal imageDetIdList As String) As Boolean

        Try

            Dim Request As HttpRequest = Context.Request
            Me.mobjUser = BuildUserObject(Request.Cookies("docAssist"))

            Dim dsVersion As Accucentric.docAssist.Data.dsVersionControl = Session(versionID & "VersionControl")

            Dim pageCount As Integer = dsVersion.ImageVersionDetail.Count

            'Clear pages
            For X As Integer = 0 To pageCount - 1
                dsVersion.ImageVersionDetail.Item(X).SeqID = CInt(dsVersion.ImageVersionDetail.Item(X).SeqID * -1)
            Next

            Dim items() As String = imageDetIdList.Split(",")

            'Reorder pages
            For X As Integer = 0 To pageCount - 1
                Dim objFind As Object
                Dim intImageDetId As Integer
                objFind = items(x).replace("moveimg", "")
                dsVersion.ImageVersionDetail.PrimaryKey = New DataColumn() {dsVersion.ImageVersionDetail.Columns("ImageDetId")}
                Dim drVersionImageDetail As Data.dsVersionControl.ImageVersionDetailRow = dsVersion.ImageVersionDetail.Rows.Find(objFind)
                drVersionImageDetail.SeqID = x + 1
            Next

            Return True

        Catch ex As Exception

            Dim conMaster As SqlClient.SqlConnection
            conMaster = Functions.BuildMasterConnection
            Functions.LogException(mobjUser.UserId.ToString, mobjUser.AccountId.ToString, "", "Move Pages Error: " & ex.Message, ex.StackTrace, Server.MachineName, "", "", conMaster, True)

            Throw ex

        End Try

    End Function

    <WebMethod(EnableSession:=True)> _
    Public Function pollAddPage(ByVal versionID As Integer) As DataSet

        Dim sqlCon As New SqlClient.SqlConnection

        Try

            Dim Request As HttpRequest = Context.Request
            Me.mobjUser = BuildUserObject(Request.Cookies("docAssist"))

            sqlCon = Functions.BuildConnection(Me.mobjUser)
            If sqlCon.State <> ConnectionState.Open Then sqlCon.Open()

            Dim cmdSql As New SqlCommand("SMVersionChangePoll", sqlCon)
            cmdSql.CommandType = CommandType.StoredProcedure
            cmdSql.Parameters.Add("@VersionID", versionID)

            Dim output As New SqlParameter("@CurrentVersionID", Nothing)
            output.Direction = ParameterDirection.Output
            cmdSql.Parameters.Add(output)
            cmdSql.ExecuteNonQuery()

            Dim result As Integer = output.Value

            cmdSql.Dispose()

            Dim dt As New DataTable("DocInfo")
            dt.Columns.Add("VersionId")
            Dim dr As DataRow
            dr = dt.NewRow
            dr("VersionId") = result
            dt.Rows.Add(dr)

            Dim ds As New DataSet("Wrapper")
            ds.Tables.Add(dt)

            Return ds

        Catch ex As Exception

            Dim conMaster As SqlClient.SqlConnection
            conMaster = Functions.BuildMasterConnection
            Functions.LogException(mobjUser.UserId.ToString, mobjUser.AccountId.ToString, "", "Add pages poll: " & ex.Message, ex.StackTrace, Server.MachineName, "", "", conMaster, True)

            Throw ex

        Finally

            If sqlCon.State <> ConnectionState.Closed Then
                sqlCon.Close()
                sqlCon.Dispose()
            End If

        End Try

    End Function

    <WebMethod(EnableSession:=True)> _
    Public Function encrypt(ByVal value As String) As DataSet

        Try

            Dim dt As New DataTable("Encrypted")
            dt.Columns.Add("Result")
            Dim dr As DataRow
            dr = dt.NewRow
            dr("Result") = Encryption.Encryption.Encrypt(value)
            dt.Rows.Add(dr)

            Dim ds As New DataSet("Wrapper")
            ds.Tables.Add(dt)

            Return ds

        Catch ex As Exception

            Dim conMaster As SqlClient.SqlConnection
            conMaster = Functions.BuildMasterConnection
            Functions.LogException(mobjUser.UserId.ToString, mobjUser.AccountId.ToString, "", "encrypt() Web Service: " & ex.Message, ex.StackTrace, Server.MachineName, "", "", conMaster, True)

            Throw ex

        End Try

    End Function

    <WebMethod(EnableSession:=True)> _
    Public Function getEncryptedURL(ByVal versionId As String, ByVal imageID As String, ByVal AddPageNo As String) As DataSet

        Dim sqlConMaster As New SqlClient.SqlConnection

        Try

            Dim Request As HttpRequest = Context.Request
            Me.mobjUser = BuildUserObject(Request.Cookies("docAssist"))

            sqlConMaster = Functions.BuildMasterConnection()

            Dim cmdSqlWebSvc As New SqlClient.SqlCommand("acsOptionValuebyKey", sqlConMaster)
            cmdSqlWebSvc.Parameters.Add("@OptionKey", "CLICKONCESCANURL")
            cmdSqlWebSvc.CommandType = CommandType.StoredProcedure
            Dim scanURL As String = cmdSqlWebSvc.ExecuteScalar
            cmdSqlWebSvc.Parameters.Clear()
            cmdSqlWebSvc.Parameters.Add("@OptionKey", "CLICKONCEMTOMURL")
            Dim mtomURL As String = cmdSqlWebSvc.ExecuteScalar
            cmdSqlWebSvc.Parameters.Clear()
            cmdSqlWebSvc.Parameters.Add("@OptionKey", "CLICKONCESAVEURL")
            Dim saveURL As String = cmdSqlWebSvc.ExecuteScalar

            Dim sb As New System.Text.StringBuilder
            sb.Append("<Param><AccountID>" & Me.mobjUser.AccountId.ToString & "</AccountID>")
            sb.Append("<AccountDesc>" & Me.mobjUser.AccountDesc.ToString & "</AccountDesc>")
            sb.Append("<UserID>" & Me.mobjUser.UserId.ToString & "</UserID>")
            sb.Append("<LoginName>" & Me.mobjUser.EMailAddress.ToString & "</LoginName>")
            sb.Append("<ScanWS>" & mtomURL & "</ScanWS>")
            sb.Append("<SaveWS>" & saveURL & "</SaveWS>")
            sb.Append("<Quality>0</Quality>")
            sb.Append("<Date>" & Now.Date.ToString & "</Date>")
            sb.Append("<VersionID>" & versionId & "</VersionID>")
            sb.Append("<ImageID>" & imageID & "</ImageID>")
            sb.Append("<AddPageNo>" & AddPageNo & "</AddPageNo></Param>")

            Dim dt As New DataTable("Encrypted")
            dt.Columns.Add("Result")
            Dim dr As DataRow
            dr = dt.NewRow
            dr("Result") = scanURL & "?Param=" & HttpUtility.UrlEncode(Encryption.Encryption.Encrypt(sb.ToString))
            dt.Rows.Add(dr)

            Dim ds As New DataSet("Wrapper")
            ds.Tables.Add(dt)

            Return ds

        Catch ex As Exception

            Dim conMaster As SqlClient.SqlConnection
            conMaster = Functions.BuildMasterConnection
            Functions.LogException(mobjUser.UserId.ToString, mobjUser.AccountId.ToString, "", "encrypt() Web Service: " & ex.Message, ex.StackTrace, Server.MachineName, "", "", conMaster, True)

            Throw ex

        Finally

            If sqlConMaster.State <> ConnectionState.Closed Then
                sqlConMaster.Close()
                sqlConMaster.Dispose()
            End If

        End Try

    End Function

    <WebMethod(EnableSession:=True)> _
    Public Function ftGetWordByPoint(ByVal ImageDetID As Integer, ByVal X As String, ByVal Y As String) As DataSet

        Dim sqlCon As New SqlClient.SqlConnection

        Try

            Dim Request As HttpRequest = Context.Request
            Me.mobjUser = BuildUserObject(Request.Cookies("docAssist"))

            sqlCon = Functions.BuildFullTextConnection(Me.mobjUser)
            If sqlCon.State <> ConnectionState.Open Then sqlCon.Open()

            Dim cmdSql As New SqlCommand("SMGetWordByPoint", sqlCon)
            cmdSql.CommandType = CommandType.StoredProcedure
            cmdSql.Parameters.Add("@ImageDetID", ImageDetID)
            cmdSql.Parameters.Add("@X", System.Math.Round(CDbl(X)))
            cmdSql.Parameters.Add("@Y", System.Math.Round(CDbl(Y)))

            Dim res As New DataTable
            Dim da As New SqlClient.SqlDataAdapter(cmdSql)
            da.Fill(res)

            'Dim val As String
            res.TableName = "Magic"

            'If res.Rows.Count > 0 Then
            '    val = res.Rows(0)("Word")
            'Else
            '    val = ""
            'End If

            'cmdSql.Dispose()

            'Dim dt As New DataTable("Magic")
            'dt.Columns.Add("Result")
            'Dim dr As DataRow
            'dr = dt.NewRow
            'dr("Result") = val
            'dt.Rows.Add(dr)

            Dim ds As New DataSet("Wrapper")
            ds.Tables.Add(res)

            Return ds

        Catch ex As Exception

            Dim conMaster As SqlClient.SqlConnection
            conMaster = Functions.BuildMasterConnection
            Functions.LogException(mobjUser.UserId.ToString, mobjUser.AccountId.ToString, "", "ftGetWordByPoint Web Service: " & ex.Message, ex.StackTrace, Server.MachineName, "", "", conMaster, True)

            Throw ex

        Finally

            If sqlCon.State <> ConnectionState.Closed Then
                sqlCon.Close()
                sqlCon.Dispose()
            End If

        End Try

    End Function

    <WebMethod(EnableSession:=True)> _
    Public Function checkWorkflow(ByVal versionId As Integer, ByVal documentId As Integer) As DataSet

        'ByVal intUserId As Integer, ByVal intVersionId As Integer, ByVal objUser As Accucentric.docAssist.Web.Security.User, Optional ByRef intRouteId As Integer = 0, _
        'Optional ByRef DocumentId As Integer = -1

        'Dim sqlCon As New SqlClient.SqlConnection

        Try

            Dim Request As HttpRequest = Context.Request
            Me.mobjUser = BuildUserObject(Request.Cookies("docAssist"))

            Dim res As Integer = Functions.CheckWorkflow(Me.mobjUser.ImagesUserId, versionId, Me.mobjUser, 0, documentId)

            'sqlCon = Functions.BuildFullTextConnection(Me.mobjUser)
            'If sqlCon.State <> ConnectionState.Open Then sqlCon.Open()

            'Dim cmdSql As New SqlCommand("SMGetWordByPoint", sqlCon)
            'cmdSql.CommandType = CommandType.StoredProcedure
            'cmdSql.Parameters.Add("@ImageDetID", ImageDetID)
            'cmdSql.Parameters.Add("@X", System.Math.Round(CDbl(X)))
            'cmdSql.Parameters.Add("@Y", System.Math.Round(CDbl(Y)))

            Dim dt As New DataTable
            dt.Columns.Add("WFMode")
            Dim dr As DataRow
            dr = dt.NewRow
            dr("WFMode") = res
            dt.Rows.Add(dr)

            Dim ds As New DataSet("Result")
            ds.Tables.Add(dt)

            Return ds

        Catch ex As Exception

            Dim conMaster As SqlClient.SqlConnection
            conMaster = Functions.BuildMasterConnection
            Functions.LogException(mobjUser.UserId.ToString, mobjUser.AccountId.ToString, "", "checkWorkflow Web Service: " & ex.Message, ex.StackTrace, Server.MachineName, "", "", conMaster, True)

            Throw ex

        Finally

            'If sqlCon.State <> ConnectionState.Closed Then
            '    sqlCon.Close()
            '    sqlCon.Dispose()
            'End If

        End Try

    End Function

End Class
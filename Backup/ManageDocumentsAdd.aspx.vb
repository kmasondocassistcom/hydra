Partial Class ManageDocumentsAdd
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private mobjUser As Accucentric.docAssist.Web.Security.User
    Private mconSqlImage As SqlClient.SqlConnection

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        txtDocumentName.Attributes("onkeypress") = "catchSubmit();"

        If Not Page.IsPostBack Then
            txtDocumentName.MaxLength = 40
            'PopulateFolderList()
            'PopulateQualityList()
        End If

    End Sub

    Private Function DocumentExists(ByVal strDocumentName As String) As Boolean
        Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
        Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))

        Me.mobjUser.AccountId = guidAccountId
        Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
        Me.mconSqlImage.Open()

        Dim cmdSql As SqlClient.SqlCommand

        Dim dtsql As New DataTable
        cmdSql = New SqlClient.SqlCommand("acsAdminDocumentCheck", Me.mconSqlImage)
        cmdSql.CommandType = CommandType.StoredProcedure
        cmdSql.Parameters.Add("@DocumentName", strDocumentName)
        Dim dasql As New SqlClient.SqlDataAdapter(cmdSql)
        dasql.Fill(dtsql)


        If dtsql.Rows.Count > 0 Then
            Return True
        Else
            Return False

        End If
    End Function

    'Private Sub PopulateFolderList()
    '    Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
    '    Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))

    '    Me.mobjUser.AccountId = guidAccountId
    '    Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
    '    Me.mconSqlImage.Open()

    '    Dim cmdSql As SqlClient.SqlCommand

    '    Dim dtsql As New DataTable
    '    cmdSql = New SqlClient.SqlCommand("acsAdminFolderList", Me.mconSqlImage)
    '    cmdSql.CommandType = CommandType.StoredProcedure
    '    Dim dasql As New SqlClient.SqlDataAdapter(cmdSql)
    '    dasql.Fill(dtsql)

    '    'If dtsql.Rows.Count <= 0 Then
    '    '    'TODO: No Cabinets to change
    '    'Else
    '    '    lstFolders.DataTextField = "FolderName"
    '    '    lstFolders.DataValueField = "FolderID"
    '    '    lstFolders.DataSource = dtsql
    '    '    lstFolders.DataBind()

    '    'End If

    'End Sub

    Private Sub lnkFinish_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If DocumentExists(txtDocumentName.Text) Then
            lblError.Text = "Document " & txtDocumentName.Text & " already exists."
            Exit Sub
        End If


        lblError.Text = ""
        Response.Cookies("Admin")("DocumentName") = txtDocumentName.Text
        Response.Cookies("Admin")("VersionMode") = ddlVersion.SelectedValue
        Response.Redirect("ManageDocumentsAttributes.aspx")

    End Sub

    'Private Function FoldersSelected() As DataTable
    '    Dim dtFolders As New DataTable
    '    Dim dcID As New DataColumn("FolderID", GetType(System.Int64), Nothing)
    '    dtFolders.Columns.Add(dcID)


    '    Dim iCount As Integer = lstFolders.Items.Count
    '    Dim x


    '    For x = 0 To iCount - 1
    '        If lstFolders.Items(x).Selected = True Then
    '            Dim dr As DataRow
    '            dr = dtFolders.NewRow
    '            dr("FolderID") = lstFolders.Items(x).Value
    '            dtFolders.Rows.Add(dr)
    '        End If

    '    Next

    '    Return dtFolders

    'End Function

    Private Sub btnNext_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnNext.Click
        If DocumentExists(txtDocumentName.Text) Then
            lblError.Text = "Document " & txtDocumentName.Text & " already exists."
            Exit Sub
        End If

        'Session("FoldersSelected") = FoldersSelected()
        Session("VersionMode") = ddlVersion.SelectedValue


        lblError.Text = ""
        Session("mAdminDocumentName") = txtDocumentName.Text
        'Session("mAdminFolderID") = lstFolders.SelectedValue
        'Session("mAdminDocumentQuality") = cmbQuality.SelectedValue
        Response.Redirect("ManageDocumentsAttributes.aspx")

    End Sub

    Private Sub LinkButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub imgCancel_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgCancel.Click
        Response.Redirect("ManageDocuments.aspx")

    End Sub
    'Private Sub PopulateQualityList()
    '    Dim lstItem As New ListItem
    '    lstItem.Text = "Normal -Black and White (1.0 Document Units)"
    '    lstItem.Value = 0
    '    cmbQuality.Items.Add(lstItem)
    '    '
    '    '
    '    lstItem = New ListItem
    '    lstItem.Text = "High - Greyscale (1.5 Document Units)"
    '    lstItem.Value = 1
    '    cmbQuality.Items.Add(lstItem)
    '    '
    '    '
    '    lstItem = New ListItem
    '    lstItem.Text = "Best - Color (2.0 Document Units)"
    '    lstItem.Value = 2
    '    cmbQuality.Items.Add(lstItem)

    'End Sub

End Class

Imports Accucentric.docAssist.Data.Images
Imports System.Data.SqlClient
Imports dtSearch.Engine
Imports ComponentArt.Web.UI

Partial Class Search
    'Inherits ViewState.PersistViewStateToFileSystem
    Inherits System.Web.UI.Page

    Private Enum ResultTables
        HEADER = 0
        ATTRIBUTES = 1
        DISTINCT = 2
        SORT_VALUES = 3
        SORTED_LIST = 4
    End Enum

    Private Enum BrowseType
        RECYCLE_BIN = -3
    End Enum

#Region "Declarations"

    Private mobjUser As Accucentric.docAssist.Web.Security.User
    Private mconSqlWeb As SqlClient.SqlConnection
    Private mconSqlImage As New SqlClient.SqlConnection


    Private contextMenus As New ArrayList

    Private blnUpdateAttributesGrid As Boolean = False
    Private blnUpdateTree As Boolean = False
    Private blnUpdateNodeId As Boolean = False
    Private blnUpdateAdvancedPanel As Boolean = False
    Private blnUpdateSmartFolderPanel As Boolean = False
    Private blnUpdateResultsGrid As Boolean = False
    Private blnFolderBrowseMode As Boolean = False
    Private blnGridUpdate As Boolean = False
    Private blnExpandFolder As Boolean = False
    Private blnHideRelevance As Boolean = False
    Private blnContext As Boolean = False

    Private intMaxAttributeCount As Integer

    Private ds As New DataSet

    Private firstDocumentNodeValue As String

    Private Beginning As Integer
    Private Ending As Integer
    'Dim cellId As Integer = 1

    Private strNoResults = "<b><P style=""FONT-SIZE: 10pt; FONT-FAMILY: Trebuchet MS; COLOR: black"">Your search did not match any documents.<br><br></b>&nbsp&nbspSuggestions:<br>- Make sure all attributes and words are spelled correctly.<br>- Try different attributes or keywords.<br>- Try more general keywords."

    'Private blnHasMassItems As Boolean = False

    Private mcookieSearch As cookieSearch

    'Protected WithEvents btnNewShare As System.Web.UI.WebControls.ImageButton
    'Protected WithEvents btnShareNewFolder As System.Web.UI.WebControls.ImageButton
    'Protected WithEvents btnRemoveFolder As System.Web.UI.WebControls.ImageButton
    'Protected WithEvents btnFolderSecurity As System.Web.UI.WebControls.ImageButton
    'Protected WithEvents btnAddFavorite As System.Web.UI.WebControls.ImageButton
    'Protected WithEvents Rename As System.Web.UI.HtmlControls.HtmlAnchor
    'Protected WithEvents imgRenameShare As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ddlFullText As System.Web.UI.WebControls.DropDownList

#End Region

#Region "Properties"

    Protected WithEvents btnAddFiles As System.Web.UI.WebControls.ImageButton
    Protected WithEvents BackLink As System.Web.UI.HtmlControls.HtmlAnchor
    Protected WithEvents lblSearchCriteria As System.Web.UI.WebControls.Label
    Protected WithEvents SortCell As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents btnAllDocs As System.Web.UI.WebControls.ImageButton
    Protected WithEvents ddlMass As System.Web.UI.WebControls.DropDownList
    Protected WithEvents btnMoveUnselect As System.Web.UI.WebControls.ImageButton
    Protected WithEvents btnEmpty As System.Web.UI.WebControls.ImageButton
    Protected WithEvents quickMenu As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents Tr1 As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents Tr2 As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents Tr3 As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents Tr4 As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents Tr5 As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents Tr6 As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents Tr7 As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents Tr8 As System.Web.UI.HtmlControls.HtmlTableRow


    Private Property SearchSortDirection() As String
        Get
            Return Session("SearchSortDirection")
        End Get
        Set(ByVal Value As String)
            Session("SearchSortDirection") = Value
        End Set
    End Property

#End Region

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Me.mcookieSearch = New cookieSearch(Me)

        Try

            Dim strHostname As String = Request.Url.AbsoluteUri.Substring(0, Request.Url.AbsoluteUri.Length - Request.Url.AbsolutePath.Length - Request.Url.Query.Length)
#If DEBUG Then
            strHostname += "/docAssistWeb1"
#End If
            System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, "key", "hostname = '" & strHostname & "';", True)

            If Not Page.IsPostBack Then

                'Clear full text search
                Me.mcookieSearch.Keywords = ""

                'Client side scripting.
                dgSearchAttributes.Attributes("onkeypress") = "submitForm();"
                txtQuickSearch.Attributes("onkeypress") = "submitQuickForm();"
                lnkSearch.Attributes("onclick") = "disable();"
                btnQuickSearch.Attributes("onclick") = "quickSearch();"
                lblError.Attributes("style") = "VISIBILITY: hidden"
                treeSearch.Attributes("onkeypress") = "catchEsc();"
                btnMassMove.Attributes("onclick") = "startProgressDisplay();"
                btnMassDownload.Attributes("onclick") = "hideDownloadOptions();"
                chkMassActions.Attributes.Add("onclick", "toggleResultItems();")

            End If

        Catch ex As Exception

        End Try

        'RegisterExcludeControl(btnMassDownload)

        Try

            'Check for timed out page
            Functions.CheckTimeout(Me.Page)

            Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
            Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))
            Me.mobjUser.AccountId = guidAccountId
            Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)

            System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, "key", "accountId = '" & guidAccountId.ToString & "';", True)

            'TODO
            'If Not Me.mobjUser.SysAdmin Then ReturnScripts.Add("disableCabinet();")

            If Functions.ModuleSetting(Functions.ModuleCode.VIEWER_V2, Me.mobjUser) Then
                System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.NewGuid.ToString, "newViewer = true;", True)
            End If

            If Me.mobjUser.EditionID = AccountEdition.SMALL_BUSINESS_EDITION Then

                System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.NewGuid.ToString, "smallBus = true;", True)

                shareFolder.Visible = True

                Dim accountStatus As AccountState = Functions.GetAccountStatus(Me.mobjUser.AccountId.ToString)
                Select Case accountStatus
                    Case Constants.AccountState.TRIAL_ACTIVE

                        'Check for user count
                        Dim dsUsers As New DataSet
                        Dim cmdSql As New SqlClient.SqlCommand
                        Dim da As New SqlClient.SqlDataAdapter(cmdSql)
                        cmdSql = New SqlClient.SqlCommand("acsUserListbyAccountID", mconSqlImage)
                        cmdSql.CommandType = CommandType.StoredProcedure
                        cmdSql.CommandTimeout = DEFAULT_COMMAND_TIMEOUT
                        cmdSql.Parameters.Add("@AccountID", Me.mobjUser.AccountId)
                        da = New SqlClient.SqlDataAdapter(cmdSql)
                        If Not mconSqlImage.State = ConnectionState.Open Then mconSqlImage.Open()
                        da.Fill(dsUsers)

                        If dsUsers.Tables(0).Rows.Count > 2 Then
                            trUsers.Attributes("onclick") = "window.location.href='" & ConfigurationSettings.AppSettings("UpgradeURL") & "?ref=addUsers&id=" & Me.mobjUser.AccountId.ToString & "';return false;"
                        End If

                    Case Constants.AccountState.DISABLED
                        'Shouldn't ever be here.
                    Case Constants.AccountState.TRIAL_EXPIRED
                        'Shouldn't ever be here.
                End Select

            Else

                shareFolder.Visible = False

            End If

            Dim dtAccess As New DataTable
            dtAccess = Functions.UserAccess(Functions.BuildConnection(mobjUser), mobjUser.UserId, mobjUser.AccountId, False)

            If dtAccess.Rows(0)("Search") = False Then
                SignOut(Me)
            End If


            'ref=addpage&loadversion
            If Not IsNothing(Request.QueryString("loadversion")) And Not Page.IsPostBack Then
                If IsNumeric(Request.QueryString("loadversion")) Then System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.NewGuid.ToString, "LoadImage(" & Request.QueryString("loadversion") & ");", True)
            End If

            'Hide admin section to non-admin users
            If Not (mobjUser.SecAdmin = True Or mobjUser.SysAdmin = True) Then
                trUsers.Visible = False
                trGroups.Visible = False
            End If

            If mobjUser.EditionID = AccountEdition.SMALL_BUSINESS_EDITION Then
                trGroups.Visible = False
            End If

            If Not Page.IsPostBack Then

                'Account alerts
                Dim cmdSql As New SqlClient.SqlCommand("SMAlertsGet", Me.mconSqlImage)
                cmdSql.Parameters.Add("@UserId", Me.mobjUser.ImagesUserId)
                cmdSql.CommandType = CommandType.StoredProcedure
                Dim dt As New DataTable
                Dim da As New SqlClient.SqlDataAdapter(cmdSql)
                da.Fill(dt)

                If dt.Rows.Count = 0 Then
                    tblAlert.Visible = False
                Else
                    AlertMessage.InnerHtml = dt.Rows(0)("AlertMessage")
                End If

                'Recent items
                'SMRecentItemGet()
                '@UserID int
                cmdSql.Parameters.Clear()
                cmdSql.CommandText = "SMRecentItemGet"
                cmdSql.Parameters.Add("@UserId", Me.mobjUser.ImagesUserId)
                cmdSql.CommandType = CommandType.StoredProcedure
                Dim dtRecent As New DataTable
                da.Fill(dtRecent)

                If dtRecent.Rows.Count = 0 Then
                    NoRecent.InnerHtml = "No recent items."
                Else
                    NoRecent.InnerHtml = ""
                    dgRecentItems.DataSource = dtRecent
                    dgRecentItems.DataBind()
                End If

            End If

            'Hide scan if user has no access.
            trScan.Visible = dtAccess.Rows(0)("Index")

            If Functions.ModuleSetting(Functions.ModuleCode.SCAN_V2, Me.mobjUser) Then

                Dim scanURL, mtomURL, saveURL, buttonLink As String

                Try

                    mconSqlWeb = Functions.BuildMasterConnection()

                    Dim cmdSqlWebSvc As New SqlClient.SqlCommand("acsOptionValuebyKey", mconSqlWeb)
                    cmdSqlWebSvc.Parameters.Add("@OptionKey", "CLICKONCESCANURL")
                    cmdSqlWebSvc.CommandType = CommandType.StoredProcedure
                    scanURL = cmdSqlWebSvc.ExecuteScalar
                    cmdSqlWebSvc.Parameters.Clear()
                    cmdSqlWebSvc.Parameters.Add("@OptionKey", "CLICKONCEMTOMURL")
                    mtomURL = cmdSqlWebSvc.ExecuteScalar
                    cmdSqlWebSvc.Parameters.Clear()
                    cmdSqlWebSvc.Parameters.Add("@OptionKey", "CLICKONCESAVEURL")
                    saveURL = cmdSqlWebSvc.ExecuteScalar

                    Dim qualitySetting As String
                    Dim conCompany As SqlClient.SqlConnection
                    conCompany = Functions.BuildConnection(Me.mobjUser)
                    conCompany.Open()

                    cmdSqlWebSvc = New SqlClient.SqlCommand("SMScanQualityGet", conCompany)
                    cmdSqlWebSvc.Parameters.Clear()
                    cmdSqlWebSvc.CommandType = CommandType.StoredProcedure
                    Dim qSet As New SqlClient.SqlParameter
                    qSet.ParameterName = "@Quality"
                    qSet.Direction = ParameterDirection.Output
                    qSet.DbType = DbType.Int32
                    cmdSqlWebSvc.Parameters.Add(qSet)
                    cmdSqlWebSvc.ExecuteNonQuery()
                    qualitySetting = qSet.Value

                    Dim sb As New System.Text.StringBuilder
                    sb.Append("<Param><AccountID>" & Me.mobjUser.AccountId.ToString & "</AccountID>")
                    sb.Append("<AccountDesc>" & Me.mobjUser.AccountDesc.ToString & "</AccountDesc>")
                    sb.Append("<UserID>" & Me.mobjUser.UserId.ToString & "</UserID>")
                    sb.Append("<LoginName>" & Me.mobjUser.EMailAddress.ToString & "</LoginName>")
                    sb.Append("<ScanWS>" & mtomURL & "</ScanWS>")
                    sb.Append("<SaveWS>" & saveURL & "</SaveWS>")
                    sb.Append("<Quality>" & qualitySetting & "</Quality>")
                    sb.Append("<Date>" & Now.Date.ToString & "</Date></Param>")

                    trScan.Attributes("onclick") = "parent.location.href='" & scanURL & "?Param=" & HttpUtility.UrlEncode(Encryption.Encryption.Encrypt(sb.ToString)) & "';"

                Catch ex As Exception

                Finally

                    If mconSqlWeb.State <> ConnectionState.Closed Then
                        mconSqlWeb.Close()
                        mconSqlWeb.Dispose()
                    End If

                End Try

            Else
                trScan.Attributes("onclick") = "parent.location.href='DocumentManager.aspx';"
            End If

            If Not Page.IsPostBack Then BuildTree()

            If Not Page.IsPostBack Then LoadModifiedUsers()

            'Integration
            Dim blnIntegration As Boolean = Session("IntegrationSearch")
            If Session("IntegrationSearch") = True Then

                Session("IntegrationSearch") = False

                If CInt(Session("IntegrationDocumentID")) = 0 Then
                    txtQuickSearch.Text = Session("IntegrationAttributeValue")
                    'ReturnControls.Add(txtQuickSearch)
                    btnQuickSearch_Click(Nothing, Nothing)
                    Exit Sub

                Else

                    'Set the values for the modify button
                    Dim strDocumentID As String = Session("IntegrationDocumentID")
                    Dim intFieldID As Integer = CInt(Session("IntegrationAttributeID"))

                    Dim sqlCmd As New SqlClient.SqlCommand("IntegrationAttributeIDGet", Me.mconSqlImage)
                    sqlCmd.Parameters.Add("@IntegrationID", CInt(Session("IntegrationIntID")))
                    sqlCmd.Parameters.Add("@FieldID", intFieldID)
                    sqlCmd.Parameters.Add("@Action", "S")
                    sqlCmd.Parameters.Add("@DocumentID", CInt(strDocumentID))
                    sqlCmd.CommandType = CommandType.StoredProcedure
                    Dim daSql As New SqlClient.SqlDataAdapter(sqlCmd)

                    Dim dtAttributeId As New DataTable
                    daSql.Fill(dtAttributeId)

                    Dim strAttributeValue As String = RTrim(Session("IntegrationAttributeValue"))
                    Dim strAttributeID As String
                    If dtAttributeId.Rows.Count > 0 Then
                        strAttributeID = dtAttributeId.Rows(0)("AttributeID")
                        Session("IntegrationAttributeID") = strAttributeID
                        Dim strExpression As String = dtAttributeId.Rows(0)("Expression")

                        Try
                            If Trim(strExpression).Length > 0 Then strAttributeValue = Functions.VBS(strExpression, strAttributeValue)
                        Catch ex As Exception

                        End Try

                    End If

                    Dim dtDocuments As New DataTable
                    dtDocuments.Columns.Add(New DataColumn("DocumentId", SqlDbType.Int.GetType))
                    Dim drDocument As DataRow
                    drDocument = dtDocuments.NewRow
                    drDocument("DocumentId") = strDocumentID
                    dtDocuments.Rows.Add(drDocument)

                    Dim dtAttributes As New DataTable
                    dtAttributes.Columns.Add(New DataColumn("AttributeId", SqlDbType.Int.GetType))
                    dtAttributes.Columns.Add(New DataColumn("AttributeValue", Type.GetType("System.String")))
                    dtAttributes.Columns.Add(New DataColumn("AttributeValueEnd", Type.GetType("System.String")))
                    dtAttributes.Columns.Add(New DataColumn("SearchType", Type.GetType("System.String")))
                    Dim drAtt As DataRow
                    drAtt = dtAttributes.NewRow
                    drAtt("AttributeId") = strAttributeID
                    drAtt("AttributeValue") = strAttributeValue
                    drAtt("AttributeValueEnd") = ""
                    drAtt("SearchType") = "EQ"
                    dtAttributes.Rows.Add(drAtt)

                    Dim dsAd As New DataSet
                    dsAd.Tables.Add(dtDocuments)
                    dsAd.Tables.Add(dtAttributes)

                    LoadSearch.Value = 1
                    IntegrationAdvancedSearch(dsAd)

                End If

            End If

            If Request.QueryString("A") = "I" Then
                Session("IndexDocumentID") = Request.QueryString("DID")
                Server.Transfer("DocumentManager.aspx" & Request.Url.Query)
            End If

            If Not Page.IsPostBack Then LoadLastSearch()

        Catch ex As Exception

            If ex.Message.ToString.IndexOf("ArgumentNull_String") > -1 Then
                Server.Transfer("Default.aspx" & Request.Url.Query)
            End If

            'If ex.Message <> "Thread was being aborted." And Not IsNothing(mobjUser) Then
            '    Dim conMaster As SqlClient.SqlConnection
            '    conMaster = Functions.BuildMasterConnection
            '    Functions.LogException(mobjUser.UserId.ToString, mobjUser.AccountId.ToString, "", "Search Page Load: " & ex.Message, ex.StackTrace, Server.MachineName, "", Request.UrlReferrer.ToString, conMaster, True)
            'End If

        End Try

    End Sub

#Region "Tree Functions"

    Private Sub BuildTree()

        If Not Page.IsPostBack Then

            Dim firstDoc As TreeViewNode

            Dim objFolders As New Folders(Me.mconSqlImage, False)
            Dim ds As New DataSet

            ds = objFolders.FoldersGet(Me.mobjUser.ImagesUserId)

            Dim dtMemory As New DataTable
            dtMemory = objFolders.GetTreeState(Me.mobjUser.ImagesUserId)
            Dim dvMemory As New DataView(dtMemory)

            'Workspace
            If Functions.ModuleSetting(Functions.ModuleCode.SCAN_V2, Me.mobjUser, Me.mconSqlImage) = True Or Functions.ModuleSetting(Functions.ModuleCode.WORKSPACE, Me.mobjUser, Me.mconSqlImage) = True Then
                Dim nodeWs As New TreeViewNode
                nodeWs.Text = "Workspace"
                nodeWs.ID = "Workspace"
                nodeWs.Expanded = True
                nodeWs.AutoPostBackOnSelect = True
                nodeWs.ImageUrl = IMAGE_WORKSPACE
                nodeWs.LabelPadding = 3
                nodeWs.ClientSideCommand = "browseFolder('2147483647');setValue('Workspace');disableCabinet();disableRename();disableDelete();disableSecurity();disableAdd();disableFavorite();disableFolder();disableMove();disableShare();"
                nodeWs.ToolTip = "The workspace contains all of your new scanned and printed documents. These documents can be opened and indexed."
                treeSearch.Nodes.Add(nodeWs)
            End If

            'FAVORITES
            Dim FavoritesNode As New TreeViewNode
            FavoritesNode.Text = " My Favorites"
            FavoritesNode.ID = "MYFAVNODE"
            FavoritesNode.ImageUrl = IMAGE_FAVORITES
            FavoritesNode.ToolTip = "Favorites are shortcuts to your favorite folders. To add a folder select it from below and click the 'Add to Favorites' icon above the tree."

            dvMemory.RowFilter = "TreeNodeID='" & Folders.Node.Favorites & "'"
            If dvMemory.Count > 0 Then
                If dvMemory(0)("Expanded") = "1" Then
                    FavoritesNode.Expanded = True
                Else
                    FavoritesNode.Expanded = False
                End If
            Else
                FavoritesNode.Expanded = True
            End If

            FavoritesNode.ShowCheckBox = False
            FavoritesNode.LabelPadding = 3
            FavoritesNode.AutoPostBackOnSelect = False
            FavoritesNode.AutoPostBackOnExpand = False
            FavoritesNode.AutoPostBackOnCollapse = False
            FavoritesNode.ClientSideCommand = "disableCabinet();disableRename();disableDelete();disableSecurity();disableAdd();disableFavorite();disableFolder();disableMove();disableShare();"
            treeSearch.Nodes.Add(FavoritesNode)

            For Each drFavorite As DataRow In ds.Tables(TreeTables.FAVORITES).Rows
                Dim favFolder As New TreeViewNode
                favFolder.Text = drFavorite("FavoriteName")
                favFolder.ID = "F" & drFavorite("FavoriteId") & FAVORITE_SPLIT & drFavorite("FolderID")
                favFolder.ImageUrl = IMAGE_FOLDER
                favFolder.ToolTip = drFavorite("FolderPath")
                favFolder.AutoPostBackOnSelect = False
                favFolder.LabelPadding = 3
                favFolder.ClientSideCommand = "browseFolder('" & drFavorite("FolderID") & "');setValue('" & favFolder.ID & "');disableCabinet();enableRename();enableDelete();disableSecurity();disableAdd();disableFavorite();disableFolder();disableMove();disableShare();"
                FavoritesNode.Nodes.Add(favFolder)
            Next

            'DOCUMENTS
            If Functions.ModuleSetting(Functions.ModuleCode.DOCUMENT_TYPES_ATTRIBUTES, mobjUser) Then

                If ds.Tables(TreeTables.DOCUMENTS).Rows.Count > 0 Then

                    'Documents
                    Dim docNode As New TreeViewNode
                    docNode.Text = " Document Types"
                    docNode.ID = "DOCNODE"

                    docNode.ClientSideCommand = "disableCabinet();disableRename();disableDelete();disableSecurity();disableAdd();disableFavorite();disableFolder();disableMove();disableShare();"
                    docNode.ImageUrl = IMAGE_DOCUMENT_TYPES

                    docNode.ToolTip = "Perform an advanced search through your documents. You can select a single document or check multiple documents to build a search."

                    dvMemory.RowFilter = "TreeNodeID='" & Folders.Node.DocumentTypes & "'"
                    If dvMemory.Count > 0 Then
                        If dvMemory(0)("Expanded") = "1" Then
                            docNode.Expanded = True
                        Else
                            docNode.Expanded = False
                            NoDocMode.Value = "1" 'This will hide the advanced search from appearing by default when the node is collapsed.
                        End If
                    Else
                        docNode.Expanded = True
                    End If

                    docNode.ShowCheckBox = False
                    docNode.LabelPadding = 3
                    docNode.AutoPostBackOnSelect = False
                    docNode.AutoPostBackOnCollapse = False
                    docNode.AutoPostBackOnExpand = False

                    treeSearch.Nodes.Add(docNode)

                    'All documents node
                    Dim docAllDocuments As New TreeViewNode
                    docAllDocuments.Text = "All Documents"
                    docAllDocuments.ID = "AllDocs"
                    docAllDocuments.ShowCheckBox = True
                    docAllDocuments.AutoPostBackOnSelect = False
                    docAllDocuments.AutoPostBackOnCheckChanged = False
                    docAllDocuments.Checked = False
                    docAllDocuments.EditingEnabled = False
                    docAllDocuments.LabelPadding = 3
                    docAllDocuments.ExtendNodeCell = False
                    'docAllDocuments.ClientSideCommand = "disableCabinet();disableRename();disableDelete();disableSecurity();disableAdd();disableFavorite();disableFolder();disableMove();"
                    docNode.Nodes.Add(docAllDocuments)

                    Dim X As Integer = 1

                    Dim sbChecks As New System.Text.StringBuilder
                    sbChecks.Append("function NodeChecked(sender, eventArgs) { ")

                    'Dim sbClick As New System.Text.StringBuilder
                    'sbClick.Append("function loadAttributes(docid) { ")

                    Dim strFirstLoad As String

                    For Each drDocument As DataRow In ds.Tables(TreeTables.DOCUMENTS).Rows

                        If X = 1 Then
                            Session("firstDocumentNodeValue") = "D" & drDocument("DocumentId")
                            System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType(), "firstDoc", "firstDoc = " & drDocument("DocumentId") & ";", True)
                        End If

                        Dim docFolder As New TreeViewNode
                        docFolder.Text = drDocument("DocumentName")
                        docFolder.ID = "D" & drDocument("DocumentId")

                        docFolder.ImageUrl = IMAGE_DOCUMENT
                        docFolder.ShowCheckBox = True
                        docFolder.Checked = False
                        docFolder.EditingEnabled = False
                        docFolder.LabelPadding = 3
                        docFolder.AutoPostBackOnCheckChanged = False
                        docFolder.AutoPostBackOnSelect = False
                        'docFolder.ClientSideCommand = "disableCabinet();disableRename();disableDelete();disableSecurity();disableAdd();disableFavorite();disableFolder();disableMove();"

                        ''Get associated attribute id's to build client side command
                        'Dim cmdSql As New SqlClient.SqlCommand("acsGetAttributes", Me.mconSqlImage)
                        'cmdSql.CommandType = CommandType.StoredProcedure
                        'cmdSql.Parameters.Add("@DocumentId", drDocument("DocumentId"))
                        'cmdSql.Parameters.Add("@UserId", Me.mobjUser.ImagesUserId)
                        'Dim sqlDa As New SqlClient.SqlDataAdapter(cmdSql)
                        'Dim dtAttributes As New DataTable
                        'sqlDa.Fill(dtAttributes)

                        'Dim sbCommand As New System.Text.StringBuilder
                        'For Each dr As DataRow In dtAttributes.Rows
                        '    sbCommand.Append("show" & dr("AttributeId") & "();")
                        'Next

                        ''If X = 1 Then strFirstLoad = sbCommand.ToString

                        'sbClick.Append("if (docid == " & drDocument("DocumentId") & "){")
                        'sbClick.Append("try{" & sbCommand.ToString & "} catch(e) { }")
                        'sbClick.Append("}")

                        docNode.Nodes.Add(docFolder)

                        X += 1

                    Next

                    'sbChecks.Append("}")
                    sbChecks.Append("checked = true;}")
                    'sbClick.Append("}")
                    System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType(), "sbChecks", sbChecks.ToString, True)


                    'ReturnScripts.Add(sbClick.ToString)
                    'TODO ReturnScripts.Add(sbChecks.ToString)

                    ''ReturnScripts.Add("hideAll();")

                    ''Fill attributes grid with all attributes
                    'Dim sqlCmd As New SqlClient.SqlCommand("AttributesByUserID", Me.mconSqlImage)
                    'sqlCmd.CommandType = CommandType.StoredProcedure
                    'sqlCmd.Parameters.Add("@UserId", Me.mobjUser.ImagesUserId)
                    'Dim da As New SqlClient.SqlDataAdapter(sqlCmd)
                    'Dim dt As New DataTable
                    'da.Fill(dt)

                    'dgSearchAttributes.DataSource = dt
                    'dgSearchAttributes.DataBind()
                    'dgSearchAttributes.Visible = True

                Else

                    'No documents configured for this account, turn on no document mode
                    NoDocMode.Value = "1"

                End If

            Else

                'Documents disabled for the account, turn on no document mode
                NoDocMode.Value = "1"
                trAdvancedSearch.Visible = False

            End If

            'Views
            Dim viewsNode As New TreeViewNode
            viewsNode.Text = "Views"
            viewsNode.LabelPadding = 3
            viewsNode.ID = "Views"
            viewsNode.AutoPostBackOnSelect = False
            viewsNode.ImageUrl = IMAGE_VIEW
            viewsNode.ClientSideCommand = "setValue('Views');disableCabinet();disableRename();disableDelete();disableSecurity();disableAdd();disableFavorite();disableFolder();disableMove();disableShare();"
            viewsNode.AutoPostBackOnExpand = False
            viewsNode.AutoPostBackOnCollapse = False

            dvMemory.RowFilter = "TreeNodeID='" & Folders.Node.Views & "'"
            If dvMemory.Count > 0 Then
                If dvMemory(0)("Expanded") = "1" Then
                    viewsNode.Expanded = True
                Else
                    viewsNode.Expanded = False
                End If
            Else
                viewsNode.Expanded = True
            End If

            treeSearch.Nodes.Add(viewsNode)

            'Unassigned node
            If ds.Tables(TreeTables.UNASSIGNED).Rows.Count > 0 Then
                If ds.Tables(TreeTables.UNASSIGNED).Rows(0)("CanAddFilesToFolder") = True Then
                    Dim nodeUnassigned As New TreeViewNode
                    nodeUnassigned.Text = "Documents not in folders"
                    nodeUnassigned.ID = "Unassigned"
                    nodeUnassigned.AutoPostBackOnSelect = False
                    nodeUnassigned.Expanded = False
                    nodeUnassigned.ImageUrl = IMAGE_VIEW
                    nodeUnassigned.LabelPadding = 3
                    nodeUnassigned.ToolTip = "Displays documents that are not inside of a folder."

                    If Me.mobjUser.EditionID = AccountEdition.SMALL_BUSINESS_EDITION Then
                        nodeUnassigned.ClientSideCommand = "document.all.SearchType.value = 'F';browseFolder('0');setValue('Unassigned');disableCabinet();disableRename();disableDelete();disableSecurity();disableAdd();disableFavorite();disableFolder();disableMove();disableShare();"
                    Else
                        nodeUnassigned.ClientSideCommand = "document.all.SearchType.value = 'F';browseFolder('0');setValue('Unassigned');disableCabinet();disableRename();disableDelete();enableSecurity();disableAdd();disableFavorite();disableFolder();disableMove();disableShare();"
                    End If

                    viewsNode.Nodes.Add(nodeUnassigned)
                End If
            End If

            If Functions.ModuleSetting(Functions.ModuleCode.DOCUMENT_TYPES_ATTRIBUTES, Me.mobjUser) Then
                Dim nodeUnindexed As New TreeViewNode
                nodeUnindexed.Text = "Documents not indexed"
                nodeUnindexed.ID = "Unindexed"
                nodeUnindexed.AutoPostBackOnSelect = False
                nodeUnindexed.Expanded = False
                nodeUnindexed.ImageUrl = IMAGE_VIEW
                nodeUnindexed.LabelPadding = 3
                nodeUnindexed.ClientSideCommand = "browseFolder('-1');setValue('Unindexed');disableCabinet();disableRename();disableDelete();disableSecurity();disableAdd();disableFavorite();disableFolder();disableMove();disableShare();"
                nodeUnindexed.ToolTip = "Displays documents that do not have attributes assigned to them."
                viewsNode.Nodes.Add(nodeUnindexed)
            End If

            'Recently added files
            Dim nodeRecent As New TreeViewNode
            nodeRecent.Text = "Documents recently added"
            nodeRecent.ID = "Recent"
            nodeRecent.AutoPostBackOnSelect = False
            nodeRecent.Expanded = False
            nodeRecent.ImageUrl = IMAGE_VIEW
            nodeRecent.LabelPadding = 3
            nodeRecent.ClientSideCommand = "browseFolder('-2');setValue('Recent');disableCabinet();disableRename();disableDelete();disableSecurity();disableAdd();disableFavorite();disableFolder();disableMove();disableShare();"
            nodeRecent.ToolTip = "Displays the last 200 documents saved in your account that you have access to view."
            viewsNode.Nodes.Add(nodeRecent)

            'Cabinets & Folders
            Dim CatFolderNode As New TreeViewNode
            CatFolderNode.Text = "Cabinets & Folders"
            CatFolderNode.ID = "CabsFolders"
            CatFolderNode.AutoPostBackOnSelect = False

            If Me.mobjUser.SysAdmin Then
                CatFolderNode.ClientSideCommand = "enableCabinet();disableRename();disableDelete();disableSecurity();disableAdd();disableFavorite();disableFolder();disableMove();disableShare();"
            Else
                CatFolderNode.ClientSideCommand = "disableCabinet();disableRename();disableDelete();disableSecurity();disableAdd();disableFavorite();disableFolder();disableMove();disableShare();"
            End If

            dvMemory.RowFilter = "TreeNodeID='" & Folders.Node.CabinetsFolders & "'"
            If dvMemory.Count > 0 Then
                If dvMemory(0)("Expanded") = "1" Then
                    CatFolderNode.Expanded = True
                Else
                    CatFolderNode.Expanded = False
                End If
            Else
                CatFolderNode.Expanded = True
            End If

            CatFolderNode.ImageUrl = IMAGE_CABINETS_FOLDERS
            CatFolderNode.LabelPadding = 3
            CatFolderNode.AutoPostBackOnCollapse = False
            CatFolderNode.AutoPostBackOnExpand = False

            'Recycle Bin
            If Me.mobjUser.SysAdmin Then
                Dim nodeRecycle As New TreeViewNode
                nodeRecycle.Text = "Recycle Bin"
                nodeRecycle.ID = "RecycleBin"
                nodeRecycle.AutoPostBackOnSelect = False
                nodeRecycle.Expanded = False
                nodeRecycle.ImageUrl = IMAGE_RECYCLE_BIN
                nodeRecycle.LabelPadding = 3
                nodeRecycle.ClientSideCommand = "browseFolder('-3');setValue('Unassigned');disableCabinet();disableRename();disableDelete();disableSecurity();disableAdd();disableFavorite();disableFolder();disableMove();disableShare();"
                CatFolderNode.Nodes.Add(nodeRecycle)
            End If

            ''Unassigned node
            'If ds.Tables(TreeTables.UNASSIGNED).Rows.Count > 0 Then
            '    If ds.Tables(TreeTables.UNASSIGNED).Rows(0)("CanAddFilesToFolder") = True Then
            '        Dim nodeUnassigned As New TreeViewNode
            '        nodeUnassigned.Text = "Documents not in folders"
            '        nodeUnassigned.ID = "Unassigned"
            '        nodeUnassigned.AutoPostBackOnSelect = False
            '        nodeUnassigned.Expanded = False
            '        nodeUnassigned.ImageUrl = IMAGE_UNASSIGNED_NODE
            '        nodeUnassigned.LabelPadding = 3

            '        If Me.mobjUser.EditionID = AccountEdition.SMALL_BUSINESS_EDITION Then
            '            nodeUnassigned.ClientSideCommand = "document.all.SearchType.value = 'F';browseFolder('0');setValue('Unassigned');disableCabinet();disableRename();disableDelete();disableSecurity();disableAdd();disableFavorite();disableFolder();disableMove();disableShare();"
            '        Else
            '            nodeUnassigned.ClientSideCommand = "document.all.SearchType.value = 'F';browseFolder('0');setValue('Unassigned');disableCabinet();disableRename();disableDelete();enableSecurity();disableAdd();disableFavorite();disableFolder();disableMove();disableShare();"
            '        End If

            '        CatFolderNode.Nodes.Add(nodeUnassigned)
            '    End If
            'End If

            'If Functions.ModuleSetting(Functions.ModuleCode.DOCUMENT_TYPES_ATTRIBUTES, Me.mobjUser) Then
            '    Dim nodeUnindexed As New TreeViewNode
            '    nodeUnindexed.Text = "Documents not indexed"
            '    nodeUnindexed.ID = "Unindexed"
            '    nodeUnindexed.AutoPostBackOnSelect = False
            '    nodeUnindexed.Expanded = False
            '    nodeUnindexed.ImageUrl = IMAGE_UNASSIGNED_NODE
            '    nodeUnindexed.LabelPadding = 3
            '    nodeUnindexed.ClientSideCommand = "browseFolder('-1');setValue('Unindexed');disableCabinet();disableRename();disableDelete();disableSecurity();disableAdd();disableFavorite();disableFolder();disableMove();disableShare();"
            '    CatFolderNode.Nodes.Add(nodeUnindexed)
            'End If

            ''Recently added files
            'Dim nodeRecent As New TreeViewNode
            'nodeRecent.Text = "Documents recently added"
            'nodeRecent.ID = "Recent"
            'nodeRecent.AutoPostBackOnSelect = False
            'nodeRecent.Expanded = False
            'nodeRecent.ImageUrl = IMAGE_UNASSIGNED_NODE
            'nodeRecent.LabelPadding = 3
            'nodeRecent.ClientSideCommand = "browseFolder('-2');setValue('Recent');disableCabinet();disableRename();disableDelete();disableSecurity();disableAdd();disableFavorite();disableFolder();disableMove();disableShare();"
            'CatFolderNode.Nodes.Add(nodeRecent)

            If ds.Tables(TreeTables.SHARES).Rows.Count > 0 Then
                'no cabinets or folders, blank state message?
            End If

            treeSearch.Nodes.Add(CatFolderNode)

            For Each drShare As DataRow In ds.Tables(TreeTables.SHARES).Rows

                Dim sbShareCommand As New System.Text.StringBuilder

                'Build share
                Dim Share As New TreeViewNode
                Share.Text = drShare("ShareName")
                Share.ID = "S" & drShare("ShareId")
                Share.ImageUrl = IMAGE_CABINET

                If drShare("HasChildFolder") = True Then
                    Share.ContentCallbackUrl = "XmlGenerator.aspx?NodeId=" & Share.ID
                End If

                If drShare("ExpandShare") = True Then
                    Share.Expanded = True
                End If

                sbShareCommand.Append("setId('" & Share.ID & "');disableMove();browseCabinet('" & Share.ID.Replace("S", "") & "');")

                If drShare("CanAddShare") = True Then
                    sbShareCommand.Append("enableCabinet();")
                Else
                    sbShareCommand.Append("disableCabinet();")
                End If

                If drShare("CanRenameShare") = True Then
                    sbShareCommand.Append("enableRename();")
                Else
                    sbShareCommand.Append("disableRename();")
                End If

                If drShare("CanDeleteShare") = True Then
                    sbShareCommand.Append("enableDelete();")
                Else
                    sbShareCommand.Append("disableDelete();")
                End If

                If drShare("CanAddFolder") = True Then
                    sbShareCommand.Append("enableFolder();")
                Else
                    sbShareCommand.Append("disableFolder();")
                End If

                If Me.mobjUser.EditionID = AccountEdition.SMALL_BUSINESS_EDITION Then
                    sbShareCommand.Append("disableSecurity();")
                Else
                    If drShare("CanChangeSecurity") = True Then
                        sbShareCommand.Append("enableSecurity();")
                    Else
                        sbShareCommand.Append("disableSecurity();")
                    End If
                End If

                sbShareCommand.Append("disableAdd();disableFavorite();setValue('" & Share.ID & "');disableShare();")

                Share.ClientSideCommand = sbShareCommand.ToString
                Share.AutoPostBackOnSelect = False
                Share.AutoPostBackOnRename = False

                Share.LabelPadding = 3
                CatFolderNode.Nodes.Add(Share)

            Next

            'Build share children folders
            For Each drChild As DataRow In ds.Tables(TreeTables.FOLDERS).Rows

                Dim intParentFolderId As Integer = drChild("ParentFolderId")
                Dim intFolderLevel As Integer = drChild("FolderLevel")

                'Find parent
                Dim ParentNode As New TreeViewNode
                If intFolderLevel = 1 Then
                    ParentNode = treeSearch.FindNodeById("S" & intParentFolderId.ToString)
                Else
                    ParentNode = treeSearch.FindNodeById("W" & intParentFolderId.ToString)
                End If

                'Add child to parent
                If Not IsNothing(ParentNode) Then

                    Dim ChildNode As New TreeViewNode
                    ChildNode.Text = drChild("FolderName")
                    ChildNode.ID = "W" & drChild("FolderId")

                    ChildNode.AutoPostBackOnSelect = False
                    ChildNode.ImageUrl = IMAGE_FOLDER
                    ChildNode.AutoPostBackOnRename = False
                    ChildNode.EditingEnabled = False
                    ChildNode.ExpandedImageUrl = IMAGE_FOLDER_OPEN
                    ChildNode.LabelPadding = 3
                    ChildNode.ExtendNodeCell = False

                    If drChild("HasChildFolder") = True Then
                        ChildNode.ContentCallbackUrl = "XmlGenerator.aspx?NodeId=" & ChildNode.ID
                        ChildNode.Expanded = False
                    End If

                    If drChild("ExpandFolder") = True Then
                        ChildNode.Expanded = True
                    End If

                    Dim sbFolderCommand As New System.Text.StringBuilder

                    sbFolderCommand.Append("startProgressDisplay();browseFolder('" & drChild("FolderId") & "');")

                    If drChild("CanAddFolder") = True Then
                        sbFolderCommand.Append("enableFolder();")
                    Else
                        sbFolderCommand.Append("disableFolder();")
                    End If

                    If drChild("CanAddFilesToFolder") = True Then
                        sbFolderCommand.Append("enableAdd();")
                    Else
                        sbFolderCommand.Append("disableAdd();")
                    End If

                    If drChild("CanRenameFolder") = True Then
                        sbFolderCommand.Append("enableRename();")
                    Else
                        sbFolderCommand.Append("disableRename();")
                    End If

                    If drChild("CanDeleteFolder") = True Then
                        sbFolderCommand.Append("enableDelete();enableMove();")
                    Else
                        sbFolderCommand.Append("disableDelete();")
                    End If

                    If drChild("CanChangeSecurity") = True Then
                        sbFolderCommand.Append("enableSecurity();")
                    Else
                        sbFolderCommand.Append("disableSecurity();")
                    End If

                    If drChild("CanChangeSecurity") = True Then
                        sbFolderCommand.Append("enableSecurity();")
                    Else
                        sbFolderCommand.Append("disableSecurity();")
                    End If

                    sbFolderCommand.Append("enableFavorite();disableCabinet();setValue('" & ChildNode.ID & "');")

                    'If Me.mobjUser.EditionID = AccountEdition.SMALL_BUSINESS_EDITION Then
                    sbFolderCommand.Append("enableShare();")
                    'End If

                    ChildNode.ClientSideCommand = sbFolderCommand.ToString

                    ParentNode.Nodes.Add(ChildNode)

                End If

            Next

        End If

    End Sub

#End Region

    Private Sub dgSearchAttributes_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgSearchAttributes.PreRender

        Dim intAttributeId As Integer

        Try

            For Each dgRow As DataGridItem In dgSearchAttributes.Items

                Dim lblAttributeId As Label = CType(dgRow.FindControl("lblAttributeId"), Label)
                Dim lblAttributeDataType As Label = CType(dgRow.FindControl("lblAttributeDataType"), Label)
                Dim lblLength As Label = CType(dgRow.FindControl("lblLength"), Label)
                Dim txtAttributeValue As TextBox = CType(dgRow.FindControl("txtAttributeValue"), TextBox)
                Dim ddlAttributeList As DropDownList = CType(dgRow.FindControl("ddlAttributeList"), DropDownList)
                Dim chkBox As CheckBox = CType(dgRow.FindControl("chkBox"), CheckBox)
                Dim ddlSearchType As DropDownList = CType(dgRow.FindControl("ddlSearchType"), DropDownList)

                Dim txtBetweenFrom As TextBox = CType(dgRow.FindControl("txtBetweenFrom"), TextBox)
                Dim txtBetweenTo As TextBox = CType(dgRow.FindControl("txtBetweenTo"), TextBox)
                Dim lblError As Label = CType(dgRow.FindControl("lblRowError"), Label)

                intAttributeId = lblAttributeId.Text

                txtBetweenFrom.Style("DISPLAY") = "none"
                txtBetweenTo.Style("DISPLAY") = "none"
                lblError.Style("DISPLAY") = "none"
                CType(dgRow.FindControl("AndLabel"), HtmlGenericControl).Style("DISPLAY") = "none"
                ddlSearchType.Attributes("onchange") = "configAttribute(this.options[this.selectedIndex].value, '" & txtAttributeValue.ClientID & "', '" & txtBetweenFrom.ClientID & "', '" & txtBetweenTo.ClientID & "', '" & dgRow.FindControl("AndLabel").ClientID & "');"

                ddlSearchType.AutoPostBack = False

            Next

        Catch ex As Exception

            Dim conMaster As SqlClient.SqlConnection
            conMaster = Functions.BuildMasterConnection
            Functions.LogException(mobjUser.UserId.ToString, mobjUser.AccountId.ToString, "", "Attributes Advanced Search PreRender Error, Failed AttributeID: " & intAttributeId & " " & ex.Message, ex.StackTrace, Server.MachineName, "", Request.UrlReferrer.ToString, conMaster, True)

        Finally

        End Try

    End Sub

    Private Sub dgResults_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgResults.ItemDataBound

        Dim dgRow As DataGridItem = e.Item

        Dim ImageType As Label = CType(dgRow.FindControl("ImageType"), Label)
        Dim ImageId As Label = CType(dgRow.FindControl("ImageId"), Label)
        Dim VersionId As Label = CType(dgRow.FindControl("VersionId"), Label)
        Dim DocInfo As Label = CType(dgRow.FindControl("DocInfo"), Label)
        Dim DocNo As Label = CType(dgRow.FindControl("DocNo"), Label)
        Dim Hits As Label = CType(dgRow.FindControl("Hits"), Label)
        Dim Relevance As Label = CType(dgRow.FindControl("Rel"), Label)
        Dim ModDate As Label = CType(dgRow.FindControl("Date"), Label)
        Dim Attributes As Label = CType(dgRow.FindControl("Att"), Label)
        Dim chkMass As CheckBox = dgRow.FindControl("chkMass")
        Dim imgIcon As Web.UI.WebControls.Image = CType(dgRow.FindControl("imgIcon"), Web.UI.WebControls.Image)
        Dim ResultRow As HtmlTable = e.Item.FindControl("ResultRow")
        'Dim 'lnkFile As LinkButton = e.Item.FindControl("'lnkFile")
        Dim imgDownload As ImageButton = e.Item.FindControl("imgDownload")
        Dim imgView As System.Web.UI.WebControls.Image = e.Item.FindControl("imgView")

        Dim node As String = NodeId.Value

        Try

            If Not ImageType Is Nothing Then

                If (e.Item.DataItem("ImageId") Is System.DBNull.Value Or e.Item.DataItem("CurrentVersionId") Is System.DBNull.Value) And (e.Item.DataItem("ImageType") <> ImageTypes.FOLDER) Then
                    dgRow.Visible = False
                    Exit Sub
                End If

                dgRow.Attributes("id") = "row" & e.Item.DataItem("CurrentVersionId")

                If Not e.Item.DataItem("ModifiedDate") Is System.DBNull.Value Then
                    ModDate.Text = GetNiceDate(e.Item.DataItem("ModifiedDate"))
                    ModDate.Visible = True
                End If

                Select Case e.Item.DataItem("ImageType")

                    Case ImageTypes.SCAN

                        'imgDownload.Attributes("onclick") = "document.all." & imgDownload.ClientID & ".click();event.cancelBubble = true;"
                        'imgDownload.Attributes("onclick") = "download(" & VersionId.Text & ");event.cancelBubble = true;"

                        'Dim postback As New System.Web.UI.AsyncPostBackTrigger
                        'postback.ControlID = imgDownload.UniqueID
                        'uResults.Triggers.Add(postback)

                        DocInfo.Visible = True
                        Dim sbDocInfo As New System.Text.StringBuilder

                        sbDocInfo.Append("<div id=info" & e.Item.DataItem("CurrentVersionId") & ">")
                        If Not e.Item.DataItem("DocumentName") Is System.DBNull.Value Then sbDocInfo.Append("<b>" & e.Item.DataItem("DocumentName") & "</b>")
                        If Not e.Item.DataItem("CurrentPageCount") Is System.DBNull.Value Then sbDocInfo.Append(", <SPAN id=pg" & e.Item.DataItem("CurrentVersionId") & """>" & e.Item.DataItem("CurrentPageCount") & IIf(e.Item.DataItem("CurrentPageCount") = 1, " page", " pages") & "</SPAN>")
                        If Not e.Item.DataItem("DocumentTitle") Is System.DBNull.Value Then sbDocInfo.Append("<br>Title: " & Server.UrlDecode(e.Item.DataItem("DocumentTitle")))
                        If Not e.Item.DataItem("DocumentDescription") Is System.DBNull.Value Then sbDocInfo.Append("<br>Description: " & Server.UrlDecode(e.Item.DataItem("DocumentDescription")))
                        If Not e.Item.DataItem("DocumentTags") Is System.DBNull.Value Then sbDocInfo.Append("<br>Tags: " & Functions.LinkTags(Server.UrlDecode(e.Item.DataItem("DocumentTags"))))
                        sbDocInfo.Append("</div>")
                        DocInfo.Text = sbDocInfo.ToString

                        If Not e.Item.DataItem("ImageId") Is System.DBNull.Value Then
                            DocNo.Text = "<b>" & e.Item.DataItem("ImageId") & "</b>"
                            DocNo.Visible = True
                        End If

                        If Not e.Item.DataItem("HitCount") Is System.DBNull.Value Then
                            'Hits.Text = e.Item.DataItem("HitCount") & IIf(e.Item.DataItem("HitCount") = 1, " hit", " hits")
                            Hits.Text = "<a onclick='LoadFullText(" & e.Item.DataItem("CurrentVersionId") & ");'>" & e.Item.DataItem("HitCount") & IIf(e.Item.DataItem("HitCount") = 1, " hit", " hits") & "</a>"
                            Hits.Visible = True
                        End If

                        If Not e.Item.DataItem("ScorePercent") Is System.DBNull.Value Then
                            Relevance.Text = e.Item.DataItem("ScorePercent") & "%"
                            Relevance.Visible = True
                        End If

                        If blnFolderBrowseMode Then
                            If Not node = BrowseType.RECYCLE_BIN Then
                                ResultRow.Attributes("onclick") = "javascript:LoadImage(" & e.Item.DataItem("CurrentVersionId") & ");"
                                dgRow.Attributes("style") = "CURSOR: hand"
                            End If
                        Else
                            ResultRow.Attributes("onclick") = "javascript:LoadImage(" & e.Item.DataItem("CurrentVersionId") & ");"
                            dgRow.Attributes("style") = "CURSOR: hand"
                        End If

                        imgView.Attributes("onclick") = "javascript:LoadImage(" & e.Item.DataItem("CurrentVersionId") & ");"

                        Dim dv As New DataView(ds.Tables(1))
                        dv.RowFilter = "ImageId=" & e.Item.DataItem("ImageId")
                        Attributes.Text = "<div id=att" & e.Item.DataItem("CurrentVersionId") & ">" & BuildAttributes(dv) & "</div>"
                        Attributes.Visible = True

                        imgIcon.Attributes("onmouseover") = "Preview(" & e.Item.DataItem("CurrentVersionId") & ");"
                        imgIcon.Attributes("onmouseleave") = "HidePreview();"

                    Case ImageTypes.ATTACHMENT

                        DocInfo.Visible = True
                        Dim sbDocInfo As New System.Text.StringBuilder

                        'lnkFile.Visible = True
                        ''lnkFile.Attributes("OnClick") = "download(" & e.Item.DataItem("CurrentVersionId") & "," & e.Item.DataItem("CurrentVersionId") & ",'" & e.Item.DataItem("Filename") & ");"
                        'OnClick="download(<%# DataBinder.Eval(Container, "DataItem.CurrentVersionId") %>,<%# DataBinder.Eval(Container, "DataItem.ImageType") %>,'<%# DataBinder.Eval(Container, "DataItem.Filename") %>');"

                        'lnkFile.Attributes("onclick") = "document.all." & 'lnkFile.ClientID & ".click();event.cancelBubble = true;"
                        'imgDownload.Attributes("onclick") = "document.all." & imgDownload.ClientID & ".click();event.cancelBubble = true;"

                        If e.Item.DataItem("Filename") Is System.DBNull.Value Then
                            sbDocInfo.Append("<b>Unknown Filename</b>")
                            ''lnkFile.Text = "Unknown Filename"
                            'lnkFile.Visible = False
                        Else
                            'lnkFile.Text = e.Item.DataItem("Filename")
                        End If
                        'RegisterExcludeControl('lnkFile)
                        'RegisterExcludeControl(imgDownload)

                        'Dim fileDownload As New System.Web.UI.PostBackTrigger
                        'fileDownload.ControlID = 'lnkFile.UniqueID
                        'uResults.Triggers.Add(fileDownload)

                        'Dim postback As New System.Web.UI.AsyncPostBackTrigger
                        'postback.ControlID = imgDownload.UniqueID
                        'uResults.Triggers.Add(postback)

                        sbDocInfo.Append("<div id=info" & e.Item.DataItem("CurrentVersionId") & ">")
                        If Not e.Item.DataItem("DocumentName") Is System.DBNull.Value Then sbDocInfo.Append("<b>" & e.Item.DataItem("DocumentName") & "</b>")
                        'If Not e.Item.DataItem("CurrentPageCount") Is System.DBNull.Value Then sbDocInfo.Append(", " & e.Item.DataItem("CurrentPageCount") & IIf(e.Item.DataItem("CurrentPageCount") = 1, " page", " pages"))
                        If Not e.Item.DataItem("DocumentTitle") Is System.DBNull.Value Then sbDocInfo.Append("<br>Title: " & Server.UrlDecode(e.Item.DataItem("DocumentTitle")))
                        If Not e.Item.DataItem("DocumentDescription") Is System.DBNull.Value Then sbDocInfo.Append("<br>Description: " & Server.UrlDecode(e.Item.DataItem("DocumentDescription")))
                        If Not e.Item.DataItem("DocumentTags") Is System.DBNull.Value Then sbDocInfo.Append("<br>Tags: " & Functions.LinkTags(Server.UrlDecode(e.Item.DataItem("DocumentTags"))))
                        sbDocInfo.Append("</div>")
                        DocInfo.Text = sbDocInfo.ToString

                        If Not e.Item.DataItem("ImageId") Is System.DBNull.Value Then
                            DocNo.Text = "<b>" & e.Item.DataItem("ImageId") & "</b>"
                            DocNo.Visible = True
                        End If

                        If Not e.Item.DataItem("ScorePercent") Is System.DBNull.Value Then
                            Relevance.Text = e.Item.DataItem("ScorePercent") & "%"
                            Relevance.Visible = True
                        End If

                        If blnFolderBrowseMode Then
                            If Not node = BrowseType.RECYCLE_BIN Then
                                ResultRow.Attributes("onclick") = "javascript:oldViewer(" & e.Item.DataItem("CurrentVersionId") & ");"
                                dgRow.Attributes("style") = "CURSOR: hand"
                            End If
                        Else
                            ResultRow.Attributes("onclick") = "javascript:oldViewer(" & e.Item.DataItem("CurrentVersionId") & ");"
                            dgRow.Attributes("style") = "CURSOR: hand"
                        End If

                        Dim dv As New DataView(ds.Tables(1))
                        dv.RowFilter = "ImageId=" & e.Item.DataItem("ImageId")
                        Attributes.Text = "<div id=att" & e.Item.DataItem("CurrentVersionId") & ">" & BuildAttributes(dv) & "</div>"
                        Attributes.Visible = True

                        Dim strIconPath As String = "images/icons/" & System.IO.Path.GetExtension(e.Item.DataItem("Filename")).ToUpper.Replace(".", "") & ".gif"
                        If System.IO.File.Exists(Server.MapPath(strIconPath)) Then
                            imgIcon.ImageUrl = strIconPath
                            If Not e.Item.DataItem("Filename") Is System.DBNull.Value Then
                                If System.IO.Path.GetExtension(e.Item.DataItem("Filename")).ToLower = ".pdf" Then
                                    imgIcon.Attributes.Add("onclick", "javascript:loadPDF(" & e.Item.DataItem("CurrentVersionId") & ");")
                                    imgView.Attributes.Add("onclick", "javascript:loadPDF(" & e.Item.DataItem("CurrentVersionId") & ");")
                                End If
                            End If
                        Else
                            imgIcon.ImageUrl = "Images/file.gif"
                        End If

                        If Not e.Item.DataItem("HitCount") Is System.DBNull.Value Then
                            If Not e.Item.DataItem("Filename") Is System.DBNull.Value Then
                                If System.IO.Path.GetExtension(e.Item.DataItem("Filename")).ToLower = ".pdf" Then
                                    Hits.Text = "<a onclick='LoadFullTextPDF(" & e.Item.DataItem("CurrentVersionId") & ");'>" & e.Item.DataItem("HitCount") & IIf(e.Item.DataItem("HitCount") = 1, " hit", " hits") & "</a>"
                                Else
                                    Hits.Text = e.Item.DataItem("HitCount") & IIf(e.Item.DataItem("HitCount") = 1, " hit", " hits")
                                End If
                            End If
                            Hits.Visible = True
                        End If

                    Case ImageTypes.FOLDER

                        DocInfo.Visible = True
                        Dim sbDocInfo As New System.Text.StringBuilder
                        sbDocInfo.Append("<b>" & e.Item.DataItem("FolderName") & "</b>")
                        sbDocInfo.Append("<br><span class=FolderPath>" & e.Item.DataItem("FolderPath") & "</span>")
                        DocInfo.Text = sbDocInfo.ToString

                        imgIcon.ImageUrl = "Images/folders/folder.gif"

                        ResultRow.Attributes("onclick") = "javascript:browseFolder(" & e.Item.DataItem("FolderId") & ");"
                        dgRow.Attributes("style") = "CURSOR: hand"

                        imgDownload.Visible = False
                        imgView.Visible = False

                End Select

                If node <> "AllDocs" And node <> "" Then
                    If node.Replace("W", "").Replace("D", "") = BrowseType.RECYCLE_BIN Then
                        imgDownload.Visible = False
                        imgView.Visible = False
                    End If
                End If

            End If

        Catch ex As Exception

            'ShowPopupError("An error has occured while performing your search. Your documents were located but some of the result data may not display correctly.", True)

            Dim conMaster As SqlClient.SqlConnection
            conMaster = Functions.BuildMasterConnection
            Functions.LogException(mobjUser.UserId.ToString, mobjUser.AccountId.ToString, "", "Result Grid Databinding Error: " & ex.Message, ex.StackTrace, Server.MachineName, "", Request.UrlReferrer.ToString, conMaster, True)

        Finally

            If Not IsNothing(ModDate) Then
                ModDate.Attributes.Add("onclick", "showSearch(this);")
                ModDate.Attributes.Add("onmouseout", "hideDrop(this)")
                ModDate.Attributes.Add("onmouseover", "showDrop(this, 0,'');")
                ModDate.CssClass = "dtIndent"
            End If

            If Not IsNothing(chkMass) Then chkMass.Attributes("onclick") = "shade('" & ResultRow.ClientID & "','" & chkMass.ClientID & "');"

            'If dgRow.ItemIndex Mod 2 = 0 Then
            dgRow.Attributes("onmouseover") = "this.className = 'hlt';"
            dgRow.Attributes("onmouseout") = "this.className = '';"
            'Else
            'Odd
            'dgRow.Attributes("onmouseover") = "this.className = 'hlt';"
            'dgRow.Attributes("onmouseout") = "this.className = '';"
            'End If

        End Try

    End Sub

    Private Function QuickSearch(Optional ByVal dsReload As DataSet = Nothing)

        Dim sqlCmd As New SqlCommand

        If CurrentPage.Value = "" Then CurrentPage.Value = "1"

        Dim curPage As Integer = CInt(CurrentPage.Value)
        Dim firstRow As Integer = (Me.mobjUser.DefaultResultCount * (curPage - 1)) + 1
        Dim lastRow As Integer = (Me.mobjUser.DefaultResultCount + firstRow) - 1
        Dim ResultCount As Integer

        Try

            recordSearchType(Functions.SearchType.QUICK)

            If (ddlQuickSearchType.SelectedValue = 1 And Not IsNumeric(txtQuickSearch.Text.Trim)) Then
                System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.NewGuid.ToString, "document.all.NoResults.innerHTML = '" & strNoResults & "';", True)
                lblPages.Text = ""
                lblSummary.Text = ""
                dgResults.DataSource = Nothing
                dgResults.DataBind()
                'HideSort()
                HideMassIcons()
                Exit Function
            ElseIf txtQuickSearch.Text.Trim = "*" Then
                ShowPopupError("The * wildcard search is not supported in Quick Search.<br><br>Please re-enter you quick search using more specific search criteria such as J* or John*.", False)
                lblPages.Text = ""
                lblSummary.Text = ""
                dgResults.DataSource = Nothing
                dgResults.DataBind()
                'HideSort()
                HideMassIcons()
                Exit Function
            End If

            NodeId.Value = ""
            'ReturnControls.Add(NodeId)

            Dim dtBegin As DateTime = Now

            Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
            Me.mconSqlImage.Open()

            Dim startTime As DateTime = Now

            If Not dsReload Is Nothing Then

                ds = dsReload
                ResultCount = Session("ResultCount")

            Else

                'Perform DTSearch
                Dim mdtFullTextData As DataTable
                mdtFullTextData = New DataTable("dtSearchData")
                mdtFullTextData.Columns.Add(New DataColumn("ImageDetID", GetType(System.Int32), Nothing, MappingType.Element))
                mdtFullTextData.Columns.Add(New DataColumn("Score", GetType(System.Int32), Nothing, MappingType.Element))
                mdtFullTextData.Columns.Add(New DataColumn("ScorePercent", GetType(System.Int32), Nothing, MappingType.Element))
                mdtFullTextData.Columns.Add(New DataColumn("HitCount", GetType(System.Int32), Nothing, MappingType.Element))

                If ddlQuickSearchType.SelectedValue = 5 Then

                    'If Functions.ModuleSetting(Functions.ModuleCode.FULL_TEXT, Me.mobjUser, Me.mconSqlImage) Then
                    'New full text search

                    Dim conFT As SqlClient.SqlConnection = Functions.BuildFullTextConnection(Me.mobjUser)

                    Dim cmdFt As New SqlClient.SqlCommand("SMFullTextSearch", conFT)
                    cmdFt.CommandType = CommandType.StoredProcedure
                    cmdFt.Parameters.Add("@KeyWord", txtQuickSearch.Text.Trim)

                    Dim da As New SqlClient.SqlDataAdapter(cmdFt)
                    Dim res As New DataTable("Results")
                    da.Fill(res)

                    For Each dr As DataRow In res.Rows

                        Dim drFullText As DataRow
                        drFullText = mdtFullTextData.NewRow

                        drFullText("ImageDetID") = dr("ImageDetId")
                        drFullText("Score") = dr("Score")
                        drFullText("ScorePercent") = dr("ScorePercent")
                        drFullText("HitCount") = dr("HitCount")

                        mdtFullTextData.Rows.Add(drFullText)

                    Next

                    conFT.Close()
                    conFT.Dispose()

                    'Else

                    '    'Existing dtsearch

                    '    Dim IndexLocationDirectory As String
                    '    IndexLocationDirectory = ConfigurationSettings.AppSettings("IndexLocation")

                    '    If IndexLocationDirectory.TrimEnd.Substring(Len(IndexLocationDirectory) - 1, 1) <> "\" Then
                    '        IndexLocationDirectory = IndexLocationDirectory.TrimEnd & "\"
                    '    End If

                    '    IndexLocationDirectory = IndexLocationDirectory & Me.mobjUser.DBName & "\"

                    '    Dim sj As SearchJob = New SearchJob
                    '    Dim results As SearchResults
                    '    Dim i As Integer
                    '    sj.IndexesToSearch = New System.Collections.Specialized.StringCollection
                    '    sj.IndexesToSearch.Add(IndexLocationDirectory)
                    '    sj.MaxFilesToRetrieve = ConfigurationSettings.AppSettings("DTSSearchMaxFiles")
                    '    sj.TimeoutSeconds = ConfigurationSettings.AppSettings("DTSSearchTimeOutSecs")
                    '    sj.Request = IIf(txtQuickSearch.Text.Trim = "*", "THISWILLNOTMATCHEVERCAUSEALEXSAIDSO", IIf(txtQuickSearch.Text.ToLower.IndexOf(" or ") > -1, txtQuickSearch.Text, txtQuickSearch.Text.Replace(" ", " AND ")))

                    '    Dim sr As New SearchResults

                    '    sj.Execute()
                    '    results = sj.Results

                    '    If results.Count > 0 Then 'build datatable
                    '        For i = 0 To results.Count - 1

                    '            Try

                    '                results.GetNthDoc(i)
                    '                Dim drFullText As DataRow
                    '                drFullText = mdtFullTextData.NewRow

                    '                If IsNothing(results.CurrentItem.UserFields) Then
                    '                    If IsNumeric(System.IO.Path.GetFileName(results.CurrentItem.Filename)) Then drFullText("ImageDetID") = System.IO.Path.GetFileName(results.CurrentItem.Filename)
                    '                Else
                    '                    Dim detId As String = results.CurrentItem.UserFields("ImageDetID")
                    '                    If detId.IndexOf("-") > -1 Then
                    '                        drFullText("ImageDetID") = detId.Split("-")(0)
                    '                    Else
                    '                        drFullText("ImageDetID") = results.CurrentItem.UserFields("ImageDetID")
                    '                    End If
                    '                End If

                    '                drFullText("Score") = results.CurrentItem.Score
                    '                drFullText("ScorePercent") = results.CurrentItem.ScorePercent

                    '                'If txtQuickSearch.Text.Trim.ToLower = "page" Then
                    '                '    drFullText("HitCount") = results.CurrentItem.HitCount - 2
                    '                'Else
                    '                '    drFullText("HitCount") = results.CurrentItem.HitCount - 1
                    '                'End If
                    '                drFullText("HitCount") = results.CurrentItem.HitCount

                    '                mdtFullTextData.Rows.Add(drFullText)

                    '            Catch ex As Exception

                    '            End Try

                    '        Next
                    '    End If

                    'End If

                End If

                Dim sb As New System.Text.StringBuilder
                Dim sqlDa As New SqlDataAdapter(sqlCmd)
                Dim sqlBld As SqlCommandBuilder

                Dim strFullText As String
                'Add global temp table for fulltext search results
                If Not mdtFullTextData Is Nothing Then
                    strFullText = System.Guid.NewGuid().ToString().Replace("-", "")
                    sb = New System.Text.StringBuilder
                    sb.Append("CREATE TABLE ##" & strFullText.ToString & " (ImageDetID int NULL, Score int NULL, ScorePercent int NULL, HitCount int NULL)" & vbCrLf)
                    sqlCmd = New SqlCommand(sb.ToString, Me.mconSqlImage)
                    sqlBld = New SqlCommandBuilder
                    sqlCmd.ExecuteNonQuery()
                    ' Add fulltext results
                    sqlCmd = New SqlCommand("SELECT ImageDetID, Score, ScorePercent, HitCount FROM ##" & strFullText, Me.mconSqlImage)
                    sqlDa = New SqlDataAdapter(sqlCmd)
                    sqlBld = New SqlCommandBuilder(sqlDa)
                    sqlDa.Update(CType(mdtFullTextData, DataTable))
                End If

                sqlCmd.Parameters.Clear()

                sqlCmd = New SqlCommand("SMQuickSearch", Me.mconSqlImage)
                sqlCmd.CommandTimeout = 180
                sqlCmd.CommandType = CommandType.StoredProcedure
                sqlCmd.Parameters.Add(New SqlParameter("@UserID", Me.mobjUser.ImagesUserId))
                sqlCmd.Parameters.Add(New SqlParameter("@BeginRow", firstRow))
                sqlCmd.Parameters.Add(New SqlParameter("@EndRow", lastRow))

                If ddlQuickSearchType.SelectedValue = 5 Then
                    'sqlCmd.Parameters.Add(New SqlParameter("@QSValue", IIf(txtQuickSearch.Text.Trim = "*", "THISWILLNOTMATCHEVERCAUSEALEXSAIDSO", IIf(txtQuickSearch.Text.ToLower.IndexOf(" or ") > -1, txtQuickSearch.Text, txtQuickSearch.Text.Replace(" ", " AND ")))))
                    sqlCmd.Parameters.Add(New SqlParameter("@QSValue", IIf(txtQuickSearch.Text.ToLower.IndexOf(" or ") > -1, txtQuickSearch.Text.Trim, txtQuickSearch.Text.Replace(" ", " AND ").Trim)))
                Else
                    'sqlCmd.Parameters.Add(New SqlParameter("@QSValue", IIf(txtQuickSearch.Text.Trim = "*", "THISWILLNOTMATCHEVERCAUSEALEXSAIDSO", IIf(txtQuickSearch.Text.ToLower.IndexOf(" or ") > -1, txtQuickSearch.Text, txtQuickSearch.Text.Replace(" ", " AND ")))))
                    sqlCmd.Parameters.Add(New SqlParameter("@QSValue", txtQuickSearch.Text.Replace("*", "%").Trim))
                End If

                sqlCmd.Parameters.Add(New SqlParameter("@QSType", ddlQuickSearchType.SelectedValue))
                sqlCmd.Parameters.Add(New SqlParameter("@DiffFromGMT", Me.mobjUser.TimeDiffFromGMT))

                If ddlQuickSearchType.SelectedValue = 1 And IsNumeric(txtQuickSearch.Text.Trim) Then
                    If txtQuickSearch.Text.Trim > 2147483647 Then
                        'SQL overflow error. Users are searching fro values with the wrong mode selected.
                        'Assume this is a document search and update the UI.
                        ddlQuickSearchType.SelectedValue = 0
                        'ReturnControls.Add(ddlQuickSearchType)
                        sqlCmd.Parameters.Remove(sqlCmd.Parameters.Item("@QSType"))
                        sqlCmd.Parameters.Add(New SqlParameter("@QSType", ddlQuickSearchType.SelectedValue))
                    End If
                End If

                If SortDirection.Value.Trim <> "" Then sqlCmd.Parameters.Add(New SqlParameter("@SortDirection", SortDirection.Value))
                If SortAttributeId.Value.Trim <> "" Then sqlCmd.Parameters.Add(New SqlParameter("@SortValue", SortAttributeId.Value))

                Dim prmMaxAttributes As New SqlClient.SqlParameter
                prmMaxAttributes.ParameterName = "@ResultCount"
                prmMaxAttributes.Direction = ParameterDirection.Output
                prmMaxAttributes.DbType = DbType.Int32
                sqlCmd.Parameters.Add(prmMaxAttributes)

                If ddlQuickSearchType.SelectedValue = 5 Then
                    Me.mcookieSearch.Keywords = txtQuickSearch.Text
                    sqlCmd.Parameters.Add(New SqlParameter("@FullTextTable", "##" & strFullText))
                Else
                    Me.mcookieSearch.Keywords = ""
                End If

                ds.Tables.Clear()
                sqlDa = New SqlDataAdapter(sqlCmd)
                sqlDa.Fill(ds)

                ResultCount = sqlCmd.Parameters("@ResultCount").Value

                If Me.mobjUser.DefaultSkipResult = 1 And ResultCount = 1 Then
                    System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.NewGuid.ToString, "LoadImage(" & ds.Tables(0).Rows(0)("VersionId") & ");", True)
                End If

            End If

            Dim endTime As DateTime = Now

            dgResults.DataSource = ds
            dgResults.DataBind()

            If ResultCount < lastRow Then lastRow = ResultCount

            If ResultCount > 0 Then

                SaveNextPreviousDocList(ResultCount)

                dgResults.DataSource = ds
                dgResults.DataBind()

                System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, "key", "document.all.NoResults.innerHTML = '';", True)
                Dim pages As Integer = Math.Ceiling(ResultCount / Me.mobjUser.DefaultResultCount)
                BuildPagination(pages)

                Dim dblSeconds As Double = Math.Round(endTime.Subtract(startTime).TotalMilliseconds() / 1000, 2)
                lblSummary.Text = "Results <b>" & firstRow & " - " & lastRow & "</b> of <b>" & ResultCount & "</b> (" & IIf(dblSeconds < 1, dblSeconds & " second)", dblSeconds & " seconds)")

                ShowMassIcons()

            Else
                System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, "key", "document.all.NoResults.innerHTML = '" & strNoResults & "';", True)
                lblPages.Text = ""
                lblSummary.Text = ""
                dgResults.DataSource = Nothing
                dgResults.DataBind()
                'HideSort()
                HideMassIcons()

            End If

        Catch ex As Exception

            If ex.Message <> "Thread was being aborted." Then
                ShowPopupError("An error has occured while performing your search. Please try again.", True)
                Dim conMaster As SqlClient.SqlConnection
                conMaster = Functions.BuildMasterConnection
                Functions.LogException(mobjUser.UserId.ToString, mobjUser.AccountId.ToString, "", "Quick Search Error, Search Type: " & ddlQuickSearchType.SelectedValue & " Value: " & txtQuickSearch.Text & " " & ex.Message, ex.StackTrace, Server.MachineName, "", Request.UrlReferrer.ToString, conMaster, True)
            End If

        Finally

            Session("SearchType") = "Quick"
            Session("CurrentPage") = curPage
            Session("QuickSearchValue") = txtQuickSearch.Text
            Session("QuickSearchType") = ddlQuickSearchType.SelectedValue
            Session("ResultCount") = ResultCount
            Session("DataSource") = ds

            HideBackButton()
            HideFolderHeading()
            HideRestoreButton()

            SearchType.Value = "Q"

            If Me.mconSqlImage.State <> ConnectionState.Closed Then
                Me.mconSqlImage.Close()
                Me.mconSqlImage.Dispose()
            End If

            System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, "quickSearch", "disableTree();resetTimeout();resizeElements();resultsMode();stopProgressDisplay();", True)

        End Try

    End Function

    Private Sub btnQuickSearch_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnQuickSearch.Click

        If txtQuickSearch.Text.Trim.Length > 0 Then
            SortDirection.Value = ""
            SortAttributeId.Value = ""
            'ReturnControls.Add(SortDirection)
            'ReturnControls.Add(SortAttributeId)
            QuickSearch()
        End If

    End Sub

    Private Sub AdvancedSearch(Optional ByVal dsReload As DataSet = Nothing)

        Dim sqlCmd As SqlClient.SqlCommand

        Dim curPage As Integer = CInt(CurrentPage.Value)
        Dim firstRow As Integer = (Me.mobjUser.DefaultResultCount * (curPage - 1)) + 1
        Dim lastRow As Integer = (Me.mobjUser.DefaultResultCount + firstRow) - 1

        Dim dtDocuments As New DataTable("Documents")
        Dim dtAttributes As New DataTable("Attributes")

        Dim ResultCount As Integer

        Dim startTime As DateTime

        Try

            recordSearchType(Functions.SearchType.ADVANCED)

            Me.mcookieSearch.Keywords = ""

            Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
            Me.mconSqlImage.Open()

            'Build Documents table
            Dim dr As DataRow
            Dim dc As DataColumn

            dtDocuments.Columns.Add(New DataColumn("DocumentId", SqlDbType.Int.GetType))

            If Not dsReload Is Nothing Then
                ResultCount = Session("ResultCount")
                ds = dsReload
                startTime = Now
            Else

                Dim strDocIds As String() = DocList.Value.Split(",")
                For Each strItem As String In strDocIds
                    If strItem <> "" Then

                        If IsNumeric(strItem) Then
                            dr = dtDocuments.NewRow
                            dr("DocumentId") = strItem
                            dtDocuments.Rows.Add(dr)
                        End If

                    End If
                Next

                'Perform DTSearch
                Dim mdtFullTextData As DataTable

                'If txtKeywords.Text.Trim.Length > 0 Then

                '    Dim IndexLocationDirectory As String
                '    IndexLocationDirectory = ConfigurationSettings.AppSettings("IndexLocation")

                '    If IndexLocationDirectory.TrimEnd.Substring(Len(IndexLocationDirectory) - 1, 1) <> "\" Then
                '        IndexLocationDirectory = IndexLocationDirectory.TrimEnd & "\"
                '    End If

                '    IndexLocationDirectory = IndexLocationDirectory & Me.mobjUser.DBName & "\"

                '    Dim sj As SearchJob = New SearchJob
                '    Dim results As SearchResults
                '    Dim i As Integer
                '    sj.IndexesToSearch = New System.Collections.Specialized.StringCollection
                '    sj.IndexesToSearch.Add(IndexLocationDirectory)
                '    sj.MaxFilesToRetrieve = ConfigurationSettings.AppSettings("DTSSearchMaxFiles")
                '    sj.TimeoutSeconds = ConfigurationSettings.AppSettings("DTSSearchTimeOutSecs")
                '    sj.Request = IIf(txtKeywords.Text.Trim = "*", "THISWILLNOTMATCHEVERCAUSEALEXSAIDSO", IIf(txtKeywords.Text.ToLower.IndexOf(" or ") > -1, txtKeywords.Text, txtKeywords.Text.Replace(" ", " AND ")))

                '    Dim sr As New SearchResults

                '    mdtFullTextData = New DataTable("dtSearchData")
                '    mdtFullTextData.Columns.Add(New DataColumn("ImageDetID", GetType(System.Int32), Nothing, MappingType.Element))
                '    mdtFullTextData.Columns.Add(New DataColumn("Score", GetType(System.Int32), Nothing, MappingType.Element))
                '    mdtFullTextData.Columns.Add(New DataColumn("ScorePercent", GetType(System.Int32), Nothing, MappingType.Element))
                '    mdtFullTextData.Columns.Add(New DataColumn("HitCount", GetType(System.Int32), Nothing, MappingType.Element))

                '    sj.Execute()
                '    results = sj.Results

                '    If results.Count > 0 Then 'build datatable
                '        For i = 0 To results.Count - 1
                '            results.GetNthDoc(i)
                '            Dim drFullText As DataRow
                '            drFullText = mdtFullTextData.NewRow

                '            If IsNothing(results.CurrentItem.UserFields) Then

                '                If results.CurrentItem.Filename.ToString.IndexOf(">") > 0 Then
                '                    Dim ImageDetId As String = System.IO.Path.GetFileName(results.CurrentItem.Filename.Replace(results.CurrentItem.Filename.Substring(results.CurrentItem.Filename.IndexOf(">")), ""))
                '                    If IsNumeric(ImageDetId) Then drFullText("ImageDetID") = System.IO.Path.GetFileName(ImageDetId)
                '                Else
                '                    If IsNumeric(System.IO.Path.GetFileName(results.CurrentItem.Filename)) Then drFullText("ImageDetID") = System.IO.Path.GetFileName(results.CurrentItem.Filename)
                '                End If
                '            Else
                '                drFullText("ImageDetID") = results.CurrentItem.UserFields("ImageDetID")
                '            End If

                '            drFullText("Score") = results.CurrentItem.Score
                '            drFullText("ScorePercent") = results.CurrentItem.ScorePercent
                '            drFullText("HitCount") = results.CurrentItem.HitCount
                '            mdtFullTextData.Rows.Add(drFullText)
                '        Next
                '    End If

                'End If

                'Build temp search tables
                Dim strDoc As String = System.Guid.NewGuid().ToString().Replace("-", "")
                Dim strAttributes As String = System.Guid.NewGuid().ToString().Replace("-", "")

                Dim sb As New System.Text.StringBuilder
                sb.Append("CREATE TABLE ##" & strDoc.ToString & " (DocumentId int)" & vbCrLf)
                sb.Append("CREATE TABLE ##" & strAttributes & " (AttributeId int, AttributeValue varchar(75), SearchType varchar(2), AttributeValueEnd varchar(75))" & vbCrLf)

                sqlCmd = New SqlCommand(sb.ToString, Me.mconSqlImage)
                Dim sqlDa As SqlDataAdapter
                Dim sqlBld As SqlCommandBuilder
                sqlCmd.ExecuteNonQuery()

                'Build Attributes Table
                If Not dgSearchAttributes Is Nothing Then
                    dtAttributes.Columns.Add(New DataColumn("AttributeId", SqlDbType.Int.GetType))
                    dtAttributes.Columns.Add(New DataColumn("AttributeValue", Type.GetType("System.String")))
                    dtAttributes.Columns.Add(New DataColumn("AttributeValueEnd", Type.GetType("System.String")))
                    dtAttributes.Columns.Add(New DataColumn("SearchType", Type.GetType("System.String")))
                    'dsAttributes.Tables.Add(dtAttributes)
                    '
                    ' Loop through each row
                    '
                    Dim dgRow As DataGridItem
                    For Each dgRow In dgSearchAttributes.Items
                        '
                        ' Get the row controls
                        Dim btnCalendar As Button = CType(dgRow.FindControl("btnCalendar"), Button)
                        Dim lblAttributeId As Label = CType(dgRow.FindControl("lblAttributeId"), Label)
                        Dim lblAttributeDataType As Label = CType(dgRow.FindControl("lblAttributeDataType"), Label)
                        Dim lblLength As Label = CType(dgRow.FindControl("lblLength"), Label)
                        Dim txtAttributeValue As TextBox = CType(dgRow.FindControl("txtAttributeValue"), TextBox)
                        Dim ddlAttributeList As DropDownList = CType(dgRow.FindControl("ddlAttributeList"), DropDownList)
                        Dim chkBox As CheckBox = CType(dgRow.FindControl("chkBox"), CheckBox)
                        Dim ddlSearchType As DropDownList = CType(dgRow.FindControl("ddlSearchType"), DropDownList)

                        Dim intAttributeId As Integer = lblAttributeId.Text

                        Select Case lblAttributeDataType.Text
                            'Case "CheckBox"
                            '    If chkBox.Checked Then
                            '        dr = dtAttributes.NewRow
                            '        dr("AttributeId") = intAttributeId
                            '        dr("AttributeValue") = 1
                            '        dr("AttributeValueEnd") = ""
                            '        dtAttributes.Rows.Add(dr)
                            '    End If
                            'Case "ListView"
                            '    If ddlAttributeList.SelectedValue > -1 Then
                            '        dr = dtAttributes.NewRow
                            '        dr("AttributeId") = intAttributeId
                            '        dr("AttributeValue") = ddlAttributeList.SelectedValue
                            '        dr("AttributeValueEnd") = ""
                            '        dtAttributes.Rows.Add(dr)
                            '    End If
                            Case "Date"

                                Select Case ddlSearchType.SelectedValue

                                    Case 5 'Between


                                        Dim txtBetweenFrom As TextBox = CType(dgRow.FindControl("txtBetweenFrom"), TextBox)
                                        Dim txtBetweenTo As TextBox = CType(dgRow.FindControl("txtBetweenTo"), TextBox)

                                        If txtBetweenFrom.Text.Trim.Length > 0 And txtBetweenTo.Text.Trim.Length > 0 Then

                                            Try
                                                dr = dtAttributes.NewRow
                                                dr("AttributeId") = intAttributeId
                                                dr("SearchType") = "BW"

                                                Dim dateFrom As Date = CDate(txtBetweenFrom.Text)
                                                Dim strFrom As String = dateFrom.Year.ToString & "/" & dateFrom.Month & "/" & dateFrom.Day
                                                strFrom = Format(CDate(strFrom), "yyyy/MM/dd")
                                                dr("AttributeValue") = strFrom

                                                Dim dateTo As Date = CDate(txtBetweenTo.Text)
                                                Dim strTo As String = dateTo.Year.ToString & "/" & dateTo.Month & "/" & dateTo.Day
                                                strTo = Format(CDate(strTo), "yyyy/MM/dd")

                                                dr("AttributeValueEnd") = strTo

                                                dr("AttributeValue") = dr("AttributeValue").Replace("*", "%")
                                                dtAttributes.Rows.Add(dr)
                                            Catch ex As Exception

                                            End Try

                                        End If

                                    Case Else
                                        Dim sSearch As String = txtAttributeValue.Text '.Replace("*", "%")
                                        Dim sSearchValues As String() = Split(sSearch, ";")
                                        For Each sSearchValue As String In sSearchValues
                                            'If IsDate(sSearchValue) Then
                                            '    dr = dtAttributes.NewRow
                                            '    Dim dateValue As DateTime = sSearchValue
                                            '    dr("AttributeId") = intAttributeId
                                            '    dr("AttributeValue") = dateValue.ToString("yyyy/MM/dd")
                                            '    dr("AttributeValueEnd") = ""
                                            '    dtAttributes.Rows.Add(dr)
                                            'ElseIf sSearchValue.IndexOf("%") >= 0 Then
                                            '    dr = dtAttributes.NewRow
                                            '    dr("AttributeId") = intAttributeId
                                            '    dr("AttributeValue") = sSearchValue
                                            '    dr("AttributeValueEnd") = ""
                                            '    dtAttributes.Rows.Add(dr)
                                            'End If
                                            If sSearchValue.Trim <> "" Then
                                                'Dim dateValue As DateTime = sSearchValue
                                                dr = dtAttributes.NewRow
                                                dr("AttributeId") = intAttributeId

                                                Try
                                                    Dim dateSearch As Date = CDate(sSearchValue)
                                                    sSearchValue = dateSearch.Year.ToString & "/" & dateSearch.Month & "/" & dateSearch.Day
                                                    sSearchValue = Format(CDate(sSearchValue), "yyyy/MM/dd")
                                                Catch ex As Exception

                                                End Try

                                                Select Case ddlSearchType.SelectedValue
                                                    Case 0 'Equals
                                                        dr("AttributeValue") = sSearchValue
                                                        dr("AttributeValueEnd") = ""
                                                        dr("SearchType") = "EQ"
                                                    Case 1 'Starts with
                                                        dr("AttributeValue") = sSearchValue & "*"
                                                        dr("AttributeValueEnd") = ""
                                                        dr("SearchType") = "WC"
                                                    Case 2 'Contains
                                                        dr("AttributeValue") = "*" & sSearchValue & "*"
                                                        dr("AttributeValueEnd") = ""
                                                        dr("SearchType") = "WC"
                                                    Case 3 'Greater than
                                                        dr("AttributeValue") = sSearchValue
                                                        dr("AttributeValueEnd") = ""
                                                        dr("SearchType") = "GT"
                                                    Case 4 'Less than
                                                        dr("AttributeValue") = sSearchValue
                                                        dr("AttributeValueEnd") = ""
                                                        dr("SearchType") = "LT"
                                                    Case 5 'Between
                                                        Dim txtBetweenFrom As TextBox = CType(dgRow.FindControl("txtBetweenFrom"), TextBox)
                                                        Dim txtBetweenTo As TextBox = CType(dgRow.FindControl("txtBetweenTo"), TextBox)
                                                        dr("SearchType") = "BW"

                                                        If txtBetweenFrom.Text.Trim.Length > 0 And txtBetweenTo.Text.Trim.Length > 0 Then

                                                            Try
                                                                Dim dateFrom As Date = CDate(txtBetweenFrom.Text)
                                                                Dim strFrom As String = dateFrom.Year.ToString & "/" & dateFrom.Month & "/" & dateFrom.Day
                                                                strFrom = Format(CDate(strFrom), "yyyy/MM/dd")
                                                                dr("AttributeValue") = strFrom

                                                                Dim dateTo As Date = CDate(txtBetweenTo.Text)
                                                                Dim strTo As String = dateTo.Year.ToString & "/" & dateTo.Month & "/" & dateTo.Day
                                                                strTo = Format(CDate(strTo), "yyyy/MM/dd")
                                                                dr("AttributeValueEnd") = strTo

                                                            Catch ex As Exception

                                                            End Try

                                                        End If

                                                End Select


                                                If sSearchValue.ToString.IndexOf("*") > -1 Then
                                                    dr("AttributeValue") = dr("AttributeValue")
                                                    dr("AttributeValueEnd") = ""
                                                    dr("SearchType") = "WC"
                                                End If

                                                If sSearchValue.ToString.IndexOf(">") > -1 Then
                                                    dr("AttributeValue") = dr("AttributeValue").Replace(">", "")
                                                    dr("AttributeValueEnd") = ""
                                                    dr("SearchType") = "GT"
                                                End If

                                                If sSearchValue.ToString.IndexOf("<") > -1 Then
                                                    dr("AttributeValue") = dr("AttributeValue").Replace("<", "")
                                                    dr("AttributeValueEnd") = ""
                                                    dr("SearchType") = "LT"
                                                End If

                                                dr("AttributeValue") = dr("AttributeValue").Replace("*", "%")
                                                dtAttributes.Rows.Add(dr)
                                            End If
                                        Next

                                End Select

                            Case Else
                                If txtAttributeValue.Text.Trim.Length > 0 Then
                                    'Modified to support multiple searches 
                                    'Add WildCard Support {*}
                                    Dim sSearch As String = txtAttributeValue.Text ' = txtAttributeValue.Text.Replace("*", "%")

                                    Dim sSearchValue As String() = Split(sSearch, ";")
                                    Dim i As Integer = 0

                                    For i = 0 To UBound(sSearchValue)
                                        dr = dtAttributes.NewRow
                                        dr("AttributeId") = intAttributeId

                                        Select Case ddlSearchType.SelectedValue
                                            Case 0 'Equals
                                                dr("AttributeValue") = sSearchValue(i)
                                                dr("AttributeValueEnd") = ""
                                                dr("SearchType") = "EQ"
                                            Case 1 'Starts with
                                                dr("AttributeValue") = sSearchValue(i) & "*"
                                                dr("AttributeValueEnd") = ""
                                                dr("SearchType") = "WC"
                                            Case 2 'Contains
                                                dr("AttributeValue") = "*" & sSearchValue(i) & "*"
                                                dr("AttributeValueEnd") = ""
                                                dr("SearchType") = "WC"
                                            Case 3 'Greater than
                                                dr("AttributeValue") = sSearchValue(i)
                                                dr("AttributeValueEnd") = ""
                                                dr("SearchType") = "GT"
                                            Case 4 'Less than
                                                dr("AttributeValue") = sSearchValue(i)
                                                dr("AttributeValueEnd") = ""
                                                dr("SearchType") = "LT"
                                            Case 5 'Between
                                                Dim txtBetweenFrom As TextBox = CType(dgRow.FindControl("txtBetweenFrom"), TextBox)
                                                Dim txtBetweenTo As TextBox = CType(dgRow.FindControl("txtBetweenTo"), TextBox)
                                                dr("SearchType") = "BW"
                                                dr("AttributeValue") = txtBetweenFrom.Text
                                                dr("AttributeValueEnd") = txtBetweenTo.Text
                                        End Select

                                        If sSearchValue(i).ToString.IndexOf("*") > -1 Then
                                            dr("AttributeValue") = sSearchValue(i)
                                            dr("AttributeValueEnd") = ""
                                            dr("SearchType") = "WC"
                                        End If

                                        If sSearchValue(i).ToString.IndexOf(">") > -1 Then
                                            dr("AttributeValue") = sSearchValue(i).Replace(">", "")
                                            dr("AttributeValueEnd") = ""
                                            dr("SearchType") = "GT"
                                        End If

                                        If sSearchValue(i).ToString.IndexOf("<") > -1 Then
                                            dr("AttributeValue") = sSearchValue(i).Replace("<", "")
                                            dr("AttributeValueEnd") = ""
                                            dr("SearchType") = "LT"
                                        End If

                                        dr("AttributeValue") = dr("AttributeValue").Replace("*", "%")
                                        dtAttributes.Rows.Add(dr)

                                    Next i
                                Else
                                    Dim txtBetweenFrom As TextBox = CType(dgRow.FindControl("txtBetweenFrom"), TextBox)
                                    Dim txtBetweenTo As TextBox = CType(dgRow.FindControl("txtBetweenTo"), TextBox)

                                    If txtBetweenFrom.Text.Trim.Length > 0 And txtBetweenTo.Text.Trim.Length > 0 Then

                                        Try
                                            'Dim dateFrom As Date = CDate(txtBetweenFrom.Text)
                                            'Dim strFrom As String = dateFrom.Year.ToString & "/" & dateFrom.Month & "/" & dateFrom.Day
                                            'strFrom = Format(CDate(strFrom), "yyyy/MM/dd")

                                            'Dim dateTo As Date = CDate(txtBetweenTo.Text)
                                            'Dim strTo As String = dateTo.Year.ToString & "/" & dateTo.Month & "/" & dateTo.Day
                                            'strTo = Format(CDate(strTo), "yyyy/MM/dd")

                                            dr = dtAttributes.NewRow
                                            dr("SearchType") = "BW"
                                            dr("AttributeId") = intAttributeId
                                            dr("AttributeValue") = txtBetweenFrom.Text
                                            dr("AttributeValueEnd") = txtBetweenTo.Text
                                            dtAttributes.Rows.Add(dr)
                                        Catch ex As Exception

                                        End Try

                                    End If
                                End If
                        End Select
                    Next

                End If

                'Add documents
                sqlCmd = New SqlCommand("SELECT * FROM ##" & strDoc, Me.mconSqlImage)
                sqlDa = New SqlDataAdapter(sqlCmd)
                sqlBld = New SqlCommandBuilder(sqlDa)

                If Not dsReload Is Nothing Then
                    For Each drDocs As DataRow In dsReload.Tables(0).Rows
                        Dim drNew As DataRow
                        drNew = dtDocuments.NewRow
                        drNew("DocumentId") = drDocs("DocumentId")
                        dtDocuments.Rows.Add(drNew)
                    Next
                End If

                sqlDa.Update(dtDocuments)

                'Add Attributes
                sqlCmd = New SqlCommand("SELECT * FROM ##" & strAttributes, Me.mconSqlImage)
                sqlDa = New SqlDataAdapter(sqlCmd)
                sqlBld = New SqlCommandBuilder(sqlDa)

                'If Not dsReload Is Nothing Then
                '    For Each drAtt As DataRow In dsReload.Tables(1).Rows
                '        Dim drNewAtt As DataRow
                '        drNewAtt = dtAttributes.NewRow
                '        drNewAtt("AttributeId") = drAtt("AttributeId")
                '        drNewAtt("AttributeValue") = drAtt("AttributeValue")
                '        drNewAtt("SearchType") = drAtt("SearchType")
                '        drNewAtt("AttributeValueEnd") = drAtt("AttributeValueEnd")
                '        dtAttributes.Rows.Add(drNewAtt)
                '    Next
                'End If

                sqlDa.Update(dtAttributes)

                Dim strFullText As String
                'Add global temp table for fulltext search results
                If Not mdtFullTextData Is Nothing Then
                    strFullText = System.Guid.NewGuid().ToString().Replace("-", "")
                    sb = New System.Text.StringBuilder
                    sb.Append("CREATE TABLE ##" & strFullText.ToString & " (ImageDetID int NULL, Score int NULL, ScorePercent int NULL, HitCount int NULL)" & vbCrLf)
                    sqlCmd = New SqlCommand(sb.ToString, Me.mconSqlImage)
                    sqlBld = New SqlCommandBuilder
                    sqlCmd.ExecuteNonQuery()
                    ' Add fulltext results
                    sqlCmd = New SqlCommand("SELECT ImageDetID, Score, ScorePercent, HitCount FROM ##" & strFullText, Me.mconSqlImage)
                    sqlDa = New SqlDataAdapter(sqlCmd)
                    sqlBld = New SqlCommandBuilder(sqlDa)
                    sqlDa.Update(CType(mdtFullTextData, DataTable))
                End If

                sqlCmd.Parameters.Clear()

                sqlCmd = New SqlCommand("SMAdvSearch", Me.mconSqlImage)
                sqlCmd.CommandTimeout = 180
                sqlCmd.CommandType = CommandType.StoredProcedure
                sqlCmd.Parameters.Add(New SqlParameter("@UserID", Me.mobjUser.ImagesUserId))
                sqlCmd.Parameters.Add(New SqlParameter("@BeginRow", firstRow))
                sqlCmd.Parameters.Add(New SqlParameter("@EndRow", lastRow))
                sqlCmd.Parameters.Add(New SqlParameter("@DocTable", "##" & strDoc))
                sqlCmd.Parameters.Add(New SqlParameter("@AttributeTable", "##" & strAttributes))
                sqlCmd.Parameters.Add(New SqlParameter("@DiffFromGMT", Me.mobjUser.TimeDiffFromGMT))
                'sqlCmd.Parameters.Add(New SqlParameter("@NoAttributeSearch", chkOrphans.Checked))
                'sqlCmd.Parameters.Add(New SqlParameter("@FullTextValue ", IIf(txtKeywords.Text.Trim = "*", "THISWILLNOTMATCHEVERCAUSEALEXSAIDSO", IIf(txtKeywords.Text.ToLower.IndexOf(" or ") > -1, txtKeywords.Text, txtKeywords.Text.Replace(" ", " AND ")))))

                If SortDirection.Value.Trim <> "" Then sqlCmd.Parameters.Add(New SqlParameter("@SortDirection", SortDirection.Value))
                If SortAttributeId.Value.Trim <> "" Then sqlCmd.Parameters.Add(New SqlParameter("@SortValue", SortAttributeId.Value))

                'Searching by folder
                If SearchFolderId.Value.Trim <> "" Then
                    sqlCmd.Parameters.Add(New SqlParameter("@FolderID", SearchFolderId.Value))
                    If chkSubfolders.Checked Then sqlCmd.Parameters.Add(New SqlParameter("@IncludeSubFolders", True))
                End If

                Dim prmMaxAttributes As New SqlClient.SqlParameter
                prmMaxAttributes.ParameterName = "@ResultCount"
                prmMaxAttributes.Direction = ParameterDirection.Output
                prmMaxAttributes.DbType = DbType.Int32
                sqlCmd.Parameters.Add(prmMaxAttributes)

                'If txtKeywords.Text.Trim.Length > 0 Then
                '    Me.mcookieSearch.Keywords = txtKeywords.Text
                '    sqlCmd.Parameters.Add(New SqlParameter("@FullTextTable", "##" & strFullText))
                'End If

                If ddlModifiedUser.SelectedValue <> "00000000-0000-0000-0000-000000000000" Then
                    sqlCmd.Parameters.Add(New SqlParameter("@ModifiedUser", ddlModifiedUser.SelectedValue))
                End If

                ds.Tables.Clear()
                sqlDa = New SqlDataAdapter(sqlCmd)

                startTime = Now

                sqlDa.Fill(ds)
                ResultCount = sqlCmd.Parameters("@ResultCount").Value

                If Me.mobjUser.DefaultSkipResult = 1 And ResultCount = 1 Then
                    System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.NewGuid.ToString, "LoadImage(" & ds.Tables(0).Rows(0)("VersionId") & ");", True)
                End If

            End If

            Dim endTime As DateTime = Now

            dgResults.DataSource = ds
            dgResults.DataBind()

            If ResultCount < lastRow Then lastRow = ResultCount

            If ResultCount > 0 Then

                SaveNextPreviousDocList(ResultCount)

                dgResults.DataSource = ds
                dgResults.DataBind()

                System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType(), "noResuts", "document.all.NoResults.innerHTML = '';", True)

                Dim pages As Integer = Math.Ceiling(ResultCount / Me.mobjUser.DefaultResultCount)
                BuildPagination(pages)

                Dim dblSeconds As Double = Math.Round(endTime.Subtract(startTime).TotalMilliseconds() / 1000, 2)
                lblSummary.Text = "Results <b>" & firstRow & " - " & lastRow & "</b> of <b>" & ResultCount & "</b> (" & IIf(dblSeconds < 1, dblSeconds & " second)", dblSeconds & " seconds)")

                ShowMassIcons()

            Else

                System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType(), "noResuts", "document.all.NoResults.innerHTML = '" & strNoResults & "';", True)
                lblPages.Text = ""
                lblSummary.Text = ""
                dgResults.DataSource = Nothing
                dgResults.DataBind()
                HideMassIcons()

            End If

            'Get search criteria
            Dim sbCriteria As New System.Text.StringBuilder
            Dim strCriteria As String

            sbCriteria.Append(SearchDocuments.Value)
            'sbCriteria.Remove(sbCriteria.ToString.LastIndexOf(", "), 1)
            If sbCriteria.ToString.IndexOfAny(", ") > -1 Then sbCriteria.Remove(sbCriteria.ToString.IndexOfAny(", "), 1)
            sbCriteria.Replace("AllDocuments", "All Documents")
            sbCriteria.Append(" - ")

            If dsReload Is Nothing Then

                'Loop through dg on search page
                For Each dgi As DataGridItem In dgSearchAttributes.Items
                    Dim ddlSearchType As DropDownList = CType(dgi.FindControl("ddlSearchType"), DropDownList)
                    Dim txtAttributeValue As TextBox = CType(dgi.FindControl("txtAttributeValue"), TextBox)
                    Dim txtBetweenFrom As TextBox = CType(dgi.FindControl("txtBetweenFrom"), TextBox)
                    Dim txtBetweenTo As TextBox = CType(dgi.FindControl("txtBetweenTo"), TextBox)
                    Dim lblAttributeId As Label = CType(dgi.FindControl("lblAttributeId"), Label)
                    Dim lblAttributeName As Label = CType(dgi.FindControl("lblAttribute"), Label)

                    Select Case ddlSearchType.SelectedValue
                        Case 0 'Equal
                            If txtAttributeValue.Text.Trim.Length > 0 Then
                                sbCriteria.Append(lblAttributeName.Text & " Equals (" & txtAttributeValue.Text & "), ")
                            End If
                        Case 1 'Starts with
                            If txtAttributeValue.Text.Trim.Length > 0 Then
                                sbCriteria.Append(lblAttributeName.Text & " Starts With (" & txtAttributeValue.Text & "), ")
                            End If
                        Case 2 'Contains
                            If txtAttributeValue.Text.Trim.Length > 0 Then
                                sbCriteria.Append(lblAttributeName.Text & " Contains (" & txtAttributeValue.Text & "), ")
                            End If
                        Case 5 'Bewtween
                            If txtBetweenFrom.Text.Trim.Length > 0 And txtBetweenTo.Text.Trim.Length Then
                                sbCriteria.Append(lblAttributeName.Text & " Between (" & txtBetweenFrom.Text & " and " & txtBetweenTo.Text & "), ")
                            End If
                    End Select
                Next

            Else

                'For Each drCriteria As DataRow In dsReload.Tables(1).Rows

                '    Dim cmdSql As New SqlClient.SqlCommand("SELECT AttributeName FROM Attributes WHERE AttributeId = @AttributeId")
                '    cmdSql.Parameters.Add("@AttributeId", drCriteria("AttributeId"))
                '    cmdSql.Connection = Me.mconSqlImage
                '    cmdSql.CommandType = CommandType.Text
                '    Dim strAttributeName As String = cmdSql.ExecuteScalar

                '    Select Case drCriteria("SearchType")
                '        Case "EQ" 'Equal
                '            sbCriteria.Append(strAttributeName & " Equals """ & drCriteria("AttributeValue") & """, ")
                '        Case "WC" 'Starts with, contains
                '            sbCriteria.Append(strAttributeName & " Starts With """ & drCriteria("AttributeValue") & """, ")
                '        Case "BW" 'Bewtween
                '            sbCriteria.Append(strAttributeName & " Between """ & drCriteria("AttributeValue") & """ and """ & drCriteria("AttributeValueEnd") & """, ")
                '    End Select

                'Next

            End If

            'If chkOrphans.Checked Then sbCriteria.Append("Un-indexed")

            strCriteria = sbCriteria.ToString
            If strCriteria.EndsWith(", ") Then
                strCriteria = strCriteria.Remove(strCriteria.LastIndexOf(", "), 1)
            End If

            If Not dsReload Is Nothing Then
                SetSearchCriteria(Session("SearchCriteria"))
            Else
                SetSearchCriteria(strCriteria.Replace("%", "*"))
                Session("SearchCriteria") = strCriteria.Replace("%", "*")
            End If

            'If intTotalResults = 1 Then
            '    Try

            '        If Me.mobjUser.DefaultSkipResult Then
            '            Dim intVersionId As Integer = ds.Tables(ResultTables.HEADER).Rows(0)("VersionId")
            '            Session("HideResultsButton") = True
            '            Response.Redirect("Index.aspx?VID=" & intVersionId)
            '        End If

            '    Catch ex As Exception

            '    End Try
            'End If

        Catch ex As Exception

            If ex.Message <> "Thread was being aborted." Then
                ShowPopupError("An error has occured while performing your search. Please try again.", True)
                Dim conMaster As SqlClient.SqlConnection
                conMaster = Functions.BuildMasterConnection
                Functions.LogException(mobjUser.UserId.ToString, mobjUser.AccountId.ToString, "", "Advanced Search Error: " & ex.Message, ex.StackTrace, Server.MachineName, "", Request.UrlReferrer.ToString, conMaster, True)
            End If

        Finally

            Session("SearchType") = "Advanced"
            Session("CurrentPage") = curPage
            Session("ResultCount") = ResultCount
            Session("DataSource") = ds

            SearchType.Value = "A"
            'ReturnControls.Add(SearchType)

            HideFolderHeading()
            HideRestoreButton()

            If IsNothing(dsReload) Then
                ShowBackButton(False)
                btnBack.Attributes("style") = "VISIBILITY: visible"
            Else
                HideBackButton()
                btnBack.Attributes("style") = "VISIBILITY: hidden"
            End If

            If Me.mconSqlImage.State <> ConnectionState.Closed Then
                Me.mconSqlImage.Close()
                Me.mconSqlImage.Dispose()
            End If

            System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType(), "advSearch", "document.all.BackMethod.value='A';document.all.SearchType.value = 'A';document.all.btnBack.alt = 'Modify Search';resizeElements();resultsMode();stopProgressDisplay();resetTimeout();", True)

        End Try

    End Sub

    Private Sub lnkSearch_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles lnkSearch.Click

        SortDirection.Value = ""
        SortAttributeId.Value = ""
        'ReturnControls.Add(SortDirection)
        'ReturnControls.Add(SortAttributeId)

        Try
            ' Validate the search criteria
            Dim bolError As Boolean = False
            Dim blnAttributeFound As Boolean = False
            Dim blnBetweenError As Boolean = False

            ' Define Grid Controls
            Dim dgRow As DataGridItem
            'Dim btnCalendar As Button
            Dim lblAttributeId As Label
            Dim lblAttributeDataType As Label
            Dim lblDataFormat As Label
            Dim lblLength As Label
            Dim txtAttributeValue As TextBox
            Dim ddlAttributeList As DropDownList
            Dim chkBox As CheckBox
            Dim cv As Label
            Dim ddlSearchType As DropDownList
            Dim blnIsEmpty As Boolean = True
            Dim txtBetweenFrom As TextBox
            Dim txtBetweenTo As TextBox
            'Dim pnlValue As Panel

            Dim intAttributeId As Integer
            Dim intLength As Integer
            Dim strAttributeValue As String
            Dim strDataFormat As String
            Dim strDataType As String

            For Each dgRow In dgSearchAttributes.Items
                ' Get the row controls
                'pnlValue = CType(dgRow.FindControl("pnlValue"), Panel)

                'btnCalendar = CType(dgRow.FindControl("btnCalendar"), Button)
                lblAttributeId = CType(dgRow.FindControl("lblAttributeId"), Label)
                lblAttributeDataType = CType(dgRow.FindControl("lblAttributeDataType"), Label)
                lblDataFormat = CType(dgRow.FindControl("lblDataFormat"), Label)
                lblLength = CType(dgRow.FindControl("lblLength"), Label)
                txtAttributeValue = CType(dgRow.FindControl("txtAttributeValue"), TextBox)
                ddlAttributeList = CType(dgRow.FindControl("ddlAttributeList"), DropDownList)
                chkBox = CType(dgRow.FindControl("chkBox"), CheckBox)
                ddlSearchType = CType(dgRow.FindControl("ddlSearchType"), DropDownList)
                txtBetweenFrom = CType(dgRow.FindControl("txtBetweenFrom"), TextBox)
                txtBetweenTo = CType(dgRow.FindControl("txtBetweenTo"), TextBox)
                cv = CType(dgRow.FindControl("lblRowError"), Label)
                cv.Text = ""
                cv.Visible = False

                If txtAttributeValue.Text.Trim.Length > 0 Or ddlModifiedUser.SelectedValue <> System.Guid.Empty.ToString _
                Or ddlAttributeList.SelectedValue <> "" Or chkBox.Checked = True Then
                    blnIsEmpty = False
                End If

                'Just check if something is populated in any attribute now, no formatting
                Select Case ddlSearchType.SelectedValue
                    Case 5 'Between

                        If (txtBetweenFrom.Text.Trim.Length > 0 And txtBetweenTo.Text.Trim.Length > 0) Then blnAttributeFound = True

                        If txtBetweenTo.Text.Trim.Length = 0 Then
                            cv.Text = "Please enter an ending search range."
                            cv.Visible = True
                            blnBetweenError = True
                        End If

                        If txtBetweenFrom.Text.Trim.Length = 0 Then
                            cv.Text = "Please enter a beginning search range."
                            cv.Visible = True
                            blnBetweenError = True
                        End If

                    Case Else 'String attributes

                        If txtAttributeValue.Text.Trim.Length > 0 Then
                            blnAttributeFound = True
                        End If

                End Select

            Next

            If blnAttributeFound Then
                AdvancedSearch()
            Else
                lblError.Text = "Please enter attributes to search."
                lblError.Visible = True
            End If

        Catch ex As Exception

        Finally
            System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType(), "stop", "stopProgressDisplay();", True)
        End Try

    End Sub

    Private Sub ShowBackButton(ByVal UpButton As Boolean)

        If UpButton Then
            btnBack.Src = "Images/up.btn.gif"
            btnBack.Visible = True
            'ReturnControls.Add(btnBack)
        Else
            btnBack.Src = "Images/back.btn.gif"
            btnBack.Visible = True
            'ReturnControls.Add(btnBack)
        End If

    End Sub

    Private Sub HideBackButton()
        btnBack.Visible = False
        'ReturnControls.Add(btnBack)
    End Sub

    Private Sub LoadModifiedUsers()

        Dim conSqlWeb As New SqlClient.SqlConnection

        Try

            conSqlWeb = Functions.BuildConnection(Me.mobjUser)

            Dim cmdSql As New SqlClient.SqlCommand("SELECT * FROM ViewWebUsers")
            cmdSql.CommandType = CommandType.Text
            cmdSql.Connection = conSqlWeb

            Dim dtUsers As New DataTable("Users")
            Dim da As New SqlClient.SqlDataAdapter(cmdSql)
            da.Fill(dtUsers)

            dtUsers.Columns.Add(New DataColumn("Name", GetType(System.String), "FirstName" & "+ ' ' +" & "LastName" & "+ ' [' +" & "EMailAddress" & "+ ']'", MappingType.Element))
            Dim dv As New DataView(dtUsers)
            dv.Sort = "LastName"

            ddlModifiedUser.DataSource = dv
            ddlModifiedUser.DataTextField = "Name"
            ddlModifiedUser.DataValueField = "UserId"
            ddlModifiedUser.DataBind()

            Dim ddItem As New ListItem("Select a user...", System.Guid.Empty.ToString)
            ddItem.Selected = True

            ddlModifiedUser.Items.Insert(0, ddItem)
            ddlModifiedUser.SelectedIndex = 0

        Catch ex As Exception

        Finally

            If conSqlWeb.State <> ConnectionState.Closed Then
                conSqlWeb.Close()
                conSqlWeb.Dispose()
            End If

        End Try

    End Sub

    Private Sub btnBrowseFolder_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBrowseFolder.Click
        SortDirection.Value = ""
        SortAttributeId.Value = ""
        BrowseTreeFolder(BrowseFolder.Value)
        Exit Sub
    End Sub

    Private Sub HideFolderHeading()
        System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.NewGuid.ToString, "document.all.FolderHeading.style.display = 'none';", True)
    End Sub

    Private Sub SetFolderHeading(ByVal Name As String, ByVal path As String)
        FolderHeading.InnerText = Name.Replace("&#43;", "+")
        FolderHeading.Attributes("style") = "DISPLAY: inline; TOOLTIP: " & path
    End Sub

    Private Function SortResults(ByVal dsResults As DataSet) As DataView

        Try

            'Build table
            Dim dtResults As New DataTable
            If dtResults.Rows.Count > 0 Then dtResults.Rows.Clear()
            If dtResults.Columns.Count > 0 Then dtResults.Columns.Clear()

            Dim dvSort As New DataView(dsResults.Tables(ResultTables.HEADER))

            'Dataview is already sorted, chunk out the rows we want
            Dim intRecPerPg As Integer = Me.mobjUser.DefaultResultCount
            Dim intPageNumber As Integer = PageNo.Value
            Dim intTotalPages As Integer = System.Math.Ceiling(dvSort.Count / Me.mobjUser.DefaultResultCount)

            If intPageNumber > intTotalPages Then
                PageNo.Value = 1
                intPageNumber = 1
            End If

            Dim intBeginning As Integer = (((intPageNumber * intRecPerPg) - intRecPerPg) + 1) - 1
            Dim intEnding As Integer = (intBeginning + intRecPerPg) - 1

            Beginning = intBeginning + 1
            Ending = intEnding + 1

            lblPages.Text = BuildPagination(intTotalPages)
            'ajaxManager.AjaxSettings.AddAjaxSetting(sender, lblPages)
            Session("ResultPages") = lblPages.Text

            dvSort.RowFilter = "RowId >= " & intBeginning & " AND RowId <= " & intEnding

            Dim CurrentVersionId As Integer

            Try

                Dim dtResultsTable As New DataTable
                dtResultsTable.Columns.Add(New DataColumn("RowId", GetType(System.Int32), Nothing, MappingType.Element))
                Dim dcVersionId As DataColumn
                dcVersionId = New DataColumn("VersionId", GetType(System.Int32), Nothing, MappingType.Element)
                dcVersionId.Unique = True
                dtResultsTable.Columns.Add(dcVersionId)

                Dim intRowId As Integer = 0

                Dim Row As Integer = 0
                For intCount As Integer = 0 To dvSort.Count - 1
                    If dvSort(intCount)("ImageType") <> ImageTypes.FOLDER Then
                        Dim dr As DataRow = dtResultsTable.NewRow
                        dr("RowId") = Row
                        CurrentVersionId = dvSort(intCount)("VersionId")
                        dr("VersionId") = dvSort(intCount)("VersionId")
                        dtResultsTable.Rows.Add(dr)
                        Row += 1
                    End If
                Next

                Session("dtVersionId") = dtResultsTable
                Session("intTotalDocuments") = dtResultsTable.Rows.Count



            Catch ex As Exception

            End Try

            Return dvSort

        Catch ex As Exception

        Finally

            'ReturnControls.Add(dgResults)
            'ReturnControls.Add(lblPages)

            Session("SearchPage") = PageNo.Value

        End Try

    End Function

    Private Function BuildPagination(ByVal intPageCount As Integer) As String
        Dim sbPages As New System.Text.StringBuilder

        If intPageCount = 1 Then
            lblPages.Text = ""
            'ReturnControls.Add(lblPages)
            Exit Function
        End If

        'Get current page
        Dim intCurrentPage As Integer = CurrentPage.Value

        'Number of pages to display on each side of the current page
        Dim intBuffer As Integer = 3

        sbPages.Append("Page:  ")

        If intPageCount < intBuffer Then
            For x As Integer = 1 To intPageCount
                If x = intCurrentPage Then
                    'sbPages.Append("<a style=""FONT-SIZE: 8pt; COLOR: black; FONT-FAMILY: verdana"" href=""#"" onclick=""LoadPage('" & x & "')"">" & "<b>" & x & "</b>" & "</a>" & Space(2))
                    sbPages.Append("<b>" & intCurrentPage & "</b>" & Space(2))
                Else
                    'sbPages.Append("<a style=""FONT-SIZE: 8pt; COLOR: black; FONT-FAMILY: verdana"" href=""#"" onclick=""LoadPage('" & x & "')"">" & x & "</a>" & Space(2))
                    sbPages.Append("<a style=""FONT-SIZE: 8pt; COLOR: black; FONT-FAMILY: verdana"" href=""javascript:LoadPage('" & x & "')"">" & x & "</a>" & Space(2))
                End If
            Next
            lblPages.Text = sbPages.ToString
            'ReturnControls.Add(lblPages)
            Exit Function
        End If

        'Previous page first to see if we need First and Last links
        If (intCurrentPage - intBuffer) < 1 Then
            'We cant go back the buffer size because we will go negative
            'Lets draw back all we can
            For X As Integer = 1 To intCurrentPage - 1
                'sbPages.Append("<a style=""FONT-SIZE: 8pt; COLOR: black; FONT-FAMILY: verdana"" href=""#"" onclick=""LoadPage('" & x & "')"">" & x & "</a>" & Space(2))
                sbPages.Append("<a style=""FONT-SIZE: 8pt; COLOR: black; FONT-FAMILY: verdana"" href=""javascript:LoadPage('" & X & "')"">" & X & "</a>" & Space(2))

            Next
        Else
            If intCurrentPage - intBuffer > 1 Then sbPages.Append("<a style=""FONT-SIZE: 8pt; COLOR: black; FONT-FAMILY: verdana"" href=""#"" onclick=""LoadPage('" & "1" & "')"">" & "1..." & "</a>" & Space(2))
            For x As Integer = intCurrentPage - intBuffer To intCurrentPage - 1
                'sbPages.Append("<a style=""FONT-SIZE: 8pt; COLOR: black; FONT-FAMILY: verdana"" href=""#"" onclick=""LoadPage('" & x & "')"">" & x & "</a>" & Space(2))
                sbPages.Append("<a style=""FONT-SIZE: 8pt; COLOR: black; FONT-FAMILY: verdana"" href=""javascript:LoadPage('" & x & "')"">" & x & "</a>" & Space(2))
            Next
        End If

        'Current page with no hyperlink
        sbPages.Append("<b>" & intCurrentPage & "</b>" & Space(2))

        'Next pages to see if we need Last links
        If (intCurrentPage + intBuffer) > intPageCount Then
            'We cant go back the buffer size because we will go negative
            'Lets draw back all we can
            For X As Integer = intCurrentPage + 1 To intPageCount '- 1
                'sbPages.Append("<a style=""FONT-SIZE: 8pt; COLOR: black; FONT-FAMILY: verdana"" href=""#"" onclick=""LoadPage('" & x & "')"">" & x & "</a>" & Space(2))
                sbPages.Append("<a style=""FONT-SIZE: 8pt; COLOR: black; FONT-FAMILY: verdana"" href=""javascript:LoadPage('" & X & "')"">" & X & "</a>" & Space(2))
            Next
        Else
            For X As Integer = intCurrentPage + 1 To intCurrentPage + intBuffer
                'sbPages.Append("<a style=""FONT-SIZE: 8pt; COLOR: black; FONT-FAMILY: verdana"" href=""#"" onclick=""LoadPage('" & x & "')"">" & x & "</a>" & Space(2))
                sbPages.Append("<a style=""FONT-SIZE: 8pt; COLOR: black; FONT-FAMILY: verdana"" href=""javascript:LoadPage('" & X & "')"">" & X & "</a>" & Space(2))
            Next
            If intCurrentPage + intBuffer < intPageCount Then sbPages.Append("<a style=""FONT-SIZE: 8pt; COLOR: black; FONT-FAMILY: verdana"" href=""#"" onclick=""LoadPage('" & intPageCount & "')"">" & " ..." & intPageCount & "</a>" & Space(2))
        End If

        lblPages.Text = sbPages.ToString
        'ReturnControls.Add(lblPages)

    End Function

    Private Sub btnAddFiles_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddFiles.Click

        Try
            If NodeId.Value.StartsWith("S") Then Exit Sub
            System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.NewGuid.ToString, "addMode('" & NodeId.Value & "');resetTimeout();", True)
        Catch ex As Exception

        End Try

    End Sub

    Private Sub dgResults_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgResults.ItemCommand

        Dim VersionId As Integer
        Dim ImageType As Label = CType(e.Item.FindControl("ImageType"), Label)

        Try

            Select Case e.CommandName

                Case "Download"

                    Select Case ImageType.Text

                        Case ImageTypes.ATTACHMENT

                            VersionId = CInt(CType(e.Item.FindControl("VersionId"), Label).Text)

                            'Create file and push it to the user
                            Dim objAttachments As New FileAttachments(Functions.BuildConnection(Me.mobjUser))

                            Dim dt As New DataTable
                            dt = objAttachments.GetFile(VersionId)

                            Dim imgByte As Byte() = CType(dt.Rows(0)("Data"), Byte())

                            Response.Clear()

                            Dim fileName As String = CType(e.Item.FindControl("'lnkFile"), LinkButton).Text
                            Response.AddHeader("Content-Disposition", "attachment; filename=" & Server.UrlEncode(fileName).Replace("+", "%20"))
                            Response.AddHeader("Content-Length", imgByte.Length)

                            If fileName.ToLower.EndsWith(".pdf") Then
                                Response.ContentType = "application/pdf"
                            Else
                                Response.ContentType = "application/octet-stream"
                            End If

                            Response.BinaryWrite(imgByte)
                            Response.End()

                        Case ImageTypes.SCAN

                            VersionId = CInt(CType(e.Item.FindControl("VersionId"), Label).Text)

                            Dim file As String = Server.MapPath("./tmp/" & Functions.ExportPDF(Me.mobjUser, VersionId, 1, 10000, Me, False))

                            Response.Clear()

                            Dim fileName As String = CType(e.Item.FindControl("'lnkFile"), LinkButton).Text
                            Response.AddHeader("Content-Disposition", "attachment; filename=docAssist.pdf")
                            Response.AddHeader("Content-Length", New System.IO.FileInfo(file).Length)
                            Response.ContentType = "application/pdf"
                            Response.WriteFile(file)
                            Response.End()

                    End Select

                    'Track attachment
                    Try
                        Accucentric.docAssist.Web.Data.Functions.TrackPageRequest(Me.mobjUser.UserId, Me.mobjUser.AccountId, "Attachment View", "Out", VersionId, "", Functions.GetMasterConnectionString)
                    Catch ex As Exception

                    End Try

                    'Log recent item
                    Try
                        Dim blnReturn As Boolean = Functions.TrackRecentItem(Me.mobjUser, "VW", VersionId)
                    Catch ex As Exception

                    End Try

            End Select

        Catch ex As Exception

            If ex.Message <> "Thread was being aborted." Then

                Dim conMaster As SqlClient.SqlConnection
                conMaster = Functions.BuildMasterConnection
                Functions.LogException(mobjUser.UserId.ToString, mobjUser.AccountId.ToString, "", "Result File Download Error, VersionId: " & VersionId & " " & ex.Message, ex.StackTrace, Server.MachineName, "", Request.UrlReferrer.ToString, conMaster, True)

                RegisterStartupScript("DownloadError", "<script>showInfoBox('<b>An error occured while downloading this file. Please refresh the page and try again.</b><br><br>Our staff has been notified of the error and is working to correct any possible issues.');</script>")

            End If

        Finally

        End Try

    End Sub

    Private Sub HideRestoreButton()
        System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.NewGuid.ToString, "document.getElementById('divRestore').style.visibility = 'hidden';", True)
    End Sub

    Private Sub ShowRestoreButton()
        System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.NewGuid.ToString, "document.getElementById('divRestore').style.visibility = 'visible';", True)
    End Sub

    Private Sub ShowMassIcons()
        System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.NewGuid.ToString, "document.all.MassIcons.style.visibility='visible';", True)
    End Sub

    Private Sub HideMassIcons()
        System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.NewGuid.ToString, "document.all.MassIcons.style.visibility='hidden';", True)
    End Sub

    Private Sub SetSearchCriteria(ByVal Criteria As String)

        Try
            Criteria = Criteria.Replace("'", "\'")
            System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.NewGuid.ToString, "document.all.SearchCriteria.innerText='" & Criteria.Replace("%", "*") & "';document.all.SearchCriteria.onmouseover = function(event) { Tip('" & Criteria.Replace("%", "*") & "'); };", True)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub btnCabinetSort_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCabinetSort.Click

        Try
            Dim strShareName As String

            Dim dtBegin As DateTime = Now

            Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
            Dim objFolders As New Folders(Me.mconSqlImage, False)

            Dim intParentFolderId As Integer
            'intMaxAttributeCount = objFolders.BrowseShare(Me.mobjUser.ImagesUserId, NodeId.Value.Replace("S", ""), ds, strShareName)
            intMaxAttributeCount = objFolders.BrowseShare(Me.mobjUser.ImagesUserId, NodeId.Value.Replace("S", ""), ds, strShareName, Sort.Value, SortDirection.Value)

            Dim intTotalResults As Integer = ds.Tables(0).Rows.Count

            Dim dv As New DataView
            dv = SortResults(ds)

            dgResults.DataSource = dv
            dgResults.DataBind()

        Catch ex As Exception

        Finally

            System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.NewGuid.ToString, "document.all.SearchType.value='S';resultsMode();stopProgressDisplay();", True)

        End Try

    End Sub

    Private Sub dgResults_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgResults.PreRender

        Try

            'Create client side scripts for checkboxes
            Dim sbcheckAll As New System.Text.StringBuilder
            Dim sbuncheckAll As New System.Text.StringBuilder

            sbcheckAll.Append("function checkAll() {")
            sbuncheckAll.Append("function uncheckAll() {")

            For Each dgi As DataGridItem In dgResults.Items

                'Dim imgDownload As ImageButton = dgi.FindControl("imgDownload")



                Dim ResultRow As HtmlTable = dgi.FindControl("ResultRow")
                Dim chkMass As CheckBox = dgi.FindControl("chkMass")

                'Dim lnkFileNameStartup As LinkButton = dgi.FindControl("'lnkFileName")
                'TODO If Not IsNothing('lnkFileNameStartup) Then RegisterExcludeControl('lnkFileNameStartup)

                If chkMass.Visible Then
                    sbcheckAll.Append("document.getElementById('" & chkMass.ClientID & "').checked = true;")
                    sbcheckAll.Append("document.getElementById('" & ResultRow.ClientID & "').className = 'selected';")
                    sbuncheckAll.Append("document.getElementById('" & chkMass.ClientID & "').checked = false;")
                    sbuncheckAll.Append("document.getElementById('" & ResultRow.ClientID & "').className = 'ResultAttributes';")
                End If

            Next

            sbcheckAll.Append("}")
            sbuncheckAll.Append("}")

            System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType(), "checkAll", sbcheckAll.ToString, True)
            System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType(), "uncheckAll", sbuncheckAll.ToString, True)

        Catch ex As Exception

        Finally

        End Try

    End Sub

    Private Sub btnMassDelete_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnMassDelete.Click

        Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
        Me.mconSqlImage.Open()

        Dim blnItemsSelected As Boolean = False
        Dim Documents As DataTable
        Documents.Columns.Add("VersionId")

        Dim sqlCmd As SqlCommand
        Dim sqlDa As SqlDataAdapter
        Dim sqlBld As SqlCommandBuilder

        Dim items() As DataGridItem

        Try

            For Each dgi As DataGridItem In dgResults.Items

                Dim chkMass As CheckBox = dgi.FindControl("chkMass")
                Dim lblVersionId As Label = dgi.FindControl("VersionId")

                'Dim 'lnkFileNameStartup As LinkButton = dgi.FindControl("'lnkFile")
                'TODO If Not IsNothing('lnkFileNameStartup) Then RegisterExcludeControl('lnkFileNameStartup)

                If chkMass.Checked And IsNumeric(lblVersionId.Text) Then
                    blnItemsSelected = True
                    Dim dr As DataRow = Documents.NewRow
                    dr("VersionId") = lblVersionId.Text
                    Documents.Rows.Add(dr)
                End If

            Next

            If blnItemsSelected Then

                Dim tempTableName As String = "##" & System.Guid.NewGuid.ToString.Replace("-", "")
                Dim sb As New System.Text.StringBuilder
                sb.Append("CREATE TABLE " & tempTableName & " (VersionId int)" & vbCrLf)

                sqlCmd = New SqlCommand(sb.ToString, Me.mconSqlImage)
                sqlCmd.ExecuteNonQuery()

                sqlCmd = New SqlCommand("SELECT * FROM ##" & tempTableName, Me.mconSqlImage)
                sqlDa = New SqlDataAdapter(sqlCmd)
                sqlBld = New SqlCommandBuilder(sqlDa)

                sqlDa.Update(Documents)

            End If

            'Check if mass action icons should remain visible
            HideMassIcons()
            For Each dgi As DataGridItem In dgResults.Items
                Dim lblImageType As Label = dgi.FindControl("ImageType")
                If dgi.Visible Then
                    Select Case CType(lblImageType.Text, ImageTypes)
                        Case Constants.ImageTypes.SCAN
                            ShowMassIcons()
                            Exit For
                        Case Constants.ImageTypes.ATTACHMENT
                            ShowMassIcons()
                            Exit For
                    End Select
                End If
            Next

        Catch ex As Exception

            'trnSql.Rollback()

        Finally

            If Me.mconSqlImage.State <> ConnectionState.Closed Then
                Me.mconSqlImage.Close()
                Me.mconSqlImage.Dispose()
            End If

            System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.NewGuid.ToString, "stopProgressDisplay();", True)

        End Try

    End Sub

    Private Sub btnDeleteCheck_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDeleteCheck.Click

        Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
        Me.mconSqlImage.Open()

        Dim blnItemsSelected As Boolean = False
        Dim Documents As New DataTable
        Documents.Columns.Add("ImageId")

        Dim sqlCmd As SqlCommand
        Dim sqlDa As SqlDataAdapter
        Dim sqlBld As SqlCommandBuilder

        Dim items As New Collection

        Try

            For Each dgi As DataGridItem In dgResults.Items

                Dim chkMass As CheckBox = dgi.FindControl("chkMass")
                Dim lblImageId As Label = dgi.FindControl("DocNo")

                'Dim 'lnkFileNameStartup As LinkButton = dgi.FindControl("'lnkFile")
                'TODO If Not IsNothing('lnkFileNameStartup) Then 'RegisterExcludeControl('lnkFileNameStartup)

                Dim ImageId As Integer = lblImageId.Text.Replace("<b>", "").Replace("</b>", "")

                If chkMass.Checked And IsNumeric(ImageId) And dgi.Visible = True Then
                    blnItemsSelected = True
                    Dim dr As DataRow = Documents.NewRow
                    dr("ImageId") = ImageId
                    Documents.Rows.Add(dr)
                    items.Add(dgi)
                    chkMass.Checked = False
                End If

            Next

            If blnItemsSelected Then

                Dim tempTableName As String = "##" & System.Guid.NewGuid.ToString.Replace("-", "")
                sqlCmd = New SqlCommand("CREATE TABLE " & tempTableName & " (ImageId int)", Me.mconSqlImage)
                sqlCmd.ExecuteNonQuery()

                sqlCmd = New SqlCommand("SELECT * FROM " & tempTableName, Me.mconSqlImage)
                sqlDa = New SqlDataAdapter(sqlCmd)
                sqlBld = New SqlCommandBuilder(sqlDa)

                sqlDa.Update(Documents)

                sqlCmd = New SqlCommand("SMMassDelete", Me.mconSqlImage)
                sqlCmd.CommandType = CommandType.StoredProcedure
                sqlCmd.Parameters.Clear()
                sqlCmd.Parameters.Add("@DocListTable", tempTableName)
                sqlCmd.Parameters.Add("@UserID", Me.mobjUser.ImagesUserId)

                Dim Results As New DataTable
                sqlDa = New SqlDataAdapter(sqlCmd)
                sqlDa.Fill(Results)

                If Results.Rows.Count > 0 Then

                    'Errors deleting
                    Dim sb As New System.Text.StringBuilder
                    sb.Append("You do not have access to delete the following documents: ")
                    Dim count As Integer = 0
                    For Each dr As DataRow In Results.Rows
                        count += 1
                        sb.Append(dr("ImageId"))
                        If count <> Results.Rows.Count Then sb.Append(", ")
                    Next

                    ShowPopupError(sb.ToString, False)

                Else
                    'Success
                    btnUpdateRecent_Click(Nothing, Nothing)

                    For Each dgi As DataGridItem In items
                        dgi.Visible = False
                    Next

                    'Check if mass action icons should remain visible
                    HideMassIcons()
                    For Each dgi As DataGridItem In dgResults.Items
                        Dim lblImageType As Label = dgi.FindControl("ImageType")
                        If dgi.Visible Then
                            Select Case CType(lblImageType.Text, ImageTypes)
                                Case Constants.ImageTypes.SCAN
                                    ShowMassIcons()
                                    Exit For
                                Case Constants.ImageTypes.ATTACHMENT
                                    ShowMassIcons()
                                    Exit For
                            End Select
                        End If
                    Next

                    For Each dr As DataRow In Documents.Rows
                        Functions.UpdateCachedResults(dr("ImageId"), Me)
                    Next

                End If

            Else

                ShowPopupError("Please select one or more documents to delete.", False)
                Exit Sub

            End If

        Catch ex As Exception

            ShowPopupError("An error occured while deleting the selected documents.", True)

            Dim sb As New System.Text.StringBuilder
            For Each dr As DataRow In Documents.Rows
                sb.Append(dr("ImageId") & ", ")
            Next

            Dim conMaster As SqlClient.SqlConnection
            conMaster = Functions.BuildMasterConnection
            Functions.LogException(mobjUser.UserId.ToString, mobjUser.AccountId.ToString, "", "Mass Delete, ImageId's: " & sb.ToString & " " & ex.Message, ex.StackTrace, Server.MachineName, "", Request.UrlReferrer.ToString, conMaster, True)

        Finally

            If Me.mconSqlImage.State <> ConnectionState.Closed Then
                Me.mconSqlImage.Close()
                Me.mconSqlImage.Dispose()
            End If

            System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType(), "finish", "stopProgressDisplay();resultsMode();resizeElements();", True)

        End Try

    End Sub

    Private Sub btnDeleteUnselect_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDeleteUnselect.Click

        Try

            For Each gridItem As DataGridItem In dgResults.Items

                Dim chkMass As CheckBox = gridItem.FindControl("chkMass")

                If Not IsNothing(chkMass) Then

                    Dim Table4 As System.Web.UI.HtmlControls.HtmlTable = gridItem.FindControl("Table4")

                    If chkMass.Checked Then
                        Dim CanDeleteFile As Label = gridItem.FindControl("CanDeleteFile")

                        If CanDeleteFile.Text = "False" Then
                            System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.NewGuid.ToString, "document.getElementById('" & chkMass.ClientID & "').checked = false;document.getElementById('" & Table4.ClientID & "').className = '';", True)
                        End If

                    End If

                End If

            Next

        Catch ex As Exception

        End Try

    End Sub

    Private Sub btnDeleteShade_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDeleteShade.Click

        Try

            For Each gridItem As DataGridItem In dgResults.Items

                Dim chkMass As CheckBox = gridItem.FindControl("chkMass")

                If Not IsNothing(chkMass) Then

                    Dim Table4 As System.Web.UI.HtmlControls.HtmlTable = gridItem.FindControl("Table4")

                    If chkMass.Checked Then
                        Dim CanDeleteFile As Label = gridItem.FindControl("CanDeleteFile")

                        If CanDeleteFile.Text = "False" Then
                            System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.NewGuid.ToString, "document.getElementById('" & Table4.ClientID & "').className = 'selectederror';", True)
                        End If

                    End If

                End If

            Next

        Catch ex As Exception

        End Try

    End Sub

    Private Sub btnMoveCheck_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnMoveCheck.Click

        If Not IsNumeric(MoveToFolder.Value) Then Exit Sub

        If Session("FolderId") = MoveToFolder.Value Then Exit Sub

        Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
        Me.mconSqlImage.Open()

        Dim blnItemsSelected As Boolean = False
        Dim Documents As New DataTable
        Documents.Columns.Add("ImageId")

        Dim sqlCmd As SqlCommand
        Dim sqlDa As SqlDataAdapter
        Dim sqlBld As SqlCommandBuilder

        Dim items As New Collection

        Try

            For Each dgi As DataGridItem In dgResults.Items

                Dim chkMass As CheckBox = dgi.FindControl("chkMass")
                Dim DocNo As Label = dgi.FindControl("DocNo")

                Dim ImageId As Integer = DocNo.Text.Replace("<b>", "").Replace("</b>", "")

                'Dim 'lnkFileNameStartup As LinkButton = dgi.FindControl("'lnkFile")
                'TODO If Not IsNothing('lnkFileNameStartup) Then 'RegisterExcludeControl('lnkFileNameStartup)

                If chkMass.Checked And IsNumeric(ImageId) And dgi.Visible = True Then
                    blnItemsSelected = True
                    Dim dr As DataRow = Documents.NewRow
                    dr("ImageId") = ImageId
                    Documents.Rows.Add(dr)
                    items.Add(dgi)
                    chkMass.Checked = False
                End If

            Next

            If blnItemsSelected Then

                Dim tempTableName As String = "##" & System.Guid.NewGuid.ToString.Replace("-", "")
                sqlCmd = New SqlCommand("CREATE TABLE " & tempTableName & " (ImageId int)", Me.mconSqlImage)
                sqlCmd.ExecuteNonQuery()

                sqlCmd = New SqlCommand("SELECT * FROM " & tempTableName, Me.mconSqlImage)
                sqlDa = New SqlDataAdapter(sqlCmd)
                sqlBld = New SqlCommandBuilder(sqlDa)

                sqlDa.Update(Documents)

                sqlCmd = New SqlCommand("SMMassMove", Me.mconSqlImage)
                sqlCmd.CommandType = CommandType.StoredProcedure
                sqlCmd.Parameters.Clear()
                sqlCmd.Parameters.Add("@DocListTable", tempTableName)
                sqlCmd.Parameters.Add("@UserID", Me.mobjUser.ImagesUserId)
                sqlCmd.Parameters.Add("@DestinationFolderID", MoveToFolder.Value)

                Dim Results As New DataTable
                sqlDa = New SqlDataAdapter(sqlCmd)
                sqlDa.Fill(Results)

                If Results.Rows.Count > 0 Then

                    'Errors deleting
                    Dim sb As New System.Text.StringBuilder
                    sb.Append("You do not have access to move the following documents: ")
                    Dim count As Integer = 0
                    For Each dr As DataRow In Results.Rows
                        count += 1
                        sb.Append(dr("ImageId"))
                        If count <> Results.Rows.Count Then sb.Append(", ")
                    Next

                    ShowPopupError(sb.ToString, False)

                Else
                    'Success

                    For Each dgi As DataGridItem In items
                        dgi.Visible = False
                    Next

                    'Check if mass action icons should remain visible
                    HideMassIcons()
                    For Each dgi As DataGridItem In dgResults.Items
                        Dim lblImageType As Label = dgi.FindControl("ImageType")
                        If dgi.Visible Then
                            Select Case CType(lblImageType.Text, ImageTypes)
                                Case Constants.ImageTypes.SCAN
                                    ShowMassIcons()
                                    Exit For
                                Case Constants.ImageTypes.ATTACHMENT
                                    ShowMassIcons()
                                    Exit For
                            End Select
                        End If
                    Next

                    If Session("FolderId") <> MoveToFolder.Value Then
                        For Each dr As DataRow In Documents.Rows
                            Functions.UpdateCachedResults(dr("ImageId"), Me)
                        Next
                    End If

                End If

            Else

                ShowPopupError("Please select one or more documents to move.", False)
                Exit Sub

            End If

        Catch ex As Exception

            ShowPopupError("An error occured while deleting the selected documents.", True)

            Dim sb As New System.Text.StringBuilder
            For Each dr As DataRow In Documents.Rows
                sb.Append(dr("ImageId") & ", ")
            Next

            Dim conMaster As SqlClient.SqlConnection
            conMaster = Functions.BuildMasterConnection
            Functions.LogException(mobjUser.UserId.ToString, mobjUser.AccountId.ToString, "", "Mass Move, ImageId's: " & sb.ToString & " " & ex.Message, ex.StackTrace, Server.MachineName, "", Request.UrlReferrer.ToString, conMaster, True)

        Finally

            If Me.mconSqlImage.State <> ConnectionState.Closed Then
                Me.mconSqlImage.Close()
                Me.mconSqlImage.Dispose()
            End If

            System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType(), "finish", "stopProgressDisplay();resultsMode();resizeElements();", True)

        End Try

    End Sub

    Private Sub btnMoveShade_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnMoveShade.Click

        Try

            For Each gridItem As DataGridItem In dgResults.Items

                Dim chkMass As CheckBox = gridItem.FindControl("chkMass")

                If Not IsNothing(chkMass) Then

                    Dim Table4 As System.Web.UI.HtmlControls.HtmlTable = gridItem.FindControl("Table4")

                    If chkMass.Checked Then
                        Dim CanMoveFile As Label = gridItem.FindControl("CanMoveFile")

                        If CanMoveFile.Text = "False" Then
                            System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.NewGuid.ToString, "document.getElementById('" & Table4.ClientID & "').className = 'selectederror';", True)
                        End If

                    End If

                End If

            Next

        Catch ex As Exception

        End Try

    End Sub

    Private Sub btnMoveUnselect_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnMoveUnselect.Click

        Try

            For Each gridItem As DataGridItem In dgResults.Items

                Dim chkMass As CheckBox = gridItem.FindControl("chkMass")

                If Not IsNothing(chkMass) Then

                    Dim Table4 As System.Web.UI.HtmlControls.HtmlTable = gridItem.FindControl("Table4")

                    If chkMass.Checked Then
                        Dim CanMoveFile As Label = gridItem.FindControl("CanMoveFile")

                        If CanMoveFile.Text = "False" Then
                            System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.NewGuid.ToString, "document.getElementById('" & chkMass.ClientID & "').checked = false;document.getElementById('" & Table4.ClientID & "').className = '';", True)
                        End If

                    End If

                End If

            Next

        Catch ex As Exception

        End Try

    End Sub

    Private Sub btnMassMove_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnMassMove.Click

        Dim trnSql As SqlClient.SqlTransaction
        Dim conSqlCompany As New SqlClient.SqlConnection

        conSqlCompany = Functions.BuildConnection(Me.mobjUser)
        conSqlCompany.Open()
        trnSql = conSqlCompany.BeginTransaction

        Dim objFolders As New Accucentric.docAssist.Data.Images.Folders(conSqlCompany, False, trnSql)

        Try

            For Each dgi As DataGridItem In dgResults.Items

                Dim chkMass As CheckBox = dgi.FindControl("chkMass")
                Dim lblVersionId As Label = dgi.FindControl("DocNo")

                'Dim 'lnkFileNameStartup As LinkButton = dgi.FindControl("'lnkFile")
                'TODO If Not IsNothing('lnkFileNameStartup) Then 'RegisterExcludeControl('lnkFileNameStartup)

                If chkMass.Checked Then

                    Dim result As Integer = objFolders.MoveToFolder(Me.mobjUser.ImagesUserId, lblVersionId.Text, MoveFolder.Value)

                    If Not result > 0 Then
                        'Move error
                        System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.NewGuid.ToString, "alert('An error occured while moving the selected documents. Please try again. If the problem continues contact support.');", True)
                        Exit Sub
                    End If

                    dgi.Visible = False
                    chkMass.Checked = False

                End If

            Next

            trnSql.Commit()

            'Check if mass action icons should remain visible
            HideMassIcons()
            For Each dgi As DataGridItem In dgResults.Items
                Dim lblImageType As Label = dgi.FindControl("ImageType")
                If dgi.Visible Then
                    Select Case CType(lblImageType.Text, ImageTypes)
                        Case Constants.ImageTypes.SCAN
                            ShowMassIcons()
                            Exit For
                        Case Constants.ImageTypes.ATTACHMENT
                            ShowMassIcons()
                            Exit For
                    End Select
                End If
            Next

        Catch ex As Exception

            trnSql.Rollback()

        Finally

            If conSqlCompany.State <> ConnectionState.Closed Then
                conSqlCompany.Close()
                conSqlCompany.Dispose()
            End If

            System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.NewGuid.ToString, "stopProgressDisplay();", True)

        End Try

    End Sub

    Private Sub btnDownloadUnselect_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDownloadUnselect.Click

        Try

            ShowMassIcons()

            For Each gridItem As DataGridItem In dgResults.Items

                Dim chkMass As CheckBox = gridItem.FindControl("chkMass")

                If Not IsNothing(chkMass) Then

                    Dim Table4 As System.Web.UI.HtmlControls.HtmlTable = gridItem.FindControl("Table4")

                    If chkMass.Checked Then
                        Dim CanDownloadFile As Label = gridItem.FindControl("CanDownloadFile")

                        If CanDownloadFile.Text = "False" Then
                            System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.NewGuid.ToString, "document.getElementById('" & chkMass.ClientID & "').checked = false;document.getElementById('" & Table4.ClientID & "').className = '';", True)
                        End If

                    End If

                End If

            Next

        Catch ex As Exception

        End Try

    End Sub

    Private Sub btnDownloadShade_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDownloadShade.Click

        Try

            For Each gridItem As DataGridItem In dgResults.Items

                Dim chkMass As CheckBox = gridItem.FindControl("chkMass")

                If Not IsNothing(chkMass) Then

                    Dim Table4 As System.Web.UI.HtmlControls.HtmlTable = gridItem.FindControl("Table4")

                    If chkMass.Checked Then
                        Dim CanDownloadFile As Label = gridItem.FindControl("CanDownloadFile")

                        If CanDownloadFile.Text = "False" Then
                            System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.NewGuid.ToString, "document.getElementById('" & Table4.ClientID & "').className = 'selectederror';", True)
                        End If

                    End If

                End If

            Next

        Catch ex As Exception

        End Try

    End Sub

    Private Sub btnMassDownload_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnMassDownload.Click


        Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
        Me.mconSqlImage.Open()

        Dim blnItemsSelected As Boolean = False
        Dim Documents As New DataTable
        Documents.Columns.Add("ImageId")

        Dim sqlCmd As SqlCommand
        Dim sqlDa As SqlDataAdapter
        Dim sqlBld As SqlCommandBuilder

        Dim blnContainsScans As Boolean

        Try

            For Each dgi As DataGridItem In dgResults.Items

                Dim chkMass As CheckBox = dgi.FindControl("chkMass")
                Dim lblImageId As Label = dgi.FindControl("DocNo")

                'Dim 'lnkFileNameStartup As LinkButton = dgi.FindControl("'lnkFile")
                'TODO If Not IsNothing('lnkFileNameStartup) Then 'RegisterExcludeControl('lnkFileNameStartup)

                Dim ImageId As Integer = lblImageId.Text.Replace("<b>", "").Replace("</b>", "")

                If chkMass.Checked And IsNumeric(ImageId) And dgi.Visible = True Then
                    blnItemsSelected = True
                    Dim drDoc As DataRow = Documents.NewRow
                    drDoc("ImageId") = ImageId
                    Documents.Rows.Add(drDoc)
                End If

            Next

            If blnItemsSelected Then

                Dim tempTableName As String = "##" & System.Guid.NewGuid.ToString.Replace("-", "")
                sqlCmd = New SqlCommand("CREATE TABLE " & tempTableName & " (ImageId int)", Me.mconSqlImage)
                sqlCmd.ExecuteNonQuery()

                sqlCmd = New SqlCommand("SELECT * FROM " & tempTableName, Me.mconSqlImage)
                sqlDa = New SqlDataAdapter(sqlCmd)
                sqlBld = New SqlCommandBuilder(sqlDa)

                sqlDa.Update(Documents)

                sqlCmd = New SqlCommand("SMMassEmailDownload", Me.mconSqlImage)
                sqlCmd.CommandType = CommandType.StoredProcedure
                sqlCmd.Parameters.Clear()
                sqlCmd.Parameters.Add("@DocListTable", tempTableName)
                sqlCmd.Parameters.Add("@UserID", Me.mobjUser.ImagesUserId)

                Dim prm As New SqlClient.SqlParameter
                prm.ParameterName = "@TotalSizeKB"
                prm.Direction = ParameterDirection.Output
                prm.DbType = DbType.Int32
                sqlCmd.Parameters.Add(prm)

                Dim Results As New DataTable
                sqlDa = New SqlDataAdapter(sqlCmd)
                sqlDa.Fill(Results)

                Dim intByteCount As Integer = sqlCmd.Parameters("@TotalSizeKB").Value

                If Results.Rows.Count > 0 Then

                    'Errors deleting
                    Dim sb As New System.Text.StringBuilder
                    sb.Append("You do not have access to download the following documents: ")
                    Dim count As Integer = 0
                    For Each dr As DataRow In Results.Rows
                        count += 1
                        sb.Append(dr("ImageId"))
                        If count <> Results.Rows.Count Then sb.Append(", ")
                    Next

                    ShowPopupError(sb.ToString, False)
                    Exit Sub

                Else
                    'Success

                    If intByteCount > MASS_DOWNLOAD_SIZE Then
                        ShowPopupError("The selected documents are too large to download at once. Please unselect documents and try again.", False)
                        Exit Sub
                    End If

                    Try

                        'Put files in a ZIP file
                        Dim mem As New System.IO.MemoryStream
                        Dim zip As New ICSharpCode.SharpZipLib.Zip.ZipOutputStream(mem)
                        Dim zipentry As ICSharpCode.SharpZipLib.Zip.ZipEntry
                        Dim crc As New ICSharpCode.SharpZipLib.Checksums.Crc32

                        For Each dgi As DataGridItem In dgResults.Items

                            Dim imgByte As Byte()
                            Dim chkMass As CheckBox = dgi.FindControl("chkMass")
                            Dim lblVersionId As Label = dgi.FindControl("VersionId")

                            If Not IsNothing(chkMass) Then

                                If chkMass.Visible And chkMass.Checked Then

                                    Dim lblImageType As Label = dgi.FindControl("ImageType")
                                    Dim DocNo As Label = dgi.FindControl("DocNo")

                                    Dim ImageId As Integer = DocNo.Text.Replace("<b>", "").Replace("</b>", "")


                                    Select Case CType(lblImageType.Text, ImageTypes)

                                        Case Constants.ImageTypes.ATTACHMENT

                                            Dim objAttachments As New FileAttachments(Functions.BuildConnection(Me.mobjUser))
                                            Dim dt As New DataTable
                                            dt = objAttachments.GetFile(lblVersionId.Text)
                                            imgByte = CType(dt.Rows(0)("Data"), Byte())

                                            Dim strFilename As String = ImageId & " - " & CStr(dt.Rows(0)("Filename"))
                                            zipentry = New ICSharpCode.SharpZipLib.Zip.ZipEntry(strFilename)

                                            zip.SetLevel(6)
                                            zipentry.DateTime = DateTime.Now
                                            zipentry.Size = imgByte.Length

                                            crc.Reset()
                                            crc.Update(imgByte)

                                            zip.PutNextEntry(zipentry)
                                            zip.Write(imgByte, 0, imgByte.Length)

                                            'Track attachment
                                            Try
                                                Accucentric.docAssist.Web.Data.Functions.TrackPageRequest(Me.mobjUser.UserId, Me.mobjUser.AccountId, "Web Export", "Out", lblVersionId.Text, "", Functions.GetMasterConnectionString)
                                            Catch ex As Exception

                                            End Try

                                        Case Constants.ImageTypes.SCAN

                                            Dim PageCount As Integer = CType(dgi.FindControl("Pages"), Label).Text

                                            If chkSplitPages.Checked Then

                                                For currentPage As Integer = 1 To PageCount

                                                    Dim AttachmentTitle As Label = dgi.FindControl("Title")

                                                    Dim strFile, strFilename As String
                                                    If rdoPdf.Checked Then
                                                        strFile = Server.MapPath("tmp/" & Functions.ExportPDF(Me.mobjUser, lblVersionId.Text, currentPage, currentPage, Me, False))
                                                        strFilename = ImageId & " - " & IIf(AttachmentTitle.Text.Trim = "", "Untitled", AttachmentTitle.Text) & " - Page " & currentPage.ToString & ".pdf"
                                                    Else
                                                        strFile = Server.MapPath("tmp/" & Functions.ExportTIF(Me.mobjUser, lblVersionId.Text, currentPage, currentPage, Me, False))
                                                        strFilename = ImageId & " - " & IIf(AttachmentTitle.Text.Trim = "", "Untitled", AttachmentTitle.Text) & " - Page " & currentPage.ToString & ".tif"
                                                    End If

                                                    Dim byteFile As Byte() = GetDataAsByteArray(strFile)

                                                    zipentry = New ICSharpCode.SharpZipLib.Zip.ZipEntry(strFilename)

                                                    zip.SetLevel(6)
                                                    zipentry.DateTime = DateTime.Now
                                                    zipentry.Size = byteFile.Length

                                                    crc.Reset()
                                                    crc.Update(byteFile)

                                                    zip.PutNextEntry(zipentry)
                                                    zip.Write(byteFile, 0, byteFile.Length)

                                                    Try
                                                        System.IO.File.Delete(strFile)
                                                    Catch ex As Exception

                                                    End Try

                                                    'Track attachment
                                                    Try
                                                        Accucentric.docAssist.Web.Data.Functions.TrackPageRequest(Me.mobjUser.UserId, Me.mobjUser.AccountId, "Web Export", "Out", lblVersionId.Text, "", Functions.GetMasterConnectionString)
                                                    Catch ex As Exception

                                                    End Try

                                                Next

                                            Else

                                                Dim AttachmentTitle As Label = dgi.FindControl("Title")

                                                Dim strFile, strFilename As String
                                                If rdoPdf.Checked Then
                                                    strFile = Server.MapPath("tmp/" & Functions.ExportPDF(Me.mobjUser, lblVersionId.Text, 1, PageCount, Me, False))
                                                    strFilename = ImageId & " - " & IIf(AttachmentTitle.Text.Trim = "", "Untitled", AttachmentTitle.Text) & ".pdf"
                                                Else
                                                    strFile = Server.MapPath("tmp/" & Functions.ExportTIF(Me.mobjUser, lblVersionId.Text, 1, PageCount, Me, False))
                                                    strFilename = ImageId & " - " & IIf(AttachmentTitle.Text.Trim = "", "Untitled", AttachmentTitle.Text) & ".tif"
                                                End If

                                                Dim byteFile As Byte() = GetDataAsByteArray(strFile)

                                                zipentry = New ICSharpCode.SharpZipLib.Zip.ZipEntry(strFilename)

                                                zip.SetLevel(6)
                                                zipentry.DateTime = DateTime.Now
                                                zipentry.Size = byteFile.Length

                                                crc.Reset()
                                                crc.Update(byteFile)

                                                zip.PutNextEntry(zipentry)
                                                zip.Write(byteFile, 0, byteFile.Length)

                                                Try
                                                    System.IO.File.Delete(strFile)
                                                Catch ex As Exception

                                                End Try

                                                'Track attachment
                                                Try
                                                    For x As Integer = 1 To PageCount
                                                        Accucentric.docAssist.Web.Data.Functions.TrackPageRequest(Me.mobjUser.UserId, Me.mobjUser.AccountId, "Web Export", "Out", lblVersionId.Text, "", Functions.GetMasterConnectionString)
                                                    Next
                                                Catch ex As Exception

                                                End Try

                                            End If

                                    End Select

                                End If

                            End If

                        Next


                        Dim zipBytes As Byte()

                        zip.Finish()
                        zip.Close()

                        zipBytes = mem.ToArray

                        Dim FileTime As DateTime = Now.AddHours(Me.mobjUser.TimeDiffFromGMT)

                        Response.Clear()

                        Response.AddHeader("Content-Disposition", "attachment; filename=" & "docAssist_" & (FileTime.ToShortDateString.Replace("/", "-") & Space(1) & FileTime.ToShortTimeString.Replace(":", ".")).Replace(" ", "_") & ".zip")
                        Response.AddHeader("Content-Length", zipBytes.Length)
                        Response.ContentType = "application/octet-stream"
                        Response.BinaryWrite(zipBytes)
                        Response.End()
                        'Response.Flush()

                    Catch ex As Exception

                        Throw New Exception("Error processing attachment. " & ex.Message & " " & ex.StackTrace)

                    End Try

                End If

            Else

                ShowPopupError("Please select one or more documents to download.", False)
                Exit Sub

            End If

        Catch ex As Exception

            'Response.Write(ex.Message)

            If ex.Message.IndexOf("Thread was being aborted.") = -1 Then

                ShowPopupError("An error occured while downloading the selected documents. Please refresh the page and try again.", True)

                Dim sb As New System.Text.StringBuilder
                For Each dr As DataRow In Documents.Rows
                    sb.Append(dr("ImageId") & ", ")
                Next

                Dim conMaster As SqlClient.SqlConnection
                conMaster = Functions.BuildMasterConnection
                Functions.LogException(mobjUser.UserId.ToString, mobjUser.AccountId.ToString, "", "Mass Download, ImageId's: " & sb.ToString & " " & ex.Message, ex.StackTrace, Server.MachineName, "", Request.UrlReferrer.ToString, conMaster, True)

            End If

        Finally

            If Me.mconSqlImage.State <> ConnectionState.Closed Then
                Me.mconSqlImage.Close()
                Me.mconSqlImage.Dispose()
            End If

            System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.NewGuid.ToString, "stopProgressDisplay();resultsMode();resizeElements();", True)

        End Try

    End Sub

    Private Sub btnEmailCheck_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnEmailCheck.Click

        Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
        Me.mconSqlImage.Open()

        Dim blnItemsSelected As Boolean = False
        Dim Documents As New DataTable
        Documents.Columns.Add("ImageId")

        Dim sqlCmd As SqlCommand
        Dim sqlDa As SqlDataAdapter
        Dim sqlBld As SqlCommandBuilder

        Dim blnContainsScans As Boolean

        Dim dt As New DataTable
        dt.Columns.Add(New DataColumn("ImageType", GetType(System.String), Nothing, MappingType.Element))
        dt.Columns.Add(New DataColumn("VersionId", GetType(System.String), Nothing, MappingType.Element))
        dt.Columns.Add(New DataColumn("DocId", GetType(System.String), Nothing, MappingType.Element))
        dt.Columns.Add(New DataColumn("Title", GetType(System.String), Nothing, MappingType.Element))

        Try

            For Each dgi As DataGridItem In dgResults.Items

                Dim chkMass As CheckBox = dgi.FindControl("chkMass")
                Dim DocNo As Label = dgi.FindControl("DocNo")

                Dim ImageId As Integer = DocNo.Text.Replace("<b>", "").Replace("</b>", "")

                'Dim 'lnkFileNameStartup As LinkButton = dgi.FindControl("'lnkFile")
                'TODO If Not IsNothing('lnkFileNameStartup) Then RegisterExcludeControl('lnkFileNameStartup)

                If chkMass.Checked And IsNumeric(ImageId) And dgi.Visible = True Then

                    'Session table
                    Dim lblVersionId As Label = dgi.FindControl("VersionId")
                    Dim lblImageType As Label = dgi.FindControl("ImageType")
                    Dim lblAttachmentDocumentId As Label = dgi.FindControl("DocNo")
                    Dim AttachmentTitle As Label = dgi.FindControl("Title")

                    If lblImageType.Text = "1" Then blnContainsScans = True

                    Dim dr As DataRow = dt.NewRow
                    dr("ImageType") = lblImageType.Text
                    dr("VersionId") = lblVersionId.Text
                    dr("DocId") = ImageId
                    dr("Title") = AttachmentTitle.Text
                    dt.Rows.Add(dr)

                    blnItemsSelected = True
                    Dim drDoc As DataRow = Documents.NewRow
                    drDoc("ImageId") = ImageId
                    Documents.Rows.Add(drDoc)

                End If

            Next

            If blnItemsSelected Then

                Dim tempTableName As String = "##" & System.Guid.NewGuid.ToString.Replace("-", "")
                sqlCmd = New SqlCommand("CREATE TABLE " & tempTableName & " (ImageId int)", Me.mconSqlImage)
                sqlCmd.ExecuteNonQuery()

                sqlCmd = New SqlCommand("SELECT * FROM " & tempTableName, Me.mconSqlImage)
                sqlDa = New SqlDataAdapter(sqlCmd)
                sqlBld = New SqlCommandBuilder(sqlDa)

                sqlDa.Update(Documents)

                sqlCmd = New SqlCommand("SMMassEmailDownload", Me.mconSqlImage)
                sqlCmd.CommandType = CommandType.StoredProcedure
                sqlCmd.Parameters.Clear()
                sqlCmd.Parameters.Add("@DocListTable", tempTableName)
                sqlCmd.Parameters.Add("@UserID", Me.mobjUser.ImagesUserId)

                Dim prm As New SqlClient.SqlParameter
                prm.ParameterName = "@TotalSizeKB"
                prm.Direction = ParameterDirection.Output
                prm.DbType = DbType.Int32
                sqlCmd.Parameters.Add(prm)

                Dim Results As New DataTable
                sqlDa = New SqlDataAdapter(sqlCmd)
                sqlDa.Fill(Results)

                Dim intByteCount As Integer = sqlCmd.Parameters("@TotalSizeKB").Value

                If Results.Rows.Count > 0 Then

                    'Errors deleting
                    Dim sb As New System.Text.StringBuilder
                    sb.Append("You do not have access to e-mail the following documents: ")
                    Dim count As Integer = 0
                    For Each dr As DataRow In Results.Rows
                        count += 1
                        sb.Append(dr("ImageId"))
                        If count <> Results.Rows.Count Then sb.Append(", ")
                    Next

                    ShowPopupError(sb.ToString, False)
                    Exit Sub

                Else
                    'Success

                    If intByteCount > MASS_EMAIL_SIZE Then
                        ShowPopupError("The selected documents are too large to e-mail at once. Please unselect documents and try again.", False)
                        Exit Sub
                    End If

                    Session("MassEmail") = dt

                    If blnContainsScans Then
                        System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.NewGuid.ToString, "massEmail(true);", True)
                    Else
                        System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.NewGuid.ToString, "massEmail();", True)
                    End If

                End If

            Else

                ShowPopupError("Please select one or more documents to e-mail.", False)
                Exit Sub

            End If

        Catch ex As Exception

            ShowPopupError("An error occured while e-mailing the selected documents.", True)

            Dim sb As New System.Text.StringBuilder
            For Each dr As DataRow In Documents.Rows
                sb.Append(dr("ImageId") & ", ")
            Next

            Dim conMaster As SqlClient.SqlConnection
            conMaster = Functions.BuildMasterConnection
            Functions.LogException(mobjUser.UserId.ToString, mobjUser.AccountId.ToString, "", "Mass E-Mail, ImageId's: " & sb.ToString & " " & ex.Message, ex.StackTrace, Server.MachineName, "", Request.UrlReferrer.ToString, conMaster, True)

        Finally

            If Me.mconSqlImage.State <> ConnectionState.Closed Then
                Me.mconSqlImage.Close()
                Me.mconSqlImage.Dispose()
            End If

            System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.NewGuid.ToString, "stopProgressDisplay();resultsMode();resizeElements();", True)

        End Try

    End Sub

    Private Sub btnEmailUnselect_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnEmailUnselect.Click

        Try

            For Each gridItem As DataGridItem In dgResults.Items

                Dim chkMass As CheckBox = gridItem.FindControl("chkMass")

                If Not IsNothing(chkMass) Then

                    Dim Table4 As System.Web.UI.HtmlControls.HtmlTable = gridItem.FindControl("Table4")

                    If chkMass.Checked Then
                        Dim CanEmailFile As Label = gridItem.FindControl("CanEmailFile")

                        If CanEmailFile.Text = "False" Then
                            System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.NewGuid.ToString, "document.getElementById('" & chkMass.ClientID & "').checked = false;document.getElementById('" & Table4.ClientID & "').className = '';", True)
                        End If

                    End If

                End If

            Next

        Catch ex As Exception

        End Try

    End Sub

    Private Sub btnEmailShade_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnEmailShade.Click

        Try

            For Each gridItem As DataGridItem In dgResults.Items

                Dim chkMass As CheckBox = gridItem.FindControl("chkMass")

                If Not IsNothing(chkMass) Then

                    Dim Table4 As System.Web.UI.HtmlControls.HtmlTable = gridItem.FindControl("Table4")

                    If chkMass.Checked Then
                        Dim CanEmailFile As Label = gridItem.FindControl("CanEmailFile")
                        If CanEmailFile.Text = "False" Then
                            System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.NewGuid.ToString, "document.getElementById('" & Table4.ClientID & "').className = 'selectederror';", True)
                        End If

                    End If

                End If

            Next

        Catch ex As Exception

        End Try

    End Sub

    Private Sub btnPrintCheck_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrintCheck.Click

        Try

            Dim intByteCount As Integer = 0

            'Number of checkboxes selected
            Dim intSelected As Integer = 0
            For Each dgi As DataGridItem In dgResults.Items
                Dim chkMass As CheckBox = dgi.FindControl("chkMass")
                If Not IsNothing(chkMass) Then
                    If chkMass.Visible And chkMass.Checked Then intSelected += 1
                End If
            Next
            If intSelected = 0 Then Exit Sub


            'Check if user has permission to delete all selected items
            Dim blnDeleteError As Boolean = False
            For Each gridItem As DataGridItem In dgResults.Items
                Dim chkMass As CheckBox = gridItem.FindControl("chkMass")
                If Not IsNothing(chkMass) Then
                    If chkMass.Checked Then
                        Dim CanPrintFile As Label = gridItem.FindControl("CanPrintFile")

                        Dim lblImageType As Label = gridItem.FindControl("lblImageType")
                        Select Case CType(lblImageType.Text, ImageTypes)
                            Case Constants.ImageTypes.SCAN
                                If CanPrintFile.Text = "False" Then blnDeleteError = True
                        End Select


                    End If
                End If
            Next

            If blnDeleteError Then
                'No access to one or more documents
                System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.NewGuid.ToString, "if (confirm('You do not have access to print one or more documents. Click OK to unselect the documents or click CANCEL to show the documents you do not have access to.')) { document.getElementById('btnPrintUnselect').click(); } else { document.getElementById('btnPrintShade').click(); }", True)
                Exit Sub
            End If


            'Check if user selected file attachments
            Dim blnAttachmentError As Boolean = False
            For Each gridItem As DataGridItem In dgResults.Items
                Dim chkMass As CheckBox = gridItem.FindControl("chkMass")
                If Not IsNothing(chkMass) Then
                    If chkMass.Checked Then
                        Dim lblImageType As Label = gridItem.FindControl("lblImageType")
                        Select Case CType(lblImageType.Text, ImageTypes)
                            Case Constants.ImageTypes.ATTACHMENT
                                blnAttachmentError = True
                        End Select
                    End If
                End If
            Next

            If blnAttachmentError Then
                'No access to one or more documents
                System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.NewGuid.ToString, "if (confirm('You cannot print file attachments. Click OK to unselect the attachments or click CANCEL to show the attachments you cannot print.')) { document.getElementById('btnPrintAttachUnselect').click(); } else { document.getElementById('btnPrintShade').click(); }", True)
            Else
                'Full access to delete
                Dim sb As New System.Text.StringBuilder

                For Each gridItem As DataGridItem In dgResults.Items
                    Dim chkMass As CheckBox = gridItem.FindControl("chkMass")
                    If Not IsNothing(chkMass) Then
                        If chkMass.Checked Then
                            Dim lblVersionId As Label = gridItem.FindControl("lblVersionId")
                            sb.Append(lblVersionId.Text & ",")
                        End If
                    End If
                Next

                Dim Versions As String = sb.ToString

                System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.NewGuid.ToString, "massPrint('" & Versions.Remove(Versions.LastIndexOf(","), 1) & "','" & Me.mobjUser.AccountId.ToString & "');", True)

            End If

        Catch ex As Exception

        End Try

    End Sub

    Private Sub btnPrintUnselect_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrintUnselect.Click

        Try

            For Each gridItem As DataGridItem In dgResults.Items

                Dim chkMass As CheckBox = gridItem.FindControl("chkMass")

                If Not IsNothing(chkMass) Then

                    Dim Table4 As System.Web.UI.HtmlControls.HtmlTable = gridItem.FindControl("Table4")

                    If chkMass.Checked Then
                        Dim CanPrintFile As Label = gridItem.FindControl("CanPrintFile")

                        If CanPrintFile.Text = "False" Then
                            System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.NewGuid.ToString, "document.getElementById('" & chkMass.ClientID & "').checked = false;document.getElementById('" & Table4.ClientID & "').className = '';", True)
                        End If

                    End If

                End If

            Next

        Catch ex As Exception

        End Try

    End Sub

    Private Sub btnPrintShade_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrintShade.Click

        Try

            For Each gridItem As DataGridItem In dgResults.Items

                Dim chkMass As CheckBox = gridItem.FindControl("chkMass")

                If Not IsNothing(chkMass) Then

                    Dim Table4 As System.Web.UI.HtmlControls.HtmlTable = gridItem.FindControl("Table4")

                    If chkMass.Checked Then
                        Dim CanPrintFile As Label = gridItem.FindControl("CanPrintFile")
                        If CanPrintFile.Text = "False" Then
                            System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.NewGuid.ToString, "document.getElementById('" & Table4.ClientID & "').className = 'selectederror';", True)
                        End If

                    End If

                End If

            Next

        Catch ex As Exception

        End Try

    End Sub

    Private Sub btnPrintAttachShade_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrintAttachShade.Click

        Try

            For Each gridItem As DataGridItem In dgResults.Items

                Dim chkMass As CheckBox = gridItem.FindControl("chkMass")

                If Not IsNothing(chkMass) Then

                    Dim Table4 As System.Web.UI.HtmlControls.HtmlTable = gridItem.FindControl("Table4")

                    If chkMass.Checked Then

                        Dim lblImageType As Label = gridItem.FindControl("lblImageType")
                        Select Case CType(lblImageType.Text, ImageTypes)
                            Case Constants.ImageTypes.ATTACHMENT
                                System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.NewGuid.ToString, "document.getElementById('" & Table4.ClientID & "').className = 'selectederror';", True)
                        End Select

                    End If

                End If

            Next

        Catch ex As Exception

        End Try


    End Sub

    Private Sub btnPrintAttachUnselect_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrintAttachUnselect.Click

        Try

            For Each gridItem As DataGridItem In dgResults.Items

                Dim chkMass As CheckBox = gridItem.FindControl("chkMass")

                If Not IsNothing(chkMass) Then

                    Dim Table4 As System.Web.UI.HtmlControls.HtmlTable = gridItem.FindControl("Table4")

                    If chkMass.Checked Then

                        Dim lblImageType As Label = gridItem.FindControl("lblImageType")
                        Select Case CType(lblImageType.Text, ImageTypes)
                            Case Constants.ImageTypes.ATTACHMENT
                                System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.NewGuid.ToString, "document.getElementById('" & chkMass.ClientID & "').checked = false;document.getElementById('" & Table4.ClientID & "').className = '';", True)
                        End Select

                    End If

                End If

            Next

        Catch ex As Exception

        End Try

    End Sub

    Private Sub btnEmpty_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnEmpty.Click

        Try

            Dim sb As New System.Text.StringBuilder

            'Edit mode
            sb.Append("alert('You must assign a name.');")
            sb.Append("document.all.Rename.click();")

            System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.NewGuid.ToString, sb.ToString, True)

        Catch ex As Exception

        End Try

    End Sub

    Private Function DisplayMessage(ByVal msg As String)

        Try

            SetFolderHeading("", "")
            SetSearchCriteria("")
            dgResults.DataSource = Nothing
            dgResults.DataBind()
            lblSummary.Text = ""
            System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.NewGuid.ToString, "document.all.btnBack.style.visibility = 'hidden';document.all.NoResults.innerHTML = '" & msg & "';", True)

        Catch ex As Exception

        End Try

    End Function

    Private Sub btnTrackExpandCollapse_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnTrackExpandCollapse.Click

        Try

            Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)

            Dim objFolders As New Folders(Me.mconSqlImage)

            Dim Node As Folders.Node

            Select Case TrackNode.Value
                Case "MYFAVNODE" 'Favorites
                    Node = Folders.Node.Favorites
                Case "DOCNODE" 'Document Types
                    Node = Folders.Node.DocumentTypes
                Case "CabsFolders" 'Cabinets & Folders
                    Node = Folders.Node.CabinetsFolders
                Case "Views"
                    Node = Folders.Node.Views
            End Select

            objFolders.SaveTreeState(Me.mobjUser.ImagesUserId, Node, IIf(TrackAction.Value = "EXPAND", True, False))

        Catch ex As Exception

        Finally

            If Me.mconSqlImage.State <> ConnectionState.Closed Then
                Me.mconSqlImage.Close()
                Me.mconSqlImage.Dispose()
            End If

        End Try

    End Sub

    Private Sub IntegrationAdvancedSearch(Optional ByVal dsReload As DataSet = Nothing)

        Dim curPage As Integer = 1
        Dim firstRow As Integer = (Me.mobjUser.DefaultResultCount * (curPage - 1)) + 1
        Dim lastRow As Integer = (Me.mobjUser.DefaultResultCount + firstRow) - 1

        Dim sqlCmd As SqlClient.SqlCommand

        Dim dtBegin As DateTime = Now

        Dim dtDocuments As New DataTable("Documents")
        dtDocuments = dsReload.Tables(0)
        Dim dtAttributes As New DataTable("Attributes")
        dtAttributes = dsReload.Tables(1)

        Dim ResultCount As Integer

        Try
            Me.mcookieSearch.Keywords = ""

            Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
            Me.mconSqlImage.Open()

            'Build temp search tables
            Dim strDoc As String = System.Guid.NewGuid().ToString().Replace("-", "")
            Dim strAttributes As String = System.Guid.NewGuid().ToString().Replace("-", "")

            Dim sb As New System.Text.StringBuilder
            sb.Append("CREATE TABLE ##" & strDoc.ToString & " (DocumentId int)" & vbCrLf)
            sb.Append("CREATE TABLE ##" & strAttributes & " (AttributeId int, AttributeValue varchar(75), SearchType varchar(2), AttributeValueEnd varchar(75))" & vbCrLf)

            sqlCmd = New SqlCommand(sb.ToString, Me.mconSqlImage)
            Dim sqlDa As SqlDataAdapter
            Dim sqlBld As SqlCommandBuilder
            sqlCmd.ExecuteNonQuery()

            'Add documents
            sqlCmd = New SqlCommand("SELECT * FROM ##" & strDoc, Me.mconSqlImage)
            sqlDa = New SqlDataAdapter(sqlCmd)
            sqlBld = New SqlCommandBuilder(sqlDa)

            sqlDa.Update(dtDocuments)

            'Add Attributes
            sqlCmd = New SqlCommand("SELECT * FROM ##" & strAttributes, Me.mconSqlImage)
            sqlDa = New SqlDataAdapter(sqlCmd)
            sqlBld = New SqlCommandBuilder(sqlDa)

            sqlDa.Update(dtAttributes)

            sqlCmd.Parameters.Clear()

            sqlCmd = New SqlCommand("SMAdvSearch", Me.mconSqlImage)
            sqlCmd.CommandTimeout = 180
            sqlCmd.CommandType = CommandType.StoredProcedure
            sqlCmd.Parameters.Add(New SqlParameter("@UserID", Me.mobjUser.ImagesUserId))
            sqlCmd.Parameters.Add(New SqlParameter("@BeginRow", firstRow))
            sqlCmd.Parameters.Add(New SqlParameter("@EndRow", lastRow))
            sqlCmd.Parameters.Add(New SqlParameter("@DocTable", "##" & strDoc))
            sqlCmd.Parameters.Add(New SqlParameter("@AttributeTable", "##" & strAttributes))
            sqlCmd.Parameters.Add(New SqlParameter("@DiffFromGMT", Me.mobjUser.TimeDiffFromGMT))

            If SortDirection.Value.Trim <> "" Then sqlCmd.Parameters.Add(New SqlParameter("@SortDirection", SortDirection.Value))
            If SortAttributeId.Value.Trim <> "" Then sqlCmd.Parameters.Add(New SqlParameter("@SortValue", SortAttributeId.Value))

            Dim prmMaxAttributes As New SqlClient.SqlParameter
            prmMaxAttributes.ParameterName = "@ResultCount"
            prmMaxAttributes.Direction = ParameterDirection.Output
            prmMaxAttributes.DbType = DbType.Int32
            sqlCmd.Parameters.Add(prmMaxAttributes)

            Dim startTime As DateTime = Now

            sqlDa = New SqlDataAdapter(sqlCmd)
            sqlDa.Fill(ds)
            ResultCount = sqlCmd.Parameters("@ResultCount").Value

            If Me.mobjUser.DefaultSkipResult = 1 And ResultCount = 1 Then
                System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.NewGuid.ToString, "LoadImage(" & ds.Tables(0).Rows(0)("VersionId") & ");", True)
            End If

            Dim endTime As DateTime = Now

            dgResults.DataSource = ds
            dgResults.DataBind()

            If ResultCount < lastRow Then lastRow = ResultCount

            If ResultCount > 0 Then

                SaveNextPreviousDocList(ResultCount)

                dgResults.DataSource = ds
                dgResults.DataBind()

                System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.NewGuid.ToString, "document.all.NoResults.innerHTML = '';", True)

                Dim pages As Integer = Math.Ceiling(ResultCount / Me.mobjUser.DefaultResultCount)
                BuildPagination(pages)

                Dim dblSeconds As Double = Math.Round(endTime.Subtract(startTime).TotalMilliseconds() / 1000, 2)
                lblSummary.Text = "Results <b>" & firstRow & " - " & lastRow & "</b> of <b>" & ResultCount & "</b> (" & IIf(dblSeconds < 1, dblSeconds & " second)", dblSeconds & " seconds)")

            Else

                System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.NewGuid.ToString, "document.all.NoResults.innerHTML = '" & strNoResults & "';", True)
                lblPages.Text = ""
                lblSummary.Text = ""
                dgResults.DataSource = Nothing
                dgResults.DataBind()
                HideMassIcons()

            End If

        Catch ex As Exception

            If ex.Message <> "Thread was being aborted." Then
                ShowPopupError("An error has occured while performing your integration search. Please try again.", True)
                Dim conMaster As SqlClient.SqlConnection
                conMaster = Functions.BuildMasterConnection
                Functions.LogException(mobjUser.UserId.ToString, mobjUser.AccountId.ToString, "", "Integration Search Error: " & ex.Message, ex.StackTrace, Server.MachineName, "", Request.UrlReferrer.ToString, conMaster, True)
            End If

        Finally

            Session("SearchType") = "Advanced"
            Session("CurrentPage") = curPage
            Session("ResultCount") = ResultCount
            Session("DataSource") = ds

            SearchType.Value = "A"
            'ReturnControls.Add(SearchType)

            Dim dsSession As New DataSet
            Try
                dsSession.Tables.Add(dtDocuments)
                dsSession.Tables.Add(dtAttributes)
            Catch ex As Exception

            End Try

            HideFolderHeading()
            HideRestoreButton()

            If IsNothing(dsReload) Then
                ShowBackButton(False)
                btnBack.Attributes("style") = "VISIBILITY: visible"
            Else
                HideBackButton()
                btnBack.Attributes("style") = "VISIBILITY: hidden"
            End If

            If Me.mconSqlImage.State <> ConnectionState.Closed Then
                Me.mconSqlImage.Close()
                Me.mconSqlImage.Dispose()
            End If

            If Not dsReload Is Nothing Then

            Else
                LoadSearch.Value = 1
            End If

            System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.NewGuid.ToString, "document.all.BackMethod.value='A';document.all.SearchType.value = 'A';document.all.btnBack.alt = 'Modify Search';resizeElements();resultsMode();stopProgressDisplay();resetTimeout();", True)

        End Try

    End Sub

    Private Sub dgRecentItems_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgRecentItems.ItemDataBound

        'RecentKey
        'QS - Quick Search
        'VW - View
        'SV - Save

        'KeyValue is version id

        Try

            Dim RecentKey As Label = e.Item.FindControl("RecentKey")
            Dim Value As Label = e.Item.FindControl("Value")
            Dim KeyValue As Label = e.Item.FindControl("KeyValue")
            Dim lblDesc As Label = e.Item.FindControl("lblDesc")
            Dim ImageType As Label = e.Item.FindControl("ImageType")
            Dim Filename As Label = e.Item.FindControl("Filename")
            Dim imgIcon As Web.UI.WebControls.Image = e.Item.FindControl("imgIcon")

            If Not IsNothing(lblDesc) Then

                e.Item.ID = "recent" & KeyValue.Text

                e.Item.Attributes("style") = "CURSOR: hand"

                Select Case RecentKey.Text
                    Case "QS" 'Quick Search

                        lblDesc.Text = Value.Text
                        e.Item.Attributes("onclick") = "tagSearch('" & Value.Text & "');"
                        imgIcon.ImageUrl = "Images/search/search.jpg"

                    Case "VW", "SV"

                        imgIcon.ImageUrl = "Images/folders/doc.gif"

                        Select Case ImageType.Text
                            Case 1
                                lblDesc.Text = Server.UrlDecode(Value.Text)
                                imgIcon.Attributes("onmouseover") = "Preview(" & KeyValue.Text & ");"
                                imgIcon.Attributes("onmouseout") = "HidePreview();"
                                imgIcon.ImageUrl = "Images/folders/scan.gif"
                            Case 2
                                lblDesc.Text = Server.UrlDecode(Value.Text)
                            Case 3
                                lblDesc.Text = Server.UrlDecode(Value.Text)
                        End Select

                        If Filename.Text = "" Then
                            'Image
                            e.Item.Attributes("onclick") = "loadRecent('" & KeyValue.Text & "');"
                        Else
                            'Attachment
                            e.Item.Attributes("onclick") = "oldViewer('" & KeyValue.Text & "');"
                        End If


                        Dim strIconPath As String = "images/icons/" & System.IO.Path.GetExtension(Filename.Text).ToUpper.Replace(".", "") & ".gif"
                        If System.IO.File.Exists(Server.MapPath(strIconPath)) Then
                            imgIcon.ImageUrl = strIconPath
                            If System.IO.Path.GetExtension(Filename.Text).ToLower = ".pdf" Then
                                imgIcon.Attributes.Add("onclick", "javascript:loadPDF(" & KeyValue.Text & ");")
                            End If
                        End If

                End Select

            End If

        Catch ex As Exception

        End Try

    End Sub

    Private Sub btnUpdateRecent_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnUpdateRecent.Click

        Try
            Dim cmdSql As New SqlClient.SqlCommand("", Me.mconSqlImage)
            Dim da As New SqlClient.SqlDataAdapter(cmdSql)
            cmdSql.Parameters.Clear()
            cmdSql.CommandText = "SMRecentItemGet"
            cmdSql.Parameters.Add("@UserId", Me.mobjUser.ImagesUserId)
            cmdSql.CommandType = CommandType.StoredProcedure
            Dim dtRecent As New DataTable
            da.Fill(dtRecent)

            If dtRecent.Rows.Count = 0 Then
                NoRecent.InnerHtml = "No recent items."
            Else
                NoRecent.InnerHtml = ""
                dgRecentItems.DataSource = dtRecent
                dgRecentItems.DataBind()
            End If

            'ReturnControls.Add(dgRecentItems)
            'ReturnControls.Add(NoRecent)

        Catch ex As Exception

            Dim conMaster As SqlClient.SqlConnection
            conMaster = Functions.BuildMasterConnection
            Functions.LogException(mobjUser.UserId.ToString, mobjUser.AccountId.ToString, "", "Recent Items Load: " & ex.Message, ex.StackTrace, Server.MachineName, "", Request.UrlReferrer.ToString, conMaster, True)

        End Try

    End Sub

    Private Sub btnStaple_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnStaple.Click

        Dim count As Integer = 0

        Dim dt As New DataTable("VersionIDs")
        dt.Columns.Add(New DataColumn("VersionId"))
        dt.Columns.Add(New DataColumn("ImageId"))

        Try

            For Each dgi As DataGridItem In dgResults.Items

                Dim chkMass As CheckBox = dgi.FindControl("chkMass")
                Dim lblVersionId As Label = dgi.FindControl("VersionId")

                'Dim 'lnkFileNameStartup As LinkButton = dgi.FindControl("'lnkFile")
                'TODO If Not IsNothing('lnkFileNameStartup) Then 'RegisterExcludeControl('lnkFileNameStartup)
                Dim DocNo As Label = dgi.FindControl("DocNo")

                Dim ImageId As Integer = DocNo.Text.Replace("<b>", "").Replace("</b>", "")

                Dim lblImageType As Label = CType(dgi.FindControl("ImageType"), Label)

                If Not lblImageType Is Nothing Then

                    If chkMass.Checked Then

                        Select Case CType(lblImageType.Text, ImageTypes)
                            Case Constants.ImageTypes.ATTACHMENT
                                System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.NewGuid.ToString, "resultsMode();resizeElements();alert('You cannot staple file attachments together. Please uncheck any file attachments and try again.');", True)
                                Session("StapleDocs") = Nothing
                                Exit Sub
                            Case Else
                                Dim dr As DataRow
                                dr = dt.NewRow
                                dr("VersionId") = lblVersionId.Text
                                dr("ImageId") = ImageId
                                dt.Rows.Add(dr)
                                count += 1
                        End Select

                    End If

                End If

            Next

            If count < 2 Then
                System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.NewGuid.ToString, "resultsMode();resizeElements();alert('Please select two or more documents before using the staple function.');", True)
                Session("StapleDocs") = Nothing
                Exit Sub
            Else
                Session("StapleDocs") = dt
                System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.NewGuid.ToString, "resultsMode();resizeElements();staple();", True)
            End If

        Catch ex As Exception

        Finally

        End Try

    End Sub

    Private Sub treeSearch_NodeCheckChanged(ByVal sender As Object, ByVal e As ComponentArt.Web.UI.TreeViewNodeEventArgs) Handles treeSearch.NodeCheckChanged

        If Me.mconSqlImage.State <> ConnectionState.Open Then
            Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
            Me.mconSqlImage.Open()
        End If

        Dim sb As New System.Text.StringBuilder

        Try

            Dim nodeCollection As TreeViewNode() = treeSearch.CheckedNodes

            If treeSearch.CheckedNodes.Length = 0 Then
                e.Node.Checked = True
                Dim sbNone As New System.Text.StringBuilder
                sbNone.Append("var selNode = window['treeSearch'].findNodeById('" & e.Node.ID & "');")
                sbNone.Append("selNode.SetProperty('Checked', 'true');")
                sbNone.Append("selNode.SaveState();")
                sbNone.Append("window['treeSearch'].Render();")
                System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType(), "advSearchSel", sbNone.ToString, True)
                Exit Sub
            End If

            Dim dtAttributes As New ImageAttributes
            Dim bolSelectedItems As Boolean = False

            If e Is Nothing Then
                If treeSearch.CheckedNodes.Length = 1 And treeSearch.CheckedNodes(0).ID = "AllDocs" Then
                    For Each node As TreeViewNode In treeSearch.FindNodeById("DOCNODE").Nodes
                        If sb.Length > 0 Then sb.Append(",")
                        If node.ID <> "AllDocs" And IsNumeric(node.ID.Replace("D", "")) Then sb.Append(node.ID.ToString.Replace("D", ""))
                        bolSelectedItems = True
                    Next
                    DocList.Value = sb.ToString
                Else
                    For Each node As TreeViewNode In treeSearch.CheckedNodes
                        If sb.Length > 0 Then sb.Append(",")
                        If node.ID <> "AllDocs" And IsNumeric(node.ID.Replace("D", "")) Then sb.Append(node.ID.ToString.Replace("D", ""))
                        bolSelectedItems = True
                    Next
                    DocList.Value = sb.ToString
                End If

            Else

                If e.Node.ID = "AllDocs" Then

                    'Check 'All Documents', uncheck children
                    Dim docNode As TreeViewNodeCollection = treeSearch.FindNodeById("DOCNODE").Nodes
                    treeSearch.FindNodeById("DOCNODE").UnCheckAll()
                    treeSearch.FindNodeById("DOCNODE").Checked = True

                    Dim sbClient As New System.Text.StringBuilder
                    sbClient.Append("var node = window['treeSearch'].findNodeById('DOCNODE');")
                    sbClient.Append("var selNode = window['treeSearch'].findNodeById('AllDocs');")
                    sbClient.Append("node.unCheckAll();")
                    sbClient.Append("selNode.SetProperty('Checked', 'true');")
                    sbClient.Append("window['treeSearch'].Render();")
                    System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType(), "AllDocs", sb.ToString, True)

                    For Each nodeCheck As TreeViewNode In docNode
                        If sb.Length > 0 Then sb.Append(",")
                        If nodeCheck.ID <> "AllDocs" Then sb.Append(nodeCheck.ID.ToString.Replace("D", ""))
                        bolSelectedItems = True
                        DocList.Value = sb.ToString
                    Next

                Else

                    Dim sbClient As New System.Text.StringBuilder
                    sbClient.Append("var node = window['treeSearch'].findNodeById('DOCNODE');")
                    sbClient.Append("node.unCheckAll();")

                    For Each nodeCheck As TreeViewNode In treeSearch.CheckedNodes
                        If nodeCheck.ID <> "AllDocs" Then
                            sbClient.Append("var " & nodeCheck.ID & " = window['treeSearch'].findNodeById('" & nodeCheck.ID & "');")
                            sbClient.Append(nodeCheck.ID & ".SetProperty('Checked', 'true');")
                        End If
                    Next

                    sbClient.Append("window['treeSearch'].Render();")
                    System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType(), "sbclient", sbClient.ToString, True)

                    For Each nodeCheck As TreeViewNode In treeSearch.CheckedNodes
                        If sb.Length > 0 Then sb.Append(",")
                        If nodeCheck.ID <> "AllDocs" Then sb.Append(nodeCheck.ID.ToString.Replace("D", ""))
                        bolSelectedItems = True
                        DocList.Value = sb.ToString
                    Next

                End If

            End If

            'Get attributes
            If bolSelectedItems Then

                'Dim dsSearchAttributes As New dsSearchAttributes
                Dim Attributes As New DataTable
                Dim sqlCmd As New SqlClient.SqlCommand("acsGetAttributes", Me.mconSqlImage)
                sqlCmd.CommandType = CommandType.StoredProcedure
                sqlCmd.Parameters.Add("@UserId", Me.mobjUser.ImagesUserId)
                sqlCmd.Parameters.Add("@DocumentId", sb.ToString)

                'Set value if coming from application integration
                If Session("gblAttributeID") <> "" And Session("gblAttributeValue") <> "" Then
                    sqlCmd.Parameters.Add("@AttributeID", Session("gblAttributeID"))
                    sqlCmd.Parameters.Add("@AttributeValue", Session("gblAttributeValue"))
                    Session("gblAttributeID") = ""
                    Session("gblAttributeValue") = ""
                End If

                Dim da As New SqlDataAdapter(sqlCmd)
                da.Fill(Attributes)
                da.Dispose()

                If Attributes.Rows.Count > 0 Then
                    dgSearchAttributes.DataSource = Attributes
                    dgSearchAttributes.Visible = True
                    dgSearchAttributes.DataBind()

                    ''Load common attributes from previous document configuration
                    'If Page.IsPostBack Then

                    '    Dim dvLoadAttributes As New DataView(dtAttributes)

                    '    For Each dgi As DataGridItem In dgSearchAttributes.Items
                    '        Dim ddlSearchType As DropDownList = CType(dgi.FindControl("ddlSearchType"), DropDownList)
                    '        Dim txtAttributeValue As TextBox = CType(dgi.FindControl("txtAttributeValue"), TextBox)
                    '        Dim txtBetweenFrom As TextBox = CType(dgi.FindControl("txtBetweenFrom"), TextBox)
                    '        Dim txtBetweenTo As TextBox = CType(dgi.FindControl("txtBetweenTo"), TextBox)
                    '        Dim lblAttributeId As Label = CType(dgi.FindControl("lblAttributeId"), Label)

                    '        If Not ddlSearchType Is Nothing Then
                    '            dvLoadAttributes.RowFilter = "AttributeId='" & "" & lblAttributeId.Text & "'"
                    '            If dvLoadAttributes.Count > 0 Then

                    '                Select Case dvLoadAttributes(0)("AttributeName")
                    '                    Case 0 'Equal
                    '                        txtAttributeValue.Text = dvLoadAttributes(0)("AttributeValue")
                    '                        ddlSearchType.SelectedValue = dvLoadAttributes(0)("AttributeName")
                    '                    Case 1 'Starts with
                    '                        txtAttributeValue.Text = dvLoadAttributes(0)("AttributeValue")
                    '                        ddlSearchType.SelectedValue = dvLoadAttributes(0)("AttributeName")
                    '                    Case 2 'Contains
                    '                        txtAttributeValue.Text = dvLoadAttributes(0)("AttributeValue")
                    '                        ddlSearchType.SelectedValue = dvLoadAttributes(0)("AttributeName")
                    '                    Case 5 'Bewtween
                    '                        Dim strValue As String = dvLoadAttributes(0)("AttributeValue")
                    '                        Dim strValues As String() = strValue.Split("�")
                    '                        txtBetweenFrom.Text = strValues(0)
                    '                        txtBetweenTo.Text = strValues(1)
                    '                        ddlSearchType.SelectedValue = dvLoadAttributes(0)("AttributeName")
                    '                End Select

                    '            End If
                    '        End If
                    '    Next
                    'End If

                Else
                    dgSearchAttributes.Visible = True
                End If

            End If

        Catch ex As Exception

            ShowPopupError("An error occured while loading the selected document attributes.", True)

            Dim conMaster As SqlClient.SqlConnection
            conMaster = Functions.BuildMasterConnection
            Functions.LogException(mobjUser.UserId.ToString, mobjUser.AccountId.ToString, "", "Attribute Load Error, Advanced Search, Documents Loaded: " & sb.ToString & " " & ex.Message, ex.StackTrace, Server.MachineName, "", Request.UrlReferrer.ToString, conMaster, True)

        Finally

            If treeSearch.CheckedNodes.Length > 0 Then
                System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType(), "searchMode", "searchMode();", True)
            End If

            System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType(), "showAttributes", "showAttributes();stopProgressDisplay();", True)

        End Try

    End Sub

    Private Sub treeSearch_NodeSelected(ByVal sender As Object, ByVal e As ComponentArt.Web.UI.TreeViewNodeEventArgs) Handles treeSearch.NodeSelected

        SortDirection.Value = ""
        SortAttributeId.Value = ""
        'ReturnControls.Add(SortDirection)
        'ReturnControls.Add(SortAttributeId)
        BrowseTreeFolder(BrowseFolder.Value)
        Exit Sub

        Try

            blnHideRelevance = True

            Try

                NodeId.Value = e.Node.ID
                NodeValue.Value = e.Node.ID

                If e.Node.ID = "AllDocs" Then

                    'Uncheck all nodes and check the node user clicked on
                    'Dim sb As New System.Text.StringBuilder
                    'sb.Append("var ndAll = window['treeSearch'].findNodeById('DOCNODE');")
                    'sb.Append("var selNode = window['treeSearch'].findNodeById('" & e.Node.ID & "');")
                    'sb.Append("ndAll.unCheckAll();")
                    'sb.Append("selNode.SetProperty('Checked', 'true');")
                    'sb.Append("selNode.SaveState();")
                    'sb.Append("window['treeSearch'].Render();")

                    ''ReturnScripts.Add(sb.ToString)

                    Dim node As TreeViewNode = treeSearch.FindNodeById("DOCNODE")
                    node.UnCheckAll()
                    e.Node.Checked = True
                    treeSearch_NodeCheckChanged(Nothing, Nothing)

                    Exit Sub

                End If

                If e.Node.ID = "DOCNODE" Then
                    treeSearch_NodeCheckChanged(Nothing, Nothing)
                    Exit Sub
                End If

                Select Case e.Node.ID.Substring(0, 1)

                    Case "F" 'Favorite


                    Case "W" 'Folder/Favorite


                    Case "D" 'Document

                        Try

                            If e.Node.ID = "DOCNODE" Then Exit Select

                            'Uncheck all nodes and check the node user clicked on
                            Dim sb As New System.Text.StringBuilder
                            sb.Append("var node = window['treeSearch'].findNodeById('" & "DOCNODE" & "');")
                            sb.Append("var selNode = window['treeSearch'].findNodeById('" & e.Node.ID & "');")
                            sb.Append("node.unCheckAll();")
                            sb.Append("selNode.SetProperty('Checked', 'true');")
                            sb.Append("selNode.SaveState();")
                            sb.Append("window['treeSearch'].Render();")

                            System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType(), "advSearch", sb.ToString, True)

                            Dim node As TreeViewNode = treeSearch.FindNodeById("DOCNODE")
                            node.UnCheckAll()
                            e.Node.Checked = True
                            treeSearch_NodeCheckChanged(Nothing, Nothing)

                        Catch ex As Exception

                        End Try

                    Case "S"

                End Select


            Catch ex As Exception

            Finally

            End Try

        Catch ex As Exception

        Finally

            If Me.mconSqlImage.State <> ConnectionState.Closed Then
                Me.mconSqlImage.Close()
                Me.mconSqlImage.Dispose()
            End If

        End Try

    End Sub

    Private Sub BrowseTreeFolder(ByVal FolderId As Integer, Optional ByVal dsReload As DataSet = Nothing)

        Dim curPage As Integer = CInt(CurrentPage.Value)
        Dim firstRow As Integer = (Me.mobjUser.DefaultResultCount * (curPage - 1)) + 1
        Dim lastRow As Integer = (Me.mobjUser.DefaultResultCount + firstRow) - 1
        Dim ResultCount As Integer

        Try

            recordSearchType(Functions.SearchType.FOLDER)

            Me.mcookieSearch.Keywords = ""

            blnFolderBrowseMode = True

            Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
            Dim objFolders As New Folders(Me.mconSqlImage, False)

            Dim startTime As DateTime = Now

            If IsNothing(dsReload) Then
                ds = objFolders.SMFolderBrowse(Me.mobjUser.ImagesUserId, FolderId, firstRow, lastRow, Me.mobjUser.TimeDiffFromGMT, ResultCount, IIf(SortDirection.Value.Trim <> "", SortDirection.Value.Trim, Nothing), IIf(SortAttributeId.Value.Trim <> "", SortAttributeId.Value.Trim, Nothing))
                SetFolderHeading(FolderName.Value, "")
            Else
                ds = dsReload
                ResultCount = Session("ResultCount")
                SetFolderHeading(Session("FolderName"), "")
            End If

            Dim endTime As DateTime = Now

            If ResultCount < lastRow Then lastRow = ResultCount

            Select Case ResultCount

                Case 0

                    System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType(), "noResults", "document.all.NoResults.innerHTML = '" & "No document or files were found in this folder." & "';", True)
                    lblPages.Text = ""
                    lblSummary.Text = ""
                    dgResults.DataSource = Nothing
                    dgResults.DataBind()
                    HideMassIcons()

                Case -1 'No access

                    ShowPopupError("Access Denied<br><br>Please contact a docAssist administrator within your organization if you need access to this folder.", False)
                    lblPages.Text = ""
                    lblSummary.Text = ""
                    dgResults.DataSource = Nothing
                    dgResults.DataBind()
                    HideMassIcons()

                Case Else

                    SaveNextPreviousDocList(ResultCount)

                    dgResults.DataSource = ds
                    dgResults.DataBind()

                    System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType(), "clear", "document.all.NoResults.innerHTML = '';", True)

                    Dim pages As Integer = Math.Ceiling(ResultCount / Me.mobjUser.DefaultResultCount)
                    BuildPagination(pages)

                    Dim dblSeconds As Double = Math.Round(endTime.Subtract(startTime).TotalMilliseconds() / 1000, 2)
                    lblSummary.Text = "Results <b>" & firstRow & " - " & lastRow & "</b> of <b>" & ResultCount & "</b> (" & IIf(dblSeconds < 1, dblSeconds & " second)", dblSeconds & " seconds)")

                    If NodeId.Value <> BrowseType.RECYCLE_BIN Then
                        ShowMassIcons()
                        HideRestoreButton()
                    Else
                        HideMassIcons()
                        ShowRestoreButton()
                    End If

            End Select

        Catch ex As Exception

            ShowPopupError("An error has occured while browsing this folder. Please try again.", True)

            Dim conMaster As SqlClient.SqlConnection
            conMaster = Functions.BuildMasterConnection
            Functions.LogException(mobjUser.UserId.ToString, mobjUser.AccountId.ToString, "", "Browse Folder Error, FolderID: " & FolderId & " " & ex.Message, ex.StackTrace, Server.MachineName, "", Request.UrlReferrer.ToString, conMaster, True)

        Finally

            HideBackButton()

            blnFolderBrowseMode = False

            Session("SearchType") = "FolderBrowse"
            Session("CurrentPage") = curPage
            Session("ResultCount") = ResultCount
            Session("DataSource") = ds
            Session("FolderName") = FolderName.Value
            Session("FolderId") = FolderId

            SearchType.Value = "F"

            System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType(), "browseComplete", "stopProgressDisplay();resultsMode();resizeElements();", True)

            If Me.mconSqlImage.State <> ConnectionState.Closed Then
                Me.mconSqlImage.Close()
                Me.mconSqlImage.Dispose()
            End If

        End Try

    End Sub

    Private Sub btnQuickPaging_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnQuickPaging.Click

        If txtQuickSearch.Text.Trim.Length > 0 Then
            QuickSearch()
        End If

    End Sub

    Private Function ShowPopupError(ByVal Message As String, ByVal StaffNotificationMessage As Boolean)
        If StaffNotificationMessage Then
            System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.NewGuid.ToString, "showInfoBox('<b>" & Message & "</b><br><br>Our staff has been notified of the error and is working to correct any possible issues.');", True)
        Else
            System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.NewGuid.ToString, "showInfoBox('<b>" & Message & "</b>');", True)
        End If
    End Function

    Private Sub LoadLastSearch()

        Try

            Select Case Session("SearchType")

                Case "Home"
                    System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.NewGuid.ToString, "parent.window.landMode();", True)
                Case "Quick"

                    Dim ds As New DataSet
                    ds = Session("DataSource")

                    If Not IsNothing(ds) Then
                        'txtQuickSearch.Text = Session("QuickSearchValue")
                        'ddlQuickSearchType.SelectedValue = Session("QuickSearchType")
                        CurrentPage.Value = Session("CurrentPage")
                        'ReturnControls.Add(CurrentPage)
                        QuickSearch(ds)
                        'btnQuickSearch_Click(Nothing, Nothing)
                    End If

                Case "Advanced"

                    Dim ds As New DataSet
                    ds = Session("DataSource")

                    If Not IsNothing(ds) Then
                        CurrentPage.Value = Session("CurrentPage")
                        AdvancedSearch(ds)
                    End If

                Case "FolderBrowse"

                    Dim ds As New DataSet
                    ds = Session("DataSource")

                    If Not IsNothing(ds) Then
                        BrowseFolder.Value = Session("FolderId")
                        CurrentPage.Value = Session("CurrentPage")
                        NodeId.Value = Session("FolderId")
                        BrowseTreeFolder(Nothing, ds)
                    End If

                Case "IntegrationAdvanced"

                    Dim ds As New DataSet
                    ds = Session("DataSource")

                    If Not IsNothing(ds) Then
                        CurrentPage.Value = Session("CurrentPage")
                        IntegrationAdvancedSearch(ds)
                    End If

            End Select

        Catch ex As Exception

        End Try

    End Sub

    Private Sub SaveNextPreviousDocList(ByVal ResultCount As Integer)

        Try

            Dim tmpDocs As New DataTable
            tmpDocs.Columns.Add("Row")
            tmpDocs.Columns.Add("VersionId")
            Dim count = 1
            For Each drTmp As DataRow In ds.Tables(0).Rows
                Dim newRow As DataRow = tmpDocs.NewRow
                newRow("Row") = count
                If ds.Tables(0).Columns.Contains("VersionId") Then
                    newRow("VersionId") = drTmp("VersionId")
                Else
                    newRow("VersionId") = drTmp("CurrentVersionId")
                End If
                tmpDocs.Rows.Add(newRow)
                count += 1
            Next

            Session("dtVersionId") = tmpDocs
            Session("intTotalDocuments") = Me.mobjUser.DefaultResultCount

        Catch ex As Exception

        End Try

    End Sub

    Private Sub LoadAttributes_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles LoadAttributes.Click

        If Me.mconSqlImage.State <> ConnectionState.Open Then
            Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
            Me.mconSqlImage.Open()
        End If

        Dim sb As New System.Text.StringBuilder

        Try

            'Dim dsSearchAttributes As New dsSearchAttributes
            Dim Attributes As New DataTable
            Dim sqlCmd As New SqlClient.SqlCommand("acsGetAttributes", Me.mconSqlImage)
            sqlCmd.CommandType = CommandType.StoredProcedure
            sqlCmd.Parameters.Add("@UserId", Me.mobjUser.ImagesUserId)
            sqlCmd.Parameters.Add("@DocumentId", DocList.Value.Replace(",Allocs,", ""))

            ''Set value if coming from application integration
            'If Session("gblAttributeID") <> "" And Session("gblAttributeValue") <> "" Then
            '    sqlCmd.Parameters.Add("@AttributeID", Session("gblAttributeID"))
            '    sqlCmd.Parameters.Add("@AttributeValue", Session("gblAttributeValue"))
            '    Session("gblAttributeID") = ""
            '    Session("gblAttributeValue") = ""
            'End If

            Dim da As New SqlDataAdapter(sqlCmd)
            da.Fill(Attributes)
            da.Dispose()

            If Attributes.Rows.Count > 0 Then
                dgSearchAttributes.DataSource = Attributes
                dgSearchAttributes.Visible = True
                dgSearchAttributes.DataBind()
            Else
                dgSearchAttributes.Visible = True
            End If

        Catch ex As Exception

            ShowPopupError("An error occured while loading the selected document attributes.", True)

            Dim conMaster As SqlClient.SqlConnection
            conMaster = Functions.BuildMasterConnection
            Functions.LogException(mobjUser.UserId.ToString, mobjUser.AccountId.ToString, "", "Attribute Load Error, Advanced Search, Documents Loaded: " & sb.ToString & " " & ex.Message, ex.StackTrace, Server.MachineName, "", Request.UrlReferrer.ToString, conMaster, True)

        Finally

            If treeSearch.CheckedNodes.Length > 0 Then
                System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType(), "searchMode", "searchMode();", True)
            End If

            System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType(), "stopProgressDisplay", "stopProgressDisplay();", True)

        End Try

    End Sub

    Private Sub btnSort_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSort.Click

        CurrentPage.Value = "1"

        Select Case SearchType.Value
            Case "A"
                AdvancedSearch()
            Case "Q"
                QuickSearch()
            Case "F"
                BrowseTreeFolder(BrowseFolder.Value)
        End Select

    End Sub

    Private Sub btnPaging_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPaging.Click

        Select Case SearchType.Value
            Case "A"
                AdvancedSearch()
            Case "Q"
                QuickSearch()
            Case "F"
                BrowseTreeFolder(BrowseFolder.Value)
        End Select

    End Sub

    Private Sub btnRestore_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRestore.Click

        Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
        Me.mconSqlImage.Open()

        Dim blnItemsSelected As Boolean = False
        Dim Documents As New DataTable
        Documents.Columns.Add("ImageId")

        Dim sqlCmd As SqlCommand
        Dim sqlDa As SqlDataAdapter
        Dim sqlBld As SqlCommandBuilder

        Dim items As New Collection

        Try

            For Each dgi As DataGridItem In dgResults.Items

                Dim chkMass As CheckBox = dgi.FindControl("chkMass")
                Dim lblImageId As Label = dgi.FindControl("DocNo")

                'Dim 'lnkFileNameStartup As LinkButton = dgi.FindControl("'lnkFile")
                'TODO If Not IsNothing('lnkFileNameStartup) Then 'RegisterExcludeControl('lnkFileNameStartup)

                Dim ImageId As Integer = lblImageId.Text.Replace("<b>", "").Replace("</b>", "")

                If chkMass.Checked And IsNumeric(ImageId) And dgi.Visible = True Then
                    blnItemsSelected = True
                    Dim dr As DataRow = Documents.NewRow
                    dr("ImageId") = ImageId
                    Documents.Rows.Add(dr)
                    items.Add(dgi)
                End If

            Next

            If blnItemsSelected Then

                Dim tempTableName As String = "##" & System.Guid.NewGuid.ToString.Replace("-", "")
                sqlCmd = New SqlCommand("CREATE TABLE " & tempTableName & " (ImageId int)", Me.mconSqlImage)
                sqlCmd.ExecuteNonQuery()

                sqlCmd = New SqlCommand("SELECT * FROM " & tempTableName, Me.mconSqlImage)
                sqlDa = New SqlDataAdapter(sqlCmd)
                sqlBld = New SqlCommandBuilder(sqlDa)

                sqlDa.Update(Documents)

                sqlCmd = New SqlCommand("SMMassRestore", Me.mconSqlImage)
                sqlCmd.CommandType = CommandType.StoredProcedure
                sqlCmd.Parameters.Clear()
                sqlCmd.Parameters.Add("@DocListTable", tempTableName)
                sqlCmd.Parameters.Add("@UserID", Me.mobjUser.ImagesUserId)

                Dim Results As New DataTable
                sqlDa = New SqlDataAdapter(sqlCmd)
                sqlDa.Fill(Results)

                If Results.Rows.Count > 0 Then

                    'Errors deleting
                    Dim sb As New System.Text.StringBuilder
                    sb.Append("Errors occured while restoring the following documents: ")
                    Dim count As Integer = 0
                    For Each dr As DataRow In Results.Rows
                        count += 1
                        sb.Append(dr("ImageId"))
                        If count <> Results.Rows.Count Then sb.Append(", ")
                    Next

                    ShowPopupError(sb.ToString, False)

                Else
                    'Success

                    For Each dgi As DataGridItem In items
                        dgi.Visible = False
                    Next

                    ''Check if mass action icons should remain visible
                    'HideMassIcons()
                    'For Each dgi As DataGridItem In dgResults.Items
                    '    Dim lblImageType As Label = dgi.FindControl("ImageType")
                    '    If dgi.Visible Then
                    '        Select Case CType(lblImageType.Text, ImageTypes)
                    '            Case Constants.ImageTypes.SCAN
                    '                ShowMassIcons()
                    '                Exit For
                    '            Case Constants.ImageTypes.ATTACHMENT
                    '                ShowMassIcons()
                    '                Exit For
                    '        End Select
                    '    End If
                    'Next

                End If

            Else

                ShowPopupError("Please select one or more documents to restore.", False)
                Exit Sub

            End If

        Catch ex As Exception

            ShowPopupError("An error occured while restoring the selected documents.", True)

            Dim sb As New System.Text.StringBuilder
            For Each dr As DataRow In Documents.Rows
                sb.Append(dr("ImageId") & ", ")
            Next

            Dim conMaster As SqlClient.SqlConnection
            conMaster = Functions.BuildMasterConnection
            Functions.LogException(mobjUser.UserId.ToString, mobjUser.AccountId.ToString, "", "Restore Files, ImageId's: " & sb.ToString & " " & ex.Message, ex.StackTrace, Server.MachineName, "", Request.UrlReferrer.ToString, conMaster, True)

        Finally

            If Me.mconSqlImage.State <> ConnectionState.Closed Then
                Me.mconSqlImage.Close()
                Me.mconSqlImage.Dispose()
            End If

            System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.NewGuid.ToString, "stopProgressDisplay();", True)

        End Try

    End Sub

    Private Sub btnHome_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnHome.Click
        Session("SearchType") = "Home"
    End Sub

    Private Sub btnRecycleCheck_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnRecycleCheck.Click

        Dim trnSql As SqlClient.SqlTransaction

        Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
        Me.mconSqlImage.Open()

        Try

            If Not IsDate(recycleTime.Text) Then
                lblRecycleError.Text = "Please specify a valid date."
                lblRecycleError.Visible = True
                Exit Sub
            End If

            Dim sqlCmd As New SqlClient.SqlCommand("SMEmptyRecycleBin", Me.mconSqlImage)
            sqlCmd.CommandType = CommandType.StoredProcedure
            sqlCmd.Parameters.Clear()

            Dim Parm As New SqlClient.SqlParameter
            Parm = New SqlParameter
            Parm.ParameterName = "@DocumentCountDeleted"
            Parm.SqlDbType = SqlDbType.Int
            Parm.Direction = ParameterDirection.Output

            sqlCmd.Parameters.Add(Parm)
            sqlCmd.Parameters.Add("@UserID", Me.mobjUser.ImagesUserId)

            If rdoRecyleAll.Checked Then
                sqlCmd.Parameters.Add("@DeleteOlderThanDate", CDate("1/1/2099"))
            Else
                sqlCmd.Parameters.Add("@DeleteOlderThanDate", CDate(recycleTime.Text))
            End If

            sqlCmd.Parameters.Add("@Mode", CInt(emptyMode.Value))

            Select Case emptyMode.Value
                Case "0"

                    sqlCmd.ExecuteNonQuery()

                    If sqlCmd.Parameters("@DocumentCountDeleted").Value = 0 Then
                        System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.NewGuid.ToString, "alert('No documents matched this date range.');", True)
                    Else
                        System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.NewGuid.ToString, "if (confirm('This action will permanently delete " & sqlCmd.Parameters("@DocumentCountDeleted").Value & " documents. Do you want to continue?')) { document.getElementById('emptyMode').value = '1';document.getElementById('btnRecycleCheck').click(); } else { hideRecycleBin(); }", True)
                    End If

                Case "1"

                    trnSql = Me.mconSqlImage.BeginTransaction
                    sqlCmd.Transaction = trnSql
                    sqlCmd.ExecuteNonQuery()
                    trnSql.Commit()

                    System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.NewGuid.ToString, "document.getElementById('emptyMode').value = '0';hideRecycleBin();browseFolder(-3);", True)

                Case Else

            End Select


        Catch ex As Exception

            If emptyMode.Value = "1" Then trnSql.Rollback()

            lblRecycleError.Text = "An error has occured. Please try again."
            lblRecycleError.Visible = True

            Dim conMaster As SqlClient.SqlConnection
            conMaster = Functions.BuildMasterConnection
            Functions.LogException(mobjUser.UserId.ToString, mobjUser.AccountId.ToString, "", "Empty Recycle Bin, Date: " & recycleTime.Text & ", Mode " & emptyMode.Value & " " & ex.Message, ex.StackTrace, Server.MachineName, "", Request.UrlReferrer.ToString, conMaster, True)

        Finally

            If Me.mconSqlImage.State <> ConnectionState.Closed Then
                Me.mconSqlImage.Close()
                Me.mconSqlImage.Dispose()
            End If

        End Try

    End Sub

    Private Function GetNiceDate(ByVal ModifiedDate As DateTime) As String

        Dim userTime As DateTime = Now.AddHours(Me.mobjUser.TimeDiffFromGMT)

        ' Get time span elapsed since the date.
        Dim s As TimeSpan = userTime.Subtract(ModifiedDate)

        ' Get total number of days elapsed.
        Dim dayDiff As Integer = CInt(s.TotalDays)

        ' Get total number of seconds elapsed.
        Dim secDiff As Integer = CInt(s.TotalSeconds)

        ' Don't allow out of range values.
        'If dayDiff < 0 OrElse dayDiff >= 31 Then
        '    Return Nothing
        'End If

        'Handle same-day times.
        If dayDiff = 0 Then
            Return "<b>Today</b>" & Space(1) & ModifiedDate.ToLongTimeString
        ElseIf dayDiff = 1 Then
            Return "<b>Yesterday</b>" & Space(1) & ModifiedDate.ToLongTimeString
        Else
            Return String.Format("{0} {1}", ModifiedDate.ToShortDateString, ModifiedDate.ToLongTimeString)
        End If

        ' Handle previous days.
        'If dayDiff = 1 Then
        '    Return "yesterday"
        'End If

        'If dayDiff < 7 Then
        '    Return String.Format("{0} {1}", ModifiedDate.DayOfWeek.ToString, ModifiedDate.ToLongTimeString)
        'End If

        'If dayDiff < 31 Then
        '    Return String.Format("{0} {1}", ModifiedDate.ToShortDateString, ModifiedDate.ToLongTimeString)
        'End If

        'PadDate(CDate(e.Item.DataItem("ModifiedDate")).ToShortDateString) & " " & )

    End Function

    Private Sub download_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles download.Click

        Dim VersionId As Integer = dlID.Value.Split(",")(0)
        Dim ImageType As ImageTypes = dlID.Value.Split(",")(1)
        Dim fileName As String = dlID.Value.Split(",")(2)

        Try

            Select Case ImageType

                Case ImageTypes.ATTACHMENT

                    'VersionId = CInt(CType(e.Item.FindControl("VersionId"), Label).Text)

                    'Create file and push it to the user
                    Dim objAttachments As New FileAttachments(Functions.BuildConnection(Me.mobjUser))

                    Dim dt As New DataTable
                    dt = objAttachments.GetFile(VersionId)

                    Dim imgByte As Byte() = CType(dt.Rows(0)("Data"), Byte())

                    Response.Clear()

                    Response.AddHeader("Content-Disposition", "attachment; filename=" & Server.UrlEncode(fileName).Replace("+", "%20"))
                    Response.AddHeader("Content-Length", imgByte.Length)

                    If fileName.ToLower.EndsWith(".pdf") Then
                        Response.ContentType = "application/pdf"
                    Else
                        Response.ContentType = "application/octet-stream"
                    End If

                    Response.BinaryWrite(imgByte)
                    Response.End()

                Case ImageTypes.SCAN

                    Dim file As String = Server.MapPath("./tmp/" & Functions.ExportPDF(Me.mobjUser, VersionId, 1, 10000, Me, False))

                    Response.Clear()

                    Response.AddHeader("Content-Disposition", "attachment; filename=docAssist.pdf")
                    Response.AddHeader("Content-Length", New System.IO.FileInfo(file).Length)
                    Response.ContentType = "application/pdf"
                    Response.WriteFile(file)
                    Response.End()

            End Select

            'Track attachment
            Try
                Accucentric.docAssist.Web.Data.Functions.TrackPageRequest(Me.mobjUser.UserId, Me.mobjUser.AccountId, "Attachment View", "Out", VersionId, "", Functions.GetMasterConnectionString)
            Catch ex As Exception

            End Try

            'Log recent item
            Try
                Dim blnReturn As Boolean = Functions.TrackRecentItem(Me.mobjUser, "VW", VersionId)
            Catch ex As Exception

            End Try

        Catch ex As Exception

            If ex.Message <> "Thread was being aborted." Then

                Dim conMaster As SqlClient.SqlConnection
                conMaster = Functions.BuildMasterConnection
                Functions.LogException(mobjUser.UserId.ToString, mobjUser.AccountId.ToString, "", "Result File Download Error, VersionId: " & VersionId & " " & ex.Message, ex.StackTrace, Server.MachineName, "", Request.UrlReferrer.ToString, conMaster, True)

                RegisterStartupScript("DownloadError", "<script>showInfoBox('<b>An error occured while downloading this file. Please refresh the page and try again.</b><br><br>Our staff has been notified of the error and is working to correct any possible issues.');</script>")

            End If

        Finally

        End Try

    End Sub

    Private Sub recordSearchType(ByVal searchType As SearchType)
        Session("LastSearchType") = CInt(searchType)
    End Sub

End Class
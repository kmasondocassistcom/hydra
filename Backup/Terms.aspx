<%@ Page Language="vb" AutoEventWireup="false" Codebehind="Terms.aspx.vb" Inherits="docAssistWeb.Terms"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML xmlns:o>
	<HEAD>
		<title>docAssist - Open a Support Case</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="css/core.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body>
		<form id="Terms" method="post" runat="server">
			<TABLE width="100%">
				<TBODY>
					<TR>
						<TD colSpan="2">
							<p>Before you can start using docAssist you must agree to our terms of service. 
								Please review the links below.
								<ul>
									<li>
										<p><a target=_blank href="http://www.docassist.com/termsofservice.html">Terms of Service</a></p>
									<li>
										<p><a target=_blank href="http://www.docassist.com/html/privacy.html">Privacy Statement</a></p>
									</li>
								</ul>
							<p align="right">
								<asp:Literal id="litClose" runat="server"></asp:Literal>
								<BUTTON class="btn primary" id="OpenCase" type="button" runat="server"><SPAN><SPAN>Accept 
											Terms of Service</SPAN></SPAN></BUTTON>
							</p>
						</TD>
					</TR>
				</TBODY>
			</TABLE>
		</form>
	</body>
</HTML>

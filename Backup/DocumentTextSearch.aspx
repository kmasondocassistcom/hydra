<%@ Page Language="vb" AutoEventWireup="false" Codebehind="DocumentTextSearch.aspx.vb" Inherits="docAssistWeb.DocumentTextSearch"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
  <HEAD>
		<title></title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
  </HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<table cellpadding="0" cellspacing="0" border="0" width="100%">
				<tr>
					<td valign="middle">
						<P class="PageContent">&nbsp; Search:
							<asp:TextBox id="txtSearch" runat="server" CssClass="PageContent" Width="250px" Height="23px"></asp:TextBox>&nbsp;
							<input type="button" value="Search" onclick="SearchDocument();" id="btnSearch" runat="server">&nbsp;
							<a class="PageContent" href="javascript:RemoveFilter();" runat="server" id="lnkClearFilter">
								Remove Filter</a>
						</P>
					</td>
				</tr>
			</table>
		</form>
		<script language="javascript">
			function RemoveFilter()
			{
				parent.location.href = 'DocumentText.aspx?Search=0Clear0';
			}
			function SearchDocument()
			{
				var space = "test "+"1 ";
				parent.location.href = 'DocumentText.aspx?Search=' + document.all.txtSearch.value.replace(/^\s+|\s+$/g, '');
			}
			function LoadSearch(val)
			{
				document.all.txtSearch.value = val;
			}
			function SubmitForm()
			{
				if (event.keyCode == 13)
				{
					event.cancelBubble = true;
					event.returnValue = false;
					document.all.btnSearch.click();
				}
			}
		</script>
	</body>
</HTML>

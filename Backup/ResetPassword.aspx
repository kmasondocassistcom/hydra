<%@ Register TagPrefix="uc1" TagName="docHeader" Src="Controls/docHeader.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ResetPassword.aspx.vb" Inherits="docAssistWeb.ResetPassword"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML xmlns:o>
	<HEAD>
		<title>docAssist - Forgot your password?</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="css/core.css" type="text/css" rel="stylesheet">
		<style type="text/css">
			BODY { BACKGROUND-IMAGE: url(images/background.gif); BACKGROUND-REPEAT: repeat-x }
		</style>
	</HEAD>
	<body>
		<form id="Support" method="post" runat="server">
			<table width="300" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="20">&nbsp;</td>
					<td><img src="images/logoBug.gif" alt="docAssist" width="185" height="89"></td>
				</tr>
			</table>
			<table width="100%">
				<tr>
					<td>
						<table width="100%">
							<tr>
								<td align="center">
									<table width="700">
										<TBODY>
											<tr>
												<td width="100%">
													<h1>Forgot your password?</h1>
												</td>
												<td align="right">
													<img src="Images/icon_sets/process.png">
												</td>
											</tr>
											<tr>
											<tr>
												<td colspan="2">
													<asp:label id="lblError" runat="server" Visible="False" CssClass="error"></asp:label>
												</td>
											</tr>
											<TR>
												<td colspan="2">
													<ol id="number">
														<li>
															<h2>Enter your e-mail address below and we will send your login information.</h2>
															<p>
																<table>
																	<tr>
																		<td nowrap width="70">Name:</td>
																		<td width="100%"><asp:textbox id="txtName" runat="server"></asp:textbox></td>
																	</tr>
																	<tr>
																		<td nowrap>E-mail:</td>
																		<td width="100%"><asp:textbox id="txtEmail" runat="server"></asp:textbox></td>
																	</tr>
																	<tr>
																		<td nowrap>Company:</td>
																		<td width="100%"><asp:textbox id="txtCompany" runat="server"></asp:textbox></td>
																	</tr>
																	<tr>
																		<td nowrap>Phone No:</td>
																		<td width="100%"><asp:textbox id="txtPhoneNumber" runat="server"></asp:textbox></td>
																	</tr>
																</table>
															</p>
														<li>
															<h2>What's wrong?</h2>
															<p>
																<table>
																	<tr>
																		<td nowrap width="70">Subject:</td>
																		<td width="100%"><asp:textbox id="txtSubject" runat="server" Width="400px"></asp:textbox></td>
																	</tr>
																</table>
															</p>
														<li>
															<h2>Finally we just need a few details about your case.</h2>
															<h3>Please try to be as descriptive as possible. If there are any steps that we can 
																follow to reproduce your issue please include them. If you have a question 
																about a specific document please provide us with the Document Number. The more 
																information you provide us with, the quicker we can help you.</h3>
															<p>
																<table>
																	<tr valign="baseline">
																		<td><asp:textbox id="txtComments" runat="server" Width="440px" Height="72px" TextMode="MultiLine"></asp:textbox></td>
																		<td align="center">
																			<form action="#">
																				<button type="button" class="btn primary" id="OpenCase" runat="server"><span><span>Open 
																							Case</span></span></button> or <a onclick="javascript:window.history.back(1);">
																					Cancel</a>
																			</form>
																		</td>
																	</tr>
																</table>
															</p>
														</li>
													</ol>
												</td>
											</TR>
								</td>
							</tr>
							<!--
							-->
						</table>
					</td>
				</tr>
			</table>
			</TD></TR></TBODY></TABLE></TD></TR></TABLE>
		</form>
		<script src="scripts/jquery.js" type="text/javascript"></script>
		<script language="javascript">
		/*
			$(document).ready(function(){
				$("#number li").each(function (i) {
					i = i+1;
					$(this).addClass("item"+i);
			});
			 
			});
		*/
		</script>
		</TR></TBODY></TABLE>
	</body>
</HTML>

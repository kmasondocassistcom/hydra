Imports Telerik.WebControls
Imports Accucentric.docAssist
Imports Accucentric.docAssist.Data.Images

Partial Class FolderPermissions
    Inherits System.Web.UI.Page
    
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents chkSenderFolderAccess As System.Web.UI.WebControls.CheckBox

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Dim mconSqlImage As SqlClient.SqlConnection
    Dim mobjUser As Accucentric.docAssist.Web.Security.User
    Private strId As String
    Dim blnInherit As Boolean

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim conSqlMaster As New SqlClient.SqlConnection
        Dim blnEmailModuleInstalled As Boolean = False

        Try

            chkInherit.Attributes("onclick") = "return clearPrompt();"
            txtAddress.Attributes("onkeypress") = "hitReturn();"
            rdoSendersAll.Attributes("onclick") = "disableAdd(true);"
            rdoSendersSpecified.Attributes("onclick") = "disableAdd(false);"
            'chkSaveAttachments.Attributes("onclick") = "disabledChecks(document.all.chkSaveAttachments.checked);"
            chkAllowAccountUsersAccess.Attributes("onclick") = "disableFolderAccess(document.all.chkAllowAccountUsersAccess.checked);"

            Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
            Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))
            Me.mobjUser.AccountId = guidAccountId

            strId = Request.QueryString("Id").Replace("W", "")
            ScreenMode.InnerHtml = "Folder Permissions"
            chkInherit.Text = "Inherit permissions from parent folder."
            chkInherit.Visible = True

            If Not Page.IsPostBack Then

                Dim objFolders As New Folders(Functions.BuildConnection(Me.mobjUser))
                Dim ds As New DataSet

                If strId = "Unassigned" Then
                    ds = objFolders.SecurityFolderGet(0)
                    chkInherit.Checked = False
                    chkInherit.Visible = False

                    lblName.Text = "Unassigned"
                    Tabs.Visible = False


                Else
                    ds = objFolders.SecurityFolderGet(strId)

                    lblName.Text = ds.Tables(0).Rows(0)("FolderName")

                    If ds.Tables(0).Rows(0)("FolderInherits") Then
                        blnInherit = True
                        dgPermissions.Enabled = False
                        chkInherit.Checked = True
                    End If

                End If

                dgPermissions.DataSource = ds.Tables(1)
                dgPermissions.DataBind()

                Dim dsEmailOptions As DataSet
                'Check to see if the email module is enabled first
                If Functions.ModuleSetting(Functions.ModuleCode.INBOUND_EMAIL, Me.mobjUser) = True And Me.mobjUser.SysAdmin = True And strId <> "Unassigned" Then

                    Tabs.Visible = True

                    'Get EmailPrefix for account
                    blnEmailModuleInstalled = True
                    conSqlMaster = Functions.BuildMasterConnection

                    Dim objEmailSettings As New Folders(conSqlMaster, False)
                    Dim dsEmail As DataSet
                    Dim strEmailPrefix As String

                    strEmailPrefix = objEmailSettings.EmailPrefixGet(Me.mobjUser.AccountId.ToString, strId)

                    If strEmailPrefix <> "" Then
                        lblEmailDomain.Text = "." & strEmailPrefix.ToLower & "@email.docassist.com" 'TODO: make HOT LINK in UI!

                        'Get dataset of email options
                        dsEmailOptions = objEmailSettings.EmailOptionsFolderGet(Me.mobjUser.AccountId.ToString, strId)
                        'TODO: Load options into UI here

                        If dsEmailOptions.Tables(0).Rows.Count > 0 Then

                            chkEmailEnabled.Checked = dsEmailOptions.Tables(0).Rows(0)("Enabled")

                            If dsEmailOptions.Tables(0).Rows(0)("FilterWhiteListEnabled") = True Then
                                rdoSendersSpecified.Checked = True
                                'ReturnScripts.Add("disableAdd(true);")
                            Else
                                rdoSendersAll.Checked = True
                                'ReturnScripts.Add("disableAdd(false);")
                            End If

                            txtAlias.Text = dsEmailOptions.Tables(0).Rows(0)("Alias")
                            chkAllowAccountUsersAccess.Checked = dsEmailOptions.Tables(0).Rows(0)("FilterAllowAccountUsers")
                            chkRequireAccountUserAccess.Checked = dsEmailOptions.Tables(0).Rows(0)("AccountUsersRequireAccess")
                            cmbDocumentTypes.SelectedValue = dsEmailOptions.Tables(0).Rows(0)("DocumentId")
                            chkSaveEmailMessage.Checked = dsEmailOptions.Tables(0).Rows(0)("SaveMessage")
                            'chkSaveAttachments.Checked = dsEmailOptions.Tables(0).Rows(0)("SaveAttachments")
                            chkSendNoAttachmentWarning.Checked = dsEmailOptions.Tables(0).Rows(0)("SendNoAttachmentWarning")
                            chkConvertToImages.Checked = dsEmailOptions.Tables(0).Rows(0)("ImageAttachments")
                            chkSendEmailConfirmation.Checked = dsEmailOptions.Tables(0).Rows(0)("SendConfirmation")

                        End If

                        If dsEmailOptions.Tables(1).Rows.Count > 0 Then
                            lstSendersAllowed.DataSource = dsEmailOptions.Tables(1)
                            lstSendersAllowed.DataTextField = "White"
                            lstSendersAllowed.DataValueField = "EmailID"
                            lstSendersAllowed.DataBind()
                        End If


                        'Load documents
                        Dim conCompany As SqlClient.SqlConnection
                        conCompany = Functions.BuildConnection(Me.mobjUser)
                        conCompany.Open()
                        Dim sqlCmd As SqlClient.SqlCommand = New SqlClient.SqlCommand("acsDocumentsByCabinetFolder", conCompany)
                        sqlCmd.CommandType = CommandType.StoredProcedure
                        sqlCmd.Parameters.Add("@UserName", Me.mobjUser.UserId)
                        sqlCmd.Parameters.Add("@CabinetId", "0")
                        sqlCmd.Parameters.Add("@FolderId", "0")
                        Dim daSql As New SqlClient.SqlDataAdapter(sqlCmd)
                        Dim dt As New DataTable
                        daSql.Fill(dt)

                        cmbDocumentTypes.DataSource = dt
                        cmbDocumentTypes.DataTextField = "DocumentName"
                        cmbDocumentTypes.DataValueField = "DocumentId"
                        cmbDocumentTypes.DataBind()



                        'Check to see if workflow is installed
                        If Functions.ModuleSetting(Functions.ModuleCode.WORKFLOW, Me.mobjUser) = True Then

                            divWorkflow.Visible = True

                            'Load all workflow/queues into drop downs
                            Dim objWf As New Workflow(Functions.BuildConnection(Me.mobjUser))
                            Dim dsSubmit As DataSet
                            dsSubmit = objWf.WFSubmitterWorkflowsGet(0, 0, CInt(cmbDocumentTypes.SelectedValue))

                            cmbWorkflow.Items.Add(New ListItem("<None>", "-2"))

                            For Each dr As DataRow In dsSubmit.Tables(1).Rows

                                'cmbWorkflow.Items.Add(New ListItem(strCategoryName, "OPTGROUP"))
                                cmbWorkflow.Items.Add(New ListItem(dr("WorkflowName"), dr("WorkflowId")))

                            Next

                            cmbWorkflow.SelectedValue = -2
                            ''Load existing WF data

                            If dsEmailOptions.Tables(0).Rows.Count > 0 Then

                                Try
                                    cmbWorkflow.SelectedValue = dsEmailOptions.Tables(0).Rows(0)("WorkflowID")
                                Catch ex As Exception

                                End Try


                                If cmbWorkflow.SelectedValue = -2 Or cmbWorkflow.SelectedValue = "-2" Then
                                    cmbQueue.Enabled = False
                                    cmbQueue.Items.Clear()
                                    chkUrgent.Checked = False
                                    chkUrgent.Enabled = False

                                Else
                                    cmbQueue.Enabled = True
                                    cmbQueue.Items.Clear()
                                    chkUrgent.Enabled = True


                                    Dim dvFilter As New DataView(dsSubmit.Tables(2))
                                    dvFilter.RowFilter = "WorkflowId=" & cmbWorkflow.SelectedValue

                                    cmbQueue.DataTextField = "QueueName"
                                    cmbQueue.DataValueField = "QueueId"
                                    cmbQueue.DataSource = dvFilter
                                    cmbQueue.DataBind()

                                End If

                                Try
                                    cmbQueue.SelectedValue = dsEmailOptions.Tables(0).Rows(0)("QueueID")
                                    chkUrgent.Checked = dsEmailOptions.Tables(0).Rows(0)("Urgent")
                                Catch
                                End Try
                            End If



                            'Set workflow/queue from record

                            If cmbWorkflow.SelectedValue = -2 Or cmbWorkflow.SelectedValue = "-2" Then
                                cmbQueue.Enabled = False
                                cmbQueue.Items.Clear()
                                chkUrgent.Checked = False
                                chkUrgent.Enabled = False
                            End If

                        Else 'No workflow

                            divWorkflow.Visible = False

                        End If

                    End If


                Else
                    Tabs.Visible = False
                End If

            End If

        Catch ex As Exception

        Finally

            'ReturnControls.Add(Tabs)

            If conSqlMaster.State <> ConnectionState.Closed Then
                conSqlMaster.Close()
                conSqlMaster.Dispose()
            End If

        End Try

    End Sub

    Private Sub dgPermissions_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgPermissions.ItemDataBound

        Try

            Dim ddlFilePermissions As DropDownList = CType(e.Item.FindControl("ddlFilePermissions"), DropDownList)
            Dim ddlFolderPermissons As DropDownList = CType(e.Item.FindControl("ddlFolderPermissons"), DropDownList)
            Dim lblFileLevel As Label = CType(e.Item.FindControl("lblFileLevel"), Label)
            Dim lblFolderLevel As Label = CType(e.Item.FindControl("lblFolderLevel"), Label)

            If Not IsNothing(ddlFilePermissions) Then
                ddlFilePermissions.SelectedValue = lblFileLevel.Text
                ddlFolderPermissons.SelectedValue = lblFolderLevel.Text
                If blnInherit Then
                    ddlFilePermissions.Enabled = False
                    ddlFolderPermissons.Enabled = False
                Else
                    ddlFilePermissions.Enabled = True
                    ddlFolderPermissons.Enabled = True
                End If
            End If

            If strId = "Unassigned" Then ddlFolderPermissons.Enabled = False

        Catch ex As Exception

        Finally
            'ReturnControls.Add(dgPermissions)
        End Try

    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click

        Dim conSql As New SqlClient.SqlConnection
        Dim conSqlMaster As New SqlClient.SqlConnection
        Dim sqlTran As SqlClient.SqlTransaction
        Dim intReturn As Integer
        Dim intEmailID As Integer

        Try

            If chkEmailEnabled.Checked And Tabs.Visible = True Then
                If txtAlias.Text.Trim = "" Then
                    System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType(), "email", "document.getElementById('tdEmail').click();", True)
                    lblError.Text = "You must provide an email address for this folder."
                    lblError.Visible = True
                    'ReturnControls.Add(lblError)
                    Exit Sub
                End If
            End If

            'If Not chkSaveAttachments.Checked And Not chkSaveEmailMessage.Checked And Tabs.Visible = True And txtAlias.Text.Trim <> "" Then
            '    ReturnScripts.Add("document.getElementById('tdEmail').click();")
            '    lblError.Text = "You must select the 'Save Attachments' option to save."
            '    lblError.Visible = True
            '    'ReturnControls.Add(lblError)
            '    Exit Sub
            'End If

            'Build temp table
            Dim dtPermissions As New DataTable
            dtPermissions.Columns.Add(New DataColumn("FolderId"))
            dtPermissions.Columns.Add(New DataColumn("GroupId"))
            dtPermissions.Columns.Add(New DataColumn("SecValue"))
            dtPermissions.Columns.Add(New DataColumn("SecLevel"))

            If strId = "Unassigned" Then strId = 0

            For Each dgRow As DataGridItem In dgPermissions.Items

                Dim ddlFilePermissions As DropDownList = CType(dgRow.FindControl("ddlFilePermissions"), DropDownList)
                Dim ddlFolderPermissons As DropDownList = CType(dgRow.FindControl("ddlFolderPermissons"), DropDownList)
                Dim lblGroupId As Label = CType(dgRow.FindControl("lblGroupId"), Label)

                Dim dr As DataRow = dtPermissions.NewRow
                dr("FolderId") = strId
                dr("GroupId") = lblGroupId.Text
                dr("SecValue") = CInt(Functions.FolderSecurity.FILES)
                dr("SecLevel") = ddlFilePermissions.SelectedValue
                dtPermissions.Rows.Add(dr)

                dr = dtPermissions.NewRow
                dr("FolderId") = strId
                dr("GroupId") = lblGroupId.Text
                dr("SecValue") = CInt(Functions.FolderSecurity.FOLDER)
                dr("SecLevel") = ddlFolderPermissons.SelectedValue
                dtPermissions.Rows.Add(dr)

            Next

            If chkInherit.Checked Then
                Dim dr As DataRow = dtPermissions.NewRow
                dr("FolderId") = strId
                dr("GroupId") = -1
                dr("SecValue") = CInt(Functions.FolderSecurity.INHERIT)
                dr("SecLevel") = IIf(chkInherit.Checked, 1, 0)
                dtPermissions.Rows.Add(dr)
            End If

            conSql = Functions.BuildConnection(Me.mobjUser)
            conSql.Open()

            Dim tempTableName As String = "##" & System.Guid.NewGuid.ToString.Replace("-", "")
            Dim sqlCmd As New SqlClient.SqlCommand("CREATE TABLE " & tempTableName & " (FolderID int NULL, GroupID int NULL, SecValue int NULL, SecLevel int NULL)", conSql)
            sqlCmd.ExecuteNonQuery()

            sqlCmd = New SqlClient.SqlCommand("SELECT * FROM " & tempTableName, conSql)
            Dim sqlDa As New SqlClient.SqlDataAdapter(sqlCmd)
            Dim sqlBld As New SqlClient.SqlCommandBuilder(sqlDa)

            sqlDa.Update(dtPermissions)

            sqlTran = conSql.BeginTransaction

            Dim objFolders As New Folders(conSql, False, sqlTran)

            intReturn = objFolders.SecurityFolderInsert(tempTableName)

            sqlTran.Commit()

            If Functions.ModuleSetting(Functions.ModuleCode.INBOUND_EMAIL, Me.mobjUser) = True And Me.mobjUser.SysAdmin = True And strId <> 0 And txtAlias.Text.Trim <> "" Then

                If txtAlias.Text.Trim = "" And chkEmailEnabled.Checked Then
                    System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType(), "emailclick", "document.getElementById('tdEmail').click();", True)
                    lblError.Text = "You must provide a valid e-mail alias."
                    lblError.Visible = True
                    'ReturnControls.Add(lblError)
                    Exit Sub
                End If

                conSqlMaster = Functions.BuildMasterConnection
                sqlTran = conSqlMaster.BeginTransaction

                Dim objEmailSettings As New Folders(conSqlMaster, False, sqlTran)

                intEmailID = objEmailSettings.EmailFolderInsert(Me.mobjUser.AccountId.ToString, strId, txtAlias.Text.Trim, chkEmailEnabled.Checked, chkSendEmailConfirmation.Checked, chkSaveEmailMessage.Checked, True, chkConvertToImages.Checked, chkSendNoAttachmentWarning.Checked, rdoSendersSpecified.Checked, chkAllowAccountUsersAccess.Checked, chkRequireAccountUserAccess.Checked, IIf(IsNumeric(cmbDocumentTypes.SelectedValue), cmbDocumentTypes.SelectedValue, 0), IIf(cmbWorkflow.SelectedValue <= 0, Nothing, cmbWorkflow.SelectedValue), IIf(cmbQueue.SelectedValue <> "", cmbQueue.SelectedValue, Nothing), chkUrgent.Checked)

                If intEmailID < 0 Then
                    sqlTran.Rollback()
                    System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType(), "emailclick", "document.getElementById('tdEmail').click();", True)
                    lblError.Text = "This alias is already in use on another folder. Please choose a different one."
                    lblError.Visible = True
                    'ReturnControls.Add(lblError)
                    Exit Sub
                End If

                'Save White List
                If intEmailID > 0 Then
                    If lstSendersAllowed.Items.Count > 0 Then
                        'Clear WhilteList
                        intReturn = objEmailSettings.EmailWhiteListInsert(intEmailID, "REMOVEALL")

                        Dim lstitem As ListItem
                        For Each lstitem In lstSendersAllowed.Items
                            intReturn = objEmailSettings.EmailWhiteListInsert(intEmailID, lstitem.Text)
                        Next
                    End If
                End If

                sqlTran.Commit()

            End If

            System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType(), "close", "window.close();", True)

        Catch ex As Exception

            If Not IsNothing(sqlTran) Then sqlTran.Rollback()

#If DEBUG Then
            lblError.Text = ex.Message
#Else
            lblError.Text = "An error has occured while saving. Please try again. If the problem continues please contact support."
#End If

            lblError.Visible = True
            'ReturnControls.Add(lblError)

        Finally

            If conSql.State <> ConnectionState.Closed Then
                conSql.Close()
                conSql.Dispose()
            End If
            If conSqlMaster.State <> ConnectionState.Closed Then
                conSqlMaster.Close()
                conSqlMaster.Dispose()
            End If

        End Try

    End Sub

    Private Sub chkInherit_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkInherit.CheckedChanged
        System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType(), "showSecurity", "showSecurity();", True)
    End Sub

    Private Sub GridItems(ByVal Setting As Boolean)

        Try
            For Each dgi As DataGridItem In dgPermissions.Items

                Dim ddlFilePermissions As DropDownList = CType(dgi.FindControl("ddlFilePermissions"), DropDownList)
                Dim ddlFolderPermissons As DropDownList = CType(dgi.FindControl("ddlFolderPermissons"), DropDownList)

                If Not IsNothing(ddlFilePermissions) Then
                    ddlFilePermissions.Enabled = Setting
                    ddlFolderPermissons.Enabled = Setting
                End If

            Next

            dgPermissions.Enabled = Setting

        Catch ex As Exception

        Finally
            'ReturnControls.Add(dgPermissions)
        End Try

    End Sub

    Private Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click

        Try

            Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)

            Dim objFolders As New Folders(Me.mconSqlImage, False)

            blnInherit = chkInherit.Checked

            GridItems(IIf(blnInherit, False, True))

            Dim dsPerms As New DataSet

            If chkInherit.Checked Then
                dsPerms = objFolders.SecurityFolderPermissionsGet(strId)
            Else
                dsPerms = objFolders.SecurityFolderGet(strId)
            End If

            dgPermissions.DataSource = dsPerms.Tables(1)
            dgPermissions.DataBind()

        Catch ex As Exception

        Finally

            If Me.mconSqlImage.State <> ConnectionState.Closed Then
                Me.mconSqlImage.Close()
                Me.mconSqlImage.Dispose()
            End If

            'ReturnControls.Add(dgPermissions)


        End Try

    End Sub

    Private Sub btnAddEmail_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddEmail.Click
        lstSendersAllowed.Items.Add(txtAddress.Text)
        txtAddress.Text = ""
        System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType(), "addEmail", "document.getElementById('tdSecurity').className = 'unselectedTab';document.getElementById('tdEmail').className = 'selectedTab';showEmail();", True)
    End Sub

    Private Sub btnRemove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemove.Click
        lstSendersAllowed.Items.Remove(lstSendersAllowed.SelectedItem)
        System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType(), "removeEmail", "showEmail();", True)
    End Sub

    Private Sub cmbDocumentTypes_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbDocumentTypes.SelectedIndexChanged
        'Load all workflow/queues into drop downs
        Dim objWf As New Workflow(Functions.BuildConnection(Me.mobjUser))
        Dim dsSubmit As DataSet
        dsSubmit = objWf.WFSubmitterWorkflowsGet(0, 0, CInt(cmbDocumentTypes.SelectedValue))
        cmbWorkflow.Items.Clear()
        cmbWorkflow.Items.Add(New ListItem("<None>", "-2"))

        For Each dr As DataRow In dsSubmit.Tables(1).Rows

            'cmbWorkflow.Items.Add(New ListItem(strCategoryName, "OPTGROUP"))
            cmbWorkflow.Items.Add(New ListItem(dr("WorkflowName"), dr("WorkflowId")))

        Next

        cmbWorkflow.SelectedValue = -2

        If cmbWorkflow.SelectedValue = -2 Or cmbWorkflow.SelectedValue = "-2" Then
            cmbQueue.Enabled = False
            cmbQueue.Items.Clear()
            chkUrgent.Checked = False
            chkUrgent.Enabled = False
        End If

        System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Name", "document.getElementById('tdSecurity').className = 'unselectedTab';document.getElementById('tdEmail').className = 'selectedTab';showEmail();", True)

    End Sub

    Private Sub cmbWorkflow_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbWorkflow.SelectedIndexChanged

        If cmbWorkflow.SelectedValue = -2 Or cmbWorkflow.SelectedValue = "-2" Then
            cmbQueue.Enabled = False
            cmbQueue.Items.Clear()
            chkUrgent.Checked = False
            chkUrgent.Enabled = False

        Else
            cmbQueue.Enabled = True
            cmbQueue.Items.Clear()
            chkUrgent.Enabled = True

            Dim objWf As New Workflow(Functions.BuildConnection(Me.mobjUser))
            Dim dsSubmit As DataSet
            dsSubmit = objWf.WFSubmitterWorkflowsGet(0, 0, CInt(cmbDocumentTypes.SelectedValue))
            Dim dvFilter As New DataView(dsSubmit.Tables(2))
            dvFilter.RowFilter = "WorkflowId=" & cmbWorkflow.SelectedValue

            cmbQueue.DataTextField = "QueueName"
            cmbQueue.DataValueField = "QueueId"
            cmbQueue.DataSource = dvFilter
            cmbQueue.DataBind()

        End If

        System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType(), "emailtab", "document.getElementById('tdSecurity').className = 'unselectedTab';document.getElementById('tdEmail').className = 'selectedTab';showEmail();", True)

    End Sub

End Class
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ManageMapAddDetailMapChange.aspx.vb" Inherits="docAssistWeb.ManageMapAddDetailMapChange"%>
<%@ Register TagPrefix="uc1" TagName="AdminNav" Src="Controls/AdminNav.ascx" %>
<%@ Register TagPrefix="uc1" TagName="docHeader" Src="Controls/docHeader.ascx" %>
<%@ Register TagPrefix="uc1" TagName="TopRight" Src="Controls/TopRight.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>docAssist - Manage Integration</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<TABLE height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TBODY>
					<TR>
						<TD width="100%" colSpan="5"><uc1:docheader id="docHeader" runat="server"></uc1:docheader></TD>
					</TR>
					<TR vAlign="middle" height="100%">
						<TD class="Navigation1" style="BORDER-RIGHT: #cccccc thin solid" vAlign="top" height="100%"><uc1:adminnav id="AdminNav1" runat="server"></uc1:adminnav><IMG height="1" src="dummy.gif" width="120" DESIGNTIMEDRAGDROP="101"></TD>
						<TD vAlign="top" align="center" height="100%"><BR>
							<table cellSpacing="0" cellPadding="0" width="100%" border="0">
								<tr>
									<td style="FONT-SIZE: 9pt; FONT-FAMILY: Verdana"><IMG height="1" src="dummy.gif" width="35">Manage 
										Integration</td>
								</tr>
							</table>
							<table height="100%" cellSpacing="0" cellPadding="0" width="100%">
					</TR>
					<tr height="50%">
					</tr>
					<tr height="15" width="100%">
						<td></td>
						<td align="right"><asp:label id="Label3" runat="server" Visible="False" Font-Names="Verdana" Font-Size="8.25pt"
								Height="3px" Width="50px">Application:</asp:label></td>
						<td>&nbsp;
							<asp:label id="lblCurrent" runat="server" Visible="False" Font-Names="Verdana" Font-Size="8.25pt"
								Height="2px" Width="100%"></asp:label></td>
						<td></td>
					</tr>
					<tr height="15" width="100%">
						<td></td>
						<td style="WIDTH: 72px" align="right"><asp:label id="Label4" runat="server" Font-Names="Verdana" Font-Size="8.25pt" Height="3px"
								Width="41px">Application/Document:</asp:label></td>
						<TD>
							<P>&nbsp;
								<asp:label id="lblDocument" runat="server" Font-Names="Verdana" Font-Size="8.25pt" Height="2px"
									Width="100%"></asp:label></P>
						</TD>
						<TD width="50%"></TD>
					<TR height="15" width="100%">
						<td style="HEIGHT: 21px" width="50%"></td>
						<td style="WIDTH: 72px; HEIGHT: 21px" align="left">&nbsp;
						</td>
						<td style="HEIGHT: 21px"></td>
						<td style="HEIGHT: 21px" width="50%"></td>
					</TR>
					<tr height="15" width="100%">
						<td style="WIDTH: 393px" width="393"></td>
						<td style="WIDTH: 72px; HEIGHT: 17px">
							<P align="left">&nbsp;&nbsp;
							</P>
						</td>
						<td style="HEIGHT: 17px"><asp:label id="Label1" runat="server" Font-Names="Verdana" Font-Size="8.25pt" Width="208px">Application Form Fields</asp:label><asp:label id="Label2" runat="server" Font-Names="Verdana" Font-Size="8.25pt" Width="208px">Document Attributes</asp:label></td>
						<td width="50%"></td>
					</tr>
					<tr height="15" width="100%">
						<td style="WIDTH: 393px" width="393"></td>
						<td style="WIDTH: 72px" vAlign="middle">
							<P align="left">&nbsp;&nbsp;</P>
						</td>
						<td align="left"><asp:dropdownlist id="cmbAppFields" runat="server" Font-Names="Verdana" Font-Size="8.25pt" Height="32px"
								Width="180px"></asp:dropdownlist>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<asp:dropdownlist id="cmbAttributes" runat="server" Font-Names="Verdana" Font-Size="8.25pt" Height="32px"
								Width="180px"></asp:dropdownlist></td>
						<td width="50%">&nbsp;
							<asp:imagebutton id="btnAdd" runat="server" ImageUrl="Images/add_item.gif"></asp:imagebutton></td>
					</tr>
					<tr height="15" width="100%">
						<td width="50%"><INPUT id="FolderId" type="hidden" name="FolderId" runat="server"></td>
						<td style="WIDTH: 72px; HEIGHT: 17px"><BR>
						</td>
						<td style="FONT-SIZE: 8pt; FONT-FAMILY: Verdana; HEIGHT: 17px" valign="top">&nbsp;
							<asp:checkbox id="chkInteractive" runat="server" Font-Names="Verdana" Font-Size="8.25pt" Width="144px"
								Text="Interactive Selection" AutoPostBack="True"></asp:checkbox><BR>
							<asp:label id="Label5" runat="server" Font-Names="Verdana" Font-Size="8.25pt" Width="80px">Mappings</asp:label><BR>
							<asp:listbox id="lstMappings" runat="server" Font-Names="Verdana" Font-Size="8.25pt" Height="200px"
								Width="500px"></asp:listbox><BR>
							<span id="FolderMappings" runat="server">
								<asp:label id="Label6" runat="server" Width="150px" Font-Size="8.25pt" Font-Names="Verdana"
									Font-Bold="True">Folder Mappings</asp:label><BR>
								Application Field:
								<asp:DropDownList id="ddlAppFolder" runat="server" Width="180px"></asp:DropDownList><BR>
								<BR>
								<asp:TextBox id="txtFolder" runat="server"></asp:TextBox><IMG height="1" src="spacer.gif" width="5"><IMG id="ChooseFolder" style="CURSOR: hand" onclick="FolderLookup()" height="19" alt="Choose Folder"
									src="Images/addToFolder_btn.gif" runat="server"><IMG height="1" src="spacer.gif" width="5"><IMG id="ClearFolder" style="CURSOR: hand" onclick="document.getElementById('FolderId').value = '';document.getElementById('txtFolder').value = '';ActivateSave();"
									height="20" alt="Clear Folder" src="Images/clear.gif" runat="server"><IMG height="1" src="spacer.gif" width="20">Detection 
								Value:
								<asp:TextBox id="txtFolderValue" runat="server"></asp:TextBox>&nbsp;
								<asp:imagebutton id="btnAddAppFolder" runat="server" ImageUrl="Images/add_item.gif"></asp:imagebutton><BR>
								<BR>
								<asp:listbox id="lstAppFolder" runat="server" Width="500px" Height="200px" Font-Size="8.25pt"
									Font-Names="Verdana"></asp:listbox><BR>
								Attribute Capture Mode:
								<asp:RadioButton id="rdoReplace" runat="server" Text="Replace" GroupName="Attributes" Checked="True"
									CssClass="PageContent"></asp:RadioButton>&nbsp;
								<asp:RadioButton id="rdoAppend" runat="server" Text="Append" GroupName="Attributes" CssClass="PageContent"></asp:RadioButton><BR>
								<BR>
							</span>
							<asp:label id="lblError" runat="server" Font-Names="Verdana" Font-Size="8.25pt" Height="16px"
								Width="100%" ForeColor="Red"></asp:label></td>
						<td width="50%" vAlign="top">
							<P><BR>
								<BR>
								<BR>
								&nbsp;&nbsp;
								<asp:imagebutton id="btnMoveUp" runat="server" ImageUrl="Images/move_up.gif"></asp:imagebutton><BR>
								<BR>
								&nbsp;&nbsp;
								<asp:imagebutton id="buttonRemove" runat="server" ImageUrl="Images/remove_item.gif"></asp:imagebutton><BR>
								<BR>
								&nbsp;&nbsp;
								<asp:imagebutton id="btnMoveDown" runat="server" ImageUrl="Images/move_down.gif"></asp:imagebutton></P>
							<P><BR>
								<BR>
								<BR>
								<BR>
								<BR>
								<BR>
								<BR>
								<BR>
								<BR>
								&nbsp;&nbsp;
								<asp:imagebutton id="btnRemoveFolderMapping" runat="server" ImageUrl="Images/remove_item.gif"></asp:imagebutton></P>
							<P>&nbsp;</P>
						</td>
					</tr>
					<tr height="15" width="100%">
						<td style="WIDTH: 395px" width="395"></td>
						<td style="WIDTH: 72px"><BR>
							<P>&nbsp;</P>
						</td>
						<td align="right"><asp:imagebutton id="btnCancel" runat="server" ImageUrl="Images/btn_canceladmin.gif"></asp:imagebutton>&nbsp;<asp:imagebutton id="btnNext" runat="server" ImageUrl="Images/btn_finish.gif"></asp:imagebutton></td>
						<td width="50"></td>
					</tr>
					<tr height="100%">
						<td></td>
					</tr>
				</tr></TABLE>
			<P><BR>
				<BR>
			</P>
			</TD>
			<TD class="RightContent" height="100%">&nbsp;</TD>
			</TR>
			<TR>
				<TD class="PageFooter" vAlign="bottom" bgColor="#cccccc" colSpan="3"><uc1:topright id="docFooter" runat="server"></uc1:topright></TD>
			</TR>
			<TR height="1">
				<TD style="BACKGROUND-COLOR: #959394" width="20"><IMG height="1" src="images/spacer.gif" width="20"></TD>
				<TD style="BACKGROUND-COLOR: #959394" width="100%"><IMG height="1" src="images/spacer.gif" width="100%"></TD>
				<TD style="BACKGROUND-COLOR: #959394" width="20"><IMG height="1" src="images/spacer.gif" width="20"></TD>
			</TR>
			</TBODY></TABLE></form>
		<script language="javascript" src="scripts/tooltip_wrap.js"></script>
		<script language="javascript">

				function FolderLookup()
				{
					var val = window.showModalDialog('Tree.aspx?','FolderLookup','resizable:no;status:no;dialogHeight:625px;dialogWidth:250px;scroll:no');
					if (val != undefined)
					{
						varArray = val.split('�');
						document.all.FolderId.value = varArray[0].replace('W','');
						document.all.txtFolder.value = varArray[1];
					}
				}
				
				function ClearFolder()
				{
					document.getElementById('FolderId').value = '';
					document.getElementById('txtFolder').value = '';
				}

				function catchSubmit()
				{
					if (event.keyCode == 13)
					{
						event.cancelBubble = true;
						event.returnValue = false;
						return false;
					}
				}
				
		</script>
	</body>
</HTML>

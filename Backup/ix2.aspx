<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ix2.aspx.vb" Inherits="docAssistWeb.ix2"%>
<%@ Register TagPrefix="uc1" TagName="IndexNavigation" Src="IndexNavigation.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<TITLE>docAssist - Image Viewer</TITLE>
		<META content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<META content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<META content="JavaScript" name="vs_defaultClientScript">
		<META content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<BODY class="TransparentNoPadding" onresize="">
		<SCRIPT language="javascript" src="ImageViewer.js"></SCRIPT>
		<DIV id="service" style="BEHAVIOR: url(webservice.htc)" onresult="onWSresult()"></DIV>
		<DIV id="ServicePrint" style="BEHAVIOR: url(webservice.htc)" onresult="onWSPrintResult()"></DIV>
		<DIV id="ServiceRotate" style="BEHAVIOR: url(webservice.htc)" onresult="onWSRotateResult()"></DIV>
		<DIV id="ServiceRotateCheck" style="BEHAVIOR: url(webservice.htc)" onresult="onWSRotateCheckResult()"></DIV>
		<DIV id="navService" style="BEHAVIOR: url(webservice.htc)" onresult="onNavWSresult()"></DIV>
		<DIV id="svcAnnotation" style="BEHAVIOR: url(webservice.htc)" onresult="onAnnotationResult()"></DIV>
		<FORM id="Form1" method="post" runat="server">
			<ASP:LABEL id="lblFileNameClient" Runat="server" Visible="False"></ASP:LABEL><ASP:LITERAL id="litLpkLicense" Runat="server"></ASP:LITERAL><INPUT id="lblVersionId" type="hidden" name="lblVersionId" runat="server">
			<INPUT id="lblPageNo" type="hidden" name="lblPageNo" runat="server"><INPUT id="lblPageCount" type="hidden" name="lblPageCount" runat="server"><INPUT id="hostname" type="hidden" runat="server">
			<INPUT id="lblFileNameResult" type="hidden" name="lblFileNameResult" runat="server">
			<INPUT id="AccountId" type="hidden" name="AccountId" runat="server"> <input id="viewerWidth" type="hidden" value="0">
			<ASP:TEXTBOX id="txtFileNameServer" runat="server" Visible="False"></ASP:TEXTBOX></TD><INPUT id="UserView" style="DISPLAY: none" value="1" runat="server"></TD><INPUT id="AutoDisplay" style="DISPLAY: none" value="0" name="Text1" runat="server">
			<SPAN id="notice" style="DISPLAY: none"></SPAN>
			<ASP:LABEL id="lblErrorMessage" runat="server" ForeColor="Red" CSSCLASS="ErrorMessage"></ASP:LABEL>
			<OBJECT id="imageCtrl" onfocus="ActivateEvents();" height="45" width="45" border="0" classid="clsid:6D3CF4F3-C2F3-46E7-A126-3E53102A6B91" VIEWASTEXT>
				<PARAM NAME="_ExtentX" VALUE="1191">
				<PARAM NAME="_ExtentY" VALUE="1191">
				<PARAM NAME="ErrStr" VALUE="B52D26FB0B770B730AF6F993D7ED5D3F">
				<PARAM NAME="ErrCode" VALUE="103701755">
				<PARAM NAME="ErrInfo" VALUE="235694652">
				<PARAM NAME="Persistence" VALUE="-22015">
				<PARAM NAME="_cx" VALUE="1191">
				<PARAM NAME="_cy" VALUE="1191">
				<PARAM NAME="AutoSize" VALUE="5">
				<PARAM NAME="Font" VALUE="MS Sans Serif">
				<PARAM NAME="SaveTransparencyColor" VALUE="0">
				<PARAM NAME="OLEDropMode" VALUE="0">
				<PARAM NAME="SaveTIFFCompression" VALUE="0">
				<PARAM NAME="SaveTransparent" VALUE="0">
				<PARAM NAME="SaveJPEGProgressive" VALUE="0">
				<PARAM NAME="SaveJPEGGrayscale" VALUE="0">
				<PARAM NAME="SaveJPEGLumFactor" VALUE="10">
				<PARAM NAME="SaveJPEGChromFactor" VALUE="10">
				<PARAM NAME="SaveJPEGSubSampling" VALUE="2">
				<PARAM NAME="ViewAntialias" VALUE="-1">
				<PARAM NAME="ViewDithered" VALUE="-1">
				<PARAM NAME="AlignH" VALUE="1">
				<PARAM NAME="AlignV" VALUE="1">
				<PARAM NAME="LoadRotated" VALUE="0">
				<PARAM NAME="JPEGEnhDecomp" VALUE="-1">
				<PARAM NAME="WMFConvert" VALUE="0">
				<PARAM NAME="ProcessImageID" VALUE="5">
				<PARAM NAME="OwnDIB" VALUE="-1">
				<PARAM NAME="FileTimeout" VALUE="10000">
				<PARAM NAME="AsyncPriority" VALUE="0">
				<PARAM NAME="LZWPassword" VALUE="">
				<PARAM NAME="ViewUpdate" VALUE="-1">
				<PARAM NAME="TWAINProductName" VALUE="">
				<PARAM NAME="TWAINProductFamily" VALUE="">
				<PARAM NAME="TWAINManufacturer" VALUE="">
				<PARAM NAME="TWAINVersionInfo" VALUE="">
				<PARAM NAME="SavePDFCompression" VALUE="8">
				<PARAM NAME="ViewProgressive" VALUE="0">
				<PARAM NAME="SaveTIFFByteOrder" VALUE="0">
				<PARAM NAME="FTPUserName" VALUE="">
				<PARAM NAME="FTPPassword" VALUE="">
				<PARAM NAME="ProxyServer" VALUE="">
				<PARAM NAME="SaveEXIFThumbnailSize" VALUE="0">
				<PARAM NAME="SaveLJPPrediction" VALUE="1">
				<PARAM NAME="PDFSwapBlackandWhite" VALUE="0">
				<PARAM NAME="SaveTIFFRowsPerStrip" VALUE="0">
				<PARAM NAME="TIFFIFDOffset" VALUE="0">
				<PARAM NAME="ViewGrayMode" VALUE="0">
				<PARAM NAME="SaveWSQQuant" VALUE="1">
				<PARAM NAME="DisplayError" VALUE="0">
				<PARAM NAME="EvalMode" VALUE="0">
			</OBJECT>
			trtrtrtr
			<ASP:LITERAL id="litUnlockImageCtrl" runat="server"></ASP:LITERAL>
			<OBJECT id="objPrint" classid="clsid:66157B4F-9E4A-488C-92A4-4434A16FCBF2">
				<PARAM NAME="_ExtentX" VALUE="953">
				<PARAM NAME="_ExtentY" VALUE="794">
				<PARAM NAME="ErrStr" VALUE="9F06F7851D72F014A45EE97F36C59F98">
				<PARAM NAME="ErrCode" VALUE="103701755">
				<PARAM NAME="ErrInfo" VALUE="235694652">
				<PARAM NAME="_cx" VALUE="953">
				<PARAM NAME="_cy" VALUE="794">
				<PARAM NAME="AddCR" VALUE="-1">
				<PARAM NAME="Alignment" VALUE="0">
				<PARAM NAME="BackColor" VALUE="16777215">
				<PARAM NAME="BackStyle" VALUE="0">
				<PARAM NAME="BMargin" VALUE="240">
				<PARAM NAME="CurrentX" VALUE="240">
				<PARAM NAME="CurrentY" VALUE="240">
				<PARAM NAME="DocName" VALUE="PrintPRO Document">
				<PARAM NAME="DrawMode" VALUE="13">
				<PARAM NAME="DrawStyle" VALUE="1">
				<PARAM NAME="DrawWidth" VALUE="1">
				<PARAM NAME="FillColor" VALUE="0">
				<PARAM NAME="FillStyle" VALUE="1">
				<PARAM NAME="ForeColor" VALUE="0">
				<PARAM NAME="LMargin" VALUE="240">
				<PARAM NAME="ScaleMode" VALUE="1">
				<PARAM NAME="TMargin" VALUE="240">
				<PARAM NAME="WordWrap" VALUE="-1">
				<PARAM NAME="Font" VALUE="MS Sans Serif">
				<PARAM NAME="AutoMargin" VALUE="-1">
				<PARAM NAME="OutputFileName" VALUE="">
				<PARAM NAME="PicTransparent" VALUE="0">
				<PARAM NAME="PicTransparentColor" VALUE="0">
				<PARAM NAME="UseDefaultPrinter" VALUE="-1">
				<PARAM NAME="PrintPreviewScale" VALUE="1">
				<PARAM NAME="PrintPreviewOnly" VALUE="0">
				<PARAM NAME="OwnDIB" VALUE="0">
				<PARAM NAME="PrintToFile" VALUE="0">
				<PARAM NAME="SaveFileType" VALUE="4">
				<PARAM NAME="EvalMode" VALUE="0">
			</OBJECT>
			<OBJECT id="objNx" style="VISIBILITY: hidden" classid="clsid:BD1A7D5D-F3FB-4238-A072-6948D1150CD2">
				<PARAM NAME="_cx" VALUE="847">
				<PARAM NAME="_cy" VALUE="847">
				<PARAM NAME="Client" VALUE="">
				<PARAM NAME="DefaultPenColor" VALUE="16711680">
				<PARAM NAME="DefaultFillColor" VALUE="12632256">
				<PARAM NAME="DefaultPenWidth" VALUE="1">
				<PARAM NAME="DefaultTextColor" VALUE="0">
				<PARAM NAME="DefaultTool" VALUE="4103">
				<PARAM NAME="DefaultBackColor" VALUE="16777215">
				<PARAM NAME="DefaultTextFont" VALUE="Arial">
				<PARAM NAME="DefaultStampFont" VALUE="Courier New">
				<PARAM NAME="Subject" VALUE="">
				<PARAM NAME="Author" VALUE="">
				<PARAM NAME="Comments" VALUE="">
				<PARAM NAME="Title" VALUE="">
				<PARAM NAME="DefaultHighlightFill" VALUE="0">
				<PARAM NAME="DefaultHighlightBack" VALUE="0">
				<PARAM NAME="EnableContextMenu" VALUE="-1">
				<PARAM NAME="InteractMode" VALUE="0">
				<PARAM NAME="SaveAnnotations" VALUE="-1">
				<PARAM NAME="UsePaletteMenu" VALUE="-1">
				<PARAM NAME="DefaultStampText" VALUE="">
				<PARAM NAME="DefaultButtonFont" VALUE="Arial">
				<PARAM NAME="DefaultButtonText" VALUE="">
				<PARAM NAME="PreserveWTLayers" VALUE="0">
				<PARAM NAME="RequireToolPalette" VALUE="-1">
				<PARAM NAME="TIFFAnnotationType" VALUE="0">
				<PARAM NAME="AllowPaint" VALUE="-1">
				<PARAM NAME="UnicodeMode" VALUE="0">
				<PARAM NAME="NXPEditors" VALUE="-1">
				<PARAM NAME="LoadAnnotations" VALUE="-1">
				<PARAM NAME="MLECompatible" VALUE="0">
				<PARAM NAME="FontScaling" VALUE="0">
				<PARAM NAME="TextToolArrows" VALUE="-1">
				<PARAM NAME="RecalibrateXDPI" VALUE="-1">
				<PARAM NAME="RecalibrateYDPI" VALUE="-1">
				<PARAM NAME="DefaultRulerFont" VALUE="Arial">
			</OBJECT>
			<ASP:LITERAL id="litUnlockPrint" Runat="server"></ASP:LITERAL><ASP:LITERAL id="litScripts" runat="server"></ASP:LITERAL><ASP:LITERAL id="litProgress" runat="server"></ASP:LITERAL>
			<input id="imageLoaded" type="hidden" value="0" name="imageLoaded"> <input id="pageWidth" type="hidden" value="0" name="pageWidth">
			<input id="notateHwnd" type="hidden" value="0" name="notateHwnd">
			<script src="scripts/cookies.js" language="javascript"></script>
			<SCRIPT language="javascript" event="ToolUse(tool, action, x, y)" for="imageCtrl">imageCtrl_ToolUse(tool, action, x, y);</SCRIPT>
			<SCRIPT language="javascript" event="MouseMove(button, shift, x, y)" for="imageCtrl">imageCtrl_MouseMove(button, shift, x, y);</SCRIPT>
			<SCRIPT language="javascript" event="MouseDown(button, shift, x, y)" for="imageCtrl">imageCtrl_MouseDown(button, shift, x, y);</SCRIPT>
			<SCRIPT language="javascript" event="MouseUp(button, shift, x, y)" for="imageCtrl">imageCtrl_MouseUp(button, shift, x, y);</SCRIPT>
			<SCRIPT language="javascript" event="AnnotateEvent(a,b);" for="objNx">
				switch(a)
				{
					case 1001: //add
						SetDirty();
						break;
					case 1002: //move
						SetDirty();
						break;
					case 1003: //delete
						SetDirty();
						break;
					case 1017: //text edit
						SetDirty();
						break;	
					default:
						break;
				}
			</SCRIPT>
			<SCRIPT language="javascript" event="Progress(a,b,c,d,PctComplete,f,h)" for="imageCtrl">
				var timer; 
				if (PctComplete == 100 & b == 15)
				{
					flagRemoval();
				}
			</SCRIPT>
			<script language="javascript">
			
				document.all.objNx.ToolbarActivated = false;
				//document.all.imageCtrl.MenuSetEnabled(1,0,false);
				//document.all.objNx.EnableContextMenu = true;
				
				function enableNxMenu()
				{
					document.all.objNx.EnableContextMenu = true;
				}
				
				function disableNxMenu()
				{
					document.all.objNx.EnableContextMenu = false;
				}				
				
				function flagRemoval()
				{
					timer = setTimeout("clearFlag()",300); 
				}
				
				function clearFlag()
				{	
					parent.frames["frameAttributes"].DisableSave();
					clearTimeout(timer);
				}

				function DeleteAnnotations()
				{
					if (confirm('Clear all annotations?') == true )
					{
						ClearAnnotations();
					}
				}

				function Confirmation()
				{
					startConfirmation();
					setTimeout("stopConfirmation();",2000)
				}

				function startConfirmation()
				{
				/*
					var nXpos = (document.body.clientWidth - 350) / 2; 
					var nYpos = (document.body.clientHeight - 80) / 2;
					document.all.frameConfirm.style.top = 200;
					document.all.frameConfirm.style.left = nXpos;
					document.all.frameConfirm.style.top = nYpos;
					document.all.frameConfirm.style.zIndex = 99;
					document.all.frameConfirm.style.display = "inline";
					document.all.frameConfirm.style.visibility = "visible";
					*/
				}			

				function stopConfirmation()
				{
					document.all.frameConfirm.style.display = "none";
					document.all.frameConfirm.style.visibility = "hidden";
				}			
				
				function ActivateEvents()
				{
				
				}
				
			</script>
			<script language="vbscript">

				function ClearAnnotations()
					SetLayerAnnotation
					document.all.objNx.PrgDeleteAll document.all.objNx.CurrentLayer
				end function
				
				function SetLayerAnnotation()
					dim layercount
					layercount = document.all.objNx.NumLayers

					if layercount = 1 then
						document.all.objNx.CreateLayer
					end if
					
					if document.all.objNx.NumLayers <> 2 then
						document.all.objNx.CreateLayer
						document.all.objNx.CreateLayer
					end if

					annLayer = document.all.objNx.GetFirstLayer
					redLayer = document.all.objNx.GetNextLayer
					document.all.objNx.SetCurrentLayer annLayer					
				end function
				
				function SelectRedaction()
					SetLayerRedaction
				end function
				
				function SetLayerRedaction()
					dim layercount
					layercount = document.all.objNx.NumLayers

					if layercount = 1 then
						document.all.objNx.CreateLayer
					end if
					
					if document.all.objNx.NumLayers <> 2 then
						document.all.objNx.CreateLayer
						document.all.objNx.CreateLayer
					end if
				
					annLayer = document.all.objNx.GetFirstLayer
					redLayer = document.all.objNx.GetNextLayer
					document.all.objNx.SetCurrentLayer redLayer
				end function
				
				function Redaction()
					SetLayerRedaction
					document.all.imageCtrl.Toolset 0,1,0
					document.all.objNx.Tool = 4098
					document.all.objNx.DefaultFillColor = RGB(190,190,190)
					document.all.objNx.SetToolDefaultAttribute 4098, 16 , 3
					document.all.objNx.SetPenWidth document.all.objNx.CurrentLayer, 4098, 0
				end function
				
				function ToggleAnnotation(mode)
					if mode = "Hide Annotations" then
						ShowAnnotations()
					else
						HideAnnotations()
					end if
				end function
				
				function ShowAnnotations()
					SetLayerAnnotation
					document.all.objNx.SetVisible document.all.objNx.CurrentLayer, 1
				end function
				
				function HideAnnotations()
					SetLayerAnnotation
					document.all.objNx.SetVisible document.all.objNx.CurrentLayer, 0
				end function
				
				function StickyNote()
					SetLayerAnnotation
					document.all.imageCtrl.Toolset 0,1,0
					dim element2
					element2 = document.all.objNx.ElementCreate
					document.all.objNx.DefaultFillColor = RGB(255,255,0)
					document.all.objNx.ElementSetText element2, " "
					document.all.objNx.ElementSetType element2, 4097
					document.all.objNx.ElementSetBackColor element2, RGB(255,255,0)
					document.all.objNx.ElementSetBoundingRect element2, 30, 30, 310, 310
					document.all.objNx.ElementSetBackstyle element2, 2
					document.all.objNx.ElementAppend element2
					document.all.objNx.ElementDestroy element2
				end function
				
				function DrawingTool()
					SetLayerAnnotation
					document.all.imageCtrl.Toolset 0,1,0
					document.all.objNx.Tool = 4103
					document.all.objNx.SetPenColor document.all.objNx.CurrentLayer, 4103, RGB(255,0,0)
					document.all.objNx.SetPenWidth document.all.objNx.CurrentLayer, 4103, 5
				end function

				function TextTool()
					SetLayerAnnotation
					document.all.imageCtrl.Toolset 0,1,0
					document.all.objNx.Tool = 4097
					document.all.objNx.SetToolDefaultAttribute 4097, 16 , 3
					document.all.objNx.SetPenWidth document.all.objNx.CurrentLayer, 4097, 0
				end function

				function HighlighterTool()
					SetLayerAnnotation
					document.all.imageCtrl.Toolset 0,1,0
					document.all.objNx.Tool = 4098
					document.all.objNx.DefaultFillColor = RGB(255,255,0)
					document.all.objNx.SetToolDefaultAttribute 4098, 16 , 3
					document.all.objNx.SetPenWidth document.all.objNx.CurrentLayer, 4098, 0
				end function

				function StampTool()
					SetLayerAnnotation
					document.all.imageCtrl.Toolset 0,1,0
					document.all.objNx.Tool = 4104
					document.all.objNx.SetText document.all.objNx.CurrentLayer, 4104, "APPROVED"
				end function
				
				function GetAnnotation()
				
					dim saveAnnotations
					dim data
					dim size 
					dim temp
					dim temp1
					dim retstr
					dim base64
					
					saveAnnotations = false
					
					'check for annotations
					SetLayerAnnotation
					if document.all.objNx.PrgGetItemCount(document.all.objNx.CurrentLayer) > 0 then
						saveAnnotations = true
					end if
					SetLayerRedaction
					
					if document.all.objNx.PrgGetItemCount(document.all.objNx.CurrentLayer) > 0 then
						saveAnnotations = true
					end if
					
					if saveAnnotations = true then
						document.all.imageCtrl.Toolset 0,1,0
						document.all.objNx.TIFFAnnotationType = 0
						document.all.objNx.GetAnnIntoVariant data, Clng(size)
						retstr = SimpleBinaryToString(data)
						base64 = Base64Encode(retstr)
						GetAnnotation = base64
					else
						'clear all annotations/redactions because its garbage at this point
						document.all.imageCtrl.Toolset 0,1,0
						document.all.objNx.Clear
						document.all.objNx.TIFFAnnotationType = 0
						document.all.objNx.GetAnnIntoVariant data, Clng(size)
						retstr = SimpleBinaryToString(data)
						base64 = Base64Encode(retstr)
						GetAnnotation = base64
					end if

				end function
			
				Function SimpleBinaryToString(Binary)
					Dim I, S
					For I = 1 To LenB(Binary)
						S = S & Chr(AscB(MidB(Binary, I, 1)))
					Next
						SimpleBinaryToString = S
				End Function

				Function Base64Encode(inData)
					Const Base64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
					Dim cOut, sOut, I
					  
					'For each group of 3 bytes
					For I = 1 To Len(inData) Step 3
						Dim nGroup, pOut, sGroup
					    
						'Create one long from this 3 bytes.
						nGroup = &H10000 * Asc(Mid(inData, I, 1)) + &H100 * MyASC(Mid(inData, I + 1, 1)) + MyASC(Mid(inData, I + 2, 1))
					    
						'Oct splits the long To 8 groups with 3 bits
						nGroup = Oct(nGroup)
					    
						'Add leading zeros
						nGroup = String(8 - Len(nGroup), "0") & nGroup
					    
						'Convert To base64
						pOut = Mid(Base64, CLng("&o" & Mid(nGroup, 1, 2)) + 1, 1) + _
						Mid(Base64, CLng("&o" & Mid(nGroup, 3, 2)) + 1, 1) + _
						Mid(Base64, CLng("&o" & Mid(nGroup, 5, 2)) + 1, 1) + _
						Mid(Base64, CLng("&o" & Mid(nGroup, 7, 2)) + 1, 1)
					    
						'Add the part To OutPut string
						sOut = sOut + pOut
					    
						'Add a new line For Each 76 chars In dest (76*3/4 = 57)
						'If (I + 2) Mod 57 = 0 Then sOut = sOut + vbCrLf
					Next
					Select Case Len(inData) Mod 3
						Case 1: '8 bit final
						sOut = Left(sOut, Len(sOut) - 2) + "=="
						Case 2: '16 bit final
						sOut = Left(sOut, Len(sOut) - 1) + "="
					End Select
				Base64Encode = sOut
				End Function

				Function MyASC(OneChar)
					If OneChar = "" Then MyASC = 0 Else MyASC = Asc(OneChar)
				End Function
				
			</script>
		</FORM>
	</BODY>
</HTML>

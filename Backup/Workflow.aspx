﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Workflow.aspx.vb" Inherits="docAssistWeb.WorkflowDash" %>

<%@ Register Src="Controls/docHeader.ascx" TagName="docHeader" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>docAssist - Workflow Dashboard</title>
    <link type="text/css" rel="Stylesheet" href="Styles.css" />
    <link type="text/css" rel="Stylesheet" href="css/jquery-ui.css" />
    <link type="text/css" rel="Stylesheet" href="css/workflow.css" />
</head>
<body>
    <form id="frmWorkflow" runat="server">
    <div>
        <uc1:docHeader ID="docHeader" runat="server" />
    </div>
    <%--<div>
    	<div id="radio">
		<input type="radio" id="radio1" name="radio" /><label for="radio1">Review</label>
		<input type="radio" id="radio2" name="radio" checked="checked" /><label for="radio2">View</label>
		<input type="radio" id="radio3" name="radio" /><label for="radio3">All</label>
	</div>--%>
    </div>
    <div id="inboxes">
    </div>
    <table cellpadding="0" cellspacing="0" border="0" id="grid">
    </table>
    </form>
</body>

<script src="scripts/workflow.js"></script>

<script src="scripts/jquery-ui.js"></script>

<script src="scripts/jquery.dataTables.min.js"></script>

</html>

<%@ Register TagPrefix="uc1" TagName="TopRight" Src="Controls/TopRight.ascx" %>
<%@ Register TagPrefix="uc1" TagName="docHeader" Src="Controls/docHeader.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="AccountIPFiltering.aspx.vb" Inherits="docAssistWeb.AccountIPFiltering" %>
<%@ Register TagPrefix="uc1" TagName="AdminNav" Src="Controls/AdminNav.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title></title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="./Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0" MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<TABLE height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TBODY>
					<TR>
						<TD width="100%" colSpan="3"><uc1:docheader id="docHeader" runat="server"></uc1:docheader></TD>
					</TR>
					<TR vAlign="middle" height="100%">
						<TD class="Navigation1" style="BORDER-RIGHT: #cccccc thin solid" vAlign="top" height="100%"><uc1:adminnav id="AdminNav2" runat="server"></uc1:adminnav><IMG height="1" src="dummy.gif" width="120"></TD>
						<TD vAlign="top" align="center" height="100%"><br>
							<table width="100%">
								<tr>
									<td width="25"></td>
									<td>
										<p>
											<h2>IP Security</h2>
											<table width="100%">
												<tr>
													<td><asp:checkbox id="chkEnabled" runat="server" Text="Enable IP Filtering" Font-Size="X-Small" AutoPostBack="True"></asp:checkbox></td>
													<td align="right"><BUTTON class="btn primary" id="btnAdd" onclick="addMode();" type="button"><SPAN><SPAN><img src=Images/buttons/plus-circle.png> 
																	Add New Range</SPAN></SPAN></BUTTON></td>
												</tr>
											</table>
											<div class="addSection" id="add" style="DISPLAY: none">Start: <input id="rangeBegin" type="text" size="15" runat="server">
												End: <input id="rangeEnd" type="text" runat="server" size="15"> Description: <input id="rangeDesc" type="text" size="23" runat="server">&nbsp;&nbsp;<button class="btn primary" id="addRange" type="button" runat="server"><span><span>Save</span></span></button>
												or <a onclick="$('#add').fadeOut(400);">Cancel</a>
												<p id="ipAddress" runat="server"></p>
												<p class="error" id="errMsg" style="DISPLAY: none"></p>
											</div>
											<div id="view">
												<p><ASP:DATAGRID id="Ranges" runat="server" Width="100%" CSSCLASS="transparent" AUTOGENERATECOLUMNS="False">
														<AlternatingItemStyle CssClass="TableAlternateItem" BackColor="#E0E0E0"></AlternatingItemStyle>
														<HeaderStyle Font-Bold="True" CssClass="TableHeader" BackColor="DarkGray"></HeaderStyle>
														<Columns>
															<asp:TemplateColumn>
																<HeaderStyle Width="30px"></HeaderStyle>
																<ItemStyle HorizontalAlign="Center"></ItemStyle>
																<ItemTemplate>
																	<ASP:LINKBUTTON id="lnkRemove" runat="server" ToolTip="Delete" CommandName="Delete">
																		<img src="Images/trash.gif" border="0"></ASP:LINKBUTTON>
																	<asp:Label id=lblId runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.IPRestrictID") %>'>
																	</asp:Label>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:BoundColumn DataField="IPDescription" HeaderText="Description"></asp:BoundColumn>
															<asp:BoundColumn DataField="StartIPRange" HeaderText="Start"></asp:BoundColumn>
															<asp:BoundColumn DataField="EndIPRange" HeaderText="End"></asp:BoundColumn>
															<asp:BoundColumn DataField="UserAddedName" HeaderText="Added By"></asp:BoundColumn>
															<asp:BoundColumn DataField="AddedDatetime" HeaderText="Date Added"></asp:BoundColumn>
														</Columns>
													</ASP:DATAGRID></p>
											</div>
									</td>
								</tr>
							</table>
						</TD>
						<TD class="RightContent" height="100%">&nbsp;</TD>
					</TR>
					<TR>
						<TD class="PageFooter" vAlign="bottom" bgColor="#cccccc" colSpan="3"><uc1:topright id="docFooter" runat="server"></uc1:topright></TD>
					</TR>
					<TR height="1">
						<TD style="BACKGROUND-COLOR: #959394" width="20"><IMG height="1" src="images/spacer.gif" width="20"></TD>
						<TD style="BACKGROUND-COLOR: #959394" width="100%"><IMG height="1" src="images/spacer.gif" width="100%"></TD>
						<TD style="BACKGROUND-COLOR: #959394" width="20"><IMG height="1" src="images/spacer.gif" width="20"></TD>
					</TR>
				</TBODY>
			</TABLE>
		</form>
		<script language="javascript">
			$(document).ready(function() { $("input").keypress(function(){if(event.keyCode == 13){ return false; }});  });
			function addMode(){ $('#rangeBegin').val('');$('#rangeEnd').val('');$('#rangeDesc').val('');$('#errMsg').hide();$("#add").fadeIn(400);$('#rangeBegin').focus();}
			function err(msg){$('#errMsg').empty();$('#errMsg').append(msg);$('#errMsg').show();}
		</script>
	</body>
</HTML>

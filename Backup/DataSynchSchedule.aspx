<%@ Register TagPrefix="uc1" TagName="TopRight" Src="Controls/TopRight.ascx" %>
<%@ Register TagPrefix="uc1" TagName="docHeader" Src="Controls/docHeader.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="DataSynchSchedule.aspx.vb" Inherits="docAssistWeb.DataSynchSchedule"%>
<%@ Register TagPrefix="uc1" TagName="AdminNav" Src="Controls/AdminNav.ascx" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>docAssist - Manage Documents</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body class="Prefs" bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0" MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<TABLE height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TBODY>
					<TR>
						<TD width="100%" colSpan="3"><uc1:docheader id="docHeader" runat="server"></uc1:docheader></TD>
					</TR>
					<TR vAlign="middle" height="100%">
						<TD class="Navigation1" style="BORDER-RIGHT: #cccccc thin solid" vAlign="top" height="100%"><uc1:adminnav id="AdminNav2" runat="server"></uc1:adminnav><IMG height="1" src="dummy.gif" width="120"></TD>
						<TD vAlign="top" noWrap align="center" height="100%"><BR>
							<TABLE id="Table1" cellSpacing="0" cellPadding="0" width="92%" align="left" border="0">
								<TR>
									<TD width="20" height="15" rowSpan="6">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</TD>
									<TD class="PrefHeader" height="15">Add Data Synchronization Job Schedule
									</TD>
								</TR>
								<TR>
									<TD align="right"><IMG height="10" src="dummy.gif" width="1">
									</TD>
								</TR>
								<TR vAlign="top" align="left">
									<TD align="left" width="100%">
										<TABLE class="Prefs" id="Table2" cellSpacing="0" cellPadding="0" width="100%" align="left"
											border="0">
											<TR>
												<TD class="LabelName" noWrap align="right">Job:</TD>
												<TD class="Text" bgColor="whitesmoke"><IMG height="1" src="dummy.gif" width="2">
													<asp:dropdownlist id="ddlJobs" runat="server" Width="200px" CssClass="Text">
														<asp:ListItem Value="Vouchers">Vouchers</asp:ListItem>
													</asp:dropdownlist></TD>
											</TR>
											<TR>
												<TD class="Text" noWrap align="right" width="160"></TD>
												<TD>&nbsp;</TD>
											</TR>
											<TR>
												<TD class="LabelName" vAlign="top" noWrap align="right" width="160">Schedule:</TD>
												<TD>
													<TABLE id="Table3" cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR>
															<TD bgColor="whitesmoke">
																<asp:radiobutton id="rdoNow" runat="server" Text="Now" GroupName="Schedule" CssClass="Text" Checked="True"></asp:radiobutton></TD>
														</TR>
														<TR>
															<TD class="Text">
																<asp:radiobutton id="rdoDaily" runat="server" Text="Daily at" GroupName="Schedule"></asp:radiobutton>&nbsp;
																<ew:timepicker id="txtDaily" runat="server" Width="57px" CssClass="Text" CellPadding="5px" NumberOfColumns="1"
																	PopupWidth="75px" PopupHeight="220px" MinuteInterval="FifteenMinutes">
																	<CLEARTIMESTYLE BackColor="White"></CLEARTIMESTYLE>
																	<TIMESTYLE BackColor="White" ForeColor="Black" Font-Names="Arial" Font-Size="8pt"></TIMESTYLE>
																	<SELECTEDTIMESTYLE BackColor="White" Font-Names="Arial" Font-Size="8pt" Font-Bold="True"></SELECTEDTIMESTYLE>
																</ew:timepicker></TD>
														</TR>
														<TR>
															<TD class="Text" bgColor="whitesmoke">
																<asp:radiobutton id="rdoWeekly" runat="server" Text="Weekly at" GroupName="Schedule"></asp:radiobutton>&nbsp;
																<ew:timepicker id="txtWeekly" runat="server" Width="57px" CssClass="Text" CellPadding="5px" NumberOfColumns="1"
																	PopupWidth="75px" PopupHeight="220px" MinuteInterval="FifteenMinutes">
																	<CLEARTIMESTYLE BackColor="White"></CLEARTIMESTYLE>
																	<TIMESTYLE BackColor="White" ForeColor="Black" Font-Names="Arial" Font-Size="8pt"></TIMESTYLE>
																	<SELECTEDTIMESTYLE BackColor="White" Font-Names="Arial" Font-Size="8pt" Font-Bold="True"></SELECTEDTIMESTYLE>
																</ew:timepicker></TD>
														</TR>
														<TR>
															<TD class="Text" id="Weekly" bgColor="whitesmoke"><IMG height="1" src="dummy.gif" width="10">
																<asp:checkbox id="chkMonday" runat="server" Text="Monday" Font-Size="8pt"></asp:checkbox>&nbsp;
																<asp:checkbox id="chkTuesday" runat="server" Text="Tuesday" Font-Size="8pt"></asp:checkbox>&nbsp;
																<asp:checkbox id="chkWednesday" runat="server" Text="Wednesday" Font-Size="8pt"></asp:checkbox>&nbsp;
																<asp:checkbox id="chkThursday" runat="server" Text="Thursday" Font-Size="8pt"></asp:checkbox>&nbsp;
																<asp:checkbox id="chkFriday" runat="server" Text="Friday" Font-Size="8pt"></asp:checkbox>&nbsp;
																<asp:checkbox id="chkSaturday" runat="server" Text="Saturday" Font-Size="8pt"></asp:checkbox>&nbsp;
																<asp:checkbox id="chkSunday" runat="server" Text="Sunday" Font-Size="8pt"></asp:checkbox>&nbsp;</TD>
														</TR>
													</TABLE>
													<BR>
													<asp:Label id="lblError" runat="server" CssClass="ErrorText" Visible="False"></asp:Label></TD>
											</TR>
											<TR>
												<TD class="Text" noWrap align="right" width="160"></TD>
												<TD>&nbsp;<IMG height="1" src="dummy.gif" width="350">
													<asp:imagebutton id="btnAdd" runat="server" ImageUrl="Images/save_small.gif"></asp:imagebutton></TD>
											</TR>
										</TABLE>
									</TD>
								</TR>
							</TABLE>
						</TD>
						<TD height="100%">&nbsp;</TD>
					</TR>
					<TR>
						<TD class="PageFooter" vAlign="bottom" bgColor="#cccccc" colSpan="3"><uc1:topright id="docFooter" runat="server"></uc1:topright></TD>
					</TR>
					<TR height="1">
						<TD style="BACKGROUND-COLOR: #959394" width="20"><IMG height="1" src="images/spacer.gif" width="20"></TD>
						<TD style="BACKGROUND-COLOR: #959394" width="100%"><IMG height="1" src="images/spacer.gif" width="100%"></TD>
						<TD style="BACKGROUND-COLOR: #959394" width="20"><IMG height="1" src="images/spacer.gif" width="20"></TD>
					</TR>
				</TBODY>
			</TABLE>
			<script language="javascript">
		
			disableWeekly();
			
			function disableRealTime()
			{
				document.getElementById('rdoRealTime').disabled = true;
				document.getElementById('rdoRealTime').checked = false;
				document.getElementById('rdoDaily').checked = true;
			}

			function enableRealTime()
			{
				document.getElementById('rdoRealTime').disabled = false;
			}
			
			function disableWeekly()
			{
				document.getElementById('Weekly').disabled = true;
			}
			
			function enableWeekly()
			{
				document.getElementById('Weekly').disabled = false;
			}
		
			</script>
		</form>
	</body>
</HTML>

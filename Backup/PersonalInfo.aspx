<%@ Page Language="vb" AutoEventWireup="false" Codebehind="PersonalInfo.aspx.vb" Inherits="docAssistWeb.PersonalInfo"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title></title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body class="Prefs" bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form class="Prefs" id="frmPreferences" method="post" runat="server">
			<br>
			<table class="Prefs" cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
				<tr>
					<td width="20" rowSpan="6"></td>
					<td class="PrefHeader">My Personal Information
					</td>
				</tr>
				<tr>
					<td><BR>
					</td>
				</tr>
				<tr>
					<td align="left">
						<table class="Prefs" cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
							<tr>
								<td width="20" rowSpan="4"></td>
								<td class="LabelName" noWrap align="right">Name:</td>
								<td bgColor="whitesmoke">&nbsp;
									<asp:label id="lblName" runat="server" CssClass="Text"></asp:label></td>
							</tr>
							<tr>
								<td class="LabelName" noWrap align="right" width="160">E-Mail Address:</td>
								<td>&nbsp;
									<asp:label id="lblEmail" runat="server" CssClass="Text"></asp:label></td>
							</tr>
							<tr>
								<td class="LabelName" noWrap align="right" width="160">Contact Number:</td>
								<td bgColor="whitesmoke">&nbsp;
									<asp:label id="lblNumber" runat="server" CssClass="Text"></asp:label></td>
							</tr>
							<tr>
								<td class="LabelName" noWrap align="right" width="160">Last Login:</td>
								<td>&nbsp;
									<asp:label id="lblLogin" runat="server" CssClass="Text"></asp:label></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			</TD></TR></TABLE></TR></TABLE></form>
	</body>
</HTML>

Imports Accucentric.docAssist.Data.Images

Partial Class Terms
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub OpenCase_ServerClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OpenCase.ServerClick

        Dim mobjUser As Accucentric.docAssist.Web.Security.User
        Dim conMaster As New SqlClient.SqlConnection

        Try

            mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
            Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))
            mobjUser.AccountId = guidAccountId

            conMaster = Functions.BuildMasterConnection

            Dim Util As New SystemUtilities(conMaster, True)

            Util.AcceptTermsOfService(mobjUser.AccountId.ToString, mobjUser.UserId.ToString, Request.UserHostAddress)

            Session("Terms") = Nothing

            litClose.Text = "<script>window.parent.GB_hide(false);</script>"

        Catch ex As Exception

            Dim conLog As SqlClient.SqlConnection
            conLog = Functions.BuildMasterConnection
            Functions.LogException(mobjUser.UserId.ToString, mobjUser.AccountId.ToString, "", "Accept Terms of Service: " & ex.Message, ex.StackTrace, Server.MachineName, "", Request.UrlReferrer.ToString, conLog, True)

        Finally

            If conMaster.State <> ConnectionState.Closed Then
                conMaster.Close()
                conMaster.Dispose()
            End If

        End Try

    End Sub

End Class
Partial Class Support
    Inherits System.Web.UI.Page

    Dim mobjUser As Accucentric.docAssist.Web.Security.User

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents btnOk As System.Web.UI.WebControls.ImageButton

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try
            Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
            txtName.Text = Me.mobjUser.UserName
            'txtName.Enabled = False
            txtCompany.Text = Me.mobjUser.CompanyName
            'txtCompany.Enabled = False
            txtEmail.Text = Me.mobjUser.EMailAddress
            'txtEmail.Enabled = False
        Catch ex As Exception

        End Try

    End Sub

    Private Function ValidatePage() As Boolean

        If txtName.Text.Trim = "" Then
            lblError.Text = "Please enter a name."
            lblError.Visible = True
            Return False
        End If

        If txtEmail.Text.Trim = "" Then
            lblError.Text = "Please enter an e-mail address."
            lblError.Visible = True
            Return False
        End If

        Dim strRegEx As String = "^\w+([-+.]\w+)*@\w+([-.]\w+)*\.([a-zA-Z]{2,4})$"
        If System.Text.RegularExpressions.Regex.IsMatch(txtEmail.Text, strRegEx) = False Then
            lblError.Text = "Please enter a valid e-mail address."
            lblError.Visible = True
            Return False
        End If

        If txtCompany.Text.Trim = "" Then
            lblError.Text = "Please enter a company."
            lblError.Visible = True
            Return False
        End If

        If txtCompany.Text.Trim = "" Then
            lblError.Text = "Please enter a company."
            lblError.Visible = True
            Return False
        End If

        If txtCompany.Text.Trim = "" Then
            lblError.Text = "Please enter a company."
            lblError.Visible = True
            Return False
        End If

        If txtSubject.Text.Trim = "" Then
            lblError.Text = "Please enter a subject."
            lblError.Visible = True
            Return False
        End If

        Return True

    End Function

    Private Sub OpenCase_ServerClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OpenCase.ServerClick

        Try

            If ValidatePage() = True Then

                Dim sbMessage As New System.Text.StringBuilder
                sbMessage.Append("Name: " & txtName.Text & vbCr)
                sbMessage.Append("E-Mail Address: " & txtEmail.Text & vbCr)
                sbMessage.Append("Company: " & txtCompany.Text & vbCr)
                sbMessage.Append("Phone Number: " & txtPhoneNumber.Text & vbCr)
                sbMessage.Append("Comments: " & txtComments.Text)

                Dim objMail As New Quiksoft.FreeSMTP.EmailMessage
                objMail.Recipients.Add(ConfigurationSettings.AppSettings("SupportBox"))
                objMail.From.Email = txtEmail.Text
                objMail.From.Name = txtName.Text
                objMail.Subject = txtSubject.Text

                objMail.BodyParts.Add(sbMessage.ToString)

                Dim objMailSender As New Quiksoft.FreeSMTP.SMTP(ConfigurationSettings.AppSettings("SupportMailServer"))
                objMailSender.Send(objMail)

                System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, "supportSent", "alert('Your support request has been sent.');window.history.go(-1);", True)

            End If

        Catch ex As Exception

            lblError.Text = "An error has occured. Please try again. If the problem continues you can e-mail support at support@docassist.com"
            lblError.Visible = True

        End Try

    End Sub

End Class
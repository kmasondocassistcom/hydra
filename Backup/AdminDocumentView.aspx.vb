Partial Class AdminDocumentView
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private mobjUser As Accucentric.docAssist.Web.Security.User

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim sqlConn As New SqlClient.SqlConnection

        Try

            If Request.Cookies("docAssist") Is Nothing Then Response.Redirect("Default.aspx")
            If Request.Cookies("docAssist") Is Nothing Then
                Response.Redirect("Default.aspx")
                Exit Sub
            End If

            Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
            Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))

            'This is the parameter that points to the current account.
            'TODO: Test this with multi account login.
            Me.mobjUser.AccountId = guidAccountId

            sqlConn = Functions.BuildConnection(Me.mobjUser)
            sqlConn.Open()

            Dim sqlCmd As New SqlClient.SqlCommand("acsAdminDisplayDocumentStructure")
            sqlCmd.Connection = sqlConn
            sqlCmd.CommandType = CommandType.StoredProcedure
            sqlCmd.CommandTimeout = DEFAULT_COMMAND_TIMEOUT
            Dim daSql As New SqlClient.SqlDataAdapter(sqlCmd)
            Dim dsStructure As New DataSet
            daSql.Fill(dsStructure)

            sqlConn.Close()
            sqlConn.Dispose()

            Dim sbTable As New System.Text.StringBuilder

            sbTable.Append("<TABLE border=""0"" width=""100%"" height=""100%"" cellpadding=""0"" cellspacing=""0"" style=""FONT-SIZE: 8.25pt; FONT-FAMILY: Verdana""><TR></TR>")

            Dim dr As DataRow
            Dim strLastRole As String
            Dim strLastFolder As String

            Dim intCounter As Integer = 0

            For Each dr In dsStructure.Tables(0).Rows

                If dr("CabinetName") <> strLastRole Then
                    'If intCounter > 0 Then sbTable.Append("<TR height=""12""><TD></TD></TR>")
                    ''sbTable.Append("<TR><TD style=""BACKGROUND-COLOR: #cccccc"">" & dr("CabinetName") & "</TD><TD style=""BACKGROUND-COLOR: #cccccc"">" & dr("FolderName") & "</TD><TD style=""BACKGROUND-COLOR: #cccccc"">" & dr("DocumentName") & "</TD></TR>")

                    'If dr("FolderName") <> strLastFolder Then
                    '    sbTable.Append("<TR><TD style=""BACKGROUND-COLOR: #cccccc"">" & dr("CabinetName") & "</TD><TD style=""BACKGROUND-COLOR: #cccccc"">" & dr("FolderName") & "</TD><TD style=""BACKGROUND-COLOR: #cccccc"">" & dr("DocumentName") & "</TD></TR>")
                    'Else
                    '    sbTable.Append("<TR><TD style=""BACKGROUND-COLOR: #cccccc""></TD><TD style=""BACKGROUND-COLOR: #cccccc""></TD><TD style=""BACKGROUND-COLOR: #cccccc"">" & dr("DocumentName") & "</TD></TR>")
                    'End If

                Else
                    If dr("FolderName") <> strLastFolder Then
                        sbTable.Append("<TR><TD>" & dr("CabinetName") & "</TD><TD>" & dr("FolderName") & "</TD><TD>" & dr("DocumentName") & "</TD></TR>")
                    Else
                        sbTable.Append("<TR><TD></TD><TD></TD><TD>" & dr("DocumentName") & "</TD></TR>")
                    End If

                    'sbTable.Append("<TR><TD></TD><TD>" & dr("FolderName") & "</TD><TD>" & dr("DocumentName") & "</TR>")
                End If
                strLastRole = dr("CabinetName")
                strLastFolder = dr("FolderName")
                intCounter += 1
            Next




            sbTable.Append("<tr><td><BR><BR></td></tr>")
            sbTable.Append("<TR><TD style=""BACKGROUND-COLOR: #959394""><b>Document</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</TD><TD width=""100%"" style=""BACKGROUND-COLOR: #959394""><b>Attributes</b></TD></TR>")


            Dim dr2 As DataRow
            Dim strLast As String
            Dim intRowCounter As Integer = 0
            For Each dr2 In dsStructure.Tables(1).Rows

                If dr2("DocumentName") <> strLast Then
                    If intRowCounter > 0 Then sbTable.Append("<TR height=""12""><TD></TD></TR>")
                    sbTable.Append("<TR><TD style=""BACKGROUND-COLOR: #cccccc"">" & dr2("DocumentName") & "</TD><TD style=""BACKGROUND-COLOR: #cccccc"">" & dr2("AttributeName") & "</TD></TR>")
                Else
                    sbTable.Append("<TR><TD></TD><TD>" & dr2("AttributeName") & "</TD></TR>")

                End If
                strLast = dr2("DocumentName")
                intRowCounter += 1
            Next









            sbTable.Append("<TR height=""100%""><TD height=""100%""></TD></TABLE>")

            lblTable.Text = sbTable.ToString

        Catch ex As Exception
            If sqlConn.State <> ConnectionState.Closed Then sqlConn.Close()
            sqlConn.Dispose()
        End Try
    End Sub

End Class

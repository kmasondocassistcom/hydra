<%@ Page Language="vb" AutoEventWireup="false" Codebehind="WorkflowSubmit.aspx.vb" Inherits="docAssistWeb.WorkflowSubmit"%>
<%@ Register TagPrefix="cc1" Namespace="OptGroupLists" Assembly="OptGroupLists" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title></title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<table class="PageContent" id="tblSubmit" height="240" cellSpacing="0" cellPadding="0"
				width="255" border="0">
				<tr>
					<TD height="35"></TD>
					<td width="100%">To send this document to a workflow route, please select the 
						workflow below.
					</td>
				</tr>
				<tr>
					<TD></TD>
					<td vAlign="bottom"><b>Workflow:</b>
						<BR>
						<cc1:optgroupddl id="ddlWorkflow" runat="server" Width="236px" AutoPostBack="True"></cc1:optgroupddl></td>
				</tr>
				<tr>
					<TD></TD>
					<td vAlign="top"><b>Next Queue:<BR>
						</b>
						<cc1:optgroupddl id="ddlQueue" runat="server" Width="236px" AutoPostBack="True" Enabled="False"></cc1:optgroupddl></td>
				</tr>
				<tr>
					<TD height="15"></TD>
					<td>
						<P><b>
								<asp:CheckBox id="chkUrgent" runat="server" Text="Urgent" Height="4px" Enabled="False" AutoPostBack="True"></asp:CheckBox><BR>
							</b><B>Notes:</B>
						</P>
					</td>
				</tr>
				<tr vAlign="top">
					<TD vAlign="top" height="70"></TD>
					<td vAlign="top" height="70"><asp:textbox id="txtNotes" runat="server" Width="236px" Height="59px" TextMode="MultiLine" Enabled="False"
							AutoPostBack="True"></asp:textbox></td>
				</tr>
				<tr>
					<td height="100%"></td>
					<td></td>
				</tr>
			</table>
			<asp:Literal id="litUrgent" runat="server"></asp:Literal>
			<asp:Literal id="litQueue" runat="server"></asp:Literal>
			<asp:Literal id="litWfid" runat="server"></asp:Literal>
			<asp:Literal id="litMode" runat="server"></asp:Literal>
		</form>
		<script language="javascript">
			function PassWorkflowNote()
			{
				parent.frames["frameAttributes"].WorkflowNoteSet(document.all.txtNotes.value);
			}
			function reloadPage()
			{
				window.location.reload();
			}
		</script>
		<script language="javascript" src="scripts/cookies.js"></script>
		<script language="javascript">
			var resultbln = readCookie("results");
			if (resultbln == "1") { window.history.back(); }
		</script>
	</body>
</HTML>

Imports Accucentric.docAssist
Partial Class DocumentNotes
    Inherits System.Web.UI.Page

    Dim mobjUser As Accucentric.docAssist.Web.Security.User
    Dim mconSqlImage As SqlClient.SqlConnection
    Protected WithEvents lblNotes As System.Web.UI.WebControls.Label
    Protected WithEvents lblNote As System.Web.UI.WebControls.Label
    Dim mcookieSearch As cookieSearch

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'Check if this is a read only user, if so hide the ability to add notes.

        'Get VersionId of the image to load text from
        Me.mobjUser = BuildUserObject(Request.Cookies("docAssist"))
        Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
        Me.mcookieSearch = New cookieSearch(Me.Page)
        
        Dim intVersionId As Integer

        If Me.mcookieSearch.VersionID = -1 Then
            Me.mcookieSearch.VersionID = Request.QueryString("VID")
        End If

        If Me.mcookieSearch.VersionID > 0 Then
            intVersionId = Me.mcookieSearch.VersionID

            'Load the data and save to the session
            Dim dsVersionControl As Accucentric.docAssist.Data.dsVersionControl
            If Session(intVersionId & "VersionControl") Is Nothing Then
                dsVersionControl = Accucentric.docAssist.Data.GetVersionControl(intVersionId, Me.mconSqlImage)
                Session(intVersionId & "VersionControl") = dsVersionControl
            Else
                dsVersionControl = Session(intVersionId & "VersionControl")
                'Check to make sure we haven't changed versions
                If Not CType(dsVersionControl.ImageVersion.Rows(0), Data.dsVersionControl.ImageVersionRow).VersionID = intVersionId Then
                    dsVersionControl = Data.GetVersionControl(intVersionId, Me.mconSqlImage)
                    Session(intVersionId & "VersionControl") = dsVersionControl
                    'lblPageNo.Value = 1
                End If
            End If

            Dim intDocumentId As Integer = CType(dsVersionControl.Images.Rows(0), Accucentric.docAssist.Data.dsVersionControl.ImagesRow).DocumentID

            Try
                Select Case DocumentSecurityLevel(Me.mcookieSearch.VersionID, Me.mobjUser, Me.mconSqlImage)
                    Case Web.Security.Functions.SecurityLevel.NoAccess
                        'Shouldn't even be here

                    Case Web.Security.Functions.SecurityLevel.Read
                        'Hide the ability to add notes.
                        txtNote.Visible = False
                        btnSave.Visible = False
                    Case Web.Security.Functions.SecurityLevel.Change
                        'Show normal
                    Case Web.Security.Functions.SecurityLevel.Delete
                        'Show normal
                    Case Else

                End Select

                'Close SQL connection.
                If Me.mconSqlImage.State <> ConnectionState.Closed Then
                    Me.mconSqlImage.Close()
                    Me.mconSqlImage.Dispose()
                End If

            Catch ex As Exception
                'Close SQL connection.
                If Me.mconSqlImage.State <> ConnectionState.Closed Then
                    Me.mconSqlImage.Close()
                    Me.mconSqlImage.Dispose()
                End If
            End Try

        End If

        'Try

        '    Me.mobjUser = BuildUserObject(Request.Cookies("docAssist"))
        '    Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
        '    Me.mcookieSearch = New cookieSearch(Me)

        'Catch ex As Exception

        '    If ex.Message = "User is not logged on" Then
        '        Response.Redirect("Default.aspx")
        '    End If

        'End Try

        'Try

        '    If Me.mconSqlImage.State <> ConnectionState.Open Then
        '        Me.mconSqlImage.Open()
        '    End If

        '    Dim sqlCmd As New SqlClient.SqlCommand("acsImageNotesByVersionID", Me.mconSqlImage)
        '    sqlCmd.CommandType = CommandType.StoredProcedure
        '    sqlCmd.Parameters.Add("@VersionID", Me.mcookieSearch.VersionID)
        '    sqlCmd.Parameters.Add("@DiffFromGMT", Me.mobjUser.TimeDiffFromGMT)

        '    Dim dtNotes As New DataTable
        '    Dim sqlda As New SqlClient.SqlDataAdapter(sqlCmd)
        '    sqlda.Fill(dtNotes)

        '    Dim sbDocumentText As New System.Text.StringBuilder
        '    Dim intPageCount As Integer = 1

        '    Me.mconSqlImage.Close()
        '    Me.mconSqlImage.Dispose()

        '    If dtNotes.Rows.Count = 0 Then
        '        'No notes available message.
        '    Else
        '        'Format notes
        '        Dim sbNotes As New System.Text.StringBuilder
        '        Dim dr As DataRow

        '        For Each dr In dtNotes.Rows
        '            sbNotes.Append("<BR><BR><b>Added by " & "<a href=""mailto:" & dr("EmailAddress") & """>" & dr("username") & "</a>" & " on " & dr("notedatetime") & "</b><BR><BR>" & dr("note") & "<BR><BR>")
        '        Next

        '        lblNote.Text = sbNotes.ToString

        '    End If

        'Catch ex As Exception

        '    'Error loading notes
        '    Me.mconSqlImage.Close()
        '    Me.mconSqlImage.Dispose()

        'End Try

    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click

        If txtNote.Text.Trim = "" Then
            lblErrorMsg.Text = "You must enter a note before saving."
            Exit Sub
        End If

        Try

            Me.mobjUser = BuildUserObject(Request.Cookies("docAssist"))
            Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
            Me.mcookieSearch = New cookieSearch(Me)

        Catch ex As Exception

            If ex.Message = "User is not logged on" Then
                Response.Redirect("Default.aspx")
            End If

        End Try

        Try

            If Me.mconSqlImage.State <> ConnectionState.Open Then
                Me.mconSqlImage.Open()
            End If

            'Preserve breaks
            Dim strNote As String = txtNote.Text.Replace(Chr(13), "<BR>")

            Dim sqlCmd As New SqlClient.SqlCommand("acsImageNotesInsert", Me.mconSqlImage)
            sqlCmd.CommandType = CommandType.StoredProcedure
            sqlCmd.Parameters.Add("@VersionID", Me.mcookieSearch.VersionID)
            sqlCmd.Parameters.Add("@Note", strNote)
            sqlCmd.Parameters.Add("@UserID", Me.mobjUser.ImagesUserId)

            sqlCmd.ExecuteScalar()

            'Reload notes to reflect the note just added by the user

            Me.mconSqlImage.Close()
            Me.mconSqlImage.Dispose()

            txtNote.Text = ""
            lblErrorMsg.Text = ""

        Catch ex As Exception

            lblErrorMsg.Text = "An error has occured."

            'Error loading notes
            Me.mconSqlImage.Close()
            Me.mconSqlImage.Dispose()

        End Try

    End Sub
End Class

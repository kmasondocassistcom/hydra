Imports Telerik.WebControls
Imports Telerik.WebControls.RadUploadUtils
Imports Telerik.Web.UI
Imports Accucentric.docAssist
Imports Accucentric.docAssist.Data.Images

Public Class ViewAttachments
    Inherits System.Web.UI.Page
    'Inherits zumiControls.zumiPage

#Region "Declarations"
    Dim mobjUser As Accucentric.docAssist.Web.Security.User
    Dim mcookieSearch As cookieSearch
    Dim mconSqlImage As SqlClient.SqlConnection
    Dim conSqlMaster As New SqlClient.SqlConnection

    Protected WithEvents FileName As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents Title As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents Tags As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents CurrentVersion As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblCurrentVersion As System.Web.UI.WebControls.Label
    Protected WithEvents tblCheckInOut As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents lblCheckOutStatus As System.Web.UI.WebControls.Label
    Protected WithEvents btnCheckOut As System.Web.UI.WebControls.ImageButton
    Protected WithEvents btnCancelCheckOut As System.Web.UI.WebControls.ImageButton
    Protected WithEvents UploadDirections As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents chkNewVersion As System.Web.UI.WebControls.CheckBox
    Protected WithEvents Description As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents txtVersionComments As System.Web.UI.WebControls.TextBox
    Protected WithEvents CommentLabel As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents btnSave As System.Web.UI.WebControls.ImageButton
    Protected WithEvents dgAttachmentVersionDetail As System.Web.UI.WebControls.DataGrid
    Protected WithEvents lblModifyUser As System.Web.UI.WebControls.Label
    Protected WithEvents lblModifyDate As System.Web.UI.WebControls.Label
    Protected WithEvents lblHeaderComments As System.Web.UI.WebControls.Label
    Protected WithEvents RadProgressArea1 As RadProgressArea
    Protected WithEvents RadProgressManager1 As RadProgressManager
    Protected WithEvents Upload As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents lblNoVersions As System.Web.UI.WebControls.Label
    Protected WithEvents radProgress As RadProgressArea
    Protected WithEvents LinkButton1 As System.Web.UI.WebControls.LinkButton
    Protected WithEvents lnkFilename As System.Web.UI.WebControls.LinkButton
    Protected WithEvents Comments As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents txtUploadFilename As System.Web.UI.HtmlControls.HtmlInputFile
    Protected WithEvents litHide As System.Web.UI.WebControls.Literal
    Protected WithEvents Cancel As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents SetCancel As System.Web.UI.WebControls.ImageButton
    Protected WithEvents litRedirect As System.Web.UI.WebControls.Literal
    Protected WithEvents Hidden1 As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents versionId As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents lblError As System.Web.UI.WebControls.Label
#End Region

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Enum Tables
        HEADER = 0
        DETAIL = 1
    End Enum

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'Client side script binding
        btnSave.Attributes("style") = "VISIBILITY: hidden"
        'btnSave.Attributes.Add("onclick", "document.getElementById('CheckFlag').value = 0;window[""RadProgressManager""].StartProgressPolling();")
        btnSave.Attributes.Add("onclick", "document.getElementById('CheckFlag').value = 0;startPolling();")
        txtUploadFilename.Attributes.Add("onchange", "javascript:parent.frames['frameAttributes'].ActivateSave();")

        btnCheckOut.Attributes("onclick") = "document.getElementById('CheckFlag').value = '1';"
        btnCancelCheckOut.Attributes("onclick") = "document.getElementById('CheckFlag').value = '1';"

        'RegisterExcludeControl(btnSave)
        'RegisterExcludeControl(lnkFilename)

        Try

            Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
            Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))
            Me.mobjUser.AccountId = guidAccountId
            Me.mcookieSearch = New cookieSearch(Me)
            Me.mconSqlImage = New SqlClient.SqlConnection
            chkNewVersion.Attributes("onclick") = "toggleComments();"

            If Not Page.IsPostBack Then

                Dim objAttachments As New FileAttachments(Functions.BuildConnection(Me.mobjUser))
                Dim ds As New DataSet
                ds = objAttachments.AttachmentDetailsByVersionId(Me.mcookieSearch.VersionID, Me.mobjUser.TimeDiffFromGMT)

                versionId.Value = Me.mcookieSearch.VersionID
                'ReturnControls.Add(versionId)

                If ds.Tables(Tables.DETAIL).Rows.Count = 0 Then
                    dgAttachmentVersionDetail.Visible = False
                    lblNoVersions.Text = "No previous versions available."
                    lblNoVersions.Visible = True
                Else
                    dgAttachmentVersionDetail.DataSource = ds.Tables(Tables.DETAIL)
                    dgAttachmentVersionDetail.DataBind()
                    lblNoVersions.Text = ""
                    lblNoVersions.Visible = False
                End If

                'Check permissions
                Dim intVersionId As Integer = Me.mcookieSearch.VersionID
                Dim dsVersionControl As Accucentric.docAssist.Data.dsVersionControl

                mconSqlImage = Functions.BuildConnection(Me.mobjUser)

                If Session(intVersionId & "VersionControl") Is Nothing Then
                    dsVersionControl = Accucentric.docAssist.Data.GetVersionControl(intVersionId, Me.mconSqlImage, Me.mobjUser.ImagesUserId)
                    Session(intVersionId & "VersionControl") = dsVersionControl
                Else
                    dsVersionControl = Session(intVersionId & "VersionControl")
                    ' Check to make sure we haven't changed versions

                    If Not CType(dsVersionControl.ImageVersion.Rows(0), Data.dsVersionControl.ImageVersionRow).VersionID = intVersionId Then
                        dsVersionControl = Data.GetVersionControl(intVersionId, Me.mconSqlImage)
                        Session(intVersionId & "VersionControl") = dsVersionControl
                        'lblPageNo.Value = 1
                    End If
                End If

                Dim intDocumentId As Integer = CType(dsVersionControl.Images.Rows(0), Accucentric.docAssist.Data.dsVersionControl.ImagesRow).DocumentID

                Dim Rights As Accucentric.docAssist.Web.Security.SecurityLevel
                Rights = Functions.DocumentSecurityLevel(Me.mcookieSearch.VersionID, Me.mobjUser, Me.mconSqlImage)

                lnkFilename.Text = ds.Tables(Tables.HEADER).Rows(0)("FileName") & " (" & Functions.GetBytesAsMegs(ds.Tables(Tables.HEADER).Rows(0)("Size")) & ")"
                lnkFilename.Visible = True
                lblModifyUser.Text = ds.Tables(Tables.HEADER).Rows(0)("UserName")
                lblModifyDate.Text = CDate(ds.Tables(Tables.HEADER).Rows(0)("ModifiedDate")).ToShortDateString & " " & CDate(ds.Tables(Tables.HEADER).Rows(0)("ModifiedDate")).ToShortTimeString
                lblCurrentVersion.Text = ds.Tables(Tables.HEADER).Rows(0)("VersionNum")

                If CStr(ds.Tables(Tables.HEADER).Rows(0)("Comments")).Trim.Length > 0 Then
                    lblHeaderComments.Text = ds.Tables(Tables.HEADER).Rows(0)("Comments")
                    lblHeaderComments.Visible = True
                    Comments.Visible = True
                Else
                    lblHeaderComments.Visible = False
                    Comments.Visible = False
                End If

                If Functions.ModuleSetting(Functions.ModuleCode.VERSION_CONTROL, Me.mobjUser) Then
                    'Version Control Enabled

                    'Account level version flag
                    conSqlMaster = Functions.BuildMasterConnection
                    Dim cmdSql As New SqlClient.SqlCommand("FAVersionModeGet", conSqlMaster)
                    cmdSql.CommandType = CommandType.StoredProcedure
                    cmdSql.Parameters.Add("@AccountID", Me.mobjUser.AccountId.ToString)

                    Select Case cmdSql.ExecuteScalar
                        Case 0
                            Upload.Visible = False
                        Case 1

                        Case 2
                            Upload.Visible = True
                            chkNewVersion.Checked = True
                            chkNewVersion.Enabled = False
                            txtVersionComments.Enabled = True
                            txtVersionComments.CssClass = ""
                    End Select

                Else

                    'Version Control Disabled
                    lblCheckOutStatus.Visible = False
                    btnCheckOut.Visible = False
                    btnCancelCheckOut.Visible = False
                    Upload.Visible = True
                    CommentLabel.Visible = False
                    txtVersionComments.Visible = False
                    chkNewVersion.Visible = False
                    dgAttachmentVersionDetail.Visible = False

                End If

                'Check In/Out Table
                If ds.Tables(Tables.HEADER).Rows(0)("CheckOutStatus") = True Then
                    If mobjUser.ImagesUserId = ds.Tables(Tables.HEADER).Rows(0)("CheckOutUserId") And Rights > Web.Security.Functions.SecurityLevel.Read Then
                        lblCheckOutStatus.Text = "This file is currently checked out by you."
                        btnCancelCheckOut.Visible = True
                        Upload.Visible = True
                    Else
                        lblCheckOutStatus.Text = "This file is currently checked out by " & ds.Tables(Tables.HEADER).Rows(0)("CheckOutUserName")
                        If mobjUser.SysAdmin Then
                            Upload.Visible = False
                            btnCancelCheckOut.Visible = True
                        Else
                            Upload.Visible = False
                            btnCancelCheckOut.Visible = False
                        End If
                    End If
                Else
                    If Rights > Web.Security.Functions.SecurityLevel.Read Or mobjUser.SysAdmin Then
                        Upload.Visible = True
                        btnCheckOut.Visible = True
                    Else
                        Upload.Visible = False
                    End If
                End If

            End If

        Catch ex As Exception

            Dim conMaster As SqlClient.SqlConnection
            conMaster = Functions.BuildMasterConnection
            Functions.LogException(mobjUser.UserId.ToString, mobjUser.AccountId.ToString, "", "ViewAttachments.aspx Page Load Error: " & ex.Message, ex.StackTrace, Server.MachineName, "", Request.UrlReferrer.ToString, conMaster, True)

#If DEBUG Then
            lblError.Text = ex.Message
#Else
            lblerror.Text = "An error has occured."
#End If

        Finally

            If conSqlMaster.State <> ConnectionState.Closed Then
                conSqlMaster.Close()
                conSqlMaster.Dispose()
            End If

            If mconSqlImage.State <> ConnectionState.Closed Then
                mconSqlImage.Close()
                mconSqlImage.Dispose()
            End If

        End Try

    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Response.Redirect("FileAttachments.aspx")
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click

        Dim sqlConCompany As New SqlClient.SqlConnection
        sqlConCompany = Functions.BuildConnection(Me.mobjUser)
        sqlConCompany.Open()

        Dim ds As New DataSet
        Dim LatestVersionId As Integer

        Try

            If Session("Cancel") = "1" Then
                Session("Cancel") = "0"
                Exit Sub
            End If

            Dim objAttachments As New FileAttachments(sqlConCompany, False)

            Dim uploadContext As RadUploadContext
            uploadContext = RadUploadContext.Current

            Dim uploadedFiles As UploadedFileCollection
            uploadedFiles = uploadContext.UploadedFiles

            If uploadedFiles.Count > 0 Then

                Dim strSaveFile As String

                Dim file As UploadedFile = uploadedFiles(0)

                strSaveFile = ConfigurationSettings.AppSettings("UploadPath") & System.IO.Path.GetFileName(file.FileName)

                file.SaveAs(strSaveFile)

                Dim byteArray As Byte() = GetDataAsByteArray(strSaveFile)

                LatestVersionId = objAttachments.UpdateAttachment(0, Me.mobjUser.ImagesUserId, txtVersionComments.Text, file.ContentLength, _
                System.IO.Path.GetFileName(file.FileName), byteArray, mcookieSearch.VersionID, chkNewVersion.Checked)

                versionId.Value = LatestVersionId
                'ReturnControls.Add(versionId)

                Try
                    If System.IO.File.Exists(strSaveFile) Then System.IO.File.Delete(strSaveFile)
                Catch ex As Exception

                End Try

                Dim blnCheckedOut As Boolean = objAttachments.IsFileChekedOut(mcookieSearch.VersionID)
                If blnCheckedOut Then
                    objAttachments.CheckInByVersionId(mcookieSearch.VersionID, Me.mobjUser.ImagesUserId, txtVersionComments.Text)
                    btnCancelCheckOut.Visible = False
                End If

                'Track attachment
                Try
                    Accucentric.docAssist.Web.Data.Functions.TrackPageRequest(Me.mobjUser.UserId, Me.mobjUser.AccountId, "Attachment Add", "In", Me.mcookieSearch.VersionID.ToString, mcookieSearch.VersionID, Functions.GetMasterConnectionString)
                Catch ex As Exception

                End Try

                'ReturnScripts.Add("loadVersionId('" & LatestVersionId.ToString & "');")
                'litRedirect.Text = "<script language='javascript'>parent.location.replace='Index.aspx?VID=" & LatestVersionId.ToString & "';</script>"
                'Response.Redirect("Index.aspx?VID=" & LatestVersionId.ToString)
                'Exit Sub

                'Refresh header/versions grid

                ds = objAttachments.AttachmentDetailsByVersionId(LatestVersionId, Me.mobjUser.TimeDiffFromGMT)

                If ds.Tables(Tables.DETAIL).Rows.Count = 0 Then
                    dgAttachmentVersionDetail.Visible = False
                    'ReturnScripts.Add("document.all.VersionGrid.innerHTML = 'No previous versions avaialble.';")
                    lblNoVersions.Text = "No previous versions avaialble."
                    lblNoVersions.Visible = True
                Else
                    dgAttachmentVersionDetail.DataSource = ds.Tables(Tables.DETAIL)
                    dgAttachmentVersionDetail.DataBind()
                    lblNoVersions.Text = ""
                    lblNoVersions.Visible = False
                End If

                'Check permissions
                Dim intVersionId As Integer = Me.mcookieSearch.VersionID
                Dim dsVersionControl As Accucentric.docAssist.Data.dsVersionControl

                mconSqlImage = Functions.BuildConnection(Me.mobjUser)

                If Session(intVersionId & "VersionControl") Is Nothing Then
                    dsVersionControl = Accucentric.docAssist.Data.GetVersionControl(intVersionId, Me.mconSqlImage)
                    Session(intVersionId & "VersionControl") = dsVersionControl
                Else
                    dsVersionControl = Session(intVersionId & "VersionControl")
                    ' Check to make sure we haven't changed versions

                    If Not CType(dsVersionControl.ImageVersion.Rows(0), Data.dsVersionControl.ImageVersionRow).VersionID = intVersionId Then
                        dsVersionControl = Data.GetVersionControl(intVersionId, Me.mconSqlImage)
                        Session(intVersionId & "VersionControl") = dsVersionControl
                        'lblPageNo.Value = 1
                    End If
                End If

                Dim intDocumentId As Integer = CType(dsVersionControl.Images.Rows(0), Accucentric.docAssist.Data.dsVersionControl.ImagesRow).DocumentID

                Dim Rights As Accucentric.docAssist.Web.Security.SecurityLevel
                Rights = Functions.DocumentSecurityLevel(Me.mcookieSearch.VersionID, Me.mobjUser, Me.mconSqlImage)

                lnkFilename.Text = ds.Tables(Tables.HEADER).Rows(0)("FileName") & " (" & Functions.GetBytesAsMegs(ds.Tables(Tables.HEADER).Rows(0)("Size")) & ")"
                lnkFilename.Visible = True
                lblModifyUser.Text = ds.Tables(Tables.HEADER).Rows(0)("UserName")
                lblModifyDate.Text = CDate(ds.Tables(Tables.HEADER).Rows(0)("ModifiedDate")).ToShortDateString & " " & CDate(ds.Tables(Tables.HEADER).Rows(0)("ModifiedDate")).ToShortTimeString
                lblCurrentVersion.Text = ds.Tables(Tables.HEADER).Rows(0)("VersionNum")

                If CStr(ds.Tables(Tables.HEADER).Rows(0)("Comments")).Trim.Length > 0 Then
                    lblHeaderComments.Text = ds.Tables(Tables.HEADER).Rows(0)("Comments")
                    lblHeaderComments.Visible = True
                    Comments.Visible = True
                Else
                    lblHeaderComments.Visible = False
                    Comments.Visible = False
                End If

                'Check In/Out Table
                If ds.Tables(Tables.HEADER).Rows(0)("CheckOutStatus") = True Then
                    If mobjUser.ImagesUserId = ds.Tables(Tables.HEADER).Rows(0)("CheckOutUserId") And Rights > Web.Security.Functions.SecurityLevel.Read Then
                        lblCheckOutStatus.Text = "This file is currently checked out by you."
                        btnCancelCheckOut.Visible = True
                        Upload.Visible = True
                    Else
                        lblCheckOutStatus.Text = "This file is currently checked out by " & ds.Tables(Tables.HEADER).Rows(0)("CheckOutUserName")
                        If mobjUser.SysAdmin Then
                            Upload.Visible = True
                            btnCancelCheckOut.Visible = True
                        Else
                            Upload.Visible = False
                            btnCancelCheckOut.Visible = False
                        End If
                    End If
                Else
                    If Rights > Web.Security.Functions.SecurityLevel.Read Or mobjUser.SysAdmin Then
                        Upload.Visible = True
                        btnCheckOut.Visible = True
                    Else
                        Upload.Visible = False
                    End If
                End If


                ds = objAttachments.AttachmentDetailsByVersionId(LatestVersionId, Me.mobjUser.TimeDiffFromGMT)

                If ds.Tables(Tables.DETAIL).Rows.Count = 0 Then
                    dgAttachmentVersionDetail.Visible = False
                    lblNoVersions.Text = "No previous versions avaialble."
                    lblNoVersions.Visible = True
                Else
                    dgAttachmentVersionDetail.Visible = True
                    dgAttachmentVersionDetail.DataSource = ds.Tables(Tables.DETAIL)
                    dgAttachmentVersionDetail.DataBind()
                    lblNoVersions.Text = ""
                    lblNoVersions.Visible = False
                End If


                If Functions.ModuleSetting(Functions.ModuleCode.VERSION_CONTROL, Me.mobjUser) Then
                    'Version Control Enabled

                    'Account level version flag
                    conSqlMaster = Functions.BuildMasterConnection
                    Dim cmdSql As New SqlClient.SqlCommand("FAVersionModeGet", conSqlMaster)
                    cmdSql.CommandType = CommandType.StoredProcedure
                    cmdSql.Parameters.Add("@AccountID", Me.mobjUser.AccountId.ToString)

                    Select Case cmdSql.ExecuteScalar
                        Case 0
                            Upload.Visible = False
                        Case 1

                        Case 2
                            Upload.Visible = True
                            chkNewVersion.Checked = True
                            chkNewVersion.Enabled = False
                            txtVersionComments.Enabled = True
                            txtVersionComments.CssClass = ""
                    End Select

                Else

                    'Version Control Disabled
                    lblCheckOutStatus.Visible = False
                    btnCheckOut.Visible = False
                    btnCancelCheckOut.Visible = False
                    Upload.Visible = True
                    CommentLabel.Visible = False
                    txtVersionComments.Visible = False
                    chkNewVersion.Visible = False
                    dgAttachmentVersionDetail.Visible = False

                End If

                txtVersionComments.Text = ""

            End If

        Catch ex As Exception

#If DEBUG Then
            lblError.Text = ex.Message
#Else
            lblerror.Text = "An error has occured."
#End If

        Finally

            If chkNewVersion.Checked Then
                If Me.mobjUser.CloseViewer Then
                    litHide.Text = "<script language='javascript'>parent.parent.updateVersionId(" & Me.mcookieSearch.VersionID & "," & LatestVersionId & ");parent.parent.GB_hide(false);</script>"
                Else
                    litHide.Text = "<script language='javascript'>parent.parent.updateVersionId(" & Me.mcookieSearch.VersionID & "," & LatestVersionId & ");</script>"
                End If
            Else
                If Me.mobjUser.CloseViewer Then
                    litHide.Text = "<script language='javascript'>parent.parent.GB_hide(false);</script>"
                End If
            End If

            If sqlConCompany.State <> ConnectionState.Closed Then
                sqlConCompany.Close()
                sqlConCompany.Dispose()
            End If

        End Try

    End Sub

    Private Sub btnCheckOut_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCheckOut.Click

        Try
            Dim objAttachments As New FileAttachments(Functions.BuildConnection(Me.mobjUser))
            objAttachments.CheckOutByVersionId(Me.mcookieSearch.VersionID, Me.mobjUser.ImagesUserId)
            btnCheckOut.Visible = False
            btnCancelCheckOut.Visible = True
            lblCheckOutStatus.Text = "This file is currently checked out by you."
        Catch ex As Exception

        Finally
            'ReturnControls.Add(lblCheckOutStatus)
            'ReturnControls.Add(btnCheckOut)
            'ReturnControls.Add(btnCancelCheckOut)
        End Try

    End Sub

    Private Sub dgAttachmentVersionDetail_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgAttachmentVersionDetail.ItemCommand

        Try

            Select Case e.CommandName
                Case "Select"
                    Dim lblDetailVersionId As Label = CType(e.Item.FindControl("lblDetailVersionId"), Label)
                    If Not IsNothing(lblDetailVersionId) Then
                        Dim intVersionId As Integer = lblDetailVersionId.Text

                        'Create file and push it to the user
                        Dim objAttachments As New FileAttachments(Functions.BuildConnection(Me.mobjUser))
                        Dim dt As New DataTable
                        dt = objAttachments.GetFile(intVersionId)

                        Dim imgByte As Byte() = CType(dt.Rows(0)("Data"), Byte())

                        Response.Clear()
                        Response.AddHeader("Content-Disposition", "attachment; filename=" & Server.UrlEncode(dt.Rows(0)("Filename")).Replace("+", "%20"))
                        Response.AddHeader("Content-Length", imgByte.Length)
                        Response.ContentType = "application/octet-stream"
                        Response.BinaryWrite(imgByte)

                        'Track attachment
                        Try
                            Accucentric.docAssist.Web.Data.Functions.TrackPageRequest(Me.mobjUser.UserId, Me.mobjUser.AccountId, "Attachment View", "In", Me.mcookieSearch.VersionID.ToString, lblDetailVersionId.Text.ToString, Functions.GetMasterConnectionString)
                        Catch ex As Exception

                        End Try

                        'Log recent item
                        Try
                            Dim blnReturn As Boolean = Functions.TrackRecentItem(Me.mobjUser, "VW", Me.mcookieSearch.VersionID)
                            If Not blnReturn Then
                                'error logging recent item
                            End If
                        Catch ex As Exception
                        End Try

                        Response.End()

                    End If

            End Select
        Catch ex As Exception

        End Try


    End Sub

    Private Sub dgAttachmentVersionDetail_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgAttachmentVersionDetail.ItemDataBound

        Try
            Dim lnkLoad As System.Web.UI.WebControls.LinkButton = CType(e.Item.FindControl("lnkLoad"), System.Web.UI.WebControls.LinkButton)
            Dim lblDetailSize As Label = CType(e.Item.FindControl("lblDetailSize"), Label)

            If Not IsNothing(lnkLoad) Then
                'RegisterExcludeControl(lnkLoad)
                lnkLoad.Attributes("onclick") = "document.getElementById('txtUploadFilename').value = '';"
            End If

            If Not IsNothing(lblDetailSize) Then
                lblDetailSize.Text = Functions.GetBytesAsMegs(lblDetailSize.Text)
                'ReturnControls.Add(lblDetailSize)
                lnkLoad.Attributes.Add("onclick", "javascript:HideProgress();")


            End If

        Catch ex As Exception

        Finally

        End Try

    End Sub

    Private Sub btnCancelCheckOut_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancelCheckOut.Click

        Dim conSql As New SqlClient.SqlConnection
        conSql = Functions.BuildConnection(Me.mobjUser)

        Try

            Dim objAttachments As New FileAttachments(conSql, False)
            objAttachments.CancelCheckOutByVersionId(Me.mcookieSearch.VersionID)
            btnCheckOut.Visible = True
            btnCancelCheckOut.Visible = False
            lblCheckOutStatus.Text = ""

            'Check if the upload box needs to be displayed (i.e. when an admin overrides a checkout)
            Dim ds As New DataSet
            ds = objAttachments.AttachmentDetailsByVersionId(Me.mcookieSearch.VersionID, Me.mobjUser.TimeDiffFromGMT)

            Dim Rights As Accucentric.docAssist.Web.Security.SecurityLevel
            Rights = Functions.DocumentSecurityLevel(Me.mcookieSearch.VersionID, Me.mobjUser, conSql)

            'Check In/Out Table
            If ds.Tables(Tables.HEADER).Rows(0)("CheckOutStatus") = True Then
                If mobjUser.ImagesUserId = ds.Tables(Tables.HEADER).Rows(0)("CheckOutUserId") And Rights > Web.Security.Functions.SecurityLevel.Read Then
                    lblCheckOutStatus.Text = "This file is currently checked out by you."
                    btnCancelCheckOut.Visible = True
                    Upload.Visible = True
                Else
                    lblCheckOutStatus.Text = "This file is currently checked out by " & ds.Tables(Tables.HEADER).Rows(0)("CheckOutUserName")
                    If mobjUser.SysAdmin Then
                        Upload.Visible = False
                        btnCancelCheckOut.Visible = True
                    Else
                        Upload.Visible = False
                        btnCancelCheckOut.Visible = False
                    End If
                End If
            Else
                If Rights > Web.Security.Functions.SecurityLevel.Read Or mobjUser.SysAdmin Then
                    Upload.Visible = True
                    btnCheckOut.Visible = True
                Else
                    Upload.Visible = False
                End If
            End If

        Catch ex As Exception

            Dim conMaster As SqlClient.SqlConnection
            conMaster = Functions.BuildMasterConnection
            Functions.LogException(mobjUser.UserId.ToString, mobjUser.AccountId.ToString, "", "Cancel Check Out Error: " & ex.Message, ex.StackTrace, Server.MachineName, "", Request.UrlReferrer.ToString, conMaster, True)

        Finally

            If conSql.State <> ConnectionState.Closed Then
                conSql.Close()
                conSql.Dispose()
            End If

            'ReturnControls.Add(Upload)
            'ReturnControls.Add(lblCheckOutStatus)
            'ReturnControls.Add(btnCheckOut)
            'ReturnControls.Add(btnCancelCheckOut)

        End Try

    End Sub

    Private Sub lnkFilename_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkFilename.Click

        Try

            'Create file and push it to the user
            Dim objAttachments As New FileAttachments(Functions.BuildConnection(Me.mobjUser))
            Dim dt As New DataTable

            Dim intVersionId As Integer = versionId.Value

            dt = objAttachments.GetFile(intVersionId)

            Dim imgByte As Byte() = CType(dt.Rows(0)("Data"), Byte())

            Response.Clear()
            Response.AddHeader("Content-Disposition", "attachment; filename=" & Server.UrlEncode(dt.Rows(0)("Filename")).Replace("+", "%20"))
            Response.AddHeader("Content-Length", imgByte.Length)
            Response.ContentType = "application/octet-stream"
            Response.BinaryWrite(imgByte)

            'Track attachment
            Try
                Accucentric.docAssist.Web.Data.Functions.TrackPageRequest(Me.mobjUser.UserId, Me.mobjUser.AccountId, "Attachment View", "Out", Me.mcookieSearch.VersionID, "", Functions.GetMasterConnectionString)
            Catch ex As Exception

            End Try

        Catch ex As Exception

        End Try

    End Sub

    Private Sub SetCancel_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles SetCancel.Click
        Session("Cancel") = "1"
        'ReturnControls.Add(Cancel)
    End Sub

End Class
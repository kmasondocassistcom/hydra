Imports System.Web
Imports System.Web.SessionState
Imports System.Globalization
Imports System.Threading

Public Class [Global]
    Inherits System.Web.HttpApplication

#Region " Component Designer Generated Code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Component Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Required by the Component Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Component Designer
    'It can be modified using the Component Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        components = New System.ComponentModel.Container()
    End Sub

#End Region

    Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the application is started
        'UnlockPICImagXpress.PS_Unlock(1908224437, 373696256, 1341647946, 36364)
#If DEBUG Then
        AddHandler System.AppDomain.CurrentDomain.DomainUnload, AddressOf AdUnloading
#End If
    End Sub

    Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the session is started
    End Sub

    Sub Application_BeginRequest(ByVal sender As Object, ByVal e As EventArgs)

        'If IO.File.Exists(Server.MapPath("./app_offline.htm")) Then
        '    Response.Status = "503 Unavailable (in Maintenance Mode)"
        '    Response.Write(String.Format("<html><h1>{0}</h1></html>", Response.Status))
        '    Response.End()
        'End If

        ' Fires at the beginning of each request
        Try
            If Request.UserLanguages.Length > 0 Then
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(Request.UserLanguages(0))
            End If
        Catch ex As Exception
            Dim strError As String = ex.Message
        End Try

    End Sub

    Sub Application_AuthenticateRequest(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires upon attempting to authenticate the use
    End Sub

    Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)

        Dim ex As New Exception
        ex = Server.GetLastError.GetBaseException

        Dim strUrl As String = Request.Url.ToString
        Dim strErrorMessage As String = ex.Message.ToString
        Dim strStackTrace As String = ex.StackTrace.ToString
        'Dim innerEx As String = ex.InnerException.ToString
        Dim errorId As Integer = 0

        '#If DEBUG Then
        'Response.Write(strErrorMessage & "<BR>" & strStackTrace)
        '#End If

        Try

            If Not strErrorMessage.IndexOf("docAssistImages.asmx") > -1 Then

                Dim conMaster As SqlClient.SqlConnection
                conMaster = Functions.BuildMasterConnection
                Dim objUser As Accucentric.docAssist.Web.Security.User = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
                Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))
                objUser.AccountId = guidAccountId
                errorId = Functions.LogException(objUser.UserId.ToString, objUser.AccountId.ToString, "", ex.Message, ex.StackTrace, Server.MachineName, "", Request.UrlReferrer.ToString, conMaster, True)

            End If


        Catch ex2 As Exception

        End Try

        Select Case ex.Message.ToString

            Case "Session state can only be used when enableSessionState is set to true, either in a configuration file or in the Page directive"
                Try

                    Dim aCookie As HttpCookie
                    Dim i As Integer
                    Dim cookieName As String
                    Dim limit As Integer = Request.Cookies.Count - 1
                    For i = 0 To limit

                        If Request.Cookies(i).Name = "docAssistSave" Then i += 1
                        If Request.Cookies(i).Name = SESSION_COOKIE_KEY Then i += 1
                        cookieName = Request.Cookies(i).Name
                        aCookie = New HttpCookie(cookieName)
                        aCookie.Expires = DateTime.Now.AddDays(-1)
                        Response.Cookies.Add(aCookie)

                    Next

                    Response.Cookies(SESSION_COOKIE_KEY)(SESSION_COOKIE) = Now
                    Response.Cookies(SESSION_COOKIE_KEY).Expires = Now.AddDays(7) 'Set this so that the next time the browser loads it wont load a timeout message, the login process will reset it to the interval.

                    Response.Redirect("Default.aspx?ErrId=" & ErrorCodes.SESSION_TIMEOUT)

                Catch excep As Exception
                    strErrorMessage = excep.Message
                End Try

                Exit Sub

            Case Else

                Response.Cookies("docAssistError")("Url") = strUrl
                Response.Cookies("docAssistError")("ErrorMessage") = strErrorMessage
                Response.Cookies("docAssistError")("StackTrace") = strStackTrace
                Server.Transfer("GeneralError.aspx?Id=" & errorId.ToString)

        End Select

    End Sub

    Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the session ends
    End Sub

    Sub Application_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the application ends
    End Sub

    Private Sub AdUnloading(ByVal sender As Object, ByVal e As EventArgs)
#If DEBUG Then
        System.Environment.Exit(0)
#End If
    End Sub

End Class

Partial Class InstallFramework
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        
    End Sub

    Private Sub btnSignOut_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSignOut.Click

        Dim aCookie As HttpCookie
        Dim i As Integer
        Dim cookieName As String
        Dim limit As Integer = Request.Cookies.Count - 1
        For i = 0 To limit
            If Request.Cookies(i).Name = "docAssistSave" Then i += 1
            cookieName = Request.Cookies(i).Name
            aCookie = New HttpCookie(cookieName)
            aCookie.Expires = DateTime.Now.AddDays(-1)
            Response.Cookies.Add(aCookie)
        Next

        Dim cookieSearch As New cookieSearch(Me.Page)
        cookieSearch.Expire()

        Response.Redirect("Default.aspx")

    End Sub
End Class

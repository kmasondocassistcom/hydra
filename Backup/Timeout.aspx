<%@ Page Language="vb" AutoEventWireup="false" Codebehind="Timeout.aspx.vb" Inherits="docAssistWeb.Timeout"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>docAssist - Session Timeout</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<META HTTP-EQUIV="Refresh" CONTENT="5;URL=https://my.docassist.com/Default.aspx">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<TABLE height="100%" cellSpacing="0" cellPadding="0" border="0">
				<TR>
				</TR>
				<TR>
					<TD width="50%"></TD>
					<TD align="left">
						<TABLE cellSpacing="0" cellPadding="0" border="0" width="600" align="center">
							<TBODY>
								<TR>
									<TD style="WIDTH: 458px" width="458"><IMG src="Images/doc_header_logo.gif">
									</TD>
								</TR>
								<tr>
									<td><img src="spacer.gif" height="5" width="1"></td>
								</tr>
								<TR>
									<TD bgColor="#cc0000" colSpan="3" height="5"><img src="spacer.gif" height="8" width="1"></TD>
								<TR>
									<TD style="WIDTH: 459px" align="center" colSpan="2"><img src="spacer.gif" height="1" width="800">
									</TD>
								</TR>
							</TBODY>
						</TABLE>
						<font face="verdana" size="2">
							<P style="FONT-SIZE: 8pt; FONT-FAMILY: verdana">Your session has expired.
							</P>
							<P style="FONT-SIZE: 8pt; FONT-FAMILY: verdana">
							In 5 seconds you will automatically be redirected to the login page. After 
							logging in you can resume your session. </font>
						<BR>
						<BR>
						<BR>
						<BR>
						<BR>
						</P>
						<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
							<TR bgColor="#999999" height="3">
								<TD></TD>
							</TR>
							<TR bgColor="#cc0000" height="25">
								<TD style="FONT-SIZE: 7pt; COLOR: white; FONT-FAMILY: Verdana" align="right">
									&nbsp;</TD>
							</TR>
							<TR bgColor="#999999" height="3">
								<TD></TD>
							</TR>
						</TABLE>
						<img src="spacer.gif" height="1" width="800">
					</TD>
					<TD align="right" width="50%"></TD>
				</TR>
				<TR>
					<TD>&nbsp;</TD>
					<TD>&nbsp;</TD>
					<TD>&nbsp;</TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>

Partial Class ManageDocumentsAttributes
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private mobjUser As Accucentric.docAssist.Web.Security.User
    Private mconSqlImage As SqlClient.SqlConnection

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        txtDocumentName.Attributes("onkeypress") = "catchSubmit();"

        If Not Page.IsPostBack Then
            txtDocumentName.Text = Session("mAdminDocumentName")
            txtDocumentName.ReadOnly = True
            PopulateAttributesList()
        End If

    End Sub


    Private Sub PopulateAttributesList()

        Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
        Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))

        Me.mobjUser.AccountId = guidAccountId
        Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
        Me.mconSqlImage.Open()

        Dim cmdSql As SqlClient.SqlCommand

        Dim dtsql As New DataTable
        cmdSql = New SqlClient.SqlCommand("acsAdminAttributeList", Me.mconSqlImage)
        cmdSql.CommandType = CommandType.StoredProcedure
        Dim dasql As New SqlClient.SqlDataAdapter(cmdSql)
        dasql.Fill(dtsql)

        If dtsql.Rows.Count <= 0 Then
            'TODO: No Cabinets to change
        Else
            lstAttributes.DataTextField = "AttributeName"
            lstAttributes.DataValueField = "AttributeID"
            lstAttributes.DataSource = dtsql
            lstAttributes.DataBind()
        End If

    End Sub

    Private Sub lnkFinish_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        Session("SelectedAttributes") = AttributesSelected()
        If Session("SelectedAttributes").rows.count <= 0 Then
            lblError.Text = "You must select at least one attribute."
            Exit Sub
        End If
        Response.Redirect("ManageDocumentsAttributesOrder.aspx")

    End Sub

    Private Function AttributesSelected() As DataTable
        Dim dtAttributes As New DataTable
        Dim dcID As New DataColumn("AttributeID", GetType(System.Int64), Nothing)
        Dim dcValue As New DataColumn("AttributeName", GetType(System.String), Nothing)
        Dim dcOrder As New DataColumn("DisplayOrder", GetType(System.Int64), Nothing)
        dtAttributes.Columns.Add(dcID)
        dtAttributes.Columns.Add(dcValue)
        dtAttributes.Columns.Add(dcOrder)


        Dim iCount As Integer = lstAttributes.Items.Count
        Dim x


        For x = 0 To iCount - 1
            If lstAttributes.Items(x).Selected = True Then
                Dim dr As DataRow
                dr = dtAttributes.NewRow
                dr("AttributeID") = lstAttributes.Items(x).Value
                dr("AttributeName") = lstAttributes.Items(x).Text
                dr("DisplayOrder") = x + 1
                dtAttributes.Rows.Add(dr)
            End If

        Next

        Return dtAttributes

    End Function


    Private Sub btnNext_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnNext.Click

        Session("SelectedAttributes") = AttributesSelected()
        If Session("SelectedAttributes").rows.count <= 0 Then
            lblError.Text = "You must select at least one attribute."
            Exit Sub
        End If
        Response.Redirect("ManageDocumentsAttributesOrder.aspx")

    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("ManageDocuments.aspx")

    End Sub
End Class

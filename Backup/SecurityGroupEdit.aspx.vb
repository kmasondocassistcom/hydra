Imports Accucentric.docAssist
Imports Accucentric.docAssist.Data.Images
Imports System.Data.SqlClient

Partial Class SecurityGroupEdit

    Inherits System.Web.UI.Page

    Private mobjUser As Accucentric.docAssist.Web.Security.User
    Private mconSqlCompany As SqlClient.SqlConnection


    Dim intGroupId As Integer

    Private dtAssisgnedUsers As New DataTable
    Private dtAvailableUsers As New DataTable

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        rdoAdvancedPerms.Attributes("onclick") = "setEnable();"

        'Page scripting
        txtGroupName.Attributes("onkeypress") = "ignoreEvent();"
        txtGroupDesc.Attributes("onkeypress") = "ignoreEvent();"

        Dim conSql As SqlClient.SqlConnection

        If Not Page.IsPostBack Then

            Try

                intGroupId = Request.QueryString("ID")

                If intGroupId = 975 Then
                    lstAssigned.Enabled = False
                    lstAvailable.Enabled = False
                    btnAdd.Enabled = False
                    btnAssignAll.Enabled = False
                    btnRemove.Enabled = False
                    btnRemoveAll.Enabled = False
                    txtGroupName.Enabled = False
                    txtGroupDesc.Enabled = False
                End If

                If Request.Cookies("docAssist") Is Nothing Then
                    Response.Redirect("Default.aspx")
                    Exit Sub
                End If

                Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
                Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))

                Me.mobjUser.AccountId = guidAccountId
                Me.mconSqlCompany = Functions.BuildConnection(Me.mobjUser)

                If Not (Me.mobjUser.SecAdmin = True Or Me.mobjUser.SysAdmin = True) Then
                    System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, "script", "alert('Access denied.');window.location.href='Default.aspx';", True)
                    Exit Sub
                End If

                conSql = Functions.BuildConnection(Me.mobjUser)

                Dim objGroups As New Security(conSql, False)

                If Request.QueryString("Mode") = "Copy" Then

                    Dim ds As New DataSet
                    ds = objGroups.GroupGetById(intGroupId)

                    rdoAdvancedPerms.Checked = ds.Tables(0).Rows(0)("DocumentAttributeRightsEnabled")

                    If rdoAdvancedPerms.Checked = False Then
                        System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, "script", "disable();", True)
                    Else
                        System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, "script", "enable();", True)
                    End If

                    dgGroupPermissions.DataSource = ds.Tables(1)
                    dgGroupPermissions.DataBind()
                    dgGroupPermissions.Visible = True

                    lstAvailable.DataSource = ds.Tables(2)
                    lstAvailable.DataValueField = "UserId"
                    lstAvailable.DataTextField = "UserName"
                    lstAvailable.DataBind()

                    lstAssigned.DataSource = ds.Tables(3)
                    lstAssigned.DataValueField = "UserId"
                    lstAssigned.DataTextField = "UserName"
                    lstAssigned.DataBind()

                    'Get attribute settings
                    Dim dt As New DataTable
                    dt = objGroups.SecurityDocumentAttributeGetList(intGroupId)
                    dgAttributes.DataSource = dt
                    dgAttributes.DataBind()

                Else

                    If intGroupId > 0 Then 'Load

                        Dim ds As New DataSet
                        ds = objGroups.GroupGetById(intGroupId)

                        rdoAdvancedPerms.Checked = ds.Tables(0).Rows(0)("DocumentAttributeRightsEnabled")

                        If rdoAdvancedPerms.Checked = False Then
                            System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, "script", "disable();", True)
                        Else
                            System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, "script", "enable();", True)
                        End If

                        txtGroupName.Text = ds.Tables(0).Rows(0)("GroupName")
                        txtGroupDesc.Text = ds.Tables(0).Rows(0)("GroupDesc")

                        rdoAdvancedPerms.Checked = ds.Tables(0).Rows(0)("DocumentAttributeRightsEnabled")

                        dgGroupPermissions.DataSource = ds.Tables(1)
                        dgGroupPermissions.DataBind()
                        dgGroupPermissions.Visible = True

                        lstAvailable.DataSource = ds.Tables(2)
                        lstAvailable.DataValueField = "UserId"
                        lstAvailable.DataTextField = "UserName"
                        lstAvailable.DataBind()

                        lstAssigned.DataSource = ds.Tables(3)
                        lstAssigned.DataValueField = "UserId"
                        lstAssigned.DataTextField = "UserName"
                        lstAssigned.DataBind()

                        'Get attribute settings
                        Dim dt As New DataTable
                        dt = objGroups.SecurityDocumentAttributeGetList(intGroupId)
                        dgAttributes.DataSource = dt
                        dgAttributes.DataBind()

                        'Lookup settings
                        Dim dtLookups As New DataTable
                        dtLookups = objGroups.SecurityDocumentAttributeLookupGetList(intGroupId)
                        dgLookups.DataSource = dtLookups
                        dgLookups.DataBind()


                    Else 'New

                        Dim ds As New DataSet
                        ds = objGroups.GroupGetById(intGroupId)

                        'rdoAdvancedPerms.Checked = ds.Tables(0).Rows(0)("DocumentAttributeRightsEnabled")

                        'If rdoAdvancedPerms.Checked = False Then
                        System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, "script", "disable();allowMassAction=false;", True)
                        'Else
                        '    ReturnScripts.Add("enable();")
                        'End If

                        dgGroupPermissions.DataSource = ds.Tables(1)
                        dgGroupPermissions.DataBind()
                        dgGroupPermissions.Visible = True

                        lstAvailable.DataSource = ds.Tables(2)
                        lstAvailable.DataValueField = "UserId"
                        lstAvailable.DataTextField = "UserName"
                        lstAvailable.DataBind()

                        'Get attribute settings
                        Dim dt As New DataTable
                        dt = objGroups.SecurityDocumentAttributeGetList(intGroupId)
                        dgAttributes.DataSource = dt
                        dgAttributes.DataBind()

                        'Lookup settings
                        Dim dtLookups As New DataTable
                        dtLookups = objGroups.SecurityDocumentAttributeLookupGetList(intGroupId)
                        dgLookups.DataSource = dtLookups
                        dgLookups.DataBind()

                    End If

                End If

                SelectScripts()

            Catch ex As Exception

                If ex.Message = "DualLogin" Then
                    Response.Cookies("docAssist")("DualLogin") = True
                    Server.Transfer("Default.aspx")
                Else
                    Response.Redirect("Default.aspx")
                    Response.Flush()
                    Response.End()
                End If

            Finally

                If conSql.State <> ConnectionState.Closed Then
                    conSql.Close()
                    conSql.Dispose()
                End If

            End Try

        End If

    End Sub

    Private Function SelectScripts()

        Dim sbCreatorAll As New System.Text.StringBuilder
        Dim sbCreatorNone As New System.Text.StringBuilder

        Dim sbCurrentAll As New System.Text.StringBuilder
        Dim sbCurrentNone As New System.Text.StringBuilder

        Dim sbPrintAll As New System.Text.StringBuilder
        Dim sbPrintNone As New System.Text.StringBuilder

        Dim sbEmailAll As New System.Text.StringBuilder
        Dim sbEmailNone As New System.Text.StringBuilder

        Dim sbAnnotationsAll As New System.Text.StringBuilder
        Dim sbAnnotationsNone As New System.Text.StringBuilder

        Dim sbRedactionsAll As New System.Text.StringBuilder
        Dim sbRedactionsNone As New System.Text.StringBuilder

        Dim sbNoAccessAll As New System.Text.StringBuilder
        Dim sbReadOnlyAll As New System.Text.StringBuilder
        Dim sbChangeAll As New System.Text.StringBuilder
        Dim sbDeleteAll As New System.Text.StringBuilder


        sbCreatorAll.Append("function CreatorAll(){document.all.CreatorAll.href='javascript:CreatorNone();';document.all.CreatorAll.innerHTML='None';")
        sbCreatorNone.Append("function CreatorNone(){document.all.CreatorAll.href='javascript:CreatorAll();';document.all.CreatorAll.innerHTML='All';")

        sbCurrentAll.Append("function CurrentAll(){document.all.CurrentAll.href='javascript:CurrentNone();';document.all.CurrentAll.innerHTML='None';")
        sbCurrentNone.Append("function CurrentNone(){document.all.CurrentAll.href='javascript:CurrentAll();';document.all.CurrentAll.innerHTML='All';")

        sbPrintAll.Append("function PrintAll(){document.all.PrintAll.href='javascript:PrintNone();';document.all.PrintAll.innerHTML='None';")
        sbPrintNone.Append("function PrintNone(){document.all.PrintAll.href='javascript:PrintAll();';document.all.PrintAll.innerHTML='All';")

        sbEmailAll.Append("function MailAll(){document.all.MailAll.href='javascript:MailNone();';document.all.MailAll.innerHTML='None';")
        sbEmailNone.Append("function MailNone(){document.all.MailAll.href='javascript:MailAll();';document.all.MailAll.innerHTML='All';")

        sbAnnotationsAll.Append("function AnnotationsAll(){document.all.AnnotationsAll.href='javascript:AnnotationsNone();';document.all.AnnotationsAll.innerHTML='None';")
        sbAnnotationsNone.Append("function AnnotationsNone(){document.all.AnnotationsAll.href='javascript:AnnotationsAll();';document.all.AnnotationsAll.innerHTML='All';")

        sbRedactionsAll.Append("function RedactionsAll(){document.all.RedactionsAll.href='javascript:RedactionsNone();';document.all.RedactionsAll.innerHTML='None';")
        sbRedactionsNone.Append("function RedactionsNone(){document.all.RedactionsAll.href='javascript:RedactionsAll();';document.all.RedactionsAll.innerHTML='All';")

        sbNoAccessAll.Append("function NoAccessAll(){")
        sbReadOnlyAll.Append("function ReadOnlyAll(){")
        sbChangeAll.Append("function ChangeAll(){")
        sbDeleteAll.Append("function DeleteAll(){")

        Try

            For Each dgi As DataGridItem In dgGroupPermissions.Items

                Dim chkCreatorOnly As CheckBox = CType(dgi.FindControl("chkCreatorOnly"), CheckBox)
                If Not IsNothing(chkCreatorOnly) Then
                    sbCreatorAll.Append("document.all." & chkCreatorOnly.ClientID & ".checked = true;")
                    sbCreatorNone.Append("document.all." & chkCreatorOnly.ClientID & ".checked = false;")
                End If

                Dim chkCurrentVersionOnly As CheckBox = CType(dgi.FindControl("chkCurrentVersionOnly"), CheckBox)
                If Not IsNothing(chkCurrentVersionOnly) Then
                    sbCurrentAll.Append("document.all." & chkCurrentVersionOnly.ClientID & ".checked = true;")
                    sbCurrentNone.Append("document.all." & chkCurrentVersionOnly.ClientID & ".checked = false;")
                End If

                Dim chkPrint As CheckBox = CType(dgi.FindControl("chkPrint"), CheckBox)
                If Not IsNothing(chkPrint) Then
                    sbPrintAll.Append("document.all." & chkPrint.ClientID & ".checked = true;")
                    sbPrintNone.Append("document.all." & chkPrint.ClientID & ".checked = false;")
                End If

                Dim chkEmailExport As CheckBox = CType(dgi.FindControl("chkEmailExport"), CheckBox)
                If Not IsNothing(chkEmailExport) Then
                    sbEmailAll.Append("document.all." & chkEmailExport.ClientID & ".checked = true;")
                    sbEmailNone.Append("document.all." & chkEmailExport.ClientID & ".checked = false;")
                End If

                Dim chkAnnotations As CheckBox = CType(dgi.FindControl("chkAnnotations"), CheckBox)
                If Not IsNothing(chkAnnotations) Then
                    sbAnnotationsAll.Append("document.all." & chkAnnotations.ClientID & ".checked = true;")
                    sbAnnotationsNone.Append("document.all." & chkAnnotations.ClientID & ".checked = false;")
                End If

                Dim chkRedactions As CheckBox = CType(dgi.FindControl("chkRedactions"), CheckBox)
                If Not IsNothing(chkRedactions) Then
                    sbRedactionsAll.Append("document.all." & chkRedactions.ClientID & ".checked = true;")
                    sbRedactionsNone.Append("document.all." & chkRedactions.ClientID & ".checked = false;")
                End If

                Dim rdoNoAccess As CheckBox = CType(dgi.FindControl("rdoNoAccess"), RadioButton)
                If Not IsNothing(rdoNoAccess) Then
                    sbNoAccessAll.Append("document.all." & rdoNoAccess.ClientID & ".checked = true;")
                End If

                Dim rdoReadOnly As CheckBox = CType(dgi.FindControl("rdoReadOnly"), RadioButton)
                If Not IsNothing(rdoReadOnly) Then
                    sbReadOnlyAll.Append("document.all." & rdoReadOnly.ClientID & ".checked = true;")
                End If

                Dim rdoChange As CheckBox = CType(dgi.FindControl("rdoChange"), RadioButton)
                If Not IsNothing(rdoChange) Then
                    sbChangeAll.Append("document.all." & rdoChange.ClientID & ".checked = true;")
                End If

                Dim rdoDelete As CheckBox = CType(dgi.FindControl("rdoDelete"), RadioButton)
                If Not IsNothing(rdoNoAccess) Then
                    sbDeleteAll.Append("document.all." & rdoDelete.ClientID & ".checked = true;")
                End If

            Next

        Catch ex As Exception

        End Try

        sbCreatorAll.Append("}")
        sbCreatorNone.Append("}")
        sbCurrentAll.Append("}")
        sbCurrentNone.Append("}")
        sbPrintAll.Append("}")
        sbPrintNone.Append("}")

        sbEmailAll.Append("}")
        sbEmailNone.Append("}")
        sbAnnotationsAll.Append("}")
        sbAnnotationsNone.Append("}")
        sbRedactionsAll.Append("}")
        sbRedactionsNone.Append("}")

        sbNoAccessAll.Append("}")
        sbReadOnlyAll.Append("}")
        sbChangeAll.Append("}")
        sbDeleteAll.Append("}")

        System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, "1", sbCreatorAll.ToString, True)
        System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, "2", sbCreatorNone.ToString, True)
        System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, "3", sbCurrentAll.ToString, True)
        System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, "4", sbCurrentNone.ToString, True)
        System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, "5", sbPrintAll.ToString, True)
        System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, "6", sbPrintNone.ToString, True)
        System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, "7", sbEmailAll.ToString, True)
        System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, "8", sbEmailNone.ToString, True)
        System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, "9", sbAnnotationsAll.ToString, True)
        System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, "10", sbAnnotationsNone.ToString, True)
        System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, "11", sbRedactionsAll.ToString, True)
        System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, "12", sbRedactionsNone.ToString, True)
        System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, "13", sbNoAccessAll.ToString, True)
        System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, "14", sbReadOnlyAll.ToString, True)
        System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, "15", sbChangeAll.ToString, True)
        System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, "16", sbDeleteAll.ToString, True)

    End Function

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click

        Dim sqlTran As SqlClient.SqlTransaction
        Dim sqlConImages As New SqlClient.SqlConnection
        Dim cTmp As New SqlConnection

        Dim blnError As Boolean = False

        If txtGroupName.Text.Trim.Length = 0 Then
            System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, "script", "document.all.GroupNameLabel.className='ErrorText';", True)
            blnError = True
        End If

        If txtGroupDesc.Text.Trim.Length = 0 Then
            System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, "script", "document.all.GroupDescLabel.className='ErrorText';", True)
            blnError = True
        End If

        If blnError Then
            System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, "script", "document.all.ErrorMessage.innerHTML='Please correct the required fields.';document.all.ErrorMessage.className='ErrorText';", True)
        Else
            System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, "script", "document.all.ErrorMessage.innerHTML='';document.all.ErrorMessage.className='ErrorText';", True)
        End If

        If blnError Then Exit Sub

        Try

            intGroupId = Request.QueryString("ID")

            Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
            Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))
            Me.mobjUser.AccountId = guidAccountId

            sqlConImages = Functions.BuildConnection(Me.mobjUser)

            If sqlConImages.State <> ConnectionState.Open Then sqlConImages.Open()

            sqlTran = sqlConImages.BeginTransaction

            Dim objGroup As New Security(sqlConImages, False, sqlTran)

            If intGroupId > 0 Then 'Update

                'Collect users
                Dim dt As New DataTable
                dt.Columns.Add("UserID")
                dt.Columns.Add("GroupID")
                For Each lstItem As ListItem In lstAssigned.Items
                    Dim dr As DataRow
                    dr = dt.NewRow
                    dr("UserID") = lstItem.Value
                    dr("GroupID") = intGroupId
                    dt.Rows.Add(dr)
                Next

                cTmp = Functions.BuildConnection(Me.mobjUser)
                cTmp.Open()

                Dim tempTableName As String = "##" & System.Guid.NewGuid.ToString.Replace("-", "")
                Dim sqlCmd As New SqlCommand("CREATE TABLE " & tempTableName & " (UserID int, GroupID int)", cTmp)
                sqlCmd.ExecuteNonQuery()

                sqlCmd = New SqlCommand("SELECT * FROM " & tempTableName, cTmp)
                Dim sqlDa As New SqlDataAdapter(sqlCmd)
                Dim sqlBld As New SqlCommandBuilder(sqlDa)

                sqlDa.Update(dt)

                'Update header
                objGroup.GroupUpdateById(intGroupId, txtGroupName.Text, txtGroupDesc.Text)

                'Clear permissions
                objGroup.GroupUpdatePermissionsById(intGroupId, 0, 0, 0)

                For Each dgRowItem As DataGridItem In dgGroupPermissions.Items
                    Dim rdoNoAccess As RadioButton = CType(dgRowItem.FindControl("rdoNoAccess"), RadioButton)
                    Dim rdoReadOnly As RadioButton = CType(dgRowItem.FindControl("rdoReadOnly"), RadioButton)
                    Dim rdoChange As RadioButton = CType(dgRowItem.FindControl("rdoChange"), RadioButton)
                    Dim rdoDelete As RadioButton = CType(dgRowItem.FindControl("rdoDelete"), RadioButton)
                    Dim lblDocumentId As Label = CType(dgRowItem.FindControl("lblDocumentId"), Label)
                    Dim chkCreatorOnly As CheckBox = CType(dgRowItem.FindControl("chkCreatorOnly"), CheckBox)
                    Dim chkCurrentVersionOnly As CheckBox = CType(dgRowItem.FindControl("chkCurrentVersionOnly"), CheckBox)
                    Dim chkEmailExport As CheckBox = CType(dgRowItem.FindControl("chkEmailExport"), CheckBox)
                    Dim chkPrint As CheckBox = CType(dgRowItem.FindControl("chkPrint"), CheckBox)
                    Dim chkAnnotations As CheckBox = CType(dgRowItem.FindControl("chkAnnotations"), CheckBox)
                    Dim chkRedactions As CheckBox = CType(dgRowItem.FindControl("chkRedactions"), CheckBox)

                    Dim intSecAccess As Integer

                    If rdoNoAccess.Checked Then
                        intSecAccess = 0
                    ElseIf rdoReadOnly.Checked Then
                        intSecAccess = 1
                    ElseIf rdoChange.Checked Then
                        intSecAccess = 2
                    ElseIf rdoDelete.Checked Then
                        intSecAccess = 3
                    End If

                    objGroup.GroupUpdatePermissionsById(intGroupId, SecType.Access, lblDocumentId.Text, intSecAccess)
                    If chkCreatorOnly.Checked Then objGroup.GroupUpdatePermissionsById(intGroupId, SecType.CreatorOnly, lblDocumentId.Text, 1)
                    If chkCurrentVersionOnly.Checked Then objGroup.GroupUpdatePermissionsById(intGroupId, SecType.CurrentVersionOnly, lblDocumentId.Text, 1)
                    If chkEmailExport.Checked Then objGroup.GroupUpdatePermissionsById(intGroupId, SecType.Export, lblDocumentId.Text, 1)
                    If chkPrint.Checked Then objGroup.GroupUpdatePermissionsById(intGroupId, SecType.Print, lblDocumentId.Text, 1)
                    If chkAnnotations.Checked Then objGroup.GroupUpdatePermissionsById(intGroupId, SecType.Annotation, lblDocumentId.Text, 1)
                    If chkRedactions.Checked Then objGroup.GroupUpdatePermissionsById(intGroupId, SecType.Redaction, lblDocumentId.Text, 1)

                Next

                'Attribute Level Rights
                If rdoAdvancedPerms.Checked Then
                    For Each dgRowItem As DataGridItem In dgAttributes.Items
                        Dim lblAttributeId As Label = dgRowItem.FindControl("lblAttributeId")
                        Dim lblDocumentId As Label = dgRowItem.FindControl("lblDocumentId")
                        Dim ddlAccess As DropDownList = dgRowItem.FindControl("ddlAccess")
                        objGroup.GroupUpdatePermissionsById(intGroupId, SecType.Attributes, lblDocumentId.Text, ddlAccess.SelectedValue, lblAttributeId.Text)
                    Next
                End If

                'Lookup Level Rights
                For Each dgRowItem As DataGridItem In dgLookups.Items
                    Dim lblLookupAttributeId As Label = dgRowItem.FindControl("lblLookupAttributeId")
                    Dim lblDocumentId As Label = dgRowItem.FindControl("lblDocumentId")
                    Dim ddlLookup As DropDownList = dgRowItem.FindControl("ddlLookup")
                    objGroup.GroupUpdatePermissionsById(intGroupId, SecType.Lookups, lblLookupAttributeId.Text, ddlLookup.SelectedValue)
                Next

                sqlTran.Commit()

                'Update users
                objGroup.UserGroupMemberInsert(tempTableName, objGroup.GROUP_EDIT, intGroupId)

            ElseIf Request.QueryString("Mode") = "Copy" Or intGroupId = 0 Then

                'New

                'Update header
                Dim intNewGroupId As Integer = objGroup.GroupInsert(txtGroupName.Text, txtGroupDesc.Text)

                'Check that any forward action has at least one queue assigned
                For Each dgRowItem As DataGridItem In dgGroupPermissions.Items

                    Dim lblDocumentId As Label = CType(dgRowItem.FindControl("lblDocumentId"), Label)
                    Dim chkCreatorOnly As CheckBox = CType(dgRowItem.FindControl("chkCreatorOnly"), CheckBox)
                    Dim chkCurrentVersionOnly As CheckBox = CType(dgRowItem.FindControl("chkCurrentVersionOnly"), CheckBox)
                    Dim chkEmailExport As CheckBox = CType(dgRowItem.FindControl("chkEmailExport"), CheckBox)
                    Dim chkPrint As CheckBox = CType(dgRowItem.FindControl("chkPrint"), CheckBox)
                    Dim chkAnnotations As CheckBox = CType(dgRowItem.FindControl("chkAnnotations"), CheckBox)
                    Dim chkRedactions As CheckBox = CType(dgRowItem.FindControl("chkRedactions"), CheckBox)

                    Dim rdoNoAccess As RadioButton = CType(dgRowItem.FindControl("rdoNoAccess"), RadioButton)
                    Dim rdoReadOnly As RadioButton = CType(dgRowItem.FindControl("rdoReadOnly"), RadioButton)
                    Dim rdoChange As RadioButton = CType(dgRowItem.FindControl("rdoChange"), RadioButton)
                    Dim rdoDelete As RadioButton = CType(dgRowItem.FindControl("rdoDelete"), RadioButton)

                    Dim intSecAccess As Integer

                    If rdoNoAccess.Checked Then
                        intSecAccess = 0
                    ElseIf rdoReadOnly.Checked Then
                        intSecAccess = 1
                    ElseIf rdoChange.Checked Then
                        intSecAccess = 2
                    ElseIf rdoDelete.Checked Then
                        intSecAccess = 3
                    End If

                    objGroup.GroupUpdatePermissionsById(intNewGroupId, SecType.Access, lblDocumentId.Text, intSecAccess)
                    If chkCreatorOnly.Checked Then objGroup.GroupUpdatePermissionsById(intNewGroupId, SecType.CreatorOnly, lblDocumentId.Text, 1)
                    If chkCurrentVersionOnly.Checked Then objGroup.GroupUpdatePermissionsById(intNewGroupId, SecType.CurrentVersionOnly, lblDocumentId.Text, 1)
                    If chkEmailExport.Checked Then objGroup.GroupUpdatePermissionsById(intNewGroupId, SecType.Export, lblDocumentId.Text, 1)
                    If chkPrint.Checked Then objGroup.GroupUpdatePermissionsById(intNewGroupId, SecType.Print, lblDocumentId.Text, 1)
                    If chkAnnotations.Checked Then objGroup.GroupUpdatePermissionsById(intNewGroupId, SecType.Annotation, lblDocumentId.Text, 1)
                    If chkRedactions.Checked Then objGroup.GroupUpdatePermissionsById(intNewGroupId, SecType.Redaction, lblDocumentId.Text, 1)

                Next

                'Attribute Level Rights
                If rdoAdvancedPerms.Checked Then
                    For Each dgRowItem As DataGridItem In dgAttributes.Items
                        Dim lblAttributeId As Label = dgRowItem.FindControl("lblAttributeId")
                        Dim lblDocumentId As Label = dgRowItem.FindControl("lblDocumentId")
                        Dim ddlAccess As DropDownList = dgRowItem.FindControl("ddlAccess")
                        objGroup.GroupUpdatePermissionsById(intGroupId, SecType.Access, lblDocumentId.Text, ddlAccess.SelectedValue, lblAttributeId.Text)
                    Next
                End If

                'Lookup Level Rights
                For Each dgRowItem As DataGridItem In dgLookups.Items
                    Dim lblLookupAttributeId As Label = dgRowItem.FindControl("lblLookupAttributeId")
                    Dim lblDocumentId As Label = dgRowItem.FindControl("lblDocumentId")
                    Dim ddlLookup As DropDownList = dgRowItem.FindControl("ddlLookup")
                    objGroup.GroupUpdatePermissionsById(intGroupId, SecType.Lookups, lblLookupAttributeId.Text, ddlLookup.SelectedValue)
                Next

                'Collect users
                Dim dt As New DataTable
                dt.Columns.Add("UserID")
                dt.Columns.Add("GroupID")
                For Each lstItem As ListItem In lstAssigned.Items
                    Dim dr As DataRow
                    dr = dt.NewRow
                    dr("UserID") = lstItem.Value
                    dr("GroupID") = intNewGroupId
                    dt.Rows.Add(dr)
                Next

                Dim tempTableName As String = "##" & System.Guid.NewGuid.ToString.Replace("-", "")
                Dim sqlCmd As New SqlCommand("CREATE TABLE " & tempTableName & " (UserID int, GroupID int)", sqlConImages, sqlTran)
                sqlCmd.ExecuteNonQuery()

                sqlCmd = New SqlCommand("SELECT * FROM " & tempTableName, sqlConImages, sqlTran)
                Dim sqlDa As New SqlDataAdapter(sqlCmd)
                Dim sqlBld As New SqlCommandBuilder(sqlDa)

                sqlDa.Update(dt)

                sqlTran.Commit()

                'Update users
                objGroup.UserGroupMemberInsert(tempTableName, objGroup.GROUP_EDIT, intGroupId)

            End If

            If Request.QueryString("Ref") <> "Explorer" Then
                System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, "Close", "opener.location.reload();window.close();", True)
            Else
                System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, "closeScript", "window.close();", True)
            End If

        Catch ex As Exception

            sqlTran.Rollback()

            Dim conMaster As SqlClient.SqlConnection
            conMaster = Functions.BuildMasterConnection
            Functions.LogException(mobjUser.UserId.ToString, mobjUser.AccountId.ToString, "", "User group change edit: " & ex.Message, ex.StackTrace, Server.MachineName, "", Request.UrlReferrer.ToString, conMaster, True)

        Finally

            If sqlConImages.State <> ConnectionState.Closed Then
                sqlConImages.Close()
                sqlConImages.Dispose()
            End If

            If cTmp.State <> ConnectionState.Closed Then
                cTmp.Close()
                cTmp.Dispose()
            End If

        End Try

    End Sub

    Private Sub btnAssignAll_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAssignAll.Click

        Try
            For Each lstItem As ListItem In lstAvailable.Items
                Dim NewItem As New ListItem(lstItem.Text, lstItem.Value)
                lstAssigned.Items.Add(NewItem)
            Next

            lstAvailable.Items.Clear()

        Catch ex As Exception

        Finally

        End Try

    End Sub

    Private Sub dgGroupPermissions_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgGroupPermissions.ItemDataBound

        If Not Page.IsPostBack Then

            Try
                Dim rdoNoAccess As RadioButton = CType(e.Item.FindControl("rdoNoAccess"), RadioButton)
                Dim rdoReadOnly As RadioButton = CType(e.Item.FindControl("rdoReadOnly"), RadioButton)
                Dim rdoChange As RadioButton = CType(e.Item.FindControl("rdoChange"), RadioButton)
                Dim rdoDelete As RadioButton = CType(e.Item.FindControl("rdoDelete"), RadioButton)
                Dim lblSecLevel As Label = CType(e.Item.FindControl("lblSecLevel"), Label)

                Select Case lblSecLevel.Text
                    Case 0
                        rdoNoAccess.Checked = True
                    Case 1
                        rdoReadOnly.Checked = True
                    Case 2
                        rdoChange.Checked = True
                    Case 3
                        rdoDelete.Checked = True
                    Case Else
                        rdoNoAccess.Checked = True
                End Select

            Catch ex As Exception

            End Try

        End If

    End Sub

    Private Sub btnRemoveAll_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnRemoveAll.Click

        Try
            For Each lstItem As ListItem In lstAssigned.Items
                Dim NewItem As New ListItem(lstItem.Text, lstItem.Value)
                lstAvailable.Items.Add(NewItem)
            Next

            lstAssigned.Items.Clear()

        Catch ex As Exception

        Finally

        End Try

    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAdd.Click

        Dim dtMove As New DataTable
        dtMove.Columns.Add(New DataColumn("UserId", GetType(System.Int32), Nothing, MappingType.Element))
        dtMove.Columns.Add(New DataColumn("UserName", GetType(System.String), Nothing, MappingType.Element))

        Dim dtKeep As New DataTable
        dtKeep.Columns.Add(New DataColumn("UserId", GetType(System.Int32), Nothing, MappingType.Element))
        dtKeep.Columns.Add(New DataColumn("UserName", GetType(System.String), Nothing, MappingType.Element))

        Try
            For X As Integer = 0 To lstAvailable.Items.Count - 1
                If lstAvailable.Items(X).Selected = True Then
                    Dim dr As DataRow
                    dr = dtMove.NewRow
                    dr("UserId") = lstAvailable.Items(X).Value
                    dr("UserName") = lstAvailable.Items(X).Text
                    dtMove.Rows.Add(dr)
                Else
                    Dim dr As DataRow
                    dr = dtKeep.NewRow
                    dr("UserId") = lstAvailable.Items(X).Value
                    dr("UserName") = lstAvailable.Items(X).Text
                    dtKeep.Rows.Add(dr)
                End If
            Next

            For Each lstItem As ListItem In lstAssigned.Items
                Dim dr As DataRow
                dr = dtMove.NewRow
                dr("UserId") = lstItem.Value
                dr("UserName") = lstItem.Text
                dtMove.Rows.Add(dr)
            Next

            lstAvailable.Items.Clear()
            lstAssigned.Items.Clear()

            lstAvailable.DataSource = dtKeep
            lstAvailable.DataValueField = "UserId"
            lstAvailable.DataTextField = "UserName"
            lstAvailable.DataBind()

            lstAssigned.DataSource = dtMove
            lstAssigned.DataValueField = "UserId"
            lstAssigned.DataTextField = "UserName"
            lstAssigned.DataBind()

            If lstAssigned.Items.Count > 0 Then
                lstAssigned.Items(0).Selected = True
            End If

            If lstAvailable.Items.Count > 0 Then
                lstAvailable.Items(0).Selected = True
            End If

        Catch ex As Exception

        Finally

            'ReturnControls.Add(lstAssigned)
            'ReturnControls.Add(lstAvailable)

        End Try

    End Sub

    Private Sub btnRemove_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnRemove.Click

        Dim dtMove As New DataTable
        dtMove.Columns.Add(New DataColumn("UserId", GetType(System.Int32), Nothing, MappingType.Element))
        dtMove.Columns.Add(New DataColumn("UserName", GetType(System.String), Nothing, MappingType.Element))

        Dim dtKeep As New DataTable
        dtKeep.Columns.Add(New DataColumn("UserId", GetType(System.Int32), Nothing, MappingType.Element))
        dtKeep.Columns.Add(New DataColumn("UserName", GetType(System.String), Nothing, MappingType.Element))

        Try
            For X As Integer = 0 To lstAssigned.Items.Count - 1
                If lstAssigned.Items(X).Selected = True Then
                    Dim dr As DataRow
                    dr = dtMove.NewRow
                    dr("UserId") = lstAssigned.Items(X).Value
                    dr("UserName") = lstAssigned.Items(X).Text
                    dtMove.Rows.Add(dr)
                Else
                    Dim dr As DataRow
                    dr = dtKeep.NewRow
                    dr("UserId") = lstAssigned.Items(X).Value
                    dr("UserName") = lstAssigned.Items(X).Text
                    dtKeep.Rows.Add(dr)
                End If
            Next

            For Each lstItem As ListItem In lstAvailable.Items
                Dim dr As DataRow
                dr = dtMove.NewRow
                dr("UserId") = lstItem.Value
                dr("UserName") = lstItem.Text
                dtMove.Rows.Add(dr)
            Next

            lstAvailable.Items.Clear()
            lstAssigned.Items.Clear()

            lstAvailable.DataSource = dtMove
            lstAvailable.DataValueField = "UserId"
            lstAvailable.DataTextField = "UserName"
            lstAvailable.DataBind()

            lstAssigned.DataSource = dtKeep
            lstAssigned.DataValueField = "UserId"
            lstAssigned.DataTextField = "UserName"
            lstAssigned.DataBind()

            If lstAssigned.Items.Count > 0 Then
                lstAssigned.Items(0).Selected = True
            End If

            If lstAvailable.Items.Count > 0 Then
                lstAvailable.Items(0).Selected = True
            End If

        Catch ex As Exception

        Finally

            'ReturnControls.Add(lstAssigned)
            'ReturnControls.Add(lstAvailable)

        End Try

    End Sub

    Private Sub dgAttributes_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgAttributes.PreRender

        Dim sbNone As New System.Text.StringBuilder
        Dim sbReadOnly As New System.Text.StringBuilder
        Dim sbChange As New System.Text.StringBuilder
        Dim sbDelete As New System.Text.StringBuilder
        Dim sbEnable As New System.Text.StringBuilder
        Dim sdDisable As New System.Text.StringBuilder

        sbNone.Append("function attributeNoAccessAll() { if (allowMassAction) {")
        sbReadOnly.Append("function attributeReadOnlyAll() { if (allowMassAction) {")
        sbChange.Append("function attributeChangeAll() { if (allowMassAction) {")
        sbDelete.Append("function attributeDeleteAll() { if (allowMassAction) {")
        sbEnable.Append("function enable() {")
        sdDisable.Append("function disable() {")


        Dim strLastDocName As String = ""

        For Each dgRow As DataGridItem In dgAttributes.Items

            Dim ddlAccess As DropDownList = dgRow.FindControl("ddlAccess")

            If Not ddlAccess Is Nothing Then

                Dim lblDocName As Label = dgRow.FindControl("lblDocName")
                'Dim lblSlash As Label = dgRow.FindControl("lblSlash")
                Dim lblAttributeName As Label = dgRow.FindControl("lblAttributeName")

                If lblDocName.Text = strLastDocName Then
                    lblDocName.Text = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                    'lblSlash.Visible = False
                Else
                    strLastDocName = lblDocName.Text
                    lblDocName.Text = lblDocName.Text & "<BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                End If

                sbNone.Append("document.all." & ddlAccess.ClientID & ".selectedIndex = 0;")
                sbReadOnly.Append("document.all." & ddlAccess.ClientID & ".selectedIndex = 1;")
                sbChange.Append("document.all." & ddlAccess.ClientID & ".selectedIndex = 2;")
                sbDelete.Append("document.all." & ddlAccess.ClientID & ".selectedIndex = 3;")
                sbEnable.Append("document.all." & ddlAccess.ClientID & ".disabled = false;")
                sdDisable.Append("document.all." & ddlAccess.ClientID & ".disabled = true;")

            End If

        Next

        sbNone.Append("}}")
        sbReadOnly.Append("}}")
        sbChange.Append("}}")
        sbDelete.Append("}}")

        sbEnable.Append("}")
        sdDisable.Append("}")

        System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, "script", sbNone.ToString, True)
        System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, "script", sbReadOnly.ToString, True)
        System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, "script", sbChange.ToString, True)
        System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, "script", sbDelete.ToString, True)
        System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, "script", sbEnable.ToString, True)
        System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, "script", sdDisable.ToString, True)

    End Sub

    Private Sub dgLookups_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgLookups.PreRender

        Dim sbNone As New System.Text.StringBuilder
        Dim sbReadOnly As New System.Text.StringBuilder
        Dim sbChange As New System.Text.StringBuilder
        Dim sbDelete As New System.Text.StringBuilder

        sbNone.Append("function lookupNoAccessAll() {")
        sbReadOnly.Append("function lookupReadOnlyAll() {")
        sbChange.Append("function lookupChangeAll() {")
        sbDelete.Append("function lookupDeleteAll() {")

        Dim strLastDocName As String = ""

        For Each dgRow As DataGridItem In dgLookups.Items

            Dim ddlLookupAccess As DropDownList = dgRow.FindControl("ddlLookup")

            If Not ddlLookupAccess Is Nothing Then

                sbNone.Append("document.all." & ddlLookupAccess.ClientID & ".selectedIndex = 0;")
                sbReadOnly.Append("document.all." & ddlLookupAccess.ClientID & ".selectedIndex = 1;")
                sbChange.Append("document.all." & ddlLookupAccess.ClientID & ".selectedIndex = 2;")
                sbDelete.Append("document.all." & ddlLookupAccess.ClientID & ".selectedIndex = 3;")

            End If

        Next

        sbNone.Append("}")
        sbReadOnly.Append("}")
        sbChange.Append("}")
        sbDelete.Append("}")

        System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, "script", sbNone.ToString, True)
        System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, "script", sbReadOnly.ToString, True)
        System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, "script", sbChange.ToString, True)
        System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, "script", sbDelete.ToString, True)

    End Sub

    Private Sub rdoAdvancedPerms_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdoAdvancedPerms.CheckedChanged

        If rdoAdvancedPerms.Checked = False Then
            dgAttributes.Enabled = False
        Else
            dgAttributes.Enabled = True
        End If

        'ReturnControls.Add(dgAttributes)

    End Sub

End Class
Partial Class ManageAttributes
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
    End Sub

    Private Sub lnkAddAttribute_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkAddAttribute.Click
        Session("mAdminAttributesMode") = "ADD"
        Response.Redirect("ManageAtributesAdd.aspx")
    End Sub

    Private Sub lnkChangeAttribute_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkChangeAttribute.Click
        Session("mAdminAttributesMode") = "CHANGE"
        Response.Redirect("ManageAtributesChange.aspx")
    End Sub

    Private Sub lnkRemoveAttribute_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkRemoveAttribute.Click
        Session("mAdminAttributesMode") = "REMOVE"
        Response.Redirect("ManageAtributesChange.aspx")
    End Sub
End Class

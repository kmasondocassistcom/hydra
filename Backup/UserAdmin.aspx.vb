Imports Accucentric.docAssist

Public Class _UserAdmin
    Inherits System.Web.UI.Page

    Private mobjUser As Web.Security.User
    'Protected WithEvents lblErrorMessage As System.Web.UI.WebControls.Label
    'Protected WithEvents litScripts As System.Web.UI.WebControls.Literal
    'Protected WithEvents hostname As System.Web.UI.HtmlControls.HtmlInputHidden
    'Protected WithEvents dgUsers As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents Image1 As System.Web.UI.WebControls.Image
    'Protected WithEvents Image2 As System.Web.UI.WebControls.Image
    'Protected WithEvents IMG1 As System.Web.UI.HtmlControls.HtmlImage
    'Protected WithEvents imgAddUser As System.Web.UI.HtmlControls.HtmlImage
    Private mconSqlImage As SqlClient.SqlConnection

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Function BuildDataset() As DataSet

        Dim dsUsers As New DataSet
        Dim conSqlWeb As New SqlClient.SqlConnection(Functions.ConnectionString())
        '
        '' Image Users
        '     Dim cmdSql As New SqlClient.SqlCommand("SELECT [ImageUserId]=UserId, UserName, SecAdmin FROM Users", Me.mconSqlImage)
        'Dim da As New SqlClient.SqlDataAdapter(cmdSql)
        'If Not Me.mconSqlImage.State = ConnectionState.Open Then Me.mconSqlImage.Open()
        'da.Fill(dsUsers.ImageUsers)
        '
        ' Web Users
        Dim cmdSql As New SqlClient.SqlCommand
        Dim da As New SqlClient.SqlDataAdapter(cmdSql)
        cmdSql = New SqlClient.SqlCommand("acsUserListbyAccountID", mconSqlImage)
        cmdSql.CommandType = CommandType.StoredProcedure
        cmdSql.CommandTimeout = DEFAULT_COMMAND_TIMEOUT
        cmdSql.Parameters.Add("@AccountID", Me.mobjUser.AccountId)
        da = New SqlClient.SqlDataAdapter(cmdSql)
        If Not mconSqlImage.State = ConnectionState.Open Then mconSqlImage.Open()
        da.Fill(dsUsers)

        Me.mconSqlImage.Close()
        conSqlWeb.Close()

        Return dsUsers

    End Function

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try


            Functions.CheckTimeout(Me.Page)

            Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
            Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))

            Me.mobjUser.AccountId = guidAccountId
            Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)

            If Me.mobjUser.EditionID = AccountEdition.SMALL_BUSINESS_EDITION Then RegisterStartupScript("smbiz", "<script>smbiz = true;</script>")

        Catch ex As Exception
            If ex.Message = "DualLogin" Then
                Response.Cookies("docAssist")("DualLogin") = True
                Server.Transfer("Default.aspx")
            Else
                Response.Redirect("Default.aspx")
                Response.Flush()
                Response.End()
            End If
        End Try
        '
        ' Check user permissions to his this screen ....
        If Not Page.IsPostBack Then
            Try

                If Not (Me.mobjUser.SecAdmin = True Or Me.mobjUser.SysAdmin = True) Then
                    imgAddUser.Visible = False
                    Throw New Exception("User is not authorized to access this page.")
                End If

                Dim dsUsers As DataSet = BuildDataset()
                dgUsers.DataSource = dsUsers
                dgUsers.DataBind()

                If Me.mobjUser.EditionID = AccountEdition.SMALL_BUSINESS_EDITION And dgUsers.Items.Count = 3 Then

                    'Check for expired trial
                    Dim accountStatus As AccountState = Functions.GetAccountStatus(Me.mobjUser.AccountId.ToString)
                    Select Case accountStatus
                        Case Constants.AccountState.TRIAL_ACTIVE
                            imgAddUser.Attributes("onclick") = "window.location.href='" & ConfigurationSettings.AppSettings("UpgradeURL") & "?ref=addUsers&m=" & accountStatus & "&id=" & Me.mobjUser.AccountId.ToString & "';return false;"
                        Case Constants.AccountState.DISABLED
                            'Shouldn't ever be here.
                        Case Constants.AccountState.TRIAL_EXPIRED
                            'Shouldn't ever be here.
                    End Select

                End If

                'lblUserHeading.Text = Me.mobjUser.AccountDesc & " Account Administration" & vbCr
                hostname.Value = Request.Url.AbsoluteUri.Substring(0, Request.Url.AbsoluteUri.Length - Request.Url.AbsolutePath.Length)
#If DEBUG Then
                hostname.Value = hostname.Value & "/docAssistWeb1"
#End If


            Catch ex As Exception
#If DEBUG Then
                lblErrorMessage.Text = ex.Message & "<BR>" & ex.StackTrace
#Else
                lblErrorMessage.Text = ex.Message
#End If
                lblErrorMessage.Visible = True
            Finally
                If Not Me.mconSqlImage.State = ConnectionState.Closed Then Me.mconSqlImage.Close()
            End Try
        End If

    End Sub

    Private Sub dgUsers_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgUsers.SelectedIndexChanged

        Dim sb As New System.Text.StringBuilder
        sb.Append("<SCRIPT id=""Opener"" language=""javascript"">")
        sb.Append("window.open('UserEdit.aspx','UserEdit','height=512,resizable=no,scrollbars=yes,status=yes,toolbar=no,width=520');")
        sb.Append("</SCRIPT>")

        Response.Cookies("docAssistWeb")("EditUserId") = CType(dgUsers.Items(dgUsers.SelectedIndex).FindControl("lblImageUserId"), Label).Text
        dgUsers.SelectedIndex = -1
        litScripts.Text = sb.ToString

    End Sub

    Private Sub dgUsers_DeleteCommand1(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgUsers.DeleteCommand

        Dim conSqlWeb As New SqlClient.SqlConnection(Functions.ConnectionString())
        Dim dgRowItem As DataGridItem = e.Item
        Dim guidUserId As New System.Guid(CType(dgRowItem.FindControl("lblWebUserId"), Label).Text)
        Dim intUserId As Integer = CType(dgRowItem.FindControl("lblImageUserId"), Label).Text
        Try
            '
            ' Check if user is ok to remove
            'Dim cmdSql As New SqlClient.SqlCommand("SELECT 1 FROM users WHERE UserId = @UserId AND (SecAdmin <> 1 OR (SELECT count(*) FROM users WHERE SecAdmin = 1) > 1)", Me.mconSqlImage)
            'cmdSql.CommandTimeout = DEFAULT_COMMAND_TIMEOUT
            'cmdSql.Parameters.Add("@UserId", intUserId)

            'If Not Me.mconSqlImage.State = ConnectionState.Open Then Me.mconSqlImage.Open()
            'Dim bolOk As Boolean = cmdSql.ExecuteScalar()

            'If bolOk Then
            '
            ' Remove the SecAdmin priviledge
            'cmdSql = New SqlClient.SqlCommand("UPDATE Users SET SecAdmin = 0 WHERE UserId = @UserId", Me.mconSqlImage)
            'cmdSql.CommandTimeout = DEFAULT_COMMAND_TIMEOUT
            'cmdSql.Parameters.Add("@UserId", intUserId)
            'cmdSql.ExecuteNonQuery()
            '
            ' Remove the User record in images.
            'cmdSql = New SqlClient.SqlCommand("DELETE FROM Users WHERE UserId = @UserId", Me.mconSqlImage)
            'cmdSql.CommandTimeout = DEFAULT_COMMAND_TIMEOUT
            'cmdSql.Parameters.Add("@UserId", intUserId)
            'cmdSql.ExecuteNonQuery()
            '
            ' Remove the user from the account
            Dim cmdSql As New SqlClient.SqlCommand
            cmdSql = New SqlClient.SqlCommand("DELETE FROM UserAccounts WHERE UserId = @UserId AND AccountId = @AccountId", conSqlWeb)
            cmdSql.CommandTimeout = DEFAULT_COMMAND_TIMEOUT
            cmdSql.Parameters.Add("@UserId", guidUserId)
            cmdSql.Parameters.Add("@AccountId", Me.mobjUser.AccountId)
            conSqlWeb.Open()
            cmdSql.ExecuteNonQuery()
            '
            ' Log the removal
            LogUserAccounts(guidUserId, Me.mobjUser.AccountId, Me.mobjUser.UserId, "Remove", _
                conSqlWeb)
            ' End If

            ' Dim dsUsers As dsAccountUsers = BuildDataset()
            Dim dsUsers As DataSet = BuildDataset()
            dgUsers.DataSource = dsUsers.Tables(0)
            dgUsers.DataBind()

        Catch ex As Exception
#If DEBUG Then
            lblErrorMessage.Text = ex.Message & "<BR>" & ex.StackTrace
#Else
                lblErrorMessage.Text = ex.Message
#End If
            lblErrorMessage.Visible = True
        Finally
            If Not Me.mconSqlImage.State = ConnectionState.Closed Then Me.mconSqlImage.Close()
            If Not conSqlWeb.State = ConnectionState.Closed Then conSqlWeb.Close()
        End Try

    End Sub

    Private Sub dgUsers_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgUsers.ItemDataBound

        Try
            Dim lnkDeleteCategory As LinkButton = CType(e.Item.FindControl("lnkRemove"), LinkButton)
            If Not lnkDeleteCategory Is Nothing Then lnkDeleteCategory.Attributes("onclick") = "return confirm('Are you sure you want to delete \'" & e.Item.Cells(1).Text.Replace("'", "\'") & "\'?');"

            If Me.mobjUser.EditionID = AccountEdition.SMALL_BUSINESS_EDITION Then
                Dim lnkCopyUser As HyperLink = CType(e.Item.FindControl("lnkCopyUser"), HyperLink)
                lnkCopyUser.Visible = False
            End If

            Dim lblImageUserId As Label = CType(e.Item.FindControl("lblImageUserId"), Label)

            'Block user from removing themselves
            If Me.mobjUser.ImagesUserId = lblImageUserId.Text Then lnkDeleteCategory.Visible = False

            'If Not lblImageUserId Is Nothing Then
            '    e.Item.Cells(1).Attributes("onclick") = "SetUser(" & lblImageUserId.Text & ");"
            '    e.Item.Cells(2).Attributes("onclick") = "SetUser(" & lblImageUserId.Text & ");"
            '    e.Item.Cells(1).Style("CURSOR") = "hand"
            '    e.Item.Cells(2).Style("CURSOR") = "hand"
            'End If

        Catch ex As Exception

        End Try

    End Sub

    Private Sub dgUsers_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgUsers.PreRender

        If Me.mobjUser.EditionID = AccountEdition.SMALL_BUSINESS_EDITION Then
            dgUsers.Columns(3).Visible = False
            dgUsers.Columns(5).Visible = False
            dgUsers.Columns(6).Visible = False
        End If

    End Sub
End Class

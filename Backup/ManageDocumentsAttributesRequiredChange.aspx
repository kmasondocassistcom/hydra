<%@ Register TagPrefix="uc1" TagName="AdminNav" Src="Controls/AdminNav.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ManageDocumentsAttributesRequiredChange.aspx.vb" Inherits="docAssistWeb.ManageDocumentsAttributesRequiredChange"%>
<%@ Register TagPrefix="uc1" TagName="TopRight" Src="Controls/TopRight.ascx" %>
<%@ Register TagPrefix="uc1" TagName="docHeader" Src="Controls/docHeader.ascx" %>
<%@ Register TagPrefix="mbclb" Namespace="MetaBuilders.WebControls" Assembly="MetaBuilders.WebControls.CheckedListBox" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>docAssist - Change Document</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0" MS_POSITIONING="GridLayout">
		<FORM id="Form1" method="post" runat="server">
			<TABLE id="Table1" style="Z-INDEX: 101; LEFT: 8px; POSITION: absolute; TOP: 8px" height="100%"
				cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TBODY>
					<TR>
						<TD width="100%" colSpan="5"><uc1:docheader id="docHeader" runat="server"></uc1:docheader></TD>
					</TR>
					<TR vAlign="middle" height="100%">
						<TD class="Navigation1" style="BORDER-RIGHT: #cccccc thin solid" vAlign="top" height="100%"><uc1:adminnav id="AdminNav1" runat="server"></uc1:adminnav><IMG height="1" src="dummy.gif" width="120" DESIGNTIMEDRAGDROP="101"></TD>
						<TD vAlign="top" align="center" height="100%"><BR>
							<TABLE id="Table2" cellSpacing="0" cellPadding="0" width="100%" border="0">
								<TR>
									<TD><IMG height="1" src="dummy.gif" width="35" height1><IMG src="Images/manage_documents.gif"></TD>
								</TR>
							</TABLE>
							<TABLE id="Table3" height="100%" cellSpacing="0" cellPadding="0" width="100%">
					</TR>
					<TR height="50%">
					</TR>
					<TR height="15" width="100%">
						<TD style="WIDTH: 336px"></TD>
						<TD></TD>
						<TD></TD>
						<TD></TD>
					</TR>
					<TR height="15" width="100%">
						<TD style="WIDTH: 336px"></TD>
						<TD style="WIDTH: 72px" align="left"><asp:label id="Label6" runat="server" Width="105px" Height="5px" Font-Names="Verdana" Font-Size="8pt">Document Name:</asp:label></TD>
						<TD><asp:textbox id="txtDocumentName" runat="server" Width="288px"></asp:textbox></TD>
						<TD width="50%"></TD>
					<TR height="15" width="100%">
						<TD style="HEIGHT: 21px" width="50%"></TD>
						<TD style="WIDTH: 72px; HEIGHT: 21px" align="left">&nbsp;
						</TD>
						<TD style="HEIGHT: 21px">
							<P><BR>
								<asp:label id="Label4" runat="server" Width="280px" Height="8px" Font-Names="Verdana" Font-Size="8pt">- Specify Required Attributes</asp:label><BR>
								<mbclb:checkedlistbox id="lstAttributes" runat="server" Width="440px" Height="150px"></mbclb:checkedlistbox><BR>
								<BR>
								<span style="DISPLAY: none">
								
								<asp:label id="Label1" runat="server" Width="280px" Height="8px" Font-Names="Verdana" Font-Size="8pt">Specify the list attributes that should validate:</asp:label><BR>
								<mbclb:checkedlistbox id="lstValidate" runat="server" Width="440px" Height="150px" CssClass="SearchGrid"></mbclb:checkedlistbox></P>
							<P>
								<asp:label id="Label7" runat="server" Font-Size="8pt" Font-Names="Verdana" Height="8px" Width="280px">Specify the attributes that filter by cabinet:</asp:label>
								<mbclb:checkedlistbox id="lstCabFilter" runat="server" Height="150px" Width="440px" CssClass="SearchGrid"></mbclb:checkedlistbox></P></span>
						</TD>
						<TD style="HEIGHT: 21px" width="50%"></TD>
					</TR>
					<TR height="15" width="100%">
						<TD style="WIDTH: 336px" width="336"></TD>
						<TD style="WIDTH: 72px; HEIGHT: 17px">
							<P align="left">&nbsp;&nbsp;
							</P>
						</TD>
						<TD style="HEIGHT: 17px"><asp:label id="lblError" runat="server" Width="100%" Height="3px" Font-Names="Verdana" Font-Size="8pt"
								ForeColor="Red"></asp:label></TD>
						<TD width="50%"></TD>
					</TR>
					<TR height="15" width="100%">
						<TD style="WIDTH: 336px" width="336"></TD>
						<TD style="WIDTH: 72px" vAlign="middle">
							<P align="left">&nbsp;&nbsp;</P>
						</TD>
						<TD align="right"><asp:imagebutton id="Imagebutton4" runat="server" ImageUrl="Images/btn_canceladmin.gif"></asp:imagebutton>&nbsp;
							<asp:imagebutton id="Imagebutton3" runat="server" ImageUrl="Images/btn_finish.gif"></asp:imagebutton></TD>
						<TD width="50%"></TD>
					</TR>
					<TR height="15" width="100%">
						<TD style="WIDTH: 336px" width="336"></TD>
						<TD style="WIDTH: 72px; HEIGHT: 17px"><BR>
						</TD>
						<TD style="HEIGHT: 17px"></TD>
						<TD width="50%"></TD>
					</TR>
					<TR height="15" width="100%">
						<TD style="WIDTH: 336px" width="336"></TD>
						<TD style="WIDTH: 72px"><BR>
							<P></P>
						</TD>
						<TD align="right"></TD>
						<TD width="50"></TD>
					</TR>
					<TR height="100%">
						<TD style="WIDTH: 336px"></TD>
					</TR>
				</TR></TABLE>
			<P><BR>
				<BR>
			</P>
			</TD>
			<TD class="RightContent" height="100%">&nbsp;</TD>
			</TR>
			<TR>
				<TD class="PageFooter" vAlign="bottom" bgColor="#cccccc" colSpan="3"><uc1:topright id="docFooter" runat="server"></uc1:topright></TD>
			</TR>
			<TR height="1">
				<TD style="BACKGROUND-COLOR: #959394" width="20"><IMG height="1" src="images/spacer.gif" width="20"></TD>
				<TD style="BACKGROUND-COLOR: #959394" width="100%"><IMG height="1" src="images/spacer.gif" width="100%"></TD>
				<TD style="BACKGROUND-COLOR: #959394" width="20"><IMG height="1" src="images/spacer.gif" width="20"></TD>
			</TR>
			</TBODY></TABLE></FORM>
	</body>
</HTML>

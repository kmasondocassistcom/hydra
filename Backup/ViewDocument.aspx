<%@ Register TagPrefix="cc1" Namespace="Atalasoft.Imaging.WebControls.Annotations" Assembly="Atalasoft.dotImage.WebControls.Annotations" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ViewDocument.aspx.vb" Inherits="docAssistWeb.ViewDocument.ViewDocument" EnableSessionState="True" enableViewState="False"%>
<%@ OutputCache Location="None" VaryByParam="None" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>docAssist: Document Viewer</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="css/viewer.css" type="text/css" rel="stylesheet">
		<LINK href="css/jquery-ui.css" type="text/css" rel="stylesheet"> <!--[if IE]><LINK href="css/viewer-ie.css" 
type=text/css rel=stylesheet><![endif]-->
		<script src="scripts/jquery.js"></script>
		<script src="scripts/jquery-ui.js"></script>
		<script src="scripts/jquery.rotate.1-1.js"></script>
		<script src="scripts/jquery.numeric.pack.js"></script>
		<script src="scripts/jquery.maskedinput.js"></script>
		<script src="scripts/viewer.js"></script>
		<script src="scripts/jquery.contextMenu.js"></script>
	</HEAD>
	<body>
		<form id="frmViewDocument" method="post" runat="server">
			<div id="container">
				<table width="100%">
					<tr>
						<td nowrap id="topControls"><IMG class="hidden" id="fullText" alt="Document Text" src="images/transparent.gif">
							<IMG class="sprite btn-history" id="history" alt="Document History" src="images/transparent.gif">
							<IMG class="sprite btn-notes" id="bnotes" alt="Notes" src="images/transparent.gif">
							<IMG class="sprite btn-previous-result-off" id="previousResult" alt="Previous Result"
								src="images/transparent.gif" runat="server"> <IMG class="sprite btn-next-result-off" id="nextResult" alt="Next Result" src="images/transparent.gif"
								runat="server"> <IMG class="sprite btn-first-off" id="first" alt="First Page" src="images/transparent.gif">
							<IMG class="sprite btn-previous-off" id="previous" alt="Previous Page" src="images/transparent.gif">
							<IMG class="sprite btn-next-off" id="next" alt="Next Page" src="images/transparent.gif">
							<IMG class="sprite btn-last-off" id="last" alt="Last Page" src="images/transparent.gif">
							<IMG class="sprite btn-zoom-in-off" id="zoomIn" alt="Zoom In" src="images/transparent.gif">
							<IMG class="sprite btn-zoom-out-off" id="zoomOut" alt="Zoom Out" src="images/transparent.gif">
							<IMG class="sprite btn-zoom-section-off" id="zoomSection" alt="Zoom Section" src="images/transparent.gif">
							<IMG class="sprite btn-hand-off" id="pan" alt="Pan" src="images/transparent.gif">
							<IMG class="sprite btn-fitbest-off" id="fitBest" alt="Fit best" src="images/transparent.gif">
							<IMG class="sprite btn-fitwidth-off" id="fitWidth" alt="Fit Width" src="images/transparent.gif">
							<IMG class="sprite btn-fitheight-off" id="fitHeight" alt="Fit Height" src="images/transparent.gif">
							<IMG class="sprite btn-print" id="print" alt="Print" src="images/transparent.gif" runat="server">
							<IMG class="sprite btn-email" id="email" alt="E-mail Document" src="images/transparent.gif"
								runat="server">
						</td>
						<td nowrap id="pagesSection"><div id="pageInfo">Page <input type="text" id="tCurrentPage">
								of
							</div>
							<div id="pages"></div>
						</td>
						<td nowrap id="versionSection">
							<div id="versionLabel">Version:
							</div>
							<select id="versions" runat="server">
							</select>
						</td>
						<td nowrap id="qualitySection">
							<div id="qualityLabel">Quality:
							</div>
							<select id="dQuality" runat="server">
							</select>
						</td>
						<td id="save" width="100%">
							<IMG class="sprite btn-save_small" id="savedoc" alt="Save Changes" src="images/transparent.gif"
								onclick="save();" runat="server"></td>
					</tr>
				</table>
				<img id="showThumbs" src="./images/bar_vert_thumbnails.gif" alt="Show Thumbnails" style="display:none">
				<table id="bottomContainer">
					<tr>
						<td id="thumbnails">
							<div id="thumbHeader">Document Thumbnails<img id=pinThumbs src="http://dummyimage.com/14x14"></div>
							<div id="thumbs" runat="server"></div>
						</td>
						<td id="viewerContainer"><cc1:webannotationviewer id="viewer" runat="server" Height="100%" Width="100%" TileSize="1024, 1024"></cc1:webannotationviewer></td>
						<td class="hidden" id="fileViewer">File Viewer</td>
						<td id="sideToolbar"><IMG class="sprite btn-rotate-left" id="rotateLeft" alt="Rotate Left" src="images/transparent.gif"
								runat="server"> <IMG class="sprite btn-rotate-right" id="rotateRight" alt="Rotate Right" src="images/transparent.gif"
								runat="server"> <IMG class="sprite btn-flip" id="flip" alt="Flip" src="images/transparent.gif" runat="server">
							<IMG class="sprite btn-move-pages" id="move" alt="Move Page(s)" src="images/transparent.gif"
								runat="server"> <IMG class="sprite btn-delete-page" id="delete" alt="Delete Page(s)" src="images/transparent.gif"
								runat="server"> <IMG class="sprite btn-split" id="split" alt="Split Document" src="images/transparent.gif"
								runat="server"> <IMG class="sprite btn-sticky" id="sticky" alt="Sticky Note" src="images/transparent.gif"
								runat="server"> <IMG class="sprite btn-pencil" id="pencil" alt="Drawing Tool" src="images/transparent.gif"
								runat="server"> <IMG class="sprite btn-text" id="txttool" alt="Text Tool" src="images/transparent.gif"
								runat="server"> <IMG class="sprite btn-highlighter" id="highlighter" alt="Highlighter" src="images/transparent.gif"
								runat="server">
						</td>
						<td id="attributes">
							<table width="100%" height="100%">
								<tr height="100%">
									<td valign="top" id="at">
										<div id="attributeContainer" runat="server">
											<div id="attributesHeader"></div>
											<div id="attributeContent">
												<div class="docNo" id="docNo" runat="server"></div>
												<label>Folder</label> <input id="txtFolder" readOnly type="text" size="22" runat="server">&nbsp;<IMG id="ChooseFolder" style="CURSOR: hand" onclick="folderLookup();" height="19" alt="Choose Folder"
													src="Images/addToFolder_btn.gif"><IMG height="1" src="spacer.gif" width="5"><IMG id="ClearFolder" style="CURSOR: hand" onclick="clearFolder();" height="20" alt="Clear Folder"
													src="Images/clear.gif" runat="server"> <label>Document Details </label><input id="txtTitle" type="text" runat="server" title="Title">
												<input id="txtDescription" type="text" runat="server" title="Description"> <input id="txtTags" type="text" runat="server" title="Tags (separate with spaces)">
												<label>Document Type</label><select id="ddlDocuments" runat="server"></select>
												<div id="attributeToolbar">
													<A id="imgAdd" href="#"><IMG alt="Add Attribute" src="Images/bot23.gif" border="0"></A>
													<A id="imgDelete" onclick="remove();" href="#"><IMG alt="Remove Selected Attribute" src="Images/circle_minus.gif" border="0"></A>
													<a id="imgCopy" onclick="copy();">Copy</a>
													<a id="imgPaste" onclick="paste();">Paste</a>
													<A id="imgCapture" onclick="integrationCapture();" href="#"><IMG alt="Capture Attributes" src="Images/capture.gif" border="0"></A>
												</div>
												<div id="attributeGrid" style="overflow:auto">
													<table id="grid">
														<DIV>Loading attributes...</DIV>
													</table>
												</div>

											</div>
										</div>
									</td>
								</tr>
								<tr height="200">
									<td align="left" id="wf">
										<div id="workflowContainer" runat="server">
											<div id="workflowHeader">
												<img src="Images/viewer/wf-console.gif"></div>
											<div id="workflowContent" runat="server">
												<div id="workflowSection" runat="server"><label>Workflow:</label>
													<select id="workflow" name="queue" runat="server">
													</select>
												</div>
												<div id="actionSection" runat="server"><label>Action:</label>
													<select id="action" runat="server" NAME="action">
													</select></div>
												<div></div>
												<div id="queueSection"><label>Queue:</label>
													<select id="queue" name="queue" runat="server">
													</select>
												</div>
												<div id="urgentSection">
													<input id="urgent" type="checkbox" runat="server" NAME="urgent">Urgent
												</div>
												<div><textarea id="notes" runat="server" title="Notes" NAME="notes"></textarea>
												</div>
											</div>
											<div id="wfHistory" runat="server" class="hidden"></div>
											<div id="wfTabs" runat="server">
												<div id="wfRadios">
													<input type="radio" id="rReview" name="radio" checked="checked"><label for="rReview">Review</label>
													<input type="radio" id="rHistory" name="radio"><label for="rHistory">History</label>
												</div>
											</div>
										</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
			<ul id="AnnoContextMenu" class="contextMenu">
				<li class="deleteAnnotation" onclick="DeleteAnno();" onmouseout="CloseContextMenu();"><a href="#delete">Delete</a></li>
			</ul>
			<div id="dialog-confirm" title="Save changes?" class="hidden">
				<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>You have made changes to this document that have not been saved. Do you want to save your changes?</p>
			</div>			
			<input id="FolderId" type="hidden" runat="server"><input id="clientVersionId" type="hidden" runat="server">
		</form>
	</body>
</HTML>

<%@ Register TagPrefix="uc1" TagName="TopRight" Src="Controls/TopRight.ascx" %>
<%@ Register TagPrefix="uc1" TagName="AdminNav" Src="Controls/AdminNav.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ManageAtributesAdd.aspx.vb" Inherits="docAssistWeb.ManageAtributesAdd"%>
<%@ Register TagPrefix="uc1" TagName="docHeader" Src="Controls/docHeader.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>docAssist - Add Attribute</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<TABLE height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TBODY>
					<TR>
						<TD width="100%" colSpan="5"><uc1:docheader id="docHeader" runat="server"></uc1:docheader></TD>
					</TR>
					<TR vAlign="middle" height="100%">
						<TD class="Navigation1" style="BORDER-RIGHT: #cccccc thin solid" vAlign="top" height="100%"><uc1:adminnav id="AdminNav1" runat="server"></uc1:adminnav><IMG height="1" src="dummy.gif" width="120" DESIGNTIMEDRAGDROP="101">
						</TD>
						<TD vAlign="top" align="center" height="100%"><BR>
							<table cellSpacing="0" cellPadding="0" width="100%" border="0">
								<tr>
									<td><IMG height="1" src="dummy.gif" width="35" height1><IMG src="Images/manage_attributes.gif"></td>
								</tr>
							</table>
							<table height="100%" cellSpacing="0" cellPadding="0" width="100%">
					</TR>
					<tr height="50%">
					</tr>
					<tr height="15" width="100%">
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr height="15" width="100%">
						<td></td>
						<td style="WIDTH: 72px" align="left"><asp:label id="Label1" runat="server" Font-Size="8pt" Font-Names="Verdana" Height="5px" Width="96px">Attribute Name:</asp:label></td>
						<TD><asp:textbox id="txtAttributeName" runat="server" Font-Size="8pt" Font-Names="Verdana" Height="18px"
								Width="270px"></asp:textbox></TD>
						<TD width="50%"></TD>
					<TR height="15" width="100%">
						<td style="HEIGHT: 21px" width="50%"></td>
						<td style="WIDTH: 72px; HEIGHT: 21px" align="left" vAlign="top"><asp:label id="Label6" runat="server" Font-Size="8pt" Font-Names="Verdana" Height="3px" Width="64px">Data Type:</asp:label>&nbsp;
						</td>
						<td style="HEIGHT: 21px"><asp:dropdownlist id="cmbDataType" runat="server" Font-Size="8pt" Font-Names="Verdana" Height="24px"
								Width="270px" AutoPostBack="True"></asp:dropdownlist><BR>
							<asp:RadioButton id="rdoSingle" runat="server" Text="Single Column" Visible="False" GroupName="ListType"
								Checked="True"></asp:RadioButton>
							<asp:RadioButton id="rdoMultiple" runat="server" Text="Multi Column" Visible="False" GroupName="ListType"></asp:RadioButton></td>
						<td style="HEIGHT: 21px" width="50%"></td>
					</TR>
					<tr height="15" width="100%" style="display:none">
						<td style="WIDTH: 393px" width="393"></td>
						<td style="WIDTH: 72px; HEIGHT: 17px">
							<P align="left"><asp:label id="Label3" runat="server" Font-Size="8pt" Font-Names="Verdana" Height="3px" Width="36px">Length:</asp:label>&nbsp;&nbsp;
							</P>
						</td>
						<td style="HEIGHT: 17px"><asp:textbox id="txtLength" runat="server" Font-Size="8pt" Font-Names="Verdana" Height="18px"
								Width="270px">75</asp:textbox></td>
						<td width="50%"></td>
					</tr>
					<tr height="15" width="100%" style="DISPLAY: none">
						<td style="WIDTH: 393px; HEIGHT: 21px" width="393"></td>
						<td style="WIDTH: 72px; HEIGHT: 21px" vAlign="middle">
							<P align="left"><asp:label id="Label4" runat="server" Font-Size="8pt" Font-Names="Verdana" Height="3px" Width="14px">Format:</asp:label>&nbsp;&nbsp;</P>
						</td>
						<td style="HEIGHT: 21px"><asp:textbox id="txtFormat" runat="server" Font-Size="8pt" Font-Names="Verdana" Width="270px"></asp:textbox></td>
						<td style="HEIGHT: 21px" width="50%"></td>
					</tr>
					<tr height="15" width="100%">
						<td width="395"></td>
						<td>
						</td>
						<td vAlign="top"><asp:label id="lblError" runat="server" Font-Size="8pt" Font-Names="Verdana" Height="10px"
								Width="368px" ForeColor="Red"></asp:label></td>
						<td width="50%"></td>
					</tr>
					<tr height="15" width="100%">
						<td style="WIDTH: 395px" width="395"></td>
						<td style="WIDTH: 72px"><BR>
							<P></P>
						</td>
						<td align="right">
							<asp:imagebutton id="btnCancel" runat="server" ImageUrl="Images/btn_canceladmin.gif"></asp:imagebutton>&nbsp;
							<asp:imagebutton id="btnFinish" runat="server" ImageUrl="Images/btn_finish.gif"></asp:imagebutton></td>
						<td width="50"></td>
					</tr>
					<tr height="100%">
						<td></td>
					</tr>
				</tr></TABLE>
			<P><BR>
				<BR>
			</P>
			</TD>
			<TD class="RightContent" height="100%">&nbsp;</TD>
			</TR>
			<TR>
				<TD class="PageFooter" vAlign="bottom" bgColor="#cccccc" colSpan="3"><uc1:topright id="docFooter" runat="server"></uc1:topright></TD>
			</TR>
			<TR height="1">
				<TD style="BACKGROUND-COLOR: #959394" width="20"><IMG height="1" src="images/spacer.gif" width="20"></TD>
				<TD style="BACKGROUND-COLOR: #959394" width="100%"><IMG height="1" src="images/spacer.gif" width="100%"></TD>
				<TD style="BACKGROUND-COLOR: #959394" width="20"><IMG height="1" src="images/spacer.gif" width="20"></TD>
			</TR>
			</TBODY></TABLE></form>
		<script language="javascript">
		function nb_numericOnly(evt)
			{
				evt = (evt) ? evt : event;
				var charCode = (evt.charCode) ? evt.charCode : ((evt.keyCode) ? evt.keyCode
				: ((evt.which) ? evt.which : 0));
				if (charCode > 31 && (charCode < 48 || charCode >57)) {return false;}
				return true;
			}
			
			function catchSubmit()
			{
				if (event.keyCode == 13)
				{
					event.cancelBubble = true;
					event.returnValue = false;
					//document.all.lnkSearch.click();
					return false;
				}
			}	
		</script>
	</body>
</HTML>

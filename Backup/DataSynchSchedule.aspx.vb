Imports Accucentric.docAssist.Data.Images

Partial Class DataSynchSchedule
    Inherits System.Web.UI.Page

    Dim mobjUser As Accucentric.docAssist.Web.Security.User
    Dim mconDataSync As New SqlClient.SqlConnection


#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents btnSave As System.Web.UI.WebControls.ImageButton

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try
            Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
        Catch ex As Exception
            Response.Redirect("Default.aspx")
            Exit Sub
        End Try

        rdoNow.Attributes.Add("onclick", "disableWeekly();")
        rdoDaily.Attributes.Add("onclick", "disableWeekly();")
        rdoWeekly.Attributes.Add("onclick", "enableWeekly();")

        Try

            If Not Page.IsPostBack Then

                Me.mconDataSync = Functions.BuildConnection(Me.mobjUser)
                Dim Sync As New DataSync(Me.mconDataSync)

                Dim dtJobs As New DataTable
                dtJobs = Sync.DIJobListGet()

                ddlJobs.DataSource = dtJobs
                ddlJobs.DataTextField = "JobName"
                ddlJobs.DataValueField = "Val"
                ddlJobs.DataBind()

            End If

        Catch ex As Exception

        Finally

            'ReturnControls.Add(ddlJobs)

            If Me.mconDataSync.State <> ConnectionState.Closed Then
                Me.mconDataSync.Close()
                Me.mconDataSync.Dispose()
            End If

        End Try

    End Sub

    Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAdd.Click

        Me.mconDataSync = Functions.BuildConnection(Functions.ConnectionType.Notifications_DataSynch)
        Dim Sync As New DataSync(Me.mconDataSync)

        Dim intUtcTime As Integer
        Dim strHex As String = ""
        Dim strScheduleText As String = ""

        Try

            Dim JobId As Integer = ddlJobs.SelectedValue.Split("-")(0)
            Dim JobDetailId As Integer = ddlJobs.SelectedValue.Split("-")(1)

            If rdoWeekly.Checked Then

                Dim blnDaySelected As Boolean = False

                If chkMonday.Checked Then blnDaySelected = True
                If chkTuesday.Checked Then blnDaySelected = True
                If chkWednesday.Checked Then blnDaySelected = True
                If chkThursday.Checked Then blnDaySelected = True
                If chkFriday.Checked Then blnDaySelected = True
                If chkSaturday.Checked Then blnDaySelected = True
                If chkSunday.Checked Then blnDaySelected = True

                If blnDaySelected = False Then
                    lblError.Text = "Please choose at least one day of the week to add a weekly alert."
                    lblError.Visible = True
                    'ReturnControls.Add(lblError)
                    Exit Sub
                End If

            End If

            'Schedule Type
            Dim intScheduleType As Integer
            If rdoNow.Checked Then
                intScheduleType = 0
            ElseIf rdoDaily.Checked Then
                intScheduleType = 1
            ElseIf rdoWeekly.Checked Then
                intScheduleType = 2
            End If

            'Schedule Text

            If Not rdoNow.Checked Then
                Dim sb As New System.Text.StringBuilder
                sb.Append(Format(Now, "yyyyMMdd"))
                sb.Append("T")

                Dim time As DateTime

                If rdoDaily.Checked Then
                    time = CDate(txtDaily.SelectedTime)
                ElseIf rdoWeekly.Checked Then
                    time = CDate(txtWeekly.SelectedTime)
                End If

                sb.Append(Format(time.AddHours(Me.mobjUser.TimeDiffFromGMT * -1), "HHmm00"))
                sb.Append(Space(1))

                sb.Append("FREQ=")

                If rdoDaily.Checked Then
                    sb.Append("DAILY")
                ElseIf rdoWeekly.Checked Then
                    sb.Append("WEEKLY")
                End If

                If rdoWeekly.Checked Then
                    sb.Append(";BYDAY=")
                    If chkMonday.Checked Then sb.Append("MO,")
                    If chkTuesday.Checked Then sb.Append("TU,")
                    If chkWednesday.Checked Then sb.Append("WE,")
                    If chkThursday.Checked Then sb.Append("TH,")
                    If chkFriday.Checked Then sb.Append("FR,")
                    If chkSaturday.Checked Then sb.Append("SA,")
                    If chkSunday.Checked Then sb.Append("SU")
                End If

                strScheduleText = sb.ToString

                If strScheduleText.EndsWith(",") Then strScheduleText = strScheduleText.Remove(strScheduleText.LastIndexOf(","), 1)

                'Shcedule Data
                Dim intDecimal As Integer
                If chkMonday.Checked Then intDecimal += 2
                If chkTuesday.Checked Then intDecimal += 4
                If chkWednesday.Checked Then intDecimal += 8
                If chkThursday.Checked Then intDecimal += 16
                If chkFriday.Checked Then intDecimal += 32
                If chkSaturday.Checked Then intDecimal += 64
                If chkSunday.Checked Then intDecimal += 1

                strHex = Microsoft.VisualBasic.Hex(intDecimal)

                Dim intHours, intMinutes As Integer

                intHours = time.AddHours(Me.mobjUser.TimeDiffFromGMT * -1).Hour * 1080000
                intMinutes = time.AddHours(Me.mobjUser.TimeDiffFromGMT * -1).Minute * 18000

                intUtcTime = intHours + intMinutes

            End If

            If rdoNow.Checked Then intUtcTime = 0

            Dim intResult As Integer = 0

            intResult = Sync.DataSyncAddSubscriber(Me.mobjUser.AccountId.ToString, JobId, JobDetailId, intUtcTime, intScheduleType, strHex, strScheduleText)

            If intResult <= 0 Then
                'ERROR
                Throw New Exception("Error inserting schedule. Returned: " & intResult)
            Else
                Response.Redirect("Synchs.aspx")
            End If

        Catch ex As Exception

#If DEBUG Then
            lblError.Text = ex.Message
#Else
            lblError.Text = "An error has occured while adding this synchronization. If the problem continues please contact support."
#End If
            lblError.Visible = True

            'If ex.Message.IndexOf("Thread was being aborted") = -1 Then
            '    Dim conMaster As SqlClient.SqlConnection
            '    conMaster = Functions.BuildMasterConnection
            '    Functions.LogException(mobjUser.UserId.ToString, mobjUser.AccountId.ToString, "", "Data Sync Schedule Add: " & Space(1) & ex.Message, ex.StackTrace, Server.MachineName, "", Request.UrlReferrer.ToString, conMaster, True)
            'End If

        Finally

            'ReturnControls.Add(lblError)

            If Me.mconDataSync.State <> ConnectionState.Closed Then
                Me.mconDataSync.Close()
                Me.mconDataSync.Dispose()
            End If

        End Try

    End Sub

    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.PreRender

        For Each li As ListItem In ddlJobs.Items
            li.Attributes.Add("onfocus", "alert('" & li.Text & "');")
        Next

        'ReturnControls.Add(ddlJobs)

    End Sub

End Class
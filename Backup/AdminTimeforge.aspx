<%@ Register TagPrefix="uc1" TagName="TopRight" Src="Controls/TopRight.ascx" %>
<%@ Register TagPrefix="uc1" TagName="docHeader" Src="Controls/docHeader.ascx" %>
<%@ Register TagPrefix="uc1" TagName="AdminNav" Src="Controls/AdminNav.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="AdminTimeforge.aspx.vb" Inherits="docAssistWeb.AdminTimeforge"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>docAssist - Document View</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<TABLE height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TBODY>
					<TR>
						<TD width="100%" colSpan="5">
							<uc1:docHeader id="docHeader" runat="server"></uc1:docHeader></TD>
					</TR>
					<TR vAlign="middle" height="100%">
						<TD class="Navigation1" style="BORDER-RIGHT: #cccccc thin solid" vAlign="top" height="100%">
							<uc1:AdminNav id="docAdminNav" runat="server"></uc1:AdminNav></TD>
						<TD vAlign="top" align="left" height="100%">
							<h2 style="padding-top:20px;margin-left:25px;">TimeForge Integration Configuration</h2>
							<div style="PADDING-LEFT:30px; FLOAT:left">Your integration key is:
							</div>
							<span id="tfid" runat="server"></span>
						<TD width="10%"></TD>
					</TR>
					</TR>
					<TR>
						<TD class="PageFooter" vAlign="bottom" bgColor="#cccccc" colSpan="3">
							<uc1:TopRight id="docFooter" runat="server"></uc1:TopRight></TD>
					</TR>
					<TR height="1">
						<TD style="BACKGROUND-COLOR: #959394" width="20"><IMG height="1" src="images/spacer.gif" width="20"></TD>
						<TD style="BACKGROUND-COLOR: #959394" width="100%"><IMG height="1" src="images/spacer.gif" width="100%"></TD>
						<TD style="BACKGROUND-COLOR: #959394" width="20"><IMG height="1" src="images/spacer.gif" width="20"></TD>
					</TR>
				</TBODY>
			</TABLE>
		</form>
	</body>
</HTML>

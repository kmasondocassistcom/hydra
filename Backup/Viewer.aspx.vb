Imports System.IO
Imports Accucentric.docAssist
Imports Accucentric.docAssist.Web
Imports System.Web.SessionState
Imports Atalasoft.Imaging
Imports Atalasoft.Imaging.Codec
Imports Atalasoft.Imaging.Codec.Tiff
Imports Atalasoft.Imaging.Drawing
Imports Atalasoft.Imaging.ImageProcessing
Imports Atalasoft.Imaging.ImageProcessing.Document
Imports Atalasoft.Imaging.ImageProcessing.Transforms
Imports Atalasoft.Imaging.WebControls
Imports Atalasoft.Imaging.WebControls.Annotations
Imports Atalasoft.Annotate
Imports Atalasoft.Annotate.UI
Imports Atalasoft.Annotate.Renderer
Imports System.Runtime.Serialization
Imports System.Runtime.Serialization.SerializationBinder
Imports System.Reflection

Namespace Viewer

    Partial Class Viewer
        Inherits System.Web.UI.Page

#Region "Declarations"

        Private mobjUser As Accucentric.docAssist.Web.Security.User
        Private mcookieSearch As cookieSearch

        Private mconSqlImage As SqlClient.SqlConnection

        Protected WithEvents txtFileNameServer As System.Web.UI.WebControls.TextBox
        Protected WithEvents lblFileNameClient As System.Web.UI.WebControls.Label
        Protected WithEvents litScripts As System.Web.UI.WebControls.Literal
        Protected WithEvents lblVersionId As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents lblPageNo As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents lblFileNameResult As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents WorkflowTabs As System.Web.UI.HtmlControls.HtmlTableRow
        Protected WithEvents navIndex As IndexNavigation
        Protected WithEvents txtUserId As System.Web.UI.WebControls.Label
        Protected WithEvents tabThumbnails As System.Web.UI.HtmlControls.HtmlImage
        Protected WithEvents dgDistribution As System.Web.UI.WebControls.DataGrid
        Protected WithEvents Hidden1 As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents Hidden2 As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents Annotation As Atalasoft.Imaging.WebControls.Annotations.WebAnnotationViewer

#End Region

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents Literal1 As System.Web.UI.WebControls.Literal
        Protected WithEvents litUnlockImageCtrl As System.Web.UI.WebControls.Literal

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Public Sub New()
            AnnotationRenderers.Add(GetType(StickyNote), New TextAnnotationRenderingEngine)
            AnnotationRenderers.Add(GetType(TextTool), New TextAnnotationRenderingEngine)
        End Sub

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

            Try

                Functions.CheckTimeout(Me.Page)

                Me.mobjUser = BuildUserObject(Request.Cookies("docAssist"))
                Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)

                txtUser.Text = Me.mobjUser.UserId.ToString

            Catch ex As Exception

                Select Case ex.Message
                    Case "DualLogin"
                        Response.Cookies("docAssist")("DualLogin") = True
                        If Request.Url.Query.Trim.Length > 0 Then
                            Response.Redirect("Default.aspx" & Request.Url.Query)
                        Else
                            Server.Transfer("Default.aspx")
                        End If
                    Case "User is not logged on"
                        If Request.Url.Query.Trim.Length > 0 Then
                            Response.Redirect("Default.aspx" & Request.Url.Query)
                        Else
                            Server.Transfer("Default.aspx")
                        End If
                    Case "Thread was being aborted."
                        If Request.Url.Query.Trim.Length > 0 Then
                            Response.Redirect("Default.aspx" & Request.Url.Query)
                        Else
                            Server.Transfer("Default.aspx")
                        End If
                End Select

            End Try

            Dim dsVersionControl As New Accucentric.docAssist.Data.dsVersionControl

            If Not Page.IsPostBack Then

                Try

                    Dim vid As Integer = Request.QueryString("VID")

                    'Load document into session
                    dsVersionControl = Accucentric.docAssist.Data.GetVersionControl(vid, Me.mconSqlImage, Me.mobjUser.ImagesUserId)
                    Session(vid & "VersionControl") = dsVersionControl

                    If Request.QueryString("ref") = "explorer" And Request.QueryString("load") = "fulltext" Then
                        System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.NewGuid.ToString, "setTimeout(""textPopup(0,1);"",1000)", True)
                    End If

                    If Request.QueryString("ref") = "pdfviewer" And Request.QueryString("load") = "fulltext" Then
                        System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.NewGuid.ToString, "setTimeout(""textPopup(1,1);"",1000)", True)
                    End If

                    lblAttributeError.Text = ""
                    lblAttributeError.Visible = False

                    Me.mcookieSearch = New cookieSearch(Me)

                    If Not Request.QueryString("VID") Is Nothing Then
                        Me.mcookieSearch.VersionID = CInt(Request.QueryString("VID"))
                    End If

                    LoadImage()

                    RegisterStartupScript("Ids", "<script>var versionId = " & Me.mcookieSearch.VersionID & ";var imageId = " & CType(dsVersionControl.Images.Rows(0), Accucentric.docAssist.Data.dsVersionControl.ImagesRow).ImageID & ";</script>")

                Catch ex As Exception

                    If ex.Message = "Version has been deleted." Then
                        Server.Transfer("VersionDeleted.aspx", True)
                    Else
                        Dim conMaster As SqlClient.SqlConnection
                        conMaster = Functions.BuildMasterConnection
                        Functions.LogException(mobjUser.UserId.ToString, mobjUser.AccountId.ToString, "", "Viewer Page Load (First Segment): " & ex.Message, ex.StackTrace, Server.MachineName, "", Request.UrlReferrer.ToString, conMaster, True)
                    End If

                End Try

                Try
                    'Find current row
                    Dim dc As New DataColumn("VersionId")
                    Session("dtVersionId").PrimaryKey = New DataColumn() {Session("dtVersionId").Columns("VersionId")}
                    Dim tmpDr As DataRow = Session("dtVersionId").Rows.Find(Me.mcookieSearch.VersionID)
                    Session("intCurrentRow") = (tmpDr("RowID") + 1)
                    Session("intPrevNextVersionId") = Me.mcookieSearch.VersionID
                Catch ex As Exception

                End Try

                Try
                    'Check if user is supposed to see this version, if not change the version id to the one they have access to and hide the version drop down
                    Dim intVersionId As Integer = Me.mcookieSearch.VersionID
                    Dim intRestrict As Integer
                    Dim intVersionIdReturned As Integer
                    Dim intImageType As Integer
                    Dim documentId, pages As Integer
                    Functions.UserVersionOnlyCheck(Me.mobjUser.ImagesUserId, intVersionId, intVersionIdReturned, intRestrict, Me.mconSqlImage, intImageType, documentId, pages)

                    If intVersionId <> intVersionIdReturned Then
                        Me.mcookieSearch.VersionID = intVersionIdReturned
                        intVersionId = intVersionIdReturned
                    End If

                    If intVersionId <> intVersionIdReturned Then
                        Me.mcookieSearch.VersionID = intVersionIdReturned
                        VersionId.Value = intVersionIdReturned
                    Else
                        VersionId.Value = intVersionId
                    End If

                    Session(intVersionId & "PageCount") = pages

                    Session("FileType") = intImageType
                    ImageType.Value = intImageType

                    Session("ImageType") = CInt(CType(Session("FileType"), Constants.ImageTypes))

                    Select Case CType(Session("FileType"), Constants.ImageTypes)
                        Case Constants.ImageTypes.ATTACHMENT

                            If Request.QueryString("ref") = "pdfviewer" Then
                                Session("Filename") = Nothing
                                Dim returns As String = BuildPage(Me.mcookieSearch.VersionID, 1, Me.mobjUser.DefaultQuality, True)
                                frameFile.Visible = False
                                System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.NewGuid.ToString, "pdfViewer = true;", True)
                                tblThumbnails.Visible = False
                                tdAnnotations.Visible = False
                            Else
                                ImageType.Value = "3"
                                tblThumbnails.Visible = False
                                objViewer.Visible = False
                                Page.Controls.Remove(objViewer)
                                objViewer = Nothing
                                frameFile.Visible = True
                                frameFile.Attributes("src") = "ViewAttachments.aspx"
                                tdAnnotations.Visible = False
                                frameAttributes.Attributes("src") = "ViewerAttributes.aspx"
                                System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.NewGuid.ToString, "pdfViewer = false;", True)
                            End If

                            frameEmail.Visible = False

                        Case Constants.ImageTypes.SCAN

                            Dim returns As String = BuildPage(Me.mcookieSearch.VersionID, 1, Me.mobjUser.DefaultQuality)
                            frameFile.Visible = False
                            System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.NewGuid.ToString, "var res = " & returns & ";clearQuality();if (res > 0) { addQuality('Low','1'); }if (res >= 50) { addQuality('Medium','50');	}if (res >= 100) { addQuality('High','100'); }selectQuality(document.all.DownloadQuality.value);pdfViewer = false;", True)

                            frameEmail.Visible = False

                        Case Constants.ImageTypes.EMAIL

                            Dim conSql As New SqlClient.SqlConnection

                            Try

                                ImageType.Value = "5"
                                tblThumbnails.Visible = False
                                objViewer.Visible = False
                                Page.Controls.Remove(objViewer)
                                objViewer = Nothing
                                frameFile.Visible = False
                                tdAnnotations.Visible = False
                                frameAttributes.Attributes("src") = "ViewerAttributes.aspx"
                                System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.NewGuid.ToString, "pdfViewer = false;", True)

                                frameEmail.Visible = True

                                conSql = Functions.BuildConnection(Me.mobjUser)

                                If conSql.State <> ConnectionState.Open Then conSql.Open()

                                'Get the message from the database, it is stored in the Image column just like a regular image
                                Dim sqlCmd As New SqlClient.SqlCommand("acsImageDetailRange", conSql)
                                sqlCmd.CommandType = CommandType.StoredProcedure
                                sqlCmd.Parameters.Add("@VersionID", VersionId.Value)
                                sqlCmd.Parameters.Add("@PageStart", 1)
                                sqlCmd.Parameters.Add("@PageEnd", 1)

                                Dim dt As New DataTable
                                Dim da As New SqlClient.SqlDataAdapter(sqlCmd)
                                da.Fill(dt)

                                If dt.Rows.Count = 0 Then
                                    'ToDo: No email body returned
                                Else
                                    Dim msg As Byte() = dt.Rows(0)("Image")
                                    Dim body As String = System.Text.Encoding.ASCII.GetString(msg)

                                    'TODO: Strip javascript
                                    frameEmail.InnerHtml = body.Replace(vbLf, "<BR>")

                                End If

                            Catch ex As Exception

                                If conSql.State <> ConnectionState.Closed Then
                                    conSql.Close()
                                    conSql.Dispose()
                                End If

                            End Try




                    End Select

                    If Request.QueryString("WFA") = "1" Then
                        frameAttributes.Attributes("src") = "ViewerAttributes.aspx?WFA=1"
                    Else
                        frameAttributes.Attributes("src") = "ViewerAttributes.aspx"
                    End If

                    If intRestrict = 0 Then
                        Me.mcookieSearch.HideVersionDropDownList = True
                    Else
                        Me.mcookieSearch.HideVersionDropDownList = False
                    End If

                    pageNo.Value = "1"

                    accountId.Value = Me.mobjUser.AccountId.ToString

                    DownloadQuality.Value = Me.mobjUser.DefaultQuality

                    If Request.QueryString("ref") <> "pdfviewer" Then
                        pageCount.Value = pages
                    End If

                    docId.Value = documentId

                    If Not IsNothing(objViewer) Then

                        Select Case mobjUser.DefaultDocView
                            Case 0
                                objViewer.AutoZoom = Atalasoft.Imaging.WebControls.AutoZoomMode.BestFit
                            Case 1
                                objViewer.AutoZoom = Atalasoft.Imaging.WebControls.AutoZoomMode.FitToWidth
                            Case 2
                                objViewer.AutoZoom = Atalasoft.Imaging.WebControls.AutoZoomMode.FitToHeight
                            Case Else
                                objViewer.AutoZoom = Atalasoft.Imaging.WebControls.AutoZoomMode.BestFit
                        End Select

                    End If

                Catch ex As Exception

                    Dim conMaster As SqlClient.SqlConnection
                    conMaster = Functions.BuildMasterConnection
                    Functions.LogException(mobjUser.UserId.ToString, mobjUser.AccountId.ToString, "", "Viewer Page Load: " & ex.Message, ex.StackTrace, Server.MachineName, "", Request.UrlReferrer.ToString, conMaster, True)

                Finally

                    Me.mconSqlImage.Close()
                    Me.mconSqlImage.Dispose()

                End Try

            End If

        End Sub

        Private Function LoadImage()

            navIndex.Visible = True

            Dim intCurrentRouteId As Integer = 0
            Dim wfMode As Functions.WorkflowMode

            Dim strVersionId As String = Me.mcookieSearch.VersionID
            Session(strVersionId & "WorkflowId") = Nothing
            Session(strVersionId & "QueueId") = Nothing
            Session(strVersionId & "Notes") = Nothing
            Session(strVersionId & "Urgent") = Nothing
            Session(strVersionId & "Route") = Nothing

            If Not Page.IsPostBack Then
                'Log recent item
                Try
                    Dim blnReturn As Boolean = Functions.TrackRecentItem(Me.mobjUser, "VW", Me.mcookieSearch.VersionID)
                Catch ex As Exception
                End Try
            End If

            'Account level workflow
            If Functions.ModuleSetting(Functions.ModuleCode.WORKFLOW, Me.mobjUser) Then

                'Check if workflow console should be displayed
                wfMode = CheckWorkflow(Me.mobjUser.ImagesUserId, Me.mcookieSearch.VersionID, Me.mobjUser, intCurrentRouteId)

                Try
                    Session("WorkflowMode") = wfMode
                Catch ex As Exception

                End Try

                tblWorkflow.Visible = True

                Select Case wfMode
                    Case Functions.WorkflowMode.NO_ACCESS
                        tblWorkflow.Style("display") = "none"
                        frameWorkflow.Attributes("src") = "dummy.htm"
                    Case Functions.WorkflowMode.AUTHORIZED_SUBMITTER_NOT_SUBMITTED
                        'tblWorkflow.Style("visibility") = "visible"
                        ShowWorkflow.Value = "1"
                        'tblWorkflow.Style("display") = "block"
                        RegisterStartupScript("WFPane", "<script language='javascript'>document.all.tblWorkflow.style.display = 'block';</script>")
                        WorkflowTabs.Style("display") = "none"
                        frameWorkflow.Attributes("src") = "WorkflowSubmit.aspx?Mode=" & Functions.WorkflowMode.AUTHORIZED_SUBMITTER_NOT_SUBMITTED
                        frameWorkflow.Attributes("scrolling") = "no"
                    Case Functions.WorkflowMode.VIEW_ACCESS_REVIEWER_PENDING
                        'tblWorkflow.Style("visibility") = "visible"
                        ShowWorkflow.Value = "1"
                        RegisterStartupScript("WFPane", "<script language='javascript'>document.all.tblWorkflow.style.display = 'block';</script>")
                        WorkflowTabs.Style("display") = "none"
                        frameWorkflow.Attributes("src") = "WorkflowHistory.aspx"
                        frameWorkflow.Attributes("scrolling") = "auto"
                    Case Functions.WorkflowMode.ACTION_ACCESS_REVIEWER_PENDING
                        'tblWorkflow.Style("visibility") = "visible"
                        ShowWorkflow.Value = "1"
                        RegisterStartupScript("WFPane", "<script language='javascript'>document.all.tblWorkflow.style.display = 'block';</script>")
                        WorkflowTabs.Style("display") = "block"
                        frameWorkflow.Attributes("src") = "WorkflowReview.aspx?Mode=" & Functions.WorkflowMode.ACTION_ACCESS_REVIEWER_PENDING & "&Route=" & intCurrentRouteId
                        frameWorkflow.Attributes("scrolling") = "no"
                    Case Functions.WorkflowMode.AUTHORIZED_SUBMITTER_SUBMITTED
                        'tblWorkflow.Style("visibility") = "visible"
                        ShowWorkflow.Value = "1"
                        RegisterStartupScript("WFPane", "<script language='javascript'>document.all.tblWorkflow.style.display = 'block';</script>")
                        WorkflowTabs.Style("display") = "none"
                        frameWorkflow.Attributes("src") = "WorkflowHistory.aspx"
                        frameWorkflow.Attributes("scrolling") = "auto"
                    Case Functions.WorkflowMode.REVIEWED
                        'tblWorkflow.Style("visibility") = "visible"
                        ShowWorkflow.Value = "1"
                        RegisterStartupScript("WFPane", "<script language='javascript'>document.all.tblWorkflow.style.display = 'block';</script>")
                        WorkflowTabs.Style("display") = "none"
                        frameWorkflow.Attributes("src") = "WorkflowHistory.aspx"
                        frameWorkflow.Attributes("scrolling") = "auto"
                    Case Functions.WorkflowMode.AUTHORIZED_SUBMITTER_RESUBMIT
                        'tblWorkflow.Style("visibility") = "visible"
                        ShowWorkflow.Value = "1"
                        'tblWorkflow.Style("display") = "block"
                        RegisterStartupScript("WFPane", "<script language='javascript'>document.all.tblWorkflow.style.display = 'block';</script>")
                        frameWorkflow.Attributes("src") = "WorkflowSubmit.aspx?Mode=" & Functions.WorkflowMode.AUTHORIZED_SUBMITTER_NOT_SUBMITTED
                        frameWorkflow.Attributes("scrolling") = "no"
                    Case Else
                        tblWorkflow.Style("visibility") = "hidden"
                        frameWorkflow.Attributes("src") = "dummy.htm"
                End Select

            Else

                'Account level workflow disabled, hide everything
                tblWorkflow.Visible = True
                tblWorkflow.Style("visibility") = "hidden"
                frameWorkflow.Attributes("src") = "dummy.htm"

                wfMode = Functions.WorkflowMode.NO_ACCESS 'Toolbar looks at this variable for permissions

            End If

            txtShowImage.Value = 1
            litHideProgress.Visible = False

        End Function

        Protected Overrides Sub Finalize()

            MyBase.Finalize()

        End Sub

        Private Sub btnWf_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnWf.Click

            Select Case mode.Value
                Case Functions.WorkflowMode.NO_ACCESS
                    tblWorkflow.Style("display") = "none"
                    frameWorkflow.Attributes("src") = "dummy.htm"
                Case Functions.WorkflowMode.AUTHORIZED_SUBMITTER_NOT_SUBMITTED '1
                    ShowWorkflow.Value = "1"
                    System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.NewGuid.ToString, "document.all.tblWorkflow.style.display = 'block';", True)
                    WorkflowTabs.Style("display") = "none"
                    frameWorkflow.Attributes("src") = "WorkflowSubmit.aspx?Mode=" & Functions.WorkflowMode.AUTHORIZED_SUBMITTER_NOT_SUBMITTED '1
                    frameWorkflow.Attributes("scrolling") = "no"
                Case Functions.WorkflowMode.VIEW_ACCESS_REVIEWER_PENDING '10
                    ShowWorkflow.Value = "1"
                    System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.NewGuid.ToString, "document.all.tblWorkflow.style.display = 'block';", True)
                    WorkflowTabs.Style("display") = "none"
                    frameWorkflow.Attributes("src") = "WorkflowHistory.aspx"
                    frameWorkflow.Attributes("scrolling") = "auto"
                Case Functions.WorkflowMode.ACTION_ACCESS_REVIEWER_PENDING '20
                    ShowWorkflow.Value = "1"
                    System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.NewGuid.ToString, "document.all.tblWorkflow.style.display = 'block';", True)
                    WorkflowTabs.Style("display") = "block"
                    frameWorkflow.Attributes("src") = "WorkflowReview.aspx?Mode=" & Functions.WorkflowMode.ACTION_ACCESS_REVIEWER_PENDING & "&Route=" & routeid.Value '20
                    frameWorkflow.Attributes("scrolling") = "no"
                Case Functions.WorkflowMode.AUTHORIZED_SUBMITTER_SUBMITTED '2
                    ShowWorkflow.Value = "1"
                    System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.NewGuid.ToString, "document.all.tblWorkflow.style.display = 'block';", True)
                    WorkflowTabs.Style("display") = "none"
                    frameWorkflow.Attributes("src") = "WorkflowHistory.aspx"
                    frameWorkflow.Attributes("scrolling") = "auto"
                Case Functions.WorkflowMode.REVIEWED '30
                    ShowWorkflow.Value = "1"
                    System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.NewGuid.ToString, "document.all.tblWorkflow.style.display = 'block';", True)
                    WorkflowTabs.Style("display") = "none"
                    frameWorkflow.Attributes("src") = "WorkflowHistory.aspx"
                    frameWorkflow.Attributes("scrolling") = "auto"
                Case Functions.WorkflowMode.AUTHORIZED_SUBMITTER_RESUBMIT '3
                    ShowWorkflow.Value = "1"
                    System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.NewGuid.ToString, "document.all.tblWorkflow.style.display = 'block';", True)
                    frameWorkflow.Attributes("src") = "WorkflowSubmit.aspx?Mode=" & Functions.WorkflowMode.AUTHORIZED_SUBMITTER_NOT_SUBMITTED '1
                    frameWorkflow.Attributes("scrolling") = "no"
                Case Else
                    tblWorkflow.Style("visibility") = "hidden"
                    frameWorkflow.Attributes("src") = "dummy.htm"
            End Select

            'System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.NewGuid.ToString, "ResizeControls();", True)

        End Sub

        Private Sub objViewer_AnnotationCreated(ByVal sender As Object, ByVal e As AnnotationCreatedEventArgs) Handles objViewer.AnnotationCreated

            Me.mobjUser = BuildUserObject(Request.Cookies("docAssist"))

            Select Case objViewer.Annotations.Layers.Count
                Case 0
                    objViewer.Annotations.Layers.Add(New LayerAnnotation)
                    objViewer.Annotations.Layers.Add(New LayerAnnotation)
                Case 1
                    objViewer.Annotations.Layers.Add(New LayerAnnotation)
            End Select

            'Sticky note
            Dim sticky As StickyNote = CType(IIf(TypeOf e.AnnotationData Is StickyNote, e.AnnotationData, Nothing), StickyNote)
            If Not sticky Is Nothing Then
                sticky.Text = "Double click to edit"
                sticky.Fill = New AnnotationBrush(Color.Yellow)
                sticky.Shadow = New AnnotationBrush(Color.Gray)
                sticky.ShadowOffset = New PointF(10, 10)
                sticky.Font = New AnnotationFont("Arial", 60.0F)
                sticky.CanRotate = True
                Dim stickyAnn As New TextAnnotation(sticky)
                objViewer.Annotations.Layers(0).Items.Add(stickyAnn)
                Return
            End If

            Dim textTl As TextTool = CType(IIf(TypeOf e.AnnotationData Is TextTool, e.AnnotationData, Nothing), TextTool)
            If Not textTl Is Nothing Then
                textTl.Text = "Double click to edit"
                textTl.Fill = New AnnotationBrush(Color.White)
                textTl.FontBrush = New AnnotationBrush(Color.Black)
                textTl.Font = New AnnotationFont("Arial", 60.0F)
                textTl.CanRotate = True
                Dim txtTool As New TextAnnotation(textTl)
                objViewer.Annotations.Layers(0).Items.Add(txtTool)
                Return
            End If

            Dim rect As RectangleData = CType(IIf(TypeOf e.AnnotationData Is RectangleData, e.AnnotationData, Nothing), RectangleData)
            If Not rect Is Nothing Then
                rect.Outline = New AnnotationPen(Color.Transparent, 1)
                rect.Fill = New AnnotationBrush(Color.FromArgb(128, Color.Yellow))
                rect.CanRotate = True
                Dim rectAnno As New RectangleAnnotation(rect)
                objViewer.Annotations.Layers(0).Items.Add(rectAnno)
                Return
            End If

            Dim ellipse As EllipseData = CType(IIf(TypeOf e.AnnotationData Is EllipseData, e.AnnotationData, Nothing), EllipseData)
            If Not ellipse Is Nothing Then
                ellipse.CanRotate = True
                ellipse.Outline = New AnnotationPen(Color.Red, 12)
                ellipse.Fill = New AnnotationBrush(Color.Transparent)
                Dim ellipseAnno As New EllipseAnnotation(ellipse)
                objViewer.Annotations.Layers(0).Items.Add(ellipseAnno)
                Return
            End If

        End Sub

        <RemoteInvokable()> _
        Public Function Track(ByVal versionId As String, ByVal pageNo As String) As Boolean

            Try

                Me.mcookieSearch = New cookieSearch(Me)

                Dim intVersionId As Integer = Me.mcookieSearch.VersionID

                Dim annotations As Byte()
                Dim redactions As Byte()

                'Annotations
                If objViewer.Annotations.Layers(0).Items.Count > 0 Then

                    Dim annoCon As New WebAnnotationController
                    Dim msAnn As New MemoryStream

                    If objViewer.Annotations.Layers(0).Items.Count > 0 Then
                        annoCon.Layers.Add(objViewer.Annotations.Layers(0))
                    End If

                    msAnn.Seek(0, SeekOrigin.Begin)
                    annoCon.Save(msAnn, AnnotationDataFormat.Xmp)
                    annotations = msAnn.ToArray()

                End If

                Dim dsVersion As Accucentric.docAssist.Data.dsVersionControl

                'Load the existing dataset
                If Not Session(intVersionId & "VersionControl") Is Nothing Then
                    dsVersion = Session(intVersionId & "VersionControl")
                End If

                'Check for the page already existing
                'First get the ImageDetID column for the page being processed
                Dim objFind(1) As Object
                objFind(0) = intVersionId
                objFind(1) = pageNo
                Dim drImageVersionDetail As Accucentric.docAssist.Data.dsVersionControl.ImageVersionDetailRow = dsVersion.ImageVersionDetail.Rows.Find(objFind)

                'Then get the annotation for the page processed
                Dim objFind2(1) As Object
                objFind2(0) = drImageVersionDetail("VersionID")
                objFind2(1) = drImageVersionDetail("ImageDetID")

                Dim drImageAnnotation As Accucentric.docAssist.Data.dsVersionControl.ImageAnnotationRow
                drImageAnnotation = dsVersion.ImageAnnotation.Rows.Find(objFind2)

                ' Save the annotation
                If Not drImageAnnotation Is Nothing Then
                    If objViewer.Annotations.Layers(0).Items.Count = 0 Then
                        'If Not annotations Is Nothing Then drImageAnnotation.Annotation = Nothing
                        drImageAnnotation.Annotation = Nothing
                    Else
                        If Not annotations Is Nothing Then drImageAnnotation.Annotation = annotations
                    End If
                Else
                    Dim drImageAnnotationRow As Accucentric.docAssist.Data.dsVersionControl.ImageAnnotationRow
                    drImageAnnotationRow = dsVersion.ImageAnnotation.NewImageAnnotationRow
                    drImageAnnotationRow("VersionID") = drImageVersionDetail("VersionID")
                    drImageAnnotationRow("ImageDetID") = drImageVersionDetail("ImageDetID")
                    If Not annotations Is Nothing Then drImageAnnotationRow("Annotation") = annotations
                    'If Not redactions Is Nothing Then drImageAnnotationRow("Redaction") = redactions
                    dsVersion.ImageAnnotation.Rows.Add(drImageAnnotationRow)
                End If

                'Save the session variable
                Session(intVersionId & "VersionControl") = dsVersion

            Catch ex As Exception

            End Try

        End Function

        <RemoteInvokable()> _
        Public Function ClearAnnotations() As Boolean
            objViewer.Annotations.Layers.Clear()
        End Function

        <RemoteInvokable()> _
        Public Function GetImageAnnotations(ByVal VersionId As String, ByVal PageNo As String) As Boolean

            objViewer.ClearAnnotations()

            Me.mobjUser = BuildUserObject(Request.Cookies("docAssist"))

            Dim dsVersionControl As Accucentric.docAssist.Data.dsVersionControl

            'Load the existing dataset
            If Not Session(VersionId & "VersionControl") Is Nothing Then
                dsVersionControl = Session(VersionId & "VersionControl")
            End If

            Dim conSql As New SqlClient.SqlConnection

            Try

                conSql = BuildConnection(Me.mobjUser)

                Dim objFind(1) As Object
                Dim intImageDetId As Integer
                objFind(0) = VersionId
                objFind(1) = PageNo
                dsVersionControl.ImageVersionDetail.PrimaryKey = New DataColumn() {dsVersionControl.ImageVersionDetail.Columns("VersionId"), dsVersionControl.ImageVersionDetail.Columns("SeqId")}
                Dim drVersionImageDetail As Accucentric.docAssist.Data.dsVersionControl.ImageVersionDetailRow = dsVersionControl.ImageVersionDetail.Rows.Find(objFind)
                Dim dvImageAnnotation As New DataView(dsVersionControl.ImageAnnotation, "ImageDetID = " & drVersionImageDetail.ImageDetID.ToString, "ImageDetID", DataViewRowState.CurrentRows)

                If Not drVersionImageDetail Is Nothing Then

                    Dim bytAnnotation As Byte()

                    If Not IsNothing(drVersionImageDetail) Then

                        If dvImageAnnotation.Count > 0 Then

                            'Load annotation from session
                            If Not dvImageAnnotation.Item(0).Item("Annotation") Is Nothing Then
                                bytAnnotation = dvImageAnnotation.Item(0).Item("Annotation")
                            End If

                        Else

                            'Load from database
                            Dim sqlCmd As New SqlClient.SqlCommand("acsImageAnnotation", conSql)
                            sqlCmd.CommandType = CommandType.StoredProcedure
                            sqlCmd.Parameters.Add("@VersionID", VersionId)
                            sqlCmd.Parameters.Add("@ImageDetId", intImageDetId)

                            If conSql.State <> ConnectionState.Open Then conSql.Open()

                            Dim drAnn As SqlClient.SqlDataReader = sqlCmd.ExecuteReader(CommandBehavior.SingleRow)

                            If drAnn.Read() Then
                                bytAnnotation = drAnn("Annotation")
                            End If

                            drAnn.Close()

                        End If

                    End If

                    objViewer.Annotations.Layers.Clear()
                    objViewer.Annotations.Layers.Add(New LayerAnnotation)
                    objViewer.Annotations.Layers.Add(New LayerAnnotation)

                    'Apply annotations
                    If Not bytAnnotation Is Nothing Then

                        Dim msR As New MemoryStream(bytAnnotation, 0, bytAnnotation.Length)
                        msR.Seek(0, SeekOrigin.Begin)

                        'Dim tmpViewer As New WebAnnotationController
                        'tmpViewer.Load(msR, AnnotationDataFormat.Xmp)

                        objViewer.Annotations.Load(msR, AnnotationDataFormat.Xmp)

                        'For Each ann As AnnotationUI In tmpViewer.Layers(0).Items
                        '    objViewer.Annotations.Layers(0).Items.Add(ann)
                        'Next

                        'tmpViewer.Dispose()

                    End If

                End If

                objViewer.UpdateAnnotations()

                Return True

            Catch ex As Exception

                Dim errMsg As String = ""

            Finally

                If conSql.State <> ConnectionState.Closed Then
                    conSql.Close()
                    conSql.Dispose()
                End If

            End Try

        End Function

        'Private Sub objViewer_AnnotationsModified(ByVal sender As Object, ByVal e As System.EventArgs) Handles objViewer.AnnotationsModified

        '    Me.mcookieSearch = New cookieSearch(Me)

        '    Dim pageNo As Integer = Session("CurrentPage")

        '    If pageNo > 0 Then
        '        Track(mcookieSearch.VersionID, pageNo)
        '    End If

        'End Sub

        <RemoteInvokable()> _
        Public Function BuildPage(ByVal VersionId As String, ByVal PageNo As String, ByVal Quality As String, Optional ByVal pdfViewer As Boolean = False) As String

            Dim conSql As New SqlClient.SqlConnection

            Try

                Dim rotation As Integer = 0

                Me.mobjUser = BuildUserObject(Request.Cookies("docAssist"))

                'Track any existing annotations
                Dim trackPage As Integer = Session(VersionId & "CurrentPage")
                If trackPage > 0 Then Track(VersionId, trackPage)

                Session(VersionId & "CurrentPage") = PageNo

                Dim openFile As String
                If pdfViewer And System.IO.File.Exists(Session("Filename")) Then
                    Dim sourceFile As String = Session("Filename")
                    Dim fileName As String = System.Guid.NewGuid().ToString & ".tif"
                    Dim saveFileName As String = Server.MapPath("./tmp/" & fileName)
                    Functions.ExtractTIFPageFromPDF(sourceFile, PageNo, saveFileName)
                    objViewer.Open(saveFileName)
                Else

                    'Build new page
                    Dim ws As New docAssistImages

                    Dim blnPdf As Boolean = False
                    If Request.QueryString("ref") = "pdfviewer" Then blnPdf = True

                    If PageNo > CInt(Session(VersionId & "PageCount")) Then
                        Return Nothing
                    End If

                    Dim returns() As String = ws.BuildPageQuality(VersionId, PageNo, rotation, Me.mobjUser.AccountId, Quality, blnPdf)

                    objViewer.ClearAnnotations()
                    'objViewer.Image = Nothing
                    objViewer.Open(returns(0))

                    If pdfViewer And pageCount.Value = "" Then
                        pageCount.Value = Functions.PageCount(Session("Filename"))
                        'ReturnControls.Add(pageCount)
                    End If

                    'Load annotations
                    GetImageAnnotations(VersionId, PageNo)

                    Return returns(1)

                End If

            Catch ex As Exception

                'TODO: Need to return some script to show error, this isn't a full postback
                'ShowPopupError("An error has occured while loading this document.", True)

                Dim conMaster As SqlClient.SqlConnection
                conMaster = Functions.BuildMasterConnection
                Functions.LogException(mobjUser.UserId.ToString, mobjUser.AccountId.ToString, "", "Error Loading Document, VersionID: " & VersionId & " " & ex.Message, ex.StackTrace, Server.MachineName, "", Request.UrlReferrer.ToString, conMaster, True)

                Throw ex

            Finally

                objViewer.UpdateClient()

                If conSql.State <> ConnectionState.Closed Then
                    conSql.Close()
                    conSql.Dispose()
                End If

            End Try

        End Function

        Private Function ShowPopupError(ByVal Message As String, ByVal StaffNotificationMessage As Boolean)
            If StaffNotificationMessage Then
                System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.NewGuid.ToString, "showInfoBox('<b>" & Message & "</b><br><br>Our staff has been notified of the error and is working to correct any possible issues.');", True)
            Else
                System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.NewGuid.ToString, "showInfoBox('<b>" & Message & "</b>');", True)
            End If
        End Function

        Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender

        End Sub
    End Class

    Public Class StickyNote
        Inherits TextData
        Public Sub New()
            MyBase.New()
        End Sub
        Public Sub New(ByVal info As SerializationInfo, ByVal context As StreamingContext)
            MyBase.New(info, context)
        End Sub
    End Class

    Public Class TextTool
        Inherits TextData
        Public Sub New()
            MyBase.New()
        End Sub
        Public Sub New(ByVal info As SerializationInfo, ByVal context As StreamingContext)
            MyBase.New(info, context)
        End Sub
    End Class

End Namespace
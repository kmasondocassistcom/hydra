Imports System
Imports System.Data
Imports System.Configuration
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.IO
Imports Accucentric.docAssist
Imports Accucentric.docAssist.Data.Images

Public Class MultiUpload
    Implements System.Web.IHttpHandler

    Private mconSqlImage As New SqlClient.SqlConnection

    Public Sub New()

    End Sub

#Region "IHttpHandler Members"

    Public ReadOnly Property IsReusable() As Boolean Implements System.Web.IHttpHandler.IsReusable
        Get
            Return True
        End Get
    End Property

    Public Sub ProcessRequest(ByVal context As HttpContext) Implements System.Web.IHttpHandler.ProcessRequest

        Dim attempts As Integer = 1

Begin:

        Dim objUser As Accucentric.docAssist.Web.Security.User
        objUser = BuildUserObject(context.Request.Cookies("docAssist"))
        Me.mconSqlImage = Functions.BuildConnection(objUser)
        Dim trnSql As SqlClient.SqlTransaction

        Me.mconSqlImage.Open()
        trnSql = Me.mconSqlImage.BeginTransaction

        Dim sb As New System.Text.StringBuilder

        Try

            If context.Request.Files.Count > 0 Then

                ' get the applications path 
                Dim tempFile As String = context.Request.PhysicalApplicationPath

                Dim objAttachments As New FileAttachments(Me.mconSqlImage, False, trnSql)

                For j As Integer = 0 To context.Request.Files.Count - 1

                    Dim uploadFile As HttpPostedFile = context.Request.Files(j)

                    If uploadFile.ContentLength > 0 Then

                        Dim fileName As String = uploadFile.FileName
                        tempFile = context.Server.MapPath("./tmp") & "\" & System.Guid.NewGuid.ToString.Replace("-", "") & ".tmp"
                        uploadFile.SaveAs(tempFile)

                        Dim folderId As Integer = CInt(context.Request.Cookies("docAssistUpload")("BulkFolderId"))

                        Dim intNewVersionId As Integer
                        Dim fileData As Byte() = GetDataAsByteArray(tempFile)

                        sb.Append("FolderId: " & folderId & Chr(13))
                        sb.Append("DocumentId: " & "0" & Chr(13))
                        sb.Append("UserId: " & objUser.ImagesUserId & Chr(13))
                        sb.Append("Title: " & "" & Chr(13))
                        sb.Append("Desc: " & "" & Chr(13))
                        sb.Append("Comments: " & "" & Chr(13))
                        sb.Append("Tag: " & "" & Chr(13))
                        sb.Append("Length: " & fileData.Length & Chr(13))
                        sb.Append("Filename: " & fileName & Chr(13))

                        Dim intAttachmentId As Integer = objAttachments.InsertFolderAttachment(folderId, 0, objUser.ImagesUserId, "", "", "", "", fileData.Length, fileName, fileData, intNewVersionId)

                        trnSql.Commit()

                        sb.Append("ImageId: " & intAttachmentId & Chr(13))
                        sb.Append("VersionID returned: " & intNewVersionId & Chr(13))

                        'Track attachment
                        Try
                            Accucentric.docAssist.Web.Data.Functions.TrackPageRequest(objUser.UserId, objUser.AccountId, "Attachment Add", "In", intNewVersionId, intAttachmentId.ToString, Functions.GetMasterConnectionString)
                        Catch ex As Exception

                        End Try

                        Try
                            System.IO.File.Delete(tempFile)
                        Catch ex As Exception

                        End Try

                    End If

                Next

            End If

            ' Used as a fix for a bug in mac flash player that makes the 
            HttpContext.Current.Response.Write(" ")

        Catch sqlEx As SqlClient.SqlException

            If sqlEx.Number = 1205 Then 'Deadlock

                trnSql.Rollback()

                If attempts > 4 Then
                    Throw New Exception("Too many tries")
                End If

                attempts += 1

                Dim timeOut As DateTime = Now.AddMilliseconds(1000)
                Do
                Loop Until Now > timeOut

                GoTo Begin
            End If

        Catch ex As Exception

            trnSql.Rollback()

            'Dim logfile As String = context.Server.MapPath("../tmp/") & "BULK_UPLOAD_ERROR" & System.Guid.NewGuid.ToString.Replace("-", "") & ".txt"
            'Dim fs As New System.IO.StreamWriter(logfile)
            'fs.write(ex.Message & Chr(13) & ex.StackTrace)
            'fs.Close()

            Dim conMaster As SqlClient.SqlConnection
            conMaster = Functions.BuildMasterConnection
            Functions.LogException(objUser.UserId.ToString, objUser.AccountId.ToString, "", "Bulk Upload Error: " & ex.Message, ex.StackTrace, HttpContext.Current.Server.MachineName, "", HttpContext.Current.Request.UrlReferrer.ToString, conMaster, True)

        Finally

            'Dim logfile As String = context.Server.MapPath("../tmp/") & System.Guid.NewGuid.ToString.Replace("-", "") & ".txt"
            'Dim fs As New System.IO.StreamWriter(logfile)
            'fs.Write(sb.ToString)
            'fs.Close()

            If Me.mconSqlImage.State <> ConnectionState.Closed Then
                Me.mconSqlImage.Close()
                Me.mconSqlImage.Dispose()
            End If

        End Try

    End Sub

    Public Function GetDataAsByteArray(ByVal strFilename As String) As Byte()

        Dim fsData As System.IO.FileStream
        Dim xTemp() As Byte

        fsData = New System.IO.FileStream(strFilename, IO.FileMode.Open, IO.FileAccess.Read)
        Dim len As Integer = CType(fsData.Length, Integer)
        Dim rData As New System.IO.BinaryReader(fsData)

        xTemp = rData.ReadBytes(len)

        rData.Close()
        fsData.Close()

        Return xTemp

    End Function

#End Region

End Class

<%@ Page Language="vb" AutoEventWireup="false" Codebehind="UserPreferences.aspx.vb" Inherits="docAssistWeb.UserPreferences"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title></title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body class="Prefs" bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form class="Prefs" id="frmPreferences" method="post" runat="server">
			<br>
			<table class="Prefs" cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
				<tr>
					<td width="20" rowSpan="6"></td>
					<td class="PrefHeader">Change My Display
					</td>
				</tr>
				<tr>
					<td><br>
						<IMG height="1" src="spacer.gif" width="20"><span class="SubHeader">Viewer Options</span>
					</td>
				</tr>
				<tr>
					<td align="left">
						<table class="Prefs" cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
							<tr>
								<td width="20" rowSpan="3"></td>
								<td class="LabelName" noWrap align="right">Default Start Page:</td>
								<td bgColor="whitesmoke">&nbsp;<asp:dropdownlist id="ddlStartPage" runat="server" Width="248px" CssClass="Text"></asp:dropdownlist></td>
							</tr>
							<tr>
								<td class="LabelName" noWrap align="right" width="160">Default Document View:</td>
								<td>&nbsp;<asp:dropdownlist id="ddlDocView" runat="server" Width="248px" CssClass="Text">
										<asp:ListItem Value="0">Best Fit</asp:ListItem>
										<asp:ListItem Value="1">Fit Width</asp:ListItem>
										<asp:ListItem Value="2">Fit Height</asp:ListItem>
									</asp:dropdownlist></td>
							</tr>
							<tr>
								<td class="LabelName" noWrap align="right" width="160">Results Per Page:</td>
								<td bgColor="whitesmoke">&nbsp;<asp:dropdownlist id="ddlNoResults" runat="server" Width="248px" CssClass="Text">
										<asp:ListItem Value="0">25</asp:ListItem>
										<asp:ListItem Value="1">50</asp:ListItem>
										<asp:ListItem Value="2">100</asp:ListItem>
									</asp:dropdownlist></td>
							</tr>
							<tr>
								<td></td>
								<td class="LabelName" noWrap align="right" width="160">Default Quality:</td>
								<td>&nbsp;<asp:dropdownlist id="ddlDefault" runat="server" CssClass="Text">
										<asp:ListItem Value="100">High</asp:ListItem>
										<asp:ListItem Value="50">Medium</asp:ListItem>
										<asp:ListItem Value="1">Low</asp:ListItem>
									</asp:dropdownlist>
									<asp:label id="lblDisabled" runat="server" CssClass="Text" Visible="False">(This setting has been disabled by your administrator)</asp:label></td>
							</tr>
							<tr>
								<td colSpan="2"></td>
								<td>
									<asp:checkbox id="CloseViewer" runat="server" CssClass="Text" Text="Close viewer after saving"></asp:checkbox></td>
							</tr>
							<tr>
								<td colSpan="3"><SPAN class="SubHeader"><IMG height="1" src="spacer.gif" width="20">Explorer 
										Options</SPAN></td>
							</tr>
							<tr>
								<td colSpan="3"><IMG height="1" src="spacer.gif" width="55"><asp:checkbox id="chkLoad" runat="server" CssClass="Text" Text="Skip results if only one match"></asp:checkbox><BR>
								</td>
							</tr>
							<tr>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<BR>
			<BR>
			<BR>
			</TD></TR></TABLE></TR></TABLE>
			<table cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
				<tr>
					<td><IMG height="1" src="spacer.gif" width="375">
						<asp:imagebutton id="btnSave" runat="server" ImageUrl="Images/btn_save.gif"></asp:imagebutton><IMG height="1" src="spacer.gif" width="20">
						<asp:label id="lblError" runat="server" CssClass="ErrorText" ForeColor="Red"></asp:label></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>

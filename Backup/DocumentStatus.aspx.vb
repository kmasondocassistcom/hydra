Imports Accucentric.docAssist

Partial Class DocumentStatus
    Inherits System.Web.UI.Page

    Private mconSqlImage As SqlClient.SqlConnection
    Private mcookieSearch As cookieSearch
    Protected WithEvents imgPending As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ImageButton1 As System.Web.UI.WebControls.ImageButton
    Private mobjUser As Web.Security.User

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Dim sqlConn As New SqlClient.SqlConnection
        Try
            If Request.Cookies("docAssist") Is Nothing Then Response.Redirect("default.aspx")

            If Request.Cookies("docAssist") Is Nothing Then
                Response.Redirect("Default.aspx")
                Exit Sub
            End If

            Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
            Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))
            Me.mobjUser.AccountId = guidAccountId

            ''SECURITY
            'Dim dtAccess As New DataTable
            'dtAccess = Functions.UserAccess(Functions.BuildConnection(mobjUser), mobjUser.UserId, mobjUser.AccountId)

            'If dtAccess.Rows(0)("Workflow") = False Then
            '    Response.Redirect("Default.aspx")
            'End If

            sqlConn = Functions.BuildConnection(Me.mobjUser)
            sqlConn.Open()
            Dim sqlCmd As New SqlClient.SqlCommand("acsWorkFlowAdminDocumentsByUserId")
            sqlCmd.Parameters.Add("@UserId", Me.mobjUser.UserId)
            sqlCmd.Connection = sqlConn
            sqlCmd.CommandType = CommandType.StoredProcedure
            sqlCmd.CommandTimeout = DEFAULT_COMMAND_TIMEOUT
            Dim daSql As New SqlClient.SqlDataAdapter(sqlCmd)
            Dim dtDocuments As New DataTable
            daSql.Fill(dtDocuments)

            ddlDocument.DataSource = dtDocuments
            ddlDocument.DataTextField = "DocumentName"
            ddlDocument.DataValueField = "DocumentID"
            ddlDocument.DataBind()

            sqlConn.Close()
            sqlConn.Dispose()

        Catch ex As Exception
            If sqlConn.State <> ConnectionState.Closed Then sqlConn.Close()
            sqlConn.Dispose()
        End Try




    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click

        Dim dateFrom As Date = txtFrom.Text
        Dim dtThrough As Date = txtThrough.Text

        Dim intFromDays As Integer = DateDiff(DateInterval.Day, dateFrom, Now)
        Dim intThroughDays As Integer = DateDiff(DateInterval.Day, dtThrough, Now)

        Dim sqlConn As New SqlClient.SqlConnection

        Try

            sqlConn = Functions.BuildConnection(Me.mobjUser)
            sqlConn.Open()
            Dim sqlCmd As New SqlClient.SqlCommand("acsWFDocumentStatus")

            sqlCmd.Parameters.Add("@UserId", Me.mobjUser.ImagesUserId)
            sqlCmd.Parameters.Add("@DocumentID", ddlDocument.SelectedValue)
            sqlCmd.Parameters.Add("@CurrentStatus", ddlStatus.SelectedValue)
            sqlCmd.Parameters.Add("@DaysBeginFilter", intFromDays)
            sqlCmd.Parameters.Add("@DaysEndFilter", intThroughDays)
            sqlCmd.Parameters.Add("@DiffFromGMT", Me.mobjUser.TimeDiffFromGMT)
            sqlCmd.Connection = sqlConn
            sqlCmd.CommandType = CommandType.StoredProcedure
            sqlCmd.CommandTimeout = DEFAULT_COMMAND_TIMEOUT

            Dim daSql As New SqlClient.SqlDataAdapter(sqlCmd)


            Dim dsResults As New DataSet
            daSql.Fill(dsResults)

            sqlConn.Close()
            sqlConn.Dispose()

            If dsResults.Tables(0).Rows.Count = 0 Then
                lblResults.Text = "No documents found."
                Exit Sub
            Else
                'Found documents
                Dim dvResults As New DataView

                dvResults = resultsTable(dsResults, 10020, 1)

                'Dim x As Integer = 5

            End If

        Catch ex As Exception

        End Try


    End Sub

    Friend Function resultsTable(ByRef dsSearchResults As DataSet, ByVal intSortAttributeId As Integer, ByVal intSortDirection As Integer, Optional ByVal strAttributeName As String = Nothing) As DataView
        '
        ' Check for rows
        'If Not dsSearchResults.Tables(0).Rows.Count > 0 Then
        '    Return Nothing
        'End If

        Dim dtSearchResults As DataTable = dsSearchResults.Tables(0)
        Dim dtResultsAttributes As DataTable
        If dsSearchResults.Tables.Count() > 1 Then
            dtResultsAttributes = dsSearchResults.Tables(1)
        End If
        '
        ' Create the result table and add the static columns
        Dim bolAdvancedSearch As Boolean = False
        Dim dtResult As New DataTable("Results")
        'dtResult.Columns.Add(New DataColumn("VersionId", GetType(System.Int32), Nothing, MappingType.Element))
        'dtResult.Columns.Add(New DataColumn("Document", GetType(System.String), Nothing, MappingType.Element))
        'dtResult.Columns.Add(New DataColumn("ModifiedDate", GetType(System.DateTime), Nothing, MappingType.Element))
        'dtResult.Columns.Add(New DataColumn("AddDate", GetType(System.DateTime), Nothing, MappingType.Element))
        'dtResult.Columns.Add(New DataColumn("VersionNum", GetType(System.Int32), Nothing, MappingType.Element))
        'dtResult.Columns.Add(New DataColumn("SeqTotal", GetType(System.Int32), Nothing, MappingType.Element))
        dtResult.Columns.Add(New DataColumn("AddDateFormatted", GetType(System.String), Nothing, MappingType.Element))
        'dtResult.Columns.Add(New DataColumn("AddDateFormatted", GetType(System.String), Nothing, MappingType.Element))
        dtResult.Columns.Add(New DataColumn("DocumentName", GetType(System.String), Nothing, MappingType.Element))
        dtResult.Columns.Add(New DataColumn("DocumentId", GetType(System.Int32), Nothing, MappingType.Element))
        dtResult.Columns.Add(New DataColumn("VersionId", GetType(System.Int32), Nothing, MappingType.Element))
        dtResult.Columns.Add(New DataColumn("VersionRouteId", GetType(System.Int32), Nothing, MappingType.Element))
        dtResult.Columns.Add(New DataColumn("RoleId", GetType(System.Int32), Nothing, MappingType.Element))
        dtResult.Columns.Add(New DataColumn("RoleDescription", GetType(System.String), Nothing, MappingType.Element))
        dtResult.Columns.Add(New DataColumn("AddedDate", GetType(System.DateTime), Nothing, MappingType.Element))
        dtResult.Columns.Add(New DataColumn("AddedUser", GetType(System.String), Nothing, MappingType.Element))
        dtResult.Columns.Add(New DataColumn("ImageId", GetType(System.Int32), Nothing, MappingType.Element))



        'dtResult.Columns.Add(New DataColumn("AttributeDataType", GetType(System.String), Nothing, MappingType.Element))
        If Not dtSearchResults.Columns("SearchText") Is Nothing Then
            bolAdvancedSearch = True
            dtResult.Columns.Add(New DataColumn("SearchText", GetType(System.String), Nothing, MappingType.Element))
        End If
        dtResult.Columns.Add(New DataColumn("WorkFlowDesc", GetType(System.String), Nothing, MappingType.Element))
        dtResult.PrimaryKey = New DataColumn() {dtResult.Columns("VersionId")}

        '
        ' Add Columns
        '
        If Not dtResultsAttributes Is Nothing Then
            For Each dr As DataRow In dtResultsAttributes.Rows
                dtResult.Columns.Add(New DataColumn(dr("AttributeName"), GetType(System.String), Nothing, MappingType.Element))
                'dtResult.Columns.Add(New DataColumn(dr("AttributeDataType"), GetType(System.String), Nothing, MappingType.Element))
            Next
        End If
        '
        ' Sort Results
        '
        Dim sbSort As New System.Text.StringBuilder
        Dim sbFilter As New System.Text.StringBuilder
        Select Case intSortAttributeId
            Case 10000
                sbSort.Append("Document")
            Case 10005
                sbSort.Append("SeqTotal")
            Case 10010
                sbSort.Append("VersionNum")
            Case 10015
                sbSort.Append("AddDate")
            Case 10020
                sbSort.Append("ModifiedDate")
            Case Else
                sbSort.Append(strAttributeName)
        End Select
        Select Case intSortDirection
            Case 0
                sbSort.Append(" ASC")
            Case 1
                sbSort.Append(" DESC")
        End Select
        '
        ' Loop through the search results and populate the table
        ' using only one row per version id.
        '
        'Dim drSearchResults As Accucentric.docAssist.Web.Data.dtSearchResultsRow
        Dim drResult As DataRow, dcResult As DataColumn
        For Each dr As DataRow In dtSearchResults.Rows
            AddResultRow(dr, dtResult, bolAdvancedSearch)
        Next
        '
        'Return dtResult
        Return New DataView(dtResult, Nothing, IIf(intSortAttributeId < 10000, strAttributeName, sbSort.ToString), DataViewRowState.CurrentRows)
    End Function

    Private Function AddResultRow(ByVal drSearchResults As DataRow, ByRef dtResult As DataTable, ByVal bolAdvancedSearch As Boolean)

        Dim drResult As DataRow
        '
        ' Check for existing versionid
        drResult = dtResult.Rows.Find(drSearchResults("VersionId"))
        If drResult Is Nothing Then
            drResult = dtResult.NewRow

            drResult("DocumentName") = drSearchResults("DocumentName")
            drResult("DocumentId") = drSearchResults("DocumentId")
            drResult("VersionId") = drSearchResults("VersionId")
            drResult("VersionRouteId") = drSearchResults("VersionRouteId")
            drResult("AddDateFormatted") = CType(drSearchResults("AddedDate"), DateTime).ToShortDateString & " " & CType(drSearchResults("AddedDate"), DateTime).ToShortTimeString
            drResult("VersionRouteId") = drSearchResults("VersionRouteId")
            drResult("RoleId") = drSearchResults("RoleId")
            drResult("RoleDescription") = drSearchResults("RoleDescription")
            drResult("AddedUser") = drSearchResults("AddedUser")
            drResult("ImageId") = drSearchResults("ImageId")

            If bolAdvancedSearch Then
                drResult("SearchText") = drSearchResults("SearchText")
                'Else
                'drResult("SearchText") = ""
            End If
            dtResult.Rows.Add(drResult)
        End If
        '
        ' Check for existing column
        Dim sb As New System.Text.StringBuilder
        If drSearchResults.Table.Columns.Contains("AttributeName") Then
            If Not drSearchResults("AttributeName") Is System.DBNull.Value Then
                'If dtResult.Columns(drSearchResults("AttributeName) Is Nothing Then
                '    dtResult.Columns.Add(New DataColumn(drSearchResults("AttributeName, GetType(System.String), Nothing, MappingType.Element))
                'End If
                '
                ' Append the existing data
                If Not drResult(drSearchResults("AttributeName")) Is System.DBNull.Value Then
                    sb.Append(drResult(drSearchResults("AttributeName")))
                    sb.Append("<BR>")
                End If
                '
                ' Add the new data
                Select Case drSearchResults("AttributeDataType")
                    Case "ListView"
                        sb.Append(drSearchResults("ValueDescription"))
                    Case "CheckBox"
                        sb.Append("X")
                    Case Else
                        sb.Append(drSearchResults("AttributeValue"))
                End Select
            End If
        End If

        If sb.ToString.Length > 0 Then
            drResult(drSearchResults("AttributeName")) = sb.ToString
        End If
    End Function




End Class


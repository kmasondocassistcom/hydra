<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="Controls/MenuBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="docHeader" Src="Controls/docHeader.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="Install.aspx.vb" Inherits="docAssistWeb.Install" EnableSessionState="False" enableViewState="True" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>docAssist - Update Components</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="css/core.css" type="text/css" rel="stylesheet">
		<style type="text/css">
		BODY
			{
				background-repeat: repeat-x;
				background-image: url(images/background.gif);
			}
		</style>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<table width="300" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="20">&nbsp;</td>
					<td><img src="images/logoBug.gif" alt="docAssist" width="185" height="89"></td>
				</tr>
			</table>
			<table width="100%">
				<tr>
					<td align="center">
						<table width="700">
							<tr>
								<td colspan="2">
									<h1>docAssist Components Required</h1>
									<h2>Please follow these 4 easy steps to install the required components.</h2>
									<ol id="step">
										<li>
											<p>Click <a href="Downloads/docAssistComponents.exe">here</a> to download the 
												required components.</p>
										</li>
										<li>
											<p>Once the file has downloaded you will be prompted with a window similar to this 
												one:<br>
												<br>
												<img src="images/SecurityWarning.jpg" alt="Sample Security Warning Dialog"></p>
										</li>
										<li>
											<p>Click "Run" to start the installation. The wizard will guide you through the 
												installation process.</p>
										</li>
										<li>
											<p>Once the installation has completed click <a href="Default.aspx">here</a> to 
												continue using docAssist.</p>
										</li>
									</ol>
								</td>
							</tr>
							<tr>
								<td width="45"><img src="Images/icon_sets/info.png"></td>
								<td>
									If you experience any difficulty installing these componenents, please contact <a href="Support.aspx">
										technical support</a> or email <a href="mailto:support@docassist.com">support@docassist.com</a>. 
									You can also reach technical support directly at 877.399.1100.
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			</TD></TR></TABLE>
		</form>
		<script src="scripts/jquery.js" type="text/javascript"></script>
		<script language="javascript">
			$(document).ready(function(){
				$("#step li").each(function (i) {
					i = i+1;
					$(this).addClass("item"+i);
			});
			 
				$("#number li").each(function (i) {
					i = i+1;
					$(this).addClass("item"+i);
			});
			 
				$("#commentlist li").each(function (i) {
					i = i+1;
					$(this).prepend('<span class="commentnumber"> #'+i+'</span>');
			});
			 
			});
		</script>
	</body>
</HTML>

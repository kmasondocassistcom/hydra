<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="Controls/MenuBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="docHeader" Src="Controls/docHeader.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ConfigureFolders.aspx.vb" Inherits="docAssistWeb._Folders"%>
<%@ Register TagPrefix="uc1" TagName="AdminNav" Src="Controls/AdminNav.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
  <HEAD>
		<TITLE>docAssist</TITLE>
		<META content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<META content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<META content="JavaScript" name="vs_defaultClientScript">
		<META content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
  </HEAD>
	<BODY onload="Disable();ShowConfiguration();SetupServices();ChangeCabinets();document.all.ddlCabinets.enabled=true;">
		<DIV id="svcUpdate" style="BEHAVIOR: url(webservice.htc)" onresult="onUpdateResult()"></DIV>
		<DIV id="svcGet" style="BEHAVIOR: url(webservice.htc)" onresult="onGetResult()"></DIV>
		<DIV id="svcGetFolders" style="BEHAVIOR: url(webservice.htc)" onresult="onGetCabinetResult()"></DIV>
		<FORM id="Form1" onsubmit="SelectAllAssigned();" method="post" runat="server">
			<INPUT id="hostname" type="hidden" runat="server">
			<TABLE height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TBODY>
					<TR>
						<TD width="100%" colSpan="3"><UC1:DOCHEADER id="docHeader" runat="server"></UC1:DOCHEADER></TD>
					</TR>
					<TR vAlign="middle" height="100%">
						<TD class="Navigation1" vAlign="top" width="180" height="100%"><UC1:ADMINNAV id="AdminNav1" runat="server"></UC1:ADMINNAV></TD>
						<TD class="PageContent" vAlign="middle" align="center" height="100%">
							<P class="Heading"><ASP:LABEL id="lblPageHeading" CssClass="PageHeading" Runat="server">Configuration Manager</ASP:LABEL></P>
							<TABLE class="transparent" cellSpacing="0" cellPadding="0" align="center" border="0">
								<TBODY>
									<TR>
										<TD class="TableHeader" style="BORDER-RIGHT: #d00000 1px solid; BORDER-TOP: #d00000 1px solid; BORDER-LEFT: #d00000 1px solid"
											width="364">Folders</TD>
										<TD>&nbsp;</TD>
										<TD class="TableHeader" style="BORDER-RIGHT: #d00000 1px solid; BORDER-TOP: #d00000 1px solid; BORDER-LEFT: #d00000 1px solid"
											width="336">
											<TABLE class="transparentnopadding" cellSpacing="0" cellPadding="0" width="100%" border="0">
												<TR class="TableHeader">
													<TD>Modify Folders</TD>
													<TD align="right"><A id="lnkNew" style="DISPLAY: none" href="javascript:Clear();">New</A>&nbsp;<A id="lnkAdd" href="javascript:AddFolder();">Add</A></TD>
												</TR>
											</TABLE>
										</TD>
									</TR>
									<TR>
										<TD style="BORDER-RIGHT: #d00000 1px solid; BORDER-LEFT: #d00000 1px solid; BORDER-BOTTOM: #d00000 1px solid">
											<TABLE class="transparentnopadding" cellSpacing="0" cellPadding="0" border="0">
												<TR>
													<TD style="HEIGHT: 9px" align="right" colSpan="3">Cabinets:&nbsp;
														<ASP:DROPDOWNLIST id="ddlCabinets" CssClass="Inputbox" Runat="server" Width="160px"></ASP:DROPDOWNLIST></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 32px" vAlign="top"><IMG height="6" src="images/spacer.gif" width="1"><BR>
														Available&nbsp;Folders
														<ASP:LISTBOX id="lstAvailable" runat="server" CssClass="InputBox" Width="160px" Height="216px"></ASP:LISTBOX></TD>
													<TD style="PADDING-RIGHT: 4px; PADDING-LEFT: 4px"><INPUT id="btnAdd" onclick="MoveDocument(1);" type="button" value=" > "><BR>
														&nbsp;<BR>
														<INPUT id="btnRemove" onclick="MoveDocument(-1);" type="button" value=" < ">
													</TD>
													<TD vAlign="top">
														<TABLE class="transparentnopadding" cellSpacing="0" cellPadding="0" border="0">
															<TR height="6">
																<TD colSpan="2"><IMG height="6" src="images/spacer.gif" width="1"></TD>
															</TR>
															<TR>
																<TD rowSpan="2">Assigned&nbsp;Folders<BR>
																	<ASP:LISTBOX id="lstAssigned" runat="server" CssClass="InputBox" Width="160px" Height="216px"></ASP:LISTBOX></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
											</TABLE>
										</TD>
										<TD>&nbsp;</TD>
										<TD style="BORDER-RIGHT: #d00000 1px solid; BORDER-LEFT: #d00000 1px solid; BORDER-BOTTOM: #d00000 1px solid"
											vAlign="top"><INPUT id="txtFolderId" type="hidden">
											<TABLE class="transparent" cellSpacing="0" cellPadding="0" width="100%" border="0">
												<TR>
													<TD>Folder&nbsp;Name:&nbsp;</TD>
													<TD width="100%"><ASP:TEXTBOX id="txtFolderName" CssClass="InputBox" Runat="server" Width="100%"></ASP:TEXTBOX></TD>
													<TD>&nbsp;</TD>
												</TR>
											</TABLE>
										</TD>
									</TR>
									<TR>
										<TD colSpan="2"><SPAN class="red" id="showMessage" runat="server"></SPAN></TD>
										<TD align="right"><ASP:LINKBUTTON id="btnSave" Runat="server">Save</ASP:LINKBUTTON></TD>
									</TR>
								</TBODY>
							</TABLE>
							<INPUT id="hiddenCabinetFolders" type="hidden" runat="server"> <INPUT id="hiddenFolders" type="hidden" runat="server">
						</TD>
						<TD class="RightContent" width="160">&nbsp;</TD>
					</TR>
					<TR height="1">
						<TD style="BACKGROUND-COLOR: #959394" width="160"><IMG height="1" src="images/spacer.gif" width="160"></TD>
						<TD style="BACKGROUND-COLOR: #959394" width="100%"><IMG height="1" src="images/spacer.gif" width="100%"></TD>
						<TD style="BACKGROUND-COLOR: #959394" width="80"><IMG height="1" src="images/spacer.gif" width="80"></TD>
					</TR>
				</TBODY>
			</TABLE>
			<SCRIPT language="javascript">
		        document.all.docHeader_btnSearch.src = "./Images/toolbar_search_white.gif";
		        document.all.docHeader_btnIndex.src = "./Images/toolbar_index_white.gif";
		        document.all.docHeader_btnScan.src = "./Images/toolbar_scan_white.gif";
		        document.all.docHeader_btnOptions.src = "./Images/toolbar_options_grey.gif";
			</SCRIPT>
			<SCRIPT language="javascript">
				var blnFolderId = -1;
				var selectedFolderId = 0;
				var blnDirtyData = false;
				var hostName;
			
				function AddFolder()
				{
					var ddlCabinets = document.all.ddlCabinets;
					if(ddlCabinets.selectedIndex >= 0)
					{
						var strFolderName = document.all.txtFolderName.value;
						var intFolderId = -1
						if(document.all.lstAssigned.selectedIndex < 0 && document.all.lstAvailable.selectedIndex < 0)
						{
							document.all.lstAvailable.options[document.all.lstAvailable.options.length] = new Option(strFolderName, intFolderId);
							document.all.showMessage.innerHTML = "Folder added successfully.";
							blnDirtyData = true;
						}
						else 
						{
							if (document.all.lstAssigned.selectedIndex >= 0)
								document.all.lstAssigned.options[document.all.lstAssigned.selectedIndex].text = strFolderName;
							else if (document.all.lstAvailable.selectedIndex >= 0)
								document.all.lstAvailable.options[document.all.lstAvailable.selectedIndex].text = strFolderName;
							document.all.showMessage.innerHTML = "Document updated successfully.";
							blnDirtyData = true;
						}
					}
				}

				function FolderSelected(listBox)
				{
					var strText = listBox.options[listBox.selectedIndex].text;
					var intValue = listBox.options[listBox.selectedIndex].value;
					document.all.txtFolderId.value = intValue;
					document.all.txtFolderName.value = strText;
					if(listBox.id == "lstAvailable")
					{
						document.all.lstAssigned.selectedIndex = -1;
					}
					else
					{
						document.all.lstAvailable.selectedIndex = -1;
					}
					document.all.lnkNew.style.display = "inline";
					document.all.lnkAdd.innerHTML = "Change";
					UpdateButtons();

				}

				function ChangeCabinets()
				{
					var result;
					var ddlCabinets = document.all.ddlCabinets;
					if(ddlCabinets.selectedIndex >= 0)
					{
						if(blnDirtyData)
							result = confirm("All changes will be lost.  Continue?");
						else
							result = true;
							
						if(result)
						{
							// Load the new folder type
							Clear();
							Disable();
							svcGetFolders.onServiceAvailable = GetCabinetFolders(ddlCabinets.options[ddlCabinets.selectedIndex].value);
						}
					} 
					else
					{
						clear();
						Disable();
					}
				}
				
				function Clear()
				{
						document.all.lnkAdd.innerHTML = "Add";
						document.all.txtFolderId.value = -1;
						document.all.txtFolderName.value = "";
						document.all.lnkNew.style.display = "none";
						document.all.lstAvailable.selectedIndex = -1;
						document.all.lstAssigned.selectedIndex = -1;
						UpdateButtons();
						
				}

				function Disable()
				{
					document.all.txtFolderName.disabled = true;
					document.all.lnkNew.disabled = true;
					document.all.lnkAdd.disabled = true;
				}

				function Enable()
				{
					document.all.txtFolderName.disabled = false;
					document.all.lnkNew.disabled = false;
					document.all.lnkAdd.disabled = false;
				}
			
				
				function GetDocumentDetail(FolderId)
				{
					Disable();
					
				}
				
				function GetCabinetFolders(cabinetId)
				{
					Disable();
					svcGetFolders.Admin.callService("CabinetFolders", cabinetId);
				}
				
				function MoveDocument(direction)
				{
					var intItemValue;
					var strItemText;
					var lstAvailable = document.all.lstAvailable;
					var lstAssigned  = document.all.lstAssigned;
					var newOption    = document.createElement("OPTION")
					
					if(lstAvailable.selectedIndex >= 0 && direction == 1)
					{
						strItemValue = lstAvailable.options[lstAvailable.options.selectedIndex].text;
						intItemValue = lstAvailable.options[lstAvailable.options.selectedIndex].value;
						lstAvailable.remove(lstAvailable.options.selectedIndex);
						
						lstAssigned.options.add(newOption);
						newOption.innerText = strItemValue;
						newOption.value = intItemValue;
						lstAssigned.selectedIndex = lstAssigned.options.length-1;
					}
					else if (lstAssigned.selectedIndex >= 0 && direction == -1)
					{
						strItemValue = lstAssigned.options[lstAssigned.options.selectedIndex].text;
						intItemValue = lstAssigned.options[lstAssigned.options.selectedIndex].value;
						lstAssigned.remove(lstAssigned.options.selectedIndex);
						
						lstAvailable.options.add(newOption);
						newOption.innerText = strItemValue;
						newOption.value = intItemValue;
						lstAvailable.selectedIndex = lstAvailable.options.length-1;
					}
					UpdateButtons();
					blnDirtyData = true;
				}
				
				function onGetCabinetResult()
				{
					// Check for error
					if (event.result.error)
					{
						//Pull the error information from the event.result.errorDetail properties
						var xfaultcode		= event.result.errorDetail.code;
						var xfaultstring	= event.result.errorDetail.string;
						var xfaultsoap		= event.result.errorDetail.raw;
									
						// Add code to handle specific codes here
						alert("Error: (" + xfaultcode + ") " + xfaultstring + "\n" + xfaultsoap);
					}
					else if((!event.result.error))
					{
						var lstAvailable = document.all.lstAvailable;
						var lstAssigned = document.all.lstAssigned;
						var sXQLAssigned = "/CabinetFolders/Assigned";
						var sXQLAvailable = "/CabinetFolders/Available";
						var oXml = new ActiveXObject("MSXML2.DOMDocument"); //document.all.itemsDocuments;
						oXml.async = false;
						oXml.loadXML(event.result.value);
						
						// Remove the existing options
						for (var intOption = lstAvailable.options.length-1; intOption >=0; intOption--)
							lstAvailable.options.remove(intOption);
						for (var intOption = lstAssigned.options.length-1; intOption >=0; intOption--)
							lstAssigned.options.remove(intOption);

						//query for items to add
						var oNodes = oXml.selectNodes(sXQLAssigned);
						for (var i=0; i<=oNodes.length-1; i++)
						{
							var oOption = document.createElement("OPTION");
							var sFolderId = oNodes.item(i).childNodes(0).text;
							var sFolderName = oNodes.item(i).childNodes(1).text;
							lstAssigned.options.add(oOption, i);
							oOption.value = sFolderId;
							oOption.text = sFolderName;
						}
						lstAvailable.selectedIndex = -1;
						
						//query for items to add
						var oNodes = oXml.selectNodes(sXQLAvailable);
						for (var i=0; i<=oNodes.length-1; i++)
						{
							var oOption = document.createElement("OPTION");
							var sFolderId = oNodes.item(i).childNodes(0).text;
							var sFolderName = oNodes.item(i).childNodes(1).text;
							lstAvailable.options.add(oOption, i);
							oOption.value = sFolderId;
							oOption.text = sFolderName;
						}
						lstAvailable.selectedIndex = -1;
						Enable();
						UpdateButtons();
					}
				}	
				
				function onGetResult()
				{
					// Check for error
					if (event.result.error)
					{
						//Pull the error information from the event.result.errorDetail properties
						var xfaultcode		= event.result.errorDetail.code;
						var xfaultstring	= event.result.errorDetail.string;
						var xfaultsoap		= event.result.errorDetail.raw;
									
						// Add code to handle specific codes here
						alert("Error: (" + xfaultcode + ") " + xfaultstring);
					}
					else if((!event.result.error))
					{
						var list;
						var oXml = new ActiveXObject("MSXML2.DOMDocument"); //document.all.itemsAttributes;
						oXml.async = false;
						oXml.loadXML(event.result.value);
						var sXql = "/Attributes/Attributes";
						
						if (document.all.lstAssigned.selectedIndex >= 0)
							list		= document.all.lstAssigned;
						else
							list		= document.all.lstAvailable;
						FolderName	= document.all.txtFolderName;
						FolderId     = document.all.txtFolderId;
						
					

					}
				}

				function onUpdateResult()
				{
					// Check for error
					if((event.result.error))
					{
						//Pull the error information from the event.result.errorDetail properties
						var xfaultcode		= event.result.errorDetail.code;
						var xfaultstring	= event.result.errorDetail.string;
						var xfaultsoap		= event.result.errorDetail.raw;
									
						// Add code to handle specific codes here
						alert("Error: (" + xfaultcode + ") " + xfaultstring);
					}
					else if(!event.result.error)
					{
						var intFolderId = -1 //event.result.value;
						var strFolderName = Form1.txtFolderName.value;

						document.all.lnkAdd.disabled = false;
						document.all.txtFolderName.disabled = false;
						
						if(document.all.lstAssigned.selectedIndex < 0 && document.all.lstAvailable.selectedIndex < 0)
						{
							document.all.lstAvailable.options[document.all.lstAvailable.options.length] = new Option(strFolderName, intFolderId);
							document.all.showMessage.innerHTML = "Document added successfully.";
						}
						else 
						{
							if (document.all.lstAssigned.selectedIndex >= 0)
								document.all.lstAssigned.options[document.all.lstAssigned.selectedIndex].text = strFolderName;
							else if (document.all.lstAvailable.selectedIndex >= 0)
								document.all.lstAvailable.options[document.all.lstAvailable.selectedIndex].text = strFolderName;
							document.all.showMessage.innerHTML = "Document updated successfully.";
						}
						
						Clear();
						UpdateButtons();
						blnDirtyData = true;
					}
				}	
				
				function SelectAllAssigned()
				{
					var lstAssigned = document.all.lstAssigned;
					lstAssigned.onClick = "";
					lstAssigned.multiple = true;
					var selectedItems = new Array();
					for(var i=0; i<lstAssigned.options.length; i++)
					{	
						selectedItems[i] = lstAssigned.options[i].value + ";" + lstAssigned.options[i].text;
					}
					document.all.hiddenCabinetFolders.value = selectedItems;
					
					var lstAvailable = document.all.lstAvailable;
					selectedItems = new Array();
					for(var i=0; i<lstAvailable.options.length; i++)
					{
						selectedItems[i] = lstAvailable.options[i].value + ";" + lstAvailable.options[i].text;
					}
					document.all.hiddenFolders.value = selectedItems;
				}

				function SetupServices()
				{
						svcGetFolders.useService(hostName + "/UserAdmin.asmx?wsdl", "Admin");
						svcGet.useService(hostName + "/UserAdmin.asmx?wsdl", "Properties");
						svcUpdate.useService(hostName + "/UserAdmin.asmx?wsdl", "Update");
						
						if(document.all.ddlCabinets.selectedIndex > 0)
						{
							ChangeFolders();
						}
				}
	
				function ShowConfiguration()
				{
					document.all.ConfigurationLinks.style.display='block';
					document.all.lnkShowFolders.innerHTML = document.all.lnkShowFolders.innerHTML + "<IMG src='images//arrow_left.gif' border=0>";
					hostName = document.all.hostname.value; 
					
				}
				
				function UpdateDocument()
				{
					//svcUpdate.Update.callService("AddAttribute", strAttributeId, strAttributeName, strDataType, strDataFormat, strLength, strList);
				}

				function UpdateButtons()
				{
					var lstAvailable = document.all.lstAvailable;
					var lstAssigned = document.all.lstAssigned;
					var btnAdd = document.all.btnAdd;
					var btnRemove = document.all.btnRemove;
					
					btnAdd.disabled = true;
					btnRemove.disabled = true;
										
					if(lstAvailable.selectedIndex >= 0)
					{
						btnAdd.disabled = false;
					}
					else if(lstAssigned.selectedIndex >=0)
					{
						btnRemove.disabled = false;
					}
				}
			</SCRIPT>
		</FORM>
	</BODY>
</HTML>

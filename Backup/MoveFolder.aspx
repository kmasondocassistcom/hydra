<%@ Page Language="vb" AutoEventWireup="false" Codebehind="MoveFolder.aspx.vb" Inherits="docAssistWeb.MoveFolder"%>
<%@ Register TagPrefix="componentart" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Choose Target Folder</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
		<base target="_self">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server" class="PageContent">
			<table cellpadding="0" cellspacing="0" border="0" height="630" width="240">
				<tr>
					<td vAlign="top" class="PageContent"><br>
						Select a destination. The selected folder and any child folders will be moved.
						<DIV id="divTree" style="OVERFLOW: auto; WIDTH: 239px; HEIGHT: 557px; TEXT-ALIGN: right">
							<componentart:treeview id="treeSearch" runat="server" Width="100%" HoverNodeCssClass="NodeHover" SelectedNodeCssClass="SelectedNode"
								NodeRowCssClass="TreeFont" ShowLines="True" LineImagesFolderUrl="images/lines/" AutoScroll="False"
								CollapseNodeOnSelect="False" ItemSpacing="0" ClientSideOnNodeSelect="NodeClick"></componentart:treeview></DIV>
					</td>
				</tr>
				<tr>
					<td align="right">
						<asp:ImageButton id="btnMove" runat="server" ImageUrl="Images/button_ok.gif"></asp:ImageButton></td>
				</tr>
			</table>
			<input type="hidden" id='FolderId' runat="server">
			<script language="javascript">
			
				function NodeClick(node)
				{
					document.getElementById('FolderId').value = node.ID;
				}
				
			</script>
		</form>
	</body>
</HTML>

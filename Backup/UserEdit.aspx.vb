Imports Accucentric.docAssist.Data.Images
Imports System.Data.SqlClient

Partial Class UserEdit
    Inherits System.Web.UI.Page

    Private mobjUser As Accucentric.docAssist.Web.Security.User
    Private mconSqlMaster As SqlClient.SqlConnection
    Private mconSqlCompany As SqlClient.SqlConnection

    Protected WithEvents AdminRow As System.Web.UI.HtmlControls.HtmlTableRow

    Private intUserId As Integer

    Dim Mode As SaveMode

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Enum Table
        HEADER = 0
        AVAILABLE_GROUPS = 1
        ASSIGNED_GROUPS = 2
    End Enum

    Private Enum SaveMode
        NEW_USER = 0
        UPDATE_USER = 1
        COPY_USER = 2
    End Enum

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        SavePassword()

        'TODO: Pass Userid in session instead of query string.
        chkGenPw.Attributes.Add("onclick", "generatePassword(this);")

        Try

            Functions.CheckTimeout(Me.Page)

            'Page scripting
            txtEmailAddress.Attributes("onkeypress") = "ignoreEvent();"
            txtPassword.Attributes("onkeypress") = "checkForce();ignoreEvent();"
            txtPasswordConfirm.Attributes("onkeypress") = "ignoreEvent();"
            txtPhoneNumber.Attributes("onkeypress") = "ignoreEvent();"
            txtFirstName.Attributes("onkeypress") = "ignoreEvent();"
            txtLastName.Attributes("onkeypress") = "ignoreEvent();"
            ddlUserType.Attributes("onkeypress") = "ignoreEvent();"
            chkForcePasswordChange.Attributes("onkeypress") = "ignoreEvent();"
            chkUserEnabled.Attributes("onkeypress") = "ignoreEvent();"

            intUserId = Request.QueryString("ID")

            Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
            Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))
            Me.mobjUser.AccountId = guidAccountId
            Me.mconSqlCompany = Functions.BuildConnection(Me.mobjUser)

            If Not (Me.mobjUser.SecAdmin = True Or Me.mobjUser.SysAdmin = True) Then
                'ReturnScripts.Add("")
                Exit Sub
            End If

            If intUserId = 0 Then
                System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.Empty.ToString, "document.all.ReadOnlyEmail.style.display='none';", True)
            Else
                System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.Empty.ToString, "document.all.ReadOnlyEmail.style.display='block';", True)
            End If

            If Not Page.IsPostBack Then

                Dim objUsers As New SECURITY(Functions.BuildConnection(Me.mobjUser))

                If Functions.ModuleSetting(Functions.ModuleCode.WORKFLOW, mobjUser) = True Then
                    Dim lstItem As New ListItem("Workflow Only", "1")
                    ddlUserType.Items.Add(lstItem)
                End If

                If Request.QueryString("Mode") = "Copy" Then

                    Dim ds As New DataSet
                    ds = objUsers.UserGetById(intUserId)

                    txtEmailAddress.Text = ""
                    System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.Empty.ToString, "document.all.txtPassword.value='';document.all.txtPasswordConfirm.value='';", True)
                    chkUserEnabled.Checked = True 'ds.Tables(Table.HEADER).Rows(0)("Enabled")
                    txtPhoneNumber.Text = ds.Tables(Table.HEADER).Rows(0)("ContactNo")
                    ddlUserType.SelectedValue = ds.Tables(Table.HEADER).Rows(0)("UserType")

                    lstAvailable.DataTextField = "GroupName"
                    lstAvailable.DataValueField = "GroupId"
                    lstAvailable.DataSource = ds.Tables(Table.AVAILABLE_GROUPS)
                    lstAvailable.DataBind()

                    lstAssigned.DataTextField = "GroupName"
                    lstAssigned.DataValueField = "GroupId"
                    lstAssigned.DataSource = ds.Tables(Table.ASSIGNED_GROUPS)

                    lstAssigned.DataBind()

                    chkGenPw.Visible = True
                    System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.Empty.ToString, "document.all.ReadOnlyEmail.style.display='NONE';", True)
                    
                Else

                    Select Case intUserId
                        Case 0 'New User

                            Dim ds As New DataSet
                            ds = objUsers.UserGetById(intUserId)

                            lstAvailable.DataTextField = "GroupName"
                            lstAvailable.DataValueField = "GroupId"
                            lstAvailable.DataSource = ds.Tables(Table.AVAILABLE_GROUPS)
                            lstAvailable.DataBind()

                            lstAssigned.DataTextField = "GroupName"
                            lstAssigned.DataValueField = "GroupId"
                            lstAssigned.DataSource = ds.Tables(Table.ASSIGNED_GROUPS)
                            lstAssigned.DataBind()

                            chkGenPw.Visible = True
                            chkUserEnabled.Checked = True

                            System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.Empty.ToString, "document.all.ReadOnlyEmail.style.display='NONE';", True)

                        Case Else 'Load User

                            Dim ds As New DataSet
                            ds = objUsers.UserGetById(intUserId)

                            txtEmailAddress.Text = ds.Tables(Table.HEADER).Rows(0)("EMailAddress")
                            txtEmailAddress.Visible = False
                            System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.Empty.ToString, "document.all.ReadOnlyEmail.innerHTML='" & ds.Tables(Table.HEADER).Rows(0)("EMailAddress") & "';", True)
                            'Load dummy passwords

                            System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.Empty.ToString, "document.all.txtPassword.value='***********';document.all.txtPasswordConfirm.value='***********';", True)

                            chkUserEnabled.Checked = ds.Tables(Table.HEADER).Rows(0)("Enabled")
                            txtPhoneNumber.Text = ds.Tables(Table.HEADER).Rows(0)("ContactNo")
                            txtFirstName.Text = ds.Tables(Table.HEADER).Rows(0)("FirstName")
                            txtLastName.Text = ds.Tables(Table.HEADER).Rows(0)("LastName")
                            ddlUserType.SelectedValue = ds.Tables(Table.HEADER).Rows(0)("UserType")

                            lstAvailable.DataTextField = "GroupName"
                            lstAvailable.DataValueField = "GroupId"
                            lstAvailable.DataSource = ds.Tables(Table.AVAILABLE_GROUPS)
                            lstAvailable.DataBind()

                            lstAssigned.DataTextField = "GroupName"
                            lstAssigned.DataValueField = "GroupId"
                            lstAssigned.DataSource = ds.Tables(Table.ASSIGNED_GROUPS)
                            lstAssigned.DataBind()

                    End Select

                End If

            End If

            If Me.mobjUser.EditionID = AccountEdition.SMALL_BUSINESS_EDITION Then
                groupSection.Visible = False
                chkForcePasswordChange.Style("DISPLAY") = "none"
                chkGenPw.Visible = False
                ddlUserType.Visible = False
                UserTypeLabel.Visible = False
            End If

        Catch ex As Exception

            Dim conMaster As SqlClient.SqlConnection
            conMaster = Functions.BuildMasterConnection
            Functions.LogException(mobjUser.UserId.ToString, mobjUser.AccountId.ToString, "", "UserEdit Page Load Error: " & ex.Message, ex.StackTrace, Server.MachineName, "", Request.UrlReferrer.ToString, conMaster, True)

        Finally

            If mconSqlCompany.State <> ConnectionState.Closed Then
                mconSqlCompany.Close()
                mconSqlCompany.Dispose()
            End If

        End Try

    End Sub

    Private Sub btnAddAll_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddAll.Click

        Try
            For Each lstItem As ListItem In lstAvailable.Items
                Dim NewItem As New ListItem(lstItem.Text, lstItem.Value)
                lstAssigned.Items.Add(NewItem)
            Next

            lstAvailable.Items.Clear()

        Catch ex As Exception

        Finally

        End Try

    End Sub

    Private Sub btnAddOne_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddOne.Click

        Dim dtMove As New DataTable
        dtMove.Columns.Add(New DataColumn("GroupId", GetType(System.Int32), Nothing, MappingType.Element))
        dtMove.Columns.Add(New DataColumn("GroupName", GetType(System.String), Nothing, MappingType.Element))

        Dim dtKeep As New DataTable
        dtKeep.Columns.Add(New DataColumn("GroupId", GetType(System.Int32), Nothing, MappingType.Element))
        dtKeep.Columns.Add(New DataColumn("GroupName", GetType(System.String), Nothing, MappingType.Element))

        Try
            For X As Integer = 0 To lstAvailable.Items.Count - 1
                If lstAvailable.Items(X).Selected = True Then
                    Dim dr As DataRow
                    dr = dtMove.NewRow
                    dr("GroupId") = lstAvailable.Items(X).Value
                    dr("GroupName") = lstAvailable.Items(X).Text
                    dtMove.Rows.Add(dr)
                Else
                    Dim dr As DataRow
                    dr = dtKeep.NewRow
                    dr("GroupId") = lstAvailable.Items(X).Value
                    dr("GroupName") = lstAvailable.Items(X).Text
                    dtKeep.Rows.Add(dr)
                End If
            Next

            For Each lstItem As ListItem In lstAssigned.Items
                Dim dr As DataRow
                dr = dtMove.NewRow
                dr("GroupId") = lstItem.Value
                dr("GroupName") = lstItem.Text
                dtMove.Rows.Add(dr)
            Next

            lstAvailable.Items.Clear()
            lstAssigned.Items.Clear()

            lstAvailable.DataSource = dtKeep
            lstAvailable.DataValueField = "GroupId"
            lstAvailable.DataTextField = "GroupName"
            lstAvailable.DataBind()

            lstAssigned.DataSource = dtMove
            lstAssigned.DataValueField = "GroupId"
            lstAssigned.DataTextField = "GroupName"
            lstAssigned.DataBind()

            If lstAssigned.Items.Count > 0 Then
                lstAssigned.Items(0).Selected = True
            End If

            If lstAvailable.Items.Count > 0 Then
                lstAvailable.Items(0).Selected = True
            End If

        Catch ex As Exception

        Finally

            'ReturnControls.Add(lstAssigned)
            'ReturnControls.Add(lstAvailable)

        End Try

    End Sub

    Private Sub btnRemoveAll_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnRemoveAll.Click
        Try
            For Each lstItem As ListItem In lstAssigned.Items
                Dim NewItem As New ListItem(lstItem.Text, lstItem.Value)
                lstAvailable.Items.Add(NewItem)
            Next

            lstAssigned.Items.Clear()

        Catch ex As Exception

        Finally

            'ReturnControls.Add(lstAssigned)
            'ReturnControls.Add(lstAvailable)

        End Try
    End Sub

    Private Sub btnRemoveOne_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnRemoveOne.Click

        Dim dtMove As New DataTable
        dtMove.Columns.Add(New DataColumn("GroupId", GetType(System.Int32), Nothing, MappingType.Element))
        dtMove.Columns.Add(New DataColumn("GroupName", GetType(System.String), Nothing, MappingType.Element))

        Dim dtKeep As New DataTable
        dtKeep.Columns.Add(New DataColumn("GroupId", GetType(System.Int32), Nothing, MappingType.Element))
        dtKeep.Columns.Add(New DataColumn("GroupName", GetType(System.String), Nothing, MappingType.Element))

        Try
            For X As Integer = 0 To lstAssigned.Items.Count - 1
                If lstAssigned.Items(X).Selected = True Then
                    Dim dr As DataRow
                    dr = dtMove.NewRow
                    dr("GroupId") = lstAssigned.Items(X).Value
                    dr("GroupName") = lstAssigned.Items(X).Text
                    dtMove.Rows.Add(dr)
                Else
                    Dim dr As DataRow
                    dr = dtKeep.NewRow
                    dr("GroupId") = lstAssigned.Items(X).Value
                    dr("GroupName") = lstAssigned.Items(X).Text
                    dtKeep.Rows.Add(dr)
                End If
            Next

            For Each lstItem As ListItem In lstAvailable.Items
                Dim dr As DataRow
                dr = dtMove.NewRow
                dr("GroupId") = lstItem.Value
                dr("GroupName") = lstItem.Text
                dtMove.Rows.Add(dr)
            Next

            lstAvailable.Items.Clear()
            lstAssigned.Items.Clear()

            lstAvailable.DataSource = dtMove
            lstAvailable.DataValueField = "GroupId"
            lstAvailable.DataTextField = "GroupName"
            lstAvailable.DataBind()

            lstAssigned.DataSource = dtKeep
            lstAssigned.DataValueField = "GroupId"
            lstAssigned.DataTextField = "GroupName"
            lstAssigned.DataBind()

            If lstAssigned.Items.Count > 0 Then
                lstAssigned.Items(0).Selected = True
            End If

            If lstAvailable.Items.Count > 0 Then
                lstAvailable.Items(0).Selected = True
            End If

        Catch ex As Exception

        Finally

            'ReturnControls.Add(lstAssigned)
            'ReturnControls.Add(lstAvailable)

        End Try

    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click

        Dim sqlTranMaster As SqlClient.SqlTransaction
        Dim sqlTranCompany As SqlClient.SqlTransaction

        Dim objEncryption As New Encryption.Encryption

        Dim cTmp As New SqlConnection

        Try

            mconSqlMaster = New SqlClient.SqlConnection
            mconSqlCompany = New SqlClient.SqlConnection

            Select Case intUserId
                Case -1
                    Mode = SaveMode.COPY_USER
                Case 0
                    Mode = SaveMode.NEW_USER
                Case Else
                    Mode = SaveMode.UPDATE_USER
            End Select

            Dim blnErrorFound As Boolean = False

            'Check for required fields
            If Mode = SaveMode.NEW_USER Or Request.QueryString("Mode") = "Copy" Then
                If txtEmailAddress.Text.Trim.Length = 0 Then
                    System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.Empty.ToString, "document.all.EMailAddressLabel.className='ErrorText';", True)
                    blnErrorFound = True
                Else
                    System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.Empty.ToString, "document.all.EMailAddressLabel.className='Text';", True)
                End If
            End If

            If txtPassword.Text.Trim.Length = 0 Then
                System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.Empty.ToString, "document.all.PasswordLabel.className='ErrorText';", True)
                blnErrorFound = True
            Else
                System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.Empty.ToString, "document.all.PasswordLabel.className='Text';", True)
            End If

            If txtPasswordConfirm.Text.Trim.Length = 0 Then
                System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.Empty.ToString, "document.all.PasswordConfirmLabel.className='ErrorText';", True)
                blnErrorFound = True
            Else
                System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.Empty.ToString, "document.all.PasswordConfirmLabel.className='Text';", True)
            End If

            If txtFirstName.Text.Trim.Length = 0 Then
                System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.Empty.ToString, "document.all.FirstNameLabel.className='ErrorText';", True)
                blnErrorFound = True
            Else
                System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.Empty.ToString, "document.all.FirstNameLabel.className='Text';", True)
            End If

            If txtLastName.Text.Trim.Length = 0 Then
                System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.Empty.ToString, "document.all.LastNameLabel.className='ErrorText';", True)
                blnErrorFound = True
            Else
                System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.Empty.ToString, "document.all.LastNameLabel.className='Text';", True)
            End If

            If blnErrorFound Then
                lblError.Text = "Please provide the required fields."
                lblError.Visible = True
                Exit Sub
            Else
                lblError.Text = ""
                lblError.Visible = False
            End If

            If Mode = SaveMode.NEW_USER Or Request.QueryString("Mode") = "Copy" Then

                'Check for valid e-mail address
                If CheckValidEmailAddress(txtEmailAddress.Text) = False Then
                    System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.Empty.ToString, "document.all.EMailAddressLabel.className='ErrorText';", True)
                    lblError.Text = "Please provide a valid e-mail address."
                    lblError.Visible = True
                    Exit Sub
                Else
                    System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.Empty.ToString, "document.all.EMailAddressLabel.className='Text';", True)
                    lblError.Text = ""
                    lblError.Visible = False
                End If

                'Validate password
                If Not chkGenPw.Checked Then
                    If txtPassword.Text <> txtPasswordConfirm.Text Then
                        System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.Empty.ToString, "document.all.PasswordLabel.className='ErrorText';document.all.PasswordConfirmLabel.className='ErrorText';", True)
                        txtPassword.Text = ""
                        txtPasswordConfirm.Text = ""
                        lblError.Text = "Passwords must match."
                        lblError.Visible = True
                        Exit Sub
                    Else
                        System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.Empty.ToString, "document.all.PasswordLabel.className='Text';document.all.PasswordConfirmLabel.className='Text';", True)
                        lblError.Text = ""
                        lblError.Visible = False
                    End If

                    'Strength
                    If Functions.PasswordStrongValidation(txtPassword.Text) = False Then
                        System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.Empty.ToString, "document.all.PasswordLabel.className='ErrorText';document.all.PasswordConfirmLabel.className='ErrorText';", True)
                        lblError.Text = Functions.PasswordStrongValidationMessage
                        lblError.Visible = True
                        Exit Sub
                    Else
                        System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.Empty.ToString, "document.all.PasswordLabel.className='Text';document.all.PasswordConfirmLabel.className='Text';", True)
                        lblError.Text = ""
                        lblError.Visible = False
                    End If

                End If

            End If

            'Open SQL connections to master and company databases.
            mconSqlMaster = Functions.BuildConnection(Functions.ConnectionType.Master)
            mconSqlCompany = Functions.BuildConnection(Me.mobjUser)
            mconSqlCompany.Open()

            'Process save
            If Mode = SaveMode.NEW_USER Or Request.QueryString("Mode") = "Copy" Then

                sqlTranMaster = mconSqlMaster.BeginTransaction

                If chkGenPw.Checked Then
                    Dim randomPassword As String = Functions.GeneratePassword
                    txtPassword.Text = GeneratePassword()
                    txtPasswordConfirm.Text = GeneratePassword()
                End If

                'Insert user to master database
                Dim objUsers As New Security(mconSqlMaster, False, sqlTranMaster)
                Dim strWebUserId As String

                If Me.mobjUser.EditionID = AccountEdition.SMALL_BUSINESS_EDITION Then
                    strWebUserId = objUsers.UserMasterInsert(Me.mobjUser.AccountId.ToString, txtEmailAddress.Text, txtFirstName.Text, txtLastName.Text, txtPhoneNumber.Text, _
                    objEncryption.Encrypt(txtPassword.Text), chkUserEnabled.Checked, False, Mode, Me.mobjUser.UserId.ToString, False)
                Else
                    strWebUserId = objUsers.UserMasterInsert(Me.mobjUser.AccountId.ToString, txtEmailAddress.Text, txtFirstName.Text, txtLastName.Text, txtPhoneNumber.Text, _
                    objEncryption.Encrypt(txtPassword.Text), chkUserEnabled.Checked, chkForcePasswordChange.Checked, Mode, Me.mobjUser.UserId.ToString, chkForcePasswordChange.Checked)
                End If

                If strWebUserId = "-1" Then
                    System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.Empty.ToString, "document.all.EMailAddressLabel.className='ErrorText';", True)
                    lblError.Text = "This user already exists."
                    lblError.Visible = True
                    sqlTranMaster.Rollback()
                    Exit Sub
                ElseIf strWebUserId.Length < 36 Then
                    System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.Empty.ToString, "document.all.EMailAddressLabel.className='ErrorText';", True)
                    lblError.Text = "An error has occured while saving. Error Code: 9579"
                    lblError.Visible = True
                    sqlTranMaster.Rollback()
                    Exit Sub
                End If

                'Insert user to company database
                sqlTranCompany = mconSqlCompany.BeginTransaction
                objUsers = New Security(mconSqlCompany, False, sqlTranCompany)

                Dim intUserIdResult As Integer = objUsers.UserCompanyInsertUpdate(strWebUserId, ddlUserType.SelectedValue)

                If intUserIdResult < 1 Then
                    lblError.Text = "An error has occured while saving. Error Code: 9580"
                    lblError.Visible = True
                    sqlTranCompany.Rollback()
                    Exit Sub
                End If

                'Collect users
                Dim dt As New DataTable
                dt.Columns.Add("UserID")
                dt.Columns.Add("GroupID")
                For Each lstItem As ListItem In lstAssigned.Items
                    Dim dr As DataRow
                    dr = dt.NewRow
                    dr("UserID") = intUserIdResult
                    dr("GroupID") = lstItem.Value
                    dt.Rows.Add(dr)
                Next

                If Me.mobjUser.EditionID = AccountEdition.SMALL_BUSINESS_EDITION Then
                    ' Add user to admin group for small business edition
                    Dim dr As DataRow
                    dr = dt.NewRow
                    dr("UserID") = intUserIdResult
                    dr("GroupID") = 1
                    dt.Rows.Add(dr)
                End If

                cTmp = Functions.BuildConnection(Me.mobjUser)
                cTmp.Open()

                Dim tempTableName As String = "##" & System.Guid.NewGuid.ToString.Replace("-", "")
                Dim sqlCmd As New SqlCommand("CREATE TABLE " & tempTableName & " (UserID int, GroupID int)", cTmp)
                sqlCmd.ExecuteNonQuery()

                sqlCmd = New SqlCommand("SELECT * FROM " & tempTableName, cTmp)
                Dim sqlDa As New SqlDataAdapter(sqlCmd)
                Dim sqlBld As New SqlCommandBuilder(sqlDa)

                sqlDa.Update(dt)

                sqlTranMaster.Commit()
                sqlTranCompany.Commit()

                'If Not Me.mobjUser.EditionID = AccountEdition.SMALL_BUSINESS_EDITION Then

                Dim grpUpdate As New Security(cTmp, True)

                'Update groups
                grpUpdate.UserGroupMemberInsert(tempTableName, objUsers.USER_EDIT, intUserId)

                Try
                    Dim params As New Collection
                    params.Add(New EmailParameter("FULLNAME", txtFirstName.Text & " " & txtLastName.Text))
                    params.Add(New EmailParameter("EMAIL", txtEmailAddress.Text.ToLower))
                    params.Add(New EmailParameter("PASSWORD", txtPassword.Text))

                    If Not IsNothing(Session("CustomLogo")) Then
                        Dim customGuid As String = Session("CustomLogo")
                        params.Add(New EmailParameter("LOGINURL", "https://my.docassist.com/Default.aspx?LID=" & customGuid.Replace("_banner.gif", "")))
                    Else
                        params.Add(New EmailParameter("LOGINURL", "https://my.docassist.com/Default.aspx?"))
                    End If

                    Functions.sendEmailMessage(Functions.EmailType.NEW_USER, txtEmailAddress.Text.ToLower, "", "", params, Me)
                Catch ex As Exception
                    Functions.LogException(mobjUser.UserId.ToString, mobjUser.AccountId.ToString, "", "New User Email Notification Failure: " & ex.Message, ex.StackTrace, Server.MachineName, "", Request.UrlReferrer.ToString, Functions.BuildMasterConnection, True)
                End Try

                'End If

            ElseIf Mode = SaveMode.UPDATE_USER Then

                sqlTranMaster = mconSqlMaster.BeginTransaction

                'Check if user has updated the password
                Dim strUpdatePassword As String
                If txtPassword.Text <> "***********" Then
                    strUpdatePassword = txtPassword.Text
                Else
                    strUpdatePassword = ""
                End If

                If strUpdatePassword <> "" Then

                    'Validate password
                    If txtPassword.Text <> txtPasswordConfirm.Text Then
                        System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.Empty.ToString, "document.all.PasswordLabel.className='ErrorText';document.all.PasswordConfirmLabel.className='ErrorText';", True)
                        txtPassword.Text = ""
                        txtPasswordConfirm.Text = ""
                        lblError.Text = "Passwords must match."
                        lblError.Visible = True
                        Exit Sub
                    Else
                        System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.Empty.ToString, "document.all.PasswordLabel.className='Text';document.all.PasswordConfirmLabel.className='Text';", True)
                        lblError.Text = ""
                        lblError.Visible = False
                    End If

                    'Strength
                    If Functions.PasswordStrongValidation(txtPassword.Text) = False Then
                        System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.Empty.ToString, "document.all.PasswordLabel.className='ErrorText';document.all.PasswordConfirmLabel.className='ErrorText';", True)
                        lblError.Text = Functions.PasswordStrongValidationMessage
                        lblError.Visible = True
                        Exit Sub
                    Else
                        System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.Empty.ToString, "document.all.PasswordLabel.className='Text';document.all.PasswordConfirmLabel.className='Text';", True)
                        lblError.Text = ""
                        lblError.Visible = False
                    End If

                End If

                'Insert user to master database
                Dim objUsers As New Security(mconSqlMaster, False, sqlTranMaster)
                Dim strWebUserId As String

                If Me.mobjUser.EditionID = AccountEdition.SMALL_BUSINESS_EDITION Then
                    strWebUserId = objUsers.UserMasterInsert(Me.mobjUser.AccountId.ToString, txtEmailAddress.Text, txtFirstName.Text, txtLastName.Text, txtPhoneNumber.Text, _
                    IIf(strUpdatePassword = "", "", objEncryption.Encrypt(strUpdatePassword)), chkUserEnabled.Checked, False, SaveMode.UPDATE_USER, Me.mobjUser.UserId.ToString, False)
                Else
                    strWebUserId = objUsers.UserMasterInsert(Me.mobjUser.AccountId.ToString, txtEmailAddress.Text, txtFirstName.Text, txtLastName.Text, txtPhoneNumber.Text, _
                    IIf(strUpdatePassword = "", "", objEncryption.Encrypt(strUpdatePassword)), chkUserEnabled.Checked, chkForcePasswordChange.Checked, SaveMode.UPDATE_USER, Me.mobjUser.UserId.ToString, chkForcePasswordChange.Checked)
                End If

                If strWebUserId = "-1" Then
                    System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.Empty.ToString, "document.all.EMailAddressLabel.className='ErrorText';", True)
                    lblError.Text = "This user already exists."
                    lblError.Visible = True
                    sqlTranMaster.Rollback()
                    Exit Sub
                ElseIf strWebUserId.Length < 36 Then
                    System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.Empty.ToString, "document.all.EMailAddressLabel.className='ErrorText';", True)
                    lblError.Text = "An error has occured while saving. Error Code: 9579"
                    lblError.Visible = True
                    sqlTranMaster.Rollback()
                    Exit Sub
                End If

                'Insert user to company database
                sqlTranCompany = mconSqlCompany.BeginTransaction
                objUsers = New Security(mconSqlCompany, False, sqlTranCompany)

                Dim intUserIdResult As Integer = objUsers.UserCompanyInsertUpdate(strWebUserId, ddlUserType.SelectedValue)

                If intUserIdResult < 1 Then
                    lblError.Text = "An error has occured while saving. Error Code: 9580"
                    lblError.Visible = True
                    sqlTranCompany.Rollback()
                    Exit Sub
                End If

                sqlTranMaster.Commit()
                sqlTranCompany.Commit()

                If Not Me.mobjUser.EditionID = AccountEdition.SMALL_BUSINESS_EDITION Then

                    'Collect users
                    Dim dt As New DataTable
                    dt.Columns.Add("UserID")
                    dt.Columns.Add("GroupID")
                    For Each lstItem As ListItem In lstAssigned.Items
                        Dim dr As DataRow
                        dr = dt.NewRow
                        dr("UserID") = intUserIdResult
                        dr("GroupID") = lstItem.Value
                        dt.Rows.Add(dr)
                    Next

                    cTmp = Functions.BuildConnection(Me.mobjUser)
                    cTmp.Open()

                    Dim tempTableName As String = "##" & System.Guid.NewGuid.ToString.Replace("-", "")
                    Dim sqlCmd As New SqlCommand("CREATE TABLE " & tempTableName & " (UserID int, GroupID int)", cTmp)
                    sqlCmd.ExecuteNonQuery()

                    sqlCmd = New SqlCommand("SELECT * FROM " & tempTableName, cTmp)
                    Dim sqlDa As New SqlDataAdapter(sqlCmd)
                    Dim sqlBld As New SqlCommandBuilder(sqlDa)

                    sqlDa.Update(dt)


                    Dim grpUpdate As New Security(cTmp, True)

                    'Update groups
                    grpUpdate.UserGroupMemberInsert(tempTableName, objUsers.USER_EDIT, intUserId)

                End If

            End If

            'litOut.Text = "<script>window.returnValue = '1';window.close();</script>"
            'System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.Empty.ToString, "", True)
            System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, "Close", "window.returnValue = '1';window.close();", True)

        Catch ex As Exception

            sqlTranMaster.Rollback()
            sqlTranCompany.Rollback()

            lblError.Text = "An error has occured while saving. If the problem continues contact support."
            lblError.Visible = True

            Dim conMaster As SqlClient.SqlConnection
            conMaster = Functions.BuildMasterConnection
            Functions.LogException(mobjUser.UserId.ToString, mobjUser.AccountId.ToString, "", "User Edit: " & ex.Message, ex.StackTrace, Server.MachineName, "", Request.UrlReferrer.ToString, conMaster, True)

        Finally

            'ReturnControls.Add(lblError)
            'ReturnControls.Add(txtPassword)
            'ReturnControls.Add(txtPasswordConfirm)

            If cTmp.State <> ConnectionState.Closed Then
                cTmp.Close()
                cTmp.Dispose()
            End If

            If mconSqlMaster.State <> ConnectionState.Closed Then
                mconSqlMaster.Close()
                mconSqlMaster.Dispose()
            End If

            If mconSqlCompany.State <> ConnectionState.Closed Then
                mconSqlCompany.Close()
                mconSqlCompany.Dispose()
            End If

        End Try

    End Sub

    Private Sub SavePassword()
        Dim Password As String = txtPassword.Text
        txtPassword.Attributes.Add("value", Password)
        Dim PasswordConfirm As String = txtPasswordConfirm.Text
        txtPasswordConfirm.Attributes.Add("value", PasswordConfirm)
    End Sub

End Class
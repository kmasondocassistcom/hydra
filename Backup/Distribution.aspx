<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Distribution.aspx.vb"
    Inherits="docAssistWeb.Distribution" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>G/L Account Distribution</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <link href="Styles.css" type="text/css" rel="stylesheet">
</head>
<body bottommargin="0" leftmargin="0" topmargin="0" rightmargin="0">
    <form id="frmGL" method="post" runat="server">
    <table width="100%">
        <tr>
            <td>GL/Account
            </td>
            <td>Description
            </td>
            <td>Amount
            </td>
            <td>Comment
            </td>
        </tr>
        <tr>
            <td><input type=text /></td>
            <td><input type=text /></td>
            <td><input type=text /></td>
            <td><input type=text /></td>
        </tr>
        <tr>
            <td><input type=text /></td>
            <td><input type=text /></td>
            <td><input type=text /></td>
            <td><input type=text /></td>
        </tr>
        <tr>
            <td><input type=text /></td>
            <td><input type=text /></td>
            <td><input type=text /></td>
            <td><input type=text /></td>
        </tr>
    </table>
    </form>
</body>
</html>

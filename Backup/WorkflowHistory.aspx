<%@ Register TagPrefix="cc1" Namespace="OptGroupLists" Assembly="OptGroupLists" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="WorkflowHistory.aspx.vb" Inherits="docAssistWeb.WorkflowHistory"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title></title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<table id="tblSubmit" height="250" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TBODY>
					<tr vAlign="top">
						<td vAlign="top" width="100%"><asp:label id="lblWorkflowName" runat="server" Width="100%" Font-Bold="True" Font-Size="8pt"></asp:label><BR>
							<asp:datagrid id="dgHistory" runat="server" Width="100%" ShowHeader="False" AutoGenerateColumns="False"
								PageSize="150" GridLines="None">
								<AlternatingItemStyle BackColor="White"></AlternatingItemStyle>
								<ItemStyle BackColor="#E0E0E0"></ItemStyle>
								<Columns>
									<asp:TemplateColumn>
										<ItemTemplate>
											<TABLE borderColor="#0" cellSpacing="0" cellPadding="0">
												<TBODY>
													<TR vAlign="top" width="100%">
														<TD vAlign="top" align="left" width="100%"><asp:label id=lblUserName runat="server" Font-Bold="False" Font-Size="8pt" Font-Names="Verdana" Text='<%# DataBinder.Eval(Container, "DataItem.UserName") %>'>
															</asp:label></TD>
														<TD vAlign="top" align="right" width="100%"><asp:label id=lblAction runat="server" Font-Bold="True" Font-Size="8pt" Font-Names="Verdana" Text='<%# DataBinder.Eval(Container, "DataItem.StatusDesc") %>' Visible="True">
															</asp:label></TD>
						</td>
					</tr>
				</TBODY>
			</table>
			<asp:label id=lblDateTime runat="server" Width="100%" Font-Size="8pt" Font-Names="Verdana" Text='<%# DataBinder.Eval(Container, "DataItem.ActionDateTime") %>' Visible="True">
			</asp:label>
			<asp:label id=lblNote runat="server" BorderStyle="Solid" Font-Size="8pt" Font-Names="Verdana" Text='<%# DataBinder.Eval(Container, "DataItem.Note") %>' Visible="True" BorderWidth="0px">
			</asp:label><asp:label id=lblRoutedTo runat="server" Width="100%" Font-Size="8pt" Font-Names="Verdana" Text='<%# DataBinder.Eval(Container, "DataItem.QueueName") %>' Visible="True" Font-Italic="True"></asp:label>
			</ItemTemplate>
			<EditItemTemplate>
				<BR>
			</EditItemTemplate>
			</asp:TemplateColumn> </Columns> </asp:datagrid></TD></TR>
			<tr>
				<td height="100%"></td>
			</tr>
			</TBODY></TABLE><asp:literal id="litAction" runat="server"></asp:literal><asp:literal id="litQueue" runat="server"></asp:literal><BR>
			<asp:literal id="litUrgent" runat="server"></asp:literal><asp:literal id="litRoute" runat="server"></asp:literal></form>
		<script language="javascript" src="scripts/cookies.js">
			var resultbln = readCookie("results");
			if (resultbln == "1") { window.history.back(); }
		</script>
		<script language="javascript">
			function reloadPage()
			{
				window.location.reload();
			}
		</script>
		<script language="javascript" src="scripts/cookies.js"></script>
		<script language="javascript">
			//document.body.overflow = 'auto';
			var resultbln = readCookie("results");
			if (resultbln == "1") { window.history.back(); }
		</script>
	</body>
</HTML>

<%@ Page Language="vb" AutoEventWireup="false" Codebehind="PrintDocument.aspx.vb" Inherits="docAssistWeb.PrintDocument"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>docAssist - Print Document</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body>
		<script language="javascript" src="scripts/print.js">
			
		</script>
		<form id="Form1" method="post" runat="server">
			<table cellpadding="0" cellspacing="0" width="350" height="125">
				<tr>
					<TD colSpan="2"><IMG height="1" src="spacer.gif" width="15">
						<asp:Label id="lblPrintOptions" runat="server" Font-Names="Verdana" Font-Size="8.25pt">Please select printing options.</asp:Label><BR>
					</TD>
					<td>
					</td>
				</tr>
				<tr>
					<TD><img src="spacer.gif" height="1" width="35"></TD>
					<td width="100%">
						<asp:CheckBox id="chkAnnotations" runat="server" Text="Include Annotations" Font-Names="Verdana"
							Font-Size="8.25pt"></asp:CheckBox><BR>
						<asp:CheckBox id="chkRedactions" runat="server" Text="Remove Redactions" Font-Names="Verdana"
							Font-Size="8.25pt"></asp:CheckBox>
					</td>
				</tr>
				<tr>
					<TD></TD>
					<td align="right"><a href="#"><img src="Images/btn_cancel.gif" border="0" onclick="window.returnValue=undefined;window.close();"></a>&nbsp;<a href="#"><img src="Images/button_ok.gif" border="0" onclick="printResult();"></a>&nbsp;
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>

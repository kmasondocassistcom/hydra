Partial Class Integration
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Response.Expires = 0
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        Try

            Dim objUser As New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
            Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))
            objUser.AccountId = guidAccountId

        Catch ex As Exception

            Response.Redirect("Default.aspx" & Request.Url.Query)

        End Try

        Try
            If Request.QueryString("A") = "SavedSearch" Then

                'Generate password
                Dim alphabet() As String = {"A", "a", "B", "b", "C", "D", "d", "E", "e", "F", "f", "G", "g", "H", "h", "J", "j", "L", "K", "k", "M", "m", "N", _
                "n", "P", "p", "Q", "q", "R", "r", "S", "s", "T", "t", "U", "u", "V", "v", "W", "w", "X", "x", "Y", "y", "Z", "z"}
                Dim numbers() As String = {"2", "3", "4", "5", "6", "7", "8", "9"}

                Dim rnd As New Random
                Dim random As String = String.Empty
                For i As Integer = 0 To 3
                    random += alphabet(rnd.Next(0, 46))
                    random += numbers(rnd.Next(0, 8))
                Next

                Response.Redirect("Search.aspx" & Request.Url.Query & "&" & random)

            End If

            If CInt(Request.QueryString("DID")) = 0 Then
                Session("IntegrationSearch") = True
                Session("IntegrationDocumentID") = Request.QueryString("DID")

                For x As Integer = 0 To Request.QueryString.Count
                    If Request.QueryString.AllKeys(x).StartsWith("AV") Then
                        Dim strValue As String = Request.QueryString(x)
                        Session("IntegrationAttributeValue") = strValue
                        Exit For
                    End If
                Next

                litGo.Text = "<script language='javascript'>top.location='Search.aspx';</script>"
                Exit Sub
            End If

            If Request.QueryString("A") = "S" And Not Page.IsPostBack Then
                Dim sArray As String() = Request.QueryString.AllKeys
                Session("IntegrationSearch") = True
                Session("IntegrationIntID") = Request.QueryString("IID")
                Session("IntegrationDocumentID") = Request.QueryString("DID")
                Session("IntegrationAttributeID") = Replace(sArray(3), "AV", "")
                Session("IntegrationAttributeValue") = Request.QueryString(3).Trim
                litGo.Text = "<script language='javascript'>top.location='Search.aspx';</script>"
            End If

            If Request.QueryString("A") = "I" Then
                Session("IndexDocumentID") = Request.QueryString("DID")
                Server.Transfer("DocumentManager.aspx" & Request.Url.Query)
            End If

        Catch ex As Exception

        End Try

    End Sub

End Class

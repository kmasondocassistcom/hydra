Imports Accucentric.docAssist
Imports System.Text.RegularExpressions
Imports Accucentric.docAssist.Data.Images

Partial Class Print
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Dim intPageCount As Integer
    Dim strFromName As String
    Dim strFromEmail As String


    Private mcookieSearch As cookieSearch
    Private mobjUser As Accucentric.docAssist.Web.Security.User
    Private mconSqlImage As SqlClient.SqlConnection
    Private blnAnnotation As Boolean = False
    Private blnRedaction As Boolean = False

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Response.Expires = 0
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        Try
            Me.mobjUser = BuildUserObject(Request.Cookies("docAssist"))
            Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
            Me.mcookieSearch = New cookieSearch(Me.Page)
            Me.mconSqlImage.Open()

            If Me.mcookieSearch.VersionID = -1 Then
                Me.mcookieSearch.VersionID = Request.QueryString("VID")
            End If

        Catch ex As Exception
            If ex.Message = "User is not logged on" Then
                'TODO: Close window
                Response.Redirect("Default.aspx")
            End If
        End Try

        Try
            txtRangeFrom.Attributes.Add("OnKeypress", "return nb_numericOnly(event);")
            txtRangeTo.Attributes.Add("OnKeypress", "return nb_numericOnly(event);")
            imgExport.Attributes("onclick") = "disable();"
        Catch ex As Exception

        End Try

        Try
            'Annotation security
            DocumentAnnotationSecurityLevel(Me.mobjUser.ImagesUserId, Me.mcookieSearch.VersionID, Me.mconSqlImage, blnAnnotation, blnRedaction)

            If blnAnnotation = False Then
                chkAnnotations.Visible = False
                rowAnnotations.Visible = False
            End If

            If blnRedaction = False Then
                chkRedactions.Visible = False
                rowRedaction.Visible = False
            End If

        Catch ex As Exception

        End Try

        If Not Page.IsPostBack Then

            Dim daImages As New docAssistImages
            Dim Request As HttpRequest = Context.Request
            Dim intVersionId As Integer

            'Get VersionId of the image to e-mail
            intVersionId = Me.mcookieSearch.VersionID
            txtRangeTo.Text = daImages.PageCount(intVersionId)

            Request = Nothing
            daImages = Nothing

        Else
            hidPostCount.Value = CInt(hidPostCount.Value + 1)
            page_body.Attributes("onload") = "javascript:if(document.all.rdoAllPages.checked){rdoPages_click(document.all.rdoAllPages);}else{rdoPages_click(document.all.rdoRange);}"
        End If

    End Sub

    Private Function ValidatePage(Optional ByVal blnExport As Boolean = False) As Boolean

        Try

            'Check page ranges.
            If rdoRange.Checked Then

                Dim dtImages As New docAssistImages
                Dim Request As HttpRequest = Context.Request
                Dim intVersionId As Integer
                Dim intPageCounter As Integer

                'Get VersionId of the current e-mail.
                intVersionId = Me.mcookieSearch.VersionID

                intPageCounter = dtImages.PageCount(intVersionId)

                If (txtRangeTo.Text < 1) Or (txtRangeFrom.Text > intPageCounter) Or (txtRangeTo.Text > intPageCounter) Or _
                (txtRangeFrom.Text > txtRangeTo.Text) Then
                    lblPageError.Text = "Invalid page range."
                    Return False
                Else
                    lblPageError.Text = ""
                    Return True
                End If

                Request = Nothing
                dtImages = Nothing
            Else
                Return True
            End If

        Catch Ex As Exception
            lblPageError.Text = "Unexpected error: " & Ex.Message
            Return False
        End Try

    End Function

    Private Sub imgExport_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgExport.Click

        Try

            If ValidatePage(True) = True Then


                Dim Request As HttpRequest = Context.Request
                Dim intCounter As Integer
                Dim intVersionId As Integer
                Dim strAttachment As String

                'Get VersionId of the image to e-mail
                intVersionId = Me.mcookieSearch.VersionID

                'Build the image
                Dim daImages As New docAssistImages
                intPageCount = daImages.PageCount(intVersionId)
                If rdoAllPages.Checked Then
                    strAttachment = ExportPDF(Me.mobjUser, intVersionId, 1, intPageCount, Me, chkAnnotations.Checked)
                Else
                    If txtRangeTo.Text > intPageCount Then
                        txtRangeTo.Text = intPageCount
                        Exit Sub
                    End If
                    strAttachment = ExportPDF(Me.mobjUser, intVersionId, txtRangeFrom.Text, txtRangeTo.Text, Me, chkAnnotations.Checked)
                End If

                Response.Redirect("Export.aspx?File=" & Request.Url.GetLeftPart(UriPartial.Path).Replace("Print.aspx", "") & "tmp/" & strAttachment.Replace("\", "/"))

                daImages = Nothing

            End If

        Catch ex As Exception
            lblPageError.Text = "An error has occured."
        Finally

        End Try

    End Sub

End Class
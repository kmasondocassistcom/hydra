<%@ Page Language="vb" AutoEventWireup="false" Codebehind="PasswordExpired.aspx.vb" Inherits="docAssistWeb.PasswordExpired"%>
<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="Controls/MenuBar.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<TITLE></TITLE>
		<META content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<META content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<META content="JavaScript" name="vs_defaultClientScript">
		<META content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="css/core.css" type="text/css" rel="stylesheet">
	</HEAD>
	<BODY bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0" onload="document.all.txtCurrent.focus();">
		<FORM id="frmPwChange" method="post" runat="server">
			<h4>Before you can continue you need to change your password.
			</h4>
			<p><asp:Label id="lblReq" runat="server"></asp:Label></p>
			<label>Current Password</label><br>
			<input type="password" id="txtCurrent" runat="server" size="40" tabIndex="1"><br>
			<label>New Password</label><br>
			<input type="password" id="txtConfirmPass" runat="server" size="40" tabIndex="2"><br>
			<label>Confirm Password</label><br>
			<input type="password" id="txtNewPass" runat="server" size="40" tabIndex="3">
			<p align="right">
				<asp:label id="lblMsg" runat="server" CssClass="errorLabel"></asp:label>
				<BUTTON class="btn primary" id="btnChange" type="button" runat="server"><SPAN><SPAN>Change 
							Password</SPAN></SPAN></BUTTON>
			</p>
		</FORM>
		<script language="javascript">
			function submitForm(){
				if (event.keyCode == 13){
					event.cancelBubble = true;
					event.returnValue = false;
					document.all.btnChange.click();
				}
			}
		</script>
	</BODY>
</HTML>

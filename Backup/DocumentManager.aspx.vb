Partial Class DocumentManager
    Inherits System.Web.UI.Page

    Private mobjUser As Accucentric.docAssist.Web.Security.User


#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents file As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents litLpkLicense As System.Web.UI.WebControls.Literal
    Protected WithEvents litUnlockImageCtrl As System.Web.UI.WebControls.Literal

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load


        Functions.CheckTimeout(Me.Page)

        Dim sbLoadMessage As New System.Text.StringBuilder
        sbLoadMessage.Append("<SPAN id=LoadMessage>" & vbCr)

        sbLoadMessage.Append("<style type=""text/css"">" & vbCr)
        sbLoadMessage.Append("<!--" & vbCr)
        sbLoadMessage.Append(".style1 {" & vbCr)
        sbLoadMessage.Append("color: #FFFFFF;" & vbCr)
        sbLoadMessage.Append("font-family: Verdana, Arial, Helvetica, sans-serif;" & vbCr)
        sbLoadMessage.Append("font-size: 24px;" & vbCr)
        sbLoadMessage.Append("}" & vbCr)
        sbLoadMessage.Append(".style2 {" & vbCr)
        sbLoadMessage.Append("font-family: ""Trebuchet MS"", Verdana, Arial, sans-serif;" & vbCr)
        sbLoadMessage.Append("font-size: 10px;" & vbCr)
        sbLoadMessage.Append("}" & vbCr)
        sbLoadMessage.Append("-->" & vbCr)
        sbLoadMessage.Append("</style>" & vbCr)
        sbLoadMessage.Append("</head>" & vbCr)
        sbLoadMessage.Append("<body>" & vbCr)
        sbLoadMessage.Append("<table width=""100%"" height=""100%""  border=""0"" cellpadding=""120"" cellspacing=""0"">" & vbCr)
        sbLoadMessage.Append("<tr>" & vbCr)
        sbLoadMessage.Append("<td align=""center"" valign=""middle""><table width=""404"" border=""0"" cellpadding=""2"" cellspacing=""0"" bgcolor=""444444"">" & vbCr)
        sbLoadMessage.Append("<tr>" & vbCr)
        sbLoadMessage.Append("<td><table width=""400"" border=""0"" cellspacing=""0"" cellpadding=""20"">" & vbCr)
        sbLoadMessage.Append("<tr>" & vbCr)
        sbLoadMessage.Append("<td bgcolor=""BE0000""><div align=""center""><span class=""style1"">Loading... Please wait. </span></div></td>" & vbCr)
        sbLoadMessage.Append("</tr>" & vbCr)
        sbLoadMessage.Append(" <tr>" & vbCr)
        sbLoadMessage.Append("<td height=""2"" bgcolor=""#FFFFFF""></td>" & vbCr)
        sbLoadMessage.Append("</tr>" & vbCr)
        sbLoadMessage.Append("<tr>" & vbCr)
        sbLoadMessage.Append("<td height=""15"" bgcolor=""#666666""></td>" & vbCr)
        sbLoadMessage.Append("</tr>" & vbCr)
        sbLoadMessage.Append(" </table></td>" & vbCr)
        sbLoadMessage.Append("</tr>" & vbCr)
        sbLoadMessage.Append(" </table></td>" & vbCr)
        sbLoadMessage.Append("</tr></table>" & vbCr)
        sbLoadMessage.Append("</SPAN>")


        Dim objDecrypt As New Encryption.Encryption
        Dim strWebServiceUrl As String
        Try

            Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
            Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))
        Catch ex As Exception
            If Request.Url.Query.Trim.Length > 0 Then
                Response.Redirect("Default.aspx" & Request.Url.Query)
            Else
                Server.Transfer("Default.aspx")
            End If
        End Try

        Response.Write(sbLoadMessage.ToString)
        Dim loaderPage As String = System.Reflection.Assembly.GetExecutingAssembly().CodeBase.ToLower.Replace(".dll", ".aspx")
        'DirectCast(Request.Create(loaderPage), HttpRequest)()
        'Dim request2 As System.Net.HttpWebRequest = DirectCast(System.Net.WebRequest.Create(loaderPage), System.Net.HttpWebRequest)
        System.Net.WebRequest.Create(loaderPage)

        Dim conSqlWeb As New SqlClient.SqlConnection

        Try
            Dim sqlConMaster As New SqlClient.SqlConnection
            sqlConMaster.ConnectionString = objDecrypt.Decrypt(ConfigurationSettings.AppSettings("Connection"))
            sqlConMaster.Open()
            Dim cmdSqlWebSvc As New SqlClient.SqlCommand("acsOptionValuebyKey", sqlConMaster)
            cmdSqlWebSvc.Parameters.Add("@OptionKey", "DocMgrWebService")
            cmdSqlWebSvc.CommandTimeout = DEFAULT_COMMAND_TIMEOUT
            cmdSqlWebSvc.CommandType = CommandType.StoredProcedure
            strWebServiceUrl = cmdSqlWebSvc.ExecuteScalar
            sqlConMaster.Close()
            conSqlWeb.Close()
            conSqlWeb.Dispose()
        Catch ex As Exception

        Finally
            If conSqlWeb.State <> ConnectionState.Closed Then
                conSqlWeb.Close()
                conSqlWeb.Dispose()
            End If
        End Try


        Try
            'Check if user should be loading this page
            Dim dtAccess As New DataTable
            dtAccess = Functions.UserAccess(Functions.BuildConnection(mobjUser), mobjUser.UserId, mobjUser.AccountId)

            If dtAccess.Rows(0)("Index") = True Then
                'url.Value = Session("strWebServiceUrl")
                Dim sbConfig As New System.Text.StringBuilder
                sbConfig.Append(Now().ToString & ";")
                sbConfig.Append(Me.mobjUser.UserId.ToString & ";")
                sbConfig.Append(Me.mobjUser.AccountId.ToString & ";")
                sbConfig.Append(strWebServiceUrl & ";")
                sbConfig.Append(ConfigurationSettings.AppSettings("SaveServiceUrl"))
                uid.Value = objDecrypt.Encrypt(sbConfig.ToString).Replace("+", " ").Replace("/", "�")
            Else
                SignOut(Me)

            End If
        Catch ex As Exception

            If ex.Message = "DualLogin" Then
                Response.Cookies("docAssist")("DualLogin") = True
                Server.Transfer("Default.aspx")
                Exit Sub
            Else

            End If

            If Request.QueryString("PURL").Trim.Length > 0 Then
                Server.Transfer("Default.aspx?PURL=" & Request.QueryString("PURL"))
                Exit Sub
            Else
                Server.Transfer("Default.aspx")
            End If

            Try
                If Request.QueryString("A") = "I" Then
                    Server.Transfer("Default.aspx?PURL=" & Request.QueryString("PURL"))
                    Exit Sub
                Else
                    Server.Transfer("Default.aspx")
                End If

            Catch exception As Exception

            End Try

        End Try


        Try
            Dim strFileName As String = Request.QueryString("File").Replace("~", "+").Replace("�", "/")
            If strFileName.Trim.Length > 0 Then
                'Dim strArray As String = Request.QueryString("File") '.Replace(" ", "+").Replace("�", "/")
                filenm.Value = strFileName
                loadoption.Value = "1"
                Exit Sub
            End If
        Catch ex As Exception

        End Try

        Try
            Dim strPrintDriverFile As String = Request.QueryString("PURL")

            If strPrintDriverFile.Trim.Length > 0 Then

                If Functions.ModuleSetting(Functions.ModuleCode.PRINT_DRIVER, Me.mobjUser) Then

                    Dim sbIndexer As New System.Text.StringBuilder
                    sbIndexer.Append(Now().ToString & ";")
                    sbIndexer.Append(Me.mobjUser.UserId.ToString & ";")
                    sbIndexer.Append(Me.mobjUser.AccountId.ToString & ";")
                    sbIndexer.Append(strWebServiceUrl & ";")
                    sbIndexer.Append(ConfigurationSettings.AppSettings("SaveServiceUrl"))
                    Dim objEnc As New Encryption.Encryption
                    uid.Value = objEnc.Encrypt(sbIndexer.ToString)

                    purl.Value = strPrintDriverFile
                    loadoption.Value = "2"
                    Exit Sub

                Else
                    Functions.SignOut(Me, False)
                    Functions.ThrowLoginError(Me, ErrorCodes.ERROR_PRINT_ACCESS_DENIED)
                End If

            End If

        Catch ex As Exception

        End Try

        Dim sqlCon As New SqlClient.SqlConnection
        sqlCon = Functions.BuildConnection(Me.mobjUser)

        If sqlCon.State <> ConnectionState.Open Then sqlCon.Open()

        Dim intAttributeId As Integer
        Dim strAttributeValue As String

        Try
            If Request.QueryString("A") = "I" Then

                If Functions.ModuleSetting(Functions.ModuleCode.PRINT_DRIVER, Me.mobjUser) Then

                    loadoption.Value = "3"
                    Dim sbIndexer As New System.Text.StringBuilder
                    sbIndexer.Append(Now().ToString & ";")
                    sbIndexer.Append(Me.mobjUser.UserId.ToString & ";")
                    sbIndexer.Append(Me.mobjUser.AccountId.ToString & ";")
                    sbIndexer.Append(Request.QueryString("DID") & "%")

                    Dim strNames() As String = Request.QueryString.AllKeys
                    For x As Integer = 0 To strNames.Length - 1
                        If Left(strNames(x), 2) = "AV" Then

                            Dim sqlCmd As New SqlClient.SqlCommand("IntegrationAttributeIDGet", sqlCon)
                            sqlCmd.Parameters.Add("@IntegrationID", CInt(Request.QueryString("IID")))
                            sqlCmd.Parameters.Add("@FieldID", strNames(x).Replace("AV", ""))
                            sqlCmd.Parameters.Add("@Action", "I")
                            sqlCmd.Parameters.Add("@DocumentID", CInt(Request.QueryString("DID")))
                            sqlCmd.CommandType = CommandType.StoredProcedure
                            Dim daSql As New SqlClient.SqlDataAdapter(sqlCmd)
                            Dim dtAttributeId As New DataTable
                            daSql.Fill(dtAttributeId)

                            'Dim strAttributeID As String

                            If dtAttributeId.Rows.Count > 0 Then

                                Dim strExpression As String = dtAttributeId.Rows(0)("Expression")
                                intAttributeId = dtAttributeId.Rows(0)("AttributeID")

                                'strAttributeID = strNames(x).Replace("AV", "")
                                'Dim strAttributeValue As String = Request.QueryString(strNames(x))
                                If strExpression.Trim.Length > 0 Then
                                    strAttributeValue = Functions.VBS(strExpression, Request.QueryString(strNames(x)))
                                Else
                                    strAttributeValue = Request.QueryString(strNames(x))
                                End If

                            End If

                            sbIndexer.Append(intAttributeId.ToString & ";" & strAttributeValue & "|")

                        End If

                    Next

                    Dim objEnc As New Encryption.Encryption
                    Dim strIndexerQueryString As String = objEnc.Encrypt(sbIndexer.ToString.Remove(sbIndexer.ToString.Length - 1, 1))

                    filenm.Value = strIndexerQueryString

                Else
                    Functions.SignOut(Me, False)
                    Functions.ThrowLoginError(Me, ErrorCodes.ERROR_INTEGRATION_ACCESS_DENIED)
                End If

            End If

        Catch ex As Exception

        Finally
            If sqlCon.State <> ConnectionState.Closed Then sqlCon.Close()
        End Try

    End Sub

End Class

'Imports Accucentric.docAssist.Web
'Imports System.Web.SessionState

'Partial Class NewIndex
'    Inherits System.Web.UI.Page

'    Private mobjUser As Accucentric.docAssist.Web.Security.User
'    Protected WithEvents txtFileNameServer As System.Web.UI.WebControls.TextBox
'    Protected WithEvents lblFileNameClient As System.Web.UI.WebControls.Label
'    Protected WithEvents litScripts As System.Web.UI.WebControls.Literal
'    Protected WithEvents lblVersionId As System.Web.UI.HtmlControls.HtmlInputHidden
'    Protected WithEvents lblPageNo As System.Web.UI.HtmlControls.HtmlInputHidden
'    Protected WithEvents lblFileNameResult As System.Web.UI.HtmlControls.HtmlInputHidden
'    Protected WithEvents WorkflowTabs As System.Web.UI.HtmlControls.HtmlTableRow

'    Protected WithEvents navIndex As IndexNavigation

'    Private mconSqlImage As SqlClient.SqlConnection

'    Private mcookieImage As cookieImage
'    Private mcookieSearch As cookieSearch
'    Protected WithEvents txtUserId As System.Web.UI.WebControls.Label
'    Protected WithEvents tabThumbnails As System.Web.UI.HtmlControls.HtmlImage
'    Protected WithEvents dgDistribution As System.Web.UI.WebControls.DataGrid
'    Protected WithEvents Hidden1 As System.Web.UI.HtmlControls.HtmlInputHidden
'    Protected WithEvents Hidden2 As System.Web.UI.HtmlControls.HtmlInputHidden

'#Region " Web Form Designer Generated Code "

'    'This call is required by the Web Form Designer.
'    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

'    End Sub
'    Protected WithEvents Literal1 As System.Web.UI.WebControls.Literal
'    Protected WithEvents litUnlockImageCtrl As System.Web.UI.WebControls.Literal

'    'NOTE: The following placeholder declaration is required by the Web Form Designer.
'    'Do not delete or move it.
'    Private designerPlaceholderDeclaration As System.Object

'    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
'        'CODEGEN: This method call is required by the Web Form Designer
'        'Do not modify it using the code editor.
'        InitializeComponent()
'    End Sub

'#End Region

'    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

'        Response.Expires = 0
'        Response.Cache.SetNoStore()
'        Response.AppendHeader("Pragma", "no-cache")

'        lblAttributeError.Text = ""
'        lblAttributeError.Visible = False

'        Try

'            Functions.CheckTimeout(Me.Page)

'            Me.mobjUser = BuildUserObject(Request.Cookies("docAssist"))
'            Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)

'            txtUser.Text = Me.mobjUser.UserId.ToString

'        Catch ex As Exception

'            Select Case ex.Message
'                Case "DualLogin"
'                    Response.Cookies("docAssist")("DualLogin") = True
'                    If Request.Url.Query.Trim.Length > 0 Then
'                        Response.Redirect("Default.aspx" & Request.Url.Query)
'                    Else
'                        Server.Transfer("Default.aspx")
'                    End If
'                Case "User is not logged on"
'                    If Request.Url.Query.Trim.Length > 0 Then
'                        Response.Redirect("Default.aspx" & Request.Url.Query)
'                    Else
'                        Server.Transfer("Default.aspx")
'                    End If
'                Case "Thread was being aborted."
'                    If Request.Url.Query.Trim.Length > 0 Then
'                        Response.Redirect("Default.aspx" & Request.Url.Query)
'                    Else
'                        Server.Transfer("Default.aspx")
'                    End If
'            End Select
'        End Try

'        Me.mcookieSearch = New cookieSearch(Me)
'        Me.mcookieImage = New cookieImage(Me)

'        If Not Request.QueryString("VID") Is Nothing Then
'            Me.mcookieSearch.VersionID = CInt(Request.QueryString("VID"))
'        End If

'        Try
'            'Find current row
'            Dim dc As New DataColumn("VersionId")
'            Session("dtVersionId").PrimaryKey = New DataColumn() {Session("dtVersionId").Columns("VersionId")}
'            Dim tmpDr As DataRow = Session("dtVersionId").Rows.Find(Me.mcookieSearch.VersionID)
'            Session("intCurrentRow") = (tmpDr("RowID") + 1)
'            Session("intPrevNextVersionId") = Me.mcookieSearch.VersionID
'        Catch ex As Exception

'        End Try

'        Try

'            Dim strHostname As String = Request.Url.AbsoluteUri.Substring(0, Request.Url.AbsoluteUri.Length - Request.Url.AbsolutePath.Length - Request.Url.Query.Length)
'#If DEBUG Then
'            strHostname += "/docAssistWeb1"
'#End If
'            hostname.Value = strHostname
'            'ReturnControls.Add(hostname)

'            'Check if user is supposed to see this version, if not change the version id to the one they have access to and hide the version drop down
'            Dim intVersionId As Integer = Me.mcookieSearch.VersionID
'            Dim intRestrict As Integer
'            Dim intVersionIdReturned As Integer
'            Dim intImageType As Integer
'            Functions.UserVersionOnlyCheck(Me.mobjUser.ImagesUserId, intVersionId, intVersionIdReturned, intRestrict, Me.mconSqlImage, intImageType, New Integer, New Integer)

'            Session("FileType") = intImageType
'            ImageType.Value = intImageType

'            Session("ImageType") = CInt(CType(Session("FileType"), Functions.FileType))

'            Select Case CType(Session("FileType"), Functions.FileType)
'                Case Functions.FileType.ATTACHMENT
'                    ImageType.Value = "3"
'            End Select

'            If intRestrict = 0 Then
'                Me.mcookieSearch.HideVersionDropDownList = True
'            Else
'                Me.mcookieSearch.HideVersionDropDownList = False
'            End If

'            If intVersionId <> intVersionIdReturned Then
'                Me.mcookieSearch.VersionID = intVersionIdReturned
'                VersionId.Value = intVersionIdReturned
'            Else
'                VersionId.Value = intVersionId
'            End If

'            'Load image into session

'            Dim oVersionControl As New Accucentric.docAssist.Data.VersionControl(Me.mcookieSearch.VersionID, Me.mconSqlImage)
'            Session(intVersionId & "VersionControl") = oVersionControl

'            pageNo.Value = "1"
'            'ReturnControls.Add(pageNo)

'            accountId.Value = Me.mobjUser.AccountId.ToString
'            'ReturnControls.Add(accountId)

'            DownloadQuality.Value = Me.mobjUser.DefaultQuality
'            'ReturnControls.Add(DownloadQuality)

'            pageCount.Value = oVersionControl.ImageDetail.Rows.Count
'            'ReturnControls.Add(pageCount)

'            Select Case mobjUser.DefaultDocView
'                Case 0 'Best Fit
'                    objViewer.AutoZoom = Atalasoft.Imaging.WebControls.AutoZoomMode.BestFit
'                Case 1 ' Fit width
'                    objViewer.AutoZoom = Atalasoft.Imaging.WebControls.AutoZoomMode.FitToWidth
'                Case 2 'Fit Height
'                    objViewer.AutoZoom = Atalasoft.Imaging.WebControls.AutoZoomMode.FitToHeight
'                Case Else
'                    objViewer.AutoZoom = Atalasoft.Imaging.WebControls.AutoZoomMode.BestFit
'            End Select

'        Catch ex As Exception

'        Finally

'            Me.mconSqlImage.Close()
'            Me.mconSqlImage.Dispose()

'        End Try

'    End Sub

'    Private Function LoadImage()

'        navIndex.Visible = True

'        Dim intCurrentRouteId As Integer = 0
'        Dim wfMode As Functions.WorkflowMode

'        Dim strVersionId As String = Me.mcookieSearch.VersionID
'        Session(strVersionId & "WorkflowId") = Nothing
'        Session(strVersionId & "QueueId") = Nothing
'        Session(strVersionId & "Notes") = Nothing
'        Session(strVersionId & "Urgent") = Nothing
'        Session(strVersionId & "Route") = Nothing

'        If Not Page.IsPostBack Then
'            'Log recent item
'            Try
'                Dim blnReturn As Boolean = Functions.TrackRecentItem(Me.mobjUser, "VW", Me.mcookieSearch.VersionID)
'            Catch ex As Exception
'            End Try
'        End If

'        'Account level workflow
'        If Functions.ModuleSetting(Functions.ModuleCode.WORKFLOW, Me.mobjUser) Then

'            'Check if workflow console should be displayed
'            wfMode = CheckWorkflow(Me.mobjUser.ImagesUserId, Me.mcookieSearch.VersionID, Me.mobjUser, intCurrentRouteId)

'            Try
'                Session("WorkflowMode") = wfMode
'            Catch ex As Exception

'            End Try

'            tblWorkflow.Visible = True

'            Select Case wfMode
'                Case Functions.WorkflowMode.NO_ACCESS
'                    tblWorkflow.Style("visibility") = "hidden"
'                    frameWorkflow.Attributes("src") = "dummy.htm"
'                Case Functions.WorkflowMode.AUTHORIZED_SUBMITTER_NOT_SUBMITTED
'                    'tblWorkflow.Style("visibility") = "visible"
'                    ShowWorkflow.Value = "1"
'                    'tblWorkflow.Style("display") = "block"
'                    RegisterStartupScript("WFPane", "<script language='javascript'>document.all.tblWorkflow.style.display = 'block';</script>")
'                    WorkflowTabs.Style("display") = "none"
'                    frameWorkflow.Attributes("src") = "WorkflowSubmit.aspx?Mode=" & Functions.WorkflowMode.AUTHORIZED_SUBMITTER_NOT_SUBMITTED
'                    frameWorkflow.Attributes("scrolling") = "no"
'                Case Functions.WorkflowMode.VIEW_ACCESS_REVIEWER_PENDING
'                    'tblWorkflow.Style("visibility") = "visible"
'                    ShowWorkflow.Value = "1"
'                    RegisterStartupScript("WFPane", "<script language='javascript'>document.all.tblWorkflow.style.display = 'block';</script>")
'                    WorkflowTabs.Style("display") = "none"
'                    frameWorkflow.Attributes("src") = "WorkflowHistory.aspx"
'                    frameWorkflow.Attributes("scrolling") = "auto"
'                Case Functions.WorkflowMode.ACTION_ACCESS_REVIEWER_PENDING
'                    'tblWorkflow.Style("visibility") = "visible"
'                    ShowWorkflow.Value = "1"
'                    RegisterStartupScript("WFPane", "<script language='javascript'>document.all.tblWorkflow.style.display = 'block';</script>")
'                    WorkflowTabs.Style("display") = "block"
'                    frameWorkflow.Attributes("src") = "WorkflowReview.aspx?Mode=" & Functions.WorkflowMode.ACTION_ACCESS_REVIEWER_PENDING & "&Route=" & intCurrentRouteId
'                    frameWorkflow.Attributes("scrolling") = "no"
'                Case Functions.WorkflowMode.AUTHORIZED_SUBMITTER_SUBMITTED
'                    'tblWorkflow.Style("visibility") = "visible"
'                    ShowWorkflow.Value = "1"
'                    RegisterStartupScript("WFPane", "<script language='javascript'>document.all.tblWorkflow.style.display = 'block';</script>")
'                    WorkflowTabs.Style("display") = "none"
'                    frameWorkflow.Attributes("src") = "WorkflowHistory.aspx"
'                    frameWorkflow.Attributes("scrolling") = "auto"
'                Case Functions.WorkflowMode.REVIEWED
'                    'tblWorkflow.Style("visibility") = "visible"
'                    ShowWorkflow.Value = "1"
'                    RegisterStartupScript("WFPane", "<script language='javascript'>document.all.tblWorkflow.style.display = 'block';</script>")
'                    WorkflowTabs.Style("display") = "none"
'                    frameWorkflow.Attributes("src") = "WorkflowHistory.aspx"
'                    frameWorkflow.Attributes("scrolling") = "auto"
'                Case Functions.WorkflowMode.AUTHORIZED_SUBMITTER_RESUBMIT
'                    'tblWorkflow.Style("visibility") = "visible"
'                    ShowWorkflow.Value = "1"
'                    'tblWorkflow.Style("display") = "block"
'                    RegisterStartupScript("WFPane", "<script language='javascript'>document.all.tblWorkflow.style.display = 'block';</script>")
'                    frameWorkflow.Attributes("src") = "WorkflowSubmit.aspx?Mode=" & Functions.WorkflowMode.AUTHORIZED_SUBMITTER_NOT_SUBMITTED
'                    frameWorkflow.Attributes("scrolling") = "no"
'                Case Else
'                    tblWorkflow.Style("visibility") = "hidden"
'                    frameWorkflow.Attributes("src") = "dummy.htm"
'            End Select

'            'http://localhost/docAssistWeb1/Index.aspx?VID=1792&642

'        Else

'            'Account level workflow disabled, hide everything
'            tblWorkflow.Visible = True
'            tblWorkflow.Style("visibility") = "hidden"
'            frameWorkflow.Attributes("src") = "dummy.htm"

'            wfMode = Functions.WorkflowMode.NO_ACCESS 'Toolbar looks at this variable for permissions

'        End If

'        frameAttributes.Attributes.Item("src") = "Attributes.aspx"
'        frameAttributes.Attributes.Item("style") = "background-color: Transparent; background-image: none;"

'        'If Me.mcookieImage.ShowThumbnail Then
'        '    frameImage.Attributes.Item("src") = "ImageThumbnails.aspx"
'        'Else
'        '    If ImageType.Value = "3" Then
'        '        frameImage.Attributes("src") = "ViewAttachments.aspx"
'        '    Else
'        '        frameImage.Attributes.Item("src") = "ImageViewer.aspx"
'        '    End If
'        'End If

'        txtShowImage.Value = 1
'        litHideProgress.Visible = False

'    End Function

'    Protected Overrides Sub Finalize()

'        MyBase.Finalize()

'    End Sub

'    Private Sub btnWf_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnWf.Click

'        Select Case mode.Value
'            Case Functions.WorkflowMode.NO_ACCESS
'                tblWorkflow.Style("visibility") = "hidden"
'                frameWorkflow.Attributes("src") = "dummy.htm"
'            Case Functions.WorkflowMode.AUTHORIZED_SUBMITTER_NOT_SUBMITTED
'                ShowWorkflow.Value = "1"
'                ReturnScripts.Add("document.all.tblWorkflow.style.display = 'block';")
'                WorkflowTabs.Style("display") = "none"
'                frameWorkflow.Attributes("src") = "WorkflowSubmit.aspx?Mode=" & Functions.WorkflowMode.AUTHORIZED_SUBMITTER_NOT_SUBMITTED
'                frameWorkflow.Attributes("scrolling") = "no"
'            Case Functions.WorkflowMode.VIEW_ACCESS_REVIEWER_PENDING
'                ShowWorkflow.Value = "1"
'                ReturnScripts.Add("document.all.tblWorkflow.style.display = 'block';")
'                WorkflowTabs.Style("display") = "none"
'                frameWorkflow.Attributes("src") = "WorkflowHistory.aspx"
'                frameWorkflow.Attributes("scrolling") = "auto"
'            Case Functions.WorkflowMode.ACTION_ACCESS_REVIEWER_PENDING
'                ShowWorkflow.Value = "1"
'                ReturnScripts.Add("document.all.tblWorkflow.style.display = 'block';")
'                WorkflowTabs.Style("display") = "block"
'                frameWorkflow.Attributes("src") = "WorkflowReview.aspx?Mode=" & Functions.WorkflowMode.ACTION_ACCESS_REVIEWER_PENDING & "&Route=" & routeid.Value
'                frameWorkflow.Attributes("scrolling") = "no"
'            Case Functions.WorkflowMode.AUTHORIZED_SUBMITTER_SUBMITTED
'                ShowWorkflow.Value = "1"
'                ReturnScripts.Add("document.all.tblWorkflow.style.display = 'block';")
'                WorkflowTabs.Style("display") = "none"
'                frameWorkflow.Attributes("src") = "WorkflowHistory.aspx"
'                frameWorkflow.Attributes("scrolling") = "auto"
'            Case Functions.WorkflowMode.REVIEWED
'                ShowWorkflow.Value = "1"
'                ReturnScripts.Add("document.all.tblWorkflow.style.display = 'block';")
'                WorkflowTabs.Style("display") = "none"
'                frameWorkflow.Attributes("src") = "WorkflowHistory.aspx"
'                frameWorkflow.Attributes("scrolling") = "auto"
'            Case Functions.WorkflowMode.AUTHORIZED_SUBMITTER_RESUBMIT
'                ShowWorkflow.Value = "1"
'                ReturnScripts.Add("document.all.tblWorkflow.style.display = 'block';")
'                frameWorkflow.Attributes("src") = "WorkflowSubmit.aspx?Mode=" & Functions.WorkflowMode.AUTHORIZED_SUBMITTER_NOT_SUBMITTED
'                frameWorkflow.Attributes("scrolling") = "no"
'            Case Else
'                tblWorkflow.Style("visibility") = "hidden"
'                frameWorkflow.Attributes("src") = "dummy.htm"
'        End Select

'        'ReturnControls.Add(tblWorkflow)
'        'ReturnControls.Add(frameWorkflow)
'        'ReturnControls.Add(ShowWorkflow)
'        'ReturnControls.Add(WorkflowTabs)

'        ReturnScripts.Add("ResizeControls();")

'    End Sub

'End Class
<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ViewAttachments.aspx.vb"
    Inherits="docAssistWeb.ViewAttachments" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>docAssist - View Attachments</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <link href="Styles.css" type="text/css" rel="stylesheet">
</head>
<body bottommargin="0" leftmargin="0" topmargin="0" rightmargin="0">
    <form id="Form1" method="post" runat="server">
    <table class="PageContent" id="Table1" cellspacing="0" cellpadding="0" width="100%"
        border="0">
        <!-- Parent grey container -->
        <tr>
            <td valign="top">
                <div class="Container">
                    <div class="north">
                        <div class="east">
                            <div class="south">
                                <div class="west">
                                    <div class="ne">
                                        <div class="se">
                                            <div class="sw">
                                                <div class="nw">
                                                    <table class="PageContent" id="tblMain" cellspacing="0" cellpadding="0" width="95%"
                                                        border="0">
                                                        <tr>
                                                            <td nowrap align="left">
                                                                <span class="Heading">File Details</span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top" align="left">
                                                                <table class="PageContent" cellspacing="0" cellpadding="0" width="100%" border="0">
                                                                    <tr>
                                                                        <td nowrap>
                                                                            <img height="1" src="dummy.gif" width="7">
                                                                        </td>
                                                                        <td nowrap align="right">
                                                                            <span class="Text" id="FileName" runat="server">Filename: </span>
                                                                        </td>
                                                                        <td nowrap>
                                                                            <asp:LinkButton ID="lnkFilename" runat="server"></asp:LinkButton>
                                                                        </td>
                                                                        <td align="right" width="100%">
                                                                            <span class="Text" id="CurrentVersion" runat="server">Current Version:&nbsp;<asp:Label
                                                                                ID="lblCurrentVersion" runat="server" Visible="True" CssClass="Text"></asp:Label></span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td nowrap>
                                                                            <img height="1" src="dummy.gif" width="7">
                                                                        </td>
                                                                        <td nowrap align="right">
                                                                            <span class="Text" id="Title" runat="server">Last Modified By: </span>
                                                                        </td>
                                                                        <td nowrap>
                                                                            <asp:Label ID="lblModifyUser" runat="server" CssClass="Text"></asp:Label>
                                                                        </td>
                                                                        <td align="right" width="100%">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td nowrap>
                                                                            <img height="1" src="dummy.gif" width="7">
                                                                        </td>
                                                                        <td nowrap align="right">
                                                                            <span class="Text" id="Description" runat="server">Last Modified Date: </span>
                                                                        </td>
                                                                        <td nowrap>
                                                                            <asp:Label ID="lblModifyDate" runat="server" CssClass="Text"></asp:Label>
                                                                        </td>
                                                                        <td align="right" width="100%">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td nowrap>
                                                                            <img height="1" src="dummy.gif" width="7">
                                                                        </td>
                                                                        <td nowrap align="right">
                                                                            <span class="Text" id="Comments" runat="server">Comments: </span>
                                                                        </td>
                                                                        <td nowrap>
                                                                            <asp:Label ID="lblHeaderComments" runat="server" CssClass="Text"></asp:Label>
                                                                        </td>
                                                                        <td align="right" width="100%">
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                <br>
                                                                <table id="tblCheckInOut" cellspacing="0" cellpadding="0" width="100%" border="0"
                                                                    runat="server">
                                                                    <tr>
                                                                        <td nowrap>
                                                                            <img height="1" src="dummy.gif" width="7">
                                                                        </td>
                                                                        <td nowrap align="left" width="100%">
                                                                            <asp:Label ID="lblCheckOutStatus" runat="server" CssClass="Text"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td nowrap>
                                                                            <img height="1" src="dummy.gif" width="7">
                                                                        </td>
                                                                        <td nowrap align="left">
                                                                            <asp:ImageButton ID="btnCheckOut" runat="server" Visible="False" ImageUrl="Images/checkout.gif">
                                                                            </asp:ImageButton><asp:ImageButton ID="btnCancelCheckOut" runat="server" Visible="False"
                                                                                ImageUrl="Images/cancelcheckout.gif"></asp:ImageButton>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                <br>
                                                                <table id="Upload" cellspacing="0" cellpadding="0" width="100%" border="0" runat="server">
                                                                    <tr>
                                                                        <td valign="top" nowrap>
                                                                            <span class="Text" id="UploadDirections" runat="server">To upload a newer version of
                                                                                this file select it below and click Save.</span>
                                                                            <br>
                                                                            <input id="txtUploadFilename" style="font-size: 8pt; font-family: 'Trebuchet MS'"
                                                                                type="file" size="40" name="txtFilename" runat="server">
                                                                            <asp:CheckBox ID="chkNewVersion" runat="server" CssClass="Text" Text="Create New Version">
                                                                            </asp:CheckBox>&nbsp; <span class="Text" id="CommentLabel" runat="server">Comments:</span>
                                                                            <asp:TextBox ID="txtVersionComments" runat="server" CssClass="DisabledTextbox" Enabled="False"
                                                                                MaxLength="255" Width="170px" Font-Names="Trebuchet MS" Font-Size="8pt"></asp:TextBox></SPAN>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                <asp:ScriptManager ID="ScriptManager1" runat="server">
                                                                </asp:ScriptManager>
                                                                <telerik:RadProgressArea ID="radProgress" runat="server" Skin="Default">
                                                                    <Localization Uploaded="Uploaded"></Localization>
                                                                </telerik:RadProgressArea>
                                                                <telerik:RadProgressManager ID="RadProgressManager1" runat="server" />
                                                                <div id="VersionsTable" style="overflow: auto; width: 100%; height: 170px">
                                                                    <span class="Text" id="VersionGrid">
                                                                        <asp:Label ID="lblNoVersions" runat="server" Visible="False" CssClass="Text"></asp:Label><asp:DataGrid
                                                                            ID="dgAttachmentVersionDetail" runat="server" CssClass="SearchGrid" Width="100%"
                                                                            AutoGenerateColumns="False">
                                                                            <Columns>
                                                                                <asp:TemplateColumn HeaderText="Filename">
                                                                                    <HeaderStyle Width="120px"></HeaderStyle>
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblDetailVersionId" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.VersionId") %>'>
                                                                                        </asp:Label>
                                                                                        <asp:LinkButton ID="lnkLoad" runat="server" CommandName="Select">
                                                                                            <asp:Label ID="lblDetailFileName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FileName") %>'>
                                                                                            </asp:Label>
                                                                                        </asp:LinkButton>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn HeaderText="Comments">
                                                                                    <HeaderStyle Width="200px"></HeaderStyle>
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblDetailComments" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Comments") %>'>
                                                                                        </asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn HeaderText="Version">
                                                                                    <HeaderStyle Width="75px"></HeaderStyle>
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblDetailVersion" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.VersionNum") %>'>
                                                                                        </asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn HeaderText="Size">
                                                                                    <HeaderStyle Width="75px"></HeaderStyle>
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblDetailSize" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Size") %>'>
                                                                                        </asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn HeaderText="User">
                                                                                    <HeaderStyle Width="70px"></HeaderStyle>
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblUser" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.UserName") %>'>
                                                                                        </asp:Label>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateColumn>
                                                                            </Columns>
                                                                        </asp:DataGrid></span></div>
                                                                <table height="100%" cellspacing="0" cellpadding="0" width="100%" border="0">
                                                                    <tr>
                                                                        <td>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="right">
                                                                            <img id="highSpace" src="dummy.gif" width="1">&nbsp;
                                                                            <br>
                                                                            <asp:Label ID="lblError" runat="server" CssClass="ErrorText"></asp:Label>&nbsp;&nbsp;&nbsp;
                                                                            <asp:ImageButton ID="btnSave" runat="server" ImageUrl="Images/smallred_save.gif">
                                                                            </asp:ImageButton>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <span id="CancelButton" onclick="CancelUpload();" style="cursor: hand">
                                                        <img src="Images/btn_cancelupload.gif"></span>
                                                    <input id="CheckFlag" type="hidden" value="1">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <input id="Cancel" type="hidden" value="0" runat="server"><input id="versionId" type="hidden"
                    value="0" runat="server">
            </td>
        </tr>
    </table>
    </TD></TR></TABLE>
    <asp:ImageButton ID="SetCancel" runat="server" Height="0" Width="0"></asp:ImageButton>
    </form>

    <script language="javascript">
		
			document.getElementById('CancelButton').style.visibility = 'hidden';
		
			function RadProgressManagerOnClientProgressStarted()
			{
				var txtUploadFilename = document.getElementById('txtUploadFilename');
				if (txtUploadFilename != undefined)
				{
					if ((document.all.txtUploadFilename.value != '') && (document.getElementById('CheckFlag').value != '1'))
					{
						document.all.VersionsTable.style.display='none';
						<%= radProgress.ClientID %>.Show();
						document.getElementById('CancelButton').style.visibility = 'visible';
					}
				}
			}
			
			function toggleComments()
			{
				if (document.all.chkNewVersion.checked == true)
				{
					document.all.txtVersionComments.disabled = false;	
					document.all.txtVersionComments.className = '';	
				}
				else
				{
					document.all.txtVersionComments.value = '';
					document.all.txtVersionComments.disabled = true;
					document.all.txtVersionComments.className = 'DisabledTextbox';	
				}
			}

			function HideProgress()
			{
				<%= radProgress.ClientID %>.Hide();
			}
			
			function clickSave()
			{ 
				if (document.all.txtUploadFilename.value != ''){
					document.all.btnSave.click();
				}else{
				}
			}
			
			function CancelUpload()
			{
				if (confirm('The current upload will be aborted. Are you sure?'))
				{
					document.getElementById('SetCancel').click();
					<%= radProgress.ClientID %>.CancelRequest();
				}
			}
			
			function loadVersionId(id)
			{
				//alert(id);
			}

		
			function startPolling(){
			    //window.parent.uploadOn();
			    //document.all.AdvancedCheck.style.display='none';
			    //document.all.UploadBoxes.style.display='none';
			    var status = $find("<%= RadProgressManager1.ClientID %>");
			    status.startProgressPolling();
			}

    </script>

    <asp:Literal ID="litHide" runat="server"></asp:Literal>
    <asp:Literal ID="litRedirect" runat="server"></asp:Literal>
</body>
</html>

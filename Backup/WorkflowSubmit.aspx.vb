Imports Accucentric.docAssist.Web.Security
Imports Accucentric.docAssist.Data.Images

Partial Class WorkflowSubmit
    Inherits System.Web.UI.Page
    
    Private intVersionId

    Private mobjUser As User
    Private mcookieSearch As cookieSearch

    Private dsSubmit As New DataSet


#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'RegisterWaitDisabled(ddlWorkflow)
        'RegisterWaitDisabled(ddlQueue)

        '
        'Page Init
        '
        'txtNotes.Attributes("onkeypress") = "PassWorkflowNote();"
        'txtNotes.Attributes("onblur") = "PassWorkflowNote();"

        '
        ' Check that user is authenticated.
        '
        Try
            If Request.Cookies("docAssist") Is Nothing Then
                Response.Redirect("Default.aspx")
                Exit Sub
            End If

            Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
            Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))

            Me.mcookieSearch = New cookieSearch(Me)

            Me.mobjUser.AccountId = guidAccountId
        Catch ex As Exception
            Response.Redirect("Default.aspx")
            Exit Sub
        End Try

        Dim intRouteId As Integer = Request.QueryString("Route")
        Dim strVersionId As String = Me.mcookieSearch.VersionID

        ''ReturnScripts.Add("parent.frames[""frameAttributes""].ActivateSave();")
        Session(strVersionId & "Mode") = Request.QueryString("Mode")
        Session(strVersionId & "Route") = intRouteId

        If Not Page.IsPostBack Then

            Try
                Dim objWf As New Workflow(Functions.BuildConnection(Me.mobjUser))

                dsSubmit = objWf.WFSubmitterWorkflowsGet(Me.mobjUser.ImagesUserId, , CInt(Session("NewDocumentId")))

                ddlWorkflow.Items.Add(New ListItem("Select a workflow...", "-2"))

                For Each dr As DataRow In dsSubmit.Tables(0).Rows
                    Dim intCategoryId As Integer = dr("CategoryId")
                    Dim strCategoryName As String = dr("CategoryName")
                    Dim dvFilter As New DataView(dsSubmit.Tables(1))
                    dvFilter.RowFilter = "CategoryId=" & intCategoryId

                    ddlWorkflow.Items.Add(New ListItem(strCategoryName, "OPTGROUP"))
                    For X As Integer = 0 To dvFilter.Count - 1
                        ddlWorkflow.Items.Add(New ListItem(dvFilter(X)("WorkflowName"), dvFilter(X)("WorkflowId")))
                    Next
                    ddlWorkflow.Items.Add(New ListItem(strCategoryName, "/OPTGROUP"))

                Next

                ddlWorkflow.SelectedValue = -2



            Catch ex As Exception

            Finally
                'ReturnControls.Add(ddlWorkflow)
            End Try

        End If

    End Sub

    Private Sub ddlWorkflow_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlWorkflow.SelectedIndexChanged

        Try

            Dim strVersionId As String = Me.mcookieSearch.VersionID

            If ddlWorkflow.Items(0).Text <> "None" Then ddlWorkflow.Items(0).Text = "None"

            If ddlWorkflow.SelectedValue = -2 Or ddlWorkflow.SelectedValue = "-2" Then
                ddlQueue.Enabled = False
                ddlQueue.Items.Clear()
                txtNotes.Enabled = False
                chkUrgent.Checked = False
                chkUrgent.Enabled = False

                Session(strVersionId & "Urgent") = "0"
                Session(strVersionId & "WorkflowId") = "0"
                Session(strVersionId & "QueueId") = "0"

                'ReturnControls.Add(chkUrgent)
                'ReturnControls.Add(ddlQueue)
                'ReturnControls.Add(ddlWorkflow)
                'ReturnControls.Add(txtNotes)
                Exit Sub

            End If

            Dim objWf As New Workflow(Functions.BuildConnection(Me.mobjUser))
            dsSubmit = objWf.WFSubmitterWorkflowsGet(Me.mobjUser.ImagesUserId, Me.mcookieSearch.VersionID, CInt(Session("NewDocumentId")))
            Dim dvFilter As New DataView(dsSubmit.Tables(2))
            dvFilter.RowFilter = "WorkflowId=" & ddlWorkflow.SelectedValue

            ddlQueue.DataTextField = "QueueName"
            ddlQueue.DataValueField = "QueueId"
            ddlQueue.DataSource = dvFilter
            ddlQueue.DataBind()

            ddlQueue.Enabled = True
            txtNotes.Enabled = True
            chkUrgent.Enabled = True

            System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType(), "save", "parent.frames[""frameAttributes""].ActivateSave();", True)

            Session(strVersionId & "WorkflowId") = ddlWorkflow.SelectedValue
            Session(strVersionId & "QueueId") = ddlQueue.SelectedValue

            'ReturnControls.Add(chkUrgent)
            'ReturnControls.Add(ddlQueue)
            'ReturnControls.Add(ddlWorkflow)
            'ReturnControls.Add(txtNotes)

            Dim wf As New Workflow(Functions.BuildConnection(Me.mobjUser))
            dsSubmit = wf.WFSubmitterWorkflowsGet(Me.mobjUser.ImagesUserId, Me.mcookieSearch.VersionID, CInt(Session("NewDocumentId")))

            Dim dvQueues As New DataView(dsSubmit.Tables(2))
            dvQueues.RowFilter = "WorkflowId=" & ddlWorkflow.SelectedValue & " AND QueueId=" & ddlQueue.SelectedValue

            'Move to folder
            Select Case dvQueues(0)("FolderId")
                Case 0
                    'Clear
                    System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType(), "clearFolder", "parent.frames[""frameAttributes""].setFolder('','');", True)
                Case -1
                    'Leave alone
                Case Else
                    'Move
                    System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType(), "moveFolder", "parent.frames[""frameAttributes""].setFolder('" & dvQueues(0)("FolderId") & "','" & Server.HtmlEncode(dvQueues(0)("FolderPath")).Replace("\", "\\") & "');", True)
            End Select

        Catch ex As Exception

        End Try

    End Sub

    Private Sub ddlQueue_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlQueue.SelectedIndexChanged

        Try

            Dim strVersionId As String = Me.mcookieSearch.VersionID
            System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType(), "save", "parent.frames[""frameAttributes""].ActivateSave();", True)
            Session(strVersionId & "QueueId") = ddlQueue.SelectedValue

            Dim objWf As New Workflow(Functions.BuildConnection(Me.mobjUser))
            dsSubmit = objWf.WFSubmitterWorkflowsGet(Me.mobjUser.ImagesUserId, Me.mcookieSearch.VersionID, CInt(Session("NewDocumentId")))

            Dim dvQueues As New DataView(dsSubmit.Tables(2))
            dvQueues.RowFilter = "WorkflowId=" & ddlWorkflow.SelectedValue & " AND QueueId=" & ddlQueue.SelectedValue

            'Move to folder
            Select Case dvQueues(0)("FolderId")
                Case 0
                    'Clear
                    System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType(), "clearFolder", "parent.frames[""frameAttributes""].setFolder('','');", True)
                Case -1
                    'Leave alone
                Case Else
                    'Move
                    System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType(), "moveFolder", "parent.frames[""frameAttributes""].setFolder('" & dvQueues(0)("FolderId") & "','" & Server.HtmlEncode(dvQueues(0)("FolderPath")).Replace("\", "\\") & "');", True)
            End Select

        Catch ex As Exception

        End Try

    End Sub

    Private Sub chkUrgent_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkUrgent.CheckedChanged

        Try

            Dim intPass As Integer = chkUrgent.Checked
            Dim strVersionId As String = Me.mcookieSearch.VersionID
            System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType(), "save", "parent.frames[""frameAttributes""].ActivateSave();", True)
            Session(strVersionId & "Urgent") = intPass.ToString

        Catch ex As Exception

        End Try

    End Sub

    Private Sub txtNotes_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtNotes.TextChanged

        Try

            Dim strVersionId As String = Me.mcookieSearch.VersionID
            System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType(), "save", "parent.frames[""frameAttributes""].ActivateSave();", True)
            Session(strVersionId & "Notes") = txtNotes.Text

        Catch ex As Exception

        End Try

    End Sub

End Class
Partial Class docAssistDebug
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try

            Dim sbVersion As New System.Text.StringBuilder

            'Get IE version
            Dim strIeVersion As String = Request.Browser.Version
            sbVersion.Append("Internet Explorer version: " & strIeVersion & vbCr)


            'Check users current .NET framework version.
            Dim Version() As System.Version = Request.Browser.GetClrVersions()

            sbVersion.Append(Chr(13) & ".NET Frameworks installed:" & vbCr)
            For x As Integer = 0 To Version.Length - 1
                sbVersion.Append(Version(x).ToString & "." & Version(x).Revision & vbCr)
            Next

            lblDebug.Text = sbVersion.ToString

        Catch ex As Exception

        End Try

    End Sub

End Class

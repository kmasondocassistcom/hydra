<%@ Page Language="vb" AutoEventWireup="false" Codebehind="RouteUsers.aspx.vb" Inherits="docAssistWeb.RouteUsers"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>docAssist - Workflow Route Users</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<BR>
			&nbsp;&nbsp;&nbsp; Route Users<BR>
			<BR>
			<table align="center" width="500" height="250" border="0">
				<tr>
					<td>
						<asp:DataGrid id="dgRouteUsers" runat="server" Width="490px" Height="230px" CssClass="PageContent"
							Font-Names="Verdana" Font-Size="8pt" BackColor="Transparent" CellSpacing="2" BorderWidth="0px"
							ShowFooter="True">
							<AlternatingItemStyle BackColor="Silver"></AlternatingItemStyle>
							<ItemStyle Height="10px"></ItemStyle>
							<HeaderStyle Height="10px" ForeColor="White" BackColor="Gray"></HeaderStyle>
						</asp:DataGrid>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>

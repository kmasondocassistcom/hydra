<%@ Page Language="vb" AutoEventWireup="false" Codebehind="FileUpload.aspx.vb" Inherits="docAssistWeb.FileUpload"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title></title>
		<LINK href="css/core.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body>
		<form id="frmFileUpload" method="post" runat="server">
			<table height="100%" width="100%">
				<tr>
					<td><p>Select the files to upload:</p>
						<p><div id="uploaderContainer">
								<div id="uploaderOverlay" style="Z-INDEX:2; POSITION:absolute"></div>
								<div id="selectFilesLink" style="Z-INDEX:1"><a id="selectLink" href="#">Select Files</a></div>
							</div>
						<P></P>
					</td>
				</tr>
				<tr>
					<td height="100%"><div align="center" id="dataTableContainer" style="OVERFLOW: auto; WIDTH: 255px; HEIGHT: 200px; BACKGROUND-COLOR: #ffffff"></div>
					</td>
				</tr>
				<tr>
					<td>
						<p><div id="uploadFilesLink"><a id="uploadLink" onClick="upload(); return false;" href="#">Upload 
									Files</a></div>
							<br>
							<asp:checkbox id="chkAdvanced" runat="server" Text="Configure advanced document settings"></asp:checkbox>
						<P></P>
					</td>
				</tr>
			</table>
			<div id="uiElements" style="DISPLAY:inline">
				<div id="simUploads" style="DISPLAY:none">
					Number of simultaneous uploads:
					<select id="simulUploads">
						<option value="1" selected>1</option>
						<option value="2">2</option>
						<option value="3">3</option>
						<option value="4">4</option>
					</select>
				</div>
			</div>
			<DIV></DIV>
			<input type="hidden" id="totalUploads" runat="server" value="0">
			<script type="text/javascript" src="scripts/yuiloader-min.js"></script>
			<script type="text/javascript" src="scripts/event-min.js"></script>
			<script type="text/javascript" src="scripts/dom-min.js"></script>
			<script type="text/javascript" src="scripts/logger-min.js"></script>
			<script type="text/javascript" src="scripts/element-beta-min.js"></script>
			<script type="text/javascript" src="scripts/uploader-experimental.js"></script>
			<script type="text/javascript" src="scripts/datasource-min.js"></script>
			<script type="text/javascript" src="scripts/datatable-min.js"></script>
			<script type="text/javascript" src="scripts/button-min.js"></script>
			<SCRIPT language="javascript" src="scripts/cookies.js"></SCRIPT>
			<script type="text/javascript">

	var fileCount = 0;
	
	YAHOO.util.Event.onDOMReady(function () { 
	var uiLayer = YAHOO.util.Dom.getRegion('selectLink');
	var overlay = YAHOO.util.Dom.get('uploaderOverlay');
	YAHOO.util.Dom.setStyle(overlay, 'width', uiLayer.right-uiLayer.left + "px");
	YAHOO.util.Dom.setStyle(overlay, 'height', uiLayer.bottom-uiLayer.top + "px");
	});
	YAHOO.widget.Uploader.SWFURL = "flashApplets/uploader.swf";
	var uploader = new YAHOO.widget.Uploader( "uploaderOverlay" );
	
	uploader.addListener('contentReady', handleContentReady);
	uploader.addListener('fileSelect', onFileSelect)
	uploader.addListener('uploadStart', onUploadStart);
	uploader.addListener('uploadProgress', onUploadProgress);
	uploader.addListener('uploadCancel', onUploadCancel);
	uploader.addListener('uploadComplete', onUploadComplete);
	uploader.addListener('uploadCompleteData', onUploadResponse);
	uploader.addListener('uploadError', onUploadError);
    uploader.addListener('rollOver', handleRollOver);
    uploader.addListener('rollOut', handleRollOut);
    uploader.addListener('click', handleClick);
	uploader.addListener('mouseDown', handleMouseDown);
	uploader.addListener('mouseUp', handleMouseUp);
	
	function handleRollOver () {
		YAHOO.util.Dom.setStyle(YAHOO.util.Dom.get('selectLink'), 'color', "#FFFFFF");
		YAHOO.util.Dom.setStyle(YAHOO.util.Dom.get('selectLink'), 'background-color', "#000000");
	}

	function handleRollOut () {
		YAHOO.util.Dom.setStyle(YAHOO.util.Dom.get('selectLink'), 'color', "#0000CC");
		YAHOO.util.Dom.setStyle(YAHOO.util.Dom.get('selectLink'), 'background-color', "#FFFFFF");
	}	
		function handleMouseDown () {		
	}
	
	function handleMouseUp () {
	}
	
	function handleClick () {
	}
	
	function handleContentReady () {
	// Allows the uploader to send log messages to trace, as well as to YAHOO.log
	uploader.setAllowLogging(true);
	
	// Allows multiple file selection in "Browse" dialog.
	uploader.setAllowMultipleFiles(true);
	
	// New set of file filters.
	var ff = new Array({description:"All Files", extensions:"*.*"});
		                
	// Apply new set of file filters to the uploader.
	uploader.setFileFilters(ff);
	}
	
	var fileList;

	function onFileSelect(event) {
		fileList = event.fileList;
		createDataTable(fileList);
	}

	function createDataTable(entries) {
	rowCounter = 0;
	this.fileIdHash = {};
	this.dataArr = [];
	for(var i in entries) {
		fileCount = fileCount + 1;
		document.getElementById('totalUploads').value = fileCount;
		var entry = entries[i];
		entry["progress"] = "<div style='height:5px;width:100px;background-color:#CCC;'></div>";
		dataArr.unshift(entry);
	}

	for (var j = 0; j < dataArr.length; j++) {
		this.fileIdHash[dataArr[j].id] = j;
	}

		var myColumnDefs = [
			{key:"name", label: "File", sortable:false},
     		/*{key:"size", label: "Size", sortable:false},*/
     		{key:"progress", label: "Upload progress", sortable:false}
		];

	this.myDataSource = new YAHOO.util.DataSource(dataArr);
	this.myDataSource.responseType = YAHOO.util.DataSource.TYPE_JSARRAY;
	this.myDataSource.responseSchema = {
		fields: ["id","name","created","modified","type", "size", "progress"]
	};

	this.singleSelectDataTable = new YAHOO.widget.DataTable("dataTableContainer",
			myColumnDefs, this.myDataSource, {
				/*caption:"Files To Upload",*/
				selectionMode:"single"
			});
	}	

	function upload() {
	if (fileList != null) {
		uploader.setSimUploadLimit(parseInt(document.getElementById("simulUploads").value));
		//uploader.uploadAll("FileUpload.aspx");
		    uploader.uploadAll( "FileUpload.aspx",  
	                    "POST",  
	                    {count:fileCount, 
	                     advmode:document.getElementById('chkAdvanced').checked}); 
	}	
	}

	function onUploadProgress(event) {
		rowNum = fileIdHash[event["id"]];
		prog = Math.round(100*(event["bytesLoaded"]/event["bytesTotal"]));
		progbar = "<div style='height:5px;width:100px;background-color:#CCC;'><div style='height:5px;background-color:#F00;width:" + prog + "px;'></div></div>";
		singleSelectDataTable.updateRow(rowNum, {name: dataArr[rowNum]["name"], size: dataArr[rowNum]["size"], progress: progbar});	
	}
	
	var completeFileCount = 0;
	function onUploadComplete(event) {
		completeFileCount = completeFileCount + 1;
		rowNum = fileIdHash[event["id"]];
		prog = Math.round(100*(event["bytesLoaded"]/event["bytesTotal"]));
		progbar = "<div style='height:5px;width:100px;background-color:#CCC;'><div style='height:5px;background-color:#F00;width:100px;'></div></div>";
		singleSelectDataTable.updateRow(rowNum, {name: dataArr[rowNum]["name"], size: dataArr[rowNum]["size"], progress: progbar});
		if(completeFileCount == fileCount){
			if(document.getElementById('chkAdvanced').checked)	{
				window.location = 'Upload.aspx';
			}else{
				parent.hideAdd();
				var folderId = readCookie("UploadFolderId").replace("W","");
				parent.browseFolder(folderId);
			}
		}
	}

	function onUploadStart(event) {	
	}

	function onUploadError(event) {
	}
	
	function onUploadCancel(event) {
	}
	
	function onUploadResponse(event) {
	}
			</script>
			<INPUT id="uploadCount" type="hidden" runat="server">
		</form>
	</body>
</HTML>

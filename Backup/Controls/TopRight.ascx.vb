Partial Class TopRight
    Inherits System.Web.UI.UserControl

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblText As System.Web.UI.WebControls.Label
    Protected WithEvents lblUser As System.Web.UI.WebControls.Label
    Protected WithEvents Label1 As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim objUser As Accucentric.docAssist.Web.Security.User
        '
        ' Build User object
        '
        If Not Request.Cookies("docAssist") Is Nothing Then
            If Not Request.Cookies("docAssist")("UserId") Is Nothing Then
                objUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
            End If
        End If
        '
        ' If the user object is  built then show the label
        '
        If Not objUser Is Nothing Then
            lblAccountDesc.Text = objUser.AccountDesc
            lblUserName.Text = objUser.UserName & " (" & objUser.EMailAddress & ")"
            Select Case objUser.UserType
                Case Accucentric.docAssist.Web.Security.User.UserAccessType.FULL_USER
                    lblUserType.Text = "Imaging User"
                Case Accucentric.docAssist.Web.Security.User.UserAccessType.READ_ONLY
                    lblUserType.Text = "Read-Only User"
                Case Accucentric.docAssist.Web.Security.User.UserAccessType.VIEW_ONLY
                    lblUserType.Text = "Standard User"
                Case Accucentric.docAssist.Web.Security.User.UserAccessType.WORKFLOW_ONLY
                    lblUserType.Text = "Workflow Only User"
            End Select

            If objUser.SysAdmin = True Then lblUserType.Text = lblUserType.Text & " [System Administrator]"

        End If
    End Sub

End Class

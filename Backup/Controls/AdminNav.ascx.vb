Partial Class AdminNav
    Inherits System.Web.UI.UserControl

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private conCompany As New SqlClient.SqlConnection
    Dim mobjUser As Accucentric.docAssist.Web.Security.User

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try

            smallBiz.Visible = False

            mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
            Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))
            mobjUser.AccountId = guidAccountId

            conCompany = Functions.BuildConnection(mobjUser)
            If conCompany.State <> ConnectionState.Open Then conCompany.Open()

            If Not (mobjUser.SecAdmin = True Or mobjUser.SysAdmin = True Or mobjUser.SyncAdmin = True) Then
                Session("strUserError") = "Access denied."
                Response.Redirect("Default.aspx")
            End If

            If mobjUser.SysAdmin = False Then
                ConfigHeader.Visible = False
                ConfigurationLinks.Visible = False
                WorkflowHeader.Visible = False
                WorkflowLinks.Visible = False
                IntegrationHeader.Visible = False
                IntegrationLinks.Visible = False
                AccountSettingsHeader.Visible = False
                tblAccountSettings.Visible = False
            End If

            'Users
            If Not (mobjUser.SecAdmin = True Or mobjUser.SysAdmin = True) Then
                Users.Visible = False
            End If

            'Groups
            If Not (mobjUser.SecAdmin = True Or mobjUser.SysAdmin = True) Then
                Groups.Visible = False
            End If

            'Account status
            If Not (mobjUser.SecAdmin = True Or mobjUser.SysAdmin = True) Then
                AccountStatus.Visible = False
            End If

            'Document Types/Attributes
            If Functions.ModuleSetting(Functions.ModuleCode.DOCUMENT_TYPES_ATTRIBUTES, mobjUser, conCompany) = False Then
                ConfigHeader.Visible = False
                ConfigurationLinks.Visible = False
            End If

            'Integration
            If Functions.ModuleSetting(Functions.ModuleCode.APP_INTEGRATION, mobjUser, conCompany) = False Then
                IntegrationHeader.Visible = False
                IntegrationLinks.Visible = False
            End If

            'Workflow
            If Functions.ModuleSetting(Functions.ModuleCode.WORKFLOW, mobjUser, conCompany) = False Then
                WorkflowHeader.Visible = False
                WorkflowLinks.Visible = False
            End If

            'User Admin
            If Functions.ModuleSetting(Functions.ModuleCode.USER_ADMIN, mobjUser, conCompany) = False Then
                Users.Visible = False
                Groups.Visible = False
            End If

            'Inbound E-Mail
            If Functions.ModuleSetting(Functions.ModuleCode.INBOUND_EMAIL, mobjUser, conCompany) = False Then
                tdInEmail.Visible = False
                trEmailList.Visible = False
            End If

            'Data Synchronization
            If Functions.ModuleSetting(Functions.ModuleCode.DATA_SYNC, mobjUser, conCompany) = False Then
                SyncLinks.Visible = False
            Else
                If mobjUser.SyncAdmin Or mobjUser.SysAdmin Then
                    IntegrationHeader.Visible = True
                    SyncLinks.Visible = True
                Else
                    SyncLinks.Visible = False
                End If
            End If

            'Timeforge
            If Functions.ModuleSetting(Functions.ModuleCode.TIMEFORGE, mobjUser, conCompany) = False Then
                tdTimeforge.Visible = False
            Else
                tdTimeforge.Visible = True
            End If

            If mobjUser.EditionID = AccountEdition.SMALL_BUSINESS_EDITION Then
                AccountSettingsHeader.Visible = False
                tblAccountSettings.Visible = False
                Groups.Visible = False
                smallBiz.Visible = True
                secgroups.Visible = False
            End If

        Catch ex As Exception

            Dim conMaster As SqlClient.SqlConnection
            conMaster = Functions.BuildMasterConnection
            Functions.LogException(mobjUser.UserId.ToString, mobjUser.AccountId.ToString, "", "Admin Navigation Load Error: " & ex.Message, ex.StackTrace, Server.MachineName, "", Request.UrlReferrer.ToString, conMaster, True)

        Finally

            If conCompany.State <> ConnectionState.Closed Then
                conCompany.Close()
                conCompany.Dispose()
            End If

        End Try

    End Sub

    Private Sub lnkCapture_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkCapture.Click
        Session("mAdminMapTable") = "CAPTURE"
        Response.Redirect("ManageIntegration.aspx")
    End Sub

    Private Sub lnkApplication_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkApplication.Click
        Session("mAdminMapTable") = "INTEGRATION"
        Response.Redirect("ManageIntegration.aspx")
    End Sub

    Private Sub lnkCategories_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        litRedirect.Text = "<script language='javascript'>parent.location.href = 'WorkflowAdmin.aspx#Category'</script>"
    End Sub

    Private Sub lnkGroups_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        litRedirect.Text = "<script language='javascript'>parent.location.href = 'WorkflowAdmin.aspx#Group'</script>"
    End Sub

    Private Sub lnkQueues_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        litRedirect.Text = "<script language='javascript'>parent.location.href = 'WorkflowAdmin.aspx#Queue'</script>"
    End Sub

    Private Sub lnkWorkflow_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        litRedirect.Text = "<script language='javascript'>parent.location.href = 'WorkflowAdmin.aspx#Workflow'</script>"
    End Sub

    Private Sub lnkStatus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        litRedirect.Text = "<script language='javascript'>parent.location.href = 'WorkflowAdmin.aspx#Status'</script>"
    End Sub
End Class

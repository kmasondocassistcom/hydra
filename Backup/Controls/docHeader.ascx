<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="docHeader.ascx.vb"
    Inherits="docAssistWeb.docHeader" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<link href="favicon.ico" rel="shortcut icon">
<link href="./css/buttons.css" type="text/css" rel="stylesheet">
<link href="./css/greybox.css" type="text/css" rel="stylesheet">
<table style="background-image: url(./Images/background-blue.gif); background-repeat: repeat-x"
    height="89" cellspacing="0" cellpadding="0" width="100%" border="0">
    <tr>
        <td style="padding-bottom: 0px; margin: 0px; padding-left: 0px; padding-right: 0px;
            padding-top: 0px">
            &nbsp;<img id="headerLogo" src="./Images/logoBug.gif" runat="server">
        </td>
        <td valign="bottom" width="15">
            <img height="1" src="spacer.gif" width="20">
        </td>
        <td valign="baseline" width="44" style="padding-bottom: 0px">
            <a href="../Search.aspx" id="lnkSearch" runat="server">
                <img border="0" src="../Images/toolbar_search_white.gif" id="btnSearch" runat="server"></a>
        </td>
        <td id="scanCell" valign="baseline" width="28" runat="server">
            <a id="managerLink" href="DocumentManager.aspx" runat="server">
                <img id="imgLauncher" src="../Images/manager_white.gif" border="0" runat="server"></a>
        </td>
        <td valign="baseline" width="30" style="display: none">
            <asp:ImageButton ID="btnIndex" runat="server" ImageUrl="../Images/toolbar_index_white.gif"
                CausesValidation="False"></asp:ImageButton>
        </td>
        <td valign="baseline" width="28" style="padding-bottom: 0px">
            <asp:ImageButton ID="btnWorkflow" runat="server" ImageUrl="../Images/toolbar_workflow_white.gif">
            </asp:ImageButton>
        </td>
        <td valign="bottom" align="right" width="100%">
            <table height="100%" cellspacing="0" cellpadding="0" border="0" height=41>
                <tr>
                    <td valign="top" nowrap align="right">
                        <a id="upgradeLink" runat="server">
                            <img id="upgrade" alt="Upgrade my account" src="../Images/upgrade_btn.gif" border="0"
                                runat="server"></a> <a id="lnkPreferences" href="../Preferences.aspx" runat="server">
                                    <span class="topNav">preferences</span></a> <span class="topNav">&nbsp;::&nbsp;
                        </span><a id="helpLink" runat="server"><span style="cursor:hand" class="topNav">help</span></a>
                        <span class="topNav">&nbsp;::&nbsp;
                        </span><a href="./Support.aspx"><span class="topNav">support</span></a>
                        <span class="topNav">&nbsp;::&nbsp;
                        </span><span style="cursor:hand;" class="topNav" onclick="$('#docHeader_tbnSignOut').click();">signOut
                        </span>
                        <asp:ImageButton ID="tbnSignOut" runat="server" ImageUrl="../Images/sign_out-blue.gif" Height=0 Width=0>
                        </asp:ImageButton>&nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="padding-bottom: 10px; padding-right: 10px" align="right">
                        <span style="padding-right: 10px">
                            <asp:Label ID="lblTrial" runat="server" ForeColor="LightGray" Font-Bold="True" Font-Names="Verdana"
                                Font-Size="10pt"></asp:Label></span><a class="btn primary" id="btnScan" runat="server"><span><span
                                    id="SPAN1">
                                    <img src="Images/buttons/plus-circle.png" border="0">
                                    Scan Documents</span></span></a>
                    </td>
                </tr>
                <tr>
                    <td valign="baseline" align="right" height="24">
                        <asp:ImageButton ID="btnOptions" runat="server" ImageUrl="../Images/toolbar_options_white.gif"
                            Visible="False"></asp:ImageButton>&nbsp;
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<input id="txtUserId" type="hidden" name="txtUserId" runat="server"><input id="txtAccountId"
    type="hidden" name="txtAccountId" runat="server"><input id="Hidden1" type="hidden"
        name="txtUserId" runat="server"><input id="WebServiceUrl" type="hidden" name="WebServiceUrl"
            runat="server">
<input id="ShowResults" type="hidden" name="ShowResults" runat="server">
<div class="white_content" id="light">
    <table height="100%" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td>
                <span id="InfoMsg"></span>
            </td>
        </tr>
        <tr height="100%">
            <td valign="bottom" align="right" height="100%">
                <asp:Literal ID="litSmb" runat="server"></asp:Literal><img style="cursor: hand" onclick="hideInfoBox();"
                    src="./Images/button_ok.gif" border="0">
            </td>
        </tr>
    </table>
</div>

<script language="javascript" src="./scripts/greybox.js"></script>

<script language="javascript" src="./scripts/header.js"></script>

<script language="javascript" src="./scripts/cookies.js"></script>

<script language="javascript" src="./scripts/jquery.js"></script>

<asp:Literal ID="litTerms" runat="server"></asp:Literal><asp:Literal ID="litWorkflow"
    runat="server" Visible="False"></asp:Literal><asp:Literal ID="litLauncher" runat="server"></asp:Literal><asp:Literal
        ID="litVersion" runat="server"></asp:Literal><asp:Literal ID="litPostbackCount" runat="server"></asp:Literal>

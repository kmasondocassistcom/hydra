<%@ Control Language="vb" AutoEventWireup="false" Codebehind="AdminNav.ascx.vb" Inherits="docAssistWeb.AdminNav" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<IMG height="16" src="images/spacer.gif" width="1">
<TABLE class="Navigation1" width="150" border="0">
	<TR>
		<TD width="4" rowSpan="2"><IMG height="1" src="images/spacer.gif" width="4"></TD>
		<TD><A class="Link2" id="Users" href="../UserAdmin.aspx" runat="server">User Accounts</A><span id="secgroups" runat="server"><br>
				<br>
				<A class="Link2" id="Groups" href="../SecurityGroupAdmin.aspx" runat="server">Security 
					Groups</A></span><BR>
			<br>
			<A class="Link2" id="AccountStatus" href="../AccountStatus.aspx" runat="server">Account 
				Status</A>
		</TD>
		</TD></TR>
	<TR>
		<TD><IMG height="4" src="spacer.gif" width="1"><BR>
			<A class="adminHeader" id="AccountSettingsHeader" runat="server">Account Settings</A>
			<TABLE class="transparent" id="tblAccountSettings" runat="server">
				<TR>
					<TD style="PADDING-LEFT: 10px"><A class="Link2" href="SecurityPolicy.aspx">Password 
							Policy</A></TD>
				</TR>
				<TR>
					<TD id="tdQuality" style="PADDING-LEFT: 10px" runat="server"><A class="Link2" id="quality" href="AccountSettingsViewer.aspx">Quality 
							Settings</A></TD>
				</TR>
				<TR id="tdInEmail" runat="server">
					<TD style="PADDING-LEFT: 10px"><A class="Link2" href="AccountInboundEmail.aspx">Inbound 
							E-Mail</A></TD>
				</TR>
				<TR id="trEmailList" runat="server">
					<TD style="PADDING-LEFT: 10px"><A class="Link2" href="AccountInboundEmailList.aspx">E-Mail 
							List</A></TD>
				</TR>
				<TR id="trIPRestrict" runat="server">
					<TD style="PADDING-LEFT: 10px"><A class="Link2" id="ipSec" href="AccountIPFiltering.aspx">IP 
							Security</A></TD>
				</TR>
			</TABLE>
			<span id="smallBiz" runat="server"><A class="adminHeader" runat="server">E-mail 
					Configuration</A>
				<table class="transparent" id="smallBus" runat="server">
					<TR>
						<TD style="PADDING-LEFT: 10px"><A class="Link2" href="AccountInboundEmail.aspx">E-mail 
								Alias</A></TD>
					</TR>
					<TR>
						<TD style="PADDING-LEFT: 10px"><A class="Link2" href="AccountInboundEmailList.aspx">Address 
								List</A></TD>
					</TR>
				</table>
			</span><A class="adminHeader" id="ConfigHeader" runat="server">Configuration</A>
			<TABLE class="transparent" id="ConfigurationLinks" runat="server">
				<TR>
					<TD style="WIDTH: 6px" rowSpan="5"></TD>
					<TD><A class="Link2" id="lnkShowAttributes" href="ManageAttributes.aspx">Attributes</A></TD>
				</TR>
				<TR>
					<TD><A class="Link2" id="lnkShowDocuments" href="ManageDocuments.aspx">Document 
							Types&nbsp;</A></TD>
				</TR>
				<TR>
					<TD style="HEIGHT: 1px"><A class="Link2" id="A1" href="AdminDocumentView.aspx">View</A></TD>
				</TR>
			</TABLE>
			<IMG height="4" src="spacer.gif" width="1"> <A class="adminHeader" id="IntegrationHeader" runat="server">
				Integration</A><BR>
			<TABLE class="transparent" id="IntegrationLinks" runat="server">
				<TR>
					<TD rowSpan="4"><IMG height="1" src="images/spacer.gif" width="4"></TD>
					<TD><asp:linkbutton id="lnkCapture" runat="server" CssClass="Link2">Index</asp:linkbutton></TD>
				</TR>
				<TR>
					<TD><asp:linkbutton id="lnkApplication" runat="server" CssClass="Link2">Search</asp:linkbutton></TD>
				</TR>
				<TR>
					<TD id="tdTimeforge" runat="server"><a href="AdminTimeforge.aspx" class="Link2">TimeForge</a></TD>
				</TR>
			</TABLE>
			<TABLE class="transparent" id="SyncLinks" runat="server">
				<TR id="Synch" runat="server">
					<TD rowSpan="4"><IMG height="1" src="images/spacer.gif" width="4"></TD>
					<TD><A class="Link2" href="Synchs.aspx">Synchronization</A>
					</TD>
				</TR>
				<TR id="SynchStatus" runat="server">
					<TD id="TD1" runat="server"><A class="Link2" href="SynchStatus.aspx">Synch Status</A>
					</TD>
				</TR>
			</TABLE>
			<IMG height="4" src="spacer.gif" width="1"> <A class="adminHeader" id="WorkflowHeader" runat="server">
				Workflow</A><BR>
			<TABLE class="transparent" id="WorkflowLinks" runat="server">
				<TR>
					<TD rowSpan="4"><IMG height="1" src="images/spacer.gif" width="4"></TD>
					<TD><A class="Link2" href="WorkflowAdmin.aspx#Category">Category</A></TD>
				</TR>
				<TR>
					<TD><A class="Link2" href="WorkflowAdmin.aspx#Workflow">Workflows</A></TD>
				</TR>
				<tr>
					<td><A class="Link2" href="WorkflowAdmin.aspx#Group">Groups</A></td>
				</tr>
				<tr>
					<td><A class="Link2" href="WorkflowAdmin.aspx#Queue">Queues</A></td>
				</tr>
				<tr>
					<td></td>
					<td><A class="Link2" href="WorkflowAdmin.aspx#Status">Actions</A></td>
				</tr>
			</TABLE>
			<asp:literal id="litRedirect" runat="server"></asp:literal></TD>
	</TR>
</TABLE>
<input id="txtSelected" style="VISIBILITY: hidden" type="text">
<script language="javascript">
	document.all.docHeader_btnOptions.src = "./Images/toolbar_options_grey.gif";
</script>

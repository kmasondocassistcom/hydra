<%@ Control Language="vb" AutoEventWireup="false" Codebehind="AnnotationToolbar.ascx.vb" Inherits="docAssistWeb.AnnotationToolbar" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<TABLE class="Transparent" id="tblAnnotation" cellSpacing="0" cellPadding="0" width="25"
	border="0" runat="server">
	<TR style="BACKGROUND-COLOR: transparent">
		<TD vAlign="middle" style="DISPLAY: none">
			<IMG id="imgEditing" src="images/editing.gif" border="0">
		</TD>
	</TR>
	<TR style="BACKGROUND-COLOR: transparent">
		<TD vAlign="middle">
			<A onclick="Point();" style="CURSOR: hand"><IMG id="imgPointer" alt="Pointer" src="Images/bot10.gif" border="0" height="22" width="25"></A>
		</TD>
	</TR>
	<TR style="BACKGROUND-COLOR: transparent">
		<TD vAlign="middle">
			<A onclick="javascript:rotateDegrees=90;showRotate('Rotate Left');" style="CURSOR: hand">
				<IMG id="imgRotateL" alt="Rotate Left" src="../Images/bot14off.gif" border="0" height="22"
					width="25" runat="server"></A>
		</TD>
	</TR>
	<TR style="BACKGROUND-COLOR: transparent">
		<TD vAlign="middle">
			<A onclick="javascript:rotateDegrees=-90;showRotate('Rotate Right')" style="CURSOR: hand">
				<IMG id="imgRotateR" alt="Rotate Right" src="../Images/bot15off.gif" border="0" height="22"
					width="25" runat="server"></A>
		</TD>
	</TR>
	<TR style="BACKGROUND-COLOR: transparent">
		<TD vAlign="middle">
			<A onclick="javascript:rotateDegrees=180;showRotate('Flip');" style="CURSOR: hand"><IMG id="imgFlip" alt="Flip" src="../Images/bot16off.gif" border="0" height="22" width="25"
					runat="server"></A>
		</TD>
	</TR>
	<TR style="BACKGROUND-COLOR: transparent" id="MovePages" runat="server">
		<TD vAlign="middle">
			<img src="images/move_pages.gif" height="22" width="25" alt="Move Pages" onclick="showMovePages();"
				style="CURSOR: hand">
		</TD>
	</TR>
	<TR style="BACKGROUND-COLOR: transparent" id="AddPages" runat="server">
		<TD vAlign="middle">
			<a id="addPgs" runat="server"><img src="images/addpage.gif" border="0" style="CURSOR: hand"></a>
			<asp:ImageButton id="imgAdd" runat="server" ImageUrl="images/addpage.gif" Height="22" Width="25"
				ToolTip="Add Page(s)" BorderColor="Transparent" BackColor="Transparent" BorderStyle="None"></asp:ImageButton>
		</TD>
	</TR>
	<TR style="BACKGROUND-COLOR: transparent" id="Delete" runat="server">
		<TD vAlign="middle">
			<A onclick="javascript:Delete();" style="CURSOR: hand"><IMG id="imgDelete" alt="Delete Page(s)" src="images/deletepage.gif" border="0" height="22"
					width="25"></A>
		</TD>
	</TR>
	<TR style="BACKGROUND-COLOR: transparent" id="Split" runat="server">
		<TD vAlign="middle" id="tdSplit" runat="server">
			<A onclick="javascript:Split();" style="CURSOR: hand"><IMG id="imgSplit" alt="Split Document" src="../images/split.gif" border="0" height="22"
					width="25" runat="server"></A>
		</TD>
	</TR>
	<TR style="BACKGROUND-COLOR: transparent" id="StickyNote" runat="server">
		<TD vAlign="middle">
			<A onclick="javascript:Sticky();return false;" style="CURSOR: hand"><IMG id="imgStickNote" alt="Sticky Note" src="images/postitoff.gif" border="0" height="22"
					width="25"></A>
		</TD>
	</TR>
	<TR style="BACKGROUND-COLOR: transparent" id="DrawingTool" runat="server">
		<TD vAlign="middle">
			<A onclick="javascript:Drawing();" style="CURSOR: hand"><IMG id="imgDrawingTool" alt="Drawing Tool" src="images/penciloff.gif" border="0" height="22"
					width="25"></A>
		</TD>
	</TR>
	<TR style="BACKGROUND-COLOR: transparent" id="TextTool" runat="server">
		<TD vAlign="middle">
			<A onclick="javascript:Text();" style="CURSOR: hand"><IMG id="imgTextTool" alt="Text Tool" src="images/textoff.gif" border="0" height="22"
					width="25"></A>
		</TD>
	</TR>
	<TR style="BACKGROUND-COLOR: transparent" id="Highlighter" runat="server">
		<TD vAlign="middle">
			<A onclick="javascript:Highlighter();" style="CURSOR: hand"><IMG id="imgHighlighter" alt="Highlighter Tool" src="images/highlighteroff.gif" border="0"
					height="22" width="25"></A>
		</TD>
	</TR>
	<TR style="BACKGROUND-COLOR: transparent; DISPLAY: none" id="Stamp" runat="server">
		<TD vAlign="middle">
			<A onclick="javascript:Stamp();parent.frames['frameImage'].StampTool();" style="CURSOR: hand">
				<IMG id="imgStampTool" alt="Stamp Tool" src="images/stampoff.gif" border="0" height="22"
					width="25"></A>
		</TD>
	</TR>
	<TR style="BACKGROUND-COLOR: transparent;DISPLAY: none" id="HideAnnotations">
		<TD vAlign="middle">
			<A onclick="TogAnn();" style="CURSOR: hand"><IMG id="imgToggleAnnotations" alt="Hide Annotations" src="Images/hideannotation.gif"
					border="0" height="22" width="25"></A>
		</TD>
	</TR>
	<TR style="BACKGROUND-COLOR: transparent;DISPLAY: none" id="ClearAnnotations" runat="server">
		<TD vAlign="middle">
			<A onclick="javascript:parent.frames['frameImage'].DeleteAnnotations();" style="CURSOR: hand">
				<IMG id="imgDeleteAnnotations" alt="Clear Annotations" src="Images/clearannotations.gif"
					border="0" height="22" width="25"></A>
		</TD>
	</TR>
	<TR style="BACKGROUND-COLOR: transparent;DISPLAY: none" id="RedactionHeader" runat="server">
		<TD vAlign="middle">
			<IMG id="imgRedacting" src="images/redact.gif" border="0">
		</TD>
	</TR>
	<TR style="BACKGROUND-COLOR: transparent;DISPLAY: none" id="Redaction" runat="server">
		<TD vAlign="middle">
			<A onclick="Redact();" style="CURSOR: hand"><IMG id="imgRedaction" alt="Redaction" src="images/redactionoff.gif" border="0" height="22"
					width="25"></A>
		</TD>
	</TR>
	<TR style="BACKGROUND-COLOR: transparent;DISPLAY: none" id="SelectRedactions" runat="server">
		<TD vAlign="middle">
			<A onclick="javascript:SelectRedaction();parent.frames['frameImage'].SelectRedaction();parent.frames['frameImage'].ZoomMode(0);"
				style="CURSOR: hand"><IMG id="imgSelectRedaction" alt="Select Redactions" src="images/selectredaction_off.gif"
					border="0" height="22" width="25"></A>
		</TD>
	</TR>
</TABLE>
<asp:Literal id="litAnnotation" runat="server"></asp:Literal>
<asp:TextBox id="PageCount" runat="server" Width="0px" Height="0px"></asp:TextBox>
<asp:Literal id="litRedaction" runat="server"></asp:Literal>
<script language="javascript">
	

	
	function GetPageNumber()
	{
		document.all.objAnnotation_PageCount.value = parent.frames["frameImage"].PageCount();
	}
	
	function Annotation(val)
	{
		//alert(val);
	}
	
	function Redaction(val)
	{
		//alert(val);
	}

	function showMovePages()
	{
		var val = window.showModalDialog('MovePages.aspx','MovePages','resizable:no;status:no;dialogHeight:550px;dialogWidth:710px;scroll:no');
		
		if (val != undefined)
		{
			reloadActivate();
			ActivateSave();
			var pageNo = document.all.pageNo.value;
			reloadPage(pageNo);
			reloadThumbs();
		}
	}
	
</script>

<%@ Control Language="vb" AutoEventWireup="false" Codebehind="PrefNav.ascx.vb" Inherits="docAssistWeb.PrefNav" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<%@ Register TagPrefix="componentart" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<link href="../Styles.css" type="text/css">
<TABLE class="Navigation1" width="100%" border="0" height="100%">
	<TR class="MenuContainer">
		<TD valign="top" class="MenuContainer"><BR>
			<BR>
			<COMPONENTART:TREEVIEW id="treeNav" NodeCssClass="PrefNode" BorderStyle="None" BackColor="#F7F7F7" Templates="(Collection)"
				KeyboardEnabled="False" runat="server" CssClass="Prefs" ExpandImageUrl="./images/plus.gif" CollapseImageUrl="./images/minus.gif"
				NodeRowCssClass="PrefNode" SelectedNodeCssClass="PrefSelectedNodeText" SelectedNodeRowCssClass="PrefSelectedNode"
				NodeLabelPadding="3" NodeIndent="14" CollapseNodeOnSelect="False" Width="180px" AutoScroll="False"
				MarginWidth="0">
				<Nodes>
					<componentart:TreeViewNode Text="My Information" CssClass="MainSection" LabelPadding="0" Selectable="False"
						Expanded="True">
						<componentart:TreeViewNode ID="Personal" Text="Personal Information&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;"
							ClientSideCommand="loadPage('PersonalInfo.aspx');" LabelPadding="0"></componentart:TreeViewNode>
						<componentart:TreeViewNode Text="Change My Password&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;"
							ClientSideCommand="loadPage('ChangePwd.aspx');" LabelPadding="0"></componentart:TreeViewNode>
						<componentart:TreeViewNode Text="Change My Display&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;"
							ClientSideCommand="loadPage('UserPreferences.aspx');" LabelPadding="0"></componentart:TreeViewNode>
						<componentart:TreeViewNode Text="Scan Options&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;"
							ClientSideCommand="loadPage('ScanOptions.aspx');" LabelPadding="0" Visible="False"></componentart:TreeViewNode>
					</componentart:TreeViewNode>
					<componentart:TreeViewNode Value="mnuAlerts" ID="Notifications" Text="My Alerts" CssClass="MainSection" Selectable="False"
						Expanded="True">
						<componentart:TreeViewNode ID="WFAlerts" Text="Workflow Notifications&#160;&#160;&#160;&#160;&#160;&#160;" ClientSideCommand="loadPage('WorkflowNotifications.aspx');"></componentart:TreeViewNode>
						<componentart:TreeViewNode ID="FolderAlerts" Text="Folder Notifications&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;" ClientSideCommand="loadPage('FolderNotifications.aspx');"></componentart:TreeViewNode>
					</componentart:TreeViewNode>
				</Nodes>
			</COMPONENTART:TREEVIEW></TD>
	</TR>
</TABLE>

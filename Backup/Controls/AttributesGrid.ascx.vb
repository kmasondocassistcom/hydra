Imports Accucentric.docAssist
Imports Accucentric.docAssist.Data.Images

Partial Class AttributesGrid
    Inherits System.Web.UI.UserControl

    Dim mobjUser As Accucentric.docAssist.Web.Security.User
    Private mconSqlImage As New SqlClient.SqlConnection

    Dim dtAttributes As New DataTable
    Dim dtAvailableAttributes As New DataTable

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try

            Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
            Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))
            Me.mobjUser.AccountId = guidAccountId
            Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)

            If Not Page.IsPostBack Then

                Dim sqlCmd As SqlClient.SqlCommand
                Dim dtDocuments As New Data.Images.Documents
                sqlCmd = New SqlClient.SqlCommand("acsDocumentsByCabinetFolder", Me.mconSqlImage)
                sqlCmd.CommandType = CommandType.StoredProcedure
                sqlCmd.Parameters.Add("@UserName", Me.mobjUser.UserId)
                sqlCmd.Parameters.Add("@CabinetId", "0")
                sqlCmd.Parameters.Add("@FolderId", "0")

                Dim oDb As New Data.Images.db
                oDb.PopulateTable(dtDocuments, sqlCmd)

                Dim drDocument As Data.Images.DocumentsRow
                drDocument = dtDocuments.NewRow
                drDocument.DocumentId = 0
                drDocument.DocumentName = "<None>"
                dtDocuments.Rows.Add(drDocument)

                ddlDocuments.DataSource = dtDocuments
                ddlDocuments.DataTextField = dtDocuments.DocumentName.ColumnName
                ddlDocuments.DataValueField = dtDocuments.DocumentId.ColumnName
                ddlDocuments.DataBind()

                ddlDocuments.SelectedValue = 0

                Dim intDocumentId As Integer = ddlDocuments.SelectedValue

                sqlCmd.Parameters.Clear()
                sqlCmd.CommandText = "acsAttributesByDocumentId"
                Dim sqlDa As New SqlClient.SqlDataAdapter(sqlCmd)

                sqlCmd.CommandType = CommandType.StoredProcedure
                sqlCmd.Parameters.Add("@DocumentId", intDocumentId)
                Dim dt As New DataTable
                sqlDa.Fill(dt)

                dtAttributes = dt
                dtAvailableAttributes = dt

                dgAttributes.DataSource = dt
                dgAttributes.DataBind()

            End If

        Catch ex As Exception

        Finally

            If Me.mconSqlImage.State <> ConnectionState.Closed Then
                Me.mconSqlImage.Close()
                Me.mconSqlImage.Dispose()
            End If

        End Try

    End Sub

    Private Sub AttributeChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        Try

            Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
            Me.mconSqlImage.Open()

            Dim ddlAttribute As DropDownList = CType(sender, DropDownList)

            'Find parent container (row in datagrid)
            Dim dgRow As DataGridItem = CType(ddlAttribute.Parent.NamingContainer, DataGridItem)

            Dim lblLength As Label = dgRow.FindControl("lblLength")
            Dim lblAttributeDataType As Label = dgRow.FindControl("lblAttributeDataType")

            Dim sqlCmd As New SqlClient.SqlCommand("acsAttributesByDocumentId", Me.mconSqlImage)
            Dim sqlDa As New SqlClient.SqlDataAdapter(sqlCmd)
            sqlCmd.CommandType = CommandType.StoredProcedure
            sqlCmd.Parameters.Add("@DocumentId", ddlDocuments.SelectedValue)
            Dim dt As New DataTable
            sqlDa.Fill(dt)

            Dim dv As New DataView(dt)
            dv.RowFilter = "AttributeId=" & ddlAttribute.SelectedValue

            lblLength.Text = dv(0)("Length")
            lblAttributeDataType.Text = dv(0)("AttributeDataType")

            'Dim txtAttributeValue As Telerik.WebControls.RadMaskedTextBox = dgRow.FindControl("txtAttributeValue")
            'Dim txtAttributeDate As Telerik.WebControls.RadDateInput = dgRow.FindControl("txtAttributeDate")

            Dim txtAttributeValue As TextBox = dgRow.FindControl("txtAttributeValue")
            Dim txtAttributeDate As TextBox = dgRow.FindControl("txtAttributeDate")

            txtAttributeValue.Visible = False
            txtAttributeDate.Visible = False

            Select Case lblAttributeDataType.Text
                Case "String"
                    txtAttributeValue.Visible = True

                    If lblLength.Text <> "" Then
                        'Dim intLength As Integer = CInt(lblLength.Text)
                        'txtAttributeValue.Mask = StrDup(intLength, "a")
                    Else

                    End If

                Case "Date"
                    'txtAttributeDate.MinDate = Today
                    txtAttributeDate.Visible = True

                Case "Numeric"
                    txtAttributeValue.Visible = True

                    If lblLength.Text <> "" Then
                        Dim intLength As Integer = CInt(lblLength.Text)
                        'txtAttributeValue.Mask = StrDup(intLength, "#")
                    Else

                    End If

                Case Else
                    txtAttributeValue.Visible = True

                    'txtAttributeValue.Mask = StrDup(255, "a")

            End Select



        Catch ex As Exception

        Finally

            If Me.mconSqlImage.State <> ConnectionState.Closed Then
                Me.mconSqlImage.Close()
                Me.mconSqlImage.Dispose()
            End If

        End Try

    End Sub

    Private Sub ddlDocuments_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDocuments.SelectedIndexChanged

        Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
        Me.mconSqlImage.Open()

        Try

            Dim intDocumentId As Integer = ddlDocuments.SelectedValue

            Dim sqlCmd As New SqlClient.SqlCommand("acsAttributesByDocumentId", Me.mconSqlImage)

            Dim sqlDa As New SqlClient.SqlDataAdapter(sqlCmd)

            sqlCmd.CommandType = CommandType.StoredProcedure
            sqlCmd.Parameters.Add("@DocumentId", ddlDocuments.SelectedValue)
            sqlCmd.Parameters.Add("@UserId", Me.mobjUser.ImagesUserId)
            sqlDa.Fill(dtAvailableAttributes)

            sqlCmd = New SqlClient.SqlCommand("acsAttributesByDocumentId", Me.mconSqlImage)
            sqlDa = New SqlClient.SqlDataAdapter(sqlCmd)

            sqlCmd.Parameters.Clear()
            sqlCmd.CommandType = CommandType.StoredProcedure
            sqlCmd.Parameters.Add("@DocumentId", intDocumentId)
            sqlCmd.Parameters.Add("@UserId", Me.mobjUser.ImagesUserId)
            Dim dt As New DataTable
            sqlDa.Fill(dt)

            dtAttributes = dt

            dgAttributes.DataSource = dt
            dgAttributes.DataBind()

            If Me.mconSqlImage.State <> ConnectionState.Closed Then
                Me.mconSqlImage.Close()
                Me.mconSqlImage.Dispose()
            End If

            'Custom section for Upload.aspx to filter out workflow if it shoudln't show for a certain document.
            Try
                Dim wfPane As WorkflowPane = CType(CType(sender, DropDownList).Parent.Parent.Parent, DataGridItem).FindControl("WorkflowPane")

                If Not IsNothing(wfPane) Then

                    Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
                    Me.mconSqlImage.Open()

                    Dim objWorkflow As New Workflow(Me.mconSqlImage)

                    Dim ds As New DataSet
                    ds = objWorkflow.WFSubmitterWorkflowsGet(Me.mobjUser.ImagesUserId, , CInt(ddlDocuments.SelectedValue))

                    'Find workflow pane
                    If ds.Tables(0).Rows.Count > 0 Then
                        wfPane.Visible = True
                        wfPane.UserId = Me.mobjUser.ImagesUserId
                        wfPane.DocumentId = ddlDocuments.SelectedValue
                        wfPane.LoadWorkflows()
                    Else
                        wfPane.Visible = False
                    End If

                End If
            Catch ex As Exception

            End Try

        Catch ex As Exception

        Finally

            If Me.mconSqlImage.State <> ConnectionState.Closed Then
                Me.mconSqlImage.Close()
                Me.mconSqlImage.Dispose()
            End If

        End Try

    End Sub

    Private Sub dgAttributes_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgAttributes.ItemDataBound

        Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
        Me.mconSqlImage.Open()

        Try

            Dim ddlAttribute As DropDownList = e.Item.FindControl("ddlAttribute")
            Dim lblAttributeDataType As Label = e.Item.FindControl("lblAttributeDataType")
            Dim lblLength As Label = e.Item.FindControl("lblLength")

            Dim txtAttributeValue As TextBox = e.Item.FindControl("txtAttributeValue")
            Dim txtAttributeDate As TextBox = e.Item.FindControl("txtAttributeDate")

            If Not IsNothing(ddlAttribute) Then

                If lblLength.Text = "" Then
                    'New row, get info
                    Dim sqlCmd As New SqlClient.SqlCommand("acsAttributesByDocumentId", Me.mconSqlImage)
                    Dim sqlDa As New SqlClient.SqlDataAdapter(sqlCmd)
                    sqlCmd.CommandType = CommandType.StoredProcedure
                    sqlCmd.Parameters.Add("@DocumentId", ddlDocuments.SelectedValue)
                    Dim dt As New DataTable
                    sqlDa.Fill(dt)

                    Dim dv As New DataView(dt)
                    dv.RowFilter = "AttributeId=" & dt.Rows(0)("AttributeId")

                    lblLength.Text = dv(0)("Length")
                    lblAttributeDataType.Text = dv(0)("AttributeDataType")

                End If

                Select Case lblAttributeDataType.Text
                    Case "String"
                        txtAttributeValue.Visible = True

                        If lblLength.Text <> "" Then
                            'Dim intLength As Integer = CInt(lblLength.Text)
                            'txtAttributeValue.Mask = StrDup(intLength, "a")
                        Else

                        End If

                    Case "Date"
                        'txtAttributeDate.MinDate = Today
                        txtAttributeDate.Visible = True
                    Case "Numeric"
                        txtAttributeValue.Visible = True

                        If lblLength.Text <> "" Then
                            'Dim intLength As Integer = CInt(lblLength.Text)
                            'txtAttributeValue.Mask = StrDup(intLength, "#")
                        Else

                        End If

                    Case Else
                        txtAttributeValue.Visible = True

                        'txtAttributeValue.Mask = StrDup(255, "a")

                End Select

                ddlAttribute.DataTextField = "AttributeName"
                ddlAttribute.DataValueField = "AttributeId"
                ddlAttribute.DataSource = dtAvailableAttributes
                ddlAttribute.DataBind()
                ddlAttribute.SelectedIndex = e.Item.ItemIndex

            End If

        Catch ex As Exception

        Finally

            If Me.mconSqlImage.State <> ConnectionState.Closed Then
                Me.mconSqlImage.Close()
                Me.mconSqlImage.Dispose()
            End If

        End Try

    End Sub

    Private Sub btnAddAttribute_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddAttribute.Click

        Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
        Me.mconSqlImage.Open()

        Try

            Dim sqlCmd As New SqlClient.SqlCommand("acsAttributesByDocumentId", Me.mconSqlImage)

            Dim sqlDa As New SqlClient.SqlDataAdapter(sqlCmd)

            sqlCmd.CommandType = CommandType.StoredProcedure
            sqlCmd.Parameters.Add("@DocumentId", ddlDocuments.SelectedValue)
            sqlCmd.Parameters.Add("@UserId", Me.mobjUser.ImagesUserId)
            sqlDa.Fill(dtAvailableAttributes)

            Dim dtReverse As New DataTable
            dtReverse.Columns.Add(New DataColumn("AttributeId", GetType(System.Int32), Nothing, MappingType.Element))
            dtReverse.Columns.Add(New DataColumn("AttributeValue", GetType(System.String), Nothing, MappingType.Element))
            dtReverse.Columns.Add(New DataColumn("AttributeDataType", GetType(System.String), Nothing, MappingType.Element))
            dtReverse.Columns.Add(New DataColumn("AttributeName", GetType(System.String), Nothing, MappingType.Element))
            dtReverse.Columns.Add(New DataColumn("Length", GetType(System.Int32), Nothing, MappingType.Element))

            Dim dr As DataRow
            For Each dgrowitem As DataGridItem In dgAttributes.Items

                Dim ddlAttribute As DropDownList = dgrowitem.FindControl("ddlAttribute")

                Dim txtAttributeDate As TextBox = dgrowitem.FindControl("txtAttributeDate")
                Dim txtAttributeValue As TextBox = dgrowitem.FindControl("txtAttributeValue")

                Dim lblAttributeDataType As Label = dgrowitem.FindControl("lblAttributeDataType")
                Dim lblLength As Label = dgrowitem.FindControl("lblLength")

                If Not IsNothing(ddlAttribute) Then
                    dr = dtReverse.NewRow
                    dr("AttributeId") = ddlAttribute.SelectedValue
                    If lblAttributeDataType.Text = "Date" Then
                        dr("AttributeValue") = txtAttributeDate.Text
                    Else
                        dr("AttributeValue") = txtAttributeValue.Text
                    End If
                    dr("AttributeDataType") = lblAttributeDataType.Text
                    dr("AttributeName") = ddlAttribute.SelectedItem.Text
                    dr("Length") = lblLength.Text
                    dtReverse.Rows.Add(dr)
                End If

            Next

            dr = dtReverse.NewRow
            dtReverse.Rows.Add(dr)

            dtAttributes = dtReverse
            dgAttributes.DataSource = dtAttributes
            dgAttributes.DataBind()

        Catch ex As Exception

        Finally

            If Me.mconSqlImage.State <> ConnectionState.Closed Then
                Me.mconSqlImage.Close()
                Me.mconSqlImage.Dispose()
            End If

        End Try

    End Sub

    Private Sub dgAttributes_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgAttributes.ItemCreated

        Try

            Dim ddlAttribute As DropDownList = e.Item.FindControl("ddlAttribute")

            If Not IsNothing(ddlAttribute) Then

                AddHandler ddlAttribute.SelectedIndexChanged, AddressOf AttributeChanged

            End If

        Catch ex As Exception

        End Try

    End Sub

End Class
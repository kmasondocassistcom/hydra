<%@ Register TagPrefix="cc1" Namespace="OptGroupLists" Assembly="OptGroupLists" %>
<%@ Control Language="vb" AutoEventWireup="false" Codebehind="WorkflowPane.ascx.vb" Inherits="docAssistWeb.WorkflowPane" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<table cellSpacing="0" cellPadding="0" width="200" border="0">
	<tr>
		<td>Workflow<BR>
			<cc1:optgroupddl id="ddlWorkflow" AutoPostBack="True" Width="160px" runat="server"></cc1:optgroupddl></td>
	</tr>
	<tr>
		<td><asp:dropdownlist id="ddlQueues" Width="160px" runat="server"></asp:dropdownlist></td>
	</tr>
	<tr>
		<td>Notes:<br><asp:textbox id="txtNotes" Width="160px" runat="server" TextMode="MultiLine"></asp:textbox></td>
	</tr>
	<tr>
		<td><asp:checkbox id="chkUrgent" runat="server" Text="Urgent" CssClass=PageContent></asp:checkbox></td>
	</tr>
</table>

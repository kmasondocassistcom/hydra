<System.Diagnostics.DebuggerStepThrough()> _
Friend Class cookieImage
    Inherits Object

    Private cookie As HttpCookie

    Friend Sub New(ByRef page As Page)
        '
        ' Build the SearchAttributes cookies
        Me.cookie = New HttpCookie("docAssistImage")
        If Not page.Request.Cookies("docAssistImage") Is Nothing Then
            Dim cookieCollection As System.Collections.Specialized.NameValueCollection = page.Request.Cookies("docAssistImage").Values
            For intCount As Integer = 0 To cookieCollection.Keys.Count - 1
                Me.cookie(cookieCollection.Keys(intCount)) = cookieCollection.Item(intCount)
            Next
        End If
        page.Response.Cookies.Add(Me.cookie)
    End Sub

    Friend Sub Expire()
        Me.cookie.Expires = DateAdd(DateInterval.Day, -1, Now())
    End Sub

#Region "Friend Properties"

    Friend Property PageNo() As Integer
        Get
            If Not cookie("PageNo") Is Nothing Then
                Return cookie("PageNo")
            Else
                Return 1
            End If
        End Get
        Set(ByVal Value As Integer)
            cookie("PageNo") = Value
        End Set
    End Property

    Friend Property ServerFileName() As String
        Get
            Return cookie("ServerFileName")
        End Get
        Set(ByVal Value As String)
            cookie("ServerFileName") = Value
        End Set
    End Property

    Friend Property LocalFileName() As String
        Get
            Return cookie("LocalFileName")
        End Get
        Set(ByVal Value As String)
            cookie("LocalFileName") = Value
        End Set
    End Property

    Friend Property HideBackToResults() As Boolean
        Get
            If cookie("HideBackToResults") Is Nothing Then
                Return True
            Else
                Return cookie("HideBackToResults")
            End If
        End Get
        Set(ByVal Value As Boolean)
            cookie("HideBackToResults") = Value
        End Set
    End Property

    Friend Property ShowThumbnail() As Boolean
        Get
            If cookie("ShowThumbnail") Is Nothing Then
                Return False
            Else
                Return cookie("ShowThumbnail")
            End If
        End Get
        Set(ByVal Value As Boolean)
            cookie("ShowThumbnail") = Value
        End Set
    End Property

#End Region

End Class

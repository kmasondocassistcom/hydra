﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports Accucentric.docAssist.Data.Images

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class explorer
    Inherits System.Web.Services.WebService

#Region "Declarations"

    Private mobjUser As Accucentric.docAssist.Web.Security.User
    'Public Delegate Sub StoreSession(ByVal VersionId As Integer, ByVal ItemToStore As Object)

#End Region

    <WebMethod(EnableSession:=True)> _
     Public Function AddFolder(ByVal ParentId As String, ByVal FolderName As String) As String()

        FolderName = FolderName.Replace("/amp", "&")

        Dim conSqlCompany As New SqlClient.SqlConnection

        Try

            Dim Request As HttpRequest = Context.Request
            Me.mobjUser = BuildUserObject(Request.Cookies("docAssist"))
            conSqlCompany = Functions.BuildConnection(Me.mobjUser)

            Dim dtPermissions As New DataTable

            If ParentId.StartsWith("S") Then 'Share
                ParentId = (ParentId.Replace("S", "") * -1)
            Else 'Folder
                ParentId = ParentId.Replace("W", "")
            End If

            Dim objFolders As New Folders(conSqlCompany, False)

            Dim intNewFolderId As Integer

            intNewFolderId = objFolders.InsertFolder(Me.mobjUser.ImagesUserId, FolderName, ParentId, dtPermissions)

            If intNewFolderId < 0 Then
                Dim intCount As Integer = 1
                Dim tempName As String = FolderName
                Do While intNewFolderId < 0
                    FolderName = tempName & "[" & intCount.ToString & "]"
                    intNewFolderId = objFolders.InsertFolder(Me.mobjUser.ImagesUserId, FolderName, ParentId, dtPermissions)
                    intCount += 1
                Loop
            End If

            Dim drChild As DataRow
            drChild = dtPermissions.Rows(0)

            Dim sbFolderCommand As New System.Text.StringBuilder

            sbFolderCommand.Append("browseFolder('" & intNewFolderId.ToString & "');setValue('W" & intNewFolderId.ToString & "');")

            If drChild("CanAddFolder") = True Then
                sbFolderCommand.Append("enableFolder();")
            Else
                sbFolderCommand.Append("disableFolder();")
            End If

            If drChild("CanAddFilesToFolder") = True Then
                sbFolderCommand.Append("enableAdd();")
            Else
                sbFolderCommand.Append("disableAdd();")
            End If

            If drChild("CanRenameFolder") = True Then
                sbFolderCommand.Append("enableRename();")
            Else
                sbFolderCommand.Append("disableRename();")
            End If

            If drChild("CanDeleteFolder") = True Then
                sbFolderCommand.Append("enableDelete();")
            Else
                sbFolderCommand.Append("disableDelete();")
            End If

            If drChild("CanChangeSecurity") = True Then
                sbFolderCommand.Append("enableSecurity();")
            Else
                sbFolderCommand.Append("disableSecurity();")
            End If

            sbFolderCommand.Append("enableFavorite();disableCabinet();")

            Dim values(3) As String
            values(0) = intNewFolderId.ToString
            values(1) = sbFolderCommand.ToString
            values(2) = FolderName

            Return values

        Catch ex As Exception

        Finally

            If conSqlCompany.State <> ConnectionState.Closed Then
                conSqlCompany.Close()
                conSqlCompany.Dispose()
            End If

        End Try

    End Function

End Class
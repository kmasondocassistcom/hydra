<%@ Register TagPrefix="uc1" TagName="AdminNav" Src="Controls/AdminNav.ascx" %>
<%@ Register TagPrefix="uc1" TagName="TopRight" Src="Controls/TopRight.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="WorkflowAdmin.aspx.vb" Inherits="docAssistWeb.WorkflowAdmin"%>
<%@ Register TagPrefix="uc1" TagName="docHeader" Src="Controls/docHeader.ascx" %>
<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="Controls/MenuBar.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<TITLE>docAssist - Workflow Configuration</TITLE>
		<META content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<META content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<META content="JavaScript" name="vs_defaultClientScript">
		<META content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<META http-equiv="cache-control" content="no-cache">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<BODY>
		<FORM id="Form1" method="post" runat="server">
			<INPUT id="hostname" type="hidden" runat="server">
			<TABLE height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD width="100%" colSpan="6"><UC1:DOCHEADER id="docHeader" runat="server"></UC1:DOCHEADER></TD>
					<TD width="100%"></TD>
				</TR>
				<TR vAlign="middle" height="100%">
					<TD class="Navigation1" style="BORDER-RIGHT: #cccccc thin solid; WIDTH: 171px" vAlign="top"
						height="100%"><UC1:ADMINNAV id="AdminNav1" runat="server"></UC1:ADMINNAV></TD>
					<td><img src="spacer.gif" height="1" width="25"></td>
					<TD vAlign="top" width="100%"><BR>
						<a name="Category">
							<table id="tblCategoriesHeader" cellSpacing="0" cellPadding="0" width="100%" border="0">
								<TR align="right">
									<TD></TD>
									<td class="PageContent" align="left" width="100%">
										<img src="Images/categories.gif"> <A class="PageContent" style="TEXT-ALIGN: right" href="javascript:AddCategory();">
											Add...</A></td>
									<TD>&nbsp;</TD>
									<td></td>
								</TR>
							</table>
						</a>
						<ASP:DATAGRID id="dgCategories" runat="server" AutoGenerateColumns="False" CSSCLASS="PageContent"
							Width="100%">
							<SelectedItemStyle Height="20px"></SelectedItemStyle>
							<AlternatingItemStyle CssClass="TableAlternateItem" BackColor="#E0E0E0"></AlternatingItemStyle>
							<ItemStyle Height="20px"></ItemStyle>
							<HeaderStyle Font-Bold="True" Height="20px" CssClass="TableHeader"></HeaderStyle>
							<Columns>
								<asp:TemplateColumn>
									<HeaderStyle Width="60px"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id=lblCategoryId runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.CategoryId") %>'>
										</asp:Label>
										<asp:Label id=lblCatAllowDelete runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.AllowDelete") %>'>
										</asp:Label>&nbsp;
										<asp:HyperLink id=lnEditCategory runat="server" ToolTip="Edit Category" NavigateUrl='<%# "javascript:EditCategory(" + CStr(DataBinder.Eval(Container, "DataItem.CategoryId")) + ");" %>'>
											<img src="Images/editIcon.gif" border="0"></asp:HyperLink><IMG height="1" src="spacer.gif" width="5">
										<asp:LinkButton id="lnkDeleteCategory" runat="server" ToolTip="Remove Category" CommandName="Delete">
											<img src="Images/trash.gif" border="0"></asp:LinkButton><IMG height="1" src="spacer.gif" width="10">
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:BoundColumn DataField="CategoryName" HeaderText="Category"></asp:BoundColumn>
							</Columns>
						</ASP:DATAGRID><BR>
						<a name="Workflow">
							<table id="tblWFsHeader" cellSpacing="0" cellPadding="0" width="100%" border="0">
								<TR align="right">
									<td class="PageContent" align="left" width="100%"><IMG src="Images/workflows.gif">&nbsp;<A class="PageContent" style="TEXT-ALIGN: right" href="javascript:AddWorkflow();">Add...</A></td>
									<TD>&nbsp;</TD>
								</TR>
							</table>
						</a>
						<ASP:DATAGRID id="dgWorkflows" runat="server" CSSCLASS="PageContent" AutoGenerateColumns="False"
							Width="100%">
							<AlternatingItemStyle CssClass="TableAlternateItem" BackColor="#E0E0E0"></AlternatingItemStyle>
							<ItemStyle Height="20px"></ItemStyle>
							<HeaderStyle Font-Bold="True" Height="20px" CssClass="TableHeader"></HeaderStyle>
							<Columns>
								<asp:TemplateColumn>
									<HeaderStyle Width="60px"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id=lblWorkflowId runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.WorkflowId") %>' Visible="False">
										</asp:Label>
										<asp:Label id=lblWFAllowDelete runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AllowDelete") %>' Visible="False">
										</asp:Label>&nbsp;
										<asp:HyperLink id=lnEditWorkflow runat="server" NavigateUrl='<%# "javascript:EditWorkflow(" + CStr(DataBinder.Eval(Container, "DataItem.WorkflowId")) + ");" %>' ToolTip="Edit Workflow">
											<img src="Images/editIcon.gif" border="0"></asp:HyperLink><IMG height="1" src="spacer.gif" width="5">
										<asp:LinkButton id="lnkDeleteWorkflow" runat="server" CommandName="Delete" ToolTip="Remove Workflow">
											<img src="Images/trash.gif" border="0"></asp:LinkButton><IMG height="1" src="spacer.gif" width="10">
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:BoundColumn DataField="CategoryName" HeaderText="Category"></asp:BoundColumn>
								<asp:BoundColumn DataField="WorkflowName" HeaderText="Workflow"></asp:BoundColumn>
								<asp:TemplateColumn HeaderText="Enabled">
									<HeaderStyle HorizontalAlign="Center" Width="70px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
									<ItemTemplate>
										<asp:CheckBox id="chkWFEnabled" runat="server" Enabled="False" Checked='<%# DataBinder.Eval(Container, "DataItem.WorkflowEnabled") %>'>
										</asp:CheckBox>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:TextBox id=TextBox1 runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Enabled") %>'>
										</asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
							</Columns>
						</ASP:DATAGRID><BR>
						<a name="Group">
							<table id="tblGroupsHeader" cellSpacing="0" cellPadding="0" width="100%" border="0">
								<TR align="right">
									<TD></TD>
									<td class="PageContent" align="left" width="100%"><IMG src="Images/groups.gif">&nbsp;<A class="PageContent" style="TEXT-ALIGN: right" href="javascript:AddGroup();">Add...</A></td>
									<TD>&nbsp;</TD>
									<td></td>
								</TR>
							</table>
						</a>
						<ASP:DATAGRID id="dgGroups" runat="server" CSSCLASS="PageContent" AutoGenerateColumns="False"
							Width="100%">
							<AlternatingItemStyle CssClass="TableAlternateItem" BackColor="#E0E0E0"></AlternatingItemStyle>
							<ItemStyle Height="20px"></ItemStyle>
							<HeaderStyle Font-Bold="True" Height="20px" CssClass="TableHeader"></HeaderStyle>
							<Columns>
								<asp:TemplateColumn>
									<HeaderStyle Width="60px"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id=lblGroupId runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.GroupId") %>' Visible="False">
										</asp:Label>
										<asp:Label id=lblAllowDeleteGroups runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AllowDelete") %>' Visible="False">
										</asp:Label>&nbsp;
										<asp:HyperLink id=lnkEditGroup runat="server" NavigateUrl='<%# "javascript:EditGroup(" + CStr(DataBinder.Eval(Container, "DataItem.GroupId")) + ");" %>' ToolTip="Edit Group">
											<img src="Images/editIcon.gif" border="0"></asp:HyperLink><IMG height="1" src="spacer.gif" width="5">
										<asp:LinkButton id="lnkDeleteGroup" runat="server" CommandName="Delete" ToolTip="Remove Group">
											<img src="Images/trash.gif" border="0"></asp:LinkButton><IMG height="1" src="spacer.gif" width="10">
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:BoundColumn DataField="GroupName" HeaderText="Group"></asp:BoundColumn>
								<asp:TemplateColumn HeaderText="Enabled">
									<HeaderStyle HorizontalAlign="Center" Width="70px"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
									<ItemTemplate>
										<asp:CheckBox id=chkGroupEnabled runat="server" Enabled="False" Checked='<%# DataBinder.Eval(Container, "DataItem.GroupEnabled") %>'>
										</asp:CheckBox>
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
						</ASP:DATAGRID><BR>
						<a name="Queue">
							<table id="tblQueues" cellSpacing="0" cellPadding="0" width="100%" border="0">
								<TR align="right">
									<TD></TD>
									<td class="PageContent" align="left" width="100%"><IMG src="Images/queues.gif">&nbsp;<A id="QueueAdd" runat="server" class="PageContent" style="TEXT-ALIGN: right" href="javascript:AddQueue();">Add...</A>
									<td>
									&nbsp;
									<TD></TD>
									<td></td>
								</TR>
							</table>
						</a>
						<ASP:DATAGRID id="dgQueues" runat="server" CSSCLASS="PageContent" AutoGenerateColumns="False"
							Width="100%">
							<AlternatingItemStyle CssClass="TableAlternateItem" BackColor="#E0E0E0"></AlternatingItemStyle>
							<ItemStyle Height="20px"></ItemStyle>
							<HeaderStyle Font-Bold="True" Height="20px" CssClass="TableHeader"></HeaderStyle>
							<Columns>
								<asp:TemplateColumn>
									<HeaderStyle Width="60px"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id=lblQueueId runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.QueueId") %>' Visible="False">
										</asp:Label>
										<asp:Label id=lblQueueAllowDelete runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AllowDelete") %>' Visible="False">
										</asp:Label>&nbsp;
										<asp:HyperLink id=lnkEditQueue runat="server" NavigateUrl='<%# "javascript:EditQueue(" + CStr(DataBinder.Eval(Container, "DataItem.QueueId")) + ");" %>' ToolTip="Edit Queue">
											<img src="Images/editIcon.gif" border="0"></asp:HyperLink><IMG height="1" src="spacer.gif" width="5">
										<asp:LinkButton id="lnkDeleteQueue" runat="server" CommandName="Delete" ToolTip="Remove Queue">
											<img src="Images/trash.gif" border="0"></asp:LinkButton><IMG height="1" src="spacer.gif" width="10">
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:BoundColumn DataField="CategoryName" HeaderText="Category"></asp:BoundColumn>
								<asp:BoundColumn DataField="QueueCode" HeaderText="Queue Code">
									<HeaderStyle Width="300px"></HeaderStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="QueueName" HeaderText="Queue Name"></asp:BoundColumn>
							</Columns>
						</ASP:DATAGRID>
						<asp:Label id="lblNoGroups" runat="server" Width="100%" Visible="False" Font-Size="8.25" Font-Name="Verdana"></asp:Label><BR>
						<BR>
						<a name="Status">
							<table id="tblStatusId" cellSpacing="0" cellPadding="0" width="100%" border="0">
								<TR align="right">
									<TD></TD>
									<td class="PageContent" align="left" width="100%"><IMG src="Images/actions.gif">&nbsp;<A class="PageContent" style="TEXT-ALIGN: right" href="javascript:AddStatus();">Add...</A></td>
									<TD>&nbsp;</TD>
									<td></td>
								</TR>
							</table>
							<ASP:DATAGRID id="dgStatus" runat="server" CSSCLASS="PageContent" AutoGenerateColumns="False"
								Width="100%">
								<AlternatingItemStyle CssClass="TableAlternateItem" BackColor="#E0E0E0"></AlternatingItemStyle>
								<ItemStyle Height="20px"></ItemStyle>
								<HeaderStyle Font-Bold="True" Height="20px" CssClass="TableHeader"></HeaderStyle>
								<Columns>
									<asp:TemplateColumn>
										<HeaderStyle Width="60px"></HeaderStyle>
										<ItemTemplate>
											<asp:Label id=lblStatusId runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.StatusId") %>' Visible="False">
											</asp:Label>&nbsp;
											<asp:HyperLink id=lnkEditStatus runat="server" NavigateUrl='<%# "javascript:EditStatus(" + CStr(DataBinder.Eval(Container, "DataItem.StatusId")) + ");" %>' ToolTip="Edit Action">
												<img src="Images/editIcon.gif" border="0"></asp:HyperLink><IMG height="1" src="spacer.gif" width="5">
											<asp:LinkButton id="lnkDeleteStatus" runat="server" CommandName="Delete" ToolTip="Remove Action">
												<img src="Images/trash.gif" border="0"></asp:LinkButton><IMG height="1" src="spacer.gif" width="10">
										</ItemTemplate>
									</asp:TemplateColumn>
									<asp:BoundColumn DataField="StatusDesc" HeaderText="Action"></asp:BoundColumn>
								</Columns>
							</ASP:DATAGRID><BR>
						</a>
					</TD>
					<TD><img src="spacer.gif" height="1" width="25"></TD>
				<tr>
					<td colSpan="5"><uc1:topright id="docFooter" runat="server"></uc1:topright></td>
					<td></td>
				</tr>
			</TABLE>
			</A>
		</FORM>
		<script language="javascript">

			function AddStatus()
			{
				var nXpos = (document.body.clientWidth - 400) / 2; 
				var nYpos = (document.body.clientHeight - 200) / 2;
				window.open('WorkflowStatus.aspx?ID=0'+'&Mode=0','AddStatus','height=200,width=400,resizable=yes,status=no,toolbar=no,top='+nYpos+',left='+nXpos)
			}
			function EditStatus(StatusId)
			{
					var nXpos = (document.body.clientWidth - 400) / 2; 
					var nYpos = (document.body.clientHeight - 200) / 2;
					window.open('WorkflowStatus.aspx?ID='+StatusId+'&Mode=1','EditStatus','height=200,width=400,resizable=yes,status=no,toolbar=no,top='+nYpos+',left='+nXpos)
			}
			function EditCategory(CategoryId)
			{
					var nXpos = (document.body.clientWidth - 360) / 2; 
					var nYpos = (document.body.clientHeight - 145) / 2;
					window.open('WorkflowCategoriesEdit.aspx?ID='+CategoryId+'&Mode=1','EditCategory','height=145,width=360,resizable=yes,status=no,toolbar=no,top='+nYpos+',left='+nXpos)
			}
			function AddCategory()
			{
					var nXpos = (document.body.clientWidth - 360) / 2; 
					var nYpos = (document.body.clientHeight - 145) / 2;
				window.open('WorkflowCategoriesEdit.aspx?ID=0'+'&Mode=0','AddCategory','height=145,width=360,resizable=yes,status=no,toolbar=no,top='+nYpos+',left='+nXpos)
			}
			function CATDeleteError()
			{
				alert('Cannot delete this category because it contains workflow(s).');
			}
		</script>
		<script language="javascript">
			function EditWorkflow(WorkflowId)
			{
					var nXpos = (document.body.clientWidth - 560) / 2; 
					var nYpos = (document.body.clientHeight - 930) / 2;
					window.open('WorkflowWorkflowsEdit.aspx?ID='+WorkflowId+'&Mode=1','EditWorkflow','scrollbars=no,height=615,width=545,resizable=no,status=no,toolbar=no,top='+nYpos+',left='+nXpos)
			}
			function AddWorkflow()
			{
				var nXpos = (document.body.clientWidth - 560) / 2; 
				var nYpos = (document.body.clientHeight - 930) / 2;
				window.open('WorkflowWorkflowsEdit.aspx?ID=0'+'&Mode=0','AddWorkflow','scrollbars=no,height=615,width=545,resizable=no,status=no,toolbar=no,top='+nYpos+',left='+nXpos)
			}
			function WFDeleteError()
			{
				alert('Cannot delete this workflow.');
			}
		</script>
		<script language="javascript">
			function EditGroup(CategoryId)
			{
				var nXpos = (document.body.clientWidth - 540) / 2; 
				var nYpos = (document.body.clientHeight - 400) / 2;
				window.open('WorkflowGroupsEdit.aspx?ID='+CategoryId+'&Mode=1','EditGroup','height=400,width=540,resizable=yes,status=no,toolbar=no,top='+nYpos+',left='+nXpos)
			}
			function AddGroup()
			{
				var nXpos = (document.body.clientWidth - 540) / 2; 
				var nYpos = (document.body.clientHeight - 400) / 2;
				window.open('WorkflowGroupsEdit.aspx?ID=0'+'&Mode=0','AddGroup','height=400,width=540,resizable=yes,status=no,toolbar=no,top='+nYpos+',left='+nXpos)
			}
			function GRPDeleteError()
			{
				alert('Cannot delete this group.');
			}
		</script>
		<script language="javascript">
			function EditQueue(QueueId)
			{
				var nXpos = (document.body.clientWidth - 520) / 2; 
				var nYpos = (document.body.clientHeight - 700) / 2;
				window.open('WorkflowQueuesEdit.aspx?ID='+QueueId+'&Mode=1','EditQueue','scrollbars=yes,height=700,width=520,resizable=yes,status=no,toolbar=no,top='+nYpos+',left='+nXpos)
			}
			function AddQueue()
			{
				var nXpos = (document.body.clientWidth - 520) / 2; 
				var nYpos = (document.body.clientHeight - 700) / 2;
				window.open('WorkflowQueuesEdit.aspx?ID=0'+'&Mode=0','AddQueue','scrollbars=yes,height=700,width=520,resizable=yes,status=no,toolbar=no,top='+nYpos+',left='+nXpos)
			}
			function QUEDeleteError()
			{
				alert('Cannot delete queue.');
			}
		</script>
	</BODY>
</HTML>

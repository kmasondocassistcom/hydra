Imports System.Web.Services
Imports Accucentric.docAssist.Data.Images

<System.Web.Services.WebService(Namespace:="http://docassist.com/TFIntegration")> _
Public Class TFIntegration
    Inherits System.Web.Services.WebService

    Private conCompany As New SqlClient.SqlConnection
    Private conMaster As New SqlClient.SqlConnection

#Region " Web Services Designer Generated Code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Web Services Designer.
        InitializeComponent()

        'Add your own initialization code after the InitializeComponent() call

    End Sub

    'Required by the Web Services Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Web Services Designer
    'It can be modified using the Web Services Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        components = New System.ComponentModel.Container
    End Sub

    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        'CODEGEN: This procedure is required by the Web Services Designer
        'Do not modify it using the code editor.
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

#End Region

    <WebMethod()> _
    Public Function TFGetDocumentsAndAttributes(ByVal TFIntegrationKey As String) As DataSet

        Try

            If TFIntegrationKey.Trim.Length = 0 Then
                Throw New System.Web.Services.Protocols.SoapException("No integration ID provided.", New System.Xml.XmlQualifiedName("docAssist"))
                Exit Function
            End If

            'Build master connection
            conMaster = Functions.BuildConnection(Functions.ConnectionType.Master)
            If conMaster.State <> ConnectionState.Open Then conMaster.Open()

            Dim timeForge As New Timeforge(conMaster, False)

            Dim account As DataTable = timeForge.GetAccountFromKey(TFIntegrationKey)

            If account.Rows.Count = 0 Then
                Throw New System.Web.Services.Protocols.SoapException("Invalid integration ID.", New System.Xml.XmlQualifiedName("docAssist"))
                Exit Function
            End If

            'Build company connection
            Dim masterPassword As String = Encryption.Encryption.Decrypt(ConfigurationSettings.AppSettings("MasterPW"))
            Dim connectionString As String = Functions.BuildConnectionString(account.Rows(0)("DBServerDNS"), account.Rows(0)("DBName"), "bluprint", masterPassword, "TimeForge")
            conCompany = New SqlClient.SqlConnection(connectionString)
            conCompany.Open()

            Dim tf As New Timeforge(conCompany, False)

            Return tf.TFDocumentAttributeList

        Catch ex As Exception

            Dim conMaster As SqlClient.SqlConnection = Functions.BuildConnection(Functions.ConnectionType.Master)
            Functions.LogException("", "", "", "TFGetDocumentsAndAttributes: " & TFIntegrationKey & " " & ex.Message, ex.StackTrace, Server.MachineName, "", "", conMaster, True)

            Throw New System.Web.Services.Protocols.SoapException("Error loading documents and attributes.", New System.Xml.XmlQualifiedName("docAssist"))

        Finally

            If conMaster.State <> ConnectionState.Closed Then
                conMaster.Close()
                conMaster.Dispose()
            End If

            If conCompany.State <> ConnectionState.Closed Then
                conCompany.Close()
                conCompany.Dispose()
            End If

        End Try

    End Function

    <WebMethod()> _
    Public Function TFDownloadDocument(ByVal TFIntegrationKey As String, ByVal ImageID As Integer) As String

        Try

            If TFIntegrationKey.Trim.Length = 0 Then
                Throw New System.Web.Services.Protocols.SoapException("No integration ID provided.", New System.Xml.XmlQualifiedName("docAssist"))
                Exit Function
            End If

            'Build master connection
            conMaster = Functions.BuildConnection(Functions.ConnectionType.Master)
            If conMaster.State <> ConnectionState.Open Then conMaster.Open()

            Dim timeForge As New Timeforge(conMaster, False)

            Dim account As DataTable = timeForge.GetAccountFromKey(TFIntegrationKey)

            If account.Rows.Count = 0 Then
                Throw New System.Web.Services.Protocols.SoapException("Invalid integration ID.", New System.Xml.XmlQualifiedName("docAssist"))
                Exit Function
            End If

            'Build company connection
            Dim masterPassword As String = Encryption.Encryption.Decrypt(ConfigurationSettings.AppSettings("MasterPW"))
            Dim connectionString As String = Functions.BuildConnectionString(account.Rows(0)("DBServerDNS"), account.Rows(0)("DBName"), "bluprint", masterPassword, "TimeForge")
            conCompany = New SqlClient.SqlConnection(connectionString)
            conCompany.Open()

            'Create PDF
            Dim file As String = System.Guid.NewGuid().ToString & ".pdf"
            Dim fileName As String = Server.MapPath("~/tmp/") & file
            Dim res As String = Functions.ExportPublicPDFByImageId(conCompany, ImageID, 1, 999999, fileName)

            If fileName = res Then
                If HttpContext.Current.Request.Url.ToString.StartsWith("http://dev.docassist") Then
                    Return "http://dev.docassist.com/tmp/" & file
                ElseIf HttpContext.Current.Request.Url.ToString.StartsWith("http://stage.docassist") Then
                    Return "http://stage.docassist.com/tmp/" & file
                ElseIf HttpContext.Current.Request.Url.ToString.StartsWith("https://stage.docassist") Then
                    Return "https://stage.docassist.com/tmp/" & file
                ElseIf HttpContext.Current.Request.Url.ToString.StartsWith("http://localhost") Then
                    Return "http://localhost/docAssistWeb1/tmp/" & file
                ElseIf HttpContext.Current.Request.Url.ToString.StartsWith("https://my.docassist") Then
                    Return "https://my.docassist.com/tmp/" & file
                End If
            End If

        Catch ex As Exception

            Dim conMaster As SqlClient.SqlConnection = Functions.BuildConnection(Functions.ConnectionType.Master)
            Functions.LogException("", "", "", "TFGetDocumentsAndAttributes: " & TFIntegrationKey & " " & ex.Message, ex.StackTrace, Server.MachineName, "", "", conMaster, True)

            Throw New System.Web.Services.Protocols.SoapException("Error loading documents and attributes.", New System.Xml.XmlQualifiedName("docAssist"))

        Finally

            If conMaster.State <> ConnectionState.Closed Then
                conMaster.Close()
                conMaster.Dispose()
            End If

            If conCompany.State <> ConnectionState.Closed Then
                conCompany.Close()
                conCompany.Dispose()
            End If

        End Try


    End Function

End Class

<%@ Register TagPrefix="cc1" Namespace="OptGroupLists" Assembly="OptGroupLists" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="WorkflowConsole.aspx.vb" Inherits="docAssistWeb.WorkflowConsole"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title></title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<table class="PageContent" id="tblConsole" height="235" cellSpacing="0" cellPadding="0"
				width="260" border="0">
				<tr height="100%">
					<td><iframe id="frameWorkflowConsole" runat="server" height="100%" width="100%" frameBorder="0"
							scrolling="no"></iframe>
					</td>
				</tr>
				<tr>
					<td colspan="1" vAlign="bottom" align="left"><img src="imgReview" height="20" width="50"><IMG height="20" src="imgHistory" width="50"></IFRAME>
					</td>
				</tr>
			</table>
			<asp:Literal id="litRouteId" runat="server"></asp:Literal>
		</form>
	</body>
</HTML>

<%@ Control Language="vb" AutoEventWireup="false" Codebehind="AttributeNavigation.ascx.vb" Inherits="docAssistWeb.AttributeNavigation" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<TABLE class="Navigation1" id="AttributeNavigation" cellSpacing="0" cellPadding="0" border="0"
	runat="server">
	<TR>
		<TD>&nbsp;<a href="#"><ASP:IMAGEBUTTON id="btnSave" runat="server" ImageUrl="Images/save.gif" Width="25" Height="22" ToolTip="Save"></ASP:IMAGEBUTTON></a></TD>
		<TD vAlign="middle"><IMG style="BACKGROUND-COLOR: gray" src="images/spacer.gif" width="2"></TD>
		<TD><A id="lnkVersionWindow" onclick="javascript:window.open('ImageVersions.aspx','Versions',&#13;&#10;&#9;&#9;&#9;&#9;&#9;&#9;'height=400,resizable=no,status=no,toolbar=no,width=550');return false;"
				href="#" runat="server"><IMG alt="Document History" src="Images/doc_hist.gif" border="0">
			</A>
		</TD>
		<TD><A onclick="javascript:textPopup();" href="#"><IMG id="ShowText" alt="View Document Text" src="Images/fulltext.gif" border="0" runat="server"><!-- this was img1 -->
			</A>
		</TD>
		<TD vAlign="middle"><IMG style="BACKGROUND-COLOR: gray" src="images/spacer.gif" width="2"></TD>
		<TD><A id="lnkResults" onmouseover="parent.window.status='Back to results';" onmouseout="window.status='';"
				href="javascript:BackToResults();" runat="server"><IMG id="imgBackToResults" alt="Return to Search Results" src="Images/back_to_results.gif"
					border="0"></A>
		</TD>
		<TD><ASP:IMAGEBUTTON id="btnPrevious" runat="server" ImageUrl="Images/results_previous.gif" ToolTip="Previous Result"></ASP:IMAGEBUTTON></TD>
		<TD><ASP:IMAGEBUTTON id="btnNext" runat="server" ImageUrl="Images/results_next.gif" ToolTip="Next Result"></ASP:IMAGEBUTTON></TD>
	</TR>
</TABLE>
<ASP:LITERAL id="litReload" runat="server"></ASP:LITERAL>
<SCRIPT language="javascript">
	function BackToResults()
	{
		//document.all.moveNext.value = "1";
		//parent.location.replace("Results.aspx");
	}
</SCRIPT>

Imports Accucentric.docAssist
Imports Accucentric.docAssist.Data.Images

Partial Class WorkflowAdmin
    'Inherits System.Web.UI.Page
    Inherits System.Web.UI.Page


    Private mobjUser As Web.Security.User

    Private mconSqlImage As SqlClient.SqlConnection

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Function BuildDataset() As DataSet

        Dim dsUsers As New DataSet
        Dim conSqlWeb As New SqlClient.SqlConnection(Functions.ConnectionString())
        '
        '' Image Users
        '     Dim cmdSql As New SqlClient.SqlCommand("SELECT [ImageUserId]=UserId, UserName, SecAdmin FROM Users", Me.mconSqlImage)
        'Dim da As New SqlClient.SqlDataAdapter(cmdSql)
        'If Not Me.mconSqlImage.State = ConnectionState.Open Then Me.mconSqlImage.Open()
        'da.Fill(dsUsers.ImageUsers)
        '
        ' Web Users
        Dim cmdSql As New SqlClient.SqlCommand
        Dim da As New SqlClient.SqlDataAdapter(cmdSql)
        cmdSql = New SqlClient.SqlCommand("acsUserListbyAccountID", mconSqlImage)
        cmdSql.CommandType = CommandType.StoredProcedure
        cmdSql.CommandTimeout = DEFAULT_COMMAND_TIMEOUT
        cmdSql.Parameters.Add("@AccountID", Me.mobjUser.AccountId)
        da = New SqlClient.SqlDataAdapter(cmdSql)
        If Not mconSqlImage.State = ConnectionState.Open Then mconSqlImage.Open()
        da.Fill(dsUsers)

        Me.mconSqlImage.Close()
        conSqlWeb.Close()

        Return dsUsers

    End Function
    Private Function BuildCategories() As DataTable

        Try
            Dim wfCategories As New Workflow(Functions.BuildConnection(Me.mobjUser))
            Return wfCategories.WFCategoryGet
        Catch ex As Exception

        End Try

    End Function
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try

            If Request.Cookies("docAssist") Is Nothing Then
                Response.Redirect("Default.aspx")
                Exit Sub
            End If

            Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
            Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))

            Me.mobjUser.AccountId = guidAccountId
            Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)

            If Not Me.mobjUser.SysAdmin Then
                Response.Redirect("Default.aspx")
            End If

            'Categories
            Dim dtCategories As New DataTable
            dtCategories = BuildCategories()
            dgCategories.DataSource = dtCategories
            dgCategories.DataBind()

            'Workflows
            Dim dtWorkflows As New DataTable
            dtWorkflows = BuildWorkflows()
            dgWorkflows.DataSource = dtWorkflows
            dgWorkflows.DataBind()

            'Groups
            Dim dtGroups As New DataTable
            dtGroups = BuildGroups()
            dgGroups.DataSource = dtGroups
            dgGroups.DataBind()

            If dgGroups.Items.Count = 0 Then
                lblNoGroups.Text = "You must add a group before you can add queues."
                lblNoGroups.Visible = True
                dgQueues.Visible = False
                QueueAdd.Visible = False
            Else
                lblNoGroups.Visible = False
                dgQueues.Visible = True
                QueueAdd.Visible = True
            End If

            'Queues
            Dim dtQueues As New DataTable
            dtQueues = BuildQueues()
            dgQueues.DataSource = dtQueues
            dgQueues.DataBind()

            'Status
            Dim dtStatus As New DataTable
            dtStatus = BuildStatus()
            dgStatus.DataSource = dtStatus
            dgStatus.DataBind()

        Catch ex As Exception
            If ex.Message = "DualLogin" Then
                Response.Cookies("docAssist")("DualLogin") = True
                Server.Transfer("Default.aspx")
            Else
                Response.Redirect("Default.aspx")
                Response.Flush()
                Response.End()
            End If
        End Try

    End Sub

    Private Function BuildStatus() As DataTable

        Try
            Dim wfQueues As New Workflow(Functions.BuildConnection(Me.mobjUser))
            Return wfQueues.WFStatusTypesGet()
        Catch ex As Exception

        End Try

    End Function
    Private Function BuildQueues() As DataTable

        Try
            Dim wfQueues As New Workflow(Functions.BuildConnection(Me.mobjUser))
            Return wfQueues.WFQueueGet
        Catch ex As Exception

        End Try

    End Function

    Private Function BuildGroups() As DataTable

        Try
            Dim wfCategories As New Workflow(Functions.BuildConnection(Me.mobjUser))
            Return wfCategories.WFGroupsGet
        Catch ex As Exception

        End Try

    End Function
    Private Function BuildWorkflows() As DataTable

        Try
            Dim wfWorkflows As New Workflow(Functions.BuildConnection(Me.mobjUser))
            Return wfWorkflows.WFWorkflowGet
        Catch ex As Exception

        End Try

    End Function

    Private Sub dgUsers_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

        'Dim sb As New System.Text.StringBuilder
        'sb.Append("<SCRIPT id=""Opener"" language=""javascript"">")
        'sb.Append("window.open('UserEdit.aspx','UserEdit','height=512,resizable=no,scrollbars=yes,status=yes,toolbar=no,width=520');")
        'sb.Append("</SCRIPT>")

        'Response.Cookies("docAssistWeb")("EditUserId") = CType(dgUsers.Items(dgUsers.SelectedIndex).FindControl("lblImageUserId"), Label).Text
        'dgUsers.SelectedIndex = -1
        'litScripts.Text = sb.ToString

    End Sub

    Private Sub dgUsers_DeleteCommand1(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)

        '        Dim conSqlWeb As New SqlClient.SqlConnection(Functions.ConnectionString())
        '        Dim dgRowItem As DataGridItem = e.Item
        '        Dim guidUserId As New System.Guid(CType(dgRowItem.FindControl("lblWebUserId"), Label).Text)
        '        Dim intUserId As Integer = CType(dgRowItem.FindControl("lblImageUserId"), Label).Text
        '        Try
        '            '
        '            ' Check if user is ok to remove
        '            Dim cmdSql As New SqlClient.SqlCommand("SELECT 1 FROM users WHERE UserId = @UserId AND (SecAdmin <> 1 OR (SELECT count(*) FROM users WHERE SecAdmin = 1) > 1)", Me.mconSqlImage)
        '            cmdSql.CommandTimeout = DEFAULT_COMMAND_TIMEOUT
        '            cmdSql.Parameters.Add("@UserId", intUserId)

        '            If Not Me.mconSqlImage.State = ConnectionState.Open Then Me.mconSqlImage.Open()
        '            Dim bolOk As Boolean = cmdSql.ExecuteScalar()

        '            If bolOk Then
        '                '
        '                ' Remove the SecAdmin priviledge
        '                cmdSql = New SqlClient.SqlCommand("UPDATE Users SET SecAdmin = 0 WHERE UserId = @UserId", Me.mconSqlImage)
        '                cmdSql.CommandTimeout = DEFAULT_COMMAND_TIMEOUT
        '                cmdSql.Parameters.Add("@UserId", intUserId)
        '                cmdSql.ExecuteNonQuery()
        '                '
        '                ' Remove the User record in images.
        '                'cmdSql = New SqlClient.SqlCommand("DELETE FROM Users WHERE UserId = @UserId", Me.mconSqlImage)
        '                'cmdSql.CommandTimeout = DEFAULT_COMMAND_TIMEOUT
        '                'cmdSql.Parameters.Add("@UserId", intUserId)
        '                'cmdSql.ExecuteNonQuery()
        '                '
        '                ' Remove the user from the account
        '                cmdSql = New SqlClient.SqlCommand("DELETE FROM UserAccounts WHERE UserId = @UserId AND AccountId = @AccountId", conSqlWeb)
        '                cmdSql.CommandTimeout = DEFAULT_COMMAND_TIMEOUT
        '                cmdSql.Parameters.Add("@UserId", guidUserId)
        '                cmdSql.Parameters.Add("@AccountId", Me.mobjUser.AccountId)
        '                conSqlWeb.Open()
        '                cmdSql.ExecuteNonQuery()
        '                '
        '                ' Log the removal
        '                LogUserAccounts(guidUserId, Me.mobjUser.AccountId, Me.mobjUser.UserId, "Remove", _
        '                    conSqlWeb)
        '            End If

        '            ' Dim dsUsers As dsAccountUsers = BuildDataset()
        '            Dim dsUsers As DataSet = BuildDataset()
        '            dgUsers.DataSource = dsUsers.Tables(0)
        '            dgUsers.DataBind()

        '        Catch ex As Exception
        '#If DEBUG Then
        '            lblErrorMessage.Text = ex.Message & "<BR>" & ex.StackTrace
        '#Else
        '            lblErrorMessage.Text = ex.Message
        '#End If
        '            lblErrorMessage.Visible = True
        '        Finally
        '            If Not Me.mconSqlImage.State = ConnectionState.Closed Then Me.mconSqlImage.Close()
        '            If Not conSqlWeb.State = ConnectionState.Closed Then conSqlWeb.Close()
        '        End Try

    End Sub


    Private Sub dgCategories_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgCategories.SelectedIndexChanged

    End Sub

    Private Sub dgCategories_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgCategories.DeleteCommand
        Try

            Dim dgRowItem As DataGridItem = e.Item
            Dim intCategoryId As Integer = CType(dgRowItem.FindControl("lblCategoryId"), Label).Text
            Dim blnAllowDelete As Boolean = CType(dgRowItem.FindControl("lblCatAllowDelete"), Label).Text

            If blnAllowDelete = True Then
                Dim wfCategory As New Workflow(Functions.BuildConnection(Me.mobjUser))
                wfCategory.WFCategoryDelete(intCategoryId)

                Dim dtCategories As New DataTable
                dtCategories = BuildCategories()
                dgCategories.DataSource = dtCategories
                dgCategories.DataBind()

                'ReturnScripts.Add(dgCategories)
            Else
                System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.NewGuid.ToString, "CATDelete();", True)

                'litDelete.Text = "<script language='javascript'>alert('Cannot delete this category because it contains workflow(s).');</script>"
            End If

        Catch ex As Exception

        End Try
    End Sub

    Private Sub dgGroups_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgGroups.SelectedIndexChanged

    End Sub

    Private Sub dgGroups_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgGroups.DeleteCommand
        Try

            Dim dgRowItem As DataGridItem = e.Item
            Dim intGroupId As Integer = CType(dgRowItem.FindControl("lblGroupId"), Label).Text
            Dim blnAllowDelete As Boolean = CType(dgRowItem.FindControl("lblAllowDeleteGroups"), Label).Text

            If blnAllowDelete = True Then
                Dim wfCategory As New Workflow(Functions.BuildConnection(Me.mobjUser))
                wfCategory.WFGroupsDeleteID(intGroupId)

                Dim dtGroups As New DataTable
                dtGroups = BuildGroups()
                dgGroups.DataSource = dtGroups
                dgGroups.DataBind()

                'ReturnScripts.Add(dgGroups)
            Else
                System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.NewGuid.ToString, "GRPDeleteError();", True)
            End If

        Catch ex As Exception

        End Try

    End Sub

    Private Sub dgWorkflows_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgWorkflows.SelectedIndexChanged

    End Sub

    Private Sub dgWorkflows_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgWorkflows.DeleteCommand

        Try

            Dim dgRowItem As DataGridItem = e.Item
            Dim intWFId As Integer = CType(dgRowItem.FindControl("lblWorkflowId"), Label).Text
            Dim blnAllowDelete As Boolean = CType(dgRowItem.FindControl("lblWFAllowDelete"), Label).Text

            If blnAllowDelete = True Then
                Dim wfCategory As New Workflow(Functions.BuildConnection(Me.mobjUser))
                wfCategory.WFWorkflowDeleteID(intWFId)

                'Workflows
                Dim dtWorkflows As New DataTable
                dtWorkflows = BuildWorkflows()
                dgWorkflows.DataSource = dtWorkflows
                dgWorkflows.DataBind()

                'ReturnScripts.Add(dgWorkflows)
            Else
                System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.NewGuid.ToString, "WFDeleteError();", True)
            End If

        Catch ex As Exception

        End Try

    End Sub

    Private Sub dgQueues_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgQueues.DeleteCommand
        'lblQueueAllowDelete

        Try

            Dim dgRowItem As DataGridItem = e.Item
            Dim intQueueId As Integer = CType(dgRowItem.FindControl("lblQueueId"), Label).Text
            Dim blnAllowDelete As Boolean = CType(dgRowItem.FindControl("lblQueueAllowDelete"), Label).Text

            If blnAllowDelete = True Then
                Dim wfCategory As New Workflow(Functions.BuildConnection(Me.mobjUser))
                wfCategory.WFQueueDeleteID(intQueueId)

                'Queues
                Dim dtQueues As New DataTable
                dtQueues = BuildQueues()
                dgQueues.DataSource = dtQueues
                dgQueues.DataBind()

                'ReturnScripts.Add(dgQueues)
            Else
                System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.NewGuid.ToString, "QUEDeleteError();", True)
            End If

        Catch ex As Exception

        End Try

    End Sub

    Private Sub dgStatus_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgStatus.SelectedIndexChanged

    End Sub

    Private Sub dgStatus_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgStatus.DeleteCommand
        'lblStatusId

        Try

            Dim dgRowItem As DataGridItem = e.Item
            Dim intStatusId As Integer = CType(dgRowItem.FindControl("lblStatusId"), Label).Text
            'Dim blnAllowDelete As Boolean = CType(dgRowItem.FindControl("lblQueueAllowDelete"), Label).Text

            'If blnAllowDelete = True Then
            If intStatusId <= 1000 Then
                System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.NewGuid.ToString, "alert('Cannot remove a system status.');", True)
            Else
                Dim wfCategory As New Workflow(Functions.BuildConnection(Me.mobjUser))
                wfCategory.WFStatusTypeDeleteID(intStatusId)

                'Status
                Dim dtStatus As New DataTable
                dtStatus = BuildStatus()
                dgStatus.DataSource = dtStatus
                dgStatus.DataBind()

                'ReturnScripts.Add(dgStatus)
            End If

            'Else
            '    ReturnScripts.Add("QUEDeleteError();")
            'End If

        Catch ex As Exception

        End Try
    End Sub

    Private Sub dgCategories_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgCategories.ItemDataBound

        Try
            Dim lnkDeleteCategory As LinkButton = CType(e.Item.FindControl("lnkDeleteCategory"), LinkButton)
            If Not lnkDeleteCategory Is Nothing Then lnkDeleteCategory.Attributes("onclick") = "return confirm('Delete the \'" & e.Item.Cells(1).Text.Replace("'", "\'") & "\' category?');"

            Dim lblCategoryId As Label = CType(e.Item.FindControl("lblCategoryId"), Label)
            If Not lblCategoryId Is Nothing Then
                e.Item.Cells(1).Attributes("onclick") = "EditCategory(" & lblCategoryId.Text & ");"
                e.Item.Cells(1).Style("CURSOR") = "hand"
            End If

        Catch ex As Exception

        End Try

    End Sub

    Private Sub dgWorkflows_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgWorkflows.ItemDataBound

        Try
            Dim lnkDeleteWorkflow As LinkButton = CType(e.Item.FindControl("lnkDeleteWorkflow"), LinkButton)
            If Not lnkDeleteWorkflow Is Nothing Then lnkDeleteWorkflow.Attributes("onclick") = "return confirm('Delete the \'" & e.Item.Cells(1).Text.Replace("'", "\'") & "\' workflow?');"

            Dim lblWorkflowId As Label = CType(e.Item.FindControl("lblWorkflowId"), Label)
            If Not lblWorkflowId Is Nothing Then
                e.Item.Cells(1).Attributes("onclick") = "EditWorkflow(" & lblWorkflowId.Text & ");"
                e.Item.Cells(1).Style("CURSOR") = "hand"
            End If

        Catch ex As Exception

        End Try

    End Sub

    Private Sub dgGroups_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgGroups.ItemDataBound

        Try
            Dim lnkDeleteGroup As LinkButton = CType(e.Item.FindControl("lnkDeleteGroup"), LinkButton)
            If Not lnkDeleteGroup Is Nothing Then lnkDeleteGroup.Attributes("onclick") = "return confirm('Delete the \'" & e.Item.Cells(1).Text.Replace("'", "\'") & "\' group?');"

            Dim lblGroupId As Label = CType(e.Item.FindControl("lblGroupId"), Label)
            If Not lblGroupId Is Nothing Then
                e.Item.Cells(1).Attributes("onclick") = "EditGroup(" & lblGroupId.Text & ");"
                e.Item.Cells(1).Style("CURSOR") = "hand"
            End If

        Catch ex As Exception

        End Try

    End Sub

    Private Sub dgQueues_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgQueues.ItemDataBound

        Try
            Dim lnkDeleteQueue As LinkButton = CType(e.Item.FindControl("lnkDeleteQueue"), LinkButton)
            If Not lnkDeleteQueue Is Nothing Then lnkDeleteQueue.Attributes("onclick") = "return confirm('Delete the \'" & e.Item.Cells(1).Text.Replace("'", "\'") & "\' queue?');"

            Dim lblQueueId As Label = CType(e.Item.FindControl("lblQueueId"), Label)
            If Not lblQueueId Is Nothing Then
                e.Item.Cells(1).Attributes("onclick") = "EditQueue(" & lblQueueId.Text & ");"
                e.Item.Cells(1).Style("CURSOR") = "hand"
                e.Item.Cells(2).Attributes("onclick") = "EditQueue(" & lblQueueId.Text & ");"
                e.Item.Cells(2).Style("CURSOR") = "hand"
            End If

        Catch ex As Exception

        End Try

    End Sub

    Private Sub dgStatus_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgStatus.ItemDataBound

        Try
            Dim lnkDeleteStatus As LinkButton = CType(e.Item.FindControl("lnkDeleteStatus"), LinkButton)
            If Not lnkDeleteStatus Is Nothing Then lnkDeleteStatus.Attributes("onclick") = "return confirm('Delete the \'" & e.Item.Cells(1).Text.Replace("'", "\'") & "\' action?');"

            Dim lblStatusId As Label = CType(e.Item.FindControl("lblStatusId"), Label)
            If Not lblStatusId Is Nothing Then
                e.Item.Cells(1).Attributes("onclick") = "EditStatus(" & lblStatusId.Text & ");"
                e.Item.Cells(1).Style("CURSOR") = "hand"
            End If

        Catch ex As Exception

        End Try

    End Sub
End Class

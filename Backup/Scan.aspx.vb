Partial Class Scan
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Functions.CheckTimeout(Me.Page)

        Dim sbLoadMessage As New System.Text.StringBuilder
        sbLoadMessage.Append("<SPAN id=LoadMessage>" & vbCr)

        sbLoadMessage.Append("<style type=""text/css"">" & vbCr)
        sbLoadMessage.Append("<!--" & vbCr)
        sbLoadMessage.Append(".style1 {" & vbCr)
        sbLoadMessage.Append("color: #FFFFFF;" & vbCr)
        sbLoadMessage.Append("font-family: Verdana, Arial, Helvetica, sans-serif;" & vbCr)
        sbLoadMessage.Append("font-size: 24px;" & vbCr)
        sbLoadMessage.Append("}" & vbCr)
        sbLoadMessage.Append(".style2 {" & vbCr)
        sbLoadMessage.Append("font-family: ""Trebuchet MS"", Verdana, Arial, sans-serif;" & vbCr)
        sbLoadMessage.Append("font-size: 10px;" & vbCr)
        sbLoadMessage.Append("}" & vbCr)
        sbLoadMessage.Append("-->" & vbCr)
        sbLoadMessage.Append("</style>" & vbCr)
        sbLoadMessage.Append("</head>" & vbCr)
        sbLoadMessage.Append("<body>" & vbCr)
        sbLoadMessage.Append("<table width=""100%"" height=""100%""  border=""0"" cellpadding=""120"" cellspacing=""0"">" & vbCr)
        sbLoadMessage.Append("<tr>" & vbCr)
        sbLoadMessage.Append("<td align=""center"" valign=""middle""><table width=""404"" border=""0"" cellpadding=""2"" cellspacing=""0"" bgcolor=""444444"">" & vbCr)
        sbLoadMessage.Append("<tr>" & vbCr)
        sbLoadMessage.Append("<td><table width=""400"" border=""0"" cellspacing=""0"" cellpadding=""20"">" & vbCr)
        sbLoadMessage.Append("<tr>" & vbCr)
        sbLoadMessage.Append("<td bgcolor=""BE0000""><div align=""center""><span class=""style1"">Loading... Please wait. </span></div></td>" & vbCr)
        sbLoadMessage.Append("</tr>" & vbCr)
        sbLoadMessage.Append(" <tr>" & vbCr)
        sbLoadMessage.Append("<td height=""2"" bgcolor=""#FFFFFF""></td>" & vbCr)
        sbLoadMessage.Append("</tr>" & vbCr)
        sbLoadMessage.Append("<tr>" & vbCr)
        sbLoadMessage.Append("<td height=""15"" bgcolor=""#666666""></td>" & vbCr)
        sbLoadMessage.Append("</tr>" & vbCr)
        sbLoadMessage.Append(" </table></td>" & vbCr)
        sbLoadMessage.Append("</tr>" & vbCr)
        sbLoadMessage.Append(" </table></td>" & vbCr)
        sbLoadMessage.Append("</tr></table>" & vbCr)
        sbLoadMessage.Append("</SPAN>")

        Response.Write(sbLoadMessage.ToString)

    End Sub

End Class

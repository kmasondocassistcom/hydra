<%@ Register TagPrefix="uc1" TagName="docHeader" Src="./Controls/docHeader.ascx" %>
<%@ Register TagPrefix="uc1" TagName="PrefNav" Src="./Controls/PrefNav.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="Preferences.aspx.vb" Inherits="docAssistWeb.Preferences" %>
<%@ Register TagPrefix="uc2" TagName="PrefNav" Src="Controls/PrefNav.ascx" %>
<%@ Register TagPrefix="uc1" TagName="TopRight" Src="./Controls/TopRight.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<TITLE>docAssist - Preferences</TITLE>
		<META content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<META content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<META content="JavaScript" name="vs_defaultClientScript">
		<META content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<META http-equiv="cache-control" content="no-cache">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<BODY bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<FORM id="Form1" method="post" runat="server" class=Prefs>
			<TABLE height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD width="100%" colSpan="6"><uc1:docheader id="docHeader" runat="server"></uc1:docheader></TD>
					<TD width="100%"></TD>
				</TR>
				<TR vAlign="middle" height="100%">
					<TD vAlign="top"
						height="100%"></TD>
					<td vAlign="top" width=300><uc2:prefnav id="PrefNav" runat="server"></uc2:prefnav><IMG height="1" src="spacer.gif" width="25"></td>
					<TD vAlign="top" width="100%">
					
					<iframe src=dummy.htm scrolling=auto frameborder=no id=prefFrame style="WIDTH: 100%; BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; HEIGHT: 95%; BORDER-BOTTOM-STYLE: none"></iframe>
					
					</TD>
					<TD><IMG height="1" src="spacer.gif" width="25"></TD>
				<tr>
					<td colSpan="5"><uc1:topright id="docFooter" runat="server"></uc1:topright></td>
					<td></td>
				</tr>
			</TABLE>
		</FORM>
		<script language="javascript">

			var node = window['PrefNav_treeNav'].findNodeById('Personal');

			node.select();
			window['PrefNav_treeNav'].Render();
		
			buttons();
		
			function buttons()
			{
				var btnOptions = document.all.docHeader_btnOptions;
				if (btnOptions != undefined)
				{
					btnOptions.style.display = "INLINE";
					btnOptions.src = "./Images/toolbar_options_white.gif";	
				}
			}
		
			function loadPage(pg)
			{
				var d = new Date();
				var random = d.getHours()+d.getMinutes()+d.getSeconds()+d.getMilliseconds();
				document.getElementById('prefFrame').src = pg + '?Id=' + random;
			}
	
		</script>
	</BODY>
</HTML>

<%@ Page Language="vb" AutoEventWireup="false" Codebehind="WorkflowCategoriesEdit.aspx.vb" Inherits="docAssistWeb.WorkflowCategoriesEdit"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title id="pageTitle">Workflow Category</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body onload="document.all.txtCategory.focus();">
		<form id="Form1" method="post" runat="server">
			<table cellpadding="0" cellspacing="0" height="80" width="350" border="0" class="PageContent">
				<tr>
					<td>
						<P>&nbsp; <img src="Images/categories.gif"><BR>
							<BR>
							&nbsp;
							<asp:Label id="lblMessage" runat="server"></asp:Label></P>
					</td>
				</tr>
				<tr>
					<td>&nbsp;
						<asp:Label id="lblCategory" runat="server">Category Name:</asp:Label>&nbsp;
						<asp:TextBox id="txtCategory" runat="server" Width="208px" Font-Names="Verdana" Font-Size="8.25pt"
							MaxLength="75"></asp:TextBox>
					</td>
				</tr>
				<tr>
					<td align="right">&nbsp;<A onclick="javascript:window.close()" href="#"><img src="Images/btn_cancel.gif" border="0"></A>
						<asp:ImageButton id="btnSave" runat="server" ImageUrl="Images/btn_save.gif"></asp:ImageButton></td>
				</tr>
				<tr>
					<td align="left" vAlign="top"><IMG height="1" src="spacer.gif" width="15">&nbsp;
						<asp:Label id="lblError" runat="server" Font-Size="8.25pt" Font-Names="Verdana" Visible="False"
							ForeColor="Red"></asp:Label>
					</td>
				</tr>
				<TR height="100%">
					<TD>
						<asp:Literal id="litReload" runat="server"></asp:Literal></TD>
				</TR>
			</table>
		</form>
		<SCRIPT language="javascript">
			
			function submitForm()
			{
				if (event.keyCode == 13)
				{
					event.cancelBubble = true;
					event.returnValue = false;
					document.all.btnSave.click();
				}
			}
		</SCRIPT>
	</body>
</HTML>

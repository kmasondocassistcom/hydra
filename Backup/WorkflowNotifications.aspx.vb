Imports Accucentric.docAssist.Data.Images

Partial Class WorkflowNotifications
    Inherits System.Web.UI.Page

    Dim mobjUser As Accucentric.docAssist.Web.Security.User
    Dim mconWorkflowNotifications As New SqlClient.SqlConnection
    Dim mconSqlCompany As New SqlClient.SqlConnection

    Protected WithEvents txtDaily As eWorld.UI.TimePicker
    Protected WithEvents txtWeekly As eWorld.UI.TimePicker
    Protected WithEvents Radiobutton1 As System.Web.UI.WebControls.RadioButton
    Protected WithEvents Radiobutton2 As System.Web.UI.WebControls.RadioButton

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try
            Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
        Catch ex As Exception
            Response.Redirect("Default.aspx")
            Exit Sub
        End Try

        Try

            If Not Page.IsPostBack Then

                Me.mconWorkflowNotifications = Functions.BuildConnection(Functions.ConnectionType.Notifications_Workflow)
                Me.mconSqlCompany = Functions.BuildConnection(Me.mobjUser)

                Dim objWf As New Workflow(Me.mconSqlCompany, False)
                Dim objWorkflow As New Workflow(Me.mconWorkflowNotifications, False)

                Dim sqlCmd As New SqlClient.SqlCommand
                Dim strQueuesTable As String = System.Guid.NewGuid().ToString().Replace("-", "")
                Dim sqlDa As New SqlClient.SqlDataAdapter

                Dim sb As New System.Text.StringBuilder

                Dim dtQueues As New DataTable
                dtQueues = objWf.WFUserQueuesGet(Me.mobjUser.ImagesUserId)
                sb = New System.Text.StringBuilder
                sb.Append("CREATE TABLE ##" & strQueuesTable.ToString & " (QueueId int NULL, QueueName varchar(255) NULL)" & vbCrLf)
                For Each dr As DataRow In dtQueues.Rows
                    sb.Append("INSERT INTO ##" & strQueuesTable & " (QueueId, QueueName) VALUES (" & CStr(dr("QueueId")).Replace("'", "''") & ", '" & CStr(dr("QueueName")).Replace("'", "''") & "')" & vbCrLf)
                Next
                sqlCmd = New SqlClient.SqlCommand(sb.ToString, Me.mconWorkflowNotifications)
                Dim sqlBld As New SqlClient.SqlCommandBuilder
                sqlCmd.ExecuteNonQuery()

                Dim dt As New DataTable
                Dim wf As New Workflow(Me.mconWorkflowNotifications)
                dt = wf.WFGetSubscriberQueueAlertSMTP(Me.mobjUser.UserId.ToString, Me.mobjUser.AccountId.ToString, Me.mobjUser.TimeDiffFromGMT, "##" & strQueuesTable)

                If dt.Rows.Count = 0 Then
                    NoRows.InnerHtml = "No alerts have been defined for your account. Click the Add Notification link to setup a notification."
                    dgAlerts.Visible = False
                Else
                    dgAlerts.Visible = True
                    dgAlerts.DataSource = dt
                    dgAlerts.DataBind()
                End If

            End If

        Catch ex As Exception

        Finally

            If Me.mconWorkflowNotifications.State <> ConnectionState.Closed Then
                Me.mconWorkflowNotifications.Close()
                Me.mconWorkflowNotifications.Dispose()
            End If

            If Me.mconSqlCompany.State <> ConnectionState.Closed Then
                Me.mconSqlCompany.Close()
                Me.mconSqlCompany.Dispose()
            End If

        End Try

    End Sub

    Private Sub dgAlerts_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgAlerts.ItemDataBound

        Try
            Dim lblType As Label = e.Item.FindControl("lblType")
            Dim lblAlertType As Label = e.Item.FindControl("lblAlertType")
            Dim btnDelete As ImageButton = e.Item.FindControl("btnDelete")

            btnDelete.Attributes("onclick") = "return confirm('Are you sure you want to remove this alert?');"

            Select Case lblType.Text
                Case "0"
                    lblAlertType.Text = "Initial"
                Case "1"
                    lblAlertType.Text = "Reminder"
            End Select

        Catch ex As Exception

        End Try


    End Sub


    Private Sub lnkAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkAdd.Click
        Response.Redirect("WorkflowNotificationsAdd.aspx")
    End Sub

    Private Sub dgAlerts_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgAlerts.ItemCommand
        Try
            Select Case e.CommandName
                Case "Del"

                    Try
                        Me.mconWorkflowNotifications = Functions.BuildConnection(Functions.ConnectionType.Notifications_Workflow)

                        Dim objWf As New Workflow(Me.mconWorkflowNotifications)

                        Dim lblAlertId As Label = e.Item.FindControl("AlertId")
                        objWf.WFDeleteAlert(lblAlertId.Text)

                        e.Item.Visible = False

                        Dim intVisibleItems As Integer = 0
                        For Each dgi As DataGridItem In dgAlerts.Items
                            If dgi.Visible Then intVisibleItems += 1
                        Next

                        If intVisibleItems = 0 Then
                            NoRows.InnerHtml = "No alerts have been defined for your account. Click the Add Notification link to setup a notification."
                            dgAlerts.Visible = False
                            NoRows.Visible = True
                        End If

                    Catch ex As Exception

                    Finally

                        If Me.mconSqlCompany.State <> ConnectionState.Closed Then
                            Me.mconSqlCompany.Close()
                            Me.mconSqlCompany.Dispose()
                        End If

                    End Try
            End Select
        Catch ex As Exception

        End Try
    End Sub
End Class
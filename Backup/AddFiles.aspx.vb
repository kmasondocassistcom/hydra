Imports Telerik.WebControls
Imports Telerik.WebControls.RadUploadUtils
Imports Accucentric.docAssist
Imports Accucentric.docAssist.Data.Images

Partial Class AddFiles
    'Inherits zumiControls.zumiPage
    Inherits System.Web.UI.Page


    Dim mconSqlImage As SqlClient.SqlConnection
    Dim mobjUser As Accucentric.docAssist.Web.Security.User
    Protected WithEvents lblFileName As System.Web.UI.WebControls.Label
    Protected WithEvents lblTitle As System.Web.UI.WebControls.Label
    Protected WithEvents lblCurrentVersion As System.Web.UI.WebControls.Label

    Dim mcookieSearch As cookieSearch

    Dim intAttachmentId As Integer

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        intAttachmentId = CInt(Request.QueryString("AID"))

        'btnSaveFiles.Attributes.Add("onclick", "javascript:window[""radProgressManager""].StartProgressPolling();")
        'btnSaveFiles.Attributes.Add("onclick", "document.all.radProgressManager.StartProgressPolling();")
        btnSave.Attributes.Add("onclick", "javascript: window[""RadProgressManager""].StartProgressPolling();")

        Try

            Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
            Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))
            Me.mobjUser.AccountId = guidAccountId
            Me.mcookieSearch = New cookieSearch(Me)

        Catch ex As Exception

#If DEBUG Then
            lblError.Text = ex.Message
#Else
            lblerror.Text = "An error has occured."
#End If

        Finally

            'If mconSqlImage.State <> ConnectionState.Closed Then
            '    mconSqlImage.Close()
            '    mconSqlImage.Dispose()
            'End If

        End Try

    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("FileAttachments.aspx")
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        '        Dim conSqlCompany As SqlClient.SqlConnection
        '        conSqlCompany = Functions.BuildConnection(Me.mobjUser)

        '        Dim objAttachments As New FileAttachments(conSqlCompany, False)

        '        Try

        '            Dim uploadContext As RadUploadContext
        '            uploadContext = RadUploadContext.Current

        '            Dim uploadedFiles As UploadedFileCollection
        '            uploadedFiles = uploadContext.UploadedFiles

        '            If uploadedFiles.Count > 0 Then

        '                Dim currentFileID As Integer = -1

        '                For X As Integer = 1 To uploadedFiles.Count

        '                    Dim filenm As String = ""
        '                    Dim desc As String = ""
        '                    Dim title As String = ""
        '                    Dim comments As String = ""
        '                    Dim tags As String = ""

        '                    Do While (currentFileID < 1000 And IsNothing(uploadedFiles(filenm)))
        '                        currentFileID += 1
        '                        filenm = String.Format("txtFilename{0}", IIf(currentFileID = 0, "", currentFileID.ToString))
        '                        desc = String.Format("txtDescription{0}", IIf(currentFileID = 0, "", currentFileID.ToString))
        '                        title = String.Format("txtTitle{0}", IIf(currentFileID = 0, "", currentFileID.ToString))
        '                        comments = String.Format("txtComments{0}", IIf(currentFileID = 0, "", currentFileID.ToString))
        '                        tags = String.Format("txtTags{0}", IIf(currentFileID = 0, "", currentFileID.ToString))
        '                    Loop

        '                    Dim file As UploadedFile = uploadedFiles(filenm)

        '                    If (file.FileName.Length > 0) And (file.ContentLength > 0) Then

        '                        Dim descsuffix As String = desc.Substring("txtDescription".Length)
        '                        Dim titlesuffix As String = title.Substring("txtTitle".Length)
        '                        Dim commentsuffix As String = comments.Substring("txtComments".Length)
        '                        Dim tagssuffix As String = tags.Substring("txtTags".Length)

        '                        Dim filename As String = System.IO.Path.GetFileName(file.FileName)
        '                        Dim bytes As Integer = file.ContentLength
        '                        Dim description As String = Request.Form(String.Concat("txtDescription", descsuffix)).Trim()
        '                        Dim titlestring As String = Request.Form(String.Concat("txtTitle", titlesuffix)).Trim()
        '                        Dim comment As String = Request.Form(String.Concat("txtComments", commentsuffix)).Trim()
        '                        Dim tag As String = Request.Form(String.Concat("txtTags", tagssuffix)).Trim()

        '                        Dim strSaveFile As String = ConfigurationSettings.AppSettings("UploadPath") & filename
        '                        file.SaveAs(strSaveFile)

        '                        Dim byteArray As Byte() = GetDataAsByteArray(strSaveFile)
        '                        Dim intAttachmentId As Integer = objAttachments.InsertAttachment(Me.mcookieSearch.VersionID, Me.mobjUser.ImagesUserId, titlestring, description, comment, tag, bytes, filename, byteArray)

        '                        'Track attachment
        '                        Try
        '                            Accucentric.docAssist.Web.Data.Functions.TrackPageRequest(Me.mobjUser.UserId, Me.mobjUser.AccountId, "Attachment Add", "In", Me.mcookieSearch.VersionID.ToString, intAttachmentId.ToString, Functions.GetMasterConnectionString)
        '                        Catch ex As Exception

        '                        End Try

        '                    End If

        '                Next

        '            End If

        '        Catch ex As Exception

        '#If DEBUG Then
        '            lblError.Text = ex.Message
        '#Else
        '            lblerror.Text = "An error has occured."
        '#End If

        '        Finally

        '            If conSqlCompany.State <> ConnectionState.Closed Then
        '                conSqlCompany.Close()
        '                conSqlCompany.Dispose()
        '            End If

        '        End Try

        '        Response.Redirect("FileAttachments.aspx")
    End Sub

    Private Sub SaveButton_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click

        'Dim conSqlCompany As SqlClient.SqlConnection
        'conSqlCompany = Functions.BuildConnection(Me.mobjUser)

        'Dim objAttachments As New FileAttachments(conSqlCompany, False)

        'Try

        '    Dim uploadContext As RadUploadContext
        '    uploadContext = RadUploadContext.Current

        '    Dim uploadedFiles As UploadedFileCollection
        '    uploadedFiles = uploadContext.UploadedFiles

        '    If uploadedFiles.Count > 0 Then

        '        Dim currentFileID As Integer = -1

        '        For X As Integer = 1 To uploadedFiles.Count

        '            Dim filenm As String = ""
        '            Dim desc As String = ""
        '            Dim title As String = ""
        '            Dim comments As String = ""
        '            Dim tags As String = ""

        '            Do While (currentFileID < 1000 And IsNothing(uploadedFiles(filenm)))
        '                currentFileID += 1
        '                filenm = String.Format("txtFilename{0}", IIf(currentFileID = 0, "", currentFileID.ToString))
        '                desc = String.Format("txtDescription{0}", IIf(currentFileID = 0, "", currentFileID.ToString))
        '                title = String.Format("txtTitle{0}", IIf(currentFileID = 0, "", currentFileID.ToString))
        '                comments = String.Format("txtComments{0}", IIf(currentFileID = 0, "", currentFileID.ToString))
        '                tags = String.Format("txtTags{0}", IIf(currentFileID = 0, "", currentFileID.ToString))
        '            Loop

        '            Dim file As UploadedFile = uploadedFiles(filenm)

        '            If (file.FileName.Length > 0) And (file.ContentLength > 0) Then

        '                Dim descsuffix As String = desc.Substring("txtDescription".Length)
        '                Dim titlesuffix As String = title.Substring("txtTitle".Length)
        '                Dim commentsuffix As String = comments.Substring("txtComments".Length)
        '                Dim tagssuffix As String = tags.Substring("txtTags".Length)

        '                Dim filename As String = System.IO.Path.GetFileName(file.FileName)
        '                Dim bytes As Integer = file.ContentLength
        '                Dim description As String = Request.Form(String.Concat("txtDescription", descsuffix)).Trim()
        '                Dim titlestring As String = Request.Form(String.Concat("txtTitle", titlesuffix)).Trim()
        '                Dim comment As String = Request.Form(String.Concat("txtComments", commentsuffix)).Trim()
        '                Dim tag As String = Request.Form(String.Concat("txtTags", tagssuffix)).Trim()

        '                Dim strSaveFile As String = ConfigurationSettings.AppSettings("UploadPath") & filename
        '                file.SaveAs(strSaveFile)

        '                Dim byteArray As Byte() = GetDataAsByteArray(strSaveFile)
        '                objAttachments.InsertAttachment(Me.mcookieSearch.VersionID, Me.mobjUser.ImagesUserId, titlestring, description, comment, tag, bytes, filename, byteArray)

        '            End If

        '        Next

        '    End If

        'Catch ex As Exception

        'Finally

        '    If conSqlCompany.State <> ConnectionState.Closed Then
        '        conSqlCompany.Close()
        '        conSqlCompany.Dispose()
        '    End If

        'End Try

        'Response.Redirect("FileAttachments.aspx")

    End Sub
End Class
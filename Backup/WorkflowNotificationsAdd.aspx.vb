Imports Accucentric.docAssist.Data.Images

Partial Class WorkflowNotificationsAdd
    Inherits System.Web.UI.Page

    Dim mobjUser As Accucentric.docAssist.Web.Security.User
    Dim mconWorkflowNotifications As New SqlClient.SqlConnection
    Dim mconSqlCompany As New SqlClient.SqlConnection

    Protected WithEvents Radiobutton1 As System.Web.UI.WebControls.RadioButton
    Protected WithEvents Radiobutton2 As System.Web.UI.WebControls.RadioButton

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        BuildClientSideScripts()

        Try
            Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
        Catch ex As Exception
            Response.Redirect("Default.aspx")
            Exit Sub
        End Try

        Try

            If Not Page.IsPostBack Then

                Me.mconWorkflowNotifications = Functions.BuildConnection(Me.mobjUser)
                Dim objWorkflow As New Workflow(Me.mconWorkflowNotifications)

                Dim dtQueues As New DataTable
                dtQueues = objWorkflow.WFUserQueuesGet(Me.mobjUser.ImagesUserId)

                ddlQueue.DataSource = dtQueues
                ddlQueue.DataTextField = "QueueName"
                ddlQueue.DataValueField = "QueueId"
                ddlQueue.DataBind()
            End If

        Catch ex As Exception

        Finally

            If Me.mconWorkflowNotifications.State <> ConnectionState.Closed Then
                Me.mconWorkflowNotifications.Close()
                Me.mconWorkflowNotifications.Dispose()
            End If

        End Try

    End Sub

    Private Function BuildClientSideScripts()

        rdoInitial.Attributes("onclick") = "enableRealTime();"
        rdoReminder.Attributes("onclick") = "disableRealTime();"

        rdoRealTime.Attributes("onclick") = "disableWeekly();"
        rdoDaily.Attributes("onclick") = "disableWeekly();"
        rdoWeekly.Attributes("onclick") = "enableWeekly();"

        rdoBoth.Attributes("onclick") = "disableNotBoth()"
        rdoUrgent.Attributes("onclick") = "disableNotUrgent()"
        rdoNormal.Attributes("onclick") = "disableNotNormal()"




    End Function

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAdd.Click

        Dim time As DateTime
        Dim strScheduleText As String = ""
        Dim strHex As String = ""
        Dim intUtcTime As Integer

        Me.mconWorkflowNotifications = Functions.BuildConnection(Functions.ConnectionType.Notifications_Workflow)
        Dim objWorkflow As New Workflow(Me.mconWorkflowNotifications, False)

        Try

            If rdoWeekly.Checked Then

                Dim blnDaySelected As Boolean = False

                If chkMonday.Checked Then blnDaySelected = True
                If chkTuesday.Checked Then blnDaySelected = True
                If chkWednesday.Checked Then blnDaySelected = True
                If chkThursday.Checked Then blnDaySelected = True
                If chkFriday.Checked Then blnDaySelected = True
                If chkSaturday.Checked Then blnDaySelected = True
                If chkSunday.Checked Then blnDaySelected = True

                If blnDaySelected = False Then
                    lblError.Text = "Please choose at least one day of the week to add a weekly alert."
                    lblError.Visible = True
                    Exit Sub
                End If

            End If

            'Reminder Type
            Dim intReminderType As Integer
            If rdoInitial.Checked Then
                intReminderType = 0
            ElseIf rdoReminder.Checked Then
                intReminderType = 1
            End If

            'Schedule Type
            Dim intScheduleType As Integer
            If rdoRealTime.Checked Then
                intScheduleType = 0
            ElseIf rdoDaily.Checked Then
                intScheduleType = 1
            ElseIf rdoWeekly.Checked Then
                intScheduleType = 2
            End If

            'Schedule Text
            If Not rdoRealTime.Checked Then

                Dim sb As New System.Text.StringBuilder
                sb.Append(Format(Now, "yyyyMMdd"))
                sb.Append("T")

                If rdoDaily.Checked Then
                    time = CDate(txtDaily.SelectedTime)
                ElseIf rdoWeekly.Checked Then
                    time = CDate(txtWeekly.SelectedTime)
                End If

                sb.Append(Format(time.AddHours(Me.mobjUser.TimeDiffFromGMT * -1), "HHmm00"))
                sb.Append(Space(1))

                sb.Append("FREQ=")

                If rdoDaily.Checked Then
                    sb.Append("DAILY")
                ElseIf rdoWeekly.Checked Then
                    sb.Append("WEEKLY")
                End If

                If rdoWeekly.Checked Then
                    sb.Append(";BYDAY=")

                    If chkMonday.Checked Then sb.Append("MO,")
                    If chkTuesday.Checked Then sb.Append("TU,")
                    If chkWednesday.Checked Then sb.Append("WE,")
                    If chkThursday.Checked Then sb.Append("TH,")
                    If chkFriday.Checked Then sb.Append("FR,")
                    If chkSaturday.Checked Then sb.Append("SA,")
                    If chkSunday.Checked Then sb.Append("SU")

                End If

                strScheduleText = sb.ToString

                If strScheduleText.EndsWith(",") Then strScheduleText = strScheduleText.Remove(strScheduleText.LastIndexOf(","), 1)

                'Shcedule Data
                Dim intDecimal As Integer
                If chkMonday.Checked Then intDecimal += 2
                If chkTuesday.Checked Then intDecimal += 4
                If chkWednesday.Checked Then intDecimal += 8
                If chkThursday.Checked Then intDecimal += 16
                If chkFriday.Checked Then intDecimal += 32
                If chkSaturday.Checked Then intDecimal += 64
                If chkSunday.Checked Then intDecimal += 1

                strHex = Microsoft.VisualBasic.Hex(intDecimal)

                'Insert alert
                Dim intHours, intMinutes As Integer

                intHours = time.AddHours(Me.mobjUser.TimeDiffFromGMT * -1).Hour * 1080000
                intMinutes = time.AddHours(Me.mobjUser.TimeDiffFromGMT * -1).Minute * 18000

                intUtcTime = intHours + intMinutes

            End If

            If rdoRealTime.Checked Then intUtcTime = 0

            Dim intResult As Integer = 0

            If rdoRealTime.Checked Or rdoDaily.Checked Then

                If rdoBoth.Checked = True Then
                    intResult = objWorkflow.WFAddSubscriberQueueAlertSMTP(Me.mobjUser.UserId.ToString, Me.mobjUser.AccountId.ToString, Me.mobjUser.EMailAddress, ddlQueue.SelectedValue, False, intReminderType, IIf(intUtcTime = 0, Nothing, intUtcTime), intScheduleType, Nothing, strScheduleText)
                    intResult = objWorkflow.WFAddSubscriberQueueAlertSMTP(Me.mobjUser.UserId.ToString, Me.mobjUser.AccountId.ToString, Me.mobjUser.EMailAddress, ddlQueue.SelectedValue, True, intReminderType, IIf(intUtcTime = 0, Nothing, intUtcTime), intScheduleType, Nothing, strScheduleText)
                End If

                If rdoNormal.Checked = True Then
                    intResult = objWorkflow.WFAddSubscriberQueueAlertSMTP(Me.mobjUser.UserId.ToString, Me.mobjUser.AccountId.ToString, Me.mobjUser.EMailAddress, ddlQueue.SelectedValue, False, intReminderType, IIf(intUtcTime = 0, Nothing, intUtcTime), intScheduleType, Nothing, strScheduleText)
                End If

                If rdoUrgent.Checked = True Then
                    intResult = objWorkflow.WFAddSubscriberQueueAlertSMTP(Me.mobjUser.UserId.ToString, Me.mobjUser.AccountId.ToString, Me.mobjUser.EMailAddress, ddlQueue.SelectedValue, True, intReminderType, IIf(intUtcTime = 0, Nothing, intUtcTime), intScheduleType, Nothing, strScheduleText)
                End If

            Else

                If rdoBoth.Checked = True Then
                    intResult = objWorkflow.WFAddSubscriberQueueAlertSMTP(Me.mobjUser.UserId.ToString, Me.mobjUser.AccountId.ToString, Me.mobjUser.EMailAddress, ddlQueue.SelectedValue, False, intReminderType, intUtcTime, intScheduleType, strHex, strScheduleText)
                    intResult = objWorkflow.WFAddSubscriberQueueAlertSMTP(Me.mobjUser.UserId.ToString, Me.mobjUser.AccountId.ToString, Me.mobjUser.EMailAddress, ddlQueue.SelectedValue, True, intReminderType, intUtcTime, intScheduleType, strHex, strScheduleText)
                End If

                If rdoNormal.Checked = True Then
                    intResult = objWorkflow.WFAddSubscriberQueueAlertSMTP(Me.mobjUser.UserId.ToString, Me.mobjUser.AccountId.ToString, Me.mobjUser.EMailAddress, ddlQueue.SelectedValue, False, intReminderType, intUtcTime, intScheduleType, strHex, strScheduleText)
                End If

                If rdoUrgent.Checked = True Then
                    intResult = objWorkflow.WFAddSubscriberQueueAlertSMTP(Me.mobjUser.UserId.ToString, Me.mobjUser.AccountId.ToString, Me.mobjUser.EMailAddress, ddlQueue.SelectedValue, True, intReminderType, intUtcTime, intScheduleType, strHex, strScheduleText)
                End If

            End If

            If intResult <= 0 Then
                'ERROR
                Dim intError As Int16 = 3
            Else
                Response.Redirect("WorkflowNotifications.aspx")
            End If

        Catch ex As Exception

#If DEBUG Then
            lblError.Text = ex.Message
#Else
            lblError.Text = "An error has occured while adding this notification. If the problem continues please contact support."
#End If
            lblError.Visible = True

        Finally

            If Me.mconWorkflowNotifications.State <> ConnectionState.Closed Then
                Me.mconWorkflowNotifications.Close()
                Me.mconWorkflowNotifications.Dispose()
            End If

            If Me.mconSqlCompany.State <> ConnectionState.Closed Then
                Me.mconSqlCompany.Close()
                Me.mconSqlCompany.Dispose()
            End If

        End Try

    End Sub

    Private Sub rdoRealTime_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdoRealTime.CheckedChanged

    End Sub
End Class
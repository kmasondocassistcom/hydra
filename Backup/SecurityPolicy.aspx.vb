Imports Accucentric.docAssist.Data.Images

Partial Class SecurityPolicy
    Inherits System.Web.UI.Page

    Dim mobjUser As Accucentric.docAssist.Web.Security.User

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents btnSave As System.Web.UI.WebControls.ImageButton

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try
            mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
            Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))
            mobjUser.AccountId = guidAccountId
        Catch ex As Exception
            Response.Redirect("Default.aspx")
        End Try

        Dim conMaster As New SqlClient.SqlConnection
        Try

            If Not Page.IsPostBack Then
                conMaster = Functions.BuildMasterConnection
                Dim Settings As New AccountSettings(conMaster)
                Dim Days As Integer = Settings.GetPasswordExpiration(mobjUser.AccountId.ToString)

                If Days < 0 Then
                    rdoNeverExpire.Checked = True
                    txtDays.Text = (Days * -1)
                Else
                    rdoExpireInterval.Checked = True
                    txtDays.Text = Days
                End If

            End If
        Catch ex As Exception

        Finally
            If conMaster.State <> ConnectionState.Closed Then
                conMaster.Close()
                conMaster.Dispose()
            End If
        End Try

    End Sub

    Private Sub btnSaveSettings_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSaveSettings.Click

        If Not IsNumeric(txtDays.Text) Or txtDays.Text.ToString.IndexOfAny("e") > -1 Or txtDays.Text.ToString.IndexOfAny(".") > -1 Then
            lblError.Text = "You must specify a numeric value for the days."
            'ReturnControls.Add(lblError)
            Exit Sub
        End If

        If CInt(txtDays.Text) = 0 Then
            lblError.Text = "You must specify a number of days greater than 0."
            'ReturnControls.Add(lblError)
            Exit Sub
        End If

        Dim conMaster As New SqlClient.SqlConnection
        Try

            conMaster = Functions.BuildMasterConnection
            Dim Settings As New AccountSettings(conMaster)

            Dim days As Integer = txtDays.Text

            If rdoNeverExpire.Checked Then days = days * -1

            Settings.SetPasswordExpiration(mobjUser.AccountId.ToString, days)

            lblError.Text = "Settings successfully saved."

        Catch ex As Exception

#If DEBUG Then
            lblError.Text = ex.Message
#Else
            lblError.Text = "An error has occured while saving this preference. Please try again. If the problem continues please contact support."
#End If

        Finally

            If conMaster.State <> ConnectionState.Closed Then
                conMaster.Close()
                conMaster.Dispose()
            End If

            'ReturnControls.Add(lblError)

        End Try

    End Sub
End Class
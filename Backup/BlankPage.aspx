<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="Controls/MenuBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="docHeader" Src="Controls/docHeader.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="BlankPage.aspx.vb" Inherits="docAssistWeb.BlankPage"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<TITLE>docAssist</TITLE>
		<META content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<META content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<META content="JavaScript" name="vs_defaultClientScript">
		<META content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<BODY>
		<FORM id="Form1" method="post" runat="server">
			<TABLE height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD colspan="3" width="100%">
						<uc1:docHeader id="docHeader" runat="server"></uc1:docHeader></TD>
				</TR>
				<TR vAlign="middle" height="100%">
					<TD class="Navigation1" vAlign="top" width="180" height="100%"></TD>
					<TD class="PageContent" vAlign="middle" align="center" height="100%">&nbsp;&nbsp;
						<OBJECT classid="clsid:DCB70DD2-4315-4228-9E6A-3D5BE5657593" VIEWASTEXT>
							<PARAM NAME="_ExtentX" VALUE="0">
							<PARAM NAME="_ExtentY" VALUE="0">
						</OBJECT>
					</TD>
					<TD class="RightContent">&nbsp;</TD>
				</TR>
				<TR height="1">
					<TD style="BACKGROUND-COLOR: #959394" width="180"><IMG height="1" src="images/spacer.gif" width="180"></TD>
					<TD style="BACKGROUND-COLOR: #959394" width="100%"><IMG height="1" src="images/spacer.gif" width="100%"></TD>
					<TD style="BACKGROUND-COLOR: #959394" width="90"><IMG height="1" src="images/spacer.gif" width="90"></TD>
				</TR>
			</TABLE>
		</FORM>
		<SCRIPT language="vbscript">
			sub ImgXCheck() 
				on error resume next
				dim Acro
				document.all.ix7.savefilename = "blah.tif"
				if err.number <> 0 then
					Document.all.ObjectCheck.value = "0"
				else
					Document.all.ObjectCheck.value = "1"
				end if
				Acro = nothing
			end sub
			
		</SCRIPT>
	</BODY>
</HTML>

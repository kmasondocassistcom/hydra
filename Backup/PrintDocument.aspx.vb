Partial Class PrintDocument
    Inherits System.Web.UI.Page

    Private mcookieSearch As cookieSearch
    Private mobjUser As Accucentric.docAssist.Web.Security.User
    Private mconSqlImage As SqlClient.SqlConnection
   
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try
            Me.mobjUser = BuildUserObject(Request.Cookies("docAssist"))
            Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
            Me.mcookieSearch = New cookieSearch(Me.Page)
            Me.mconSqlImage.Open()
        Catch ex As Exception
            If ex.Message = "User is not logged on" Then
                Response.Redirect("Default.aspx")
            End If
        End Try

        Try
            'Check for permissions
            Dim blnAnnotation As Boolean = False
            Dim blnRedaction As Boolean = False

            Functions.DocumentAnnotationSecurityLevel(Me.mobjUser.ImagesUserId, Me.mcookieSearch.VersionID, Me.mconSqlImage, blnAnnotation, blnRedaction)

            Dim annCount As Boolean = Request.QueryString("A")
            Dim redCount As Boolean = Request.QueryString("R")

            If blnAnnotation = False Then
                chkAnnotations.Checked = True
                chkAnnotations.Style("visibility") = "hidden"
            Else
                chkAnnotations.Style("visibility") = "visible"
            End If

            If blnRedaction = False Then
                chkRedactions.Checked = True
                chkRedactions.Style("visibility") = "hidden"
            Else
                chkRedactions.Style("visibility") = "visible"
            End If

            If annCount = 0 Then chkAnnotations.Enabled = False
            If redCount = 0 Then chkRedactions.Enabled = False

        Catch ex As Exception

        Finally
            If mconSqlImage.State <> ConnectionState.Closed Then mconSqlImage.Close()
            mconSqlImage.Dispose()
        End Try

    End Sub

End Class

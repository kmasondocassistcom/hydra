<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ViewerAttributes.aspx.vb"
    Inherits="docAssistWeb.ViewerAttributes" ValidateRequest="false" EnableEventValidation="false" %>

<%@ Register TagPrefix="uc1" TagName="AttributeNavigation" Src="AttributeNavigation.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title></title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <link href="Styles.css" type="text/css" rel="stylesheet">
    <link href="css/jquery.autocomplete.css" type="text/css" rel="stylesheet">
</head>
<body onresize="setAttributesHeight();" onload="setAttributesHeight();" scroll="yes">
    <form id="Form1" method="post" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <table class="TransparentNoPadding" cellspacing="0" cellpadding="0" width="100%"
        border="0">
        <tr>
            <td>
                <table id="Table1" cellspacing="0" cellpadding="0" width="100" border="0" runat="server">
                    <tr>
                        <td>
                            <img height="10" src="dummy.gif" width="1">
                        </td>
                    </tr>
                    <tr valign="bottom">
                        <td valign="bottom" nowrap>
                            <img height="1" src="dummy.gif" width="10"><asp:Label ID="Label8" runat="server"
                                Font-Names="Trebuchet MS" Height="3px" Font-Size="8pt">Doc. No.</asp:Label><img height="1"
                                    src="dummy.gif" width="28">
                        </td>
                        <td valign="top" nowrap width="100%" height="15">
                            <asp:Label ID="lblImageId" runat="server" Font-Names="Trebuchet MS" Font-Size="8pt"></asp:Label>
                        </td>
                    </tr>
                </table>
                <table id="tblFolder" cellspacing="0" cellpadding="0" width="100%" border="0" runat="server">
                    <tr valign="baseline">
                        <td valign="middle" nowrap>
                            <img height="1" src="dummy.gif" width="10"><asp:Label ID="Label7" runat="server"
                                Font-Names="Trebuchet MS" Height="3px" Font-Size="8pt">Folder:</asp:Label><img height="1"
                                    src="dummy.gif" width="34">
                        </td>
                        <td valign="bottom" nowrap height="15">
                            <asp:TextBox ID="txtFolder" runat="server" Font-Names="Trebuchet MS" Height="22px"
                                Font-Size="8pt" Width="100px" Enabled="True" ReadOnly="False" BorderStyle="None"></asp:TextBox>
                        </td>
                        <td valign="bottom" width="100%">
                            <img height="1" src="spacer.gif" width="1">
                            <img id="ChooseFolder" style="cursor: hand" onclick="FolderLookup()" height="19"
                                alt="Choose Folder" src="Images/addToFolder_btn.gif" runat="server"><img height="1"
                                    src="spacer.gif" width="5"><img id="ClearFolder" style="cursor: hand" onclick="document.getElementById('FolderId').value = '';document.getElementById('txtFolder').value = '';ActivateSave();"
                                        height="20" alt="Clear Folder" src="Images/clear.gif" runat="server">
                        </td>
                    </tr>
                </table>
                <table id="tblTextBoxes" cellspacing="0" cellpadding="0" border="0" runat="server">
                    <tr id="titleRow" runat="server">
                        <td valign="middle" nowrap>
                            <img height="1" src="dummy.gif" width="10"><asp:Label ID="Label4" runat="server"
                                Font-Names="Trebuchet MS" Height="3px" Font-Size="8pt">Title:</asp:Label><img height="1"
                                    src="dummy.gif" width="43">
                        </td>
                        <td nowrap width="100%">
                            <asp:TextBox ID="txtTitle" runat="server" Font-Names="Trebuchet MS" Height="22px"
                                Font-Size="8pt" Width="160px" Visible="False"></asp:TextBox><asp:Label ID="lblTitle"
                                    runat="server" Font-Names="Trebuchet MS" Font-Size="8pt" Width="160px" Visible="False"></asp:Label>
                        </td>
                    </tr>
                    <tr id="descRow" runat="server">
                        <td id="TD1" valign="middle" nowrap runat="server">
                            <img height="1" src="dummy.gif" width="10"><asp:Label ID="Label5" runat="server"
                                Font-Names="Trebuchet MS" Height="3px" Font-Size="8pt">Description:</asp:Label>
                        </td>
                        <td nowrap width="100%">
                            <asp:TextBox ID="txtComments" runat="server" Font-Names="Trebuchet MS" Height="22px"
                                Font-Size="8pt" Width="160px" Visible="False"></asp:TextBox><asp:Label ID="lblComments"
                                    runat="server" Font-Names="Trebuchet MS" Font-Size="8pt" Width="160px" Visible="False"></asp:Label>
                        </td>
                    </tr>
                    <tr id="tagRow" runat="server">
                        <td nowrap>
                            <img height="1" src="dummy.gif" width="10"><asp:Label ID="Label6" runat="server"
                                Font-Names="Trebuchet MS" Height="3px" Font-Size="8pt">Tags:</asp:Label><img height="1"
                                    src="dummy.gif" width="37">
                        </td>
                        <td nowrap width="100%">
                            <asp:TextBox ID="txtTags" runat="server" Font-Names="Trebuchet MS" Height="22px"
                                Font-Size="8pt" Width="160px" Visible="False"></asp:TextBox><asp:Label ID="lblTags"
                                    runat="server" Font-Names="Trebuchet MS" Font-Size="8pt" Width="160px" Visible="False"></asp:Label>
                        </td>
                    </tr>
                </table>
                <div><img height="1" src="dummy.gif" width="9"><asp:Label ID="Label3" runat="server" Font-Names="Trebuchet MS"
                                Height="3px" Font-Size="8pt">Document:</asp:Label>&nbsp;<img height="1" src="dummy.gif"
                                    width="6">
                            <asp:DropDownList ID="ddlDocuments" runat="server" Font-Names="Trebuchet MS" Height="4px"
                                Font-Size="8pt" Width="160px" Visible="False" AutoPostBack="True">
                            </asp:DropDownList></div>
                <table class="Transparent" cellspacing="2" cellpadding="2" width="100%" border="0">
                    <tr>
                        <td nowrap>
                            
                            <asp:TextBox ID="txtDocumentName" runat="server" Width="160px" ReadOnly="True" BorderStyle="Solid"
                                Visible="False" BorderWidth="1px" BorderColor="Silver" CssClass="LabelHighlight"
                                Font-Size="8pt"></asp:TextBox><asp:Label ID="lblDocumentId" runat="server" Visible="False"></asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
                <tr id="ErrorRow" runat="server">
                    <td id="TD2" width="100%" runat="server">
                        <table cellpadding="0" width="100%" border="0">
                            <tr width="100%">
                                <td>
                                    <img height="1" src="dummy.gif" width="10">
                                </td>
                                <td width="100%">
                                    <img height="8" src="images/spacer.gif" width="1"><asp:Label ID="lblAttributeError"
                                        runat="server" Font-Names="Trebuchet MS" Font-Size="8pt" Visible="False" ForeColor="Red"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
        <asp:UpdatePanel ID="updatePanel" runat="server">
            <ContentTemplate>
                <tr>
                    <td id="TD3" runat="server">
                        <img height="1" src="dummy.gif" width="7">
                        <asp:ImageButton ID="lnkAdd" runat="server" AlternateText="Add Attribute" ToolTip="Add Attribute"
                            ImageUrl="Images/icon-add.gif"></asp:ImageButton>&nbsp;<asp:ImageButton ID="btnRemove"
                                runat="server" ToolTip="Remove Attribute" ImageUrl="Images/icon-remove.gif">
                        </asp:ImageButton>&nbsp;<asp:ImageButton ID="btnCopy" runat="server" ImageUrl="Images/icon-copy.gif"
                            ToolTip="Copy Attributes"></asp:ImageButton>
                        <asp:ImageButton ID="btnPaste" runat="server" ImageUrl="Images/icon-paste.gif" ToolTip="Paste Attributes">
                        </asp:ImageButton><asp:ImageButton ID="btnCapture2" runat="server" ToolTip="Capture Attributes"
                            ImageUrl="Images/capture.gif"></asp:ImageButton>&nbsp;
                        <img id="imgDistribution" src="Images/btn.distribution.gif" border="0" style="
                            cursor: hand" alt="Show Distribution" runat="server">
                        <img onclick="relatedDocs()" style="cursor: hand" border="0" alt="Related Documents"
                            src="Images/icon-related.gif">
                        <table cellspacing="0" cellpadding="0" width="50" border="0">
                            <tr width="100%">
                                <td>
                                    <img height="1" src="dummy.gif" width="7">
                                </td>
                                <td width="100%">
                                    <asp:DataGrid ID="dgAttributes" runat="server" Font-Size="8pt" CssClass="PageContent"
                                        AutoGenerateColumns="False" Width="230px">
                                        <SelectedItemStyle BorderStyle="Double"></SelectedItemStyle>
                                        <ItemStyle Font-Size="8pt"></ItemStyle>
                                        <HeaderStyle Font-Bold="True" HorizontalAlign="Center" ForeColor="White" VerticalAlign="Bottom"
                                            BackColor="DarkGray"></HeaderStyle>
                                        <Columns>
                                            <asp:TemplateColumn Visible="False">
                                                <ItemStyle BackColor="White"></ItemStyle>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn Visible="False" HeaderText="AttributeId"></asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Attribute">
                                                <ItemStyle VerticalAlign="Top" HorizontalAlign="Right"></ItemStyle>
                                                <ItemTemplate>
                                                    <asp:DropDownList ID="ddlAttribute" runat="server" AutoPostBack="True" Width="130px">
                                                    </asp:DropDownList>
                                                    <asp:Label ID="lblAttributeName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AttributeName") %>'
                                                        Visible="False">
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Value">
                                                <ItemStyle VerticalAlign="Top"></ItemStyle>
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtAttributeValue" Width="90" runat="server" BorderStyle="Groove"
                                                        CssClass="inputBox" Text='<%# DataBinder.Eval(Container, "DataItem.AttributeValue") %>'>
                                                    </asp:TextBox>
                                                    <asp:Label ID="lblAttributeId" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.AttributeId") %>'>
                                                    </asp:Label>
                                                    <asp:Label ID="lblAccessLevel" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.AccessLevel") %>'>
                                                    </asp:Label>
                                                    <asp:Label ID="lblImageAttributeId" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.ImageAttributeId") %>'>
                                                    </asp:Label>
                                                    <asp:Label ID="lblAttributeValue" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.AttributeValue") %>'>
                                                    </asp:Label>
                                                    <span style="cursor: hand">
                                                        <asp:Image ID="imgBrowse" runat="server" Height="15px" Width="15px" Visible="False"
                                                            ImageUrl="Images/listbox.btn.gif"></asp:Image></span>
                                                    <asp:Label ID="lblListViewItem" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.ValueDescription") %>'>
                                                    </asp:Label>
                                                    <asp:Label ID="lblControlAttributeId" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.ControlTotalAttributeID") %>'>
                                                    </asp:Label>
                                                    <asp:Label ID="lblRowError" runat="server" Font-Names="Verdana" Visible="False" ForeColor="Red"></asp:Label>
                                                    <asp:DropDownList ID="ddlAttributeValue" runat="server" Visible="False">
                                                    </asp:DropDownList>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn Visible="False" HeaderText="AttributeProperties">
                                                <ItemStyle VerticalAlign="Top"></ItemStyle>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblAttributeDataType" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.AttributeDataType") %>'>
                                                    </asp:Label>
                                                    <asp:Label ID="lblListValidate" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.ListValidate") %>'>
                                                    </asp:Label>
                                                    <asp:Label ID="lblDataFormat" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.DataFormat") %>'>
                                                    </asp:Label>
                                                    <asp:Label ID="lblLength" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.Length") %>'>
                                                    </asp:Label>
                                                    <asp:Label ID="lblListViewStyle" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.ListViewStyle") %>'>
                                                    </asp:Label>
                                                    <asp:Label ID="lblRequired" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.Required") %>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                        </Columns>
                                    </asp:DataGrid>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div style="display: none">
            <input id="ReadOnlyUser" type="hidden" value="0" runat="server" name="ReadOnlyUser">
            <asp:ImageButton ID="btnSave" runat="server" Height="0px" Width="0px"></asp:ImageButton><asp:TextBox
                ID="txtMapName" runat="server" Height="0px" Width="0px"></asp:TextBox><asp:TextBox
                    ID="txtHeader" runat="server" Height="0px" Width="0px"></asp:TextBox><asp:TextBox
                        ID="txtDetail" runat="server" Height="0px" Width="0px"></asp:TextBox><input id="WorkflowId"
                            style="visibility: hidden" type="text" name="WorkflowId" runat="server"><input id="QueueId"
                                style="visibility: hidden" type="text" name="QueueId" runat="server"><input id="WorkflowNote"
                                    style="visibility: hidden" type="text" name="WorkflowNote" runat="server">
            <asp:TextBox ID="PageNo" runat="server" Height="0px" Width="0px"></asp:TextBox><input
                id="Urgent" style="visibility: hidden" type="text" value="0" name="Urgent" runat="server"><input
                    id="Mode" style="visibility: hidden" type="text" value="0" name="Mode" runat="server"><input
                        id="RouteId" style="visibility: hidden" type="text" value="0" name="RouteId"
                        runat="server">
            <input id="FolderId" type="hidden" name="FolderId" runat="server">
            <object id="objCapture" style="visibility: hidden" height="0" width="0" classid="clsid:D0719215-417C-4086-B157-A0CA6115FA55"
                viewastext>
                <param name="_ExtentX" value="26">
                <param name="_ExtentY" value="26">
            </object>
            <asp:TextBox ID="Annotation" runat="server" Height="0px" Width="0px"></asp:TextBox><input
                id="lblVersionId" type="hidden" name="lblVersionId" runat="server"><input id="lblPageNo"
                    type="hidden" name="lblPageNo" runat="server"><input id="lblFileNameResult" type="hidden"
                        name="lblFileNameResult" runat="server">
            <asp:TextBox ID="txtOldAttributeId" runat="server" Visible="False"></asp:TextBox><asp:TextBox
                ID="txtOldAttributeValue" runat="server" Visible="False"></asp:TextBox><input id="hiddenTripCount"
                    type="hidden" name="hiddenTripCount" runat="server"><input id="ActiveRow" type="hidden"
                        name="ActiveRow" runat="server">
            <asp:ImageButton ID="btnShade" runat="server" Height="0px" Width="0px"></asp:ImageButton><asp:ImageButton
                ID="btnClear" runat="server" Height="0px" Width="0px"></asp:ImageButton><input id="IntegrationValues"
                    type="hidden" name="IntegrationValues" runat="server">
            <asp:ImageButton ID="btnIntegration" runat="server" Height="0px" Width="0px"></asp:ImageButton><input
                id="ClearAttributes" type="hidden" name="ClearAttributes" runat="server"><input id="ApplicationName"
                    type="hidden" name="ApplicationName" runat="server"><input id="confirmNav" type="hidden"
                        value="0"><input id="moveNext" type="hidden"><input id="hiddenSubmit" type="hidden"><input
                            id="hiddenDirty" style="visibility: hidden" type="text" name="hiddenDirty" runat="server"><input
                                id="showCalendar" type="hidden" value="0" name="showCalendar" runat="server"><input
                                    id="selectedIndex" type="hidden" name="selectedIndex" runat="server">
            <input id="hostname" type="hidden" name="hostname" runat="server"><input id="FolderMap"
                type="hidden" value="0" name="hostname" runat="server"><input id="FormID" type="hidden"
                    value="0" name="hostname" runat="server"><input id="DocumentID" type="hidden" value="0"
                        name="hostname" runat="server"><input id="ControlValue" type="hidden" runat="server"
                            name="ControlValue">
            <asp:ImageButton ID="btnAddControlItem" runat="server" Height="0px" Width="0px">
            </asp:ImageButton><input id="controlName" type="hidden" runat="server"><input type="hidden"
                id="CaptureType" runat="server"><input id="ObjectCheck" type="hidden" runat="server"></div>
        <asp:ImageButton ID="dummySave" runat="server" Height="0px" Width="0px" />

        <script language="javascript" src="scripts/tooltip_wrap.js"></script>

        <script language="javascript" src="scripts/jquery.js"></script>

        <script language="javascript" src="scripts/jquery.autocomplete.js"></script>

        <script language="javascript" src="scripts/viewerAttributes.js"></script>

        <script language="javascript">

            var bolLoading = 0;
            var dirtyChecked = 0;
            var ignoreDirty = false;
            var dataEntered = false;
            var glEnabled = true;
            var controlTextbox;
            var attributes = new Array();

            $("#txtTitle").autocomplete("AttributeSuggest.aspx?did=0&aid=-1");
            $("#txtComments").autocomplete("AttributeSuggest.aspx?did=0&aid=-2");
            $("#txtTags").autocomplete("AttributeSuggest.aspx?did=0&aid=-3");

            DisableSave();

            setTimeout('setAttributesHeight();', 900);

            function setAttributesHeight() {
                /*
                var divAttributes = document.getElementById('divAttributes');
                if (divAttributes != undefined)
                {
                var newHeight = document.body.clientHeight - 200;
                if (newHeight <= 0 ) {newHeight = 150;}
                divAttributes.style.height = newHeight;
                divAttributes.style.width = 240;
                }
                */
            }

            function setDataEntered() {
                dataEntered = true;
            }

            function postback() {

                try {
                    var objCapture = new ActiveXObject("docAssistVersion.Version");
                    var ObjectCheck = document.getElementById('ObjectCheck');
                    if (ObjectCheck.value != objCapture.Version()) {
                        //new version available
                        window.open('Install.aspx');
                        return false;
                    }
                }
                catch (ex) {
                    //nothing installed
                    window.open('Install.aspx');
                    return false;
                } try {

                    var cap = new ActiveXObject("docAssistCapture.Web");

                    for (i = 0; i <= headers.length - 1; i++) {

                        //loop through all the integration mappings
                        var captured = cap.MainCall(headers[i], details[i], true);

                        if (captured.substring(0, 6) == 'ERROR|') {
                            if (captured.replace('ERROR|', '') == 'Unable to locate window') {
                                //var appName = document.getElementById('ApplicationName').value;
                                //alert('Unable to capture attributes. Please make sure ' + appName + ' is open.');
                                //return true;
                            }
                            else {
                                //alert(captured.replace('ERROR|',''));
                                event.cancelBubble = true;
                                event.returnValue = false;
                            }
                        }
                        else {
                            document.getElementById('FolderMap').value = foldermap[i];
                            document.getElementById('FormID').value = formid[i];
                            document.getElementById('DocumentID').value = documentid[i];
                            document.getElementById('CaptureType').value = capturetype[i];
                            document.getElementById('IntegrationValues').value = '';
                            document.getElementById('IntegrationValues').value = captured;
                            //document.getElementById('btnIntegration').click();
                            //							        event.cancelBubble = true;
                            //							        event.returnValue = false;
                            $("#btnIntegration").click();
                            //return true;
                        }
                    }

                    //not found
                    //alert('Unable to locate a valid application window. Please try again.');
                    //alert('Unable to locate one of the following application windows:\n\n' + strn + '\nPlease make sure you have the application open and try again.');
                    event.cancelBubble = true;
                    event.returnValue = false;
                }
                catch (ex) {
                    alert(ex.message);
                    alert('An error has occured while capturing data. Please try again.');
                }
            }

            function saveCookie(name, value, days) {
                if (!days) expires = ""
                else {
                    var date = new Date();
                    date.setTime(date.getTime() +
							(days * 24 * 60 * 60 * 1000))
                    var expires = "; expires=" +
							date.toGMTString()
                }
                document.cookie = name + "=" +
						escape(value) + expires + "; path=/"
            }

            function clearCookie(name) {
                if (!1) expires = ""
                else {
                    var date = new Date();
                    date.setTime(date.getTime() +
							(1 * 24 * 60 * 60 * 1000))
                    var expires = "; expires=" +
							date.toGMTString()
                }
                document.cookie = name + "=" +
						escape("") + expires + "; path=/"
            }

            function readCookie(name) {
                name += "="
                var ca = document.cookie.split(';')
                for (var i in ca) {
                    var c = ca[i];
                    while (c.charAt(0) == ' ')
                        c = c.substring(1, c.length)
                    if (c.indexOf(name) == 0)
                        return unescape(c.substring(name.length, c.length))
                }
                return ''
            }

            function CheckNotDirty() {
                var dirty = document.all.hiddenDirty.value;
                if ((dirty == "1") && (document.getElementById('ReadOnlyUser').value == 0)) {
                    event.returnValue = 'All your changes will be lost. You must save them before leaving the page.';
                }
            }

            function hourglass() {
                document.body.style.cursor = "wait";
            }

            function findPosX(obj) {
                var curleft = 0;
                if (obj.offsetParent) {
                    while (obj.offsetParent) {
                        curleft += obj.offsetLeft
                        obj = obj.offsetParent;
                    }
                }
                else if (obj.x)
                    curleft += obj.x;
                return curleft;
            }

            function findPosY(obj) {
                var curtop = 0;
                if (obj.offsetParent) {
                    while (obj.offsetParent) {
                        curtop += obj.offsetTop
                        obj = obj.offsetParent;
                    }
                }
                else if (obj.y)
                    curtop += obj.y;
                return curtop;
            }

            function OCR() {
                if (!bolLoading) {
                    var coordinates = parent.document.all.Coordinates;
                    var versionId = document.all.lblVersionId;
                    var pageNo = top.frames["frameImage"].document.all.lblPageNo.value;
                    var lblFileName = document.all.lblFileNameResult.value;

                    if (coordinates.value != "" && document.all.ActiveRow.value >= 0) {
                        bolLoading = 1;
                        hourglass()

                        if (versionId.value > 0)
                            svcOCR.Ocr.callService("ReadOCRFromImage", versionId.value, pageNo, coordinates.value);
                        else {
                            svcOCR.Ocr.callService("ReadOCRFromFile", lblFileName, coordinates.value);
                        }
                        top.frames["frameImage"].startLoading('Performing smartScan...');
                    }
                    else {
                        if (coordinates.value == "")
                            alert("No area has been selected. Use the pointer to select an area and try again.");
                        else if (document.all.txtAttributeValue == undefined)
                            alert("You need to be in edit mode");
                    }
                }
            }

            function Save() {
                parent.trackAnnotation();
                var btnSave = document.getElementById('btnSave');
                btnSave.click();
            }

            function dSave() {
                var dummySave = document.getElementById('dummySave');
                dummySave.click();
            }

            //					function SetupServices()
            //					{
            //						hostname = document.all.hostname.value;
            //						svcOCR.useService(hostname + "/docAssistImages.asmx?wsdl", "Ocr");
            //					}

            function ShowCalendar() {
                var selectedItem = Math.abs(document.all.selectedIndex.value) + 2;
                strFieldName = "dgAttributes__ctl" + selectedItem + "_txtAttributeValue";
                var txtAttributeValue = document.getElementById(strFieldName);
                x = findPosX(txtAttributeValue); //+ txtAttributeValue.style.offsetWidth;
                y = findPosY(txtAttributeValue);
                var calAttribute = document.all.calAttribute;
                calAttribute.style.top = y + 20;
                calAttribute.style.left = 26;
                calAttribute.style.visibility = "visible";
                document.all.showCalendar.value = 1;

            }

            function submitHandler() {
                document.forms[0].doSubmit();
            }
            function OCRResult() {

                bolLoading = 0;
                document.body.style.cursor = "default";
                if (event.result.error) {
                    //Pull the error information from the event.result.errorDetail properties
                    var xfaultcode = event.result.errorDetail.code;
                    var xfaultstring = event.result.errorDetail.string;
                    var xfaultsoap = event.result.errorDetail.raw;

                    // Add code to handle specific codes here
                    if (xfaultstring == "Server was unable to process request. --> Image area is too large, please select a smaller area") {
                        alert("The area you have selected is too large, please select a smaller area and try again.");
                        top.frames["frameImage"].stopLoading();
                    }
                    else {
                        //alert("Error: (" + xfaultcode + ") " + xfaultstring);
                        alert('An error has occured while scanning this region. Please try again. If the problem continues please contact support.');
                        top.frames["frameImage"].stopLoading();
                    }
                }
                else {
                    //document.all.txtAttributeValue.value = event.result.value;
                    if (event.result.value.indexOf("(-2)") != 0) {

                        if (event.result.value == '') {
                            alert('No data was returned from the server. Make sure you have selected an area with text.');
                        }
                        else {
                            var selectedItem = Math.abs(document.all.ActiveRow.value) + 2;
                            strFieldName = "dgAttributes__ctl" + selectedItem + "_txtAttributeValue";
                            var txtAttributeValue = document.getElementById(strFieldName);
                            if (txtAttributeValue != undefined) { txtAttributeValue.value = event.result.value; }
                            ActivateSave();
                        }

                        top.frames["frameImage"].stopLoading();
                    }
                    else
                        alert('Oops... no response was recieved. Please try again. If the problem continues contact support.');
                    top.frames["frameImage"].stopLoading();
                }
            }

            function textPopup() {
                var url;
                window.open("DocumentText.aspx", "Text", "height=600,resizable=no,scrollbars=yes,status=yes,toolbar=no,width=500");
                return false;
            }

            function LoadAnnotation() {
                try {
                    document.all.PageNo.value = parent.frames["frameImage"].PageNo();
                    document.all.Annotation.value = parent.frames["frameImage"].GetAnnotation();
                }
                catch (ex) {

                }
            }

            function ActivateSave() {
                if (document.all.hiddenDirty.value != 1) {
                    dirtyChecked = '0';
                    document.all.hiddenDirty.value = 1;
                    parent.document.getElementById('navIndex_imgSave').src = "Images/save_small.gif";
                }
            }

            function GetDirty() {
                return document.all.hiddenDirty.value;
            }

            function DisableSave() {
                var hiddenDirty = document.getElementById('hiddenDirty');
                if (hiddenDirty != undefined) { document.all.hiddenDirty.value = 0; }
                dirtyChecked = '1';
                parent.document.getElementById('navIndex_imgSave').src = "Images/save_small.off.gif"; ;
            }

            function Confirmation() {
                try {
                    parent.frames["frameImage"].Confirmation();
                }
                catch (ex) {
                }
            }

            function WorkFlowIdSet(wfid) {
                document.all.WorkflowId.value = wfid;
                if (document.all.hiddenDirty.value != 1) {
                    document.all.hiddenDirty.value = 1;
                    top.ActivateSave();
                }

            }

            function QueueIdSet(queueid) {
                document.all.QueueId.value = queueid;
                if (document.all.hiddenDirty.value != 1) {
                    document.all.hiddenDirty.value = 1;
                    top.ActivateSave();
                }
            }

            function WorkflowNoteSet(wfnote) {
                document.all.WorkflowNote.value = wfnote;
                if (document.all.hiddenDirty.value != 1) {
                    document.all.hiddenDirty.value = 1;
                    top.ActivateSave();
                }
            }

            function Urgent(val) {
                document.all.Urgent.value = val;
                if (document.all.hiddenDirty.value != 1) {
                    document.all.hiddenDirty.value = 1;
                    top.ActivateSave();
                }
            }

            function Mode(val) {
                document.all.Mode.value = val;
                if (document.all.hiddenDirty.value != 1) {
                    document.all.hiddenDirty.value = 1;
                    if (top.tblNavigation) {
                        top.ActivateSave();
                    }
                }
            }

            function RouteId(val) {
                document.all.RouteId.value = val;
                if (document.all.hiddenDirty.value != 1) {
                    document.all.hiddenDirty.value = 1;
                    if (top.tblNavigation) {
                        top.ActivateSave();
                    }
                }
            }

            function commitRow(ctrl) {
                if (event.keyCode == 13) {
                    event.cancelBubble = true;
                    event.returnValue = false;
                    var btnSaveItem = document.getElementById(ctrl);
                    btnSaveItem.click();
                }
            }

            function clearDirty() {
                document.all.hiddenDirty.value = 0;
                document.all.selectedIndex.value = 0;
                document.all.moveNext.value = "0";
                document.all.hiddenSubmit.value = "";
                ignoreDirty = true;
            }

            function FolderLookup() {
                var val = window.showModalDialog('Tree.aspx?src=viewer', 'FolderLookup', 'resizable:no;status:no;dialogHeight:625px;dialogWidth:260px;scroll:no');
                if (val != undefined) {
                    varArray = val.split('�');
                    document.all.FolderId.value = varArray[0].replace('W', '');
                    document.all.txtFolder.value = varArray[1];
                    ActivateSave();
                    var folder = varArray[0].replace('W', '');
                    try { window.parent.frames['frmDistribution'].setFolder(folder); } catch (ex) { }
                }
            }

            function ClearFolder() {
                document.getElementById('FolderId').value = '';
                document.getElementById('txtFolder').value = '';
                ActivateSave();
            }

            function setFolder(id, name) {
                document.getElementById('FolderId').value = id;
                document.getElementById('txtFolder').value = name;
                ActivateSave();
            }

            function catchKey() {

                var VK_LEFT = 37;
                var VK_UP = 38;
                var VK_RIGHT = 39;
                var VK_DOWN = 40;

                switch (event.keyCode) {
                    case VK_LEFT:
                        { window.alert("Left"); break }
                    case VK_UP:
                        { window.alert("Up"); break }
                    case VK_RIGHT:
                        { window.alert("Right"); break }
                    case VK_DOWN:
                        { window.alert("Down"); break }
                    default:
                        event.cancelBubble = true;
                        event.returnValue = false;
                        return false;
                        break;
                }
            }

            function browseAttribute(attributeId, txtAttributeValue) {
                var d = new Date();
                var random = d.getHours() + d.getMinutes() + d.getSeconds() + d.getMilliseconds();
                var folderId = document.all.FolderId.value;
                if (folderId == '') { folderId = 0; }
                var documentId = document.getElementById('ddlDocuments').options[document.getElementById('ddlDocuments').selectedIndex].value;
                var ret = window.showModalDialog('ListLookup.aspx?Id=' + attributeId + '&DocumentId=' + documentId + '&FolderID=' + folderId + '&' + random, 'SharePermissions', 'resizable:yes;status:no;dialogHeight:550px;dialogWidth:450px;scroll:no');
                if (ret != undefined) {
                    var txtAttributeValue = document.getElementById(txtAttributeValue);
                    if (txtAttributeValue != undefined) {
                        txtAttributeValue.value = ret.split('�')[0];
                        ActivateSave();
                    }
                }
            }

            function getFolderId() {
                return document.all.FolderId.value;
            }

            function getDocumentId() {
                return document.getElementById('ddlDocuments').options[document.getElementById('ddlDocuments').selectedIndex].value;
            }

            function loadFolder() {
                try { window.parent.frames['frmDistribution'].setFolder(document.getElementById('FolderId').value); } catch (ex) { }
            }

            var controlTextbox;

            function getGl() {
                return glEnabled;
            }

            function hideDistribution() {
                try {
                    setGlEnabled(true);
                    window.parent.hideDistribution();
                }
                catch (Ex) {

                }
            }

            function setControl(val) {
                try {

                    if (document.all.controlName.value == '') {
                        document.all.ControlValue.value = val;
                        document.getElementById('btnAddControlItem').click();
                        return true;
                    }

                    controlTextbox = document.getElementById(document.all.controlName.value);

                    if (controlTextbox == undefined) {
                        document.all.ControlValue.value = val;
                        document.getElementById('btnAddControlItem').click();
                    }
                    else {
                        controlTextbox.value = val;
                    }
                }
                catch (ex) {

                }
            }

            function getControl() {
                if (document.all.controlName.value == '') { return '0.00'; };
                controlTextbox = document.getElementById(document.all.controlName.value);
                if (controlTextbox == undefined) {
                    return '0.00';
                }
                else {
                    return controlTextbox.value;
                }
            }

            function setGlEnabled(setting) {
                glEnabled = setting;
            }

            function NumericOnly(evt) {
                evt = (evt) ? evt : event;
                var charCode = (evt.charCode) ? evt.charCode : ((evt.keyCode) ? evt.keyCode
					: ((evt.which) ? evt.which : 0));
                if (charCode > 31 && (charCode < 48 || charCode > 57)) { return false; }
                return true;
            }

            function showDistributionDialog(id, folderid) {
                var ctrlTotal = getControl();
                var returnVal = window.showModalDialog('Distribution.aspx?Id=' + document.all.ddlDocuments.value + '&FolderId=' + document.all.FolderId.value + '&Control=' + ctrlTotal, 'Distribution', 'resizable:no;status:no;dialogHeight:390px;dialogWidth:650px;scroll:no');
                if (returnVal != undefined) {
                    setControl(returnVal);
                }
                ActivateSave();
            }

            function NumericOnlyandDecimals(evt) {
                evt = (evt) ? evt : event;
                var charCode = (evt.charCode) ? evt.charCode : ((evt.keyCode) ? evt.keyCode
				: ((evt.which) ? evt.which : 0));
                if (charCode == 46) { return true; }
                if (charCode > 31 && (charCode < 48 || charCode > 57)) { return false; }
                return true;
            }

            function relatedDocs() {
                window.parent.showRelated(document.getElementById('lblImageId').innerHTML);
            }

            function addQuality(name, value) {
                var count = document.getElementById('navIndex_ddlQuality').length;
                document.getElementById("navIndex_ddlQuality").options[count] = new Option(name, value);
            }

            function selectQuality(quality) {
                document.getElementById("navIndex_ddlQuality").value = quality;
            }

            function clearQuality() {
                var list = document.getElementById("navIndex_ddlQuality");
                var i = 0;
                while (i < list.options.length) {
                    list.options.remove(i);
                }
            }

            function closeAction(action, versionId, data, docInfo) {
                if (parent.parent.$("#GB_frame").contents().find('#frameFile').contents().find('#txtUploadFilename').length > 0) {
                    if (parent.parent.$("#GB_frame").contents().find('#frameFile').contents().find('#txtUploadFilename').val() != '') {
                        //upload file
                    } else {
                        if (closeViewer == "1") { parent.parent.GB_hide(); }
                        parent.parent.closeHandler(action, versionId, data, docInfo);
                    }
                } else {
                    if (closeViewer == "1") { parent.parent.GB_hide(); }
                    parent.parent.closeHandler(action, versionId, data, docInfo);
                }
                //parent.parent.$("#GB_frame").contents().find('#frameAttributes').remove();
            }

            function removeFocus() {
                $("#tblFolder").focus();
            }

            function reloadAnnotations() {
                parent.reloadAnnotations();
            }
				
        </script>

        <asp:Literal ID="litSave" runat="server"></asp:Literal>
        <asp:Literal ID="litChangeAttribute" runat="server"></asp:Literal>
        <asp:Literal ID="litRefreshIntegration" runat="server"></asp:Literal>
        <asp:Literal ID="litAttributes" runat="server"></asp:Literal>
        <asp:Literal ID="litAttributeRender" runat="server"></asp:Literal>
    </form>
    </table>
    <asp:Literal ID="script" runat="server"></asp:Literal>
</body>
</html>

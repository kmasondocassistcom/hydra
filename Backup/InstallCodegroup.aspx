<%@ Page Language="vb" AutoEventWireup="false" Codebehind="InstallCodegroup.aspx.vb" Inherits="docAssistWeb.InstallCodegroup" EnableSessionState="ReadOnly" enableViewState="True" %>
<%@ Register TagPrefix="uc1" TagName="docHeader" Src="Controls/docHeader.ascx" %>
<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="Controls/MenuBar.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>docAssist - Update Components</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<TABLE height="100%" cellSpacing="0" cellPadding="0" border="0">
				<TR>
					<uc1:docHeader id="docHeader" runat="server"></uc1:docHeader>
				</TR>
				<TR>
					<TD width="50%"></TD>
					<TD align="left">
						<span id="InstallContent" runat="server">
							<P><FONT style="FONT-SIZE: 8pt; FONT-FAMILY: verdana"><STRONG>&nbsp;&nbsp;&nbsp;&nbsp; In 
										order to use the docAssist Indexer there are minimum requirements that must be 
										installed on your<BR>
										&nbsp;&nbsp;&nbsp; &nbsp;computer.&nbsp;docAssist has detected that one or more 
										of these requirements are not installed on your computer.<BR>
										<BR>
										&nbsp;&nbsp;&nbsp;&nbsp; Please ensure that the following are installed on your 
										computer:<BR>
										<BR>
									</STRONG>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - docAssist Indexer Setup<BR>
								</FONT><FONT style="FONT-SIZE: 8pt; FONT-FAMILY: verdana">
									<BR>
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Microsoft Internet Explorer Version 6.0 with 
									Service Pack 1<BR>
									<BR>
									<STRONG>&nbsp;&nbsp;&nbsp;&nbsp; Below are links to perform the installation of the 
										above requirements:<BR>
										<BR>
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</STRONG><a href="Downloads/docAssistSetup.exe">Download 
										docAssist Indexer Setup</a><BR>
									<BR>
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="Downloads/ie6setup.exe">Download Microsoft 
										Internet Explorer 6.0 Service Pack 1</a><BR>
									<BR>
									<FONT style="FONT-SIZE: 8pt; FONT-FAMILY: verdana"></FONT><STRONG>&nbsp;&nbsp;&nbsp;&nbsp; 
										Please install the missing required components on your computer and click here 
										to continue.<BR>
										<BR>
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</STRONG><EM>Note:&nbsp;If you are recieving this 
										message after upgrading components on your computer it may be necessary to 
										install the docAssist<BR>
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
										Indexer setup again.</EM><FONT style="FONT-SIZE: 8pt; FONT-FAMILY: verdana"></FONT>
									<br>
								</FONT><FONT style="FONT-SIZE: 8pt; FONT-FAMILY: verdana">
						</span><IMG height="1" src="spacer.gif" width="800"></FONT> </P>
					</TD>
					<TD align="right" width="50%"></TD>
				</TR>
				<TR>
					<TD>&nbsp;</TD>
					<TD>&nbsp;</TD>
					<TD>&nbsp;</TD>
				</TR>
			</TABLE>
		</form>
		<script language="javascript">

			document.all.docHeader_imgLauncher.src = "./Images/manager_grey.gif";
			document.all.managerLink.href = "#";

			setTimeout("Timeout()",3600000);
			function Timeout()
			{alert("Your docAssist session has timed out.\n\nAs a security precaution, sessions are ended after 60 minutes of inactivity.\n\nYou can sign in again to resume your session.");
			document.all.btnSignOut.click();}
		</script>
	</body>
</HTML>

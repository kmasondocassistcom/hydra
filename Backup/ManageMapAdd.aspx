<%@ Register TagPrefix="uc1" TagName="AdminNav" Src="Controls/AdminNav.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ManageMapAdd.aspx.vb" Inherits="docAssistWeb.ManageMapAdd"%>
<%@ Register TagPrefix="uc1" TagName="docHeader" Src="Controls/docHeader.ascx" %>
<%@ Register TagPrefix="uc1" TagName="TopRight" Src="Controls/TopRight.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>docAssist - Manage Integration</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<TABLE height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TBODY>
					<TR>
						<TD width="100%" colSpan="5"><uc1:docheader id="docHeader" runat="server"></uc1:docheader></TD>
					</TR>
					<TR vAlign="middle" height="100%">
						<TD class="Navigation1" style="BORDER-RIGHT: #cccccc thin solid" vAlign="top" height="100%"><uc1:adminnav id="AdminNav1" runat="server"></uc1:adminnav><IMG height="1" src="dummy.gif" width="120" DESIGNTIMEDRAGDROP="101"></TD>
						<TD vAlign="top" align="center" height="100%"><BR>
							<table cellSpacing="0" cellPadding="0" width="100%" border="0">
								<tr>
									<td><IMG height="1" src="dummy.gif" width="35"><IMG src="Images/manage_integration.gif"></td>
								</tr>
							</table>
							<table height="100%" cellSpacing="0" cellPadding="0" width="100%">
					</TR>
					<tr height="50%">
					</tr>
					<tr height="15" width="100%">
						<td style="WIDTH: 367px"></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr height="15" width="100%">
						<td style="WIDTH: 367px"></td>
						<td style="WIDTH: 72px" align="left"><asp:label id="Label1" runat="server" Font-Names="Verdana" Font-Size="8.25pt" Width="136px"
								Height="16px">Product Manufacturer:</asp:label></td>
						<TD>
							<P><asp:dropdownlist id="cmbPublisher" runat="server" Font-Names="Verdana" Font-Size="8.25pt" Width="270px"
									Height="32px" AutoPostBack="True"></asp:dropdownlist></P>
						</TD>
						<TD width="50%"></TD>
					<TR height="15" width="100%">
						<td style="HEIGHT: 21px" width="50%"></td>
						<td style="WIDTH: 72px; HEIGHT: 21px" align="left"><asp:label id="Label2" runat="server" Font-Names="Verdana" Font-Size="8.25pt" Width="72px"
								Height="17px"> Application:</asp:label></td>
						<td style="HEIGHT: 21px"><asp:dropdownlist id="cmbProduct" runat="server" Font-Names="Verdana" Font-Size="8.25pt" Width="270px"
								Height="32px" AutoPostBack="True"></asp:dropdownlist></td>
						<td style="HEIGHT: 21px" width="50%"></td>
					</TR>
					<tr height="15" width="100%">
						<td style="WIDTH: 367px" width="367"></td>
						<td style="WIDTH: 72px; HEIGHT: 17px">
							<P align="left"><asp:label id="Label3" runat="server" Font-Names="Verdana" Font-Size="8.25pt" Width="56px"
									Height="14px"> Version:</asp:label></P>
						</td>
						<td style="HEIGHT: 17px"><asp:dropdownlist id="cmbVersion" runat="server" Font-Names="Verdana" Font-Size="8.25pt" Width="270px"
								Height="32px" AutoPostBack="True"></asp:dropdownlist></td>
						<td width="50%"></td>
					</tr>
					<tr height="15" width="100%">
						<td style="WIDTH: 367px" width="367" height="21"></td>
						<td style="WIDTH: 72px" vAlign="middle" height="21"><P align="left"><asp:label id="Label4" runat="server" Font-Names="Verdana" Font-Size="8.25pt" Width="40px"
									Height="17px"> Form:</asp:label></P>
						</td>
						<td align="left" height="21"><asp:dropdownlist id="cmbForm" runat="server" Font-Names="Verdana" Font-Size="8.25pt" Width="270px"
								Height="32px" AutoPostBack="True"></asp:dropdownlist></td>
						<td width="50%" height="21"></td>
					</tr>
					<tr height="15" width="100%">
						<td style="WIDTH: 367px; HEIGHT: 25px" width="367"></td>
						<td style="WIDTH: 72px; HEIGHT: 25px"></td>
						<td style="HEIGHT: 25px"><asp:checkbox id="chkAdvanced" runat="server" AutoPostBack="True" Text="Assign advanced mappings"></asp:checkbox></td>
						<td style="HEIGHT: 25px" width="50%"></td>
					</tr>
					<tr height="15" width="100%">
						<td style="WIDTH: 367px; HEIGHT: 25px" width="367"></td>
						<td style="WIDTH: 72px; HEIGHT: 25px"></td>
						<td style="HEIGHT: 25px"><asp:label id="lblError" runat="server" Font-Names="Verdana" Font-Size="8.25pt" Width="100%"
								Height="14px" ForeColor="Red"></asp:label></td>
						<td style="HEIGHT: 25px" width="50%"></td>
					</tr>
					<tr height="15" width="100%">
						<td style="WIDTH: 395px" width="395"></td>
						<td style="WIDTH: 72px"><BR>
							<P>&nbsp;</P>
						</td>
						<td align="right"><asp:imagebutton id="btnCancel" runat="server" ImageUrl="Images/btn_canceladmin.gif"></asp:imagebutton>&nbsp;<asp:imagebutton id="btnNext" runat="server" ImageUrl="Images/btn_finish.gif"></asp:imagebutton></td>
						<td width="50"></td>
					</tr>
					<tr height="100%">
						<td></td>
					</tr>
				</tr></TABLE>
			<P><BR>
				<BR>
			</P>
			</TD>
			<TD class="RightContent" height="100%">&nbsp;</TD>
			</TR>
			<TR>
				<TD class="PageFooter" vAlign="bottom" bgColor="#cccccc" colSpan="3"><uc1:topright id="docFooter" runat="server"></uc1:topright></TD>
			</TR>
			<TR height="1">
				<TD style="BACKGROUND-COLOR: #959394" width="20"><IMG height="1" src="images/spacer.gif" width="20"></TD>
				<TD style="BACKGROUND-COLOR: #959394" width="100%"><IMG height="1" src="images/spacer.gif" width="100%"></TD>
				<TD style="BACKGROUND-COLOR: #959394" width="20"><IMG height="1" src="images/spacer.gif" width="20"></TD>
			</TR>
			</TBODY></TABLE></form>
	</body>
</HTML>

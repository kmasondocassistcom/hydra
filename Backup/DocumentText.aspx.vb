Imports dtSearch.Engine
Imports System.Data.SqlClient

Partial Class DocumentText
    Inherits System.Web.UI.Page

    Protected WithEvents OpenCase As System.Web.UI.HtmlControls.HtmlButton

    Private mobjUser As Accucentric.docAssist.Web.Security.User
    Private cookieSearch As cookieSearch

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        SearchText.Attributes("onkeypress") = "keyPressed();"

        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetAllowResponseInBrowserHistory(False)

        cookieSearch = New cookieSearch(Me)

        If Not Page.IsPostBack Then
            'Check for searched word
            If cookieSearch.Keywords.Trim.Length > 0 Then
                SearchText.Text = cookieSearch.Keywords.Trim
                'ReturnControls.Add(SearchText)
            End If

            LoadText(SearchText.Text)

        End If

    End Sub

    Private Sub LoadText(ByVal Search As String)

        If Search.Trim.Length = 0 Then
            lblSummary.Text = "Enter a search to filter your document's full text."
            Exit Sub
        End If

        Dim intVersionId As Integer
        Dim dtDocPages As New DataTable
        Dim sqlConImages As New SqlClient.SqlConnection
        Dim cookieImage As New cookieImage(Me)
        Dim cookieSearch As New cookieSearch(Me)
        Dim intPageCount As Integer = 1
        Dim blnOnePageHasText As Boolean = False
        Dim sbDocumentText As New System.Text.StringBuilder
        Dim summary As New System.Text.StringBuilder
        Dim intHitCount As Integer = 0
        Dim blnSearch As Boolean = False

        intVersionId = cookieSearch.VersionID

        Try

            mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
            Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))

            sqlConImages = (Functions.BuildConnection(mobjUser))
            sqlConImages.Open()

            Dim sqlCmd As New SqlClient.SqlCommand("acsImageTextByVersionId", sqlConImages)
            sqlCmd.CommandType = CommandType.StoredProcedure
            sqlCmd.Parameters.Add("@VersionID", intVersionId)
            Dim sqlda As New SqlClient.SqlDataAdapter(sqlCmd)
            sqlda.Fill(dtDocPages)

            If dtDocPages.Rows.Count = 0 Then

                lblSummary.Text = "No text is available for this document."
                Exit Sub

            Else

                'If Functions.ModuleSetting(Functions.ModuleCode.FULL_TEXT, Me.mobjUser, sqlConImages) Then

                'New full text
                Dim conFT As SqlClient.SqlConnection = Functions.BuildFullTextConnection(Me.mobjUser)

                Dim strFullText As String
                'Add global temp table for fulltext search results
                Dim sb As New System.Text.StringBuilder
                Dim sqlBld As SqlCommandBuilder

                If Not dtDocPages Is Nothing Then

                    strFullText = System.Guid.NewGuid().ToString().Replace("-", "")
                    sb = New System.Text.StringBuilder
                    sb.Append("CREATE TABLE ##" & strFullText.ToString & " (ImageDetID int NULL, SeqID int NULL)" & vbCrLf)
                    sqlCmd = New SqlCommand(sb.ToString, conFT)
                    sqlBld = New SqlCommandBuilder
                    sqlCmd.ExecuteNonQuery()

                    For Each dr As DataRow In dtDocPages.Rows
                        sqlCmd = New SqlCommand("INSERT INTO ##" & strFullText & " SELECT " & dr("ImageDetId") & ", " & dr("SeqID"), conFT)
                        sqlCmd.CommandType = CommandType.Text
                        sqlCmd.ExecuteNonQuery()
                    Next

                    sqlCmd = New SqlCommand("SMFullTextGet", conFT)
                    sqlCmd.CommandType = CommandType.StoredProcedure
                    sqlCmd.Parameters.Add("@Keyword", SearchText.Text)
                    sqlCmd.Parameters.Add("@Imagetable", "##" & strFullText)
                    Dim res As New DataSet
                    Dim da As New SqlDataAdapter(sqlCmd)
                    da.Fill(res)

                    Dim sum As New System.Text.StringBuilder
                    For Each result As DataRow In res.Tables(1).Rows
                        sum.Append("<b>Page " & result("SeqID") & "</b> " & "<a onclick=parent.GoToPage(" & result("SeqID") & ");parent.hidelbdialog();>Load Page</a>" & "<br><br>")
                        sum.Append("<p>" & System.Text.RegularExpressions.Regex.Replace(result("OCRResult"), Search, "<font style=""BACKGROUND-COLOR: #ffff00"">" & Search & "</font>", System.Text.RegularExpressions.RegexOptions.IgnoreCase) & "</p>")
                    Next

                    If Search.Trim.Length > 0 Then
                        lblSummary.Text = sum.ToString.Replace("<script", "")
                    Else
                        lblSummary.Text = "Enter a search to filter your document's full text."
                    End If

                End If


                'Dim cmdFt As New SqlClient.SqlCommand("SMFullTextSearch", conFT)
                'cmdFt.CommandType = CommandType.StoredProcedure
                'cmdFt.Parameters.Add("@KeyWord", txtQuickSearch.Text.Trim)

                'Dim da As New SqlClient.SqlDataAdapter(cmdFt)
                'Dim res As New DataTable("Results")
                'da.Fill(res)

                'For Each dr As DataRow In res.Rows

                '    Dim drFullText As DataRow
                '    drFullText = mdtFullTextData.NewRow

                '    drFullText("ImageDetID") = dr("ImageDetId")
                '    drFullText("Score") = dr("Score")
                '    drFullText("ScorePercent") = dr("ScorePercent")
                '    drFullText("HitCount") = dr("HitCount")

                '    mdtFullTextData.Rows.Add(drFullText)

                'Next

                conFT.Close()
                conFT.Dispose()


                'Else

                ''Old full text
                'Dim pdf As Boolean = IIf(Request.QueryString("pdf") = "0", False, True)

                'Dim IndexLocationDirectory As String
                'IndexLocationDirectory = ConfigurationSettings.AppSettings("IndexLocation")

                'If IndexLocationDirectory.TrimEnd.Substring(Len(IndexLocationDirectory) - 1, 1) <> "\" Then
                '    IndexLocationDirectory = IndexLocationDirectory.TrimEnd & "\"
                'End If

                'IndexLocationDirectory = IndexLocationDirectory & Me.mobjUser.DBName

                'Dim i As Integer

                'Dim itest() As Integer
                'Dim sj As SearchJob = New SearchJob
                'Dim results As SearchResults

                'sj.IndexesToSearch = New System.Collections.Specialized.StringCollection
                'sj.IndexesToSearch.Add(IndexLocationDirectory)
                'sj.MaxFilesToRetrieve = 10000

                'For Each dr As DataRow In dtDocPages.Rows

                '    If Search.Trim.Length > 0 Then
                '        If pdf Then
                '            sj.Request = "(ImageDetId CONTAINS " & CStr(dr("ImageDetId")) & "-*) AND (" & Search & ")"
                '        Else
                '            sj.Request = "(ImageDetId CONTAINS " & CStr(dr("ImageDetId")) & ") AND (" & Search & ")"
                '        End If
                '    Else
                '        lblSummary.Text = "Enter a search to filter your document's full text."
                '    End If

                '    sj.Execute()

                '    results = sj.Results

                '    If Not pdf Then

                '        If results.Count > 0 Then

                '            results.GetNthDoc(0)
                '            sbDocumentText.Append("<p><b>Page " & intPageCount & "</b> <a onclick=parent.GoToPage(" & intPageCount & ");parent.hidelbdialog();>load</a></p>")
                '            sbDocumentText.Append(System.Text.RegularExpressions.Regex.Replace(CStr(results.CurrentItem.UserFields("Imagetext")).Replace("<", "&lt;").Replace(">", "&gt"), Search, "<font style=""BACKGROUND-COLOR: #ffff00"">" & Search & "</font>", System.Text.RegularExpressions.RegexOptions.IgnoreCase) & "<br><br>")

                '            If Search.Trim.Length > 0 Then

                '                Dim pageText As String = results.CurrentItem.UserFields("Imagetext")
                '                summary.Append("<b>Page " & intPageCount & "</b> (" & Functions.CountOccurrences(pageText, Search) & " hits) " & "<a onclick=parent.GoToPage(" & intPageCount & ");parent.hidelbdialog();>Load Page</a>" & "<br><br>")
                '                summary.Append("<p>" & System.Text.RegularExpressions.Regex.Replace(pageText, Search, "<font style=""BACKGROUND-COLOR: #ffff00"">" & Search & "</font>", System.Text.RegularExpressions.RegexOptions.IgnoreCase) & "</p>")

                '            End If

                '        End If

                '        intPageCount += 1

                '    Else

                '        'Summary
                '        If Search.Trim.Length > 0 Then

                '            'PDF Attachment
                '            If results.Count > 0 Then
                '                For X As Integer = results.Count - 1 To 0 Step -1
                '                    results.GetNthDoc(X)
                '                    Dim pageText As String = results.CurrentItem.UserFields("Imagetext")
                '                    summary.Append("<p><b>Page " & results.CurrentItem.UserFields("Page") & "</b> (" & Functions.CountOccurrences(pageText, Search) & " hits) " & "<a onclick=parent.GoToPage(" & results.CurrentItem.UserFields("Page") & ");parent.hidelbdialog();>Load Page</a></p>")
                '                    summary.Append("<p>" & System.Text.RegularExpressions.Regex.Replace(pageText, Search, "<font style=""BACKGROUND-COLOR: #ffff00"">" & Search & "</font>", System.Text.RegularExpressions.RegexOptions.IgnoreCase) & "</p>")
                '                Next
                '            Else
                '                lblSummary.Text = "No results were found."
                '                Return
                '            End If

                '        End If

                '    End If

                'Next

                'If Search.Trim.Length > 0 Then
                '    If Not pdf And summary.ToString.Length = 0 Then
                '        lblSummary.Text = "No results were found."
                '    Else
                '        lblSummary.Text = summary.ToString.Replace("<script", "")
                '    End If
                'Else
                '    lblSummary.Text = "Enter a search to filter your document's full text."
                'End If

                'End If

            End If

        Catch ex As Exception

            Dim conMaster As SqlClient.SqlConnection
            conMaster = Functions.BuildMasterConnection
            Functions.LogException(mobjUser.UserId.ToString, mobjUser.AccountId.ToString, "", "Full Text Search: " & ex.Message, ex.StackTrace, Server.MachineName, "", Request.UrlReferrer.ToString, conMaster, True)

            lblSummary.Text = "An unexpected error has occured. Our support team has been notified. Please try your search again."

        Finally

            If sqlConImages.State <> ConnectionState.Closed Then
                sqlConImages.Close()
                sqlConImages.Dispose()
            End If

        End Try

    End Sub

    Private Sub Search_ServerClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Search.ServerClick
        cookieSearch.Keywords = SearchText.Text
        LoadText(SearchText.Text)
    End Sub

End Class
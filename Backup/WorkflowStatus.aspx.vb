Partial Class WorkflowStatus
    Inherits System.Web.UI.Page

    Private intStatusId As Integer = 0
    Private intMode As Integer

    Private pageTitle As New System.Web.UI.HtmlControls.HtmlGenericControl

    Private mobjUser As Accucentric.docAssist.Web.Security.User
    Private wfCategory As Accucentric.docAssist.Data.Images.Workflow

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        txtStatusDesc.Attributes("onkeypress") = "submitForm();"
        'txtStatusNote.Attributes("onkeypress") = "submitForm();"

        intMode = CInt(Request.QueryString("Mode"))
        intStatusId = CInt(Request.QueryString("ID"))

        'Security
        Try
            Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
            Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))
            Me.mobjUser.AccountId = guidAccountId
        Catch ex As Exception
            Response.Redirect("Default.aspx")
        End Try

        If Page.IsPostBack = False Then

            'Actions 
            Try

                Select Case intMode
                    Case 0

                    Case 1
                        Dim dtStatus As New DataTable
                        wfCategory = New Accucentric.docAssist.Data.Images.Workflow(Functions.BuildConnection(Me.mobjUser))
                        dtStatus = wfCategory.WFStatusTypesGetID(intStatusId)

                        If dtStatus.Rows.Count > 0 Then
                            txtStatusDesc.Text = dtStatus.Rows(0)("StatusDesc")
                            txtStatusNote.Text = dtStatus.Rows(0)("StatusNote")
                            ddlRouteAction.SelectedValue = dtStatus.Rows(0)("WorkflowAction")

                            If intStatusId <= 1000 Then
                                txtStatusDesc.Enabled = False
                                txtStatusNote.Enabled = False
                                ddlRouteAction.Enabled = False
                                'lblError.Text = "Cannot edit system actions."
                                'lblError.Visible = True
                            End If
                        End If
                    Case 2

                End Select

            Catch ex As Exception

            End Try
        End If

    End Sub

    Private Function LoadCategory(ByVal intCategoryId) As String

    End Function

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click

        Try
            If txtStatusDesc.Text.Length = 0 Then
                lblError.Text = "Status description is required."
                lblError.Visible = True
                Exit Sub
            End If

            'If txtStatusNote.Text.Length = 0 Then
            '    lblError.Text = "Status note is required."
            '    lblError.Visible = True
            '    Exit Sub
            'End If
        Catch ex As Exception

        End Try

        Try

            Select Case intMode
                Case 0
                    wfCategory = New Accucentric.docAssist.Data.Images.Workflow(Functions.BuildConnection(Me.mobjUser))
                    Dim intResult As Integer = wfCategory.WFStatusTypeInsert(txtStatusDesc.Text, txtStatusNote.Text, ddlRouteAction.SelectedValue)
                    If intResult = 0 Then
                        lblError.Text = "Status already exists."
                        lblError.Visible = True
                    Else
                        litReload.Text = "<script language='javascript'>opener.location.reload();window.close();</script>"
                    End If

                Case 1
                    wfCategory = New Accucentric.docAssist.Data.Images.Workflow(Functions.BuildConnection(Me.mobjUser))
                    Dim intResult As Integer = wfCategory.WFStatusTypeUpdate(intStatusId, txtStatusDesc.Text, txtStatusNote.Text, ddlRouteAction.SelectedValue)
                    If intResult = 0 Then
                        lblError.Text = "Status already exists."
                        lblError.Visible = True
                    Else
                        litReload.Text = "<script language='javascript'>opener.location.reload();window.close();</script>"
                    End If
                Case 2

            End Select

        Catch ex As Exception

        End Try

    End Sub
End Class

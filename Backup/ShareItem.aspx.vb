Imports Accucentric.docAssist.Data.Images

Partial Class ShareItem
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private mobjUser As Accucentric.docAssist.Web.Security.User

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim conSql As New SqlClient.SqlConnection

        Try

            Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))

            If Not Page.IsPostBack Then

                'Load shares
                conSql = Functions.BuildConnection(Me.mobjUser)
                Dim objFolders As New Folders(conSql, False)

                Dim shares As DataTable
                Select Case Request.QueryString("Type")
                    Case "F"
                        shares = objFolders.PublicShareList(Folders.ShareType.FOLDER, Request.QueryString("Id").Replace("W", ""))
                    Case "D"
                        shares = objFolders.PublicShareList(Folders.ShareType.DOCUMENT, Request.QueryString("Id").Replace("W", ""))
                End Select

                lstShares.DataSource = shares
                lstShares.DataTextField = "EmailAddress"
                lstShares.DataValueField = "ShareKey"
                lstShares.DataBind()

            End If

        Catch ex As Exception

        Finally

            If conSql.State <> ConnectionState.Closed Then
                conSql.Close()
                conSql.Dispose()
            End If

        End Try

    End Sub

    Private Sub btnInvite_ServerClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInvite.ServerClick

        Dim conSql As New SqlClient.SqlConnection

        Try

            If address.Value.Trim.Length = 0 Then
                lEmail.InnerHtml = lEmail.InnerHtml & "<span class=labelError> (email is required)</span>"
                Exit Sub
            Else
                lEmail.InnerHtml = "E-mail address"
            End If

            'Dim people() As String = address.Value.Split(",")

            'Dim bError As Boolean = False
            'For Each item As String In people
            '    If Not Functions.CheckValidEmailAddress(item) Then
            '        bError = True
            '    End If
            'Next

            If Not Functions.CheckValidEmailAddress(address.Value.Trim) Then
                lEmail.InnerHtml = lEmail.InnerHtml & "<span class=labelError> (valid email is required)</span>"
                Exit Sub
            Else
                lEmail.InnerHtml = "E-mail address"
            End If

            Dim exist As Boolean = False
            For Each person As ListItem In lstShares.Items
                If person.Text = address.Value.Trim Then
                    lEmail.InnerHtml = lEmail.InnerHtml & "<span class=labelError> (this person already has access)</span>"
                    Exit Sub
                End If
            Next

            conSql = Functions.BuildConnection(Me.mobjUser)
            Dim objFolders As New Folders(conSql, False)

            Dim key As String

            Select Case Request.QueryString("Type")
                Case "F"
                    key = objFolders.PublicShareInsert(Folders.ShareType.FOLDER, Request.QueryString("Id").Replace("W", ""), address.Value.Trim)
                Case "D"
                    key = objFolders.PublicShareInsert(Folders.ShareType.DOCUMENT, Request.QueryString("Id").Replace("W", ""), address.Value.Trim)
            End Select

            'Send email
            Dim link As New System.Text.StringBuilder
            link.Append(Me.mobjUser.AccountId.ToString & ";")
            link.Append(Request.QueryString("Id").Replace("W", "") & ";")
            link.Append(Request.QueryString("Type") & ";")
            link.Append(key & ";")
            link.Append(address.Value.Trim & ";")
            link.Append(Session("FileType"))

            Dim params As New Collection
            params.Add(New EmailParameter("USER", Me.mobjUser.UserName))

            If message.Value.Trim.Length = 0 Then
                params.Add(New EmailParameter("MESSAGE", ""))
            Else
                params.Add(New EmailParameter("MESSAGE", "<i>" & message.Value & "</i>"))
            End If

            Dim enc As New Encryption.Encryption

            Select Case Request.QueryString("Type")
                Case "F"
                    If Request.Url.ToString.StartsWith("http://dev.docassist") Then
                        params.Add(New EmailParameter("LINK", "http://dev.docassist.com/pub/ViewShared.aspx?q=" & enc.Encrypt(link.ToString).Replace("=", "~").Replace("+", "^")))
                    ElseIf Request.Url.ToString.StartsWith("http://stage.docassist") Then
                        params.Add(New EmailParameter("LINK", "http://stage.docassist.com/pub/ViewShared.aspx?q=" & enc.Encrypt(link.ToString).Replace("=", "~").Replace("+", "^")))
                    ElseIf Request.Url.ToString.StartsWith("https://stage.docassist") Then
                        params.Add(New EmailParameter("LINK", "https://stage.docassist.com/pub/ViewShared.aspx?q=" & enc.Encrypt(link.ToString).Replace("=", "~").Replace("+", "^")))
                    ElseIf Request.Url.ToString.IndexOf("localhost") > 0 Then
                        params.Add(New EmailParameter("LINK", "http://localhost/docAssistWeb1/pub/ViewShared.aspx?q=" & enc.Encrypt(link.ToString).Replace("=", "~").Replace("+", "^")))
                    ElseIf Request.Url.ToString.StartsWith("https://my.docassist") Then
                        params.Add(New EmailParameter("LINK", "https://my.docassist.com/pub/ViewShared.aspx?q=" & enc.Encrypt(link.ToString).Replace("=", "~").Replace("+", "^")))
                    End If
                Case "D"
                    If Request.Url.ToString.StartsWith("http://dev.docassist") Then
                        params.Add(New EmailParameter("LINK", "http://dev.docassist.com/pub/DownloadDocument.aspx?q=" & enc.Encrypt(link.ToString).Replace("=", "~").Replace("+", "^")))
                    ElseIf Request.Url.ToString.StartsWith("http://stage.docassist") Then
                        params.Add(New EmailParameter("LINK", "http://stage.docassist.com/pub/DownloadDocument.aspx?q=" & enc.Encrypt(link.ToString).Replace("=", "~").Replace("+", "^")))
                    ElseIf Request.Url.ToString.StartsWith("https://stage.docassist") Then
                        params.Add(New EmailParameter("LINK", "https://stage.docassist.com/pub/DownloadDocument.aspx?q=" & enc.Encrypt(link.ToString).Replace("=", "~").Replace("+", "^")))
                    ElseIf Request.Url.ToString.IndexOf("localhost") > 0 Then
                        params.Add(New EmailParameter("LINK", "http://localhost/docAssistWeb1/pub/DownloadDocument.aspx?q=" & enc.Encrypt(link.ToString).Replace("=", "~").Replace("+", "^")))
                    ElseIf Request.Url.ToString.StartsWith("https://my.docassist") Then
                        params.Add(New EmailParameter("LINK", "https://my.docassist.com/pub/DownloadDocument.aspx?q=" & enc.Encrypt(link.ToString).Replace("=", "~").Replace("+", "^")))
                    End If
            End Select

            params.Add(New EmailParameter("PROMOLINK", "http://www.docassist.com"))

            'conSql = Functions.BuildConnection(Me.mobjUser)
            'conSql.Open()

            'Dim addressBook As New Email.AddressBook(conSql, False)
            'addressBook.InsertEmailAddress(Me.mobjUser.ImagesUserId, Str)

            Select Case Request.QueryString("Type")
                Case "F"
                    Functions.sendEmailMessage(Functions.EmailType.SHARE_FOLDER, address.Value.Trim, "", "", _
                    params, Me, IIf(subject.Value.Trim.Length = 0, Me.mobjUser.UserName & " wants to share a folder with you", subject.Value), "noreply@docassist.com")
                Case "D"
                    Functions.sendEmailMessage(Functions.EmailType.SHARE_DOCUMENT, address.Value.Trim, "", "", _
                    params, Me, IIf(subject.Value.Trim.Length = 0, Me.mobjUser.UserName & " wants to share a document with you", subject.Value), "noreply@docassist.com")
            End Select

            'Add to list
            lstShares.Items.Add(New ListItem(address.Value.Trim, Request.QueryString("Id").Replace("W", "")))

            address.Value = ""
            subject.Value = ""
            message.Value = ""

        Catch ex As Exception

        Finally

            If conSql.State <> ConnectionState.Closed Then
                conSql.Close()
                conSql.Dispose()
            End If

        End Try

    End Sub

    Private Sub btnRemove_ServerClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemove.ServerClick

        Dim conSql As New SqlClient.SqlConnection

        Try

            conSql = Functions.BuildConnection(Me.mobjUser)
            Dim objFolders As New Folders(conSql, False)

            Select Case Request.QueryString("Type")
                Case "F"
                    objFolders.PublicShareRemove(Folders.ShareType.FOLDER, lstShares.SelectedValue, lstShares.SelectedItem.Text)
                Case "D"
                    objFolders.PublicShareRemove(Folders.ShareType.DOCUMENT, lstShares.SelectedValue, lstShares.SelectedItem.Text)
            End Select

            lstShares.Items.Remove(lstShares.SelectedItem)

            'For x As Integer = lstShares.Items.Count - 1 To 0 Step -1
            '    If lstShares.Items(x).Selected Then
            '        objFolders.PublicShareRemove(Folders.ShareType.FOLDER, lstShares.Items(x).Value, lstShares.Items(x).Text)
            '        lstShares.Items.Remove(lstShares.Items(x))
            '    End If
            'Next

            'For x As Integer = lstShares.Items.Count - 1 To 0 Step -1
            '    If lstShares.Items(x).Selected Then
            '        'objFolders.PublicShareRemove(Folders.ShareType.FOLDER, lstShares.Items(x).Value, lstShares.Items(x).Text)
            '        lstShares.Items.Remove(lstShares.Items(x))
            '    End If
            'Next

        Catch ex As Exception

        Finally

            'ReturnControls.Add(lEmail)
            'ReturnControls.Add(lstShares)

            If conSql.State <> ConnectionState.Closed Then
                conSql.Close()
                conSql.Dispose()
            End If

        End Try

    End Sub

End Class

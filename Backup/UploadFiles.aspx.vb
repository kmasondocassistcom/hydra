Imports Telerik.WebControls
Imports Telerik.WebControls.RadUploadUtils
Imports Telerik.Web.UI
Imports Accucentric.docAssist
Imports Accucentric.docAssist.Data.Images

Public Class UploadFiles
    Inherits System.Web.UI.Page

    Private mconSqlImage As New SqlClient.SqlConnection
    Private mobjUser As Accucentric.docAssist.Web.Security.User
    Private mcookieSearch As cookieSearch

    Protected WithEvents lblFileName As System.Web.UI.WebControls.Label
    Protected WithEvents lblTitle As System.Web.UI.WebControls.Label
    Protected WithEvents lblCurrentVersion As System.Web.UI.WebControls.Label

    'Protected WithEvents txtTitle As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtComments As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtDescription As System.Web.UI.WebControls.TextBox
    'Protected WithEvents btnCancel As System.Web.UI.WebControls.ImageButton
    Protected WithEvents radProgress As Telerik.Web.UI.RadProgressArea
    Protected WithEvents radProgressManager As Telerik.Web.UI.RadProgressManager
    'Protected WithEvents txtTags As System.Web.UI.WebControls.TextBox
    'Protected WithEvents btnSave As System.Web.UI.WebControls.ImageButton
    'Protected WithEvents lblError As System.Web.UI.WebControls.Label
    'Protected WithEvents chkAdvanced As System.Web.UI.WebControls.CheckBox
    'Protected WithEvents litRedirect As System.Web.UI.WebControls.Literal
    'Protected WithEvents FolderPath As System.Web.UI.HtmlControls.HtmlGenericControl
    'Protected WithEvents litEnable As System.Web.UI.WebControls.Literal
    'Protected WithEvents SetCancel As System.Web.UI.WebControls.ImageButton
    'Protected WithEvents Label7 As System.Web.UI.WebControls.Label
    'Protected WithEvents tblFolder As System.Web.UI.HtmlControls.HtmlTable
    'Protected WithEvents ChooseFolder As System.Web.UI.HtmlControls.HtmlImage
    'Protected WithEvents ClearFolder As System.Web.UI.HtmlControls.HtmlImage
    'Protected WithEvents FolderId As System.Web.UI.HtmlControls.HtmlInputHidden
    'Protected WithEvents NoFolderAccess As System.Web.UI.HtmlControls.HtmlInputHidden
    'Protected WithEvents LinkButton1 As System.Web.UI.WebControls.LinkButton
    'Protected WithEvents btnUpdateSession As System.Web.UI.WebControls.ImageButton

    Dim intFolderId As Integer

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load, Me.Load

        Response.Expires = 0
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        Dim jscript As String = "<script language='javascript'>function UploadComplete(){"
        jscript += String.Format("__doPostBack('{0}','');", LinkButton1.ClientID.Replace("_", "$"))
        jscript += "};</script>"
        RegisterClientScriptBlock("FileCompleteUpload", jscript)

        'RegisterExcludeControl(btnSave)

        intFolderId = CInt(Request.QueryString("FolderId").Replace("W", ""))

        'btnSave.Attributes.Add("onclick", "try{ javascript:if (checkRights() == true){ window.parent.uploadOn();document.all.AdvancedCheck.style.display='none';document.all.UploadBoxes.style.display='none';document.all.RadProgressManager.startProgressPolling();} } catch(ex){}")
        btnSave.Attributes.Add("onclick", "javascript:if (checkRights() == true){startPoll();}")

        Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
        Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))
        Me.mobjUser.AccountId = guidAccountId
        Me.mcookieSearch = New cookieSearch(Me)

        Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)

        If Me.mobjUser.EditionID = AccountEdition.SMALL_BUSINESS_EDITION Then
            chkAdvanced.Text = "Assign attributes after uploading"
        End If

        If Not Page.IsPostBack Then

            Response.Cookies("docAssistUpload")("BulkFolderId") = "0"

            Try


                If Functions.ModuleSetting(Functions.ModuleCode.DOCUMENT_TYPES_ATTRIBUTES, Me.mobjUser) Then
                    chkAdvanced.Visible = True
                Else
                    chkAdvanced.Checked = False
                    chkAdvanced.Visible = False
                End If

                If intFolderId <> 0 Then
                    Dim sqlCmd As New SqlClient.SqlCommand("FAFolderPathByFolderId", Me.mconSqlImage)
                    sqlCmd.CommandType = CommandType.StoredProcedure
                    sqlCmd.Parameters.Add("@FolderId", intFolderId)
                    Me.mconSqlImage.Open()
                    FolderPath.InnerHtml = sqlCmd.ExecuteScalar
                    FolderId.Value = intFolderId
                    Response.Cookies("docAssistUpload")("BulkFolderId") = intFolderId.ToString
                End If

                If FolderId.Value.ToString.Trim = "" Or FolderId.Value.ToString.Trim = "0" Then

                    'Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
                    Dim id As String = Request.QueryString("NodeId")

                    Dim objFolders As New Folders(Functions.BuildConnection(Me.mobjUser))
                    Dim dt As New DataTable
                    dt = objFolders.FolderCabinetPermissions(Me.mobjUser.ImagesUserId, 0)

                    If dt.Rows(0)("CanAddFilesToFolder") = "0" Then
                        NoFolderAccess.Value = 0
                    Else
                        NoFolderAccess.Value = 1
                    End If

                End If

            Catch ex As Exception

#If DEBUG Then
                lblError.Text = ex.Message
#Else
                lblError.Text = "An error has occured."
#End If

            Finally

                litEnable.Text = "<script language='javascript'>window.parent.uploadingInProgress = false;</script>"

                'ReturnControls.Add(lblError)
                'ReturnControls.Add(FolderPath)
                'ReturnControls.Add(chkAdvanced)

                If Me.mconSqlImage.State <> ConnectionState.Closed Then
                    Me.mconSqlImage.Close()
                    Me.mconSqlImage.Dispose()
                End If

            End Try

        End If

    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click

        Dim trnSql As SqlClient.SqlTransaction

        Try

            Dim uploadContext As RadUploadContext
            uploadContext = RadUploadContext.Current

            Dim uploadedFiles As UploadedFileCollection
            uploadedFiles = uploadContext.UploadedFiles

            If uploadedFiles.Count > 0 Then

                If FolderId.Value.Trim <> "" Then intFolderId = FolderId.Value

                If Session("Cancel") = "1" Then
                    Session("Cancel") = "0"
                    Exit Sub
                End If

                If chkAdvanced.Checked Then
                    'Setup advanced mode

                    Dim dt As New DataTable
                    dt.Columns.Add(New DataColumn("FolderId", GetType(System.Int32), Nothing, MappingType.Element))
                    dt.Columns.Add(New DataColumn("Title", GetType(System.String), Nothing, MappingType.Element))
                    dt.Columns.Add(New DataColumn("Description", GetType(System.String), Nothing, MappingType.Element))
                    dt.Columns.Add(New DataColumn("Comments", GetType(System.String), Nothing, MappingType.Element))
                    dt.Columns.Add(New DataColumn("Tags", GetType(System.String), Nothing, MappingType.Element))
                    dt.Columns.Add(New DataColumn("Filename", GetType(System.String), Nothing, MappingType.Element))
                    dt.Columns.Add(New DataColumn("GuidName", GetType(System.String), Nothing, MappingType.Element))

                    Dim currentFileID As Integer = -1
                    Dim intGoodSaves As Integer = 0

                    For X As Integer = 1 To uploadedFiles.Count

                        Dim dr As DataRow
                        dr = dt.NewRow

                        Dim filenm As String = ""
                        Dim desc As String = ""
                        Dim title As String = ""
                        Dim comments As String = ""
                        Dim tags As String = ""

                        Do While (currentFileID < 1000 And IsNothing(uploadedFiles(filenm)))
                            currentFileID += 1
                            filenm = String.Format("txtFilename{0}", IIf(currentFileID = 0, "", currentFileID.ToString))
                            desc = String.Format("txtDescription{0}", IIf(currentFileID = 0, "", currentFileID.ToString))
                            title = String.Format("txtTitle{0}", IIf(currentFileID = 0, "", currentFileID.ToString))
                            comments = String.Format("txtComments{0}", IIf(currentFileID = 0, "", currentFileID.ToString))
                            tags = String.Format("txtTags{0}", IIf(currentFileID = 0, "", currentFileID.ToString))
                        Loop

                        Dim file As UploadedFile = uploadedFiles(filenm)

                        If (file.FileName.Length > 0) And (file.ContentLength > 0) Then

                            Dim descsuffix As String = desc.Substring("txtDescription".Length)
                            Dim titlesuffix As String = title.Substring("txtTitle".Length)
                            Dim commentsuffix As String = comments.Substring("txtComments".Length)
                            Dim tagssuffix As String = tags.Substring("txtTags".Length)

                            Dim filename As String = System.IO.Path.GetFileName(file.FileName)
                            Dim bytes As Integer = file.ContentLength
                            Dim description As String = Request.Form(String.Concat("txtDescription", descsuffix)).Trim()
                            Dim titlestring As String = Request.Form(String.Concat("txtTitle", titlesuffix)).Trim()
                            Dim comment As String = Request.Form(String.Concat("txtComments", commentsuffix)).Trim()
                            Dim tag As String = Request.Form(String.Concat("txtTags", tagssuffix)).Trim()

                            Dim guidName As String = ConfigurationSettings.AppSettings("UploadPath") & System.Guid.NewGuid.ToString & System.IO.Path.GetExtension(filename)

                            Dim strSaveFile As String = ConfigurationSettings.AppSettings("UploadPath") & filename
                            file.SaveAs(guidName)

                            dr("FolderId") = intFolderId
                            dr("Title") = titlestring
                            dr("Description") = description
                            dr("Comments") = comment
                            dr("Tags") = tag
                            dr("Filename") = filename
                            dr("GuidName") = guidName

                            dt.Rows.Add(dr)

                        End If

                    Next

                    'Send to advanced page
                    Dim ds As New DataSet
                    ds.Tables.Add(dt)

                    Session("AdvancedUpload") = ds

                    'Server.Transfer("Upload.aspx?FolderId" & intFolderId.ToString)
                    'litRedirect.Text = "<script language='javascript'>window.parent.Content.src='Upload.aspx?FolderId=" & intFolderId.ToString & ";</script>"
                    litRedirect.Text = "<script language='javascript'>window.parent.loadAdvanced(" & intFolderId.ToString & ");</script>"
                    'ReturnScripts.Add("window.parent.loadAdvanced(" & intFolderId.ToString & ");")

                Else

                    'Upload all the files now.
                    Dim currentFileID As Integer = -1
                    Dim intGoodSaves As Integer = 0

                    For X As Integer = 1 To uploadedFiles.Count

                        Dim filenm As String = ""
                        Dim desc As String = ""
                        Dim title As String = ""
                        Dim comments As String = ""
                        Dim tags As String = ""

                        Do While (currentFileID < 1000 And IsNothing(uploadedFiles(filenm)))
                            currentFileID += 1
                            filenm = String.Format("txtFilename{0}", IIf(currentFileID = 0, "", currentFileID.ToString))
                            desc = String.Format("txtDescription{0}", IIf(currentFileID = 0, "", currentFileID.ToString))
                            title = String.Format("txtTitle{0}", IIf(currentFileID = 0, "", currentFileID.ToString))
                            comments = String.Format("txtComments{0}", IIf(currentFileID = 0, "", currentFileID.ToString))
                            tags = String.Format("txtTags{0}", IIf(currentFileID = 0, "", currentFileID.ToString))
                        Loop

                        Dim file As UploadedFile = uploadedFiles(filenm)

                        If (file.FileName.Length > 0) And (file.ContentLength > 0) Then

                            Dim descsuffix As String = desc.Substring("txtDescription".Length)
                            Dim titlesuffix As String = title.Substring("txtTitle".Length)
                            Dim commentsuffix As String = comments.Substring("txtComments".Length)
                            Dim tagssuffix As String = tags.Substring("txtTags".Length)

                            Dim filename As String = System.IO.Path.GetFileName(file.FileName)
                            Dim bytes As Integer = file.ContentLength
                            Dim description As String = Request.Form(String.Concat("txtDescription", descsuffix)).Trim()
                            Dim titlestring As String = Request.Form(String.Concat("txtTitle", titlesuffix)).Trim()
                            Dim comment As String = Request.Form(String.Concat("txtComments", commentsuffix)).Trim()
                            Dim tag As String = Request.Form(String.Concat("txtTags", tagssuffix)).Trim()

                            Dim strSaveFile As String = ConfigurationSettings.AppSettings("UploadPath") & filename
                            file.SaveAs(strSaveFile)

                            Dim byteArray As Byte() = GetDataAsByteArray(strSaveFile)
                            Dim intNewVersionId As Integer = 0

                            Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
                            Me.mconSqlImage.Open()

                            trnSql = Me.mconSqlImage.BeginTransaction

                            Dim objAttachments As New FileAttachments(Me.mconSqlImage, False, trnSql)

                            Dim intAttachmentId As Integer = objAttachments.InsertFolderAttachment(intFolderId, 0, Me.mobjUser.ImagesUserId, titlestring, description, comment, tag, bytes, filename, byteArray, intNewVersionId)

                            trnSql.Commit()

                            intGoodSaves += 1

                            'Track attachment
                            Try
                                Accucentric.docAssist.Web.Data.Functions.TrackPageRequest(Me.mobjUser.UserId, Me.mobjUser.AccountId, "Attachment Add", "In", intNewVersionId, intAttachmentId.ToString, Functions.GetMasterConnectionString)
                            Catch ex As Exception

                            End Try

                            'Log recent item
                            Try
                                Dim blnReturn As Boolean = Functions.TrackRecentItem(Me.mobjUser, "SV", intNewVersionId)
                                If Not blnReturn Then
                                    'error logging recent item
                                End If
                            Catch ex As Exception
                            End Try

                        End If

                    Next

                    Dim strConfirm As String = intGoodSaves.ToString & IIf(intgoodsaves > 1, " files saved successfully.", " file saved successfully.")

                    If intFolderId = 0 Then
                        System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, "saveComplete", "window.parent.uploadOff();parent.window.updateRecent();parent.window.landMode();", True)
                    Else
                        'System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, "saveComplete", "window.parent.uploadOff();window.parent.browseFolder(" & intFolderId.ToString & ");parent.window.updateRecent();", True)
                        System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, "saveComplete", "window.parent.uploadOff();window.parent.browseFolder(" & intFolderId.ToString & ");", True)
                    End If

                End If

            End If

        Catch ex As Exception

            trnSql.Rollback()

            Dim conMaster As SqlClient.SqlConnection
            conMaster = Functions.BuildMasterConnection
            Functions.LogException(Me.mobjUser.UserId.ToString, Me.mobjUser.AccountId.ToString, "", "Single File Upload Error: " & ex.Message, ex.StackTrace, HttpContext.Current.Server.MachineName, "", HttpContext.Current.Request.UrlReferrer.ToString, conMaster, True)

#If DEBUG Then
            lblError.Text = ex.Message
#Else
            lblError.Text = "An error has occured."
            'lblError.Text = ex.Message & ex.StackTrace
#End If
            'Show browse boxes again if error occurs
            litRedirect.Text = "<script language='javascript'>document.all.AdvancedCheck.style.display='block';document.all.UploadBoxes.style.display='block';</script>"

        Finally

            litEnable.Text = "<script language='javascript'>window.parent.uploadingInProgress = false;window.parent.enableTree();ModeClick.disabled = false;</script>"

            If Me.mconSqlImage.State <> ConnectionState.Closed Then
                Me.mconSqlImage.Close()
                Me.mconSqlImage.Dispose()
            End If

        End Try

    End Sub

    Private Sub SetCancel_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles SetCancel.Click
        Session("Cancel") = "1"
    End Sub

    Protected Function GetFlashVars() As String
        ' Adds query string info to the upload page 
        ' you can also do something like: 
        ' return "?" + Server.UrlEncode("CategoryID="+CategoryID); 
        ' we UrlEncode it because of how LoadVars works with flash, 
        ' we want a string to show up like this 'CategoryID=3&UserID=4' in 
        ' the uploadPage variable in flash. If we passed this string withou 
        ' UrlEncode then flash would take UserID as a seperate LoadVar variable 
        ' instead of passing it into the uploadPage variable. 
        ' then in the httpHandler we get the CategoryID and UserID values from 
        ' the query string. See Upload.cs in App_Code 
        Return "?" + Server.UrlEncode(Request.QueryString.ToString())
    End Function

    Protected Sub LinkButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LinkButton1.Click
        ' Do something that needs to be done such as refresh a gridView 
        ' say you had a gridView control called gvMyGrid displaying all 
        ' the files uploaded. Refresh the data by doing a databind here. 
        ' gvMyGrid.DataBind(); 
    End Sub

    Private Sub btnUpdateSession_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnUpdateSession.Click
        Response.Cookies("docAssistUpload")("BulkFolderId") = FolderId.Value.ToString
    End Sub

End Class
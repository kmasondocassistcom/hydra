<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="Controls/MenuBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="docHeader" Src="Controls/docHeader.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="InstallFramework.aspx.vb" Inherits="docAssistWeb.InstallFramework" EnableSessionState="ReadOnly" enableViewState="True" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>docAssist - Update Components</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<TABLE height="100%" cellSpacing="0" cellPadding="0" border="0">
				<TR>
					<uc1:docHeader id="docHeader" runat="server"></uc1:docHeader>
				</TR>
				<TR>
					<TD width="50%"></TD>
					<TD align="left">
						<span id="InstallContent" runat="server">
							<P><font style="FONT-SIZE: 8pt; FONT-FAMILY: verdana"><FONT style="FONT-SIZE: 8pt; FONT-FAMILY: verdana"><STRONG>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The 
											Microsoft .NET Framework was not detected on this computer. Please follow the 
											instructions below to install.</STRONG>&nbsp;<BR>
										&nbsp;&nbsp;<BR>
										</STRONG>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Installation instructions:
										<BR>
										<BR>
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1. 
										Click the link below to download the&nbsp;Microsoft .NET 1.1 Framework. You 
										will be prompted with a file download dialog.
										<BR>
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
										Select&nbsp;'Run' or 'Open'.<BR>
										<BR>
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<FONT style="FONT-SIZE: 8pt; FONT-FAMILY: verdana"><A href="Downloads/dotnetfx.exe">Download 
												Microsoft .NET Framework Version 1.1</A>&nbsp;</FONT><BR>
										<BR>
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
										[Screenshot for Run/Open]<BR>
										<BR>
									</FONT>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
									2. Follow the on screen instructions provided by the installer.<BR>
								</font><FONT style="FONT-SIZE: 8pt; FONT-FAMILY: verdana">
									<BR>
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
									[Screenshots?]&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<BR>
								</FONT><FONT style="FONT-SIZE: 8pt; FONT-FAMILY: verdana">
									<BR>
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
									3.&nbsp;When you are finished click <a href="DocumentManager.aspx">here</a> to 
									load the docAssist Indexer.<BR>
									<BR>
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									<BR>
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; If you 
									encounter any issues installing these componenents, please contact technical 
									support by following the support<BR>
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; links 
									on the website.<BR>
						</span>
						<asp:ImageButton id="btnSignOut" runat="server" Height="0px" Width="0px"></asp:ImageButton>
						<IMG height="1" src="spacer.gif" width="800"></FONT> </P>
						<P></P>
					</TD>
					<TD align="right" width="50%"></TD>
				</TR>
				<TR>
					<TD>&nbsp;</TD>
					<TD>&nbsp;</TD>
					<TD>&nbsp;</TD>
				</TR>
			</TABLE>
		</form>
		<SCRIPT language="vbscript">

		</SCRIPT>
		<script language="javascript">

			document.all.docHeader_imgLauncher.src = "./Images/manager_grey.gif";
			document.all.managerLink.href = "#";

			setTimeout("Timeout()",3600000);
			function Timeout()
			{alert("Your docAssist session has timed out.\n\nAs a security precaution, sessions are ended after 60 minutes of inactivity.\n\nYou can sign in again to resume your session.");
			document.all.btnSignOut.click();}
		</script>
	</body>
</HTML>

<%@ Register TagPrefix="uc1" TagName="AdminNav" Src="Controls/AdminNav.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="Synchs.aspx.vb" Inherits="docAssistWeb.Synchs"%>
<%@ Register TagPrefix="uc1" TagName="docHeader" Src="Controls/docHeader.ascx" %>
<%@ Register TagPrefix="uc1" TagName="TopRight" Src="Controls/TopRight.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>docAssist - Data Synchronization</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body class="Prefs" bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0" MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<TABLE height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TBODY>
					<TR>
						<TD width="100%" colSpan="3"><uc1:docheader id="docHeader" runat="server"></uc1:docheader></TD>
					</TR>
					<TR vAlign="middle" height="100%">
						<TD class="Navigation1" style="BORDER-RIGHT: #cccccc thin solid" vAlign="top" height="100%"><uc1:adminnav id="AdminNav2" runat="server"></uc1:adminnav><IMG height="1" src="dummy.gif" width="120"></TD>
						<TD vAlign="top" noWrap align="left" height="100%">&nbsp;
							<TABLE id="Table1" cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
								<TR>
									<TD width="20" rowSpan="6"></TD>
									<TD class="PrefHeader">Data Synchronization</TD>
								</TR>
								<TR>
									<TD align="left"><IMG height="1" src="dummy.gif" width="17">
										<asp:LinkButton id="lnkAdd" runat="server" Font-Size="8pt" Font-Names="Trebuchet MS" ForeColor="#BD0000">Add Synchronization</asp:LinkButton><IMG height="5" src="dummy.gif" width="1">
									</TD>
								</TR>
								<TR>
									<TD>
										<TABLE class="Prefs" id="Table2" cellSpacing="0" cellPadding="0" width="100%" align="center"
											border="0">
											<TR>
												<TD width="20"><IMG height="1" src="dummy.gif" width="20"></TD>
												<TD class="Text" noWrap align="left" width="100%"><SPAN class="Text" id="NoRows" runat="server"></SPAN>
													<asp:datagrid id="dgSynch" runat="server" Width="100%" AutoGenerateColumns="False" Font-Size="9pt"
														Font-Names="Trebuchet MS">
														<AlternatingItemStyle BackColor="WhiteSmoke"></AlternatingItemStyle>
														<HeaderStyle Font-Size="10pt" Font-Bold="True" BackColor="Gainsboro"></HeaderStyle>
														<Columns>
															<asp:TemplateColumn>
																<HeaderStyle Width="30px"></HeaderStyle>
																<ItemStyle Wrap="False"></ItemStyle>
																<ItemTemplate>
																	<asp:ImageButton id="btnDelete" runat="server" ImageUrl="Images/trash.gif" CommandName="Del"></asp:ImageButton>
																	<asp:Label id="JobId" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SubscriptionID") %>' Visible="False">
																	</asp:Label>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:BoundColumn DataField="Enabled" HeaderText="Enabled">
																<HeaderStyle Wrap="False"></HeaderStyle>
																<ItemStyle Wrap="False"></ItemStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="JobName" HeaderText="Job Name">
																<HeaderStyle Wrap="False"></HeaderStyle>
																<ItemStyle Wrap="False"></ItemStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="Details" HeaderText="Time">
																<HeaderStyle Wrap="False"></HeaderStyle>
																<ItemStyle Wrap="False"></ItemStyle>
															</asp:BoundColumn>
														</Columns>
													</asp:datagrid></TD>
											</TR>
										</TABLE>
									</TD>
								</TR>
							</TABLE>
						</TD>
						<TD height="100%">&nbsp;</TD>
					</TR>
					<TR>
						<TD class="PageFooter" vAlign="bottom" bgColor="#cccccc" colSpan="3"><uc1:topright id="docFooter" runat="server"></uc1:topright></TD>
					</TR>
					<TR height="1">
						<TD style="BACKGROUND-COLOR: #959394" width="20"><IMG height="1" src="images/spacer.gif" width="20"></TD>
						<TD style="BACKGROUND-COLOR: #959394" width="100%"><IMG height="1" src="images/spacer.gif" width="100%"></TD>
						<TD style="BACKGROUND-COLOR: #959394" width="20"><IMG height="1" src="images/spacer.gif" width="20"></TD>
					</TR>
				</TBODY>
			</TABLE>
			<input id="ODBC" type="hidden">
			<script language="javascript">
			
			function browseODBC()
			{
				var ODBC = document.getElementById('ODBC');
				var val = window.showModalDialog('ODBCSources.aspx','ODBCSources','resizable:no;status:no;dialogHeight:180px;dialogWidth:330px;scroll:no');
				if (val != undefined)
				{
					varArray = val.split('�');
					document.getElementById('ODBCID').value = varArray[0];
					document.getElementById('ODBCSource').innerHTML = varArray[1];
				}
			}
			
			function addODBCSource()
			{
				var ODBC = document.getElementById('ODBC');
				var val = window.showModalDialog('CreateODBCSource.aspx','CreateODBCSource','resizable:no;status:no;dialogHeight:200px;dialogWidth:350px;scroll:no');
				if (val != undefined)
				{
					varArray = val.split('�');
					document.getElementById('ODBCID').value = varArray[0];
					document.getElementById('ODBCSource').innerHTML = varArray[1];
				}
			}
			
			function chooseCabinet()
			{
				var val = window.showModalDialog('Cabinet.aspx?','FolderLookup','resizable:no;status:no;dialogHeight:625px;dialogWidth:250px;scroll:no');
				if (val != undefined)
				{
					varArray = val.split('�');
					document.getElementById('CabinetFilter').innerHTML = varArray[1];
					document.getElementById('CabinetID').value = varArray[0];
				}
			}
			
			</script>
		</form>
	</body>
</HTML>

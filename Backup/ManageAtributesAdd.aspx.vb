Partial Class ManageAtributesAdd
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private mobjUser As Accucentric.docAssist.Web.Security.User
    Private mconSqlImage As SqlClient.SqlConnection

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'Put user code to initialize the page here
        txtLength.Attributes.Add("onkeypress", "catchSubmit();return nb_numericOnly(event)")
        txtAttributeName.Attributes("onkeypress") = "catchSubmit();"
        txtFormat.Attributes("onkeypress") = "catchSubmit();"

        If Not Page.IsPostBack Then

            Dim lstItem As New ListItem
            lstItem.Text = "AlphaNumeric"
            lstItem.Value = "String"
            cmbDataType.Items.Add(lstItem)
            cmbDataType.Items.Add("Numeric")
            cmbDataType.Items.Add("Date")

            'lstItem = New ListItem
            'lstItem.Text = "List"
            'lstItem.Value = "ListView"
            'cmbDataType.Items.Add(lstItem)

            txtAttributeName.MaxLength = 40

        End If

    End Sub

    Private Sub lnkFinish_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        If txtAttributeName.Text = "" Then
            lblError.Text = "Attribute name is required."
            Exit Sub
        End If

        If AttributeNameExists() Then
            lblError.Text = "Attribute name already exists."
            Exit Sub
        Else

            If cmbDataType.SelectedValue = "List" Then

                'Response.Cookies("Admin")("AttributeName") = txtAttributeName.Text
                'Response.Cookies("Admin")("AttributeLength") = txtLength.Text
                'Response.Cookies("Admin")("AttributeFormat") = txtFormat.Text
                'Response.Redirect("ManageAttributesAddList.aspx")

              

            Else
                SaveAttribute()
                Response.Redirect("ManageAttributes.aspx")
            End If

        End If

    End Sub

    Private Function AttributeNameExists() As Boolean
        Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
        Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))

        Me.mobjUser.AccountId = guidAccountId
        Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
        Me.mconSqlImage.Open()

        Dim cmdSql As SqlClient.SqlCommand

        Dim dtsql As New DataTable
        cmdSql = New SqlClient.SqlCommand("acsAdminAttributeCheck", Me.mconSqlImage)
        cmdSql.CommandType = CommandType.StoredProcedure
        cmdSql.Parameters.Add("@AttributeName", txtAttributeName.Text)
        Dim dasql As New SqlClient.SqlDataAdapter(cmdSql)
        dasql.Fill(dtsql)


        If dtsql.Rows.Count > 0 Then
            Return True
        Else
            Return False

        End If

    End Function

    Private Function SaveAttribute() As Boolean

        Dim cmdSql As SqlClient.SqlCommand
        Dim dtsql As New DataTable
        cmdSql = New SqlClient.SqlCommand("acsAdminAttributeAdd", Me.mconSqlImage)
        cmdSql.CommandType = CommandType.StoredProcedure
        cmdSql.Parameters.Add("@AttributeName", txtAttributeName.Text)
        cmdSql.Parameters.Add("@AttributeDataType", cmbDataType.SelectedValue)
        cmdSql.Parameters.Add("@DataFormat", txtFormat.Text)
        cmdSql.Parameters.Add("@Length", txtLength.Text)
        cmdSql.Parameters.Add("@AttributeID", SqlDbType.Int)
        cmdSql.Parameters("@AttributeID").Direction = ParameterDirection.Output
        cmdSql.ExecuteNonQuery()

    End Function

    Private Sub cmbDataType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbDataType.SelectedIndexChanged

        If cmbDataType.SelectedItem.Text = "List" Or cmbDataType.SelectedItem.Text = "Date" Then
            btnFinish.ImageUrl = "Images/btn_nextadmin.gif"
            txtLength.Visible = False
            txtFormat.Visible = False
            Label3.Visible = False
            Label4.Visible = False
            txtLength.Text = ""
            txtFormat.Text = ""

            If cmbDataType.SelectedItem.Text = "List" Then
                rdoMultiple.Visible = True
                rdoSingle.Visible = True
            Else
                rdoMultiple.Visible = False
                rdoSingle.Visible = False
            End If

        Else
            btnFinish.ImageUrl = "Images/btn_finish.gif"
            txtLength.Visible = True
            txtFormat.Visible = True
            Label3.Visible = True
            Label4.Visible = True
            rdoMultiple.Visible = False
            rdoSingle.Visible = False
        End If

        rdoMultiple.Visible = False
        rdoSingle.Visible = False

    End Sub

    Private Sub btnFinish_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFinish.Click

        If txtAttributeName.Text = "" Then
            lblError.Text = "Please enter an attribute name."
            Exit Sub
        End If

        If AttributeNameExists() Then
            lblError.Text = "The specified attribute name is already in use. Please select another name or edit the current one."
            Exit Sub
        End If

        Select Case cmbDataType.SelectedItem.Text
            Case "Date"

            Case "List"
                'Session("mAdminAttributeName") = txtAttributeName.Text
                'Session("mAdminAttributeLength") = txtLength.Text
                'Session("mAdminAttributeFormat") = ""
                'Response.Redirect("ManageAttributesAddList.aspx")

                Dim cmdSql As SqlClient.SqlCommand
                Dim dtsql As New DataTable
                cmdSql = New SqlClient.SqlCommand("acsAdminAttributeAdd", Me.mconSqlImage)
                cmdSql.CommandType = CommandType.StoredProcedure
                cmdSql.Parameters.Add("@AttributeName", txtAttributeName.Text)
                cmdSql.Parameters.Add("@AttributeDataType", "ListView")
                cmdSql.Parameters.Add("@DataFormat", "")
                cmdSql.Parameters.Add("@Length", "")
                cmdSql.Parameters.Add("@AttributeID", SqlDbType.Int)

                If rdoSingle.Checked Then
                    cmdSql.Parameters.Add("@NumCols", 1)
                Else
                    cmdSql.Parameters.Add("@NumCols", 2)
                End If

                cmdSql.Parameters("@AttributeID").Direction = ParameterDirection.Output
                cmdSql.ExecuteNonQuery()

                Dim iNewAttributeID As Integer
                iNewAttributeID = cmdSql.Parameters("@AttributeID").Value
                Server.Transfer("ManageAttributes.aspx")
                Exit Sub

            Case Else
                If IsNumeric(txtLength.Text) = False Then
                    lblError.Text = "Invalid length. Please enter a number between 1 and 75."
                    Exit Sub
                End If

                If txtLength.Text < 1 Or txtLength.Text > 75 Then
                    lblError.Text = "Invalid length. Please enter a number between 1 and 75."
                    Exit Sub
                End If
        End Select

        SaveAttribute()

        Server.Transfer("ManageAttributes.aspx")

    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("ManageAttributes.aspx")
    End Sub

End Class

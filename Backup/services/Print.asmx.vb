Imports System.Web.Services

<System.Web.Services.WebService(Namespace:="http://tempuri.org/docAssistWeb/PrintService")> _
Public Class PrintService
    Inherits System.Web.Services.WebService

#Region " Web Services Designer Generated Code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Web Services Designer.
        InitializeComponent()

        'Add your own initialization code after the InitializeComponent() call

    End Sub

    'Required by the Web Services Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Web Services Designer
    'It can be modified using the Web Services Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        components = New System.ComponentModel.Container
    End Sub

    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        'CODEGEN: This procedure is required by the Web Services Designer
        'Do not modify it using the code editor.
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

#End Region

    Private conMaster As New SqlClient.SqlConnection

    <WebMethod()> _
    Public Function PrintAuthentication(ByVal emailAddress As String, ByVal password As String) As DataSet

        Try

            conMaster = Functions.BuildMasterConnection()

            Dim cmdSql As New SqlClient.SqlCommand("SMPrintAuthentication", conMaster)
            cmdSql.CommandType = CommandType.StoredProcedure
            cmdSql.Parameters.Add("@EmailAddress", Encryption.Encryption.Decrypt(emailAddress))
            cmdSql.Parameters.Add("@Password", password)

            Dim da As New SqlClient.SqlDataAdapter(cmdSql)

            Dim loginInformation As New DataSet("AuthenticationInfo")
            da.Fill(loginInformation)

            Return loginInformation

        Catch ex As Exception

            Throw ex

        Finally

            If conMaster.State <> ConnectionState.Closed Then
                conMaster.Close()
                conMaster.Dispose()
            End If

        End Try

    End Function

End Class

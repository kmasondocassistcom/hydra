<%@ Register TagPrefix="uc1" TagName="TopRight" Src="Controls/TopRight.ascx" %>
<%@ Register TagPrefix="uc1" TagName="docHeader" Src="Controls/docHeader.ascx" %>
<%@ Register TagPrefix="uc1" TagName="AdminNav" Src="Controls/AdminNav.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ManageAttributesChangeList.aspx.vb" Inherits="docAssistWeb.ManageAttributesChangeList"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>docAssist - Manage Attributes</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<TABLE height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TBODY>
					<TR>
						<TD width="100%" colSpan="5">
							<uc1:docHeader id="docHeader" runat="server"></uc1:docHeader>
						</TD>
					</TR>
					<TR vAlign="middle" height="100%">
						<TD class="Navigation1" height="100%" vAlign="top">
							<uc1:AdminNav id="AdminNav2" runat="server"></uc1:AdminNav><IMG height="1" src="dummy.gif" width="120"></TD>
						<TD vAlign="top" align="center" height="100%"><BR>
							<table cellSpacing="0" cellPadding="0" width="100%" border="0">
								<tr>
									<td><IMG height="1" src="dummy.gif" width="35"><IMG src="Images/manage_attributes.gif"></td>
								</tr>
							</table>
							<table height="100%" cellSpacing="0" cellPadding="0" width="100%">
					</TR>
					<tr height="50%">
					</tr>
					<tr height="15" width="100%">
						<td style="HEIGHT: 17px"></td>
						<td style="HEIGHT: 17px"></td>
						<td style="WIDTH: 456px; HEIGHT: 17px">
							<asp:Label id="lblAttributeName" runat="server" Width="480px" Height="18px" Font-Size="8.25pt"
								Font-Names="Verdana" Font-Bold="True"></asp:Label></td>
						<td style="HEIGHT: 17px"></td>
					</tr>
					<tr height="15" width="100%">
						<td style="HEIGHT: 26px"></td>
						<td style="WIDTH: 72px; HEIGHT: 26px" align="left"></td>
						<TD style="WIDTH: 456px; HEIGHT: 26px">
							<P>
								<asp:TextBox id="txtListAdd" runat="server" Width="270px" Height="23px" Font-Size="8.25pt" Font-Names="Verdana"></asp:TextBox>&nbsp;
								<asp:ImageButton id="btnAdd" runat="server" ImageUrl="Images/add_item.gif"></asp:ImageButton></P>
						</TD>
						<TD width="50%" style="HEIGHT: 26px"></TD>
					<TR height="15" width="100%">
						<td style="HEIGHT: 21px" width="50%"></td>
						<td style="WIDTH: 72px; HEIGHT: 21px" align="left">&nbsp;
						</td>
						<td style="WIDTH: 456px; HEIGHT: 21px">&nbsp;</td>
						<td style="HEIGHT: 21px" width="50%"></td>
					</TR>
					<tr height="15" width="100%">
						<td style="WIDTH: 393px" width="393"></td>
						<td style="WIDTH: 72px; HEIGHT: 17px">
							<P align="left">&nbsp;&nbsp;
							</P>
						</td>
						<td style="WIDTH: 456px; HEIGHT: 17px" vAlign="top">
							<asp:ListBox id="lstItems" runat="server" Width="440px" Height="200px" Font-Size="8.25pt" Font-Names="Verdana"></asp:ListBox></td>
						<td width="50%">
							<asp:ImageButton id="btnMoveUp" runat="server" ImageUrl="Images/move_up.gif"></asp:ImageButton><BR>
							<BR>
							<asp:ImageButton id="btnRemove" runat="server" ImageUrl="Images/remove_item.gif"></asp:ImageButton><BR>
							<BR>
							<asp:ImageButton id="btnMoveDown" runat="server" ImageUrl="Images/move_down.gif"></asp:ImageButton></td>
					</tr>
					<tr height="15" width="100%">
						<td style="WIDTH: 393px" width="393"></td>
						<td style="WIDTH: 72px" vAlign="middle">
							<P align="left">&nbsp;&nbsp;</P>
						</td>
						<td align="left" style="WIDTH: 456px">
							<asp:Label id="lblError" runat="server" Width="100%" Height="17px" ForeColor="Red" Font-Size="8.25pt"
								Font-Names="Verdana"></asp:Label></td>
						<td width="50%"></td>
					</tr>
					<tr height="15" width="100%">
						<td style="WIDTH: 393px" width="395"></td>
						<td style="WIDTH: 72px; HEIGHT: 17px"><BR>
						</td>
						<td style="WIDTH: 456px; HEIGHT: 17px" align="right"><asp:imagebutton id="btnCancel" runat="server" ImageUrl="Images/btn_canceladmin.gif"></asp:imagebutton>&nbsp;<asp:imagebutton id="btnFinish" runat="server" ImageUrl="Images/btn_finish.gif"></asp:imagebutton></td>
						<td width="50%"></td>
					</tr>
					<tr height="15" width="100%">
						<td style="WIDTH: 395px" width="395"></td>
						<td style="WIDTH: 72px"><BR>
							<P>&nbsp;</P>
						</td>
						<td align="right" style="WIDTH: 456px"></td>
						<td width="50"></td>
					</tr>
					<tr height="100%">
						<td style="BORDER-RIGHT: #cccccc thin solid"></td>
					</tr>
				</tr>
			</TABLE>
			<P><BR>
				<BR>
			</P>
			</TD>
			<TD class="RightContent" height="100%">&nbsp;</TD>
			</TR>
			<TR>
				<TD class="PageFooter" vAlign="bottom" bgColor="#cccccc" colSpan="3">
					<uc1:TopRight id="docFooter" runat="server"></uc1:TopRight></TD>
			</TR>
			<TR height="1">
				<TD style="BACKGROUND-COLOR: #959394" width="20"><IMG height="1" src="images/spacer.gif" width="20"></TD>
				<TD style="BACKGROUND-COLOR: #959394" width="100%"><IMG height="1" src="images/spacer.gif" width="100%"></TD>
				<TD style="BACKGROUND-COLOR: #959394" width="20"><IMG height="1" src="images/spacer.gif" width="20"></TD>
			</TR>
			</TBODY></TABLE>
			<script language="javascript">
		function nb_numericOnly(evt)
			{
				evt = (evt) ? evt : event;
				var charCode = (evt.charCode) ? evt.charCode : ((evt.keyCode) ? evt.keyCode
				: ((evt.which) ? evt.which : 0));
				if (charCode > 31 && (charCode < 48 || charCode >57)) {return false;}
				return true;
			}
			
			function catchSubmit()
			{
				if (event.keyCode == 13)
				{
					event.cancelBubble = true;
					event.returnValue = false;
					//document.all.lnkSearch.click();
					return false;
				}
			}	
			</script>
		</form>
	</body>
</HTML>

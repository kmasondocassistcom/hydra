Imports Telerik.WebControls
Imports Telerik.WebControls.RadUploadUtils
Imports Accucentric.docAssist
Imports Accucentric.docAssist.Data.Images

Partial Class EditViewAttachments
    Inherits System.Web.UI.Page
    
    Dim mobjUser As Accucentric.docAssist.Web.Security.User
    Dim mcookieSearch As cookieSearch
    Dim mconSqlImage As SqlClient.SqlConnection

    'Protected WithEvents RadProgressArea1 As Telerik.WebControls.RadProgressArea

    Private intAttachmentId As Integer

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Enum Tables
        HEADER = 0
        DETAIL = 1
    End Enum

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        btnSave.Attributes.Add("onclick", "javascript: window[""RadProgressManager""].StartProgressPolling();")

        Dim conSqlMaster As New SqlClient.SqlConnection

        Try

            Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
            Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))
            Me.mobjUser.AccountId = guidAccountId
            Me.mcookieSearch = New cookieSearch(Me)
            Me.mconSqlImage = New SqlClient.SqlConnection
            chkNewVersion.Attributes("onclick") = "toggleComments();"

            intAttachmentId = CInt(Request.QueryString("AID"))

            If Not Page.IsPostBack Then

                'txtVersionComments.Style("visibility") = "hidden"

                Dim objAttachments As New FileAttachments(Functions.BuildConnection(Me.mobjUser))
                Dim ds As New DataSet
                ds = objAttachments.AttachmentDetailsByVersionId(intAttachmentId, Me.mobjUser.TimeDiffFromGMT)

                If ds.Tables(Tables.DETAIL).Rows.Count = 0 Then
                    dgAttachmentVersionDetail.Visible = False
                    'ReturnScripts.Add("document.all.VersionGrid.innerHTML = 'No previous versions avaialble.';")
                    lblNoVersions.Text = "No previous versions avaialble."
                    lblNoVersions.Visible = True
                Else
                    dgAttachmentVersionDetail.DataSource = ds.Tables(Tables.DETAIL)
                    dgAttachmentVersionDetail.DataBind()
                    lblNoVersions.Text = ""
                    lblNoVersions.Visible = False
                End If

                'Check permissions
                Dim intVersionId As Integer = Me.mcookieSearch.VersionID
                Dim dsVersionControl As Accucentric.docAssist.Data.dsVersionControl

                mconSqlImage = Functions.BuildConnection(Me.mobjUser)

                If Session(intVersionId & "VersionControl") Is Nothing Then
                    dsVersionControl = Accucentric.docAssist.Data.GetVersionControl(intVersionId, Me.mconSqlImage)
                    Session(intVersionId & "VersionControl") = dsVersionControl
                Else
                    dsVersionControl = Session(intVersionId & "VersionControl")
                    ' Check to make sure we haven't changed versions

                    If Not CType(dsVersionControl.ImageVersion.Rows(0), Data.dsVersionControl.ImageVersionRow).VersionID = intVersionId Then
                        dsVersionControl = Data.GetVersionControl(intVersionId, Me.mconSqlImage)
                        Session(intVersionId & "VersionControl") = dsVersionControl
                        'lblPageNo.Value = 1
                    End If
                End If

                Dim intDocumentId As Integer = CType(dsVersionControl.Images.Rows(0), Accucentric.docAssist.Data.dsVersionControl.ImagesRow).DocumentID

                Dim Rights As Accucentric.docAssist.Web.Security.SecurityLevel
                Rights = Functions.DocumentSecurityLevel(Me.mcookieSearch.VersionID, Me.mobjUser, Me.mconSqlImage)

                Select Case Rights
                    Case Web.Security.Functions.SecurityLevel.NoAccess
                        'ReturnScripts.Add("window.close();")
                        Response.Write("window.close();")
                    Case Web.Security.Functions.SecurityLevel.Read
                        lblFileName.Text = ds.Tables(Tables.HEADER).Rows(0)("FileName") & " (" & Functions.GetBytesAsMegs(ds.Tables(Tables.HEADER).Rows(0)("Size")) & ")"
                        lblFileName.Visible = True
                        lblComments.Text = ds.Tables(Tables.HEADER).Rows(0)("Description")
                        lblComments.Visible = True
                        lblTitle.Text = ds.Tables(Tables.HEADER).Rows(0)("DocumentTitle")
                        lblTitle.Visible = True
                        lblCurrentVersion.Text = ds.Tables(Tables.HEADER).Rows(0)("CurrentVersionNum")
                        lblCurrentVersion.Visible = True
                        lblTags.Text = ds.Tables(Tables.HEADER).Rows(0)("Tags")
                        lblTags.Visible = True
                        btnSave.Visible = False
                    Case Web.Security.Functions.SecurityLevel.Change, Web.Security.Functions.SecurityLevel.Delete
                        lblFileName.Text = ds.Tables(Tables.HEADER).Rows(0)("FileName") & " (" & Functions.GetBytesAsMegs(ds.Tables(Tables.HEADER).Rows(0)("Size")) & ")"
                        lblFileName.Visible = True
                        txtComments.Text = ds.Tables(Tables.HEADER).Rows(0)("Description")
                        txtComments.Visible = True
                        txtTitle.Text = ds.Tables(Tables.HEADER).Rows(0)("DocumentTitle")
                        txtTitle.Visible = True
                        lblCurrentVersion.Text = ds.Tables(Tables.HEADER).Rows(0)("CurrentVersionNum")
                        lblCurrentVersion.Visible = True
                        txtTags.Text = ds.Tables(Tables.HEADER).Rows(0)("Tags")
                        txtTags.Visible = True
                End Select

                lblModifyUser.Text = "Last Modified By: " & ds.Tables(Tables.HEADER).Rows(0)("UserName")
                lblModifyDate.Text = "Last Modified Date: " & CDate(ds.Tables(Tables.HEADER).Rows(0)("ModifiedDate")).ToShortDateString & " " & CDate(ds.Tables(Tables.HEADER).Rows(0)("ModifiedDate")).ToShortTimeString
                lblHeaderComments.Text = "Comments: " & ds.Tables(Tables.HEADER).Rows(0)("Comments")

                'Check In/Out Table
                If ds.Tables(Tables.HEADER).Rows(0)("CheckOutStatus") = True Then
                    If mobjUser.ImagesUserId = ds.Tables(Tables.HEADER).Rows(0)("CheckOutUserId") And Rights > Web.Security.Functions.SecurityLevel.Read Then
                        lblCheckOutStatus.Text = "This file is currently checked out by you."
                        btnCancelCheckOut.Visible = True
                        Upload.Visible = True
                    Else
                        lblCheckOutStatus.Text = "This file is currently checked out by " & ds.Tables(Tables.HEADER).Rows(0)("CheckOutUserName")
                        If mobjUser.SysAdmin Then
                            Upload.Visible = True
                            btnCancelCheckOut.Visible = True
                        Else
                            Upload.Visible = False
                            btnCancelCheckOut.Visible = False
                        End If
                    End If
                Else
                    If Rights > Web.Security.Functions.SecurityLevel.Read Or mobjUser.SysAdmin Then
                        Upload.Visible = True
                        btnCheckOut.Visible = True
                    Else
                        Upload.Visible = False
                    End If
                End If
            End If

            'Account level version flag
            conSqlMaster = Functions.BuildMasterConnection
            Dim cmdSql As New SqlClient.SqlCommand("FAVersionModeGet", conSqlMaster)
            cmdSql.CommandType = CommandType.StoredProcedure
            cmdSql.Parameters.Add("@AccountID", Me.mobjUser.AccountId.ToString)

            If cmdSql.ExecuteScalar = 0 Then
                Upload.Visible = False
            End If

        Catch ex As Exception

#If DEBUG Then
            lblError.Text = ex.Message
#Else
            lblerror.Text = "An error has occured."
#End If

        Finally

            If conSqlMaster.State <> ConnectionState.Closed Then
                conSqlMaster.Close()
                conSqlMaster.Dispose()
            End If

            If mconSqlImage.State <> ConnectionState.Closed Then
                mconSqlImage.Close()
                mconSqlImage.Dispose()
            End If

        End Try

    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("FileAttachments.aspx")
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click

        '        Dim sqlConCompany As New SqlClient.SqlConnection
        '        sqlConCompany = Functions.BuildConnection(Me.mobjUser)
        '        sqlConCompany.Open()

        '        Try

        '            Dim objAttachments As New FileAttachments(sqlConCompany, False)


        '            Dim uploadContext As RadUploadContext
        '            uploadContext = RadUploadContext.Current

        '            Dim uploadedFiles As UploadedFileCollection
        '            uploadedFiles = uploadContext.UploadedFiles

        '            If uploadedFiles.Count > 0 Then

        '                Dim strSaveFile As String

        '                Dim file As UploadedFile = uploadedFiles(0)

        '                strSaveFile = ConfigurationSettings.AppSettings("UploadPath") & System.IO.Path.GetFileName(file.FileName)

        '                file.SaveAs(strSaveFile)

        '                Dim byteArray As Byte() = GetDataAsByteArray(strSaveFile)

        '                objAttachments.UpdateAttachment(mcookieSearch.VersionID, Me.mobjUser.ImagesUserId, txtVersionComments.Text, file.ContentLength, _
        '                System.IO.Path.GetFileName(file.FileName), byteArray, intAttachmentId, chkNewVersion.Checked)

        '                Try
        '                    If System.IO.File.Exists(strSaveFile) Then System.IO.File.Delete(strSaveFile)
        '                Catch ex As Exception

        '                End Try

        '                Dim blnCheckedOut As Boolean = objAttachments.IsFileChekedOut(intAttachmentId)
        '                If blnCheckedOut Then
        '                    objAttachments.CheckInByVersionId(intAttachmentId, Me.mobjUser.ImagesUserId, txtVersionComments.Text)
        '                End If

        '                'Track attachment
        '                Try
        '                    Accucentric.docAssist.Web.Data.Functions.TrackPageRequest(Me.mobjUser.UserId, Me.mobjUser.AccountId, "Attachment Add", "In", Me.mcookieSearch.VersionID.ToString, intAttachmentId.ToString, Functions.GetMasterConnectionString)
        '                Catch ex As Exception

        '                End Try

        '            End If

        '            'Update header
        '            objAttachments.UpdateHeader(intAttachmentId, txtTitle.Text, txtComments.Text, txtTags.Text)

        '            Response.Redirect("FileAttachments.aspx")

        '        Catch ex As Exception

        '#If DEBUG Then
        '            lblError.Text = ex.Message
        '#Else
        '            lblerror.Text = "An error has occured."
        '#End If

        '        Finally

        '            If sqlConCompany.State <> ConnectionState.Closed Then
        '                sqlConCompany.Close()
        '                sqlConCompany.Dispose()
        '            End If

        '        End Try

    End Sub

    Private Sub btnCheckOut_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCheckOut.Click

        Try
            Dim objAttachments As New FileAttachments(Functions.BuildConnection(Me.mobjUser))
            objAttachments.CheckOutByVersionId(intAttachmentId, Me.mobjUser.ImagesUserId)
            btnCheckOut.Visible = False
            btnCancelCheckOut.Visible = True
            lblCheckOutStatus.Text = "This file is currently checked out by you."
        Catch ex As Exception

        Finally
            'ReturnControls.Add(lblCheckOutStatus)
            'ReturnControls.Add(btnCheckOut)
            'ReturnControls.Add(btnCancelCheckOut)
        End Try

    End Sub

    Private Sub dgAttachmentVersionDetail_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgAttachmentVersionDetail.ItemCommand

        Try
            'Response.AppendHeader("Content-disposition", "attachment; filename=thefile.ext"); 

            Select Case e.CommandName
                Case "Select"
                    Dim lblDetailVersionId As Label = CType(e.Item.FindControl("lblDetailVersionId"), Label)
                    If Not IsNothing(lblDetailVersionId) Then
                        Dim intVersionId As Integer = lblDetailVersionId.Text

                        'Create file and push it to the user
                        Dim objAttachments As New FileAttachments(Functions.BuildConnection(Me.mobjUser))
                        Dim dt As New DataTable
                        dt = objAttachments.GetFile(intVersionId)

                        'Dim filename As String = Functions.GetByteArrayAsData(dt.Rows(0)("Filename"), dt.Rows(0)("Data"), ConfigurationSettings.AppSettings("UploadPath"))


                        'Dim file As New System.IO.FileInfo(filename)

                        Dim imgByte As Byte() = CType(dt.Rows(0)("Data"), Byte())


                        Response.Clear()
                        Response.AddHeader("Content-Disposition", "attachment; filename=" & dt.Rows(0)("Filename"))
                        'Response.AddHeader("Content-Length", file.Length.ToString())
                        Response.AddHeader("Content-Length", imgByte.Length)
                        Response.ContentType = "application/octet-stream"
                        'Response.WriteFile(file.FullName)
                        Response.BinaryWrite(imgByte)

                        'Track attachment
                        Try
                            Accucentric.docAssist.Web.Data.Functions.TrackPageRequest(Me.mobjUser.UserId, Me.mobjUser.AccountId, "Attachment View", "In", Me.mcookieSearch.VersionID.ToString, lblDetailVersionId.Text.ToString, Functions.GetMasterConnectionString)
                        Catch ex As Exception

                        End Try

                        Response.End()

                    End If

            End Select
        Catch ex As Exception

        End Try


    End Sub

    Private Sub dgAttachmentVersionDetail_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgAttachmentVersionDetail.ItemDataBound

        Try
            'Dim lnkLoad As System.Web.UI.Control = CType(e.Item.FindControl("lnkLoad"), System.Web.UI.Control)
            Dim lblDetailSize As Label = CType(e.Item.FindControl("lblDetailSize"), Label)

            'If Not IsNothing(lnkLoad) Then
            '    RegisterExcludeControl(lnkLoad)
            'End If

            If Not IsNothing(lblDetailSize) Then
                lblDetailSize.Text = Functions.GetBytesAsMegs(lblDetailSize.Text)
                'ReturnControls.Add(lblDetailSize)
            End If

        Catch ex As Exception

        Finally

        End Try

    End Sub

    Private Sub btnCancelCheckOut_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancelCheckOut.Click

        Try
            Dim objAttachments As New FileAttachments(Functions.BuildConnection(Me.mobjUser))
            objAttachments.CancelCheckOutByVersionId(intAttachmentId)
            btnCheckOut.Visible = True
            btnCancelCheckOut.Visible = False
            lblCheckOutStatus.Text = ""
        Catch ex As Exception

        Finally
            'ReturnControls.Add(lblCheckOutStatus)
            'ReturnControls.Add(btnCheckOut)
            'ReturnControls.Add(btnCancelCheckOut)
        End Try

    End Sub

    Private Sub dgAttachmentVersionDetail_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgAttachmentVersionDetail.SelectedIndexChanged

    End Sub
End Class
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="WorkflowWorkflowsEdit.aspx.vb" Inherits="docAssistWeb.WorkflowWorkflowsEdit"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title id="pageTitle">Workflow</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body onload="document.all.txtWorkflowName.focus();">
		<form id="Form1" method="post" runat="server">
			<table class="PageContent" cellSpacing="0" cellPadding="0" width="530" border="0">
				<tr>
					<td><BR>
						&nbsp;
						<asp:Label id="Label2" runat="server">Workflow Name:</asp:Label>&nbsp;
						<asp:TextBox id="txtWorkflowName" runat="server" Width="200px" Font-Names="verdana" Font-Size="8.25pt"
							MaxLength="75"></asp:TextBox></td>
				</tr>
				<tr>
					<td>&nbsp;
						<asp:label id="lblCategory" runat="server">Category Name:</asp:label>&nbsp;
						<asp:dropdownlist id="ddlCategories" runat="server" Width="200px"></asp:dropdownlist>&nbsp;
						<asp:label id="Label1" runat="server">(Optional)</asp:label></td>
				</tr>
				<tr>
					<td>&nbsp;
						<asp:label id="Label3" runat="server">Enabled:</asp:label>&nbsp;
						<asp:CheckBox id="chkEnabled" runat="server"></asp:CheckBox></td>
				<tr>
					<td>&nbsp;&nbsp; <b>Submitters:</b>
					</td>
				</tr>
				<tr>
					<td height="140">
						<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<TR>
								<TD width="50%" height="15">&nbsp;&nbsp;Available:</TD>
								<TD width="25" height="15" rowSpan="1"></TD>
								<TD width="50%" height="15">&nbsp; Assigned:</TD>
							</TR>
							<tr>
								<TD width="50%" height="100%">&nbsp;
									<asp:listbox id="lstSubmitAvailable" runat="server" Width="215px" Height="125px"></asp:listbox></TD>
								<TD vAlign="top" align="center" width="25"><BR>
									<BR>
									<asp:imagebutton id="btnSubmittersAdd" runat="server" ImageUrl="Images/goRight.gif"></asp:imagebutton><BR>
									<BR>
									<asp:imagebutton id="btnSubmittersRemove" runat="server" ImageUrl="Images/goLeft.gif"></asp:imagebutton></TD>
								<td width="50%">&nbsp;
									<asp:listbox id="lstSubmitAssigned" runat="server" Width="215px" Height="125px"></asp:listbox></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>&nbsp;&nbsp; <b>Documents:</b>
					</td>
				</tr>
				<tr>
					<td height="140">
						<table class="PageContent" height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<TR>
								<TD width="50%" height="15">&nbsp;&nbsp;Available:</TD>
								<TD width="25" height="15" rowSpan="1"></TD>
								<TD width="50%" height="15">&nbsp; Assigned:</TD>
							</TR>
							<tr>
								<TD width="50%" height="100%">&nbsp;
									<asp:listbox id="lstDocumentsAvailable" runat="server" Width="215px" Height="125px"></asp:listbox></TD>
								<TD vAlign="top" align="center" width="25">
									<BR>
									<BR>
									<asp:imagebutton id="btnDocumentsAdd" runat="server" ImageUrl="Images/goRight.gif"></asp:imagebutton><BR>
									<BR>
									<BR>
									<asp:imagebutton id="btnDocumentsRemove" runat="server" ImageUrl="Images/goLeft.gif"></asp:imagebutton></TD>
								<td width="50%">&nbsp;
									<asp:listbox id="lstDocumentsAssigned" runat="server" Width="215px" Height="125px"></asp:listbox></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>&nbsp;&nbsp; <b>Entry Queues:</b>
					</td>
				</tr>
				<tr>
					<td height="140">
						<table class="PageContent" height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<TR>
								<TD width="50%" height="15">&nbsp;&nbsp;Available:</TD>
								<TD width="25" height="15" rowSpan="1"></TD>
								<TD width="50%" height="15">&nbsp; Assigned:</TD>
							</TR>
							<tr>
								<TD width="50%" height="100%">&nbsp;
									<asp:listbox id="lstQueuesAvailable" runat="server" Width="215px" Height="125px"></asp:listbox></TD>
								<TD vAlign="top" align="center" width="25">
									<BR>
									<BR>
									<asp:imagebutton id="btnQueuesAdd" runat="server" ImageUrl="Images/goRight.gif"></asp:imagebutton><BR>
									<BR>
									<BR>
									<asp:imagebutton id="btnQueuesRemove" runat="server" ImageUrl="Images/goLeft.gif"></asp:imagebutton></TD>
								<td width="50%">&nbsp;
									<asp:listbox id="lstQueuesAssigned" runat="server" Width="215px" Height="125px"></asp:listbox></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td align="right">&nbsp;
						<asp:label id="lblError" runat="server" Visible="False" ForeColor="Red" Font-Size="8.25pt"
							Font-Names="Verdana"></asp:label>&nbsp; <a href="#" onclick="javascript:window.close()">
							<img src="Images/btn_cancel.gif" border="0"></a>
						<asp:imagebutton id="btnSave" runat="server" ImageUrl="Images/btn_save.gif"></asp:imagebutton></td>
				</tr>
				<tr height="100%">
					<td></td>
				</tr>
			</table>
		</form>
		<script language="javascript">
			
			function submitForm()
			{
				if (event.keyCode == 13)
				{
					event.cancelBubble = true;
					event.returnValue = false;
					document.all.btnSave.click();
				}
			}
			
			function reloadData()
			{
			    opener.location.reload();
				window.close();
			}
		</script>
	</body>
</HTML>

<%--<%@ Page Language="vb" AutoEventWireup="false" Codebehind="Cabinet.aspx.vb" Inherits="docAssistWeb.Cabinet"%>
<%@ Register TagPrefix="componentart" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Choose Cabinet</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
		<base target="_self">
	</HEAD>
	<body>
		<form class="PageContent" id="Form1" method="post" runat="server">
			<table cellpadding="0" cellspacing="0" border="0" height="100%" width="100%">
				<tr style="DISPLAY: none">
					<td>
						<TABLE id="tblShare" cellPadding="0" border="0" runat="server">
							<TR vAlign="top">
								<TD><asp:imagebutton id="btnNewShare" runat="server" ToolTip="New Cabinet"></asp:imagebutton></TD>
								<TD><asp:imagebutton id="btnShareNewFolder" runat="server" ToolTip="New Folder"></asp:imagebutton></TD>
								<TD><A id="Rename" href="#" runat="server"><IMG id="imgRenameShare" onclick="EditModeNode(document.all.NodeId.value);" alt="Rename"
											border="0" runat="server"></A></TD>
								<TD><asp:imagebutton id="btnRemoveFolder" runat="server" ToolTip="Remove Folder"></asp:imagebutton></TD>
							</TR>
						</TABLE>
					</td>
				</tr>
				<tr height=100%>
					<td>
						<DIV id="divTree" style="OVERFLOW: auto; WIDTH: 245px; HEIGHT: 100%"><componentart:treeview id="treeSearch" runat="server" ClientSideOnNodeSelect="LoadProgress" ItemSpacing="0"
								CollapseNodeOnSelect="False" ClientScriptLocation="./treeview_scripts" AutoScroll="False" LineImagesFolderUrl="images/lines/" ShowLines="True" NodeRowCssClass="TreeFont"
								SelectedNodeCssClass="SelectedNode" HoverNodeCssClass="NodeHover" Width="100%" ClientSideOnNodeRename="treeSearch_onNodeRename" ClientSideOnNodeCheckChanged="NodeChecked"
								ContentLoadingImageUrl="./images/spinner.gif" ChildSelectedHoverNodeCssClass="NodeHover" ChildSelectedHoverNodeRowCssClass="NodeHover"></componentart:treeview></DIV>
					</td>
				</tr>
				<tr>
					<td align="right">
					<a href="#"><img src="Images/button_ok.gif" onclick="NodeClick();" border=0></a>&nbsp;&nbsp;
						</td>
				</tr>
		
			</table>
			<script language="javascript">
				
				function LoadProgress(node)
				{
					document.getElementById('NodeId').value = node.ID;
				}
				
				function NodeClick()
				{
				
					var node = window["treeSearch"].findNodeById(document.getElementById('NodeId').value);

					if (node.ID.substring(0,1) == "W")
					{
						alert('You cannot choose a folder. Please select a cabinet.');
						return false;
					}

					if (node.ID == "NOACCESS")
					{
						alert('You do not have access to save files in this folder.');
						return false;
					}
								
					//loop client side to get folder path instead of db hit
					var path = node.Text;
					var newnode = node;
					
					var pass = node.ID + "�" + node.Text;
					if (pass.substring(0,1) == "S")
					{
						window.returnValue = node.ID + "�" + path.replace('&#43;','+'); 
						window.close();
					}
				}

				function EditModeNode(text)
				{
						var node = window["treeSearch"].findNodeById(text);
						if (node != null)
						{
							if (node.ID != 'CabsFolders')
							{ 
								node.edit(); 
							}
						}
				}

				function treeSearch_onNodeRename(sender, eventArgs)
				{
					try
					{
						if ((sender.Text == '') && (eventArgs == ''))
						{
							event.cancelBubble = true;
							event.returnValue = false;
							timer = setTimeout("alert('You must specify a name.');EditModeNode(document.getElementById('NodeId').value);clearTimeout(timer);",300)
						}
						else
						{				
							sender.SetProperty('ImageUrl', './images/spinner.gif');
							sender.SetProperty('Text', 'Saving...');
							sender.SaveState();
							window['treeSearch'].Render();
							document.getElementById('ParentFolder').value = sender.ParentNode.ID;
							document.getElementById('NewText').value = eventArgs;
							document.getElementById('btnRename').click();
						}
					}
					catch(ex)
					{
						alert(ex);
					}
				}

			</script>
			<INPUT id="NodeId" type="hidden" runat="server"> <INPUT id="NewText" type="hidden" runat="server">
			<INPUT id="NodeText" type="hidden" runat="server"> <INPUT id="ParentFolder" type="hidden" runat="server">
			<asp:imagebutton id="btnRename" runat="server" Width="0px" Height="0px"></asp:imagebutton></form>
	</body>
</HTML>
--%>
<%@ Register TagPrefix="uc1" TagName="docHeader" Src="Controls/docHeader.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="Mail.aspx.vb" Inherits="docAssistWeb._Mail"%>
<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="Controls/MenuBar.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<TITLE>docAssist - E-Mail/Download Document</TITLE>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="css/mail.css" type="text/css" rel="stylesheet">
		<LINK href="css/TextboxList.css" type="text/css" rel="stylesheet">
		<LINK href="css/TextboxList.Autocomplete.css" type="text/css" rel="stylesheet">
		<base target="_self">
	</HEAD>
	<BODY id="page_body" runat="server">
		<FORM id="frmMail" method="post" runat="server">
			<table width="100%">
				<tr>
					<td width="100%"><label>To</label>
						<ASP:TEXTBOX id="txtTo" runat="server" Wrap="False"></ASP:TEXTBOX></td>
					<td style="PADDING-LEFT: 15px; PADDING-TOP: 5px" vAlign="top" noWrap width="200" rowSpan="4">
						<div id="pageOptions" runat="server"><label>Pages</label><br>
							<ASP:RADIOBUTTON id="rdoAllPages" onclick="rdoPages_click(this);" runat="server" BorderWidth="0px"
								Checked="True" tabIndex="5"></ASP:RADIOBUTTON>&nbsp;<span class="caption">All 
								pages</span><br>
							<ASP:RADIOBUTTON id="rdoRange" onclick="rdoPages_click(this);" runat="server" BorderWidth="0px" tabIndex="6"></ASP:RADIOBUTTON>&nbsp;<span class="caption">From</span>
							<ASP:TEXTBOX id="txtRangeFrom" runat="server" Enabled="False" Width="37px" CssClass="lines" tabIndex="7">1</ASP:TEXTBOX><span class="caption">&nbsp;to&nbsp;</span>
							<ASP:TEXTBOX id="txtRangeTo" runat="server" Enabled="False" Width="38px" CssClass="lines" tabIndex="8"></ASP:TEXTBOX></div>
						<div id="deliveryOptions" runat="server">
							<br>
							<label>Delivery</label><br>
							<asp:RadioButton id="rdoAttachment" runat="server" Checked="True" GroupName="Delivery" tabIndex="9"></asp:RadioButton><span class="caption">Attachment</span>
							<asp:RadioButton id="rdoLink" runat="server" GroupName="Delivery" tabIndex="10"></asp:RadioButton><span class="caption">Link</span>
						</div>
						<div id="combine" runat="server"><br>
							<br>
							<asp:checkbox id="chkCombine" runat="server" BorderWidth="0px" Visible="True" tabIndex="11"></asp:checkbox><span class="caption">Combine 
								scans into single file</span></div>
						<div id="annotations" runat="server">
							<br>
							<label>Markups</label><br>
							<asp:checkbox id="chkAnnotations" runat="server" tabIndex="12"></asp:checkbox><span class="caption">Include 
								Annotations</span></div>
					</td>
				</tr>
				<tr>
					<td><label>Cc</label>
						<ASP:TEXTBOX id="txtCc" runat="server" tabIndex="1"></ASP:TEXTBOX></td>
				</tr>
				<tr>
					<td><label>Bcc</label>
						<ASP:TEXTBOX id="txtBcc" runat="server" tabIndex="2"></ASP:TEXTBOX></td>
				</tr>
				<tr>
					<td><label>Subject</label><br>
						<ASP:TEXTBOX id="txtSubject" runat="server" Width="100%" CssClass="lines" tabIndex="3"></ASP:TEXTBOX></td>
				</tr>
				<tr>
					<td colSpan="2"><label>Message</label><br>
						<ASP:TEXTBOX id="txtMessage" runat="server" Width="100%" TextMode="MultiLine" Height="240" tabIndex="4"></ASP:TEXTBOX></td>
				</tr>
				<tr>
					<td align="right" colSpan="2">
						<ASP:LABEL id="lblPageError" runat="server" CssClass="error"></ASP:LABEL>&nbsp;&nbsp;
						<ASP:LITERAL id="litSend" runat="server"></ASP:LITERAL><ASP:LITERAL id="litCancel" runat="server"></ASP:LITERAL><asp:literal id="litExport" runat="server"></asp:literal><asp:literal id="litRedirect" runat="server"></asp:literal><BUTTON class="btn" id="btnCancel" onclick="window.close();" type="button"><SPAN><SPAN>Cancel</SPAN></SPAN></BUTTON>&nbsp;&nbsp;<BUTTON class="btn" id="btnDownload" type="button" runat="server"><SPAN><SPAN>Download 
									as PDF</SPAN></SPAN></BUTTON> &nbsp;&nbsp; <BUTTON class="btn" id="btnShare" type="button" runat="server">
							<SPAN><SPAN>Share Document</SPAN></SPAN></BUTTON> &nbsp;&nbsp;<BUTTON class="btn primary" id="btnSend" type="button" runat="server"><SPAN><SPAN>Send 
									Message</SPAN></SPAN></BUTTON>
					</td>
				</tr>
			</table>
			<script src="scripts/jquery.js"></script>
			<script src="scripts/GrowingInput.js"></script>
			<script src="scripts/TextboxList.js"></script>
			<script src="scripts/TextboxList.Autocomplete.js"></script>
			<script src="scripts/TextboxList.Autocomplete.Binary.js"></script>
			<script src="scripts/email.js"></script>
			<SCRIPT language="javascript">
				setTimeout("$('#txtTo').focus();",300);
				// Autocomplete initialization
				var to = new TextboxList('#txtTo', {unique: true, plugins: {autocomplete: {}}});
				var cc = new TextboxList('#txtCc', {unique: true, plugins: {autocomplete: {}}});
				var bcc = new TextboxList('#txtBcc', {unique: true, plugins: {autocomplete: {}}});
			</SCRIPT>
		</FORM>
	</BODY>
</HTML>

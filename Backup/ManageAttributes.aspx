<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ManageAttributes.aspx.vb" Inherits="docAssistWeb.ManageAttributes"%>
<%@ Register TagPrefix="uc1" TagName="AdminNav" Src="Controls/AdminNav.ascx" %>
<%@ Register TagPrefix="uc1" TagName="docHeader" Src="Controls/docHeader.ascx" %>
<%@ Register TagPrefix="uc1" TagName="TopRight" Src="Controls/TopRight.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>docAssist - Manage Attributes</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<TABLE height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD width="100%" colSpan="3">
						<uc1:docHeader id="docHeader" runat="server"></uc1:docHeader>
					</TD>
				</TR>
				<TR vAlign="middle" height="100%">
					<TD height="100%" style="BORDER-RIGHT: #cccccc thin solid" vAlign="top">
						<uc1:AdminNav id="AdminNav1" runat="server"></uc1:AdminNav><img src="dummy.gif" height="1" width="120"></TD>
					<TD class="PageContent" vAlign="top" align="center" height="100%"><BR>
						<table cellpadding="0" cellspacing="0" border="0" width="100%">
							<tr>
								<td><IMG src="Images/manage_attributes.gif"></td>
								<td align="right"></td>
							</tr>
						</table>
						<BR>
						<BR>
						<IMG height="4" src="images/spacer.gif" width="1">&nbsp; <IMG height="4" src="images/spacer.gif" width="1">&nbsp;&nbsp;&nbsp;
						<asp:linkbutton id="lnkAddAttribute" runat="server" Width="104px" Height="24px">Add Attribute</asp:linkbutton>
						<asp:linkbutton id="lnkChangeAttribute" runat="server" Width="120px" Height="24px">Change Attribute</asp:linkbutton>
						<asp:linkbutton id="lnkRemoveAttribute" runat="server" Width="120px" Height="24px">Remove Attribute</asp:linkbutton>
					</TD>
					<TD class="RightContent" height="100%">&nbsp;</TD>
				</TR>
				<TR>
					<TD colSpan="3" class="PageFooter" bgColor="#cccccc" vAlign="bottom"><uc1:TopRight id="TopRight1" runat="server"></uc1:TopRight></TD>
				</TR>
				<TR height="1">
					<TD style="BACKGROUND-COLOR: #959394" width="20"><IMG height="1" src="images/spacer.gif" width="20"></TD>
					<TD style="BACKGROUND-COLOR: #959394" width="100%"><IMG height="1" src="images/spacer.gif" width="100%"></TD>
					<TD style="BACKGROUND-COLOR: #959394" width="20"><IMG height="1" src="images/spacer.gif" width="20"></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>

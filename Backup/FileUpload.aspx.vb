Imports System
Imports System.Data
Imports System.Configuration
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.IO
Imports Accucentric.docAssist
Imports Accucentric.docAssist.Data.Images

Partial Class FileUpload
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private mconSqlImage As New SqlClient.SqlConnection

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

Begin:
        Dim objUser As Accucentric.docAssist.Web.Security.User
        objUser = BuildUserObject(context.Request.Cookies("docAssist"))
        Me.mconSqlImage = Functions.BuildConnection(objUser)
        Dim trnSql As SqlClient.SqlTransaction

        Dim attemps As Integer = 1

        Try

            If context.Request.Files.Count > 0 Then

                Me.mconSqlImage.Open()
                trnSql = Me.mconSqlImage.BeginTransaction

                ' get the applications path 
                Dim tempFile As String = context.Request.PhysicalApplicationPath

                Dim objAttachments As New FileAttachments(Me.mconSqlImage, False, trnSql)

                For j As Integer = 0 To context.Request.Files.Count - 1

                    If Request.Form("advmode") = "true" Then


                        Dim uploadFile As HttpPostedFile = context.Request.Files(j)

                        If uploadFile.ContentLength > 0 Then

                            Dim folderId As Integer = CInt(Request.Cookies("UploadFolderId").Value.Replace("W", ""))

                            Dim fileName As String = uploadFile.FileName
                            tempFile = context.Server.MapPath("./tmp") & "\" & System.Guid.NewGuid.ToString.Replace("-", "") & ".tmp"
                            uploadFile.SaveAs(tempFile)

                            If Session("UploadFiles") Is Nothing Then
                                Dim ds As New DataSet
                                Dim dt As New DataTable
                                dt.Columns.Add(New DataColumn("FolderId", GetType(System.Int32), Nothing, MappingType.Element))
                                dt.Columns.Add(New DataColumn("Title", GetType(System.String), Nothing, MappingType.Element))
                                dt.Columns.Add(New DataColumn("Description", GetType(System.String), Nothing, MappingType.Element))
                                dt.Columns.Add(New DataColumn("Comments", GetType(System.String), Nothing, MappingType.Element))
                                dt.Columns.Add(New DataColumn("Tags", GetType(System.String), Nothing, MappingType.Element))
                                dt.Columns.Add(New DataColumn("Filename", GetType(System.String), Nothing, MappingType.Element))
                                dt.Columns.Add(New DataColumn("GuidName", GetType(System.String), Nothing, MappingType.Element))
                                Dim dr As DataRow
                                dr = dt.NewRow
                                dr("FolderId") = folderId
                                dr("Filename") = uploadfile.FileName
                                dr("GuidName") = tempFile
                                dt.Rows.Add(dr)
                                ds.Tables.Add(dt)
                                Session("UploadFiles") = ds
                            Else
                                'Add row
                                Dim tmpDs As New DataSet
                                tmpDs = Session("UploadFiles")
                                Dim tmpFiles As DataTable
                                tmpFiles = tmpDs.Tables(0)
                                Dim dr As DataRow
                                dr = tmpFiles.NewRow
                                dr("FolderId") = folderId
                                dr("Filename") = uploadfile.FileName
                                dr("GuidName") = tempFile
                                tmpFiles.Rows.Add(dr)
                                tmpDs.Tables.Clear()
                                tmpDs.Tables.Add(tmpFiles)
                                Session("UploadFiles") = tmpDs
                            End If


                            'Dim folderId As Integer = CInt(Request.Cookies("UploadFolderId").Value.Replace("W", ""))

                            'Dim intNewVersionId As Integer
                            'Dim fileData As Byte() = GetDataAsByteArray(tempFile)

                            'Dim intAttachmentId As Integer = objAttachments.InsertFolderAttachment(folderId, 0, objUser.ImagesUserId, "", "", "", "", fileData.Length, fileName, fileData, intNewVersionId)

                            'trnSql.Commit()

                            ''Track attachment
                            'Try
                            '    Accucentric.docAssist.Web.Data.Functions.TrackPageRequest(objUser.UserId, objUser.AccountId, "Attachment Add", "In", intNewVersionId, intAttachmentId.ToString, Functions.GetMasterConnectionString)
                            'Catch ex As Exception

                            'End Try

                            'Try
                            '    System.IO.File.Delete(tempFile)
                            'Catch ex As Exception

                            'End Try

                        End If

                    Else

                    Dim uploadFile As HttpPostedFile = context.Request.Files(j)

                    If uploadFile.ContentLength > 0 Then

                        Dim fileName As String = uploadFile.FileName
                        tempFile = context.Server.MapPath("./tmp") & "\" & System.Guid.NewGuid.ToString.Replace("-", "") & ".tmp"
                        uploadFile.SaveAs(tempFile)

                        Dim folderId As Integer = CInt(Request.Cookies("UploadFolderId").Value.Replace("W", ""))

                        Dim intNewVersionId As Integer
                            Dim fileData As Byte() = GetDataAsByteArray(tempFile)
                            Dim intAttachmentId As Integer = objAttachments.InsertFolderAttachment(folderId, 0, objUser.ImagesUserId, "", "", "", "", fileData.Length, fileName, fileData, intNewVersionId)

                        trnSql.Commit()

                       
                        'Track attachment
                        Try
                            Accucentric.docAssist.Web.Data.Functions.TrackPageRequest(objUser.UserId, objUser.AccountId, "Attachment Add", "In", intNewVersionId, intAttachmentId.ToString, Functions.GetMasterConnectionString)
                        Catch ex As Exception

                        End Try

                        Try
                            System.IO.File.Delete(tempFile)
                        Catch ex As Exception

                        End Try

                    End If

                    End If

                Next

            End If

        Catch sqlEx As SqlClient.SqlException

            If sqlEx.Number = 1205 Then 'Deadlock

                trnSql.Rollback()

                If attemps > 4 Then
                    Throw New Exception("Too many tries")
                End If

                attemps += 1

                Dim timeOut As DateTime = Now.AddMilliseconds(1000)
                Do
                Loop Until Now > timeOut

                GoTo Begin
            End If

        Catch ex As Exception

            trnSql.Rollback()

            'Dim logfile As String = context.Server.MapPath("../tmp/") & "BULK_UPLOAD_ERROR" & System.Guid.NewGuid.ToString.Replace("-", "") & ".txt"
            'Dim fs As New System.IO.StreamWriter(logfile)
            'fs.write(ex.Message & Chr(13) & ex.StackTrace)
            'fs.Close()

            Dim conMaster As SqlClient.SqlConnection
            conMaster = Functions.BuildMasterConnection
            Functions.LogException(objUser.UserId.ToString, objUser.AccountId.ToString, "", "Bulk Upload Error: " & ex.Message, ex.StackTrace, HttpContext.Current.Server.MachineName, "", HttpContext.Current.Request.UrlReferrer.ToString, conMaster, True)

        Finally

            'Dim logfile As String = context.Server.MapPath("../tmp/") & System.Guid.NewGuid.ToString.Replace("-", "") & ".txt"
            'Dim fs As New System.IO.StreamWriter(logfile)
            'fs.Write(sb.ToString)
            'fs.Close()

            If Me.mconSqlImage.State <> ConnectionState.Closed Then
                Me.mconSqlImage.Close()
                Me.mconSqlImage.Dispose()
            End If

        End Try

    End Sub

End Class

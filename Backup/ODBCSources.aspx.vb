'Imports Accucentric.docAssist.Data.Images

'Partial Class ODBCSources
'    Inherits System.Web.UI.Page

'    Private mconCompany As New SqlClient.SqlConnection
'    Private mobjUser As Accucentric.docAssist.Web.Security.User

'#Region " Web Form Designer Generated Code "

'    'This call is required by the Web Form Designer.
'    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

'    End Sub

'    'NOTE: The following placeholder declaration is required by the Web Form Designer.
'    'Do not delete or move it.
'    Private designerPlaceholderDeclaration As System.Object

'    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
'        'CODEGEN: This method call is required by the Web Form Designer
'        'Do not modify it using the code editor.
'        InitializeComponent()
'    End Sub

'#End Region

'    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

'        Response.Expires = 0
'        Response.Cache.SetNoStore()
'        Response.AppendHeader("Pragma", "no-cache")

'        lnkDelete.Attributes.Add("onclick", "return confirm('Are you sure you want to delete this source?');")

'        Try
'            Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
'        Catch ex As Exception
'            Response.Redirect("Default.aspx")
'            Exit Sub
'        End Try

'        Try

'            If Not Page.IsPostBack Then

'                mconCompany = Functions.BuildConnection(Me.mobjUser)

'                Dim Sync As New DataSync(mconCompany)

'                Dim dt As New DataTable
'                dt = Sync.DIObjectListGet()

'                SourcesList.DataSource = dt
'                SourcesList.DataValueField = "ObjectID"
'                SourcesList.DataTextField = "ODBCName"
'                SourcesList.DataBind()

'            End If

'        Catch ex As Exception

'            'ReturnScripts.Add("alert('An error has occured while loading your ODBC sources. Please try again. If the problem continues contact technical support.');")

'            Try
'                Functions.LogException(mobjUser.UserId.ToString, mobjUser.AccountId.ToString, "", "Error loading ODBC sources: " & ex.Message, ex.StackTrace, Server.MachineName, "", Request.UrlReferrer.ToString, Functions.BuildMasterConnection, True)
'            Catch loggingEx As Exception
'            End Try

'        Finally

'            If mconCompany.State <> ConnectionState.Closed Then
'                mconCompany.Close()
'                mconCompany.Dispose()
'            End If

'        End Try

'    End Sub


'    Private Sub Save_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Save.Click

'        If SourcesList.SelectedItem Is Nothing Then Exit Sub

'        ReturnScripts.Add("window.returnValue = '" & SourcesList.SelectedValue.ToString & "�" & SourcesList.SelectedItem.Text.ToString & "';window.close();")

'    End Sub

'    Private Sub lnkEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkEdit.Click
'        ReturnScripts.Add("editODBCSource('" & SourcesList.SelectedValue & "');")
'    End Sub

'    Private Sub lnkDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkDelete.Click

'        If SourcesList.SelectedItem Is Nothing Then Exit Sub

'        Try
'            Me.mconCompany = Functions.BuildConnection(Me.mobjUser)
'            Dim Sync As New DataSync(Me.mconCompany)
'            Sync.DIObjectRemove(SourcesList.SelectedValue)
'            SourcesList.Items.Remove(SourcesList.SelectedItem)
'        Catch ex As Exception
'            ReturnScripts.Add("alert('An error has occured while removing this source. If the problem continues please contact technical support.');")
'        Finally
'            ReturnControls.Add(SourcesList)
'            If Me.mconCompany.State <> ConnectionState.Closed Then
'                Me.mconCompany.Close()
'                Me.mconCompany.Dispose()
'            End If
'        End Try

'    End Sub

'    Private Sub refresh_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)

'        Try

'            mconCompany = Functions.BuildConnection(Me.mobjUser)

'            Dim Sync As New DataSync(mconCompany)

'            Dim dt As New DataTable
'            dt = Sync.DIObjectListGet()

'            SourcesList.DataSource = dt
'            SourcesList.DataValueField = "ObjectID"
'            SourcesList.DataTextField = "ODBCName"
'            SourcesList.DataBind()

'        Catch ex As Exception

'            ReturnScripts.Add("alert('An error has occured while loading your ODBC sources. Please try again. If the problem continues contact technical support.');")

'            Try
'                Functions.LogException(mobjUser.UserId.ToString, mobjUser.AccountId.ToString, "", "Error loading ODBC sources: " & ex.Message, ex.StackTrace, Server.MachineName, "", Request.UrlReferrer.ToString, Functions.BuildMasterConnection, True)
'            Catch loggingEx As Exception
'            End Try

'        Finally

'            ReturnControls.Add(SourcesList)

'            If mconCompany.State <> ConnectionState.Closed Then
'                mconCompany.Close()
'                mconCompany.Dispose()
'            End If

'        End Try

'    End Sub

'End Class
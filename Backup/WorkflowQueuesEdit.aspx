<%@ Page Language="vb" AutoEventWireup="false" Codebehind="WorkflowQueuesEdit.aspx.vb" Inherits="docAssistWeb.WorkflowQueuesEdit"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title id="pageTitle">Workflow Queue</title>
		<meta content="False" name="vs_showGrid">
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" onload="document.all.txtQueue.focus();"
		rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<table class="PageContent" height="100%" cellSpacing="0" cellPadding="0" width="500" border="0">
				<tr>
					<td align="right" colSpan="2"><A href="javascript:window.close();"><IMG src="Images/btn_cancel.gif" border="0"></A><A href="javascript:document.all.btnSave.click();"><IMG src="Images/btn_save.gif" border="0"></A>
					</td>
				</tr>
				<tr>
					<td colSpan="2">&nbsp; <IMG src="Images/queues.gif"></td>
				</tr>
				<tr>
					<td>&nbsp;
						<asp:label id="lblQueue" runat="server">Queue Name:</asp:label></td>
					<TD colSpan="1"><asp:textbox id="txtQueue" runat="server" MaxLength="255" Width="200px" Font-Names="Verdana"
							Font-Size="8.25pt"></asp:textbox>
					</TD>
				</tr>
				<tr>
					<td style="WIDTH: 119px">&nbsp;
						<asp:label id="lblCode" runat="server"> Queue Code:</asp:label></td>
					<TD><asp:textbox id="txtQueueCode" runat="server" MaxLength="25" Width="200px" Font-Names="Verdana"
							Font-Size="8.25pt"></asp:textbox></TD>
				</tr>
				<tr>
					<td style="WIDTH: 119px" vAlign="top">&nbsp;
						<asp:label id="Label1" runat="server"> Description:</asp:label></td>
					<TD vAlign="top"><asp:textbox id="txtDescription" runat="server" Width="300px" Font-Names="Verdana" Font-Size="8.25pt"></asp:textbox></TD>
				</tr>
				<TR>
					<TD style="WIDTH: 119px; HEIGHT: 21px" vAlign="top">&nbsp;
						<asp:label id="lblCategory" runat="server" Width="72px"> Category:</asp:label></TD>
					<TD style="HEIGHT: 21px" vAlign="top">
						<asp:dropdownlist id="ddlCategories" runat="server" Width="200px"></asp:dropdownlist>
						<asp:label id="Label6" runat="server">(Optional)</asp:label></TD>
				</TR>
				<tr>
					<td vAlign="top">&nbsp;
						<asp:label id="Label2" runat="server"> Instructions:</asp:label>&nbsp;&nbsp;&nbsp;&nbsp;
					</td>
					<TD vAlign="top" height="100"><asp:textbox id="txtInstructions" runat="server" MaxLength="2500" Width="100%" Font-Names="Verdana"
							Font-Size="8.25pt" TextMode="MultiLine" Height="100px"></asp:textbox></TD>
				</tr>
				<tr style="DISPLAY: none">
					<td vAlign="top">&nbsp;
						<asp:label id="Label5" runat="server">Total on:</asp:label>&nbsp;&nbsp;&nbsp;&nbsp;
					</td>
					<TD vAlign="top">
						<asp:DropDownList id="ddlTotalAttribute" runat="server" Font-Size="8.25pt" Font-Names="Verdana" Width="200px"></asp:DropDownList>
						<asp:label id="Label7" runat="server">(Optional)</asp:label>
					</TD>
				</tr>
				<tr>
					<td>&nbsp;
						<asp:label id="Label3" runat="server"> Group</asp:label>&nbsp;&nbsp;
					</td>
					<TD><asp:dropdownlist id="ddlGroup" runat="server" Width="200px" Font-Names="Verdana" Font-Size="8.25pt"
							AutoPostBack="True"></asp:dropdownlist></TD>
				</tr>
				<tr>
					<td align="center" colSpan="2"><ASP:DATAGRID id="dgUserPermissions" runat="server" Width="100%" Font-Names="Verdana" Font-Size="8.25pt"
							Height="100%" BorderWidth="1px" BorderColor="#BD0000" AUTOGENERATECOLUMNS="False">
							<SelectedItemStyle Height="20px"></SelectedItemStyle>
							<AlternatingItemStyle CssClass="TableAlternateItem" BackColor="#E0E0E0"></AlternatingItemStyle>
							<ItemStyle Height="20px"></ItemStyle>
							<HeaderStyle Font-Bold="True" Height="20px" ForeColor="White" CssClass="TableHeader" BackColor="Black"></HeaderStyle>
							<Columns>
								<asp:BoundColumn DataField="UserName" ReadOnly="True" HeaderText="User Name">
									<HeaderStyle Width="60%"></HeaderStyle>
								</asp:BoundColumn>
								<asp:TemplateColumn HeaderText="Access">
									<HeaderStyle HorizontalAlign="Center" Width="100%"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center" VerticalAlign="Top"></ItemStyle>
									<ItemTemplate>
										<asp:Label id="lblWebUserId" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.WebUserId") %>'>
										</asp:Label>
										<asp:Label id=lblUserId runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.UserId") %>'>
										</asp:Label>
										<asp:Label id=lblAccess runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.AccessLevel") %>'>
										</asp:Label>
										<asp:RadioButton id="rdoNone" runat="server" Text="None" GroupName="Access"></asp:RadioButton>
										<asp:RadioButton id="rdoView" runat="server" Text="View" GroupName="Access"></asp:RadioButton>
										<asp:RadioButton id="rdoReview" runat="server" Text="Review" GroupName="Access"></asp:RadioButton>
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
						</ASP:DATAGRID></td>
				<tr align="right">
					<td align="left" colSpan="1">&nbsp; Routing Paths</td>
					<td align="right"><asp:dropdownlist id="ddlActionType" runat="server" Width="200px"></asp:dropdownlist>&nbsp;
						<asp:linkbutton id="lnkAdd" runat="server" CssClass="PageContent">Add...</asp:linkbutton></td>
				</tr>
				<tr>
					<td colSpan="2">
						<TABLE height="100%" cellSpacing="0" cellPadding="0" width="490" border="0">
							<tr>
								<td align="center" colSpan="2" height="130"><ASP:DATAGRID id="dgRoutingPath" runat="server" Width="100%" Height="100%" BorderWidth="1px" BorderColor="#BD0000"
										AUTOGENERATECOLUMNS="False" BorderStyle="Solid" GridLines="Horizontal" PageSize="10000" BackColor="#E0E0E0">
										<SelectedItemStyle Height="100px" VerticalAlign="Top" BackColor="Gray"></SelectedItemStyle>
										<EditItemStyle Height="120px"></EditItemStyle>
										<AlternatingItemStyle BorderWidth="0px" CssClass="TableAlternateItem" BackColor="Transparent"></AlternatingItemStyle>
										<ItemStyle Height="120px" VerticalAlign="Top" BackColor="Transparent"></ItemStyle>
										<HeaderStyle Font-Size="9pt" Font-Names="Verdana" Font-Bold="True" HorizontalAlign="Center" ForeColor="White"
											BackColor="Black"></HeaderStyle>
										<Columns>
											<asp:TemplateColumn HeaderText="Action">
												<ItemStyle VerticalAlign="Top"></ItemStyle>
												<ItemTemplate>
													<TABLE height="100" cellSpacing="0" cellPadding="0" width="100%">
														<TR>
															<IMG height="5" src="spacer.gif" width="1"></TR>
														<TR vAlign="top">
															<TD vAlign="top" width="100%" height="100%">&nbsp;&nbsp;
																<asp:Label id="lblDisplay" runat="server" Width="100%" Font-Names="Verdana" Font-Size="9pt"></asp:Label><BR>
																&nbsp;&nbsp;&nbsp;
																<asp:Label id="lblDisplayType" runat="server" CssClass="PageContent" BackColor="#E0E0E0"></asp:Label><BR>
																&nbsp;&nbsp;
																<asp:LinkButton id="lnkRemove" runat="server" CssClass="PageContent" BackColor="#E0E0E0" CommandName="Delete">Remove</asp:LinkButton><BR>
																<BR>
																<asp:DropDownList id="ddlAction" runat="server" Width="100px" Visible="False"></asp:DropDownList></TD>
														</TR>
													</TABLE>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Queues">
												<HeaderStyle Width="350px"></HeaderStyle>
												<ItemTemplate>
													<TABLE id="ListContainer" height="100" cellSpacing="0" cellPadding="0" width="100%">
														<TR>
															<TD height="100%">
																<asp:ListBox id="lstAvailable" runat="server" Width="150px" Height="120px" OnSelectedIndexChanged="lstAvailable_SelectedIndexChanged"></asp:ListBox><BR>
																<asp:DropDownList id="ddlBackAction" runat="server" Width="200px" Visible="False">
																	<asp:ListItem Value="0">Return to previous queue</asp:ListItem>
																	<asp:ListItem Value="1">Return to ANY previous queue</asp:ListItem>
																	<asp:ListItem Value="2">Return to submitter</asp:ListItem>
																</asp:DropDownList><BR>
																<asp:Label id="lblComplete" runat="server" Width="200px" CssClass="PageContent" BackColor="#E0E0E0"
																	Visible="False">The workflow will be completed.</asp:Label></TD>
															<TD align="center" width="100%">
																<asp:ImageButton id="btnAdd" runat="server" ImageUrl="Images/goRight.gif" CommandName="Add"></asp:ImageButton><BR>
																<BR>
																<asp:ImageButton id="btnRemove" runat="server" ImageUrl="Images/goLeft.gif" CommandName="Remove"></asp:ImageButton></TD>
															<TD vAlign="top" align="left">
																<asp:ListBox id="lstAssigned" runat="server" Width="150px" Height="120px"></asp:ListBox><BR>
															</TD>
														</TR>
													</TABLE>
													<TABLE class="PageContent" id="Table1" style="DISPLAY: block" cellSpacing="0" cellPadding="0"
														width="100%">
														<TR>
															<TD vAlign="middle" align="right">Folder Action:
																<asp:RadioButton id="rdoLeave" runat="server" CssClass="PageContent" Text="None" GroupName="Folders"
																	Checked="True"></asp:RadioButton>
																<asp:RadioButton id="rdoClear" runat="server" CssClass="PageContent" Text="Remove" GroupName="Folders"></asp:RadioButton>
																<asp:RadioButton id="rdoMove" runat="server" CssClass="PageContent" Text="Move To" GroupName="Folders"></asp:RadioButton>
																<asp:textbox id="txtFolder" runat="server" Font-Size="8pt" Font-Names="Trebuchet MS" Width="100px"
																	BorderColor="Black" BorderWidth="1px" BackColor="White" BorderStyle="Solid" ReadOnly="True"
																	Enabled="True"></asp:textbox><INPUT id="FolderId" type="hidden" runat="server">&nbsp;<IMG id="ChooseFolder" style="CURSOR: hand" height="19" alt="Choose Folder" src="Images/addToFolder_btn.gif"
																	runat="server"></TD>
														</TR>
													</TABLE>
													<TABLE id="totalTable" style="DISPLAY: block" cellSpacing="0" cellPadding="0" width="100%"
														runat="server">
														<TR>
															<TD align="right">
																<asp:CheckBox id="chkTotal" runat="server" Font-Size="8pt" Font-Names="Verdana" BackColor="Transparent"
																	Text="Enable totaling. "></asp:CheckBox>
																<asp:Label id=lblTotal runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.Total") %>'>
																</asp:Label></TD>
														</TR>
													</TABLE>
												</ItemTemplate>
											</asp:TemplateColumn>
										</Columns>
									</ASP:DATAGRID></td>
							</tr>
						</TABLE>
					</td>
				</tr>
				<tr align="right">
					<td colSpan="2"><asp:literal id="litQueue" runat="server"></asp:literal><asp:literal id="litReload" runat="server"></asp:literal><asp:label id="lblError" runat="server" Font-Names="Verdana" Font-Size="8.25pt" ForeColor="Red"
							Visible="False"></asp:label></td>
				</tr>
				<tr>
					<TD vAlign="top" align="right" colSpan="2"><A onclick="javascript:window.close()" href="#"><IMG src="Images/btn_cancel.gif" border="0"></A>
						<asp:imagebutton id="btnSave" runat="server" ImageUrl="Images/btn_save.gif"></asp:imagebutton></TD>
				</tr>
				<tr>
					<td height="100%"></td>
					<TD height="100%"></TD>
				</tr>
			</table>
		</form>
		<script language="javascript">
			
			function submitForm()
			{
				if (event.keyCode == 13)
				{
					event.cancelBubble = true;
					event.returnValue = false;
					document.all.btnSave.click();
				}
			}
			
			function reloadData()
			{
				opener.location.reload();
				window.close();
			}
			
			function promptUser(msg)
			{
				alert(msg);
			}


			function FolderLookup(text, id)
			{
				var val = window.showModalDialog('Tree.aspx?','FolderLookup','resizable:no;status:no;dialogHeight:625px;dialogWidth:260px;scroll:no');
				if (val != undefined)
				{
					varArray = val.split('�');
					document.getElementById(id).value = varArray[0].replace('W','');
					document.getElementById(text).value = varArray[1];
				}
			}

		</script>
	</body>
</HTML>

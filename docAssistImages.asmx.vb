Imports System.Web.Services.Protocols
Imports System.Web.Services.Description
Imports Accucentric.docAssist.Data.Images
Imports System.IO
Imports System.Data.SqlClient
Imports Accucentric.docAssist
Imports System.Web.Services
Imports Atalasoft.Imaging
Imports Atalasoft.Imaging.Codec
Imports Atalasoft.Imaging.Codec.Tiff
Imports Atalasoft.Imaging.Metadata
Imports Atalasoft.Imaging.Drawing
Imports Atalasoft.Imaging.ImageProcessing
Imports Atalasoft.Imaging.ImageProcessing.Document
Imports Atalasoft.Imaging.ImageProcessing.Transforms
Imports Atalasoft.Imaging.WebControls
Imports Atalasoft.Imaging.WebControls.Annotations
Imports Atalasoft.Annotate
Imports Atalasoft.Annotate.UI
Imports Atalasoft.Annotate.Renderer
Imports System.Threading

'<SoapDocumentService(SoapBindingUse.Literal, SoapServiceRoutingStyle.RequestElement)> _
<System.Web.Services.WebService(Namespace:=DEFAULT_WEBSERVICE_NAMESPACE)> _
Public Class docAssistImages
    Inherits System.Web.Services.WebService

#Region "Declarations"

    Private mobjUser As Accucentric.docAssist.Web.Security.User
    'Public Delegate Sub StoreSession(ByVal VersionId As Integer, ByVal ItemToStore As Object)

#End Region

#Region " Web Services Designer Generated Code "

    Public Sub New()

        MyBase.New()

        'This call is required by the Web Services Designer.
        InitializeComponent()

    End Sub

    'Required by the Web Services Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Web Services Designer
    'It can be modified using the Web Services Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        components = New System.ComponentModel.Container

    End Sub

    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        'CODEGEN: This procedure is required by the Web Services Designer
        'Do not modify it using the code editor.
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

#End Region

#Region "Private Functions"

    'Friend Function GetImage(ByVal intVersionID As Integer, ByVal intImageDetId As Integer, ByVal dvImageAnnotation As DataView, ByVal intRotation As Integer, Optional ByVal strFileName As String = Nothing, _
    'Optional ByVal blnPdf As Boolean = False, Optional ByVal blnExport As Boolean = False, Optional ByVal intUserId As Integer = 0, Optional ByVal blnIncludeAnnotations As Boolean = True, _
    'Optional ByVal blnRemoveRedactions As Boolean = True, Optional ByVal blnPrintJob As Boolean = False) As String

    '    Dim bolConOpen As Boolean
    '    Dim sqlCmd As SqlClient.SqlCommand
    '    Dim strReturn As String
    '    Dim conSql As SqlClient.SqlConnection = BuildConnection(Me.mobjUser)
    '    Try
    '        '
    '        ' Get the image from the database
    '        ' 
    '        sqlCmd = New SqlClient.SqlCommand("acsImageDetail", conSql)
    '        sqlCmd.CommandType = CommandType.StoredProcedure
    '        sqlCmd.Parameters.Add("@ImageDetId", intImageDetId)

    '        If Not conSql.State = ConnectionState.Open Then
    '            bolConOpen = True
    '            conSql.Open()
    '        End If

    '        Dim dr As SqlClient.SqlDataReader = sqlCmd.ExecuteReader(CommandBehavior.SingleRow)
    '        Dim bytImage As Byte()

    '        'Read the image
    '        If dr.Read() Then
    '            bytImage = dr("Image")
    '        End If
    '        dr.Close()

    '        '
    '        ' If we got the image, then rotate it and save the file
    '        '
    '        'Dim ix As New PegasusImaging.WinForms.ImagXpress7.PICImagXpress
    '        Dim ix As New ImagXpr7.ImagXpressClass


    '        If Not bytImage Is Nothing Then
    '            If bytImage.Length > 0 Then
    '                If Not strFileName Is Nothing Then
    '                    strReturn = strFileName
    '                    If System.IO.File.Exists(Server.MapPath(strFileName)) Then
    '                        ix.SaveMultiPage = True
    '                    End If
    '                Else
    '                    'AB 01.16.04
    '                    'Returns a pdf file if the user is e-mailing.
    '                    If blnPdf = True Then
    '                        strReturn = "tmp\" & System.Guid.NewGuid().ToString & ".pdf"
    '                    Else
    '                        strReturn = "tmp\" & System.Guid.NewGuid().ToString & ".tif"

    '                        'If blnPrintJob Then
    '                        '    strReturn = "tmp\" & System.Guid.NewGuid().ToString & ".tif"
    '                        'Else
    '                        '    strReturn = "tmp\" & System.Guid.NewGuid().ToString & ".jpg"
    '                        'End If

    '                    End If
    '                    'End AB
    '                End If

    '                ix.LoadBlob(bytImage, bytImage.Length)

    '                '
    '                ' Check to make sure the image loaded successfully
    '                If ix.ImagError <> 0 Then
    '                    Throw New Exception(ix.ImagError)
    '                End If
    '                '
    '                'Apply annotations to the page if they exists
    '                Dim bytAnnotation As Byte()

    '                If Not IsNothing(dvImageAnnotation) Then
    '                    If dvImageAnnotation.Count > 0 Then
    '                        'Load annotation from session
    '                        'If Not dvImageAnnotation("Annotation") Is Nothing Then
    '                        bytAnnotation = dvImageAnnotation(0).Item("Annotation")
    '                        'End If
    '                    Else
    '                        'Dim bytAnnotation As Byte()
    '                        sqlCmd = New SqlClient.SqlCommand("acsImageAnnotation", conSql)
    '                        sqlCmd.CommandType = CommandType.StoredProcedure
    '                        sqlCmd.Parameters.Add("@VersionID", intVersionID)
    '                        sqlCmd.Parameters.Add("@ImageDetId", intImageDetId)

    '                        If Not conSql.State = ConnectionState.Open Then
    '                            bolConOpen = True
    '                            conSql.Open()
    '                        End If

    '                        Dim drAnn As SqlClient.SqlDataReader = sqlCmd.ExecuteReader(CommandBehavior.SingleRow)

    '                        ' Read the image

    '                        If drAnn.Read() Then
    '                            bytAnnotation = drAnn("Annotation")
    '                        End If
    '                        drAnn.Close()

    '                    End If
    '                End If


    '                Dim nx As New NOTEXP70Lib.NotateXpressClass
    '                nx.InterfaceConnect(ix)

    '                Try
    '                    If Not bytAnnotation Is Nothing Then
    '                        Try
    '                            nx.CreateLayer()
    '                            'Apply annotations
    '                            nx.TIFFAnnotationType = NOTEXP70Lib.TIFFAnnType.NXP_Compatible
    '                            nx.SetAnnFromVariant(bytAnnotation)
    '                            nx.SaveAnnotations = True

    '                            Dim intMissingLayerCount = 2 - nx.NumLayers
    '                            For x As Integer = 1 To intMissingLayerCount
    '                                nx.CreateLayer()
    '                            Next

    '                            Dim intAnnLayer As Integer = nx.GetFirstLayer()
    '                            Dim intRedLayer As Integer = nx.GetNextLayer()

    '                            Dim blnAnnotation As Boolean
    '                            Dim blnRedaction As Boolean

    '                            DocumentAnnotationSecurityLevel(mobjUser.ImagesUserId, intVersionID, conSql, blnAnnotation, blnRedaction)

    '                            If blnAnnotation = True And blnRedaction = True Then

    '                                'enable all annotations
    '                                For X As Integer = 1 To nx.PrgGetItemCount(intAnnLayer)
    '                                    Dim intSeqId As Integer
    '                                    If X = 1 Then
    '                                        intSeqId = nx.PrgGetFirstItem(intAnnLayer)
    '                                    Else
    '                                        intSeqId = nx.PrgGetNextItem(intAnnLayer)
    '                                    End If
    '                                    nx.PrgSetItemLocked(intAnnLayer, intSeqId, False)
    '                                    'nx.PrgSetItemVisible(intAnnLayer, intSeqId, True)
    '                                Next

    '                                'enable all redactions
    '                                For X As Integer = 1 To nx.PrgGetItemCount(intRedLayer)
    '                                    Dim intSeqId As Integer
    '                                    If X = 1 Then
    '                                        intSeqId = nx.PrgGetFirstItem(intRedLayer)
    '                                    Else
    '                                        intSeqId = nx.PrgGetNextItem(intRedLayer)
    '                                    End If
    '                                    nx.PrgSetItemLocked(intRedLayer, intSeqId, False)
    '                                    'nx.PrgSetItemVisible(intRedLayer, intSeqId, True)
    '                                Next

    '                            ElseIf blnAnnotation = False And blnRedaction = True Then

    '                                'disable all annotations from being selected/moved
    '                                For X As Integer = 1 To nx.PrgGetItemCount(intAnnLayer)
    '                                    Dim intSeqId As Integer
    '                                    If X = 1 Then
    '                                        intSeqId = nx.PrgGetFirstItem(intAnnLayer)
    '                                    Else
    '                                        intSeqId = nx.PrgGetNextItem(intAnnLayer)
    '                                    End If
    '                                    nx.PrgSetItemLocked(intAnnLayer, intSeqId, True)
    '                                    'nx.PrgSetItemVisible(intAnnLayer, intSeqId, True)
    '                                Next

    '                                nx.SetActive(intRedLayer, True)

    '                                'enable all redactions
    '                                For X As Integer = 1 To nx.PrgGetItemCount(intRedLayer)
    '                                    Dim intSeqId As Integer
    '                                    If X = 1 Then
    '                                        intSeqId = nx.PrgGetFirstItem(intRedLayer)
    '                                    Else
    '                                        intSeqId = nx.PrgGetNextItem(intRedLayer)
    '                                    End If
    '                                    nx.PrgSetItemLocked(intRedLayer, intSeqId, False)
    '                                    'nx.PrgSetItemVisible(intRedLayer, intSeqId, True)
    '                                Next

    '                            ElseIf blnAnnotation = True And blnRedaction = False Then

    '                                'brand redaction
    '                                nx.DeleteLayer(intAnnLayer)
    '                                nx.SetActive(intRedLayer, True)

    '                                'Check if there any any redactions to brand
    '                                Dim intNumRedactions As Integer = nx.PrgGetItemCount(intRedLayer)
    '                                If intNumRedactions > 0 Then
    '                                    'nx.Brand(1) 'brand the redactions in 1 bit so they show as black blocks
    '                                    For X As Integer = 1 To nx.PrgGetItemCount(intRedLayer)
    '                                        Dim intSeqId As Integer
    '                                        If X = 1 Then
    '                                            intSeqId = nx.PrgGetFirstItem(intRedLayer)
    '                                        Else
    '                                            intSeqId = nx.PrgGetNextItem(intRedLayer)
    '                                        End If

    '                                        nx.PrgSetItemBackstyle(intredlayer, intseqid, NOTEXP70Lib.AnnBackstyle.NXP_OPAQUE)
    '                                        nx.PrgSetItemColor(intredlayer, intseqid, NOTEXP70Lib.annotationATTRIBUTE.NXP_FillColor, Convert.ToUInt32(ColorTranslator.ToOle(System.Drawing.Color.Black)))
    '                                    Next

    '                                    nx.Brand(24)

    '                                End If

    '                                nx.DeleteLayer(intRedLayer)
    '                                nx.SetAnnFromVariant(bytAnnotation)

    '                                'enable all annotations
    '                                For X As Integer = 1 To nx.PrgGetItemCount(intAnnLayer)
    '                                    Dim intSeqId As Integer
    '                                    If X = 1 Then
    '                                        intSeqId = nx.PrgGetFirstItem(intAnnLayer)
    '                                    Else
    '                                        intSeqId = nx.PrgGetNextItem(intAnnLayer)
    '                                    End If
    '                                    nx.PrgSetItemLocked(intAnnLayer, intSeqId, False)
    '                                    'nx.PrgSetItemVisible(intAnnLayer, intSeqId, True)
    '                                Next

    '                                'disable all redactions
    '                                For X As Integer = 1 To nx.PrgGetItemCount(intRedLayer)
    '                                    Dim intSeqId As Integer
    '                                    If X = 1 Then
    '                                        intSeqId = nx.PrgGetFirstItem(intRedLayer)
    '                                    Else
    '                                        intSeqId = nx.PrgGetNextItem(intRedLayer)
    '                                    End If
    '                                    nx.PrgSetItemLocked(intRedLayer, intSeqId, True)
    '                                    'nx.PrgSetItemVisible(intRedLayer, intSeqId, True)
    '                                Next

    '                            ElseIf blnAnnotation = False And blnRedaction = False Then

    '                                'lock annotation layer
    '                                nx.DeleteLayer(intAnnLayer)
    '                                nx.SetActive(intRedLayer, True)

    '                                'Check if there any any redactions to brand
    '                                Dim intNumRedactions As Integer = nx.PrgGetItemCount(intRedLayer)
    '                                If intNumRedactions > 0 Then
    '                                    'nx.Brand(1) 'brand the redactions in 1 bit so they show as black blocks
    '                                    For X As Integer = 1 To nx.PrgGetItemCount(intRedLayer)
    '                                        Dim intSeqId As Integer
    '                                        If X = 1 Then
    '                                            intSeqId = nx.PrgGetFirstItem(intRedLayer)
    '                                        Else
    '                                            intSeqId = nx.PrgGetNextItem(intRedLayer)
    '                                        End If

    '                                        nx.PrgSetItemBackstyle(intredlayer, intseqid, NOTEXP70Lib.AnnBackstyle.NXP_OPAQUE)
    '                                        nx.PrgSetItemColor(intredlayer, intseqid, NOTEXP70Lib.annotationATTRIBUTE.NXP_FillColor, Convert.ToUInt32(ColorTranslator.ToOle(System.Drawing.Color.Black)))
    '                                    Next

    '                                    nx.Brand(24)

    '                                End If

    '                                nx.SetAnnFromVariant(bytAnnotation)
    '                                'nx.DeleteLayer(intRedLayer)

    '                                'disable all annotations from being selected/moved
    '                                For X As Integer = 1 To nx.PrgGetItemCount(intAnnLayer)
    '                                    Dim intSeqId As Integer
    '                                    If X = 1 Then
    '                                        intSeqId = nx.PrgGetFirstItem(intAnnLayer)
    '                                    Else
    '                                        intSeqId = nx.PrgGetNextItem(intAnnLayer)
    '                                    End If
    '                                    nx.PrgSetItemLocked(intAnnLayer, intSeqId, True)
    '                                    'nx.PrgSetItemVisible(intAnnLayer, intSeqId, True)
    '                                Next

    '                                'lock all redactions, they need to be behind the branding to track
    '                                For X As Integer = 1 To nx.PrgGetItemCount(intRedLayer)
    '                                    Dim intSeqId As Integer
    '                                    If X = 1 Then
    '                                        intSeqId = nx.PrgGetFirstItem(intRedLayer)
    '                                    Else
    '                                        intSeqId = nx.PrgGetNextItem(intRedLayer)
    '                                    End If
    '                                    nx.PrgSetItemLocked(intRedLayer, intSeqId, True)
    '                                    'nx.PrgSetItemVisible(intRedLayer, intSeqId, True)
    '                                Next

    '                            End If

    '                            'Deselect all items
    '                            For X As Integer = 1 To nx.PrgGetItemCount(intAnnLayer)
    '                                Dim intSeqId As Integer
    '                                If X = 1 Then
    '                                    intSeqId = nx.PrgGetFirstItem(intAnnLayer)
    '                                Else
    '                                    intSeqId = nx.PrgGetNextItem(intAnnLayer)
    '                                End If
    '                                nx.PrgSetItemShowHandles(intAnnLayer, intSeqId, True)
    '                                nx.PrgSetItemVisible(intAnnLayer, intSeqId, True)
    '                                nx.SetVisible(intAnnLayer, True)
    '                            Next

    '                            For X As Integer = 1 To nx.PrgGetItemCount(intRedLayer)
    '                                Dim intSeqId As Integer
    '                                If X = 1 Then
    '                                    intSeqId = nx.PrgGetFirstItem(intRedLayer)
    '                                Else
    '                                    intSeqId = nx.PrgGetNextItem(intRedLayer)
    '                                End If
    '                                nx.PrgSetItemShowHandles(intRedLayer, intSeqId, True)
    '                                nx.PrgSetItemVisible(intRedLayer, intSeqId, True)
    '                                nx.SetVisible(intRedLayer, True)
    '                            Next

    '                        Catch ex As Exception

    '                        End Try
    '                    Else
    '                        nx.CreateLayer()
    '                        nx.CreateLayer()
    '                    End If

    '                    'Printing
    '                    If blnPrintJob = True Then
    '                        Dim intAnnLayer As Integer = nx.GetFirstLayer()
    '                        Dim intRedLayer As Integer = nx.GetNextLayer()

    '                        If blnRemoveRedactions = False Then
    '                            nx.SetVisible(intAnnLayer, False)
    '                            nx.SetVisible(intRedLayer, True)

    '                            'Check if there any any redactions to brand
    '                            Dim intNumRedactions As Integer = nx.PrgGetItemCount(intRedLayer)
    '                            If intNumRedactions > 0 Then
    '                                'nx.Brand(1) 'brand the redactions in 1 bit so they show as black blocks
    '                                For X As Integer = 1 To nx.PrgGetItemCount(intRedLayer)
    '                                    Dim intSeqId As Integer
    '                                    If X = 1 Then
    '                                        intSeqId = nx.PrgGetFirstItem(intRedLayer)
    '                                    Else
    '                                        intSeqId = nx.PrgGetNextItem(intRedLayer)
    '                                    End If

    '                                    nx.PrgSetItemBackstyle(intredlayer, intseqid, NOTEXP70Lib.AnnBackstyle.NXP_OPAQUE)
    '                                    nx.PrgSetItemColor(intredlayer, intseqid, NOTEXP70Lib.annotationATTRIBUTE.NXP_FillColor, Convert.ToUInt32(ColorTranslator.ToOle(System.Drawing.Color.Black)))
    '                                Next

    '                                If blnIncludeAnnotations Then
    '                                    'Show all annotations
    '                                    For X As Integer = 1 To nx.PrgGetItemCount(nx.GetFirstLayer())
    '                                        Dim intSeqId As Integer
    '                                        If X = 1 Then
    '                                            intSeqId = nx.PrgGetFirstItem(nx.GetFirstLayer())
    '                                        Else
    '                                            intSeqId = nx.PrgGetNextItem(nx.GetFirstLayer())
    '                                        End If
    '                                        nx.PrgSetItemShowHandles(nx.GetFirstLayer(), intSeqId, True)
    '                                        nx.PrgSetItemVisible(nx.GetFirstLayer(), intSeqId, True)
    '                                        nx.SetVisible(nx.GetFirstLayer(), True)
    '                                    Next
    '                                End If
    '                                nx.Brand(24)

    '                            End If

    '                        End If

    '                        If blnIncludeAnnotations = True Then

    '                            nx.SetVisible(nx.GetFirstLayer(), blnIncludeAnnotations)
    '                            nx.SetVisible(nx.GetNextLayer(), False)
    '                            nx.Brand(24)

    '                        End If

    '                    End If

    '                Catch ex As Exception

    '                End Try

    '                ' Rotate the image
    '                If intRotation <> 0 Then
    '                    ix.Rotate(intRotation)
    '                End If
    '                '
    '                ' Save the file to the serer

    '                ix.SaveFileName = Server.MapPath(strReturn)

    '                'AB 01.16.04
    '                'Returns a zip file if the user is e-mailing.
    '                If blnPdf = True Or blnExport = True Then
    '                    ix.SaveMultiPage = True
    '                    ix.SaveFileType = PegasusImaging.WinForms.ImagXpress7.enumSaveFileType.FT_PDF
    '                    ix.SavePDFCompression = PegasusImaging.WinForms.ImagXpress7.enumSavePDFCompression.PDF_CCITTFAX4
    '                Else
    '                    'Removed to allow color images on the web

    '                    'If blnPrintJob Then
    '                    ix.SaveFileType = PegasusImaging.WinForms.ImagXpress7.enumSaveFileType.FT_TIFF_LZW
    '                    ix.SaveTIFFCompression = PegasusImaging.WinForms.ImagXpress7.enumSaveTIFFCompression.TIFF_LZW
    '                    'Else
    '                    '    ix.SaveFileType = PegasusImaging.WinForms.ImagXpress7.enumSaveFileType.FT_JPEG
    '                    '    ix.SaveTIFFCompression = PegasusImaging.WinForms.ImagXpress7.enumSaveTIFFCompression.TIFF_JPEG
    '                    'End If

    '                End If
    '                'End AB

    '                ix.SaveFile()
    '                nx = Nothing

    '                '
    '                ' Check to make sure the file saved
    '                If ix.ImagError <> 0 Then
    '                    Throw New Exception(ix.ImagError)
    '                End If
    '                '
    '                ' Destroy the ImageXpress control
    '                ix = Nothing
    '            End If
    '        End If

    '    Catch ex As Exception
    '        Dim x As String = ex.Message
    '        strReturn = Nothing
    '        Throw ex
    '    Finally
    '        If bolConOpen And (Not conSql.State = ConnectionState.Closed) Then
    '            conSql.Close()
    '        End If
    '    End Try

    '    Return strReturn

    'End Function

    'Friend Function GetImageQuality(ByVal intVersionID As Integer, ByVal intImageDetId As Integer, ByVal dvImageAnnotation As DataView, ByVal intRotation As Integer, _
    'ByVal DLQuality As DownloadQuality, ByRef MaxQuality As Integer, Optional ByVal strFileName As String = Nothing, Optional ByVal blnPdf As Boolean = False, _
    'Optional ByVal blnExport As Boolean = False, Optional ByVal intUserId As Integer = 0, Optional ByVal blnIncludeAnnotations As Boolean = True, Optional ByVal blnRemoveRedactions As Boolean = True, _
    'Optional ByVal blnPrintJob As Boolean = False) As String

    '    Dim bolConOpen As Boolean
    '    Dim sqlCmd As SqlClient.SqlCommand
    '    Dim strReturn As String
    '    Dim conSql As SqlClient.SqlConnection = BuildConnection(Me.mobjUser)
    '    Try
    '        '
    '        ' Get the image from the database
    '        ' 
    '        sqlCmd = New SqlClient.SqlCommand("acsImageDetail", conSql)
    '        sqlCmd.CommandType = CommandType.StoredProcedure
    '        sqlCmd.Parameters.Add("@ImageDetId", intImageDetId)

    '        If Not conSql.State = ConnectionState.Open Then
    '            bolConOpen = True
    '            conSql.Open()
    '        End If

    '        Dim dr As SqlClient.SqlDataReader = sqlCmd.ExecuteReader(CommandBehavior.SingleRow)
    '        Dim bytImage As Byte()

    '        'Read the image
    '        If dr.Read() Then
    '            bytImage = dr("Image")
    '        End If
    '        dr.Close()

    '        ' If we got the image, then rotate it and save the file
    '        Dim ix As New ImagXpr7.ImagXpressClass

    '        If Not bytImage Is Nothing Then

    '            If bytImage.Length > 0 Then
    '                If Not strFileName Is Nothing Then
    '                    strReturn = strFileName
    '                    If System.IO.File.Exists(Server.MapPath(strFileName)) Then
    '                        ix.SaveMultiPage = True
    '                    End If
    '                Else
    '                    If blnPdf = True Then
    '                        strReturn = "tmp\" & System.Guid.NewGuid().ToString & ".pdf"
    '                    Else
    '                        strReturn = "cache\" & System.Guid.NewGuid().ToString & ".tif"
    '                    End If
    '                End If

    '                ix.LoadBlob(bytImage, bytImage.Length)

    '                ' Check to make sure the image loaded successfully
    '                If ix.ImagError <> 0 Then
    '                    Throw New Exception(ix.ImagError)
    '                End If

    '                Dim nx As New NOTEXP70Lib.NotateXpressClass
    '                nx.InterfaceConnect(ix)

    '                ' Rotate the image
    '                If intRotation <> 0 Then
    '                    ix.Rotate(intRotation)
    '                End If

    '                ' Save the file to the serer
    '                'ix.SaveFileName = Server.MapPath(strReturn)
    '                ix.SaveFileName = ConfigurationSettings.AppSettings("GlobalTmp") & System.IO.Path.GetFileName(strReturn)

    '                Select Case ix.IBPP
    '                    Case 1
    '                        MaxQuality = 1
    '                    Case 4
    '                        MaxQuality = 100
    '                    Case 8
    '                        MaxQuality = 100
    '                    Case 24
    '                        MaxQuality = 100
    '                End Select

    '                Select Case DLQuality
    '                    Case DownloadQuality.G4
    '                        'Destroy the ImageXpress control
    '                        ix.SaveFileType = PegasusImaging.WinForms.ImagXpress7.enumSaveFileType.FT_TIFF_G4
    '                        ix.SaveTIFFCompression = PegasusImaging.WinForms.ImagXpress7.enumSaveTIFFCompression.TIFF_CCITTFAX4
    '                        ix.ColorDepth(1, PegasusImaging.WinForms.ImagXpress7.enumPalette.IPAL_Optimized, PegasusImaging.WinForms.ImagXpress7.enumDithered.DI_None)
    '                        ix.IResX = 150
    '                        ix.IResY = 150
    '                        'ix.SaveFileName = ix.SaveFileName.Replace(".tif", "_low.tif")
    '                    Case DownloadQuality.GREYSCALE
    '                        ix.SaveFileType = PegasusImaging.WinForms.ImagXpress7.enumSaveFileType.FT_TIFF_LZW
    '                        ix.SaveTIFFCompression = PegasusImaging.WinForms.ImagXpress7.enumSaveTIFFCompression.TIFF_LZW
    '                        ix.ColorDepth(4, PegasusImaging.WinForms.ImagXpress7.enumPalette.IPAL_Gray, PegasusImaging.WinForms.ImagXpress7.enumDithered.DI_None)
    '                        ix.IResX = 200
    '                        ix.IResY = 200
    '                        'ix.SaveFileName = ix.SaveFileName.Replace(".tif", "_medium.tif")
    '                    Case DownloadQuality.COLOR
    '                        ix.SaveFileType = PegasusImaging.WinForms.ImagXpress7.enumSaveFileType.FT_TIFF_LZW
    '                        ix.SaveTIFFCompression = PegasusImaging.WinForms.ImagXpress7.enumSaveTIFFCompression.TIFF_LZW
    '                        ix.ColorDepth(24, PegasusImaging.WinForms.ImagXpress7.enumPalette.IPAL_Optimized, PegasusImaging.WinForms.ImagXpress7.enumDithered.DI_None)
    '                        ix.IResX = 300
    '                        ix.IResY = 300
    '                        ix.ColorDepth(24, PegasusImaging.WinForms.ImagXpress7.enumPalette.IPAL_Optimized, PegasusImaging.WinForms.ImagXpress7.enumDithered.DI_None)
    '                        'ix.SaveFileName = ix.SaveFileName.Replace(".tif", "_high.tif")
    '                End Select

    '                ix.SaveFile()
    '                nx = Nothing

    '                'Check to make sure the file saved
    '                If ix.ImagError <> 0 Then
    '                    Throw New Exception(ix.ImagError)
    '                End If

    '                ix = Nothing

    '            End If
    '        End If

    '    Catch ex As Exception
    '        Dim x As String = ex.Message
    '        strReturn = Nothing
    '        Throw ex
    '    Finally
    '        If bolConOpen And (Not conSql.State = ConnectionState.Closed) Then
    '            conSql.Close()
    '        End If
    '    End Try

    '    Return strReturn

    'End Function

#End Region

#Region "Web Methods"

    Private Sub StoreInSession(ByVal VersionId As Integer, ByVal ItemToStore As Object)

    End Sub

    <WebMethod(EnableSession:=True)> _
   Public Function GetImageAnnotations(ByVal VersionId As Integer, ByVal PageNo As Integer)

        Dim Request As HttpRequest = Context.Request
        Me.mobjUser = BuildUserObject(Request.Cookies("docAssist"))
        Dim dsVersionControl As Data.dsVersionControl = Session(VersionId & "VersionControl")

        Dim conSql As New SqlClient.SqlConnection

        Try

            conSql = BuildConnection(Me.mobjUser)

            Dim objFind(1) As Object
            Dim intImageDetId As Integer
            objFind(0) = VersionId
            objFind(1) = PageNo
            dsVersionControl.ImageVersionDetail.PrimaryKey = New DataColumn() {dsVersionControl.ImageVersionDetail.Columns("VersionId"), dsVersionControl.ImageVersionDetail.Columns("SeqId")}
            Dim drVersionImageDetail As Data.dsVersionControl.ImageVersionDetailRow = dsVersionControl.ImageVersionDetail.Rows.Find(objFind)
            Dim dvImageAnnotation As New DataView(dsVersionControl.ImageAnnotation, "ImageDetID = " & drVersionImageDetail.ImageDetID.ToString, "ImageDetID", DataViewRowState.CurrentRows)

            If Not drVersionImageDetail Is Nothing Then

                Dim bytAnnotation As Byte()

                If Not IsNothing(drVersionImageDetail) Then

                    If dvImageAnnotation.Count > 0 Then

                        'Load annotation from session
                        If Not dvImageAnnotation.Item(0).Item("Annotation") Is Nothing Then
                            bytAnnotation = dvImageAnnotation.Item(0).Item("Annotation")
                        End If

                    Else

                        'Load from database
                        Dim sqlCmd As New SqlClient.SqlCommand("acsImageAnnotation", conSql)
                        sqlCmd.CommandType = CommandType.StoredProcedure
                        sqlCmd.Parameters.Add("@VersionID", VersionId)
                        sqlCmd.Parameters.Add("@ImageDetId", intImageDetId)

                        Dim drAnn As SqlClient.SqlDataReader = sqlCmd.ExecuteReader(CommandBehavior.SingleRow)

                        ' Read the image
                        If drAnn.Read() Then
                            bytAnnotation = drAnn("Annotation")
                        End If

                        drAnn.Close()

                    End If

                End If

                'Apply annotations
                If Not bytAnnotation Is Nothing Then

                End If

            End If

        Catch ex As Exception

        Finally

            If conSql.State <> ConnectionState.Closed Then
                conSql.Close()
                conSql.Dispose()
            End If

        End Try

    End Function

    <WebMethod(EnableSession:=True)> _
    Public Function InsertAttachment(ByVal AccountUserId As String, ByVal AccountId As String, ByVal UserId As Integer, ByVal FolderId As Integer, ByVal FileName As String, ByVal AttachmentData As Byte(), ByVal Size As Integer) As Integer

        Dim conSql As New SqlClient.SqlConnection
        Dim sb As New System.Text.StringBuilder

        Try

            Me.mobjUser = Functions.BuildUserObject(AccountUserId, AccountId)
            conSql = BuildConnection(Me.mobjUser)

            Dim file As New FileAttachments(conSql)

            sb.Append("FolderId: " & FolderId & Chr(13))
            sb.Append("DocumentId: " & "0" & Chr(13))
            sb.Append("UserId: " & UserId & Chr(13))
            sb.Append("Title: " & "" & Chr(13))
            sb.Append("Desc: " & "" & Chr(13))
            sb.Append("Comments: " & "" & Chr(13))
            sb.Append("Tag: " & "" & Chr(13))
            sb.Append("Length: " & AttachmentData.Length & Chr(13))
            sb.Append("Filename: " & FileName & Chr(13))

            Dim NewVersionId As Integer
            Dim intImageId As Integer
            intImageId = file.InsertFolderAttachment(FolderId, 0, UserId, "", "", "", "", AttachmentData.Length, FileName, AttachmentData, NewVersionId)


            sb.Append("ImageId: " & intImageId & Chr(13))
            sb.Append("VersionID returned: " & NewVersionId & Chr(13))


            'Track attachment
            Try
                Accucentric.docAssist.Web.Data.Functions.TrackPageRequest(Me.mobjUser.UserId, Me.mobjUser.AccountId, "Attachment Add", "In", NewVersionId, intImageId.ToString, Functions.GetMasterConnectionString)
            Catch ex As Exception

            End Try

            Return NewVersionId

        Catch ex As Exception

            Dim logfile As String = Context.Server.MapPath("./tmp") & "\" & "MultiUploadError" & System.Guid.NewGuid.ToString.Replace("-", "") & ".txt"
            Dim fs As New System.IO.StreamWriter(logfile)
            fs.write(ex.Message & Chr(13) & ex.StackTrace)
            fs.Close()

        Finally

            Dim logfile As String = Context.Server.MapPath("./tmp") & "\" & System.Guid.NewGuid.ToString.Replace("-", "") & ".txt"
            Dim fs As New System.IO.StreamWriter(logfile)
            fs.Write(sb.ToString)
            fs.Close()

            If conSql.State <> ConnectionState.Closed Then
                conSql.Close()
                conSql.Dispose()
            End If

        End Try

    End Function

    '<WebMethod(EnableSession:=True)> _
    '   Public Function BuildPage(ByVal intVersionId As Integer, ByVal intPageNo As Integer, ByVal guidAccountId As Guid) As String

    '    Session("Page") = intPageNo

    '    Dim strReturn As String
    '    Dim conSql As New SqlClient.SqlConnection

    '    Dim dsVersionControl As Data.dsVersionControl = Session(intVersionId & "VersionControl")
    '    Try
    '        '
    '        ' Create user objects
    '        '
    '        Dim Request As HttpRequest = Context.Request
    '        Me.mobjUser = BuildUserObject(Request.Cookies("docAssist"))
    '        Me.mobjUser.AccountId = guidAccountId
    '        conSql = BuildConnection(Me.mobjUser)

    '        'Accucentric.docAssist.Data.dsVersionControl
    '        '    Dim dsVersionControl As New Data.dsVersionControl
    '        'dsVersionControl = Data.GetVersionControl(intVersionId, conSql)


    '        Dim blnLegacy As Boolean = True '(guidAccountId.ToString.ToUpper = "16BD416F-A489-4483-8021-A55FE277AA4B")
    '        'Dim blnLegacy As Boolean = True

    '        '
    '        ' Get the row
    '        ' 
    '        Dim objFind(1) As Object
    '        Dim intImageDetId As Integer
    '        objFind(0) = intVersionId
    '        objFind(1) = intPageNo
    '        dsVersionControl.ImageVersionDetail.PrimaryKey = New DataColumn() {dsVersionControl.ImageVersionDetail.Columns("VersionId"), _
    '            dsVersionControl.ImageVersionDetail.Columns("SeqId")}
    '        Dim drVersionImageDetail As Data.dsVersionControl.ImageVersionDetailRow = dsVersionControl.ImageVersionDetail.Rows.Find(objFind)
    '        If Not drVersionImageDetail Is Nothing Then
    '            intImageDetId = drVersionImageDetail.ImageDetID
    '            If drVersionImageDetail.BrowserFile.Trim.Length > 0 Then
    '                '
    '                ' Check to see if the rotation happened before the page was called
    '                Dim dvImageAnnotation As New DataView(dsVersionControl.ImageAnnotation, "ImageDetID = " & drVersionImageDetail.ImageDetID.ToString, "ImageDetID", DataViewRowState.CurrentRows)
    '                'If System.IO.File.GetCreationTime(Server.MapPath(drVersionImageDetail.BrowserFile)) < drVersionImageDetail.Modified Then
    '                '
    '                ' Reload the image and rotate it

    '                strReturn = GetImage(drVersionImageDetail.VersionID, intImageDetId, dvImageAnnotation, drVersionImageDetail.RotateImage, , , )
    '                'strReturn = GetImage(intImageDetId, 0, , , , blnLegacy)
    '                'Else
    '                '
    '                ' Just return the name of the file
    '                'strReturn = drVersionImageDetail.BrowserFile
    '                'Force a build each time in case annoations needs to be passed down
    '                '   strReturn = GetImage(drVersionImageDetail.VersionID, intImageDetId, dvImageAnnotation, drVersionImageDetail.RotateImage, , , , blnLegacy)
    '                'End If
    '            Else
    '                '
    '                ' Load image and save it as a file
    '                '
    '                Dim dvImageAnnotation As New DataView(dsVersionControl.ImageAnnotation, "ImageDetID = " & drVersionImageDetail.ImageDetID.ToString, "ImageDetID", DataViewRowState.CurrentRows)
    '                strReturn = GetImage(drVersionImageDetail.VersionID, intImageDetId, dvImageAnnotation, drVersionImageDetail.RotateImage, , , )
    '            End If
    '            drVersionImageDetail.BrowserFile = strReturn
    '        End If
    '        '
    '        ' Track the page request
    '        '
    '        Functions.TrackPageRequest(Me.mobjUser, "Web View", "Out", intVersionId, intImageDetId, _
    '            ConfigurationSettings.AppSettings("Connection"))

    '        'If dsVersionControl.ImageVersion.Rows.Count > 0 Then strReturn = strReturn & ";" & dsVersionControl.ImageVersion.Rows(0).Item("SeqTotal")

    '    Catch ex As Exception
    '        Dim fileStream As New System.IO.StreamWriter(Server.MapPath("tmp\ErrorMsg.txt"), True)
    '        fileStream.WriteLine("[" & Now & "] - Error: " & ex.Message)
    '        fileStream.WriteLine(ex.StackTrace)
    '        fileStream.Flush()
    '        fileStream.Close()
    '        fileStream = Nothing
    '        'System.Diagnostics.EventLog.WriteEntry("docAssistWeb", ex.Message & vbCrLf & ex.StackTrace)
    '        strReturn = Nothing
    '        Throw New System.Web.Services.Protocols.SoapException(ex.Message & vbCrLf & ex.StackTrace, System.Xml.XmlQualifiedName.Empty, "text")
    '    Finally
    '        If Not conSql Is Nothing Then
    '            If Not conSql.State = ConnectionState.Closed Then conSql.Close()
    '        End If
    '    End Try

    '    Return strReturn.Replace("\", "/")

    'End Function

    <WebMethod(EnableSession:=True)> _
    Public Function BuildMoveThumbnail(ByVal intVersionId As Integer, ByVal intPage As Integer, ByVal intQuality As Integer, ByVal guidAccountId As String) As String()

        Dim strReturn As String
        Dim mconSqlImage As New SqlClient.SqlConnection

        Const MAX_HEIGHT As Integer = 90
        Const MAX_WIDTH As Integer = 81

        Dim image As Atalasoft.Imaging.AtalaImage

        Dim dsVersionControl As Data.dsVersionControl = Session(intVersionId & "VersionControl")
        Dim strFileName As String = "Images/NoThumbPreview.jpg"

        Try
            '
            ' Create user objects
            '
            Dim Request As HttpRequest = Context.Request
            Me.mobjUser = BuildUserObject(Request.Cookies("docAssist"))
            Me.mobjUser.AccountId = New System.Guid(guidAccountId)
            mconSqlImage = BuildConnection(Me.mobjUser)

            Dim objFind(1) As Object
            Dim intImageDetId As Integer
            objFind(0) = intVersionId
            objFind(1) = intPage
            dsVersionControl.ImageVersionDetail.PrimaryKey = New DataColumn() {dsVersionControl.ImageVersionDetail.Columns("VersionId"), dsVersionControl.ImageVersionDetail.Columns("SeqId")}

            Dim drVersionImageDetail As Data.dsVersionControl.ImageVersionDetailRow = dsVersionControl.ImageVersionDetail.Rows.Find(objFind)

            If drVersionImageDetail Is Nothing Then
                Throw New Exception("Unable to locate document thumbnail.")
            End If

            intImageDetId = drVersionImageDetail.ImageDetID

            Dim cmdSql As New SqlClient.SqlCommand("SELECT Image FROM ImageDetail WHERE ImageDetID = @ImageDetID")
            cmdSql.Parameters.Add("@ImageDetID", intImageDetId)
            cmdSql.CommandType = CommandType.Text
            cmdSql.Connection = mconSqlImage
            mconSqlImage.Open()

            Dim imageBytes() As Byte = cmdSql.ExecuteScalar

            strFileName = "cache/" & System.Guid.NewGuid.ToString & "_movetmb.jpg"

            image = AtalaImage.FromByteArray(imageBytes)
            image.Resolution = New Atalasoft.Imaging.Dpi(100, 100, image.Resolution.Units)
            If image.PixelFormat > Atalasoft.Imaging.PixelFormat.Pixel8bppGrayscale Then image = image.GetChangedPixelFormat(Atalasoft.Imaging.PixelFormat.Pixel8bppGrayscale)
            Dim thumb As New Atalasoft.Imaging.ImageProcessing.Thumbnail(New Size(90, 90))
            image = thumb.Create(image)

            Dim enJpeg As New Atalasoft.Imaging.Codec.JpegEncoder
            image.Save(Server.MapPath(strFileName), enJpeg, Nothing)

            Dim returns() As String = " , ".Split(",")
            returns(0) = "/" & strFileName.Replace("\", "/")
            returns(1) = drVersionImageDetail.SeqID & "-" & drVersionImageDetail.ImageDetID

            Return returns

        Catch ex As Exception

            Dim conMaster As SqlClient.SqlConnection
            conMaster = Functions.BuildMasterConnection
            Functions.LogException(mobjUser.UserId.ToString, mobjUser.AccountId.ToString, "", "BuildMoveThumbnail() Error: " & ex.Message, ex.StackTrace, Server.MachineName, "", "", conMaster, True)

        Finally

            image.Dispose()

            If Not mconSqlImage Is Nothing Then
                If Not mconSqlImage.State = ConnectionState.Closed Then mconSqlImage.Close()
            End If

        End Try

    End Function

    <WebMethod(EnableSession:=True)> _
    Public Function BuildAtalaThumbnail(ByVal intVersionId As Integer, ByVal intPage As Integer, ByVal intQuality As Integer, ByVal guidAccountId As String) As String()

        Dim strReturn As String
        Dim mconSqlImage As New SqlClient.SqlConnection

        ' Dim dsVersionControl As Data.dsVersionControl = Session(intVersionId & "VersionControl")

        Dim strFileName As String = "Images/NoThumbPreview.jpg"
        Try
            '
            ' Create user objects
            '
            Dim Request As HttpRequest = Context.Request
            Me.mobjUser = BuildUserObject(Request.Cookies("docAssist"))
            Me.mobjUser.AccountId = New System.Guid(guidAccountId)
            mconSqlImage = BuildConnection(Me.mobjUser)

            'Dim objFind(1) As Object
            'Dim intImageDetId As Integer
            'objFind(0) = intVersionId
            'objFind(1) = intPage
            ''dsVersionControl.ImageVersionDetail.PrimaryKey = New DataColumn() {dsVersionControl.ImageVersionDetail.Columns("VersionId"), dsVersionControl.ImageVersionDetail.Columns("SeqId")}

            'Dim drVersionImageDetail As Data.dsVersionControl.ImageVersionDetailRow = dsVersionControl.ImageVersionDetail.Rows.Find(objFind)

            'If drVersionImageDetail Is Nothing Then
            '    Throw New Exception("Unable to locate document thumbnail.")
            'End If

            'intImageDetId = drVersionImageDetail.ImageDetID

            Dim cmdSql As New SqlClient.SqlCommand("SMThumbnailGet")
            cmdSql.Parameters.Add("@VersionId", intVersionId)
            cmdSql.Parameters.Add("@PageNo", intPage)
            cmdSql.CommandType = CommandType.StoredProcedure
            cmdSql.Connection = mconSqlImage
            mconSqlImage.Open()

            Dim da As New SqlDataAdapter(cmdSql)
            Dim dtImage As New DataTable
            da.Fill(dtImage)
            da.Dispose()

            Dim image As Atalasoft.Imaging.AtalaImage

            'Dim ds As New DataSet
            'ds.Tables.Add(dtImage)
            'ds.WriteXml("c:\temp\blob.xml")

            If dtImage.Rows.Count = 0 Then
                'Return strFileName
            Else
                Dim dr As DataRow = dtImage.Rows(0)

                strFileName = "cache/" & System.Guid.NewGuid.ToString & "_tmb.jpg"

                If dr("Thumbnail") Is System.DBNull.Value Then
                    image = Atalasoft.Imaging.AtalaImage.FromByteArray(dr("Image"))
                    image.Resolution = New Atalasoft.Imaging.Dpi(100, 100, image.Resolution.Units)
                    If image.PixelFormat > Atalasoft.Imaging.PixelFormat.Pixel8bppGrayscale Then image = image.GetChangedPixelFormat(Atalasoft.Imaging.PixelFormat.Pixel8bppGrayscale)
                    Dim thumb As New Atalasoft.Imaging.ImageProcessing.Thumbnail(New Size(180, 180))
                    image = thumb.Create(image)
                Else
                    image = Atalasoft.Imaging.AtalaImage.FromByteArray(dr("Thumbnail"))
                End If

                'Dim dynThreshold As New Atalasoft.Imaging.ImageProcessing.Document.DynamicThresholdCommand
                'image = dynThreshold.Apply(image).Image

                Dim enJpeg As New Atalasoft.Imaging.Codec.JpegEncoder
                image.Save(Server.MapPath(strFileName), enJpeg, Nothing)

            End If

            Dim returns() As String = " , , ,".Split(",")
            returns(0) = "/" & strFileName.Replace("\", "/")
            returns(1) = intPage
            returns(2) = image.Height
            returns(3) = image.Width

            Return returns

        Catch ex As Exception

            Dim conMaster As SqlClient.SqlConnection
            conMaster = Functions.BuildMasterConnection
            Functions.LogException(mobjUser.UserId.ToString, mobjUser.AccountId.ToString, "", "BuildAtalaThumb() Error: " & ex.Message, ex.StackTrace, Server.MachineName, "", "", conMaster, True)

        Finally

            If Not mconSqlImage Is Nothing Then
                If Not mconSqlImage.State = ConnectionState.Closed Then mconSqlImage.Close()
            End If

        End Try

    End Function

    <WebMethod(EnableSession:=True)> _
    Public Function buildPreviewThumbnail(ByVal intVersionId As Integer, ByVal intPage As Integer, ByVal intQuality As Integer, ByVal guidAccountId As String) As DataSet

        Dim strReturn As String
        Dim mconSqlImage As New SqlClient.SqlConnection

        ' Dim dsVersionControl As Data.dsVersionControl = Session(intVersionId & "VersionControl")

        Dim strFileName As String = "Images/NoThumbPreview.jpg"
        Try
            '
            ' Create user objects
            '
            Dim Request As HttpRequest = Context.Request
            Me.mobjUser = BuildUserObject(Request.Cookies("docAssist"))
            'Me.mobjUser.AccountId = New System.Guid(guidAccountId)
            mconSqlImage = BuildConnection(Me.mobjUser)

            'Dim objFind(1) As Object
            'Dim intImageDetId As Integer
            'objFind(0) = intVersionId
            'objFind(1) = intPage
            ''dsVersionControl.ImageVersionDetail.PrimaryKey = New DataColumn() {dsVersionControl.ImageVersionDetail.Columns("VersionId"), dsVersionControl.ImageVersionDetail.Columns("SeqId")}

            'Dim drVersionImageDetail As Data.dsVersionControl.ImageVersionDetailRow = dsVersionControl.ImageVersionDetail.Rows.Find(objFind)

            'If drVersionImageDetail Is Nothing Then
            '    Throw New Exception("Unable to locate document thumbnail.")
            'End If

            'intImageDetId = drVersionImageDetail.ImageDetID

            Dim cmdSql As New SqlClient.SqlCommand("SMThumbnailGet")
            cmdSql.Parameters.Add("@VersionId", intVersionId)
            cmdSql.Parameters.Add("@PageNo", intPage)
            cmdSql.CommandType = CommandType.StoredProcedure
            cmdSql.Connection = mconSqlImage
            mconSqlImage.Open()

            Dim da As New SqlDataAdapter(cmdSql)
            Dim dtImage As New DataTable
            da.Fill(dtImage)
            da.Dispose()

            Dim image As Atalasoft.Imaging.AtalaImage

            'Dim ds As New DataSet
            'ds.Tables.Add(dtImage)
            'ds.WriteXml("c:\temp\blob.xml")

            If dtImage.Rows.Count = 0 Then
                'Return strFileName
            Else
                Dim dr As DataRow = dtImage.Rows(0)

                strFileName = "cache/" & System.Guid.NewGuid.ToString & "_tmb.jpg"

                If dr("Thumbnail") Is System.DBNull.Value Then
                    image = Atalasoft.Imaging.AtalaImage.FromByteArray(dr("Image"))
                    image.Resolution = New Atalasoft.Imaging.Dpi(100, 100, image.Resolution.Units)
                    If image.PixelFormat > Atalasoft.Imaging.PixelFormat.Pixel8bppGrayscale Then image = image.GetChangedPixelFormat(Atalasoft.Imaging.PixelFormat.Pixel8bppGrayscale)
                    Dim thumb As New Atalasoft.Imaging.ImageProcessing.Thumbnail(New Size(180, 180))
                    image = thumb.Create(image)
                Else
                    image = Atalasoft.Imaging.AtalaImage.FromByteArray(dr("Thumbnail"))
                End If

                'Dim dynThreshold As New Atalasoft.Imaging.ImageProcessing.Document.DynamicThresholdCommand
                'image = dynThreshold.Apply(image).Image

                Dim enJpeg As New Atalasoft.Imaging.Codec.JpegEncoder
                image.Save(Server.MapPath(strFileName), enJpeg, Nothing)

            End If

            Dim ds As New DataSet("Result")
            Dim dt As New DataTable("Thumbnail")
            dt.Columns.Add("Filename")
            dt.Columns.Add("Page")
            dt.Columns.Add("Height")
            dt.Columns.Add("Width")

            Dim thumbnail As DataRow
            thumbnail = dt.NewRow
            thumbnail("Filename") = "/" & strFileName.Replace("\", "/")
            thumbnail("Page") = intPage
            thumbnail("Height") = image.Height
            thumbnail("Width") = image.Width
            dt.Rows.Add(thumbnail)

            ds.Tables.Add(dt)

            Return ds

        Catch ex As Exception

            Dim conMaster As SqlClient.SqlConnection
            conMaster = Functions.BuildMasterConnection
            Functions.LogException(mobjUser.UserId.ToString, mobjUser.AccountId.ToString, "", "buildPreviewThumbnail() Error: " & ex.Message, ex.StackTrace, Server.MachineName, "", "", conMaster, True)

        Finally

            If Not mconSqlImage Is Nothing Then
                If Not mconSqlImage.State = ConnectionState.Closed Then mconSqlImage.Close()
            End If

        End Try

    End Function

    '<WebMethod(EnableSession:=True)> _
    Public Function BuildPageQuality(ByVal intVersionId As Integer, ByVal intPageNo As Integer, ByVal RotationDegrees As Integer, ByVal guidAccountId As Guid, _
    ByVal Quality As Integer, ByVal pdf As Boolean) As String()

        Dim strReturn() As String = " ,  ,  ,".Split(",")
        Dim conSql As New SqlClient.SqlConnection
        Dim MaxQuality As String
        Dim Image As AtalaImage

        Session("CurrentPage") = intPageNo

        Try

            Dim Request As HttpRequest = Context.Request
            Me.mobjUser = BuildUserObject(Request.Cookies("docAssist"))
            Me.mobjUser.AccountId = guidAccountId
            conSql = BuildConnection(Me.mobjUser)

            Dim dsVersionControl As Data.dsVersionControl
            If Not Session(intVersionId & "VersionControl") Is Nothing Then
                dsVersionControl = Session(intVersionId & "VersionControl")
            Else
                dsVersionControl = Accucentric.docAssist.Data.GetVersionControl(intVersionId, conSql, Me.mobjUser.ImagesUserId)
                Session(intVersionId & "VersionControl") = dsVersionControl
            End If

            Dim objFind(1) As Object
            objFind(0) = intVersionId
            objFind(1) = intPageNo
            dsVersionControl.ImageVersionDetail.PrimaryKey = New DataColumn() {dsVersionControl.ImageVersionDetail.Columns("VersionId"), dsVersionControl.ImageVersionDetail.Columns("SeqId")}
            Dim drVersionImageDetail As Data.dsVersionControl.ImageVersionDetailRow = dsVersionControl.ImageVersionDetail.Rows.Find(objFind)

            RotationDegrees = drVersionImageDetail.RotateImage

            Dim intImageDetID As Integer = drVersionImageDetail.ImageDetID
            Dim sqlCmd As New SqlClient.SqlCommand("acsImageDetail", conSql)
            sqlCmd.CommandType = CommandType.StoredProcedure
            sqlCmd.Parameters.Add("@ImageDetId", intImageDetID)

            If Not conSql.State = ConnectionState.Open Then conSql.Open()

            Dim dr As SqlClient.SqlDataReader = sqlCmd.ExecuteReader(CommandBehavior.SingleRow)
            Dim bytImage As Byte()

            Try
                'Track the page request
                Functions.TrackPageRequest(Me.mobjUser, "Web View", "Out", intVersionId, intImageDetID, ConfigurationSettings.AppSettings("Connection"))
            Catch ex As Exception

            End Try

            'Read the image
            If dr.Read() Then
                bytImage = dr("Image")
            End If
            dr.Close()

            If Not bytImage Is Nothing Then

                If bytImage.Length > 0 Then

                    Dim fileName As String = System.Guid.NewGuid().ToString & ".tif"
                    Dim saveFileName As String = Server.MapPath("tmp\" & fileName)

                    If pdf Then
                        Dim oFileStream As System.IO.FileStream
                        oFileStream = New System.IO.FileStream(saveFileName.Replace(".tif", ".pdf"), System.IO.FileMode.Create)
                        oFileStream.Write(bytImage, 0, bytImage.Length)
                        oFileStream.Close()
                        Dim outputFile As String = saveFileName.Replace(".tif", "_pdfview.tif")
                        Functions.ExtractTIFPageFromPDF(saveFileName.Replace(".tif", ".pdf"), intPageNo, outputFile)

                        Session("Filename") = saveFileName.Replace(".tif", ".pdf")

                        strReturn(0) = outputFile
                        strReturn(1) = MaxQuality
                        strReturn(2) = Quality
                        Return strReturn
                    Else
                        Image = Atalasoft.Imaging.AtalaImage.FromByteArray(bytImage)
                    End If

                    Dim rawDepth As Integer = Image.ColorDepth

                    Select Case Image.ColorDepth
                        Case 1
                            MaxQuality = 1
                        Case 4
                            MaxQuality = 50
                        Case 8
                            MaxQuality = 50
                        Case 24
                            MaxQuality = 100
                    End Select

                    'Rotate
                    Dim rotate As New Atalasoft.Imaging.ImageProcessing.Transforms.RotateCommand(RotationDegrees * -1)
                    Image = rotate.Apply(Image).Image

                    Dim tiffEncode As New TiffEncoder(TiffCompression.Lzw, False)

                    Select Case Quality
                        Case DownloadQuality.G4
                            tiffEncode.Compression = TiffCompression.Group4FaxEncoding
                            Image.Resolution = New Atalasoft.Imaging.Dpi(150, 150, ResolutionUnit.DotsPerInch)
                            'If Image.ColorDepth <> 1 Then Image = Image.GetChangedPixelFormat(Atalasoft.Imaging.PixelFormat.Pixel1bppIndexed)
                            If Image.ColorDepth <> 1 Then
                                Dim cmd As New ImageProcessing.Document.DynamicThresholdCommand
                                Image = cmd.Apply(Image).Image
                            End If
                        Case DownloadQuality.GREYSCALE
                            If rawDepth < 8 Then
                                'Cannot apply grayscale, use 1 bit
                                tiffEncode.Compression = TiffCompression.Group4FaxEncoding
                                Image.Resolution = New Atalasoft.Imaging.Dpi(150, 150, ResolutionUnit.DotsPerInch)
                                'If Image.ColorDepth <> 1 Then Image = Image.GetChangedPixelFormat(Atalasoft.Imaging.PixelFormat.Pixel1bppIndexed)
                                If Image.ColorDepth <> 1 Then
                                    Dim cmd As New ImageProcessing.Document.DynamicThresholdCommand
                                    Image = cmd.Apply(Image).Image
                                End If
                            Else
                                tiffEncode.Compression = TiffCompression.Lzw
                                Image.Resolution = New Atalasoft.Imaging.Dpi(200, 200, ResolutionUnit.DotsPerInch)
                                If Image.ColorDepth <> 8 Then Image = Image.GetChangedPixelFormat(Atalasoft.Imaging.PixelFormat.Pixel8bppGrayscale)
                            End If
                        Case DownloadQuality.COLOR
                            If rawDepth < 24 Then
                                'Cant apply color, find best alternative
                                If rawDepth < 8 Then
                                    'Cannot apply grayscale, use 1 bit
                                    tiffEncode.Compression = TiffCompression.Group4FaxEncoding
                                    Image.Resolution = New Atalasoft.Imaging.Dpi(150, 150, ResolutionUnit.DotsPerInch)
                                    'If Image.ColorDepth <> 1 Then Image = Image.GetChangedPixelFormat(Atalasoft.Imaging.PixelFormat.Pixel1bppIndexed)
                                    If Image.ColorDepth <> 1 Then
                                        Dim cmd As New ImageProcessing.Document.DynamicThresholdCommand
                                        Image = cmd.Apply(Image).Image
                                    End If
                                Else
                                    tiffEncode.Compression = TiffCompression.Lzw
                                    Image.Resolution = New Atalasoft.Imaging.Dpi(200, 200, ResolutionUnit.DotsPerInch)
                                    If Image.ColorDepth <> 8 Then Image = Image.GetChangedPixelFormat(Atalasoft.Imaging.PixelFormat.Pixel8bppGrayscale)
                                End If
                            Else
                                tiffEncode.Compression = TiffCompression.Lzw
                                Image.Resolution = New Atalasoft.Imaging.Dpi(300, 300, ResolutionUnit.DotsPerInch)
                                If Image.ColorDepth <> 24 Then Image = Image.GetChangedPixelFormat(Atalasoft.Imaging.PixelFormat.Pixel24bppBgr)
                            End If
                    End Select

                    Image.Save(saveFileName, tiffEncode, Nothing)

                    strReturn(0) = saveFileName
                    strReturn(1) = MaxQuality
                    strReturn(2) = Quality
                    strReturn(3) = intImageDetID

                    Return strReturn

                End If

            End If

        Catch ex As Exception

            strReturn = Nothing
            'Throw New System.Web.Services.Protocols.SoapException(ex.Message & vbCrLf & ex.StackTrace, System.Xml.XmlQualifiedName.Empty, "text")
            Throw ex

        Finally

            'Image.Dispose()

            If Not conSql Is Nothing Then
                If Not conSql.State = ConnectionState.Closed Then conSql.Close()
            End If

        End Try

        strReturn(0) = strReturn(0).ToString.Replace("\", "/")
        strReturn(1) = MaxQuality
        strReturn(2) = Quality

        Return strReturn

    End Function

    <WebMethod(EnableSession:=True)> _
    Public Function ChangeVersion(ByVal intVersionId As Integer) As Integer
        Dim conSql As SqlClient.SqlConnection
        Dim dsVersionControl As Data.dsVersionControl = Session(intVersionId & "VersionControl")
        Dim intReturn As Integer = 0
        Try
            '
            ' Create user objects
            '
            Dim Request As HttpRequest = Context.Request
            Me.mobjUser = BuildUserObject(Request.Cookies("docAssist"))
            '
            ' Load the new version
            conSql = BuildConnection(Me.mobjUser)
            Dim dsVersion As Data.dsVersionControl = Data.GetVersionControl(intVersionId, conSql)
            Session(intVersionId & "VersionControl") = dsVersion
            intReturn = CType(dsVersion.ImageVersion.Rows(0), Accucentric.docAssist.Data.dsVersionControl.ImageVersionRow).SeqTotal
        Catch ex As Exception
            Dim fileStream As New System.IO.StreamWriter(Server.MapPath("tmp\ErrorMsg.txt"), True)
            fileStream.WriteLine("[" & Now & "] - Error: " & ex.Message)
            fileStream.WriteLine(ex.StackTrace)
            fileStream.Flush()
            fileStream.Close()
            fileStream = Nothing
            intReturn = -1
            'System.Diagnostics.EventLog.WriteEntry("docAssistWeb", ex.Message & vbCrLf & ex.StackTrace)
            Throw New System.Web.Services.Protocols.SoapException(ex.Message & vbCrLf & ex.StackTrace, System.Xml.XmlQualifiedName.Empty, "text")
        Finally
            If Not conSql Is Nothing Then
                If Not conSql.State = ConnectionState.Closed Then conSql.Close()
            End If
        End Try
        Return intReturn
    End Function

    <WebMethod()> _
    Public Function ReadOCRFromFile(ByVal strFileName As String, ByVal strCoordinates As String) As String

        'Dim guidFileName As System.Guid
        'Dim strFileToReturn As String = Server.MapPath(guidFileName.NewGuid.ToString & ".tif")

        'Dim strResult As String
        'If Not System.IO.File.Exists(Server.MapPath(strFileName)) Then
        '    Throw New Exception("File name is invalid.")
        'End If

        'Try
        '    '
        '    ' create clipping region
        '    '
        '    Dim strDimensions() As String = strCoordinates.Split(",")

        '    If Not strDimensions.Length = 4 Then Throw New Exception("Invalid Coordinates were passed")

        '    Dim intLeft, intTop, intWidth, intHeight As Integer
        '    intLeft = strDimensions(0)
        '    intTop = strDimensions(1)
        '    intWidth = strDimensions(2)
        '    intHeight = strDimensions(3)

        '    Dim ix7 As New PegasusImaging.WinForms.ImagXpress7.PICImagXpress
        '    ix7.PageNbr = 1
        '    ix7.FileName = Server.MapPath(strFileName)
        '    Dim intResX As Integer = ix7.IResX
        '    Dim intResY As Integer = ix7.IResY
        '    ix7.Crop(intLeft, intTop, intWidth, intHeight)
        '    'ix7.IResX = intResX
        '    'ix7.IResY = intResY

        '    ix7.IResX = 150
        '    ix7.IResY = 150

        '    ix7.SaveFileType = PegasusImaging.WinForms.ImagXpress7.enumSaveFileType.FT_TIFF_LZW
        '    ix7.SaveTIFFCompression = PegasusImaging.WinForms.ImagXpress7.enumSaveTIFFCompression.TIFF_CCITTFAX4

        '    '#If DEBUG Then
        '    '            ix7.SaveFileName = "c:\temp\ocr.tif"
        '    '            ix7.SaveFile()
        '    '#End If

        '    ix7.SaveFileName = strFileToReturn
        '    ix7.SaveToBuffer = True
        '    ix7.SaveFile()

        '    Dim lngImageDataPtr As Integer = GlobalLock(ix7.SaveBufferHandle)
        '    Dim ptrImage As New IntPtr(lngImageDataPtr)
        '    Dim lngImageDataSize As Integer = GlobalSize(ptrImage.ToInt32)
        '    Dim bytImageData(lngImageDataSize) As Byte
        '    Runtime.InteropServices.Marshal.Copy(ptrImage, bytImageData, 0, lngImageDataSize)
        '    ix7.DeleteSaveBuffer()
        '    ix7.SaveToBuffer = False
        '    ix7.Dispose()

        '    If Not bytImageData.Length > 30000 Then
        '        Dim OutputWeb As New com.docassist.output.docAssistOutputWebService
        '        strResult = OutputWeb.OCRResult(bytImageData, "ocr$3cr3t")
        '    Else
        '        Throw New Exception("Image area is too large, please select a smaller area")
        '    End If

        'Catch ex As Exception
        '    Throw ex
        'End Try

        'Return strResult

    End Function

    <WebMethod(enableSession:=True)> _
    Public Function ReadOCRFromImage(ByVal intVersionId As Integer, ByVal intPageNo As Integer, ByVal strCoordinates As String) As String

        'Dim dsVersion As Accucentric.docAssist.Data.dsVersionControl
        'Dim strResult As String
        'Try
        '    '
        '    ' Create user objects
        '    '
        '    Dim Request As HttpRequest = Context.Request
        '    Me.mobjUser = BuildUserObject(Request.Cookies("docAssist"))
        '    '
        '    ' Get the version object
        '    '
        '    If Not Session(intVersionId & "VersionControl") Is Nothing Then
        '        dsVersion = Session(intVersionId & "VersionControl")
        '    Else
        '        Throw New Exception("Invalid call to web service")
        '    End If

        '    Dim drImageVersionDetail As Data.dsVersionControl.ImageVersionDetailRow
        '    If dsVersion.ImageVersionDetail.Rows.Count >= intPageNo Then
        '        drImageVersionDetail = dsVersion.ImageVersionDetail.Rows(intPageNo - 1)
        '        '
        '        ' Check to see if the rotation happened before the page was called
        '        Dim strFileName As String
        '        If System.IO.File.GetCreationTime(Server.MapPath(drImageVersionDetail.BrowserFile)) < drImageVersionDetail.Modified Then
        '            '
        '            ' Reload the image and rotate it
        '            strFileName = GetImage(drImageVersionDetail.VersionID, drImageVersionDetail.ImageDetID, Nothing, drImageVersionDetail.RotateImage)
        '        Else
        '            '
        '            ' Just return the name of the file
        '            strFileName = drImageVersionDetail.BrowserFile
        '        End If
        '        strResult = ReadOCRFromFile(strFileName, strCoordinates)
        '    End If
        'Catch ex As Exception
        '    Dim fileStream As New System.IO.StreamWriter(Server.MapPath("tmp\ErrorMsg.txt"), True)
        '    fileStream.WriteLine("[" & Now & "] - Error: " & ex.Message)
        '    fileStream.WriteLine(ex.StackTrace)
        '    fileStream.Flush()
        '    fileStream.Close()
        '    fileStream = Nothing
        '    'System.Diagnostics.EventLog.WriteEntry("docAssistWeb", ex.Message & vbCrLf & ex.StackTrace)
        '    Throw ex 'New System.Web.Services.Protocols.SoapException(ex.Message & vbCrLf & ex.StackTrace, System.Xml.XmlQualifiedName.Empty, "text")
        'Finally

        'End Try

        'Return strResult

    End Function

    <WebMethod(EnableSession:=True)> _
    Public Function RotationIndex(ByVal intPageNo As Integer, ByVal intVersionId As Integer) As Integer

        Dim intReturn As Integer
        Dim dsVersion As Accucentric.docAssist.Data.dsVersionControl
        Try
            If Not Session(intVersionId & "VersionControl") Is Nothing Then
                dsVersion = Session(intVersionId & "VersionControl")
            Else
                Throw New Exception("Invalid call to web service")
            End If

            Dim drImageVersionDetail As Accucentric.docAssist.Data.dsVersionControl.ImageVersionDetailRow
            If dsVersion.ImageVersionDetail.Rows.Count > intPageNo Then
                drImageVersionDetail = dsVersion.ImageVersionDetail.Rows(intPageNo - 1)
                intReturn = drImageVersionDetail.RotateImage
            Else
                Throw New Exception("Page request is greater than the current page count.")
            End If

        Catch ex As Exception
            Throw ex
        End Try

        Return intReturn

    End Function

    <WebMethod(EnableSession:=True)> _
    Public Function TrackRotation(ByVal intVersionId As Integer, ByVal intSeqId As Integer, ByVal intRotation As Integer) As Boolean

        Dim bolReturn As Boolean = False
        Dim dsVersionControl As Accucentric.docAssist.Data.dsVersionControl

        Try

            'Load the existing dataset
            If Not Session(intVersionId & "VersionControl") Is Nothing Then
                dsVersionControl = Session(intVersionId & "VersionControl")
            End If

            'Check for the page already existing
            Dim objFind(1) As Object
            objFind(0) = intVersionId
            objFind(1) = intSeqId
            Dim drImageVersionDetail As Data.dsVersionControl.ImageVersionDetailRow = dsVersionControl.ImageVersionDetail.Rows.Find(objFind)

            'Save the image rotation
            If Not drImageVersionDetail Is Nothing Then
                drImageVersionDetail.RotateImage += intRotation
                drImageVersionDetail.Modified = Now()
            End If

            'TODO: Rotate annotations

            'Save the session variable
            Session(intVersionId & "VersionControl") = dsVersionControl

            Return True

        Catch ex As Exception
            Throw ex
        Finally

        End Try

    End Function

    <WebMethod(EnableSession:=True)> _
  Public Function TrackAllRotation(ByVal intVersionId As Integer, ByVal intRotation As Integer) As Boolean

        Dim bolReturn As Boolean = False
        Dim dsVersionControl As Accucentric.docAssist.Data.dsVersionControl

        Try

            'Load the existing dataset
            If Not Session(intVersionId & "VersionControl") Is Nothing Then
                dsVersionControl = Session(intVersionId & "VersionControl")
            End If

            'Rotate all pages
            For Each ivRow As Data.dsVersionControl.ImageVersionDetailRow In dsVersionControl.ImageVersionDetail.Rows
                ivRow.RotateImage += intRotation
                ivRow.Modified = Now()
            Next

            'TODO: Rotate annotations

            'Save the session variable
            Session(intVersionId & "VersionControl") = dsVersionControl

            Return True

        Catch ex As Exception
            Throw ex
        Finally

        End Try

    End Function

    <WebMethod(EnableSession:=True)> _
    Public Function PageCount(ByVal intVersionId As Integer) As Integer
        Dim intReturn As Integer = 0
        'Dim conSql As SqlClient.SqlConnection
        Try
            '
            ' Create user objects
            '
            Dim Request As HttpRequest = Context.Request
            Me.mobjUser = BuildUserObject(Request.Cookies("docAssist"))
            '
            ' Get the page count
            ' 
            If Not Me.mobjUser Is Nothing Then
                If Not Session(intVersionId & "VersionControl") Is Nothing Then
                    Dim dsVersion As Data.dsVersionControl = Session(intVersionId & "VersionControl")
                    Dim dtImageVesionDetail As Data.dsVersionControl.ImageVersionDetailDataTable = dsVersion.ImageVersionDetail
                    If Not dtImageVesionDetail.GetChanges(DataRowState.Deleted) Is Nothing Then
                        intReturn = dtImageVesionDetail.Rows.Count - dtImageVesionDetail.GetChanges(DataRowState.Deleted).Rows.Count
                    Else
                        intReturn = dtImageVesionDetail.Rows.Count
                    End If
                End If
                'conSql = BuildConnection(Me.mobjUser)
                'Dim cmdSql As New SqlClient.SqlCommand("acsImageVersions", conSql)
                'cmdSql.CommandType = CommandType.StoredProcedure
                'cmdSql.Parameters.Add("@VersionId", id)
                'conSql.Open()
                'Dim dr As SqlClient.SqlDataReader = cmdSql.ExecuteReader
                'If dr.Read Then
                '    intReturn = dr("SeqTotal")
                'End If
                'cmdSql.Dispose()
            End If
        Catch ex As Exception
            Throw New System.Web.Services.Protocols.SoapException(ex.Message & "(" & ex.StackTrace & ")", System.Xml.XmlQualifiedName.Empty, "text")
        Finally
            'If Not conSql Is Nothing Then
            '    If Not conSql.State = ConnectionState.Closed Then conSql.Close()
            'End If
        End Try
        'conSql.Dispose()


        Return intReturn

    End Function

    '<WebMethod(EnableSession:=True)> _
    'Public Function BuildPageEmailExport(ByVal intVersionId As Integer, ByVal intStartPage As Integer, ByVal intEndPage As Integer) As String
    '    Return BuildPages(intVersionId, intStartPage, intEndPage, True, True)
    'End Function

    '<WebMethod(EnableSession:=True)> _
    'Public Function PrintPages(ByVal intVersionId As Integer, ByVal intStartPage As Integer, ByVal intEndPage As Integer, ByVal blnIncludeAnnotations As Boolean, ByVal blnRemoveRedactions As Boolean) As String

    '    Dim strReturn As String = ""
    '    Dim conSql As SqlClient.SqlConnection
    '    Dim X As Integer
    '    Dim ix As New PegasusImaging.WinForms.ImagXpress7.PICImagXpress
    '    Dim intPageCount As Integer

    '    Try
    '        Dim bytBlob() As Byte

    '        'Create user objects
    '        Dim Request As HttpRequest = Context.Request
    '        Me.mobjUser = BuildUserObject(Request.Cookies("docAssist"))

    '        Dim dsVersionControl As Data.dsVersionControl = Session(intVersionId & "VersionControl")
    '        Dim drImageVersionDetail As Data.dsVersionControl.ImageVersionDetailRow, drv As DataRowView
    '        Dim dvImageVersionDetail As New DataView(dsVersionControl.ImageVersionDetail, "SeqId >= " & intStartPage.ToString & " AND SeqId <=" & intEndPage.ToString, "SeqId", DataViewRowState.CurrentRows)

    '        strReturn = Nothing

    '        For Each drv In dvImageVersionDetail
    '            drImageVersionDetail = drv.Row

    '            Dim dvImageAnnotation As New DataView(dsVersionControl.ImageAnnotation, "ImageDetID = " & drImageVersionDetail.ImageDetID.ToString, "ImageDetID", DataViewRowState.CurrentRows)
    '            If strReturn Is Nothing Then
    '                strReturn = GetImage(drImageVersionDetail.VersionID, drImageVersionDetail.ImageDetID, dvImageAnnotation, drImageVersionDetail.RotateImage, Nothing, False, False, , blnIncludeAnnotations, blnRemoveRedactions, True)
    '            Else
    '                GetImage(drImageVersionDetail.VersionID, drImageVersionDetail.ImageDetID, dvImageAnnotation, drImageVersionDetail.RotateImage, strReturn, False, False, , blnIncludeAnnotations, blnRemoveRedactions, True)
    '            End If

    '        Next

    '        ix.FileName = Server.MapPath(strReturn)
    '        intPageCount = ix.Pages
    '        ix = Nothing

    '    Catch ex As Exception
    '        'System.Diagnostics.EventLog.WriteEntry("docAssistWeb", ex.Message & vbCrLf & ex.StackTrace)
    '        'Throw New System.Web.Services.Protocols.SoapException(ex.Message & vbCrLf & ex.StackTrace, System.Xml.XmlQualifiedName.Empty, "text")
    '        Throw New Exception(ex.Message)
    '    Finally
    '        If Not conSql Is Nothing Then
    '            If Not conSql.State = ConnectionState.Closed Then conSql.Close()
    '        End If
    '    End Try

    '    Return strReturn.Replace("\", "/") & "?" & intPageCount.ToString

    'End Function

    'Public Function BuildPages(ByVal intVersionId As Integer, ByVal intStartPage As Integer, ByVal intEndPage As Integer, ByVal blnPdf As Boolean, Optional ByVal blnExport As Boolean = False, Optional ByVal blnIncludeAnnotations As Boolean = False, Optional ByVal blnRemoveRedactions As Boolean = False, Optional ByVal WebPage As Page = Nothing) As String
    '    'Generates a TIFF for the given page range and returns a path for the TIFF file.
    '    Dim strReturn As String = ""

    '    Try
    '        Dim bytBlob() As Byte

    '        Dim Request As HttpRequest = Context.Request
    '        Me.mobjUser = BuildUserObject(Request.Cookies("docAssist"))

    '        Dim dsVersionControl As Data.dsVersionControl = Session(intVersionId & "VersionControl")
    '        Dim drImageVersionDetail As Data.dsVersionControl.ImageVersionDetailRow, drv As DataRowView
    '        Dim dvImageVersionDetail As New DataView(dsVersionControl.ImageVersionDetail, "SeqId >= " & intStartPage.ToString & " AND SeqId <=" & intEndPage.ToString, "SeqId", DataViewRowState.CurrentRows)

    '        For Each drv In dvImageVersionDetail
    '            drImageVersionDetail = drv.Row

    '            Dim dvImageAnnotation As New DataView(dsVersionControl.ImageAnnotation, "ImageDetID = " & drImageVersionDetail.ImageDetID.ToString, "ImageDetID", DataViewRowState.CurrentRows)

    '            If strReturn Is Nothing Then
    '                If blnPdf = True And blnExport = True Then
    '                    If blnPdf = True Then
    '                        strReturn = ExportPDF(Me.mobjUser, drImageVersionDetail.VersionID, intStartPage, intEndPage, WebPage)
    '                        Return strReturn
    '                        'strReturn = objExport.GetExportImage(Me.mobjUser, drImageVersionDetail.VersionID, drImageVersionDetail.ImageDetID, strReturn, blnIncludeAnnotations, blnRemoveRedactions, Server)
    '                    Else
    '                        strReturn = GetImage(drImageVersionDetail.VersionID, drImageVersionDetail.ImageDetID, dvImageAnnotation, drImageVersionDetail.RotateImage, Nothing, False, False)
    '                    End If
    '                End If
    '            Else
    '                If blnPdf = True And blnExport = True Then
    '                    If blnPdf = True Then
    '                        strReturn = ExportPDF(Me.mobjUser, drImageVersionDetail.VersionID, intStartPage, intEndPage, WebPage)
    '                        Return strReturn
    '                        'strReturn = objExport.GetExportImage(Me.mobjUser, drImageVersionDetail.VersionID, drImageVersionDetail.ImageDetID, strReturn, blnIncludeAnnotations, blnRemoveRedactions, Server)
    '                    Else
    '                        strReturn = GetImage(drImageVersionDetail.VersionID, drImageVersionDetail.ImageDetID, dvImageAnnotation, drImageVersionDetail.RotateImage, Nothing, False, False)
    '                    End If
    '                Else
    '                    GetImage(drImageVersionDetail.VersionID, drImageVersionDetail.ImageDetID, dvImageAnnotation, drImageVersionDetail.RotateImage, strReturn, True, True)
    '                End If
    '            End If

    '        Next

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message)
    '    End Try

    '    Return strReturn

    'End Function

    <WebMethod(EnableSession:=True)> _
    Public Function IndexerSaveFavorite(ByVal mUserID As String, ByVal mAccountID As String, ByVal mAuthenticationKey As String, ByVal FavName As String, ByVal FavLocation As String, ByVal SeqID As Integer) As Boolean

        Dim mSessionID As String
        Dim intReturn As Integer = 0
        Dim consql As New SqlClient.SqlConnection
        Dim dsReturn As New DataSet
        Dim da As New SqlClient.SqlDataAdapter
        Dim objEncryption As New Encryption.Encryption


        Try
            'Get the users current sessionid
            Dim consqluser As New SqlConnection

            consqluser.ConnectionString = objEncryption.Decrypt(ConfigurationSettings.AppSettings("Connection"))
            If Not consqluser.State = ConnectionState.Open Then consqluser.Open()
            Dim dtUser As New DataTable
            Dim cmdUSql As New SqlClient.SqlCommand("SELECT SessionID FROM USERS WHERE UserID = @UserID", consqluser)
            cmdUSql.Parameters.Add("@UserID", mUserID)
            cmdUSql.CommandTimeout = 120
            Dim daSql As New SqlClient.SqlDataAdapter(cmdUSql)
            daSql.Fill(dtUser)

            If dtUser.Rows.Count <= 0 Then
                Return False
                Exit Function
            Else
                mSessionID = dtUser.Rows(0).Item("SessionID")
            End If


            '
            ' Create user objects
            '

            Me.mobjUser = BuildUserObject(mSessionID, mAccountID)

            '
            ' Get the page count
            ' 
            If Not Me.mobjUser Is Nothing Then
                consql = BuildConnection(Me.mobjUser)
                Dim cmdSql As New SqlClient.SqlCommand("acsSetIndexerFavorite", consql)
                cmdSql.CommandType = CommandType.StoredProcedure
                Dim guidSessionID As New System.Guid(mUserID)
                cmdSql.Parameters.Add("@UserID", Me.mobjUser.ImagesUserId)
                cmdSql.Parameters.Add("@SeqID", SeqID)
                cmdSql.Parameters.Add("@FavName", FavName)
                cmdSql.Parameters.Add("@FavLocation", FavLocation)
                consql.Open()
                cmdSql.ExecuteNonQuery()

                cmdSql.Dispose()
            End If

        Catch ex As Exception
            'Throw New System.Web.Services.Protocols.SoapException("Unable to login user!. " & ex.Message, System.Xml.XmlQualifiedName.Empty, "text")
            Return False
        End Try
        consql.Dispose()


        Return True

    End Function


    <WebMethod(EnableSession:=True)> _
       Public Function IndexerRemoveFavorite(ByVal mUserID As String, ByVal mAccountID As String, ByVal mAuthenticationKey As String, ByVal FavName As String) As Boolean

        Dim mSessionID As String
        Dim intReturn As Integer = 0
        Dim consql As New SqlClient.SqlConnection
        Dim dsReturn As New DataSet
        Dim da As New SqlClient.SqlDataAdapter
        Dim objEncryption As New Encryption.Encryption


        Try
            'Get the users current sessionid
            Dim consqluser As New SqlConnection

            consqluser.ConnectionString = objEncryption.Decrypt(ConfigurationSettings.AppSettings("Connection"))
            If Not consqluser.State = ConnectionState.Open Then consqluser.Open()
            Dim dtUser As New DataTable
            Dim cmdUSql As New SqlClient.SqlCommand("SELECT SessionID FROM USERS WHERE UserID = @UserID", consqluser)
            cmdUSql.Parameters.Add("@UserID", mUserID)
            cmdUSql.CommandTimeout = 120
            Dim daSql As New SqlClient.SqlDataAdapter(cmdUSql)
            daSql.Fill(dtUser)

            If dtUser.Rows.Count <= 0 Then
                Return False
                Exit Function
            Else
                mSessionID = dtUser.Rows(0).Item("SessionID")
            End If


            '
            ' Create user objects
            '

            Me.mobjUser = BuildUserObject(mSessionID, mAccountID)

            '
            ' Get the page count
            ' 
            If Not Me.mobjUser Is Nothing Then
                consql = BuildConnection(Me.mobjUser)
                Dim cmdSql As New SqlClient.SqlCommand("acsRemoveIndexerFavorite", consql)
                cmdSql.CommandType = CommandType.StoredProcedure
                Dim guidSessionID As New System.Guid(mUserID)
                cmdSql.Parameters.Add("@UserID", Me.mobjUser.ImagesUserId)
                cmdSql.Parameters.Add("@FavName", FavName)

                consql.Open()
                cmdSql.ExecuteNonQuery()

                cmdSql.Dispose()
            End If

        Catch ex As Exception
            'Throw New System.Web.Services.Protocols.SoapException("Unable to login user!. " & ex.Message, System.Xml.XmlQualifiedName.Empty, "text")
            Return False
        End Try
        consql.Dispose()


        Return True

    End Function


    <WebMethod(EnableSession:=True)> _
    Public Function IndexerFavorites(ByVal mUserID As String, ByVal mAccountID As String, ByVal mAuthenticationKey As String) As DataSet

        Dim mSessionID As String
        Dim intReturn As Integer = 0
        Dim consql As New SqlClient.SqlConnection
        Dim dsReturn As New DataSet
        Dim da As New SqlClient.SqlDataAdapter
        Dim objEncryption As New Encryption.Encryption


        Try
            'Get the users current sessionid
            Dim consqluser As New SqlConnection

            consqluser.ConnectionString = objEncryption.Decrypt(ConfigurationSettings.AppSettings("Connection"))
            If Not consqluser.State = ConnectionState.Open Then consqluser.Open()
            Dim dtUser As New DataTable
            Dim cmdUSql As New SqlClient.SqlCommand("SELECT SessionID FROM USERS WHERE UserID = @UserID", consqluser)
            cmdUSql.Parameters.Add("@UserID", mUserID)
            cmdUSql.CommandTimeout = 120
            Dim daSql As New SqlClient.SqlDataAdapter(cmdUSql)
            daSql.Fill(dtUser)

            If dtUser.Rows.Count <= 0 Then
                Return New DataSet
                Exit Function
            Else
                mSessionID = dtUser.Rows(0).Item("SessionID")
            End If


            '
            ' Create user objects
            '

            Me.mobjUser = BuildUserObject(mSessionID, mAccountID)

            '
            ' Get the page count
            ' 
            If Not Me.mobjUser Is Nothing Then
                consql = BuildConnection(Me.mobjUser)
                Dim cmdSql As New SqlClient.SqlCommand("acsGetIndexerFavorite", consql)
                cmdSql.CommandType = CommandType.StoredProcedure
                Dim guidSessionID As New System.Guid(mUserID)
                cmdSql.Parameters.Add("@UserID", Me.mobjUser.ImagesUserId)
                consql.Open()
                da = New SqlClient.SqlDataAdapter(cmdSql)
                da.Fill(dsReturn)
                cmdSql.Dispose()
            End If

        Catch ex As Exception
            Throw New System.Web.Services.Protocols.SoapException("Unable to login user!. " & ex.Message, System.Xml.XmlQualifiedName.Empty, "text")
        End Try
        consql.Dispose()


        Return dsReturn

    End Function


    <WebMethod(EnableSession:=True)> _
        Public Function IndexerOptions(ByVal mUserID As String, ByVal mAccountID As String, ByVal mAuthenticationKey As String) As DataSet

        Dim mSessionID As String
        Dim intReturn As Integer = 0
        Dim consql As New SqlClient.SqlConnection
        Dim dsReturn As New DataSet
        Dim da As New SqlClient.SqlDataAdapter
        Dim objEncryption As New Encryption.Encryption


        Try
            'Get the users current sessionid
            Dim consqluser As New SqlConnection

            consqluser.ConnectionString = objEncryption.Decrypt(ConfigurationSettings.AppSettings("Connection"))
            If Not consqluser.State = ConnectionState.Open Then consqluser.Open()
            Dim dtUser As New DataTable
            Dim cmdUSql As New SqlClient.SqlCommand("SELECT SessionID FROM USERS WHERE UserID = @UserID", consqluser)
            cmdUSql.Parameters.Add("@UserID", mUserID)
            cmdUSql.CommandTimeout = 120
            Dim daSql As New SqlClient.SqlDataAdapter(cmdUSql)
            daSql.Fill(dtUser)

            If dtUser.Rows.Count <= 0 Then
                Return New DataSet
                Exit Function
            Else
                mSessionID = dtUser.Rows(0).Item("SessionID")
            End If


            '
            ' Create user objects
            '

            Me.mobjUser = BuildUserObject(mSessionID, mAccountID)

            '
            ' Get the page count
            ' 
            If Not Me.mobjUser Is Nothing Then
                consql = BuildConnection(Me.mobjUser)
                Dim cmdSql As New SqlClient.SqlCommand("acsGetIndexerValue", consql)
                cmdSql.CommandType = CommandType.StoredProcedure
                Dim guidSessionID As New System.Guid(mUserID)
                cmdSql.Parameters.Add("@UserID", Me.mobjUser.ImagesUserId)
                consql.Open()
                da = New SqlClient.SqlDataAdapter(cmdSql)
                da.Fill(dsReturn)
                cmdSql.Dispose()
            End If

        Catch ex As Exception
            Throw New System.Web.Services.Protocols.SoapException("Unable to login user!. " & ex.Message, System.Xml.XmlQualifiedName.Empty, "text")
        End Try
        consql.Dispose()


        Return dsReturn

    End Function


    <WebMethod(EnableSession:=True)> _
   Public Function IndexerSaveOption(ByVal mUserID As String, ByVal mAccountID As String, ByVal mAuthenticationKey As String, ByVal OptionKey As String, ByVal OptionValue As String) As Boolean

        Dim mSessionID As String
        Dim intReturn As Integer = 0
        Dim consql As New SqlClient.SqlConnection
        Dim dsReturn As New DataSet
        Dim da As New SqlClient.SqlDataAdapter
        Dim objEncryption As New Encryption.Encryption


        Try
            'Get the users current sessionid
            Dim consqluser As New SqlConnection

            consqluser.ConnectionString = objEncryption.Decrypt(ConfigurationSettings.AppSettings("Connection"))
            If Not consqluser.State = ConnectionState.Open Then consqluser.Open()
            Dim dtUser As New DataTable
            Dim cmdUSql As New SqlClient.SqlCommand("SELECT SessionID FROM USERS WHERE UserID = @UserID", consqluser)
            cmdUSql.Parameters.Add("@UserID", mUserID)
            cmdUSql.CommandTimeout = 120
            Dim daSql As New SqlClient.SqlDataAdapter(cmdUSql)
            daSql.Fill(dtUser)

            If dtUser.Rows.Count <= 0 Then
                Return False
                Exit Function
            Else
                mSessionID = dtUser.Rows(0).Item("SessionID")
            End If


            '
            ' Create user objects
            '

            Me.mobjUser = BuildUserObject(mSessionID, mAccountID)

            '
            ' Get the page count
            ' 
            If Not Me.mobjUser Is Nothing Then
                consql = BuildConnection(Me.mobjUser)
                Dim cmdSql As New SqlClient.SqlCommand("acsSetIndexerValue", consql)
                cmdSql.CommandType = CommandType.StoredProcedure
                Dim guidSessionID As New System.Guid(mUserID)
                cmdSql.Parameters.Add("@UserID", Me.mobjUser.ImagesUserId)
                cmdSql.Parameters.Add("@OptionKey", OptionKey)
                cmdSql.Parameters.Add("@OptionValue", OptionValue)
                consql.Open()
                cmdSql.ExecuteNonQuery()
                cmdSql.Dispose()
            End If

        Catch ex As Exception
            Return False
            Throw New System.Web.Services.Protocols.SoapException("Unable to login user!. " & ex.Message, System.Xml.XmlQualifiedName.Empty, "text")
        End Try
        consql.Dispose()


        Return True

    End Function

    <WebMethod(EnableSession:=True)> _
       Public Function WizardConfiguration(ByVal mUserID As String, ByVal mAccountID As String, ByVal mAuthenticationKey As String) As DataSet

        Dim mSessionID As String
        Dim intReturn As Integer = 0
        Dim consql As New SqlClient.SqlConnection
        Dim dsReturn As New DataSet
        Dim da As New SqlClient.SqlDataAdapter
        Dim objEncryption As New Encryption.Encryption

        Try
            'Get the users current sessionid
            Dim consqluser As New SqlConnection

            consqluser.ConnectionString = objEncryption.Decrypt(ConfigurationSettings.AppSettings("Connection"))
            If Not consqluser.State = ConnectionState.Open Then consqluser.Open()
            Dim dtUser As New DataTable
            Dim cmdUSql As New SqlClient.SqlCommand("SELECT SessionID FROM USERS WHERE UserID = @UserID", consqluser)
            cmdUSql.Parameters.Add("@UserID", mUserID)
            cmdUSql.CommandTimeout = 120
            Dim daSql As New SqlClient.SqlDataAdapter(cmdUSql)
            daSql.Fill(dtUser)

            If dtUser.Rows.Count <= 0 Then
                Return New DataSet
                Exit Function
            Else
                mSessionID = dtUser.Rows(0).Item("SessionID")
            End If

            Me.mobjUser = BuildUserObject(mSessionID, mAccountID)

            If Not Me.mobjUser Is Nothing Then
                consql = BuildConnection(Me.mobjUser)
                Dim cmdSql As New SqlClient.SqlCommand("acsWizardData", consql)
                cmdSql.CommandType = CommandType.StoredProcedure
                Dim guidSessionID As New System.Guid(mUserID)
                cmdSql.Parameters.Add("@UserID", guidSessionID)
                cmdSql.Parameters.Add("@AccountID", Me.mobjUser.AccountId)
                consql.Open()
                da = New SqlClient.SqlDataAdapter(cmdSql)
                da.Fill(dsReturn)
                cmdSql.Dispose()
            End If

            ' Add account settings table
            Dim dtAccountSettings As New DataTable("AccountSettings")
            dtAccountSettings.Columns.Add(New DataColumn("DocTypeAttributes", GetType(System.Boolean), Nothing, MappingType.Element))
            dtAccountSettings.Columns.Add(New DataColumn("Annotation", GetType(System.Boolean), Nothing, MappingType.Element))
            dtAccountSettings.Columns.Add(New DataColumn("Redaction", GetType(System.Boolean), Nothing, MappingType.Element))
            dtAccountSettings.Columns.Add(New DataColumn("Templates", GetType(System.Boolean), Nothing, MappingType.Element))
            dtAccountSettings.Columns.Add(New DataColumn("Integration", GetType(System.Boolean), Nothing, MappingType.Element))

            Dim drSettings As DataRow
            drSettings = dtAccountSettings.NewRow
            drSettings("DocTypeAttributes") = Functions.ModuleSetting(Functions.ModuleCode.DOCUMENT_TYPES_ATTRIBUTES, Me.mobjUser)
            drSettings("Annotation") = Functions.ModuleSetting(Functions.ModuleCode.ANNOTATION, Me.mobjUser)
            drSettings("Redaction") = Functions.ModuleSetting(Functions.ModuleCode.REDACTION, Me.mobjUser)
            drSettings("Templates") = Functions.ModuleSetting(Functions.ModuleCode.TEMPLATES, Me.mobjUser)
            drSettings("Integration") = Functions.ModuleSetting(Functions.ModuleCode.APP_INTEGRATION, Me.mobjUser)
            dtAccountSettings.Rows.Add(drSettings)

            dsReturn.Tables.Add(dtAccountSettings)

        Catch ex As Exception

            Throw New System.Web.Services.Protocols.SoapException("Unable to login user!. " & ex.Message, System.Xml.XmlQualifiedName.Empty, "text")

        End Try

        consql.Dispose()

        Return dsReturn

    End Function

    <WebMethod(EnableSession:=True)> _
   Public Function ScanConfig(ByVal mUserID As String, ByVal mAccountID As String, ByVal mAuthenticationKey As String) As DataSet

        Dim mSessionID As String
        Dim intReturn As Integer = 0
        Dim consql As New SqlClient.SqlConnection
        Dim dsReturn As New DataSet("Config")
        Dim da As New SqlClient.SqlDataAdapter
        Dim objEncryption As New Encryption.Encryption

        Try
            'Get the users current sessionid
            Dim consqluser As New SqlConnection

            consqluser.ConnectionString = objEncryption.Decrypt(ConfigurationSettings.AppSettings("Connection"))
            If Not consqluser.State = ConnectionState.Open Then consqluser.Open()
            Dim dtUser As New DataTable
            Dim cmdUSql As New SqlClient.SqlCommand("SELECT SessionID FROM USERS WHERE UserID = @UserID", consqluser)
            cmdUSql.Parameters.Add("@UserID", mUserID)
            cmdUSql.CommandTimeout = 120
            Dim daSql As New SqlClient.SqlDataAdapter(cmdUSql)
            daSql.Fill(dtUser)

            If dtUser.Rows.Count <= 0 Then
                Return Nothing
                Exit Function
            Else
                mSessionID = dtUser.Rows(0).Item("SessionID")
            End If

            Me.mobjUser = BuildUserObject(mSessionID, mAccountID)

            If Not Me.mobjUser Is Nothing Then
                consql = BuildConnection(Me.mobjUser)
                Dim cmdSql As New SqlClient.SqlCommand("SMPanelConfig", consql)
                cmdSql.CommandType = CommandType.StoredProcedure
                Dim guidSessionID As New System.Guid(mUserID)
                cmdSql.Parameters.Add("@UserID", guidSessionID)
                cmdSql.Parameters.Add("@AccountID", Me.mobjUser.AccountId)
                consql.Open()
                da = New SqlClient.SqlDataAdapter(cmdSql)
                da.Fill(dsReturn)
                cmdSql.Dispose()
            End If

            '' Add account settings table
            'Dim dtAccountSettings As New DataTable("AccountSettings")
            'dtAccountSettings.Columns.Add(New DataColumn("DocTypeAttributes", GetType(System.Boolean), Nothing, MappingType.Element))
            'dtAccountSettings.Columns.Add(New DataColumn("Annotation", GetType(System.Boolean), Nothing, MappingType.Element))
            'dtAccountSettings.Columns.Add(New DataColumn("Redaction", GetType(System.Boolean), Nothing, MappingType.Element))
            'dtAccountSettings.Columns.Add(New DataColumn("Templates", GetType(System.Boolean), Nothing, MappingType.Element))
            'dtAccountSettings.Columns.Add(New DataColumn("Integration", GetType(System.Boolean), Nothing, MappingType.Element))

            'Dim drSettings As DataRow
            'drSettings = dtAccountSettings.NewRow
            'drSettings("DocTypeAttributes") = Functions.ModuleSetting(Functions.ModuleCode.DOCUMENT_TYPES_ATTRIBUTES, Me.mobjUser)
            'drSettings("Annotation") = Functions.ModuleSetting(Functions.ModuleCode.ANNOTATION, Me.mobjUser)
            'drSettings("Redaction") = Functions.ModuleSetting(Functions.ModuleCode.REDACTION, Me.mobjUser)
            'drSettings("Templates") = Functions.ModuleSetting(Functions.ModuleCode.TEMPLATES, Me.mobjUser)
            'drSettings("Integration") = Functions.ModuleSetting(Functions.ModuleCode.APP_INTEGRATION, Me.mobjUser)
            'dtAccountSettings.Rows.Add(drSettings)

            'dsReturn.Tables.Add(dtAccountSettings)

            consql.Dispose()

            dsReturn.Tables(0).TableName = "Documents"
            dsReturn.Tables(1).TableName = "Attributes"
            dsReturn.Tables(2).TableName = "Categories"
            dsReturn.Tables(3).TableName = "Workflows"
            dsReturn.Tables(4).TableName = "Queues"
            dsReturn.Tables(5).TableName = "Groups"
            dsReturn.Tables(6).TableName = "IntegrationHeader"
            dsReturn.Tables(7).TableName = "IntegrationDetail"
            dsReturn.Tables(8).TableName = "FolderMappings"
            dsReturn.Tables(9).TableName = "Preferences"

            'Dim tmpXMLFile As String = Server.MapPath("./tmp/") & System.Guid.NewGuid.ToString.Replace("-", "") & ".xml"

            'dsReturn.WriteXml(tmpXMLFile)

            'Return System.IO.Path.GetFileName(tmpXMLFile)

            Return dsReturn

        Catch ex As Exception

            Throw New System.Web.Services.Protocols.SoapException("Unable to login user!. " & ex.Message, System.Xml.XmlQualifiedName.Empty, "text")

        End Try



    End Function

    '<WebMethod(EnableSession:=True)> _
    'Public Function TestSave(ByVal strFileName As String, ByVal docbinaryarray As Byte()) As String
    '    Dim intSeqTotal As Integer
    '    Try
    '        Dim strSavedocPath As String = Server.MapPath("/tmp/") & strFileName & ".tif"
    '        Dim objfilestream As FileStream = New FileStream(strSavedocPath, FileMode.Create, FileAccess.ReadWrite)
    '        objfilestream.Write(docbinaryarray, 0, docbinaryarray.Length)
    '        objfilestream.Close()
    '        Dim ix7 As New PegasusImaging.WinForms.ImagXpress7.PICImagXpress
    '        intSeqTotal = ix7.NumPages(strSavedocPath)
    '    Catch ex As Exception
    '        Return ex.Message
    '    End Try

    '    Return "Pages: " & intSeqTotal

    'End Function

    <WebMethod(EnableSession:=True)> _
    Public Function SaveNewImage(ByVal mUserID As String, ByVal mAccountID As String, ByVal iDocumentID As Integer, ByVal dsAttributes As DataSet, ByVal docbinaryarray As Byte(), _
    ByVal blnCompression As Boolean, ByVal intVersionID As Integer, ByVal blnAppendPages As Boolean, ByVal iPageInsert As Integer, ByVal blnPDF As Boolean, ByVal iQuality As Integer, _
    ByVal strSaveType As String) As String

        Try

            If strSaveType.Trim.Length = 0 Then strSaveType = "Web Indexer"

            blnPDF = False 'This reelase takes only TIF's. Conversion happens client side.

            Dim mSessionID As String
            Dim objEncryption As New Encryption.Encryption

            'Get the users current sessionid
            Dim consqluser As New SqlConnection
            consqluser.ConnectionString = objEncryption.Decrypt(ConfigurationSettings.AppSettings("Connection"))
            If Not consqluser.State = ConnectionState.Open Then consqluser.Open()
            Dim dtUser As New DataTable
            Dim cmdUSql As New SqlClient.SqlCommand("SELECT SessionID FROM USERS WHERE UserID = @UserID", consqluser)
            cmdUSql.Parameters.Add("@UserID", mUserID)
            cmdUSql.CommandTimeout = 120
            Dim daSql As New SqlClient.SqlDataAdapter(cmdUSql)
            daSql.Fill(dtUser)

            If dtUser.Rows.Count <= 0 Then
                Return -1
                Exit Function
            Else
                mSessionID = dtUser.Rows(0).Item("SessionID")
            End If

            ''If compression is passed decompress image
            'If blnCompression = True Then
            '    docbinaryarray = DecompressImage(docbinaryarray)
            'End If

            'dsImageData expected to have attributes... if empty then the presumption is no attributes for document.

            Dim consql As New SqlClient.SqlConnection

            Me.mobjUser = BuildUserObject(mSessionID, mAccountID)

            Dim strtempfilename As New System.Guid
            If Not Me.mobjUser Is Nothing Then
                consql = BuildConnection(Me.mobjUser)
                'First save file stream
                'Dim strSavedocPath As String = System.IO.Path.GetTempFileName

#If DEBUG Then
                Dim strSavedocPath As String = Server.MapPath("/docassistweb1/tmp/") & strtempfilename.NewGuid.ToString() & ".tif"
#Else
                Dim strSavedocPath As String = Server.MapPath("/tmp/") & strtempfilename.NewGuid.ToString() & ".tif"
#End If

                Try
                    If System.IO.File.Exists(strSavedocPath) = True Then System.IO.File.Delete(strSavedocPath)
                    Dim objfilestream As FileStream = New FileStream(strSavedocPath, FileMode.Create, FileAccess.ReadWrite)
                    objfilestream.Write(docbinaryarray, 0, docbinaryarray.Length)
                    objfilestream.Close()
                Catch ex As Exception
                    'Return intdebugger
                    Return ex.Message & Chr(13) & ex.StackTrace
                    Return -2
                End Try


                'Convert PDF if needed
                'If blnPDF = True Then
                '    strSavedocPath = Replace(strSavedocPath, ".tif", ".pdf")
                'End If

                'strSavedocPath = objPdf.SaveSetQuality(strSavedocPath, iQuality)
                'strSavedocPath = objPdf.GetTempPage(strSavedocPath, 1, docAssist.Imaging.docPdf.Quality.NORMAL_G4, strSavedocPath)

                Dim objSave As New Accucentric.docAssist.Data.ImageSave(Me.mobjUser.ImagesUserId, consql)

                If intVersionID <> 0 Then
                    Dim dsNewVersion As New Data.dsVersionControl
                    dsNewVersion = Data.GetVersionControl(intVersionID, consql)
                    'dsNewVersion.ImageVersionDetail.Rows(0).Item("VersionNum") = 0

                    'If blnAppendPages is true we are appending pages so we dont want to delete existing pages
                    Dim dtOriginal As New DataTable
                    Dim drOriginal As DataRow

                    'If blnAppendPages = True Then
                    '    dtOriginal = CType(dsNewVersion.ImageVersionDetail, DataTable)
                    'End If

                    If blnAppendPages = False Then
                        For Each dr As Data.dsVersionControl.ImageVersionDetailRow In dsNewVersion.ImageVersionDetail
                            dr.Delete()
                        Next
                    End If

                    'Readd original pages
                    'If blnAppendPages = True Then
                    '    For Each drOriginal In dtOriginal.Rows
                    '        dsNewVersion.ImageVersionDetail.Rows.Add(CType(drOriginal, Data.dsVersionControl.ImageVersionDetailRow))
                    '    Next
                    'End If

                    Dim drVersion As Data.dsVersionControl.ImageVersionRow
                    drVersion = dsNewVersion.ImageVersion.Rows(0)

                    'Dim ix As New PegasusImaging.WinForms.ImagXpress7.PICImagXpress
                    'ix.FileName = strSavedocPath

                    Dim iPageStart As Integer = 1

                    If iPageInsert > dsNewVersion.ImageVersionDetail.Rows.Count Then iPageInsert = (dsNewVersion.ImageVersionDetail.Rows.Count + 1)

                    ' Create new image details
                    Dim iTotalPages As Integer = Functions.PageCount(strSavedocPath)
                    Dim iOriginalPages As Integer = dsNewVersion.ImageDetail.Rows.Count
                    'If blnAppendPages = True Then
                    '    iPageStart = iPageInsert
                    'End If
                    If blnAppendPages = True Then
                        iPageStart = iPageInsert 'dsNewVersion.ImageDetail.Rows.Count + 1
                        iTotalPages = iTotalPages ' dsNewVersion.ImageDetail.Rows.Count + ix.Pages

                    Else
                        iTotalPages = iTotalPages

                    End If



                    'Beginning of document
                    'If iPageStart = 1 Then iPageStart = 0
                    If iPageStart = 0 Then
                        If iPageInsert <= drVersion.SeqTotal Then
                            'Create space for the new pages
                            Dim iTotalPagesToInsert As Integer = iTotalPages
                            If iTotalPagesToInsert > 0 Then
                                Dim iCount As Integer

                                For iCount = (dsNewVersion.ImageVersionDetail.Rows.Count) To (iPageInsert + 1) Step -1
                                    dsNewVersion.ImageVersionDetail.Rows(iCount - 1).Item("SeqID") = dsNewVersion.ImageVersionDetail.Rows(iCount - 1).Item("SeqID") + iTotalPagesToInsert
                                Next
                                'iPageStart = (iPageInsert + 1)
                            End If
                        End If
                    End If

                    'In between document
                    If iPageStart < (iOriginalPages + 1) And iPageStart <> 0 And blnAppendPages = True Then
                        If iPageInsert <= drVersion.SeqTotal Then
                            'Create space for the new pages
                            Dim iTotalPagesToInsert As Integer = iTotalPages
                            If iTotalPagesToInsert > 0 Then
                                Dim iCount As Integer

                                For iCount = (dsNewVersion.ImageVersionDetail.Rows.Count) To (iPageInsert) Step -1
                                    dsNewVersion.ImageVersionDetail.Rows(iCount - 1).Item("SeqID") = dsNewVersion.ImageVersionDetail.Rows(iCount - 1).Item("SeqID") + iTotalPagesToInsert
                                Next
                                'iPageStart = (iPageInsert + 1)
                            End If
                        End If
                    End If

                    If blnAppendPages = False Then iPageStart = 0

                    'Beginning of document
                    If iPageStart = 0 Then
                        For intCount As Integer = (iPageStart + 1) To iTotalPages
                            Dim drImageDetail As Data.dsVersionControl.ImageDetailRow = dsNewVersion.ImageDetail.NewRow
                            dsNewVersion.ImageDetail.Rows.Add(drImageDetail)
                            Dim drImageVersionDetail As Data.dsVersionControl.ImageVersionDetailRow = dsNewVersion.ImageVersionDetail.NewRow()
                            drImageVersionDetail.ImportFileName = strSavedocPath
                            drImageVersionDetail.ImportPageNo = intCount '- iOriginalPages
                            drImageVersionDetail.SeqID = (iPageInsert) + intCount
                            drImageVersionDetail.VersionID = drVersion.VersionID
                            drImageVersionDetail.ImageDetID = drImageDetail.ImageDetID
                            dsNewVersion.ImageVersionDetail.Rows.Add(drImageVersionDetail)
                        Next
                    End If

                    'In between document
                    If iPageStart < (iOriginalPages + 1) And iPageStart <> 0 And blnAppendPages = True Then
                        For intCount As Integer = iPageStart To (iPageStart + iTotalPages) - 1
                            Dim drImageDetail As Data.dsVersionControl.ImageDetailRow = dsNewVersion.ImageDetail.NewRow
                            dsNewVersion.ImageDetail.Rows.Add(drImageDetail)
                            Dim drImageVersionDetail As Data.dsVersionControl.ImageVersionDetailRow = dsNewVersion.ImageVersionDetail.NewRow()
                            drImageVersionDetail.ImportFileName = strSavedocPath
                            drImageVersionDetail.ImportPageNo = (intCount - iPageStart) + 1
                            drImageVersionDetail.SeqID = intCount
                            drImageVersionDetail.VersionID = drVersion.VersionID
                            drImageVersionDetail.ImageDetID = drImageDetail.ImageDetID
                            dsNewVersion.ImageVersionDetail.Rows.Add(drImageVersionDetail)
                        Next
                    End If

                    'End of document
                    If iPageStart = (iOriginalPages + 1) Then
                        For intCount As Integer = iPageStart To ((iPageStart + iTotalPages) - 1)
                            Dim drImageDetail As Data.dsVersionControl.ImageDetailRow = dsNewVersion.ImageDetail.NewRow
                            dsNewVersion.ImageDetail.Rows.Add(drImageDetail)
                            Dim drImageVersionDetail As Data.dsVersionControl.ImageVersionDetailRow = dsNewVersion.ImageVersionDetail.NewRow()
                            drImageVersionDetail.ImportFileName = strSavedocPath
                            drImageVersionDetail.ImportPageNo = intCount - iOriginalPages  '- iOriginalPages
                            drImageVersionDetail.SeqID = intCount
                            drImageVersionDetail.VersionID = drVersion.VersionID
                            drImageVersionDetail.ImageDetID = drImageDetail.ImageDetID
                            dsNewVersion.ImageVersionDetail.Rows.Add(drImageVersionDetail)
                        Next
                    End If

                    Dim iNewVersionID As Integer
                    iNewVersionID = objSave.UpdateImage(dsNewVersion, , , , , , False)

                    'Log recent item
                    Try
                        Dim blnReturn As Boolean = Functions.TrackRecentItem(Me.mobjUser, "SV", inewversionid)
                        If Not blnReturn Then
                            'error logging recent item
                        End If
                    Catch ex As Exception
                    End Try

                    Return iNewVersionID

                Else 'Save new image

                    Dim objVersion As New Accucentric.docAssist.Data.VersionControl(intVersionID, consql)
                    Dim drImageVersionDetail As ImageVersionDetailRow
                    Dim drImageAttributes As ImageAttributesRow

                    If dsAttributes.Tables.Count > 0 Then
                        Dim drAttributes As DataRow
                        For Each drAttributes In dsAttributes.Tables(0).Rows
                            drImageAttributes = objVersion.ImageAttributes.NewRow
                            drImageAttributes.AttributeId = drAttributes("AttributeID")
                            drImageAttributes.AttributeValue = drAttributes("AttributeValue")
                            objVersion.ImageAttributes.Rows.Add(drImageAttributes)
                        Next
                    End If

                    Dim iNewVersionID As Integer = objSave.SaveNewImage(strSavedocPath, iDocumentID, objVersion.ImageAttributes, Me.mobjUser, ConfigurationSettings.AppSettings("Connection"), _
                    strSaveType, , dsAttributes.Tables(2), dsAttributes.Tables(1))

                    'Log recent item
                    Try
                        Dim blnReturn As Boolean = Functions.TrackRecentItem(Me.mobjUser, "SV", inewversionid)
                        If Not blnReturn Then
                            'error logging recent item
                        End If
                    Catch ex As Exception
                    End Try

                    Return iNewVersionID

                End If

            End If

        Catch ex As Exception
            Return ex.Message & vbCr & ex.StackTrace
            'Throw ex
        End Try


    End Function

    Private Function SaveWorkflow(ByVal intVersionId As Integer, ByVal dtWorkflow As DataTable, ByVal consql As SqlConnection) As Boolean

        If dtWorkflow.Rows.Count <= 0 Then Exit Function
        Dim intRouteID, intPriority, intRoleRouteId As Integer
        Dim strComments As String
        intRouteID = dtWorkflow.Rows(0).Item("RouteID")
        intPriority = dtWorkflow.Rows(0).Item("Priority")
        intRoleRouteId = dtWorkflow.Rows(0).Item("RoleRouteID")
        strComments = dtWorkflow.Rows(0).Item("Comments")

        ' Get the existing Version Routes
        '
        Dim dtVersionRoutes As New Data.Images.ImageVersionRoutes
        dtVersionRoutes.FillByVersionId(intVersionId, 0, consql)
        Dim trnSql As SqlClient.SqlTransaction
        Try
            '
            ' Check to see if there are existing routes
            '
            Dim dtVersionApprovals As Data.Images.ImageVersionApprovals
            Dim drVersionApprovals As Data.Images.ImageVersionApprovalsRow
            If dtVersionRoutes.Rows.Count > 0 Then
                dtVersionApprovals = New Data.Images.ImageVersionApprovals
                For Each drVersionRoute As Data.Images.ImageVersionRoutesRow In dtVersionRoutes.Rows
                    dtVersionApprovals.FillByVersionRouteId(drVersionRoute.VersionRouteId, consql)
                Next
                Dim dvPendingApprovals As New DataView(dtVersionApprovals, "Convert(Isnull(" & dtVersionApprovals.ActionDateTime.ColumnName & ",'NULL'), System.String) = 'NULL'", Nothing, DataViewRowState.CurrentRows)
                For Each dvr As DataRowView In dvPendingApprovals
                    drVersionApprovals = dvr.Row
                    drVersionApprovals.ActionDateTime = Now()
                    drVersionApprovals.ApprovalAction = Data.Images.ImageVersionApprovalAction.Cancel
                Next
            End If
            '
            ' Get the route detail
            '
            Dim dtRouteDetail As New Data.Images.WFRouteDetail
            dtRouteDetail.FillByRouteId(intRouteID, consql, mobjUser.AccountId)
            '
            ' Add the new route
            '
            Dim drVersionRoutes As Data.Images.ImageVersionRoutesRow = dtVersionRoutes.NewRow
            drVersionRoutes.VersionId = intVersionId
            drVersionRoutes.RoleRouteId = intRoleRouteId
            drVersionRoutes.AddedDate = Now()
            drVersionRoutes.AddedUser = Me.mobjUser.ImagesUserId
            drVersionRoutes.Priority = intPriority
            dtVersionRoutes.Rows.Add(drVersionRoutes)
            trnSql = consql.BeginTransaction()
            dtVersionRoutes.Update(consql, trnSql)
            Dim intVersionRouteId = drVersionRoutes.VersionRouteId
            '
            ' Add the route detail
            '
            If dtVersionApprovals Is Nothing Then dtVersionApprovals = New Data.Images.ImageVersionApprovals
            '
            ' Submitting user
            drVersionApprovals = dtVersionApprovals.NewRow
            drVersionApprovals.VersionRouteId = intVersionRouteId
            drVersionApprovals.VersionRouteId = intVersionRouteId
            drVersionApprovals.UserId = Me.mobjUser.ImagesUserId
            drVersionApprovals.ApprovalAction = Data.Images.ImageVersionApprovalAction.Approved
            drVersionApprovals.ActionDateTime = Now()
            If strComments.Trim.Length > 0 Then
                drVersionApprovals.Comments = strComments.Trim
            End If
            dtVersionApprovals.Rows.Add(drVersionApprovals)
            '
            ' route detail approvals
            Dim bolIsFirst As Boolean = True
            For Each drRouteDetail As Data.Images.WFRouteDetailRow In dtRouteDetail.Rows
                If bolIsFirst Then
                    If Not drRouteDetail.UserID = Me.mobjUser.ImagesUserId Then bolIsFirst = False
                End If
                If Not bolIsFirst Then
                    drVersionApprovals = dtVersionApprovals.NewRow
                    drVersionApprovals.VersionRouteId = intVersionRouteId
                    drVersionApprovals.RouteDetId = drRouteDetail.RouteDetID
                    drVersionApprovals.UserId = drRouteDetail.UserID
                    dtVersionApprovals.Rows.Add(drVersionApprovals)
                End If
                bolIsFirst = False
            Next
            dtVersionApprovals.Update(consql, trnSql)

            'VersionRouteId.Value = drVersionRoutes.VersionRouteId

            trnSql.Commit()

        Catch ex As Exception
            If Not trnSql Is Nothing Then trnSql.Rollback()
            Throw ex
        Finally
            If Not consql.State = ConnectionState.Closed Then consql.Close()
        End Try


    End Function

    Private Function DecompressImage(ByVal bytes() As Byte) As Byte()
        Dim mem As New MemoryStream(bytes)
        Dim unzipper As New ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream(mem)

        Try
            unzipper.Flush()

            Return mem.ToArray
        Catch ex As Exception
            Throw ex
        Finally
            unzipper.Close()
            mem.Close()
        End Try
    End Function

    <WebMethod(EnableSession:=True)> _
    Public Function CheckForMatches(ByVal mUserID As String, ByVal mAccountID As String, ByVal dsMatchData As DataSet, ByVal iDocumentID As Integer) As DataSet
        'Get the users current sessionid
        Dim mSessionID As String
        Dim consqluser As New SqlConnection
        Dim objEncryption As New Encryption.Encryption

        consqluser.ConnectionString = objEncryption.Decrypt(ConfigurationSettings.AppSettings("Connection"))
        If Not consqluser.State = ConnectionState.Open Then consqluser.Open()
        Dim dtUser As New DataTable
        Dim cmdUSql As New SqlClient.SqlCommand("SELECT SessionID FROM USERS WHERE UserID = @UserID", consqluser)
        cmdUSql.Parameters.Add("@UserID", mUserID)
        cmdUSql.CommandTimeout = 120
        Dim daSql As New SqlClient.SqlDataAdapter(cmdUSql)
        daSql.Fill(dtUser)

        If dtUser.Rows.Count <= 0 Then
            Return New DataSet
            Exit Function
        Else
            mSessionID = dtUser.Rows(0).Item("SessionID")
        End If



        Me.mobjUser = BuildUserObject(mSessionID, mAccountID)

        If Not Me.mobjUser Is Nothing Then
            Return GetSearchResults(dsMatchData, Me.mobjUser, iDocumentID, False, Me.mobjUser.ImagesUserId)
        End If

    End Function


    Private Function GetSearchResults(ByRef dtMatchData As DataSet, ByRef objUser As Web.Security.User, ByVal iDocumentID As Integer, _
    Optional ByVal blnOrphans As Boolean = False, _
    Optional ByVal intUserID As Integer = 0, Optional ByVal intSort As Integer = 10020, Optional ByVal intSortDirection As Integer = 1, _
    Optional ByVal intPageNumber As Integer = 1, Optional ByRef intShowWfColumn As Integer = 0, Optional ByRef dtSearchAttributes As DataTable = Nothing) As DataSet

        Dim conSql As SqlClient.SqlConnection
        Dim rValue As Integer = 0
        Dim dsAttributes As DataSet = dtMatchData 'Session("SearchAttributes")
        Dim dtDocuments As New DataTable("Documents") '= dsAttributes.Tables("Documents")
        Dim dtAttributes As New DataTable("Attributes") '= dsAttributes.Tables("Attributes")
        '
        'Build Documents table
        dtDocuments.Columns.Add(New DataColumn("DocumentId", SqlDbType.Int.GetType))
        '
        ' Build Attributes Table
        dtAttributes.Columns.Add(New DataColumn("AttributeId", SqlDbType.Int.GetType))
        dtAttributes.Columns.Add(New DataColumn("AttributeValue", Type.GetType("System.String")))
        '
        ' Get the Documents
        Dim drRowSession, drRow As DataRow, drCol As DataColumn
        'For Each drRowSession In dsAttributes.Tables("Documents").Rows
        drRow = dtDocuments.NewRow

        drRow("DocumentID") = iDocumentID

        dtDocuments.Rows.Add(drRow)

        '
        ' Get the attributes
        If dsAttributes.Tables.Count > 0 Then
            For Each drRowSession In dsAttributes.Tables("ImageAttributes").Rows
                drRow = dtAttributes.NewRow
                drRow("AttributeID") = drRowSession("AttributeID")
                drRow("AttributeValue") = drRowSession("AttributeValue")
                dtAttributes.Rows.Add(drRow)
            Next
        End If
        '
        ' Get they keywords
        Dim strKeywords As String


        Dim bolCloseCon As Boolean = False
        Try

            If conSql Is Nothing Then
                conSql = Web.Security.BuildConnection(objUser)
            End If
            If Not conSql.State = ConnectionState.Open Then
                conSql.Open()
                bolCloseCon = True
            End If

            ' Build tables
            ' replace '-' in guid string
            Dim strDoc As String = System.Guid.NewGuid().ToString().Replace("-", "")
            Dim strAttributes As String = System.Guid.NewGuid().ToString().Replace("-", "")

            Dim sb As New System.Text.StringBuilder
            sb.Append("CREATE TABLE ##" & strDoc.ToString & " (DocumentId int)" & vbCrLf)
            sb.Append("CREATE TABLE ##" & strAttributes & " (AttributeId int, AttributeValue varchar(75))" & vbCrLf)

            Dim sqlcmd As New SqlCommand(sb.ToString, conSql)
            Dim sqlDa As SqlDataAdapter
            Dim sqlBld As SqlCommandBuilder

            sqlcmd.ExecuteNonQuery()

            ' Add documents
            sqlcmd = New SqlCommand("SELECT * FROM ##" & strDoc, conSql)
            sqlDa = New SqlDataAdapter(sqlcmd)
            sqlBld = New SqlCommandBuilder(sqlDa)

            sqlDa.Update(dtDocuments)

            ' Add Attributes
            sqlcmd = New SqlCommand("SELECT * FROM ##" & strAttributes, conSql)
            sqlDa = New SqlDataAdapter(sqlcmd)
            sqlBld = New SqlCommandBuilder(sqlDa)

            If Not blnOrphans Then sqlDa.Update(dtAttributes)

            sqlcmd = New SqlCommand("acsSearchResults", conSql)
            sqlcmd.CommandTimeout = 180
            sqlcmd.CommandType = CommandType.StoredProcedure
            sqlcmd.Parameters.Add(New SqlParameter("@DocTable", "##" & strDoc))
            sqlcmd.Parameters.Add(New SqlParameter("@AttributeTable", "##" & strAttributes))
            sqlcmd.Parameters.Add(New SqlParameter("@DiffFromGMT", objUser.TimeDiffFromGMT))
            sqlcmd.Parameters.Add(New SqlParameter("@ResultsPerPage", objUser.DefaultResultCount))
            sqlcmd.Parameters.Add(New SqlParameter("@PageNumber", intPageNumber))
            sqlcmd.Parameters.Add(New SqlParameter("@SortColumn", intSort))
            sqlcmd.Parameters.Add(New SqlParameter("@SortOrder", intSortDirection))
            sqlcmd.Parameters.Add(New SqlParameter("@NoAttributeSearch", blnOrphans))
            sqlcmd.Parameters.Add(New SqlParameter("@TotalDocCount", SqlDbType.Int))
            sqlcmd.Parameters.Add(New SqlParameter("@ShowWorkflowStatus", SqlDbType.Int))
            sqlcmd.Parameters.Add(New SqlParameter("@UserID", intUserID))
            sqlcmd.Parameters("@TotalDocCount").Direction = ParameterDirection.Output
            sqlcmd.Parameters("@ShowWorkflowStatus").Direction = ParameterDirection.Output

            intShowWfColumn = sqlcmd.Parameters("@ShowWorkflowStatus").Value

            If Not strKeywords Is Nothing Then
                If strKeywords.Trim.Length > 0 Then
                    sqlcmd.Parameters.Add(New SqlParameter("@SearchText", strKeywords.Trim))
                    'Session("Keywords") = Me.mtxtKeywords.Text.Trim
                End If
            End If
            '
            ' Get advanced Search options
            '

            'Dim strAdvancedOptions As String = cookieSearch.SearchAdvanced
            'If Not strAdvancedOptions Is Nothing Then

            Dim bolExact As Boolean = False
            Dim bolOrphans As Boolean = False
            Dim bolWithin As Boolean = False
            Dim bolModifiedUser As Boolean = False
            Dim intModifiedUserId As Integer = 0

            'New search options, Search within/Match exact
            If bolWithin Then sqlcmd.Parameters.Add(New SqlParameter("@SearchWithin", "1"))
            If bolExact Then sqlcmd.Parameters.Add(New SqlParameter("@ExactPhrase", "1"))
            If bolModifiedUser Then sqlcmd.Parameters.Add(New SqlParameter("@ModifiedUserId", intModifiedUserId))
            'End If


            sqlDa = New SqlDataAdapter(sqlcmd)
            Dim ds As New DataSet("results")
            sqlDa.Fill(ds)

            If Not ds.Tables(1) Is Nothing Then
                dtSearchAttributes = ds.Tables(1)
            End If

            Dim intTotalDocCount As Integer = CInt(sqlcmd.Parameters("@TotalDocCount").Value)


            rValue = intTotalDocCount
            sqlDa.Dispose()

            ' destroy tables
            sb = New System.Text.StringBuilder
            sb.Append("DROP TABLE ##" & strDoc & vbCrLf)
            sb.Append("DROP TABLE ##" & strAttributes & vbCrLf)
            sqlcmd = New SqlClient.SqlCommand(sb.ToString, conSql)
            sqlcmd.ExecuteNonQuery()



            sqlcmd.Dispose()
            sb = Nothing




            Return ds

        Catch sqlEx As SqlClient.SqlException
            Throw New Exception(sqlEx.Message)
        Finally
            If bolCloseCon Then
                If Not conSql.State = ConnectionState.Closed Then conSql.Close()
            End If
        End Try

    End Function

    <WebMethod()> _
 Public Function GetTemplates(ByVal strUserId As String, ByVal strAccountId As String) As DataSet

        Dim conSql As SqlClient.SqlConnection

        Try

            Dim Request As HttpRequest = Context.Request
            Me.mobjUser = BuildUserObject(GetSessionId(strUserId), strAccountId)
            Me.mobjUser.AccountId = New System.Guid(strAccountId)

            conSql = Functions.BuildConnection(Me.mobjUser)

            Dim BurstTemplates As New Accucentric.docAssist.Data.Images.Templates(conSql, True)

            Return BurstTemplates.BTTemplatesGet()

        Catch ex As Exception
            Throw ex
        Finally
            If conSql.State <> ConnectionState.Closed Then
                conSql.Close()
                conSql.Dispose()
            End If
        End Try

    End Function

    <WebMethod()> _
    Public Function SaveTemplate(ByVal strUserId As String, ByVal strAccountId As String, ByVal TemplateName As String, ByVal Enabled As Boolean, ByVal DocumentID As Integer, ByVal XDPI As Integer, ByVal YDPI As Integer, ByVal DetectFieldID As Integer, ByVal DetectValue As String, _
    ByVal BurstType As String, ByVal BurstMethod As String, ByVal BurstValue As String, ByVal BurstFieldID As Integer, ByVal InheritAttributes As Boolean, ByVal RemoveOriginal As Boolean, ByVal dsDetails As DataSet) As Integer

        Dim sqlTran As SqlClient.SqlTransaction
        Dim conSql As New SqlClient.SqlConnection

        Try

            Dim Request As HttpRequest = Context.Request
            Me.mobjUser = BuildUserObject(GetSessionId(strUserId), strAccountId)
            Me.mobjUser.AccountId = New System.Guid(strAccountId)

            conSql = Functions.BuildConnection(Me.mobjUser)
            If conSql.State <> ConnectionState.Open Then conSql.Open()
            sqlTran = conSql.BeginTransaction

            Dim BurstTemplates As New Accucentric.docAssist.Data.Images.Templates(conSql, False, sqlTran)

            'Header
            Dim intTemplateID As Integer = BurstTemplates.BTHeaderInsert(TemplateName, Enabled, DocumentID, XDPI, YDPI, DetectFieldID, DetectValue, BurstType, BurstMethod, BurstValue, BurstFieldID, InheritAttributes, RemoveOriginal)

            'Details
            For Each dr As DataRow In dsDetails.Tables(0).Rows
                Dim intWidth As Integer = CInt(dr("Right")) - CInt(dr("Left"))
                Dim intHeight As Integer = CInt(dr("Bottom")) - CInt(dr("Top"))
                BurstTemplates.BTDetailInsert(intTemplateID, dr("GroupId"), "", dr("AttributeId"), dr("Top"), dr("Left"), intWidth, intHeight, IIf(dr("Script") Is System.DBNull.Value, "", dr("Script")))
            Next

            sqlTran.Commit()

            Return 0

        Catch ex As Exception
            sqlTran.Rollback()
            Throw ex
        Finally
            If conSql.State <> ConnectionState.Closed Then
                conSql.Close()
                conSql.Dispose()
            End If
        End Try

    End Function

    <WebMethod()> _
    Public Function EditTemplate(ByVal strUserId As String, ByVal strAccountId As String, ByVal TemplateID As Integer, ByVal TemplateName As String, ByVal Enabled As Boolean, ByVal DocumentID As Integer, ByVal XDPI As Integer, ByVal YDPI As Integer, ByVal DetectFieldID As Integer, ByVal DetectValue As String, _
    ByVal BurstType As String, ByVal BurstMethod As String, ByVal BurstValue As String, ByVal BurstFieldID As Integer, ByVal InheritAttributes As Boolean, ByVal RemoveOriginal As Boolean, ByVal dsDetails As DataSet) As Integer

        Dim sqlTran As SqlClient.SqlTransaction
        Dim conSql As New SqlClient.SqlConnection

        Try

            Dim Request As HttpRequest = Context.Request
            Me.mobjUser = BuildUserObject(GetSessionId(strUserId), strAccountId)
            Me.mobjUser.AccountId = New System.Guid(strAccountId)

            conSql = Functions.BuildConnection(Me.mobjUser)
            If conSql.State <> ConnectionState.Open Then conSql.Open()
            sqlTran = conSql.BeginTransaction

            Dim BurstTemplates As New Accucentric.docAssist.Data.Images.Templates(conSql, False, sqlTran)

            'Delete existing template
            Dim rValue As Integer = BurstTemplates.BTTemplateRemove(TemplateID)

            'Header
            Dim intTemplateID As Integer = BurstTemplates.BTHeaderInsert(TemplateName, Enabled, DocumentID, XDPI, YDPI, DetectFieldID, DetectValue, BurstType, BurstMethod, BurstValue, BurstFieldID, InheritAttributes, RemoveOriginal)

            'Details
            For Each dr As DataRow In dsDetails.Tables(0).Rows
                Dim intWidth As Integer = CInt(dr("Right")) - CInt(dr("Left"))
                Dim intHeight As Integer = CInt(dr("Bottom")) - CInt(dr("Top"))
                BurstTemplates.BTDetailInsert(intTemplateID, dr("GroupId"), "", dr("AttributeId"), dr("Top"), dr("Left"), intWidth, intHeight, IIf(dr("Script") Is System.DBNull.Value, "", dr("Script")))
            Next

            sqlTran.Commit()

            Return 0

        Catch ex As Exception
            sqlTran.Rollback()
            Throw ex
        Finally
            If conSql.State <> ConnectionState.Closed Then
                conSql.Close()
                conSql.Dispose()
            End If
        End Try

    End Function

    <WebMethod()> _
    Public Function RemoveTemplate(ByVal strUserId As String, ByVal strAccountId As String, ByVal TemplateID As Integer) As Integer

        Dim sqlTran As SqlClient.SqlTransaction
        Dim conSql As New SqlClient.SqlConnection

        Try

            Dim Request As HttpRequest = Context.Request
            Me.mobjUser = BuildUserObject(GetSessionId(strUserId), strAccountId)
            Me.mobjUser.AccountId = New System.Guid(strAccountId)

            conSql = Functions.BuildConnection(Me.mobjUser)
            If conSql.State <> ConnectionState.Open Then conSql.Open()
            sqlTran = conSql.BeginTransaction

            Dim BurstTemplates As New Accucentric.docAssist.Data.Images.Templates(conSql, False, sqlTran)

            'Delete existing template
            Dim rValue As Integer = BurstTemplates.BTTemplateRemove(TemplateID)

            sqlTran.Commit()

            Return 0

        Catch ex As Exception
            sqlTran.Rollback()
            Throw ex
        Finally
            If conSql.State <> ConnectionState.Closed Then
                conSql.Close()
                conSql.Dispose()
            End If
        End Try

    End Function

    ' <WebMethod(EnableSession:=True)> _
    'Public Function InsertDocument(ByVal Filename As String, ByVal mUserID As String, ByVal mAccountID As String, ByVal iDocumentID As Integer, ByVal dsAttributes As DataSet, _
    'ByVal intVersionID As Integer, ByVal blnAppendPages As Boolean, ByVal iPageInsert As Integer, ByVal blnPDF As Boolean, ByVal iQuality As Integer, _
    'ByVal strSaveType As String) As String

    '     Try

    '         If strSaveType.Trim.Length = 0 Then strSaveType = "Web Indexer"

    '         blnPDF = False 'This reelase takes only TIF's. Conversion happens client side.

    '         Dim mSessionID As String
    '         Dim objEncryption As New Encryption.Encryption

    '         'Get the users current sessionid
    '         Dim consqluser As New SqlConnection
    '         consqluser.ConnectionString = objEncryption.Decrypt(ConfigurationSettings.AppSettings("Connection"))
    '         If Not consqluser.State = ConnectionState.Open Then consqluser.Open()
    '         Dim dtUser As New DataTable
    '         Dim cmdUSql As New SqlClient.SqlCommand("SELECT SessionID FROM USERS WHERE UserID = @UserID", consqluser)
    '         cmdUSql.Parameters.Add("@UserID", mUserID)
    '         cmdUSql.CommandTimeout = 120
    '         Dim daSql As New SqlClient.SqlDataAdapter(cmdUSql)
    '         daSql.Fill(dtUser)

    '         If dtUser.Rows.Count <= 0 Then
    '             Return -1
    '             Exit Function
    '         Else
    '             mSessionID = dtUser.Rows(0).Item("SessionID")
    '         End If

    '         'Dim docbinaryarray As Byte()

    '         Dim consql As New SqlClient.SqlConnection

    '         Me.mobjUser = BuildUserObject(mSessionID, mAccountID)

    '         Dim strtempfilename As New System.Guid
    '         If Not Me.mobjUser Is Nothing Then
    '             consql = BuildConnection(Me.mobjUser)
    '             'First save file stream
    '             'Dim strSavedocPath As String = System.IO.Path.GetTempFileName

    '             '#If DEBUG Then
    '             '                Dim strSavedocPath As String = Server.MapPath("/docassistweb1/tmp/") & strtempfilename.NewGuid.ToString() & ".tif"
    '             '#Else
    '             '                Dim strSavedocPath As String = Server.MapPath("/tmp/") & strtempfilename.NewGuid.ToString() & ".tif"
    '             '#End If

    '             'Try
    '             '    If System.IO.File.Exists(strSavedocPath) = True Then System.IO.File.Delete(strSavedocPath)
    '             '    Dim objfilestream As FileStream = New FileStream(Filename, FileMode.Create, FileAccess.ReadWrite)
    '             '    objfilestream.Write(docbinaryarray, 0, docbinaryarray.Length)
    '             '    objfilestream.Close()
    '             'Catch ex As Exception
    '             '    'Return intdebugger
    '             '    Return ex.Message & Chr(13) & ex.StackTrace

    '             '    Return -2
    '             'End Try


    '             ''Convert PDF if needed
    '             'If blnPDF = True Then
    '             '    strSavedocPath = Replace(strSavedocPath, ".tif", ".pdf")
    '             'End If

    '             'strSavedocPath = objPdf.SaveSetQuality(strSavedocPath, iQuality)
    '             'strSavedocPath = objPdf.GetTempPage(strSavedocPath, 1, docAssist.Imaging.docPdf.Quality.NORMAL_G4, strSavedocPath)

    '             Dim objSave As New Accucentric.docAssist.Data.ImageSave(Me.mobjUser.ImagesUserId, consql)

    '             If intVersionID <> 0 Then
    '                 Dim dsNewVersion As New Data.dsVersionControl
    '                 dsNewVersion = Data.GetVersionControl(intVersionID, consql)
    '                 'dsNewVersion.ImageVersionDetail.Rows(0).Item("VersionNum") = 0

    '                 'If blnAppendPages is true we are appending pages so we dont want to delete existing pages
    '                 Dim dtOriginal As New DataTable
    '                 Dim drOriginal As DataRow

    '                 'If blnAppendPages = True Then
    '                 '    dtOriginal = CType(dsNewVersion.ImageVersionDetail, DataTable)
    '                 'End If

    '                 If blnAppendPages = False Then
    '                     For Each dr As Data.dsVersionControl.ImageVersionDetailRow In dsNewVersion.ImageVersionDetail
    '                         dr.Delete()
    '                     Next
    '                 End If

    '                 Dim drVersion As Data.dsVersionControl.ImageVersionRow
    '                 drVersion = dsNewVersion.ImageVersion.Rows(0)

    '                 Dim ix As New PegasusImaging.WinForms.ImagXpress7.PICImagXpress
    '                 ix.FileName = Filename

    '                 Dim iPageStart As Integer = 1

    '                 If iPageInsert > dsNewVersion.ImageVersionDetail.Rows.Count Then iPageInsert = (dsNewVersion.ImageVersionDetail.Rows.Count + 1)

    '                 ' Create new image details
    '                 Dim iTotalPages As Integer
    '                 Dim iOriginalPages As Integer = dsNewVersion.ImageDetail.Rows.Count
    '                 If blnAppendPages = True Then
    '                     iPageStart = iPageInsert
    '                     iTotalPages = ix.Pages
    '                 Else
    '                     iTotalPages = ix.Pages
    '                 End If

    '                 'Beginning of document
    '                 If iPageStart = 0 Then
    '                     If iPageInsert <= drVersion.SeqTotal Then
    '                         'Create space for the new pages
    '                         Dim iTotalPagesToInsert As Integer = ix.Pages
    '                         If iTotalPagesToInsert > 0 Then
    '                             Dim iCount As Integer

    '                             For iCount = (dsNewVersion.ImageVersionDetail.Rows.Count) To (iPageInsert + 1) Step -1
    '                                 dsNewVersion.ImageVersionDetail.Rows(iCount - 1).Item("SeqID") = dsNewVersion.ImageVersionDetail.Rows(iCount - 1).Item("SeqID") + iTotalPagesToInsert
    '                             Next
    '                             'iPageStart = (iPageInsert + 1)
    '                         End If
    '                     End If
    '                 End If

    '                 'In between document
    '                 If iPageStart < (iOriginalPages + 1) And iPageStart <> 0 And blnAppendPages = True Then
    '                     If iPageInsert <= drVersion.SeqTotal Then
    '                         'Create space for the new pages
    '                         Dim iTotalPagesToInsert As Integer = ix.Pages
    '                         If iTotalPagesToInsert > 0 Then
    '                             Dim iCount As Integer

    '                             For iCount = (dsNewVersion.ImageVersionDetail.Rows.Count) To (iPageInsert) Step -1
    '                                 dsNewVersion.ImageVersionDetail.Rows(iCount - 1).Item("SeqID") = dsNewVersion.ImageVersionDetail.Rows(iCount - 1).Item("SeqID") + iTotalPagesToInsert
    '                             Next
    '                             'iPageStart = (iPageInsert + 1)
    '                         End If
    '                     End If
    '                 End If

    '                 If blnAppendPages = False Then iPageStart = 0

    '                 'Beginning of document
    '                 If iPageStart = 0 Then
    '                     For intCount As Integer = (iPageStart + 1) To iTotalPages
    '                         Dim drImageDetail As Data.dsVersionControl.ImageDetailRow = dsNewVersion.ImageDetail.NewRow
    '                         dsNewVersion.ImageDetail.Rows.Add(drImageDetail)
    '                         Dim drImageVersionDetail As Data.dsVersionControl.ImageVersionDetailRow = dsNewVersion.ImageVersionDetail.NewRow()
    '                         drImageVersionDetail.ImportFileName = Filename
    '                         drImageVersionDetail.ImportPageNo = intCount '- iOriginalPages
    '                         drImageVersionDetail.SeqID = (iPageInsert) + intCount
    '                         drImageVersionDetail.VersionID = drVersion.VersionID
    '                         drImageVersionDetail.ImageDetID = drImageDetail.ImageDetID
    '                         dsNewVersion.ImageVersionDetail.Rows.Add(drImageVersionDetail)
    '                     Next
    '                 End If

    '                 'In between document
    '                 If iPageStart < (iOriginalPages + 1) And iPageStart <> 0 And blnAppendPages = True Then
    '                     For intCount As Integer = (iPageStart) To iTotalPages + 1
    '                         Dim drImageDetail As Data.dsVersionControl.ImageDetailRow = dsNewVersion.ImageDetail.NewRow
    '                         dsNewVersion.ImageDetail.Rows.Add(drImageDetail)
    '                         Dim drImageVersionDetail As Data.dsVersionControl.ImageVersionDetailRow = dsNewVersion.ImageVersionDetail.NewRow()
    '                         drImageVersionDetail.ImportFileName = Filename
    '                         drImageVersionDetail.ImportPageNo = (intCount - iPageStart) + 1
    '                         drImageVersionDetail.SeqID = intCount
    '                         drImageVersionDetail.VersionID = drVersion.VersionID
    '                         drImageVersionDetail.ImageDetID = drImageDetail.ImageDetID
    '                         dsNewVersion.ImageVersionDetail.Rows.Add(drImageVersionDetail)
    '                     Next
    '                 End If

    '                 'End of document
    '                 If iPageStart = (iOriginalPages + 1) Then
    '                     For intCount As Integer = iPageStart To ((iPageStart + iTotalPages) - 1)
    '                         Dim drImageDetail As Data.dsVersionControl.ImageDetailRow = dsNewVersion.ImageDetail.NewRow
    '                         dsNewVersion.ImageDetail.Rows.Add(drImageDetail)
    '                         Dim drImageVersionDetail As Data.dsVersionControl.ImageVersionDetailRow = dsNewVersion.ImageVersionDetail.NewRow()
    '                         drImageVersionDetail.ImportFileName = Filename
    '                         drImageVersionDetail.ImportPageNo = intCount - iOriginalPages  '- iOriginalPages
    '                         drImageVersionDetail.SeqID = intCount
    '                         drImageVersionDetail.VersionID = drVersion.VersionID
    '                         drImageVersionDetail.ImageDetID = drImageDetail.ImageDetID
    '                         dsNewVersion.ImageVersionDetail.Rows.Add(drImageVersionDetail)
    '                     Next
    '                 End If

    '                 Dim iNewVersionID As Integer
    '                 iNewVersionID = objSave.UpdateImage(dsNewVersion)

    '                 'Log recent item
    '                 Try
    '                     Dim blnReturn As Boolean = Functions.TrackRecentItem(Me.mobjUser, "SV", inewversionid)
    '                     If Not blnReturn Then
    '                         'error logging recent item
    '                     End If
    '                 Catch ex As Exception
    '                 End Try

    '                 Return iNewVersionID

    '             Else 'Save new image

    '                 Dim objVersion As New Accucentric.docAssist.Data.VersionControl(intVersionID, consql)
    '                 Dim drImageVersionDetail As ImageVersionDetailRow
    '                 Dim drImageAttributes As ImageAttributesRow

    '                 If dsAttributes.Tables.Count > 0 Then
    '                     Dim drAttributes As DataRow
    '                     For Each drAttributes In dsAttributes.Tables(0).Rows
    '                         drImageAttributes = objVersion.ImageAttributes.NewRow
    '                         drImageAttributes.AttributeId = drAttributes("AttributeID")
    '                         drImageAttributes.AttributeValue = drAttributes("AttributeValue")
    '                         objVersion.ImageAttributes.Rows.Add(drImageAttributes)
    '                     Next
    '                 End If

    '                 Dim iNewVersionID As Integer = objSave.SaveNewImage(Filename, iDocumentID, objVersion.ImageAttributes, Me.mobjUser, ConfigurationSettings.AppSettings("Connection"), _
    '                 strSaveType, , dsAttributes.Tables(2), dsAttributes.Tables(1))

    '                 'Log recent item
    '                 Try
    '                     Dim blnReturn As Boolean = Functions.TrackRecentItem(Me.mobjUser, "SV", inewversionid)
    '                     If Not blnReturn Then
    '                         'error logging recent item
    '                     End If
    '                 Catch ex As Exception
    '                 End Try

    '                 Return iNewVersionID

    '             End If

    '         End If

    '     Catch ex As Exception
    '         Return ex.Message & vbCr & ex.StackTrace
    '         'Throw ex
    '     End Try

    ' End Function

    <WebMethod(EnableSession:=True)> _
    Public Function SaveEmailAttachment(ByVal UserId As String, ByVal AccountId As String, ByVal FileName As String, ByVal FilePath As String, ByVal DocumentId As Integer, ByVal Attributes As DataSet) As String

        Dim sqlTran As SqlClient.SqlTransaction
        Dim consqluser As New SqlConnection

        Try

            Dim mSessionID As String

            'Get the users current sessionid
            consqluser.ConnectionString = Encryption.Encryption.Decrypt(ConfigurationSettings.AppSettings("Connection"))
            If Not consqluser.State = ConnectionState.Open Then consqluser.Open()
            Dim dtUser As New DataTable
            Dim cmdUSql As New SqlClient.SqlCommand("SELECT SessionID FROM USERS WHERE UserID = @UserID", consqluser)
            cmdUSql.Parameters.Add("@UserID", UserId)
            cmdUSql.CommandTimeout = 120
            Dim daSql As New SqlClient.SqlDataAdapter(cmdUSql)
            daSql.Fill(dtUser)

            If dtUser.Rows.Count <= 0 Then
                Return -1
                Exit Function
            Else
                mSessionID = dtUser.Rows(0).Item("SessionID")
            End If

            Me.mobjUser = BuildUserObject(mSessionID, AccountId)

            Dim conSqlImage As SqlClient.SqlConnection
            conSqlImage = Functions.BuildConnection(Me.mobjUser)
            conSqlImage.Open()

            sqlTran = conSqlImage.BeginTransaction

            Dim FolderId As Integer
            Dim Title As String = ""
            Dim Description As String = ""
            Dim Comments As String = ""
            Dim Tags As String = ""

            Dim byteArray As Byte() = GetDataAsByteArray(FilePath)

            Dim strFolderID As String
            If Attributes.Tables.Count > 0 Then
                Dim dtImageAttributes = Attributes.Tables(0)
                If New DataView(dtImageAttributes, "AttributeId=-4", "AttributeId DESC", DataViewRowState.CurrentRows).Count > 0 Then

                    strFolderID = New DataView(dtImageAttributes, "AttributeId=-4", "AttributeId DESC", DataViewRowState.CurrentRows).Item(0)("AttributeValue")
                End If
            Else
                strFolderID = "0"
            End If

            If Attributes.Tables.Count > 0 Then
                Dim dtImageAttributes = Attributes.Tables(0)
                If New DataView(dtImageAttributes, "AttributeId=-1", "AttributeId DESC", DataViewRowState.CurrentRows).Count > 0 Then
                    Title = New DataView(dtImageAttributes, "AttributeId=-1", "AttributeId DESC", DataViewRowState.CurrentRows).Item(0)("AttributeValue")
                End If
            Else
                Title = ""
            End If

            If Attributes.Tables.Count > 0 Then
                Dim dtImageAttributes = Attributes.Tables(0)
                If New DataView(dtImageAttributes, "AttributeId=-2", "AttributeId DESC", DataViewRowState.CurrentRows).Count > 0 Then
                    Description = New DataView(dtImageAttributes, "AttributeId=-2", "AttributeId DESC", DataViewRowState.CurrentRows).Item(0)("AttributeValue")
                End If
            Else
                Description = ""
            End If

            Dim objAttachments As New FileAttachments(conSqlImage, False, sqlTran)
            Dim intNewVersionId As Integer
            Dim intImageId As Integer = objAttachments.InsertFolderAttachment(strFolderID, DocumentId, Me.mobjUser.ImagesUserId, Title, Description, _
            Comments, Tags, New System.IO.FileInfo(FilePath).Length, FileName, byteArray, intNewVersionId)

            Dim intReturn As Integer
            Dim dtworkflow As DataTable
            If Attributes.Tables.Count > 1 Then
                dtworkflow = Attributes.Tables(1)
            End If

            If Not IsNothing(dtworkflow) Then
                If dtworkflow.Rows.Count > 0 Then
                    Dim wf As New Accucentric.docAssist.Data.Images.Workflow(conSqlImage, False, sqlTran)
                    intReturn = wf.WFRouteInsert(intNewVersionId, Me.mobjUser.ImagesUserId, dtworkflow.Rows(0)("WorkflowID"), dtworkflow.Rows(0)("Urgent"), dtworkflow.Rows(0)("QueueID"), dtworkflow.Rows(0)("Notes"))
                    If intReturn < 1 Then Throw New Exception("Error saving workflow.")
                End If
            End If

            If intImageId > 0 Then
                sqlTran.Commit()
                'Track this page
                Try
                    Dim objEnc As Encryption.Encryption
                    Accucentric.docAssist.Web.Data.Functions.TrackPageRequest(Me.mobjUser.UserId, Me.mobjUser.AccountId, "Web E-Mail", "In", intNewVersionId.ToString, Description.Replace("From:", "").Trim, ConfigurationSettings.AppSettings("Connection"), "", "", strFolderID)
                Catch ex As Exception

                End Try

            Else
                sqlTran.Rollback()
            End If



            Return intImageId
            'Dim AttributeId As Integer
            'Dim AttributeValue As String

            'Dim intResult = objAttachments.InsertFolderAttachmentAttribute(intImageId, AttributeId, AttributeValue)


        Catch ex As Exception

            Dim conMaster As SqlClient.SqlConnection
            conMaster = Functions.BuildMasterConnection
            Functions.LogException(mobjUser.UserId.ToString, mobjUser.AccountId.ToString, "", "SaveEmailAttachment() WebService Error " & ex.Message, ex.StackTrace, Server.MachineName, "", "", conMaster, True)

            sqlTran.Rollback()

        Finally
            If consqluser.State <> ConnectionState.Closed Then
                consqluser.Close()
                consqluser.Dispose()
            End If
        End Try

    End Function

    <WebMethod(EnableSession:=True)> _
    Public Function SaveEmailMessage(ByVal UserId As String, ByVal AccountId As String, ByVal EmailSubject As String, ByVal EmailMessage As String, ByVal DocumentId As Integer, ByVal Attributes As DataSet) As String

        Dim sqlTran As SqlClient.SqlTransaction
        Dim consqluser As New SqlConnection

        Try

            Dim mSessionID As String

            'Get the users current sessionid
            consqluser.ConnectionString = Encryption.Encryption.Decrypt(ConfigurationSettings.AppSettings("Connection"))
            If Not consqluser.State = ConnectionState.Open Then consqluser.Open()
            Dim dtUser As New DataTable
            Dim cmdUSql As New SqlClient.SqlCommand("SELECT SessionID FROM USERS WHERE UserID = @UserID", consqluser)
            cmdUSql.Parameters.Add("@UserID", UserId)
            cmdUSql.CommandTimeout = 120
            Dim daSql As New SqlClient.SqlDataAdapter(cmdUSql)
            daSql.Fill(dtUser)

            If dtUser.Rows.Count <= 0 Then
                Return -1
                Exit Function
            Else
                mSessionID = dtUser.Rows(0).Item("SessionID")
            End If

            Me.mobjUser = BuildUserObject(mSessionID, AccountId)

            Dim conSqlImage As SqlClient.SqlConnection
            conSqlImage = Functions.BuildConnection(Me.mobjUser)
            conSqlImage.Open()

            sqlTran = conSqlImage.BeginTransaction

            Dim FolderId As Integer
            Dim Title As String = ""
            Dim Description As String = ""
            Dim Comments As String = ""
            Dim Tags As String = ""

            Dim byteArray As Byte()
            byteArray = System.Text.Encoding.ASCII.GetBytes(EmailMessage)

            Dim strFolderID As String
            If Attributes.Tables.Count > 0 Then
                Dim dtImageAttributes = Attributes.Tables(0)
                If New DataView(dtImageAttributes, "AttributeId=-4", "AttributeId DESC", DataViewRowState.CurrentRows).Count > 0 Then
                    strFolderID = New DataView(dtImageAttributes, "AttributeId=-4", "AttributeId DESC", DataViewRowState.CurrentRows).Item(0)("AttributeValue")
                End If
            Else
                strFolderID = "0"
            End If

            If Attributes.Tables.Count > 0 Then
                Dim strTitle As String
                If Attributes.Tables.Count > 0 Then
                    Dim dtImageAttributes = Attributes.Tables(0)
                    If New DataView(dtImageAttributes, "AttributeId=-1", "AttributeId DESC", DataViewRowState.CurrentRows).Count > 0 Then
                        strTitle = New DataView(dtImageAttributes, "AttributeId=-1", "AttributeId DESC", DataViewRowState.CurrentRows).Item(0)("AttributeValue")
                    End If
                Else
                    strTitle = ""
                End If
            End If

            If Attributes.Tables.Count > 0 Then
                Dim dtImageAttributes = Attributes.Tables(0)
                If New DataView(dtImageAttributes, "AttributeId=-2", "AttributeId DESC", DataViewRowState.CurrentRows).Count > 0 Then
                    Description = New DataView(dtImageAttributes, "AttributeId=-2", "AttributeId DESC", DataViewRowState.CurrentRows).Item(0)("AttributeValue")
                End If
            Else
                Description = ""
            End If

            Dim objAttachments As New FileAttachments(conSqlImage, False, sqlTran)
            Dim intNewVersionId As Integer
            Dim intImageId As Integer = objAttachments.InsertEmailMessage(FolderId, DocumentId, Me.mobjUser.ImagesUserId, Title, Description, _
            Comments, Tags, EmailMessage.Length, EmailSubject, byteArray, intNewVersionId)

            If intImageId > 0 Then
                sqlTran.Commit()
            Else
                sqlTran.Rollback()
            End If

            Return intImageId

            'Dim AttributeId As Integer
            'Dim AttributeValue As String

            'Dim intResult = objAttachments.InsertFolderAttachmentAttribute(intImageId, AttributeId, AttributeValue)

        Catch ex As Exception
            sqlTran.Rollback()

        Finally
            If consqluser.State <> ConnectionState.Closed Then
                consqluser.Close()
                consqluser.Dispose()
            End If
        End Try

    End Function

    <WebMethod(EnableSession:=True)> _
    Public Function AddListItem(ByVal AttributeId As Integer, ByVal Value As String, ByVal Description As String, ByVal DocumentId As Integer, ByVal FolderId As Integer) As String

        Dim conSql As SqlClient.SqlConnection

        Try

            Dim Request As HttpRequest = Context.Request
            Me.mobjUser = BuildUserObject(Request.Cookies("docAssist"))

            conSql = Functions.BuildConnection(Me.mobjUser)
            conSql.Open()

            Dim list As New ListAttributes(conSql)

            Dim intAttributeId As Integer = list.AttributeListAdd(AttributeId, Value, Description, DocumentId, FolderId)

            Return intAttributeId

        Catch ex As Exception

        Finally

            If conSql.State <> ConnectionState.Closed Then
                conSql.Close()
                conSql.Dispose()
            End If

        End Try

    End Function

    <WebMethod(EnableSession:=True)> _
  Public Function EditListItem(ByVal AttributeID As Integer, ByVal AttributeListId As Integer, ByVal Value As String, ByVal Description As String) As String

        Dim conSql As SqlClient.SqlConnection

        Try

            Dim Request As HttpRequest = Context.Request
            Me.mobjUser = BuildUserObject(Request.Cookies("docAssist"))

            conSql = Functions.BuildConnection(Me.mobjUser)
            conSql.Open()

            Dim list As New ListAttributes(conSql)

            Dim intAttributeId As Integer = list.AttributeListEdit(AttributeID, AttributeListId, Value, Description)

            Return intAttributeId

        Catch ex As Exception

        Finally

            If conSql.State <> ConnectionState.Closed Then
                conSql.Close()
                conSql.Dispose()
            End If

        End Try

    End Function

    <WebMethod(EnableSession:=True)> _
    Public Function ListItemRemove(ByVal AttributeListId As Integer) As String

        Dim conSql As SqlClient.SqlConnection

        Try

            Dim Request As HttpRequest = Context.Request
            Me.mobjUser = BuildUserObject(Request.Cookies("docAssist"))

            conSql = Functions.BuildConnection(Me.mobjUser)
            conSql.Open()

            Dim list As New ListAttributes(conSql)

            Dim intAttributeId As Integer = list.AttributeListItemRemove(AttributeListId)

            Return intAttributeId

        Catch ex As Exception

        Finally

            If conSql.State <> ConnectionState.Closed Then
                conSql.Close()
                conSql.Dispose()
            End If

        End Try

    End Function

    <WebMethod(EnableSession:=True)> _
    Public Function FilterList(ByVal AttributeId As String, ByVal SortType As Integer, ByVal SearchString As String, ByVal DocumentId As Integer, ByVal FolderId As Integer) As String()

        Dim conSql As SqlClient.SqlConnection
        Try

            Dim Request As HttpRequest = Context.Request
            Me.mobjUser = BuildUserObject(Request.Cookies("docAssist"))

            conSql = Functions.BuildConnection(Me.mobjUser)
            conSql.Open()

            Dim list As New ListAttributes(conSql)
            Dim dt As New DataTable
            Dim blnSimpleMode As Boolean
            Dim intNumColumns As Integer = 0
            Dim intAccessLevel As Integer
            dt = list.LookupFilter(Me.mobjUser.ImagesUserId, AttributeId, SortType, SearchString, intNumColumns, DocumentId, FolderId, intAccessLevel)

            If dt.Rows.Count = 0 Then Return ("No items found.�" & intNumColumns.ToString & "�" & intAccessLevel.ToString).Split("�")

            'Create HTML
            Dim sb As New System.Text.StringBuilder
            sb.Append("<TABLE border=0 cellspacing=0 cellpadding=0 width=100% class=SearchGrid id=listTable style=""CURSOR: hand"">")

            For Each dr As DataRow In dt.Rows
                sb.Append("<TR ondblclick=""selectItem();"" onclick=""pickItem('" & dr("Value").Replace("'", "\'") & "','" & dr("ValueDescription").Replace("'", "\'") & "','" & dr("AttributeListId") & "');highlightTableRow('listTable', this);"">")

                'Dim value As String = dr("Value").Replace("'", "\'")
                'value = "<span style=""TEXT-DECORATION: underline"">" & CStr(value).Substring(CStr(value).ToLower.IndexOf(SearchString), SearchString.Trim.Length) & "</span>" & CStr(value).Substring(CStr(value).ToLower.IndexOf(SearchString) + (SearchString.Length))
                'value = "-" & CStr(value).Substring(CStr(value).ToLower.IndexOf(SearchString), SearchString.Trim.Length) & "-" & CStr(value).Substring(CStr(value).ToLower.IndexOf(SearchString) + (SearchString.Length))

                Select Case intNumColumns
                    Case 1
                        sb.Append("<TD width=100% valign=top>" & dr("Value").Replace("'", "\'") & "</TD>")
                    Case 2
                        sb.Append("<TD width=50% valign=top>" & dr("Value").Replace("'", "\'") & "</TD>")
                        sb.Append("<TD width=50% valign=top>" & dr("ValueDescription").Replace("'", "\'") & "</TD>")
                End Select

                sb.Append("</TR>")
            Next

            sb.Append("<TR><TD><img src=spacer.gif height=100% width=1></TD></TR>")
            sb.Append("</TABLE>")

            Return (sb.ToString & "�" & intNumColumns.ToString & "�" & intAccessLevel.ToString).Split("�")

        Catch ex As Exception

        Finally

            If conSql.State <> ConnectionState.Closed Then
                conSql.Close()
                conSql.Dispose()
            End If

        End Try

    End Function

    <WebMethod(EnableSession:=True)> _
    Public Function SaveEmailImage(ByVal mUserID As String, ByVal mAccountID As String, ByVal FileName As String, ByVal iDocumentID As Integer, ByVal dsAttributes As DataSet, _
    ByVal blnCompression As Boolean, ByVal intVersionID As Integer, ByVal blnAppendPages As Boolean, ByVal iPageInsert As Integer, ByVal blnPDF As Boolean, ByVal iQuality As Integer, _
    ByVal strSaveType As String) As String

        Try

            If strSaveType.Trim.Length = 0 Then strSaveType = "Web E-Mail"

            'blnPDF = False 'This reelase takes only TIF's. Conversion happens client side.

            Dim mSessionID As String
            Dim objEncryption As New Encryption.Encryption

            'Get the users current sessionid
            Dim consqluser As New SqlConnection
            consqluser.ConnectionString = objEncryption.Decrypt(ConfigurationSettings.AppSettings("Connection"))
            If Not consqluser.State = ConnectionState.Open Then consqluser.Open()
            Dim dtUser As New DataTable
            Dim cmdUSql As New SqlClient.SqlCommand("SELECT SessionID FROM USERS WHERE UserID = @UserID", consqluser)
            cmdUSql.Parameters.Add("@UserID", mUserID)
            cmdUSql.CommandTimeout = 120
            Dim daSql As New SqlClient.SqlDataAdapter(cmdUSql)
            daSql.Fill(dtUser)

            If dtUser.Rows.Count <= 0 Then
                Return -1
                Exit Function
            Else
                mSessionID = dtUser.Rows(0).Item("SessionID")
            End If

            ''If compression is passed decompress image
            'If blnCompression = True Then
            '    docbinaryarray = DecompressImage(docbinaryarray)
            'End If

            'dsImageData expected to have attributes... if empty then the presumption is no attributes for document.

            Dim consql As New SqlClient.SqlConnection

            Me.mobjUser = BuildUserObject(mSessionID, mAccountID)

            Dim strtempfilename As New System.Guid
            If Not Me.mobjUser Is Nothing Then
                consql = BuildConnection(Me.mobjUser)
                'First save file stream
                'Dim strSavedocPath As String = System.IO.Path.GetTempFileName

                'Dim strSavedocPath As String = Server.MapPath("/tmp/") & strtempfilename.NewGuid.ToString() & ".tif"

                'Convert PDF if needed
                'If blnPDF = True Then
                '    strSavedocPath = Replace(strSavedocPath, ".tif", ".pdf")
                'End If

                'strSavedocPath = objPdf.SaveSetQuality(strSavedocPath, iQuality)
                'strSavedocPath = objPdf.GetTempPage(strSavedocPath, 1, docAssist.Imaging.docPdf.Quality.NORMAL_G4, strSavedocPath)

                'strSavedocPath = ProcessFileForSave(FileName, blnPDF)

                Dim objSave As New Accucentric.docAssist.Data.ImageSave(Me.mobjUser.ImagesUserId, consql)

                If intVersionID <> 0 Then
                    Dim dsNewVersion As New Data.dsVersionControl
                    dsNewVersion = Data.GetVersionControl(intVersionID, consql)
                    'dsNewVersion.ImageVersionDetail.Rows(0).Item("VersionNum") = 0

                    'If blnAppendPages is true we are appending pages so we dont want to delete existing pages
                    Dim dtOriginal As New DataTable
                    Dim drOriginal As DataRow

                    'If blnAppendPages = True Then
                    '    dtOriginal = CType(dsNewVersion.ImageVersionDetail, DataTable)
                    'End If

                    If blnAppendPages = False Then
                        For Each dr As Data.dsVersionControl.ImageVersionDetailRow In dsNewVersion.ImageVersionDetail
                            dr.Delete()
                        Next
                    End If

                    'Readd original pages
                    'If blnAppendPages = True Then
                    '    For Each drOriginal In dtOriginal.Rows
                    '        dsNewVersion.ImageVersionDetail.Rows.Add(CType(drOriginal, Data.dsVersionControl.ImageVersionDetailRow))
                    '    Next
                    'End If

                    Dim drVersion As Data.dsVersionControl.ImageVersionRow
                    drVersion = dsNewVersion.ImageVersion.Rows(0)

                    'Dim ix As New PegasusImaging.WinForms.ImagXpress7.PICImagXpress
                    'ix.FileName = strSavedocPath

                    Dim iPageStart As Integer = 1

                    If iPageInsert > dsNewVersion.ImageVersionDetail.Rows.Count Then iPageInsert = (dsNewVersion.ImageVersionDetail.Rows.Count + 1)

                    ' Create new image details
                    Dim iTotalPages As Integer
                    Dim iOriginalPages As Integer = dsNewVersion.ImageDetail.Rows.Count
                    If blnAppendPages = True Then
                        iPageStart = iPageInsert 'dsNewVersion.ImageDetail.Rows.Count + 1
                        iTotalPages = Functions.PageCount(FileName) 'ix.Pages ' dsNewVersion.ImageDetail.Rows.Count + ix.Pages

                    Else
                        iTotalPages = Functions.PageCount(FileName)

                    End If

                    'Beginning of document
                    'If iPageStart = 1 Then iPageStart = 0
                    If iPageStart = 0 Then
                        If iPageInsert <= drVersion.SeqTotal Then
                            'Create space for the new pages
                            Dim iTotalPagesToInsert As Integer = Functions.PageCount(FileName)
                            If iTotalPagesToInsert > 0 Then
                                Dim iCount As Integer

                                For iCount = (dsNewVersion.ImageVersionDetail.Rows.Count) To (iPageInsert + 1) Step -1
                                    dsNewVersion.ImageVersionDetail.Rows(iCount - 1).Item("SeqID") = dsNewVersion.ImageVersionDetail.Rows(iCount - 1).Item("SeqID") + iTotalPagesToInsert
                                Next
                                'iPageStart = (iPageInsert + 1)
                            End If
                        End If
                    End If

                    'In between document
                    If iPageStart < (iOriginalPages + 1) And iPageStart <> 0 And blnAppendPages = True Then
                        If iPageInsert <= drVersion.SeqTotal Then
                            'Create space for the new pages
                            Dim iTotalPagesToInsert As Integer = Functions.PageCount(FileName)
                            If iTotalPagesToInsert > 0 Then
                                Dim iCount As Integer

                                For iCount = (dsNewVersion.ImageVersionDetail.Rows.Count) To (iPageInsert) Step -1
                                    dsNewVersion.ImageVersionDetail.Rows(iCount - 1).Item("SeqID") = dsNewVersion.ImageVersionDetail.Rows(iCount - 1).Item("SeqID") + iTotalPagesToInsert
                                Next
                                'iPageStart = (iPageInsert + 1)
                            End If
                        End If
                    End If

                    If blnAppendPages = False Then iPageStart = 0

                    'Beginning of document
                    If iPageStart = 0 Then
                        For intCount As Integer = (iPageStart + 1) To iTotalPages
                            Dim drImageDetail As Data.dsVersionControl.ImageDetailRow = dsNewVersion.ImageDetail.NewRow
                            dsNewVersion.ImageDetail.Rows.Add(drImageDetail)
                            Dim drImageVersionDetail As Data.dsVersionControl.ImageVersionDetailRow = dsNewVersion.ImageVersionDetail.NewRow()
                            drImageVersionDetail.ImportFileName = FileName
                            drImageVersionDetail.ImportPageNo = intCount '- iOriginalPages
                            drImageVersionDetail.SeqID = (iPageInsert) + intCount
                            drImageVersionDetail.VersionID = drVersion.VersionID
                            drImageVersionDetail.ImageDetID = drImageDetail.ImageDetID
                            dsNewVersion.ImageVersionDetail.Rows.Add(drImageVersionDetail)
                        Next
                    End If

                    'In between document
                    If iPageStart < (iOriginalPages + 1) And iPageStart <> 0 And blnAppendPages = True Then
                        For intCount As Integer = (iPageStart) To iTotalPages + 1
                            Dim drImageDetail As Data.dsVersionControl.ImageDetailRow = dsNewVersion.ImageDetail.NewRow
                            dsNewVersion.ImageDetail.Rows.Add(drImageDetail)
                            Dim drImageVersionDetail As Data.dsVersionControl.ImageVersionDetailRow = dsNewVersion.ImageVersionDetail.NewRow()
                            drImageVersionDetail.ImportFileName = FileName
                            drImageVersionDetail.ImportPageNo = (intCount - iPageStart) + 1
                            drImageVersionDetail.SeqID = intCount
                            drImageVersionDetail.VersionID = drVersion.VersionID
                            drImageVersionDetail.ImageDetID = drImageDetail.ImageDetID
                            dsNewVersion.ImageVersionDetail.Rows.Add(drImageVersionDetail)
                        Next
                    End If

                    'End of document
                    If iPageStart = (iOriginalPages + 1) Then
                        For intCount As Integer = iPageStart To ((iPageStart + iTotalPages) - 1)
                            Dim drImageDetail As Data.dsVersionControl.ImageDetailRow = dsNewVersion.ImageDetail.NewRow
                            dsNewVersion.ImageDetail.Rows.Add(drImageDetail)
                            Dim drImageVersionDetail As Data.dsVersionControl.ImageVersionDetailRow = dsNewVersion.ImageVersionDetail.NewRow()
                            drImageVersionDetail.ImportFileName = FileName
                            drImageVersionDetail.ImportPageNo = intCount - iOriginalPages  '- iOriginalPages
                            drImageVersionDetail.SeqID = intCount
                            drImageVersionDetail.VersionID = drVersion.VersionID
                            drImageVersionDetail.ImageDetID = drImageDetail.ImageDetID
                            dsNewVersion.ImageVersionDetail.Rows.Add(drImageVersionDetail)
                        Next
                    End If

                    Dim iNewVersionID As Integer
                    iNewVersionID = objSave.UpdateImage(dsNewVersion)

                    'Log recent item
                    Try
                        Dim blnReturn As Boolean = Functions.TrackRecentItem(Me.mobjUser, "SV", inewversionid)
                        If Not blnReturn Then
                            'error logging recent item
                        End If
                    Catch ex As Exception
                    End Try

                    Return iNewVersionID

                Else 'Save new image

                    Dim objVersion As New Accucentric.docAssist.Data.VersionControl(intVersionID, consql)
                    Dim drImageVersionDetail As ImageVersionDetailRow
                    Dim drImageAttributes As ImageAttributesRow

                    If Not dsAttributes Is Nothing Then
                        If dsAttributes.Tables.Count > 0 Then
                            Dim drAttributes As DataRow
                            For Each drAttributes In dsAttributes.Tables(0).Rows
                                drImageAttributes = objVersion.ImageAttributes.NewRow
                                drImageAttributes.AttributeId = drAttributes("AttributeID")
                                drImageAttributes.AttributeValue = drAttributes("AttributeValue")
                                objVersion.ImageAttributes.Rows.Add(drImageAttributes)
                            Next
                        End If
                    End If

                    Dim iNewVersionID As Integer = objSave.SaveNewImage(FileName, iDocumentID, objVersion.ImageAttributes, Me.mobjUser, ConfigurationSettings.AppSettings("Connection"), _
                    strSaveType, , Nothing, Nothing)

                    Dim intReturn As Integer
                    Dim dtworkflow As DataTable
                    If dsAttributes.Tables.Count > 1 Then
                        dtworkflow = dsAttributes.Tables(1)
                    End If

                    If Not IsNothing(dtworkflow) Then
                        If dtworkflow.Rows.Count > 0 Then
                            Dim wf As New Accucentric.docAssist.Data.Images.Workflow(consql)
                            intReturn = wf.WFRouteInsert(iNewVersionID, Me.mobjUser.ImagesUserId, dtworkflow.Rows(0)("WorkflowID"), dtworkflow.Rows(0)("Urgent"), dtworkflow.Rows(0)("QueueID"), dtworkflow.Rows(0)("Notes"))
                            If intReturn < 1 Then Throw New Exception("Error saving workflow.")
                        End If
                    End If

                    'Log recent item
                    Try
                        Dim blnReturn As Boolean = Functions.TrackRecentItem(Me.mobjUser, "SV", inewversionid)
                        If Not blnReturn Then
                            'error logging recent item
                        End If
                    Catch ex As Exception
                    End Try

                    Return iNewVersionID

                End If

            End If

        Catch ex As Exception
            Return ex.Message & vbCr & ex.StackTrace
            'Throw ex
        End Try

    End Function

    <WebMethod(EnableSession:=True)> _
    Public Function AddFavorite(ByVal FolderId As Integer, ByVal FavoriteName As String) As String()

        Dim conSqlCompany As New SqlClient.SqlConnection

        Try

            Dim Request As HttpRequest = Context.Request
            Me.mobjUser = BuildUserObject(Request.Cookies("docAssist"))
            conSqlCompany = Functions.BuildConnection(Me.mobjUser)

            Dim objFolders As New Folders(conSqlCompany, False)

            Dim dtExtraInfo As DataTable

            Dim favoriteId As Integer = objFolders.FavoriteInsert(Me.mobjUser.ImagesUserId, FavoriteName, FolderId, dtExtraInfo)

            If favoriteId < 0 Then
                Dim intCount As Integer = 0
                Do While favoriteId < 0
                    FavoriteName = FavoriteName.Replace("[" & (intCount) & "]", "") & "[" & (intCount + 1) & "]"
                    favoriteId = objFolders.FavoriteInsert(Me.mobjUser.ImagesUserId, FavoriteName, FolderId, dtExtraInfo)
                    intCount += 1
                Loop
            End If

            Return (favoriteId.ToString & "�" & FavoriteName.ToString & "�" & FolderId).Split("�")

        Catch ex As Exception

        Finally

            If conSqlCompany.State <> ConnectionState.Closed Then
                conSqlCompany.Close()
                conSqlCompany.Dispose()
            End If

        End Try

    End Function

    <WebMethod(EnableSession:=True)> _
    Public Function AddCabinet(ByVal CabinetName As String) As String()

        CabinetName = CabinetName.Replace("/amp", "&")

        Dim conSqlCompany As New SqlClient.SqlConnection

        Try

            Dim Request As HttpRequest = Context.Request
            Me.mobjUser = BuildUserObject(Request.Cookies("docAssist"))
            conSqlCompany = Functions.BuildConnection(Me.mobjUser)

            Dim objFolders As New Folders(conSqlCompany, False)
            Dim intNewFolderId As Integer
            intNewFolderId = objFolders.InsertShare(Me.mobjUser.ImagesUserId, CabinetName)

            If intNewFolderId < 0 Then
                Dim intCount As Integer = 1
                Dim origName As String = CabinetName
                Do While intNewFolderId < 0
                    CabinetName = origName & "[" & intCount.ToString & "]"
                    intNewFolderId = objFolders.InsertShare(Me.mobjUser.ImagesUserId, CabinetName)
                    intCount += 1
                Loop
            End If

            Dim sbShareCommand As New System.Text.StringBuilder
            sbShareCommand.Append("browseCabinet('" & intNewFolderId.ToString & "');setValue('S" & intNewFolderId.ToString & "');")
            sbShareCommand.Append("enableCabinet();")
            sbShareCommand.Append("enableRename();")
            sbShareCommand.Append("enableDelete();")
            sbShareCommand.Append("enableFolder();")
            sbShareCommand.Append("enableSecurity();")
            sbShareCommand.Append("disableAdd();disableFavorite();")

            Dim values(3) As String
            values(0) = intNewFolderId.ToString
            values(1) = sbShareCommand.ToString
            values(2) = CabinetName

            Return values

        Catch ex As Exception

        Finally

            If conSqlCompany.State <> ConnectionState.Closed Then
                conSqlCompany.Close()
                conSqlCompany.Dispose()
            End If

        End Try

    End Function

    <WebMethod(EnableSession:=True)> _
   Public Function AddNewCabinet(ByVal CabinetName As String) As String()

        Dim conSqlCompany As New SqlClient.SqlConnection

        Try

            Dim Request As HttpRequest = Context.Request
            Me.mobjUser = BuildUserObject(Request.Cookies("docAssist"))
            conSqlCompany = Functions.BuildConnection(Me.mobjUser)

            Dim objFolders As New Folders(conSqlCompany, False)
            Dim intNewFolderId As Integer
            Dim strCabinetName As String = CabinetName
            intNewFolderId = objFolders.InsertShare(Me.mobjUser.ImagesUserId, strCabinetName)

            Dim originalName As String = CabinetName
            If intNewFolderId < 0 Then
                Dim intCount As Integer = 1
                Do While intNewFolderId < 0
                    strCabinetName = originalName & "[" & intCount.ToString & "]"
                    intNewFolderId = objFolders.InsertShare(Me.mobjUser.ImagesUserId, strCabinetName)
                    intCount += 1
                Loop
            End If

            Dim sbShareCommand As New System.Text.StringBuilder
            sbShareCommand.Append("browseCabinet('" & intNewFolderId.ToString & "');setValue('S" & intNewFolderId.ToString & "');")
            sbShareCommand.Append("enableCabinet();")
            sbShareCommand.Append("enableRename();")
            sbShareCommand.Append("enableDelete();")
            sbShareCommand.Append("enableFolder();")
            sbShareCommand.Append("enableSecurity();")
            sbShareCommand.Append("disableAdd();disableFavorite();")

            Return (intNewFolderId.ToString & "�" & sbShareCommand.ToString & "�" & strCabinetName).Split("�")

        Catch ex As Exception

        Finally

            If conSqlCompany.State <> ConnectionState.Closed Then
                conSqlCompany.Close()
                conSqlCompany.Dispose()
            End If

        End Try

    End Function

    <WebMethod(EnableSession:=True)> _
    Public Function RenameTreeItem(ByVal NodePrefix As String, ByVal FolderId As String, ByVal NewName As String) As String()
        '-1 means duplicate name

        NewName = NewName.Replace("/amp", "&")

        Dim tempName As String = NewName
        Dim conSqlCompany As New SqlClient.SqlConnection

        Try

            Dim Request As HttpRequest = Context.Request
            Me.mobjUser = BuildUserObject(Request.Cookies("docAssist"))
            conSqlCompany = Functions.BuildConnection(Me.mobjUser)

            Dim objFolders As New Folders(conSqlCompany, False)

            Dim intResult As Integer
            Dim returnName As String

            FolderId = FolderId.Replace(NodePrefix, "")
            returnName = NewName

            Select Case NodePrefix
                Case "S"
                    'Update folder
                    intResult = objFolders.UpdateShare(FolderId, NewName)

                    Dim ShareName As String = NewName

                    If intResult < 0 Then
                        Dim intCount As Integer = 1
                        Do While intResult < 0
                            ShareName = tempName & "[" & intCount.ToString & "]"
                            intResult = objFolders.UpdateShare(FolderId, ShareName)
                            returnName = ShareName
                            intCount += 1
                        Loop
                    End If

                Case "F"
                    'Update favorite
                    Dim FavValues() As String = FolderId.Split(FAVORITE_SPLIT)
                    Dim FavId As String = FavValues(0)
                    intResult = objFolders.FavoriteUpdate(Me.mobjUser.ImagesUserId, FavId, NewName)

                    Dim FavName As String = NewName

                    If intResult < 0 Then
                        Dim intCount As Integer = 1
                        Do While intResult < 0
                            FavName = tempName & "[" & intCount.ToString & "]"
                            intResult = objFolders.FavoriteUpdate(Me.mobjUser.ImagesUserId, FavId, FavName)
                            returnName = FavName
                            intCount += 1
                        Loop
                    End If

                Case "W"
                    'Update folder
                    intResult = objFolders.UpdateFolder(FolderId, NewName)

                    Dim FolderName As String = NewName

                    If intResult < 0 Then
                        Dim intCount As Integer = 1
                        Do While intResult < 0
                            FolderName = tempName & "[" & intCount.ToString & "]"
                            intResult = objFolders.UpdateFolder(FolderId, FolderName)
                            returnName = FolderName
                            intCount += 1
                        Loop
                    End If

            End Select

            Dim values(3) As String
            values(0) = intResult.ToString
            values(1) = NodePrefix
            values(2) = returnName

            Return values

        Catch ex As Exception

        Finally

            If conSqlCompany.State <> ConnectionState.Closed Then
                conSqlCompany.Close()
                conSqlCompany.Dispose()
            End If

        End Try

    End Function

    <WebMethod(EnableSession:=True)> _
    Public Function DeleteTreeItem(ByVal NodePrefix As String, ByVal FolderId As String) As Integer

        Dim conSqlCompany As New SqlClient.SqlConnection

        Try

            Dim Request As HttpRequest = Context.Request
            Me.mobjUser = BuildUserObject(Request.Cookies("docAssist"))
            conSqlCompany = Functions.BuildConnection(Me.mobjUser)

            Dim objFolders As New Folders(conSqlCompany, False)

            Select Case NodePrefix

                Case "S"

                    Dim blnCanDelete As Boolean = objFolders.ShareDelete(Me.mobjUser.ImagesUserId, FolderId.Replace("S", ""), False)

                    If blnCanDelete Then
                        If objFolders.ShareDelete(Me.mobjUser.ImagesUserId, FolderId.Replace("S", ""), True) = 1 Then
                            Return 1
                        Else
                            Return 0
                        End If
                    Else
                        Return -1
                    End If

                Case "W"

                    Dim blnCanDelete As Boolean = objFolders.FolderDelete(Me.mobjUser.ImagesUserId, Me.mobjUser.AccountId.ToString, FolderId.Replace("W", ""), False)

                    If blnCanDelete Then
                        If objFolders.FolderDelete(Me.mobjUser.ImagesUserId, Me.mobjUser.AccountId.ToString, FolderId.Replace("W", ""), True) = 1 Then
                            Return 1
                        Else
                            Return 0
                        End If
                    Else
                        Return -1
                    End If

                Case "F"

                    If objFolders.FavoriteRemove(Me.mobjUser.ImagesUserId, FolderId.Split("-FAVSPLIT-")(0).Replace("F", "")) = FolderId.Split("-FAVSPLIT-")(0).Replace("F", "") Then
                        Return 1
                    Else
                        Return 0
                    End If

            End Select

        Catch ex As Exception

        Finally

            If conSqlCompany.State <> ConnectionState.Closed Then
                conSqlCompany.Close()
                conSqlCompany.Dispose()
            End If

        End Try

    End Function

    <WebMethod(EnableSession:=True)> _
     Public Function AddFolder(ByVal ParentId As String, ByVal FolderName As String) As String()

        FolderName = FolderName.Replace("/amp", "&")

        Dim conSqlCompany As New SqlClient.SqlConnection

        Try

            Dim Request As HttpRequest = Context.Request
            Me.mobjUser = BuildUserObject(Request.Cookies("docAssist"))
            conSqlCompany = Functions.BuildConnection(Me.mobjUser)

            Dim dtPermissions As New DataTable

            Dim strId As String
            If ParentId.StartsWith("S") Then 'Share
                ParentId = (ParentId.Replace("S", "") * -1)
            Else 'Folder
                ParentId = ParentId.Replace("W", "")
            End If

            Dim objFolders As New Folders(conSqlCompany, False)

            Dim intNewFolderId As Integer

            intNewFolderId = objFolders.InsertFolder(Me.mobjUser.ImagesUserId, FolderName, ParentId, dtPermissions)

            If intNewFolderId < 0 Then
                Dim intCount As Integer = 1
                Dim tempName As String = FolderName
                Do While intNewFolderId < 0
                    FolderName = tempName & "[" & intCount.ToString & "]"
                    intNewFolderId = objFolders.InsertFolder(Me.mobjUser.ImagesUserId, FolderName, ParentId, dtPermissions)
                    intCount += 1
                Loop
            End If

            Dim drChild As DataRow
            drChild = dtPermissions.Rows(0)

            Dim sbFolderCommand As New System.Text.StringBuilder

            sbFolderCommand.Append("browseFolder('" & intNewFolderId.ToString & "');setValue('W" & intNewFolderId.ToString & "');")

            If drChild("CanAddFolder") = True Then
                sbFolderCommand.Append("enableFolder();")
            Else
                sbFolderCommand.Append("disableFolder();")
            End If

            If drChild("CanAddFilesToFolder") = True Then
                sbFolderCommand.Append("enableAdd();")
            Else
                sbFolderCommand.Append("disableAdd();")
            End If

            If drChild("CanRenameFolder") = True Then
                sbFolderCommand.Append("enableRename();")
            Else
                sbFolderCommand.Append("disableRename();")
            End If

            If drChild("CanDeleteFolder") = True Then
                sbFolderCommand.Append("enableDelete();")
            Else
                sbFolderCommand.Append("disableDelete();")
            End If

            If drChild("CanChangeSecurity") = True Then
                sbFolderCommand.Append("enableSecurity();")
            Else
                sbFolderCommand.Append("disableSecurity();")
            End If

            sbFolderCommand.Append("enableFavorite();disableCabinet();")

            Dim values(3) As String
            values(0) = intNewFolderId.ToString
            values(1) = sbFolderCommand.ToString
            values(2) = FolderName

            Return values

        Catch ex As Exception

        Finally

            If conSqlCompany.State <> ConnectionState.Closed Then
                conSqlCompany.Close()
                conSqlCompany.Dispose()
            End If

        End Try

    End Function

    <WebMethod(EnableSession:=True)> _
    Public Function AddNewFolder(ByVal ParentId As String, ByVal FolderName As String) As String()

        Dim conSqlCompany As New SqlClient.SqlConnection

        Try

            Dim Request As HttpRequest = Context.Request
            Me.mobjUser = BuildUserObject(Request.Cookies("docAssist"))
            conSqlCompany = Functions.BuildConnection(Me.mobjUser)

            Dim dtPermissions As New DataTable

            Dim strId As String
            If ParentId.StartsWith("S") Then 'Share
                ParentId = (ParentId.Replace("S", "") * -1)
            Else 'Folder
                ParentId = ParentId.Replace("W", "")
            End If

            Dim objFolders As New Folders(conSqlCompany, False)

            Dim intNewFolderId As Integer

            intNewFolderId = objFolders.InsertFolder(Me.mobjUser.ImagesUserId, FolderName, ParentId, dtPermissions)

            Dim originalName As String = FolderName
            If intNewFolderId < 0 Then
                Dim intCount As Integer = 1
                Do While intNewFolderId < 0
                    FolderName = originalName & "[" & intCount.ToString & "]"
                    intNewFolderId = objFolders.InsertFolder(Me.mobjUser.ImagesUserId, FolderName, ParentId, dtPermissions)
                    intCount += 1
                Loop
            End If

            Dim drChild As DataRow
            drChild = dtPermissions.Rows(0)

            Dim sbFolderCommand As New System.Text.StringBuilder

            sbFolderCommand.Append("browseFolder('" & intNewFolderId.ToString & "');setValue('W" & intNewFolderId.ToString & "');")

            If drChild("CanAddFolder") = True Then
                sbFolderCommand.Append("enableFolder();")
            Else
                sbFolderCommand.Append("disableFolder();")
            End If

            If drChild("CanAddFilesToFolder") = True Then
                sbFolderCommand.Append("enableAdd();")
            Else
                sbFolderCommand.Append("disableAdd();")
            End If

            If drChild("CanRenameFolder") = True Then
                sbFolderCommand.Append("enableRename();")
            Else
                sbFolderCommand.Append("disableRename();")
            End If

            If drChild("CanDeleteFolder") = True Then
                sbFolderCommand.Append("enableDelete();")
            Else
                sbFolderCommand.Append("disableDelete();")
            End If

            If drChild("CanChangeSecurity") = True Then
                sbFolderCommand.Append("enableSecurity();")
            Else
                sbFolderCommand.Append("disableSecurity();")
            End If

            sbFolderCommand.Append("enableFavorite();disableCabinet();")

            Return (intNewFolderId.ToString & "�" & sbFolderCommand.ToString & "�" & FolderName).Split("�")

        Catch ex As Exception

        Finally

            If conSqlCompany.State <> ConnectionState.Closed Then
                conSqlCompany.Close()
                conSqlCompany.Dispose()
            End If

        End Try

    End Function


    <WebMethod(EnableSession:=True)> _
   Public Function ExpandNode(ByVal ParentId As String) As String()

        Try

            Dim Request As HttpRequest = Context.Request
            Me.mobjUser = BuildUserObject(Request.Cookies("docAssist"))
            Dim objFolders As New Folders(Functions.BuildConnection(Me.mobjUser))

            Dim dt As New DataTable

            If ParentId.StartsWith("S") Then
                dt = objFolders.FoldersGetById(Me.mobjUser.ImagesUserId, CInt(ParentId.Replace("S", "")) * -1)
            Else
                dt = objFolders.FoldersGetById(Me.mobjUser.ImagesUserId, ParentId.Replace("W", ""))
            End If

            Dim nodes(dt.Rows.Count - 1) As String

            Dim row As Integer = 0

            For Each dr As DataRow In dt.Rows

                Dim sb As New System.Text.StringBuilder
                sb.Append(dr("FolderName") & "�") 'Name
                sb.Append("W" & dr("FolderId") & "�") 'ID

                If dr("HasChildFolder") = True Then 'Children
                    sb.Append("1�")
                Else
                    sb.Append("0�")
                End If

                If dr("CanViewFolderOnly") = True Then
                    sb.Append("dF();dM();document.all.btnNoAccess.click();stopProgressDisplay();")
                Else

                    Dim sbFolderCommand As New System.Text.StringBuilder

                    sbFolderCommand.Append("bF('" & dr("FolderId") & "');")

                    If dr("CanAddFolder") = True Then
                        sbFolderCommand.Append("eF();")
                    Else
                        sbFolderCommand.Append("dF();")
                    End If

                    If dr("CanAddFilesToFolder") = True Then
                        sbFolderCommand.Append("eA();")
                    Else
                        sbFolderCommand.Append("dA();")
                    End If

                    If dr("CanRenameFolder") = True Then
                        sbFolderCommand.Append("eR();")
                    Else
                        sbFolderCommand.Append("dR();")
                    End If

                    If dr("CanDeleteFolder") = True Then
                        sbFolderCommand.Append("eD('" & dr("FolderName").Replace("'", "\'") & "');eM();")
                    Else
                        sbFolderCommand.Append("dD();")
                    End If

                    If dr("CanChangeSecurity") = True Then
                        sbFolderCommand.Append("eS();")
                    Else
                        sbFolderCommand.Append("dS();")
                    End If

                    'sbFolderCommand.Append("eF();dC();setValue('" & node.ID & "');")
                    sbFolderCommand.Append("eFa();dC();")

                    sb.Append(sbFolderCommand.ToString.Replace("'", "\'"))

                End If

                nodes(row) = sb.ToString

                'node.Text = dr("FolderName")
                'node.ID = "W" & dr("FolderId")
                'node.ImageUrl = IMAGE_FOLDER
                'node.AutoPostBackOnSelect = False
                'node.LabelPadding = 3

                'If Request.QueryString("Ref") = "Popup" Then
                '    If dr("CanAddFilesToFolder") = False Then
                '        node.ID = "NOACCESS"
                '    End If
                'End If

                'If Request.QueryString("Ref") <> "Popup" Then

                '    If dr("CanViewFolderOnly") = True Then

                '        node.AutoPostBackOnSelect = False
                '        node.ClientSideCommand = "disableFavorite();disableMove();document.all.btnNoAccess.click();stopProgressDisplay();"

                '    Else

                '        Dim sbFolderCommand As New System.Text.StringBuilder

                '        sbFolderCommand.Append("bF('" & dr("FolderId") & "');")

                '        If dr("CanAddFolder") = True Then
                '            sbFolderCommand.Append("enableFolder();")
                '        Else
                '            sbFolderCommand.Append("disableFolder();")
                '        End If

                '        If dr("CanAddFilesToFolder") = True Then
                '            sbFolderCommand.Append("enableAdd();")
                '        Else
                '            sbFolderCommand.Append("disableAdd();")
                '        End If

                '        If dr("CanRenameFolder") = True Then
                '            sbFolderCommand.Append("enableRename();")
                '        Else
                '            sbFolderCommand.Append("disableRename();")
                '        End If

                '        If dr("CanDeleteFolder") = True Then
                '            sbFolderCommand.Append("enableDelete('" & node.Text.ToString.Replace("'", "\'") & "');enableMove();")
                '        Else
                '            sbFolderCommand.Append("disableDelete();")
                '        End If

                '        If dr("CanChangeSecurity") = True Then
                '            sbFolderCommand.Append("enableSecurity();")
                '        Else
                '            sbFolderCommand.Append("disableSecurity();")
                '        End If

                '        sbFolderCommand.Append("enableFavorite();disableCabinet();setValue('" & node.ID & "');")

                '        node.ClientSideCommand = sbFolderCommand.ToString

                '    End If

                'End If

                row += 1

            Next

            Return nodes

        Catch ex As Exception

            Throw New Exception("Error loading node information.")

        Finally

        End Try

    End Function

    <WebMethod(EnableSession:=True)> _
    Public Function BuildPageAlt(ByVal intVersionId As Integer, ByVal intPageNo As Integer, ByVal guidAccountId As Guid, ByVal Quality As Integer) As String()

        Dim strReturn() As String = " ,  , ".Split(",")
        Dim conSql As New SqlClient.SqlConnection
        Dim MaxQuality As String
        Dim Image As AtalaImage

        Session("CurrentPage") = intPageNo

        Try

            Dim Request As HttpRequest = Context.Request
            Me.mobjUser = BuildUserObject(Request.Cookies("docAssist"))
            Me.mobjUser.AccountId = guidAccountId
            conSql = BuildConnection(Me.mobjUser)

            Dim dsVersionControl As Data.dsVersionControl

            If Not Session(intVersionId & "VersionControl") Is Nothing Then
                dsVersionControl = Session(intVersionId & "VersionControl")
            Else
                dsVersionControl = Accucentric.docAssist.Data.GetVersionControl(intVersionId, conSql, Me.mobjUser.ImagesUserId)
                Session(intVersionId & "VersionControl") = dsVersionControl
            End If

            Dim objFind(1) As Object
            objFind(0) = intVersionId
            objFind(1) = intPageNo
            dsVersionControl.ImageVersionDetail.PrimaryKey = New DataColumn() {dsVersionControl.ImageVersionDetail.Columns("VersionId"), dsVersionControl.ImageVersionDetail.Columns("SeqId")}
            Dim drVersionImageDetail As Data.dsVersionControl.ImageVersionDetailRow = dsVersionControl.ImageVersionDetail.Rows.Find(objFind)

            'RotationDegrees = drVersionImageDetail.RotateImage

            Dim intImageDetID As Integer = drVersionImageDetail.ImageDetID
            Dim sqlCmd As New SqlClient.SqlCommand("acsImageDetail", conSql)
            sqlCmd.CommandType = CommandType.StoredProcedure
            sqlCmd.Parameters.Add("@ImageDetId", intImageDetID)

            If Not conSql.State = ConnectionState.Open Then conSql.Open()

            Dim dr As SqlClient.SqlDataReader = sqlCmd.ExecuteReader(CommandBehavior.SingleRow)
            Dim bytImage As Byte()

            'Read the image
            If dr.Read() Then
                bytImage = dr("Image")
            End If
            dr.Close()

            If Not bytImage Is Nothing Then

                If bytImage.Length > 0 Then

                    Dim fileName As String = System.Guid.NewGuid().ToString & ".tif"
                    Dim saveFileName As String = Server.MapPath("tmp\" & fileName)

                    'Load byte array from database
                    Image = Atalasoft.Imaging.AtalaImage.FromByteArray(bytImage)

                    'Dim resample As New ImageProcessing.ResampleCommand(New Size(Image.Width / 2, Image.Height / 2), ResampleMethod.Default)
                    'Image = resample.Apply(Image).Image

                    'TODO: Rotations?
                    'Dim rotate As New Atalasoft.Imaging.ImageProcessing.Transforms.RotateCommand(RotationDegrees * -1)
                    'Image = rotate.Apply(Image).Image

                    Dim tiffEncode As New TiffEncoder(TiffCompression.Lzw, False)

                    Select Case Image.ColorDepth
                        Case 1
                            MaxQuality = 1
                        Case 4
                            MaxQuality = 50
                        Case 8
                            MaxQuality = 100
                        Case 24
                            MaxQuality = 100
                    End Select

                    Select Case Quality
                        Case DownloadQuality.G4
                            tiffEncode.Compression = TiffCompression.Group4FaxEncoding
                            Image.Resolution = New Atalasoft.Imaging.Dpi(150, 150, ResolutionUnit.DotsPerInch)
                            If Image.ColorDepth <> 1 Then Image = Image.GetChangedPixelFormat(Atalasoft.Imaging.PixelFormat.Pixel1bppIndexed)
                        Case DownloadQuality.GREYSCALE
                            tiffEncode.Compression = TiffCompression.Lzw
                            Image.Resolution = New Atalasoft.Imaging.Dpi(200, 200, ResolutionUnit.DotsPerInch)
                            If Image.ColorDepth <> 8 Then Image = Image.GetChangedPixelFormat(Atalasoft.Imaging.PixelFormat.Pixel8bppGrayscale)
                        Case DownloadQuality.COLOR
                            tiffEncode.Compression = TiffCompression.Lzw
                            Image.Resolution = New Atalasoft.Imaging.Dpi(300, 300, ResolutionUnit.DotsPerInch)
                            If Image.ColorDepth <> 24 Then Image = Image.GetChangedPixelFormat(Atalasoft.Imaging.PixelFormat.Pixel24bppBgr)
                    End Select

                    Image.Save(saveFileName, tiffEncode, Nothing)

                    strReturn(0) = System.IO.Path.GetFileName(saveFileName)
                    strReturn(1) = MaxQuality
                    strReturn(2) = Quality

                    Try
                        'Track the page request
                        Functions.TrackPageRequest(Me.mobjUser, "Web View", "Out", intVersionId, intImageDetID, ConfigurationSettings.AppSettings("Connection"))
                    Catch ex As Exception

                    End Try

                    Return strReturn

                End If

            End If

        Catch ex As Exception

            strReturn = Nothing
            Throw New System.Web.Services.Protocols.SoapException(ex.Message & vbCrLf & ex.StackTrace, System.Xml.XmlQualifiedName.Empty, "text")

        Finally

            Image.Dispose()

            If Not conSql Is Nothing Then
                If Not conSql.State = ConnectionState.Closed Then conSql.Close()
            End If
        End Try

        strReturn(0) = strReturn(0).ToString.Replace("\", "/")
        strReturn(1) = MaxQuality
        strReturn(2) = Quality

        Return strReturn

    End Function

    <WebMethod(EnableSession:=True)> _
    Public Function SaveImage(ByVal mUserID As String, ByVal mAccountID As String, ByVal iDocumentID As Integer, ByVal dsAttributes As DataSet, ByVal Filename As String, _
    ByVal intVersionID As Integer, ByVal blnAppendPages As Boolean, ByVal iPageInsert As Integer, ByVal strSaveType As String) As String

        Try

            If strSaveType.Trim.Length = 0 Then strSaveType = "Web Indexer"

            Dim mSessionID As String
            Dim objEncryption As New Encryption.Encryption

            'Get the users current sessionid
            Dim consqluser As New SqlConnection
            consqluser.ConnectionString = objEncryption.Decrypt(ConfigurationSettings.AppSettings("Connection"))
            If Not consqluser.State = ConnectionState.Open Then consqluser.Open()
            Dim dtUser As New DataTable
            Dim cmdUSql As New SqlClient.SqlCommand("SELECT SessionID FROM USERS WHERE UserID = @UserID", consqluser)
            cmdUSql.Parameters.Add("@UserID", mUserID)
            cmdUSql.CommandTimeout = 120
            Dim daSql As New SqlClient.SqlDataAdapter(cmdUSql)
            daSql.Fill(dtUser)

            If dtUser.Rows.Count <= 0 Then
                Return -1
                Exit Function
            Else
                mSessionID = dtUser.Rows(0).Item("SessionID")
            End If

            Dim consql As New SqlClient.SqlConnection

            Me.mobjUser = BuildUserObject(mSessionID, mAccountID)

            Dim strtempfilename As New System.Guid

            If Not Me.mobjUser Is Nothing Then

                consql = BuildConnection(Me.mobjUser)

                Dim strSavedocPath As String = Server.MapPath("./scanFiles/") & Filename

                Dim objSave As New Accucentric.docAssist.Data.ImageSave(Me.mobjUser.ImagesUserId, consql)

                If intVersionID <> 0 Then
                    Dim dsNewVersion As New Data.dsVersionControl
                    dsNewVersion = Data.GetVersionControl(intVersionID, consql)

                    'If blnAppendPages is true we are appending pages so we dont want to delete existing pages
                    Dim dtOriginal As New DataTable
                    Dim drOriginal As DataRow

                    If blnAppendPages = False Then
                        For Each dr As Data.dsVersionControl.ImageVersionDetailRow In dsNewVersion.ImageVersionDetail
                            dr.Delete()
                        Next
                    End If

                    Dim drVersion As Data.dsVersionControl.ImageVersionRow
                    drVersion = dsNewVersion.ImageVersion.Rows(0)

                    Dim iPageStart As Integer = 1

                    If iPageInsert > dsNewVersion.ImageVersionDetail.Rows.Count Then iPageInsert = (dsNewVersion.ImageVersionDetail.Rows.Count + 1)

                    ' Create new image details
                    Dim iTotalPages As Integer = Functions.PageCount(strSavedocPath)
                    Dim iOriginalPages As Integer = dsNewVersion.ImageDetail.Rows.Count

                    If blnAppendPages = True Then
                        iPageStart = iPageInsert 'dsNewVersion.ImageDetail.Rows.Count + 1
                        iTotalPages = iTotalPages ' dsNewVersion.ImageDetail.Rows.Count + ix.Pages
                    Else
                        iTotalPages = iTotalPages
                    End If

                    'Beginning of document
                    If iPageStart = 0 Then
                        If iPageInsert <= drVersion.SeqTotal Then
                            'Create space for the new pages
                            Dim iTotalPagesToInsert As Integer = iTotalPages
                            If iTotalPagesToInsert > 0 Then
                                Dim iCount As Integer
                                For iCount = (dsNewVersion.ImageVersionDetail.Rows.Count) To (iPageInsert + 1) Step -1
                                    dsNewVersion.ImageVersionDetail.Rows(iCount - 1).Item("SeqID") = dsNewVersion.ImageVersionDetail.Rows(iCount - 1).Item("SeqID") + iTotalPagesToInsert
                                Next
                            End If
                        End If
                    End If

                    'In between document
                    If iPageStart < (iOriginalPages + 1) And iPageStart <> 0 And blnAppendPages = True Then
                        If iPageInsert <= drVersion.SeqTotal Then
                            'Create space for the new pages
                            Dim iTotalPagesToInsert As Integer = iTotalPages
                            If iTotalPagesToInsert > 0 Then
                                Dim iCount As Integer
                                For iCount = (dsNewVersion.ImageVersionDetail.Rows.Count) To (iPageInsert) Step -1
                                    dsNewVersion.ImageVersionDetail.Rows(iCount - 1).Item("SeqID") = dsNewVersion.ImageVersionDetail.Rows(iCount - 1).Item("SeqID") + iTotalPagesToInsert
                                Next
                            End If
                        End If
                    End If

                    If blnAppendPages = False Then iPageStart = 0

                    'Beginning of document
                    If iPageStart = 0 Then
                        For intCount As Integer = (iPageStart + 1) To iTotalPages
                            Dim drImageDetail As Data.dsVersionControl.ImageDetailRow = dsNewVersion.ImageDetail.NewRow
                            dsNewVersion.ImageDetail.Rows.Add(drImageDetail)
                            Dim drImageVersionDetail As Data.dsVersionControl.ImageVersionDetailRow = dsNewVersion.ImageVersionDetail.NewRow()
                            drImageVersionDetail.ImportFileName = strSavedocPath
                            drImageVersionDetail.ImportPageNo = intCount '- iOriginalPages
                            drImageVersionDetail.SeqID = (iPageInsert) + intCount
                            drImageVersionDetail.VersionID = drVersion.VersionID
                            drImageVersionDetail.ImageDetID = drImageDetail.ImageDetID
                            dsNewVersion.ImageVersionDetail.Rows.Add(drImageVersionDetail)
                        Next
                    End If

                    'In between document
                    If iPageStart < (iOriginalPages + 1) And iPageStart <> 0 And blnAppendPages = True Then
                        For intCount As Integer = iPageStart To (iPageStart + iTotalPages) - 1
                            Dim drImageDetail As Data.dsVersionControl.ImageDetailRow = dsNewVersion.ImageDetail.NewRow
                            dsNewVersion.ImageDetail.Rows.Add(drImageDetail)
                            Dim drImageVersionDetail As Data.dsVersionControl.ImageVersionDetailRow = dsNewVersion.ImageVersionDetail.NewRow()
                            drImageVersionDetail.ImportFileName = strSavedocPath
                            drImageVersionDetail.ImportPageNo = (intCount - iPageStart) + 1
                            drImageVersionDetail.SeqID = intCount
                            drImageVersionDetail.VersionID = drVersion.VersionID
                            drImageVersionDetail.ImageDetID = drImageDetail.ImageDetID
                            dsNewVersion.ImageVersionDetail.Rows.Add(drImageVersionDetail)
                        Next
                    End If

                    'End of document
                    If iPageStart = (iOriginalPages + 1) Then
                        For intCount As Integer = iPageStart To ((iPageStart + iTotalPages) - 1)
                            Dim drImageDetail As Data.dsVersionControl.ImageDetailRow = dsNewVersion.ImageDetail.NewRow
                            dsNewVersion.ImageDetail.Rows.Add(drImageDetail)
                            Dim drImageVersionDetail As Data.dsVersionControl.ImageVersionDetailRow = dsNewVersion.ImageVersionDetail.NewRow()
                            drImageVersionDetail.ImportFileName = strSavedocPath
                            drImageVersionDetail.ImportPageNo = intCount - iOriginalPages  '- iOriginalPages
                            drImageVersionDetail.SeqID = intCount
                            drImageVersionDetail.VersionID = drVersion.VersionID
                            drImageVersionDetail.ImageDetID = drImageDetail.ImageDetID
                            dsNewVersion.ImageVersionDetail.Rows.Add(drImageVersionDetail)
                        Next
                    End If

                    Dim iNewVersionID As Integer
                    iNewVersionID = objSave.UpdateImage(dsNewVersion, , , , , , False)

                    'Log recent item
                    Try
                        Dim blnReturn As Boolean = Functions.TrackRecentItem(Me.mobjUser, "SV", inewversionid)
                        If Not blnReturn Then
                            'error logging recent item
                        End If
                    Catch ex As Exception
                    End Try

                    Return iNewVersionID

                Else 'Save new image

                    Dim objVersion As New Accucentric.docAssist.Data.VersionControl(intVersionID, consql)
                    Dim drImageVersionDetail As ImageVersionDetailRow
                    Dim drImageAttributes As ImageAttributesRow

                    If dsAttributes.Tables.Count > 0 Then
                        Dim drAttributes As DataRow
                        For Each drAttributes In dsAttributes.Tables("Attributes").Rows
                            drImageAttributes = objVersion.ImageAttributes.NewRow
                            drImageAttributes.AttributeId = drAttributes("AttributeID")
                            drImageAttributes.AttributeValue = drAttributes("AttributeValue")
                            objVersion.ImageAttributes.Rows.Add(drImageAttributes)
                        Next
                    End If

                    Dim annotations As New DataTable
                    annotations = Functions.extractAnnotations(strSavedocPath)

                    Dim iNewVersionID As Integer = objSave.SaveNewImage(strSavedocPath, iDocumentID, objVersion.ImageAttributes, Me.mobjUser, ConfigurationSettings.AppSettings("Connection"), _
                    strSaveType, , annotations, IIf(dsAttributes.Tables.Contains("WorkflowData"), dsAttributes.Tables("WorkflowData"), Nothing))

                    Try
                        System.IO.File.Delete(strSavedocPath)
                    Catch ex As Exception

                    End Try

                    'Log recent item
                    Try
                        Dim blnReturn As Boolean = Functions.TrackRecentItem(Me.mobjUser, "SV", inewversionid)
                        If Not blnReturn Then
                            'error logging recent item
                        End If
                    Catch ex As Exception
                    End Try

                    Return iNewVersionID

                End If

            End If

        Catch ex As Exception

            Dim conMaster As SqlClient.SqlConnection
            conMaster = Functions.BuildMasterConnection
            Functions.LogException(mobjUser.UserId.ToString, mobjUser.AccountId.ToString, "", "ERROR SAVING DOCUMENT: " & ex.Message, ex.StackTrace, Server.MachineName, "", "", conMaster, True)

        End Try

    End Function

    <WebMethod(EnableSession:=True)> _
    Public Function SetWorkflowState(ByVal UserId As Integer, ByVal WorkflowID As Integer, ByVal QueueID As Integer, ByVal State As Boolean)

        Dim conCompany As New SqlClient.SqlConnection

        Try

            Dim Request As HttpRequest = Context.Request
            Me.mobjUser = BuildUserObject(Request.Cookies("docAssist"))
            conCompany = Functions.BuildConnection(Me.mobjUser)

            Dim dsDocuments As New DataSet
            Dim objWf As New Workflow(conCompany, False)

            objWf.WFDashBoardStateSet(UserId, WorkflowID, QueueID, State)

        Catch ex As Exception

            'Throw New Exception("Error loading node information.")

        Finally

            If conCompany.State <> ConnectionState.Closed Then
                conCompany.Close()
                conCompany.Dispose()
            End If

        End Try

    End Function

    Shared Sub New()
        Dim pdfDec As New Atalasoft.Imaging.Codec.Pdf.PdfDecoder
        pdfDec.RenderSettings.AnnotationSettings = 0
        pdfDec.Resolution = 175
        RegisteredDecoders.Decoders.Add(pdfDec)
    End Sub

#End Region

End Class
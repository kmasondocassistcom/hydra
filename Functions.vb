Imports Accucentric.docAssist
Imports Accucentric.docAssist.Web
Imports System.Data.SqlClient
Imports ICSharpCode.SharpZipLib
Imports Accucentric.docAssist.Web.Security
Imports Atalasoft.Imaging.Codec.Pdf
Imports System.IO
Imports System.IO.File
Imports System.Web.UI
Imports Atalasoft.Imaging
Imports Atalasoft.Imaging.ImageProcessing
Imports Atalasoft.Annotate
Imports Atalasoft.Annotate.UI
Imports Atalasoft.Annotate.Renderer
Imports Atalasoft.Imaging.Codec.Tiff
Imports Atalasoft.Imaging.Codec
Imports System.Text.RegularExpressions

Public Module Functions

    Public Enum Quality
        NORMAL_G4 = 0
        HIGH_GRAYSCALE = 1
        BEST_COLOR = 2
    End Enum

    Public Enum WorkflowMode
        NO_ACCESS = 0
        AUTHORIZED_SUBMITTER_NOT_SUBMITTED = 1
        AUTHORIZED_SUBMITTER_SUBMITTED = 2
        AUTHORIZED_SUBMITTER_RESUBMIT = 3
        VIEW_ACCESS_REVIEWER_PENDING = 10
        VIEW_ACCESS_REVIEWER_NOT_PENDING = 11
        ACTION_ACCESS_REVIEWER_PENDING = 20
        ACTION_ACCESS_REVIEWER_NOT_PENDING = 21
        REVIEWED = 30
    End Enum

    Public Enum ConnectionType
        Master = 0
        Company = 1
        Notifications_Main = 2
        Notifications_Workflow = 3
        Notifications_Folder = 4
        Notifications_DataSynch = 5
        FullText = 6
    End Enum

    Public Enum FolderSecurity
        FILES = 3
        FOLDER = 4
        INHERIT = 5
        SHARE_ADMIN = 6
    End Enum

    Public Enum FileType
        SCAN = 1
        SCAN_ATTACHMENT = 2
        ATTACHMENT = 3
    End Enum

    Public Enum ModuleCode
        DOCUMENT_TYPES_ATTRIBUTES = 1
        ANNOTATION = 2
        REDACTION = 3
        APP_INTEGRATION = 4
        DATA_SYNC = 5
        WORKFLOW = 6
        TEMPLATES = 7
        PRINT_DRIVER = 8
        VERSION_CONTROL = 9
        FTP = 10
        DIRECT_SCANNER_SUPPORT = 11
        FILE_REPOSITORY = 12
        FULL_TEXT_SEARCH = 13
        BURSTING = 14
        USER_ADMIN = 15
        INBOUND_EMAIL = 16
        DISTRIBUTION = 17
        FOLDER_NOTIFICATIONS = 25
        SCAN_V2 = 26
        TIMEFORGE = 27
        VIEWER_V2 = 28
        WORKSPACE = 29
        FULL_TEXT = 30
    End Enum

    Public Enum EmailType
        NEW_USER = 1
        SHARE_DOCUMENT = 2
        SHARE_FOLDER = 3
    End Enum

    Public Enum SearchType
        QUICK = 0
        ADVANCED = 1
        FOLDER = 2
    End Enum

    Friend Function GetAttributeList(ByVal intAttributeId As Integer, ByVal conSql As SqlClient.SqlConnection) As String()

        Try

            Dim cmdSql As New SqlClient.SqlCommand("SELECT * FROM AttributesLists WHERE AttributeId = @AttributeId", conSql)
            cmdSql.Parameters.Add("@AttributeId", intAttributeId)

            Dim dt As New Accucentric.docAssist.Data.Images.AttributesLists

            Dim oDb As New Accucentric.docAssist.Data.Images.db
            oDb.PopulateTable(dt, cmdSql)

            Dim dvAttributesLists As New DataView(dt, Nothing, dt.ValueDescription.ColumnName, DataViewRowState.CurrentRows)
            Dim dvi As DataRowView
            Dim x As Integer = 0
            Dim sbValues As New System.Text.StringBuilder

            For Each dvi In dvAttributesLists
                sbValues.Append(dvi("ValueDescription") & ";")
            Next

            Dim strListItems() As String = sbValues.ToString.Split(";")

            Return strListItems

        Catch ex As Exception

        End Try

    End Function

    Friend Sub AddValues(ByVal intAttributeId As Integer, ByRef ddlAttributeValues As DropDownList, ByRef conSql As SqlClient.SqlConnection, ByVal blnAddBlankRow As Boolean)

        Dim cmdSql As New SqlClient.SqlCommand("SELECT * FROM AttributesLists WHERE AttributeId = @AttributeId", conSql)
        cmdSql.Parameters.Add("@AttributeId", intAttributeId)

        Dim dt As New Accucentric.docAssist.Data.Images.AttributesLists

        Dim oDb As New Accucentric.docAssist.Data.Images.db
        oDb.PopulateTable(dt, cmdSql)

        ' Add Blank Row
        If blnAddBlankRow = True Then
            Dim dr As Accucentric.docAssist.Data.Images.AttributesListsRow = dt.NewRow
            dr.AttributeListId = -1
            dr.ValueDescription = ""
            dt.Rows.Add(dr)
        End If

        Dim dvAttributesLists As New DataView(dt, Nothing, dt.ValueDescription.ColumnName, DataViewRowState.CurrentRows)
        dvAttributesLists.Sort = "AttributeListId"

        ddlAttributeValues.DataSource = dvAttributesLists
        ddlAttributeValues.DataTextField = dt.ValueDescription.ColumnName
        ddlAttributeValues.DataValueField = dt.AttributeListId.ColumnName
        ddlAttributeValues.DataBind()

    End Sub

    Friend Function BuildConnection(ByVal ConnectionType As ConnectionType) As SqlClient.SqlConnection

        Try
            Select Case ConnectionType

                Case ConnectionType.Master

                    Return BuildMasterConnection()

                Case ConnectionType.Company

                Case ConnectionType.Notifications_Main

                    Dim objEncryption As New Encryption.Encryption

                    Dim sqlConMaster As New SqlClient.SqlConnection

                    Try
                        sqlConMaster = BuildMasterConnection()

                        If sqlConMaster.State <> ConnectionState.Open Then sqlConMaster.Open()
                        Dim cmdSql As New SqlClient.SqlCommand("SELECT NSSERVERNAME=OptionValue, NSDBMAIN=(SELECT OptionValue FROM options WHERE OptionKey = 'NSDBMAIN'), NSDBWORKFLOW=(SELECT OptionValue FROM options WHERE OptionKey = 'NSDBWORKFLOW') FROM options WHERE OptionKey = 'NSSERVERNAME'", sqlConMaster)
                        cmdSql.CommandType = CommandType.Text

                        Dim da As New SqlClient.SqlDataAdapter(cmdSql)
                        Dim dtConnection As New DataTable
                        da.Fill(dtConnection)

                        Dim sb As New System.Text.StringBuilder
                        sb.Append("User ID=bluprint;")
                        sb.Append("Password=" & objEncryption.Decrypt(Accucentric.docAssist.Web.Security.Functions.ReadSetting("Password")) & ";")
                        sb.Append("Initial Catalog=" & dtConnection.Rows(0)("NSSERVERNAME") & ";")
                        sb.Append("Data Source=" & dtConnection.Rows(0)("NSDBMAIN"))

                        Dim sqlCon As New SqlClient.SqlConnection(sb.ToString)
                        sqlCon.Open()

                        Return sqlCon

                    Catch ex As Exception

                    Finally
                        If sqlConMaster.State <> ConnectionState.Closed Then
                            sqlConMaster.Close()
                            sqlConMaster.Dispose()
                        End If
                    End Try

                Case ConnectionType.Notifications_Workflow

                    Dim objEncryption As New Encryption.Encryption

                    Dim sqlConMaster As New SqlClient.SqlConnection

                    Try
                        sqlConMaster = BuildMasterConnection()

                        If sqlConMaster.State <> ConnectionState.Open Then sqlConMaster.Open()
                        Dim cmdSql As New SqlClient.SqlCommand("SELECT NSSERVERNAME=OptionValue, NSDBMAIN=(SELECT OptionValue FROM options WHERE OptionKey = 'NSDBMAIN'), NSDBWORKFLOW=(SELECT OptionValue FROM options WHERE OptionKey = 'NSDBWORKFLOW') FROM options WHERE OptionKey = 'NSSERVERNAME'", sqlConMaster)
                        cmdSql.CommandType = CommandType.Text

                        Dim da As New SqlClient.SqlDataAdapter(cmdSql)
                        Dim dtConnection As New DataTable
                        da.Fill(dtConnection)

                        Dim sb As New System.Text.StringBuilder
                        sb.Append("User ID=bluprint;")
                        sb.Append("Password=" & objEncryption.Decrypt(Accucentric.docAssist.Web.Security.Functions.ReadSetting("Password")) & ";")
                        sb.Append("Data Source=" & dtConnection.Rows(0)("NSSERVERNAME") & ";")
                        sb.Append("Initial Catalog=" & dtConnection.Rows(0)("NSDBWORKFLOW"))

                        Dim sqlCon As New SqlClient.SqlConnection(sb.ToString)
                        sqlCon.Open()

                        Return sqlCon

                    Catch ex As Exception

                    Finally
                        If sqlConMaster.State <> ConnectionState.Closed Then
                            sqlConMaster.Close()
                            sqlConMaster.Dispose()
                        End If
                    End Try

                Case ConnectionType.Notifications_Folder

                    Dim objEncryption As New Encryption.Encryption

                    Dim sqlConMaster As New SqlClient.SqlConnection

                    Try
                        sqlConMaster = BuildMasterConnection()

                        If sqlConMaster.State <> ConnectionState.Open Then sqlConMaster.Open()
                        Dim cmdSql As New SqlClient.SqlCommand("SELECT NSSERVERNAME=OptionValue, NSDBMAIN=(SELECT OptionValue FROM options WHERE OptionKey = 'NSDBMAIN'), NSDBFOLDER=(SELECT OptionValue FROM options WHERE OptionKey = 'NSDBFOLDER') FROM options WHERE OptionKey = 'NSSERVERNAME'", sqlConMaster)
                        cmdSql.CommandType = CommandType.Text

                        Dim da As New SqlClient.SqlDataAdapter(cmdSql)
                        Dim dtConnection As New DataTable
                        da.Fill(dtConnection)

                        Dim sb As New System.Text.StringBuilder
                        sb.Append("User ID=bluprint;")
                        sb.Append("Password=" & objEncryption.Decrypt(Accucentric.docAssist.Web.Security.Functions.ReadSetting("Password")) & ";")
                        sb.Append("Data Source=" & dtConnection.Rows(0)("NSSERVERNAME") & ";")
                        sb.Append("Initial Catalog=" & dtConnection.Rows(0)("NSDBFOLDER"))

                        Dim sqlCon As New SqlClient.SqlConnection(sb.ToString)
                        sqlCon.Open()

                        Return sqlCon


                    Catch ex As Exception

                    Finally

                        If sqlConMaster.State <> ConnectionState.Closed Then
                            sqlConMaster.Close()
                            sqlConMaster.Dispose()
                        End If

                    End Try

                Case ConnectionType.Notifications_DataSynch

                    Dim objEncryption As New Encryption.Encryption

                    Dim sqlConMaster As New SqlClient.SqlConnection

                    Try
                        sqlConMaster = BuildMasterConnection()

                        If sqlConMaster.State <> ConnectionState.Open Then sqlConMaster.Open()
                        Dim cmdSql As New SqlClient.SqlCommand("SELECT NSSERVERNAME=OptionValue, NSDBMAIN=(SELECT OptionValue FROM options WHERE OptionKey = 'NSDBMAIN'), NSDBDATASYNC=(SELECT OptionValue FROM options WHERE OptionKey = 'NSDBDATASYNC') FROM options WHERE OptionKey = 'NSSERVERNAME'", sqlConMaster)
                        cmdSql.CommandType = CommandType.Text

                        Dim da As New SqlClient.SqlDataAdapter(cmdSql)
                        Dim dtConnection As New DataTable
                        da.Fill(dtConnection)

                        Dim sb As New System.Text.StringBuilder
                        sb.Append("User ID=bluprint;")
                        sb.Append("Password=" & objEncryption.Decrypt(Accucentric.docAssist.Web.Security.Functions.ReadSetting("Password")) & ";")
                        sb.Append("Data Source=" & dtConnection.Rows(0)("NSSERVERNAME") & ";")
                        sb.Append("Initial Catalog=" & dtConnection.Rows(0)("NSDBDATASYNC"))

                        Dim sqlCon As New SqlClient.SqlConnection(sb.ToString)
                        sqlCon.Open()

                        Return sqlCon

                    Catch ex As Exception

                    Finally

                        If sqlConMaster.State <> ConnectionState.Closed Then
                            sqlConMaster.Close()
                            sqlConMaster.Dispose()
                        End If

                    End Try

            End Select

        Catch ex As Exception

        End Try

    End Function

    Public Function BuildFullTextConnection(ByVal User As Accucentric.docAssist.Web.Security.User) As SqlClient.SqlConnection

        Dim objEncryption As New Encryption.Encryption

        Dim sqlConMaster As New SqlClient.SqlConnection

        Try
            sqlConMaster = BuildMasterConnection()

            If sqlConMaster.State <> ConnectionState.Open Then sqlConMaster.Open()
            Dim cmdSql As New SqlClient.SqlCommand("SELECT SERVER=OptionValue, NSDBMAIN=(SELECT OptionValue FROM options WHERE OptionKey = 'FTSERVERNAME'), NSDBWORKFLOW=(SELECT OptionValue FROM options WHERE OptionKey = 'FTSERVERNAME') FROM options WHERE OptionKey = 'FTSERVERNAME'", sqlConMaster)
            cmdSql.CommandType = CommandType.Text

            Dim da As New SqlClient.SqlDataAdapter(cmdSql)
            Dim dtConnection As New DataTable
            da.Fill(dtConnection)

            Dim sb As New System.Text.StringBuilder
            sb.Append("User ID=bluprint;")
            sb.Append("Password=" & objEncryption.Decrypt(Accucentric.docAssist.Web.Security.Functions.ReadSetting("Password")) & ";")
            sb.Append("Data Source=" & dtConnection.Rows(0)("SERVER") & ";")
            sb.Append("Initial Catalog=" & User.DBName & "_FT")

            Dim sqlCon As New SqlClient.SqlConnection(sb.ToString)
            sqlCon.Open()

            Return sqlCon

        Catch ex As Exception

        Finally
            If sqlConMaster.State <> ConnectionState.Closed Then
                sqlConMaster.Close()
                sqlConMaster.Dispose()
            End If
        End Try


    End Function

    Public Function BuildConnectionbyAccountKey(ByVal PreSharedKey As String) As SqlClient.SqlConnection

        Try

            Dim strMasterDbPassword As String
            strMasterDbPassword = Accucentric.docAssist.Web.Security.Functions.ReadSetting("Password")


            Dim oEnc As New Encryption.Encryption
            Dim strConnectionString As String = oEnc.Decrypt(System.Configuration.ConfigurationSettings.AppSettings("Connection"))
            Dim objSqlConMaster As New SqlClient.SqlConnection(strConnectionString)
            objSqlConMaster.Open()
            oEnc = Nothing

            'Query AccountKeys table
            Dim dtAccount As New DataTable
            Dim sqlCmd As New SqlClient.SqlCommand("SELECT Accounts.AccountID, Accounts.DBName, Accounts.DBServerDNS FROM AccountKeys, Accounts WHERE AccountKeys.AccountID = Accounts.AccountID AND Accounts.Enabled = 1 AND AccountKeys.PresharedKey = '" & PreSharedKey & "'", objSqlConMaster)
            Dim sqlda As New SqlClient.SqlDataAdapter(sqlCmd)

            sqlda.Fill(dtAccount)

            If dtAccount.Rows.Count = 0 Then
                Return Nothing
            Else
                Dim sqlcon As New SqlClient.SqlConnection
                Dim odb As New Accucentric.docAssist.Data.Images.db
                Dim strConnection As String = odb.BuildConnectionString(dtAccount.Rows(0)("DBServerDNS"), dtAccount.Rows(0)("DBName"), Accucentric.docAssist.Web.Security.DBUSERNAME, oEnc.Decrypt(strMasterDbPassword))
                sqlcon.ConnectionString = strConnection
                Return sqlcon
            End If

            Return objSqlConMaster
        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Public Function BuildConnection(ByVal objUser As SECURITY.User) As SqlClient.SqlConnection

        Return Accucentric.docAssist.Web.Security.BuildConnection(objUser)

    End Function

    Public Function BuildMasterConnection() As SqlClient.SqlConnection

        Try
            Dim objEncryption As New Encryption.Encryption
            Dim strConnectionString As String = objEncryption.Decrypt(ConfigurationSettings.AppSettings("Connection"))
            Dim objSqlConMaster As New SqlClient.SqlConnection(strConnectionString)
            objSqlConMaster.Open()
            objEncryption = Nothing
            Return objSqlConMaster
        Catch ex As Exception
            Throw New Exception("Error opening connection to master database.", ex)
        End Try

    End Function

    <System.Diagnostics.DebuggerStepThrough()> _
    Public Function BuildUserObject(ByRef cookie As HttpCookie) As Accucentric.docAssist.Web.Security.User

        Dim rValue As Accucentric.docAssist.Web.Security.User
        Try
            If cookie Is Nothing Then Err.Raise(5)

            'Dim strUserId As String = cookie("UserId")
            'Dim strUserId As String = Accucentric.docAssist.Web.Data.Functions.guidSessionId.ToString
            Dim strUserId As String = cookie("UserId")

            If Not strUserId Is Nothing Then
                If strUserId.Trim.Length <> "36" Then
                    Err.Raise(5)
                Else
                    rValue = New Accucentric.docAssist.Web.Security.User(strUserId, ConfigurationSettings.AppSettings("Connection"))

                End If

                If rValue Is Nothing Then
                    Err.Raise(5)
                End If

                Dim guidAccountId As New System.Guid(cookie("AccountId"))
                If Not rValue.UserId.ToString = System.Guid.Empty.ToString Then
                    If Not rValue.AccountId.ToString = guidAccountId.ToString Then
                        rValue.AccountId = guidAccountId
                    End If
                Else
                    Err.Raise(5)
                End If
            Else
                Err.Raise(5)
            End If
        Catch ex As Exception
            If Err.Number = 5 Then
                Throw New Exception("User is not logged on")
            Else
                Throw New Exception(ex.Message)
            End If
        End Try

        Return rValue

    End Function



    Public Function BuildUserObject(ByVal mGuid As String, ByVal mAccountID As String) As Accucentric.docAssist.Web.Security.User
        Dim rValue As Accucentric.docAssist.Web.Security.User
        Try
            If mGuid Is Nothing Or mAccountID Is Nothing Then Err.Raise(5)

            Dim strUserId As String = mGuid

            If Not strUserId Is Nothing Then
                If strUserId.Trim.Length <> "36" Then
                    Err.Raise(5)
                Else
                    rValue = New Accucentric.docAssist.Web.Security.User(strUserId, ConfigurationSettings.AppSettings("Connection"), , mAccountID)
                End If

                If rValue Is Nothing Then
                    Err.Raise(5)
                End If

                Dim guidAccountId As New System.Guid(mAccountID)
                If Not rValue.UserId.ToString = System.Guid.Empty.ToString Then
                    If Not rValue.AccountId.ToString = guidAccountId.ToString Then
                        rValue.AccountId = guidAccountId
                    End If
                Else
                    Err.Raise(5)
                End If
            Else
                Err.Raise(5)
            End If
        Catch ex As Exception
            If Err.Number = 5 Then
                Throw New Exception("User is not logged on")
            Else
                Throw New Exception(ex.Message)
            End If
        End Try

        Return rValue

    End Function


    Friend Function CheckIndexScan(ByVal objUser As Web.Security.User, Optional ByVal conSql As SqlClient.SqlConnection = Nothing) As Boolean

        Dim bolReturn As Boolean = False
        Dim bolCloseCon As Boolean = False

        Try

            If objUser.SecAdmin Then
                bolReturn = True
                Return bolReturn
            Else
                Dim sb As New System.Text.StringBuilder
                sb.Append("select 1 where exists(select * from Documents, DocumentSources, FTPLines_vw WITH (NOLOCK)")
                sb.Append("WHERE(FTPLines_vw.FTPAccountID = DocumentSources.FTPID)")
                sb.Append("AND Documents.DocumentID = DocumentSources.DocumentID AND FTPLines_vw.Enabled = 1 AND AccountID = @AccountID ")
                sb.Append("AND EXISTS(SELECT * FROM UserSec WHERE Sec_type = 3 and Sec_Value = Documents.DocumentID AND Sec_Level >=2 and UserID = @UserID))")

                Dim cmdsql As New SqlClient.SqlCommand(sb.ToString)
                cmdsql.Parameters.Add("@AccountId", objUser.AccountId)
                cmdsql.Parameters.Add("@UserId", objUser.ImagesUserId)

                If conSql Is Nothing Then
                    conSql = BuildConnection(objUser)
                End If
                cmdsql.Connection = conSql

                If Not conSql.State = ConnectionState.Open Then
                    bolCloseCon = True
                    conSql.Open()
                End If

                bolReturn = cmdsql.ExecuteScalar()

            End If

        Catch ex As Exception
            Throw ex

        Finally
            If (Not conSql Is Nothing) Then
                If bolCloseCon Then
                    If Not conSql.State = ConnectionState.Closed Then
                        conSql.Close()
                    End If
                End If
            End If
        End Try

        Return bolReturn

    End Function

    Friend Function ConnectionString() As String
        Dim strConnectionString As String = ConfigurationSettings.AppSettings("Connection")
        Dim strReturn As String
        Dim objEncryption As New Encryption.Encryption
        strReturn = objEncryption.Decrypt(strConnectionString)

        Return strReturn
    End Function

    Friend Function GetMasterConnectionString() As String
        Return ConfigurationSettings.AppSettings("Connection")
    End Function

    Friend Function ConvertDatasetToString(ByRef ds As DataSet) As String
        Dim strReturn As String
        '
        ' Read the dataset into memory for XML
        '
        Dim memStream As New System.IO.MemoryStream
        ds.WriteXml(memStream)
        memStream.Seek(0, IO.SeekOrigin.Begin)
        Dim readStream As New System.IO.StreamReader(memStream)
        Dim strXml As String = readStream.ReadToEnd()
        'strXml = strXml.Replace(vbCrLf, "")
        strReturn = Convert.ToBase64String(System.Text.Encoding.Unicode.GetBytes(strXml))
        memStream.Close()
        readStream.Close()
        memStream = Nothing
        readStream = Nothing
        Return strReturn
    End Function

    Friend Function ConvertStringTodataset(ByVal str As String) As DataSet

        Dim strBase As String = System.Text.Encoding.Unicode.GetString(Convert.FromBase64String(str))
        Dim strFileName As String = System.IO.Path.GetTempPath() & System.Guid.NewGuid().ToString() & ".xml"
        Dim fileStream As System.IO.StreamWriter = System.IO.File.CreateText(strFileName)
        fileStream.Write(strBase)
        fileStream.Flush()
        fileStream.Close()
        Dim dsreturn As New DataSet("AttributesDataSet")
        dsreturn.ReadXml(strFileName)
        System.IO.File.Delete(strFileName)
        Return dsreturn

    End Function

    Friend Function PrintSecurityLevel(ByVal intDocumentId As Integer, ByVal objUser As Web.Security.User, ByRef conSqlImage As SqlClient.SqlConnection) As Web.Security.SecurityLevelPrint


        Dim rValue As Web.Security.SecurityLevelPrint = Web.Security.SecurityLevelPrint.PrintDisabled
        Dim cmdSql As SqlClient.SqlCommand, bolCloseCon As Boolean = False

        Try
            '
            ' Get the logged on user's id
            '
            If Not conSqlImage.State = ConnectionState.Open Then
                conSqlImage.Open()
                bolCloseCon = True
            End If

            Dim intImagesUserId As Integer = GetImagesUserId(conSqlImage, objUser)
            If Not intImagesUserId > 0 Then Throw New Exception("Invalid User specified")
            '
            ' Get User Security Level for the document 
            '
            cmdSql = New SqlClient.SqlCommand("acsDocumentSecurityPrintLevel", conSqlImage)
            cmdSql.CommandType = CommandType.StoredProcedure
            cmdSql.Parameters.Add("@UserId", intImagesUserId)
            cmdSql.Parameters.Add("@DocumentId", intDocumentId)
            rValue = cmdSql.ExecuteScalar()

        Catch ex As Exception
            Throw ex
        Finally
            If Not conSqlImage.State = ConnectionState.Closed And bolCloseCon Then
                conSqlImage.Close()
            End If
        End Try

        Return rValue
    End Function

    Friend Function DocumentAnnotationSecurityLevel(ByVal intUserId As Integer, ByVal intVersionId As Integer, ByVal conSqlImage As SqlClient.SqlConnection, ByRef blnAnnotation As Boolean, ByRef blnRedaction As Boolean)

        Dim cmdSql As SqlClient.SqlCommand

        Try
            If conSqlImage.State <> ConnectionState.Open Then conSqlImage.Open()
            cmdSql = New SqlClient.SqlCommand("acsAnnotationSecurityByVersionID", conSqlImage)
            cmdSql.CommandType = CommandType.StoredProcedure
            cmdSql.Parameters.Add("@UserId", intUserId)
            cmdSql.Parameters.Add("@VersionId", intVersionId)

            Dim dtAccess As New DataTable
            Dim da As New SqlClient.SqlDataAdapter(cmdSql)
            da.Fill(dtAccess)
            da.Dispose()

            blnAnnotation = dtAccess.Rows(0)("Annotate")
            blnRedaction = dtAccess.Rows(0)("Redact")
        Catch ex As Exception

        End Try

    End Function

    Friend Function DocumentSecurityLevel(ByVal VersionId As Integer, ByVal objUser As Web.Security.User, ByRef conSqlImage As SqlClient.SqlConnection) As Web.Security.SecurityLevel
        Dim rValue As Web.Security.SecurityLevel = Web.Security.SecurityLevel.NoAccess
        Dim cmdSql As SqlClient.SqlCommand, bolCloseCon As Boolean = False
        Try
            '
            ' Get the logged on user's id
            '
            If Not conSqlImage.State = ConnectionState.Open Then
                conSqlImage.Open()
                bolCloseCon = True
            End If

            Dim intImagesUserId As Integer = GetImagesUserId(conSqlImage, objUser)
            If Not intImagesUserId > 0 Then Throw New Exception("Invalid User specified")
            '
            ' Get User Security Level for the document 
            '
            cmdSql = New SqlClient.SqlCommand("acsDocumentSecurityLevel", conSqlImage)
            cmdSql.CommandType = CommandType.StoredProcedure
            cmdSql.Parameters.Add("@UserId", intImagesUserId)
            cmdSql.Parameters.Add("@VersionId", VersionId)
            rValue = cmdSql.ExecuteScalar()

        Catch ex As Exception
            Throw ex
        Finally
            If Not conSqlImage.State = ConnectionState.Closed And bolCloseCon Then
                conSqlImage.Close()
            End If
        End Try

        Return rValue

    End Function


    Friend Function DocumentSecurityLevelByVersionId(ByVal intVersionId As Integer, ByVal objUser As Web.Security.User, ByRef conSqlImage As SqlClient.SqlConnection) As Web.Security.SecurityLevel
        Dim rValue As Web.Security.SecurityLevel = Web.Security.SecurityLevel.NoAccess
        Dim cmdSql As SqlClient.SqlCommand, bolCloseCon As Boolean = False
        Try
            '
            ' Get the logged on user's id
            '
            If Not conSqlImage.State = ConnectionState.Open Then
                conSqlImage.Open()
                bolCloseCon = True
            End If

            Dim intImagesUserId As Integer = GetImagesUserId(conSqlImage, objUser)
            If Not intImagesUserId > 0 Then Throw New Exception("Invalid User specified")
            '
            ' Get User Security Level for the document 
            '
            cmdSql = New SqlClient.SqlCommand("acsDocumentSecurityByVersionID", conSqlImage)
            cmdSql.CommandType = CommandType.StoredProcedure
            cmdSql.Parameters.Add("@UserId", intImagesUserId)
            cmdSql.Parameters.Add("@VersionId", intVersionId)
            rValue = cmdSql.ExecuteScalar()

        Catch ex As Exception
            Throw ex
        Finally
            If Not conSqlImage.State = ConnectionState.Closed And bolCloseCon Then
                conSqlImage.Close()
            End If
        End Try

        Return rValue

    End Function



    Friend Function EmailExportSecurityLevel(ByVal intDocumentId As Integer, ByVal objUser As Web.Security.User, ByRef conSqlImage As SqlClient.SqlConnection) As Web.Security.SecurityLevelEmailExport

        Dim rValue As Web.Security.SecurityLevelEmailExport = Web.Security.SecurityLevelEmailExport.EmailExportDisabled
        Dim cmdSql As SqlClient.SqlCommand, bolCloseCon As Boolean = False

        Try
            '
            ' Get the logged on user's id
            '
            If Not conSqlImage.State = ConnectionState.Open Then
                conSqlImage.Open()
                bolCloseCon = True
            End If

            Dim intImagesUserId As Integer = GetImagesUserId(conSqlImage, objUser)
            If Not intImagesUserId > 0 Then Throw New Exception("Invalid User specified")
            '
            ' Get User Security Level for the document 
            '
            cmdSql = New SqlClient.SqlCommand("acsDocumentSecurityEmailExportLevel", conSqlImage)
            cmdSql.CommandType = CommandType.StoredProcedure
            cmdSql.Parameters.Add("@UserId", intImagesUserId)
            cmdSql.Parameters.Add("@DocumentId", intDocumentId)
            rValue = cmdSql.ExecuteScalar()

        Catch ex As Exception
            Throw ex
        Finally
            If Not conSqlImage.State = ConnectionState.Closed And bolCloseCon Then
                conSqlImage.Close()
            End If
        End Try

        Return rValue

    End Function

    Friend Function GetImagesUserId(ByRef conSqlImage As SqlClient.SqlConnection, ByRef objUser As SECURITY.User) As Integer

        Return GetImagesUserId(conSqlImage, objUser.UserId)

    End Function

    Friend Function GetImagesUserId(ByRef conSqlImage As SqlClient.SqlConnection, ByVal guidUserId As System.Guid) As Integer

        Dim cmdSql As New SqlClient.SqlCommand("SELECT UserId FROM users WHERE UserName = @UserName", conSqlImage)
        cmdSql.Parameters.Add("@UserName", guidUserId.ToString)

        Dim odb As New Accucentric.docAssist.Data.Images.db
        Dim bolCloseCon As Boolean = False
        If Not conSqlImage.State = ConnectionState.Open Then
            conSqlImage.Open()
            bolCloseCon = True
        End If
        Dim rValue As Integer = odb.ExecuteScalar(cmdSql)
        If (Not conSqlImage.State = ConnectionState.Closed) And bolCloseCon Then
            conSqlImage.Close()
        End If
        Return rValue

    End Function

    Friend Function GetOrphanResults(ByVal cookieSearch As cookieSearch, ByVal objUser As Web.Security.User, ByRef dvOrphans As DataView, Optional ByRef conSqlImage As SqlClient.SqlConnection = Nothing) As Integer
        '
        ' Get the orphaned records
        '
        Dim rValue As Integer = 0
        Dim bolCloseCon As Boolean
        Dim conSql As SqlClient.SqlConnection
        If Not conSqlImage Is Nothing Then
            conSql = conSqlImage
        Else
            conSql = Web.Security.BuildConnection(objUser)
        End If

        Try
            '
            ' Get document list
            '
            Dim sbDocuments As New System.Text.StringBuilder
            Dim dsAttributes As DataSet = cookieSearch.SearchAttributes()
            Dim dtDocuments As DataTable = dsAttributes.Tables("Documents")
            For Each dr As DataRow In dtDocuments.Rows
                If sbDocuments.Length > 0 Then sbDocuments.Append(",")
                sbDocuments.Append(dr("DocumentId"))
            Next

            'Dim cmdSql As New SqlCommand("acsOrphanSearchResults", conSql)
            'cmdSql.CommandType = CommandType.StoredProcedure
            'cmdSql.Parameters.Add("@Documents", sbDocuments.ToString)
            'cmdSql.Parameters.Add(New SqlParameter("@DiffFromGMT", objUser.TimeDiffFromGMT))

            Dim cmdSql As New SqlCommand("acsSearchResults", conSql)
            cmdSql.CommandType = CommandType.StoredProcedure
            'cmdSql.Parameters.Add("@Documents", sbDocuments.ToString)
            cmdSql.Parameters.Add(New SqlParameter("@DiffFromGMT", objUser.TimeDiffFromGMT))

            If conSql.State = ConnectionState.Closed Then
                conSql.Open()
                bolCloseCon = True
            End If

            Dim db As New Accucentric.docAssist.Data.Images.db
            Dim dtOrphans As New Web.Data.dtSearchResults
            db.PopulateTable(dtOrphans, cmdSql)

            'If dtOrphans.Rows.Count > 0 Then
            '    Dim dsResults As New DataSet
            '    dsResults.Tables.Add(dtOrphans)
            '    dvOrphans = SortResults(cookieSearch, dsResults, conSqlImage)
            'Else
            Dim dsResults As New DataSet
            dsResults.Tables.Add(dtOrphans)

            If conSqlImage Is Nothing Then
                conSqlImage = Functions.BuildConnection(objUser)
                conSqlImage.Open()
            End If

            dvOrphans = SortResults(cookieSearch, dsResults, conSqlImage)
            'End If

            rValue = dvOrphans.Count

        Catch exSql As SqlException
            Dim SqlErr As SqlError
            For Each SqlErr In exSql.Errors
                Throw New Exception("SQL Error No " & SqlErr.Number & ": " & SqlErr.Message)
            Next
        Catch ex As Exception
            Throw ex
        Finally
            If bolCloseCon Then
                If Not conSql.State = ConnectionState.Closed Then conSql.Close()
            End If
        End Try

        Return rValue

    End Function

    Friend Function GetSearchResults(ByVal cookieSearch As cookieSearch, ByRef objUser As Web.Security.User, ByRef dvResults As DataView, _
    Optional ByVal conSql As SqlClient.SqlConnection = Nothing, Optional ByVal blnOrphans As Boolean = False, _
    Optional ByVal intUserID As Integer = 0, Optional ByVal intSort As Integer = 10020, Optional ByVal intSortDirection As Integer = 1, _
    Optional ByVal intPageNumber As Integer = 1, Optional ByRef intShowWfColumn As Integer = 0, _
    Optional ByRef dtSearchAttributes As DataTable = Nothing, Optional ByRef ds As DataSet = Nothing, Optional ByVal dtFullText As DataTable = Nothing) As Integer

        Dim rValue As Integer = 0
        Dim dsAttributes As DataSet = cookieSearch.SearchAttributes 'Session("SearchAttributes")
        Dim dtDocuments As New DataTable("Documents") '= dsAttributes.Tables("Documents")
        Dim dtAttributes As New DataTable("Attributes") '= dsAttributes.Tables("Attributes")
        '
        'Build Documents table
        dtDocuments.Columns.Add(New DataColumn("DocumentId", SqlDbType.Int.GetType))
        '
        ' Build Attributes Table
        dtAttributes.Columns.Add(New DataColumn("AttributeId", SqlDbType.Int.GetType))
        dtAttributes.Columns.Add(New DataColumn("AttributeValue", Type.GetType("System.String")))
        '
        ' Get the Documents
        Dim drRowSession, drRow As DataRow, drCol As DataColumn
        For Each drRowSession In dsAttributes.Tables("Documents").Rows
            drRow = dtDocuments.NewRow
            For Each drCol In dtDocuments.Columns
                drRow(drCol) = drRowSession(drCol.ColumnName)
            Next
            dtDocuments.Rows.Add(drRow)
        Next
        '
        ' Get the attributes
        If dsAttributes.Tables.Count > 1 Then
            For Each drRowSession In dsAttributes.Tables("Attributes").Rows
                drRow = dtAttributes.NewRow
                For Each drCol In dtAttributes.Columns
                    drRow(drCol) = drRowSession(drCol.ColumnName)
                Next
                dtAttributes.Rows.Add(drRow)
            Next
        End If
        '
        ' Get they keywords
        Dim strKeywords As String
        If Not cookieSearch.Keywords Is Nothing Then
            strKeywords = cookieSearch.Keywords
        End If

        Dim bolCloseCon As Boolean = False
        Try

            If conSql Is Nothing Then
                conSql = Web.Security.BuildConnection(objUser)
            End If
            If Not conSql.State = ConnectionState.Open Then
                conSql.Open()
                bolCloseCon = True
            End If

            ' Build tables
            ' replace '-' in guid string
            Dim strDoc As String = System.Guid.NewGuid().ToString().Replace("-", "")
            Dim strAttributes As String = System.Guid.NewGuid().ToString().Replace("-", "")


            Dim sb As New System.Text.StringBuilder
            sb.Append("CREATE TABLE ##" & strDoc.ToString & " (DocumentId int)" & vbCrLf)
            sb.Append("CREATE TABLE ##" & strAttributes & " (AttributeId int, AttributeValue varchar(75))" & vbCrLf)

            Dim sqlcmd As New SqlCommand(sb.ToString, conSql)
            Dim sqlDa As SqlDataAdapter
            Dim sqlBld As SqlCommandBuilder

            sqlcmd.ExecuteNonQuery()

            ' Add documents
            sqlcmd = New SqlCommand("SELECT * FROM ##" & strDoc, conSql)
            sqlDa = New SqlDataAdapter(sqlcmd)
            sqlBld = New SqlCommandBuilder(sqlDa)

            sqlDa.Update(dtDocuments)

            ' Add Attributes
            sqlcmd = New SqlCommand("SELECT * FROM ##" & strAttributes, conSql)
            sqlDa = New SqlDataAdapter(sqlcmd)
            sqlBld = New SqlCommandBuilder(sqlDa)

            If Not blnOrphans Then sqlDa.Update(dtAttributes)

            Dim strFullText As String
            'Add global temp table for fulltext search results
            If Not dtFullText Is Nothing Then
                strFullText = System.Guid.NewGuid().ToString().Replace("-", "")
                sb = New System.Text.StringBuilder
                sb.Append("CREATE TABLE ##" & strFullText.ToString & " (ImageDetID int NULL, Score int NULL, ScorePercent int NULL, HitCount int NULL)" & vbCrLf)
                sqlcmd = New SqlCommand(sb.ToString, conSql)
                sqlBld = New SqlCommandBuilder
                sqlcmd.ExecuteNonQuery()
                ' Add fulltext results
                sqlcmd = New SqlCommand("SELECT ImageDetID, Score, ScorePercent, HitCount FROM ##" & strFullText, conSql)
                sqlDa = New SqlDataAdapter(sqlcmd)
                sqlBld = New SqlCommandBuilder(sqlDa)
                sqlDa.Update(CType(dtFullText, DataTable))
            End If



            sqlcmd = New SqlCommand("acsSearchResults", conSql)
            sqlcmd.CommandTimeout = 180
            sqlcmd.CommandType = CommandType.StoredProcedure
            sqlcmd.Parameters.Add(New SqlParameter("@DocTable", "##" & strDoc))
            sqlcmd.Parameters.Add(New SqlParameter("@AttributeTable", "##" & strAttributes))
            sqlcmd.Parameters.Add(New SqlParameter("@DiffFromGMT", objUser.TimeDiffFromGMT))
            sqlcmd.Parameters.Add(New SqlParameter("@ResultsPerPage", objUser.DefaultResultCount))
            sqlcmd.Parameters.Add(New SqlParameter("@PageNumber", intPageNumber))
            sqlcmd.Parameters.Add(New SqlParameter("@SortColumn", intSort))
            sqlcmd.Parameters.Add(New SqlParameter("@SortOrder", intSortDirection))
            sqlcmd.Parameters.Add(New SqlParameter("@NoAttributeSearch", blnOrphans))
            sqlcmd.Parameters.Add(New SqlParameter("@TotalDocCount", SqlDbType.Int))
            sqlcmd.Parameters.Add(New SqlParameter("@ShowWorkflowStatus", SqlDbType.Int))
            sqlcmd.Parameters.Add(New SqlParameter("@UserID", intUserID))
            sqlcmd.Parameters.Add(New SqlParameter("@ReturnAllVersionIDs", 1))

            sqlcmd.Parameters("@TotalDocCount").Direction = ParameterDirection.Output
            sqlcmd.Parameters("@ShowWorkflowStatus").Direction = ParameterDirection.Output

            If Not strKeywords Is Nothing And Not strFullText Is Nothing Then
                If strKeywords.Trim.Length > 0 Then
                    sqlcmd.Parameters.Add(New SqlParameter("@FullTextTable", "##" & strFullText))
                    'Session("Keywords") = Me.mtxtKeywords.Text.Trim
                End If
            End If
            '
            ' Get advanced Search options
            '

            Dim strAdvancedOptions As String = cookieSearch.SearchAdvanced
            If Not strAdvancedOptions Is Nothing Then
                Dim strAdvancedOption() As String = strAdvancedOptions.Split(":")
                Dim bolExact As Boolean = strAdvancedOption(0)
                Dim bolOrphans As Boolean = strAdvancedOption(1)
                Dim bolWithin As Boolean = strAdvancedOption(2)
                Dim bolModifiedUser As Boolean = strAdvancedOption(3)
                Dim bolWFUnassigned As Boolean = strAdvancedOption(4)
                Dim intModifiedUserId As Integer = CInt(strAdvancedOption(5))

                'New search options, Search within/Match exact
                'If bolWithin Then sqlcmd.Parameters.Add(New SqlParameter("@SearchWithin", "1"))
                'If bolExact Then sqlcmd.Parameters.Add(New SqlParameter("@ExactPhrase", "1"))
                If bolModifiedUser Then sqlcmd.Parameters.Add(New SqlParameter("@ModifiedUserId", intModifiedUserId))
                If bolWFUnassigned Then sqlcmd.Parameters.Add(New SqlParameter("@ShowWorkFlowUnassigned", bolWFUnassigned))
            End If

            sqlDa = New SqlDataAdapter(sqlcmd)
            'Dim ds As New DataSet("results")
            ds.DataSetName = "results"
            sqlDa.Fill(ds)

            'dtVersionId = ds.Tables(2)

            'WF cut out of version 2.0
            'intShowWF = sqlcmd.Parameters("@ShowWorkflowStatus").Value
            'intShowWF = 0

            If Not ds.Tables(1) Is Nothing Then
                dtSearchAttributes = ds.Tables(1)
            End If

            Dim intTotalDocCount As Integer = CInt(sqlcmd.Parameters("@TotalDocCount").Value)
            '
            ' Get Sort Attribute Name
            '
            Dim strAttribute As String
            If intSort < 10000 Then

                'sqlcmd = New SqlCommand("SELECT AttributeName FROM Attributes WHERE AttributeId = @AttributeId", conSql)
                'sqlcmd.Parameters.Add("@AttributeId", intSort)

                strAttribute = "RowId"

            End If
            If ds.Tables(0).Rows.Count > 0 Then
                dvResults = Functions.ResultsTable(ds, intSort, intSortDirection, strAttribute)
            Else
                dvResults = Functions.ResultsTable(ds, intSort, intSortDirection, strAttribute)
            End If
            cookieSearch.TotalResults = intTotalDocCount
            rValue = intTotalDocCount
            sqlDa.Dispose()

            ' destroy tables
            sb = New System.Text.StringBuilder
            sb.Append("DROP TABLE ##" & strDoc & vbCrLf)
            sb.Append("DROP TABLE ##" & strAttributes & vbCrLf)
            If Not dtFullText Is Nothing Then
                sb.Append("DROP TABLE ##" & strFullText & vbCrLf)
            End If

            sqlcmd = New SqlClient.SqlCommand(sb.ToString, conSql)
            sqlcmd.ExecuteNonQuery()

            sqlcmd.Dispose()
            sb = Nothing

        Catch sqlEx As SqlClient.SqlException
            Throw New Exception(sqlEx.Message)
        Finally
            If bolCloseCon Then
                If Not conSql.State = ConnectionState.Closed Then conSql.Close()
            End If
        End Try

        Return rValue

    End Function

    Friend Function SearchResults(ByVal cookieSearch As cookieSearch, ByRef objUser As Web.Security.User, ByRef dvResults As DataView, _
      Optional ByVal conSql As SqlClient.SqlConnection = Nothing, Optional ByVal blnOrphans As Boolean = False, _
      Optional ByVal intUserID As Integer = 0, Optional ByVal intSort As Integer = 10020, Optional ByVal intSortDirection As Integer = 1, _
      Optional ByVal intPageNumber As Integer = 1, Optional ByRef intShowWfColumn As Integer = 0, _
      Optional ByRef dtSearchAttributes As DataTable = Nothing, Optional ByRef ds As DataSet = Nothing, Optional ByVal dtFullText As DataTable = Nothing, _
      Optional ByRef dtResults As DataTable = Nothing, Optional ByVal blnQuickSearch As Boolean = False, Optional ByVal blnQuickOCRSearch As Boolean = False) As Integer

        Dim rValue As Integer = 0
        Dim dsAttributes As DataSet = cookieSearch.SearchAttributes
        Dim dtDocuments As New DataTable("Documents")
        Dim dtAttributes As New DataTable("Attributes")

        'Build Documents table
        dtDocuments.Columns.Add(New DataColumn("DocumentId", SqlDbType.Int.GetType))

        ' Build Attributes Table
        dtAttributes.Columns.Add(New DataColumn("AttributeId", SqlDbType.Int.GetType))
        dtAttributes.Columns.Add(New DataColumn("AttributeValue", Type.GetType("System.String")))
        dtAttributes.Columns.Add(New DataColumn("AttributeValueEnd", Type.GetType("System.String")))
        dtAttributes.Columns.Add(New DataColumn("SearchType", Type.GetType("System.String")))

        ' Get the Documents
        Dim drRowSession, drRow As DataRow, drCol As DataColumn
        For Each drRowSession In dsAttributes.Tables("Documents").Rows
            drRow = dtDocuments.NewRow
            For Each drCol In dtDocuments.Columns
                drRow(drCol) = drRowSession(drCol.ColumnName)
            Next
            dtDocuments.Rows.Add(drRow)
        Next
        '
        ' Get the attributes
        If dsAttributes.Tables.Count > 1 Then
            For Each drRowSession In dsAttributes.Tables("Attributes").Rows
                drRow = dtAttributes.NewRow
                For Each drCol In dtAttributes.Columns
                    drRow(drCol) = drRowSession(drCol.ColumnName)
                Next
                dtAttributes.Rows.Add(drRow)
            Next
        End If
        '
        ' Get they keywords
        Dim strKeywords As String
        If Not cookieSearch.Keywords Is Nothing Then
            strKeywords = cookieSearch.Keywords
        End If

        Dim bolCloseCon As Boolean = False
        Try

            If conSql Is Nothing Then
                conSql = Web.Security.BuildConnection(objUser)
            End If
            If Not conSql.State = ConnectionState.Open Then
                conSql.Open()
                bolCloseCon = True
            End If

            ' Build tables
            Dim strDoc As String = System.Guid.NewGuid().ToString().Replace("-", "")
            Dim strAttributes As String = System.Guid.NewGuid().ToString().Replace("-", "")

            Dim sb As New System.Text.StringBuilder
            sb.Append("CREATE TABLE ##" & strDoc.ToString & " (DocumentId int)" & vbCrLf)
            sb.Append("CREATE TABLE ##" & strAttributes & " (AttributeId int, AttributeValue varchar(75), SearchType varchar(2), AttributeValueEnd varchar(75))" & vbCrLf)

            Dim sqlcmd As New SqlCommand(sb.ToString, conSql)
            Dim sqlDa As SqlDataAdapter
            Dim sqlBld As SqlCommandBuilder

            sqlcmd.ExecuteNonQuery()

            ' Add documents
            sqlcmd = New SqlCommand("SELECT * FROM ##" & strDoc, conSql)
            sqlDa = New SqlDataAdapter(sqlcmd)
            sqlBld = New SqlCommandBuilder(sqlDa)

            sqlDa.Update(dtDocuments)

            ' Add Attributes
            sqlcmd = New SqlCommand("SELECT * FROM ##" & strAttributes, conSql)
            sqlDa = New SqlDataAdapter(sqlcmd)
            sqlBld = New SqlCommandBuilder(sqlDa)

            sqlDa.Update(dtAttributes)

            Dim strFullText As String
            'Add global temp table for fulltext search results
            If Not dtFullText Is Nothing Then
                strFullText = System.Guid.NewGuid().ToString().Replace("-", "")
                sb = New System.Text.StringBuilder
                sb.Append("CREATE TABLE ##" & strFullText.ToString & " (ImageDetID int NULL, Score int NULL, ScorePercent int NULL, HitCount int NULL)" & vbCrLf)
                sqlcmd = New SqlCommand(sb.ToString, conSql)
                sqlBld = New SqlCommandBuilder
                sqlcmd.ExecuteNonQuery()
                ' Add fulltext results
                sqlcmd = New SqlCommand("SELECT ImageDetID, Score, ScorePercent, HitCount FROM ##" & strFullText, conSql)
                sqlDa = New SqlDataAdapter(sqlcmd)
                sqlBld = New SqlCommandBuilder(sqlDa)
                sqlDa.Update(CType(dtFullText, DataTable))
            End If

            sqlcmd = New SqlCommand("acsSearchEngine", conSql)
            sqlcmd.CommandTimeout = 180
            sqlcmd.CommandType = CommandType.StoredProcedure
            sqlcmd.Parameters.Add(New SqlParameter("@DocTable", "##" & strDoc))
            sqlcmd.Parameters.Add(New SqlParameter("@AttributeTable", "##" & strAttributes))
            sqlcmd.Parameters.Add(New SqlParameter("@DiffFromGMT", objUser.TimeDiffFromGMT))
            sqlcmd.Parameters.Add(New SqlParameter("@NoAttributeSearch", blnOrphans))
            sqlcmd.Parameters.Add(New SqlParameter("@UserID", intUserID))
            sqlcmd.Parameters.Add(New SqlParameter("@FullTextValue ", cookieSearch.Keywords))
            sqlcmd.Parameters.Add(New SqlParameter("@QuickSearch ", blnQuickSearch))

            If blnQuickSearch Then
                If blnQuickOCRSearch Then
                    If Not strKeywords Is Nothing And Not strFullText Is Nothing Then
                        If strKeywords.Trim.Length > 0 Then
                            sqlcmd.Parameters.Add(New SqlParameter("@FullTextTable", "##" & strFullText))
                        End If
                    End If
                End If
            Else
                If Not strKeywords Is Nothing And Not strFullText Is Nothing Then
                    If strKeywords.Trim.Length > 0 Then
                        sqlcmd.Parameters.Add(New SqlParameter("@FullTextTable", "##" & strFullText))
                    End If
                End If
            End If

            ' Get advanced Search options
            Dim strAdvancedOptions As String = cookieSearch.SearchAdvanced
            If Not strAdvancedOptions Is Nothing Then
                Dim strAdvancedOption() As String = strAdvancedOptions.Split(":")
                Dim bolExact As Boolean = strAdvancedOption(0)
                Dim bolOrphans As Boolean = strAdvancedOption(1)
                Dim bolWithin As Boolean = strAdvancedOption(2)
                Dim bolModifiedUser As Boolean = strAdvancedOption(3)
                'Dim bolWFUnassigned As Boolean = strAdvancedOption(4)
                Dim intModifiedUserId As Integer = CInt(strAdvancedOption(5))

                If bolModifiedUser Then sqlcmd.Parameters.Add(New SqlParameter("@ModifiedUserId", intModifiedUserId))
            End If

            sqlDa = New SqlDataAdapter(sqlcmd)
            ds.DataSetName = "results"
            sqlDa.Fill(ds)

            If Not ds.Tables(1) Is Nothing Then
                dtSearchAttributes = ds.Tables(1)
            End If

            dvResults = Functions.TableResults(ds, intSort, intSortDirection, dtResults)

            rValue = dvResults.Count

            'Dataview is already sorted, chunk out the rows we want
            Dim intRecPerPg As Integer = objUser.DefaultResultCount
            Dim intBeginning As Integer = (((intPageNumber * intRecPerPg) - intRecPerPg) + 1) - 1
            Dim intEnding As Integer = (intBeginning + intRecPerPg) - 1

            dvResults.RowFilter = "RowId >= " & intBeginning & " AND RowId <= " & intEnding

            sqlDa.Dispose()

            ' destroy tables
            sb = New System.Text.StringBuilder
            sb.Append("DROP TABLE ##" & strDoc & vbCrLf)
            sb.Append("DROP TABLE ##" & strAttributes & vbCrLf)
            If Not dtFullText Is Nothing Then
                sb.Append("DROP TABLE ##" & strFullText & vbCrLf)
            End If

            sqlcmd = New SqlClient.SqlCommand(sb.ToString, conSql)
            sqlcmd.ExecuteNonQuery()

            sqlcmd.Dispose()
            sb = Nothing

        Catch sqlEx As SqlClient.SqlException
            Throw New Exception(sqlEx.Message)
        Finally
            If bolCloseCon Then
                If Not conSql.State = ConnectionState.Closed Then conSql.Close()
            End If
        End Try

        Return rValue

    End Function

    Friend Function LogUserAccounts(ByVal guidUserId As System.Guid, ByVal guidAccountId As System.Guid, _
            ByVal guidTransactionUserId As System.Guid, ByVal strTransactionType As String, _
            Optional ByRef conSqlWeb As SqlClient.SqlConnection = Nothing, _
            Optional ByRef tranSql As SqlClient.SqlTransaction = Nothing) As Boolean

        If conSqlWeb Is Nothing Then
            conSqlWeb = New SqlClient.SqlConnection(ConnectionString())
        End If
        Dim bolCloseCon As Boolean = False
        Try
            If Not conSqlWeb.State = ConnectionState.Open Then
                bolCloseCon = True
                conSqlWeb.Open()
            End If

            Dim cmdsql As SqlClient.SqlCommand
            cmdsql = New SqlClient.SqlCommand("INSERT INTO UserAccountsLog (UserAccountsLogId, UserId, AccountId, TransactionUserId, TransactionType, TransactionDate) VALUES (newid(), @UserId, @AccountId, @TransactionUserId, @TransactionType, @TransactionDate)", conSqlWeb)
            cmdsql.CommandTimeout = DEFAULT_COMMAND_TIMEOUT
            cmdsql.Parameters.Add("@UserId", guidUserId)
            cmdsql.Parameters.Add("@AccountId", guidAccountId)
            cmdsql.Parameters.Add("@TransactionUserId", guidTransactionUserId)
            cmdsql.Parameters.Add("@TransactionType", strTransactionType)
            cmdsql.Parameters.Add("@TransactionDate", Now())

            If Not tranSql Is Nothing Then cmdsql.Transaction = tranSql

            cmdsql.ExecuteNonQuery()

        Catch ex As Exception
            Throw ex
        Finally
            If Not conSqlWeb.State = ConnectionState.Closed And bolCloseCon Then conSqlWeb.Close()
        End Try

    End Function

    Friend Function ResultsTable(ByRef dsSearchResults As DataSet, ByVal intSortAttributeId As Integer, ByVal intSortDirection As Integer, Optional ByVal strAttributeName As String = Nothing) As DataView
        '
        ' Check for rows
        'If Not dsSearchResults.Tables(0).Rows.Count > 0 Then
        '    Return Nothing
        'End If

        Dim dtSearchResults As DataTable = dsSearchResults.Tables(0)
        Dim dtResultsAttributes As DataTable
        If dsSearchResults.Tables.Count() > 1 Then
            dtResultsAttributes = dsSearchResults.Tables(1)
        End If
        '
        ' Create the result table and add the static columns
        Dim bolAdvancedSearch As Boolean = False
        Dim dtResult As New DataTable("Results")
        dtResult.Columns.Add(New DataColumn("VersionId", GetType(System.Int32), Nothing, MappingType.Element))
        dtResult.Columns.Add(New DataColumn("Document", GetType(System.String), Nothing, MappingType.Element))
        dtResult.Columns.Add(New DataColumn("ModifiedDate", GetType(System.DateTime), Nothing, MappingType.Element))
        dtResult.Columns.Add(New DataColumn("AddDate", GetType(System.DateTime), Nothing, MappingType.Element))
        dtResult.Columns.Add(New DataColumn("VersionNum", GetType(System.Int32), Nothing, MappingType.Element))
        dtResult.Columns.Add(New DataColumn("SeqTotal", GetType(System.Int32), Nothing, MappingType.Element))
        dtResult.Columns.Add(New DataColumn("ModifiedDateFormatted", GetType(System.String), Nothing, MappingType.Element))
        dtResult.Columns.Add(New DataColumn("AddDateFormatted", GetType(System.String), Nothing, MappingType.Element))
        dtResult.Columns.Add(New DataColumn("RowId", GetType(System.Int32), Nothing, MappingType.Element))

        If Not dtSearchResults.Columns("ScorePercent") Is Nothing Then
            bolAdvancedSearch = True
            dtResult.Columns.Add(New DataColumn("ScorePercent", GetType(System.Int32), Nothing, MappingType.Element))
        End If
        If Not dtSearchResults.Columns("HitCount") Is Nothing Then
            bolAdvancedSearch = True
            dtResult.Columns.Add(New DataColumn("HitCount", GetType(System.Int32), Nothing, MappingType.Element))
        End If


        dtResult.Columns.Add(New DataColumn("WorkFlowDesc", GetType(System.String), Nothing, MappingType.Element))
        dtResult.PrimaryKey = New DataColumn() {dtResult.Columns("VersionId")}

        '
        ' Add Columns
        '
        If Not dtResultsAttributes Is Nothing Then
            For Each dr As DataRow In dtResultsAttributes.Rows
                dtResult.Columns.Add(New DataColumn(dr("AttributeName"), GetType(System.String), Nothing, MappingType.Element))
                'dtResult.Columns.Add(New DataColumn(dr("AttributeDataType"), GetType(System.String), Nothing, MappingType.Element))
            Next
        End If
        '
        ' Sort Results
        '
        Dim sbSort As New System.Text.StringBuilder
        Dim sbFilter As New System.Text.StringBuilder
        Select Case intSortAttributeId
            Case 10000
                sbSort.Append("Document")
            Case 10005
                sbSort.Append("SeqTotal")
            Case 10010
                sbSort.Append("VersionNum")
            Case 10015
                sbSort.Append("AddDate")
            Case 10020
                sbSort.Append("ModifiedDate")
            Case 10030
                sbSort.Append("ScorePercent")
            Case 10040
                sbSort.Append("HitCount")
            Case Else
                sbSort.Append(strAttributeName)
        End Select
        Select Case intSortDirection
            Case 0
                sbSort.Append(" ASC")
            Case 1
                sbSort.Append(" DESC")
        End Select
        '
        ' Loop through the search results and populate the table
        ' using only one row per version id.
        '
        'Dim drSearchResults As Accucentric.docAssist.Web.Data.dtSearchResultsRow
        Dim drResult As DataRow, dcResult As DataColumn
        For Each dr As DataRow In dtSearchResults.Rows
            AddResultRow(dr, dtResult, bolAdvancedSearch)
        Next
        '
        'Return dtResult
        Return New DataView(dtResult, Nothing, IIf(intSortAttributeId < 10000, strAttributeName, sbSort.ToString), DataViewRowState.CurrentRows)

    End Function

    Friend Function TableResults(ByRef dsSearchResults As DataSet, ByVal intSortAttributeId As Integer, ByVal intSortDirection As Integer, ByRef dtResults As DataTable) As DataView

        Dim dtSearchResults As DataTable = dsSearchResults.Tables(0)
        Dim dtResultsAttributes As DataTable
        If dsSearchResults.Tables.Count() > 1 Then
            dtResultsAttributes = dsSearchResults.Tables(1)
        End If

        ' Create the result table and add the static columns
        Dim bolAdvancedSearch As Boolean = False
        Dim dtResult As New DataTable("Results")
        dtResult.Columns.Add(New DataColumn("VersionId", GetType(System.Int32), Nothing, MappingType.Element))
        dtResult.Columns.Add(New DataColumn("Document", GetType(System.String), Nothing, MappingType.Element))
        dtResult.Columns.Add(New DataColumn("ModifiedDate", GetType(System.DateTime), Nothing, MappingType.Element))
        dtResult.Columns.Add(New DataColumn("AddDate", GetType(System.DateTime), Nothing, MappingType.Element))
        dtResult.Columns.Add(New DataColumn("VersionNum", GetType(System.Int32), Nothing, MappingType.Element))
        dtResult.Columns.Add(New DataColumn("SeqTotal", GetType(System.Int32), Nothing, MappingType.Element))
        dtResult.Columns.Add(New DataColumn("ModifiedDateFormatted", GetType(System.String), Nothing, MappingType.Element))
        dtResult.Columns.Add(New DataColumn("AddDateFormatted", GetType(System.String), Nothing, MappingType.Element))
        dtResult.Columns.Add(New DataColumn("RowId", GetType(System.Int32), Nothing, MappingType.Element))
        dtResult.Columns.Add(New DataColumn("FullTextAvailable", GetType(System.Boolean), Nothing, MappingType.Element))

        If Not dtSearchResults.Columns("ScorePercent") Is Nothing Then
            bolAdvancedSearch = True
            dtResult.Columns.Add(New DataColumn("ScorePercent", GetType(System.Int32), Nothing, MappingType.Element))
        End If
        If Not dtSearchResults.Columns("HitCount") Is Nothing Then
            bolAdvancedSearch = True
            dtResult.Columns.Add(New DataColumn("HitCount", GetType(System.Int32), Nothing, MappingType.Element))
        End If

        dtResult.PrimaryKey = New DataColumn() {dtResult.Columns("VersionId")}

        ' Add Columns
        If Not dtResultsAttributes Is Nothing Then
            For Each dr As DataRow In dtResultsAttributes.Rows
                dtResult.Columns.Add(New DataColumn(dr("AttributeName"), GetType(System.String), Nothing, MappingType.Element))
            Next
        End If

        ' Sort Results
        Dim sbSort As New System.Text.StringBuilder
        Dim sbFilter As New System.Text.StringBuilder

        If intSortAttributeId < 10000 Then
            'Get sort attribute column name
            Dim dvAttributeId As New DataView(dsSearchResults.Tables(1))
            dvAttributeId.RowFilter = "AttributeId=" & intSortAttributeId
            sbSort.Append(dvAttributeId(0)("AttributeName"))
        Else
            Select Case intSortAttributeId
                Case 10000
                    sbSort.Append("Document")
                Case 10005
                    sbSort.Append("SeqTotal")
                Case 10010
                    sbSort.Append("VersionNum")
                Case 10015
                    sbSort.Append("AddDate")
                Case 10020
                    sbSort.Append("ModifiedDate")
                Case 10030
                    sbSort.Append("ScorePercent")
                Case 10040
                    sbSort.Append("HitCount")
            End Select
        End If

        Select Case intSortDirection
            Case 0
                sbSort.Append(" ASC")
            Case 1
                sbSort.Append(" DESC")
        End Select

        ' Loop through the search results and populate the table
        ' using only one row per version id.
        Dim drResult As DataRow, dcResult As DataColumn
        For Each dr As DataRow In dtSearchResults.Rows
            Functions.AddResult(dr, dtResult, bolAdvancedSearch)
        Next

        Dim returnDV As New DataView(dtResult, Nothing, sbSort.ToString, DataViewRowState.CurrentRows)

        Dim dtResultsTable As New DataTable

        dtResultsTable.Columns.Add(New DataColumn("RowId", GetType(System.Int32), Nothing, MappingType.Element))
        dtResultsTable.Columns.Add(New DataColumn("VersionId", GetType(System.Int32), Nothing, MappingType.Element))

        Dim intRowId As Integer = 0

        For x As Integer = 0 To returnDV.Count - 1
            Dim dr As DataRow = dtResultsTable.NewRow
            returnDV(x)("RowId") = intRowId
            intRowId += 1
            dr("RowId") = returnDV(x)("RowId")
            dr("VersionId") = returnDV(x)("VersionId")
            dtResultsTable.Rows.Add(dr)
        Next

        dtResults = dtResultsTable
        Return returnDV

    End Function

    Friend Function BuildResults(ByRef dsSearchResults As DataSet, ByVal intSortAttributeId As Integer, ByVal intSortDirection As Integer, ByRef dtResults As DataTable) As DataView

        Dim dtSearchResults As DataTable = dsSearchResults.Tables(0)
        Dim dtResultsAttributes As DataTable
        If dsSearchResults.Tables.Count() > 1 Then
            dtResultsAttributes = dsSearchResults.Tables(1)
        End If

        ' Create the result table and add the static columns
        Dim bolAdvancedSearch As Boolean = False
        Dim dtResult As New DataTable("Results")
        dtResult.Columns.Add(New DataColumn("VersionId", GetType(System.Int32), Nothing, MappingType.Element))
        dtResult.Columns.Add(New DataColumn("Document", GetType(System.String), Nothing, MappingType.Element))
        dtResult.Columns.Add(New DataColumn("ModifiedDate", GetType(System.DateTime), Nothing, MappingType.Element))
        dtResult.Columns.Add(New DataColumn("AddDate", GetType(System.DateTime), Nothing, MappingType.Element))
        dtResult.Columns.Add(New DataColumn("VersionNum", GetType(System.Int32), Nothing, MappingType.Element))
        dtResult.Columns.Add(New DataColumn("SeqTotal", GetType(System.Int32), Nothing, MappingType.Element))
        dtResult.Columns.Add(New DataColumn("ModifiedDateFormatted", GetType(System.String), Nothing, MappingType.Element))
        dtResult.Columns.Add(New DataColumn("AddDateFormatted", GetType(System.String), Nothing, MappingType.Element))
        dtResult.Columns.Add(New DataColumn("RowId", GetType(System.Int32), Nothing, MappingType.Element))
        dtResult.Columns.Add(New DataColumn("FullTextAvailable", GetType(System.Boolean), Nothing, MappingType.Element))

        If Not dtSearchResults.Columns("ScorePercent") Is Nothing Then
            bolAdvancedSearch = True
            dtResult.Columns.Add(New DataColumn("ScorePercent", GetType(System.Int32), Nothing, MappingType.Element))
        End If
        If Not dtSearchResults.Columns("HitCount") Is Nothing Then
            bolAdvancedSearch = True
            dtResult.Columns.Add(New DataColumn("HitCount", GetType(System.Int32), Nothing, MappingType.Element))
        End If

        dtResult.PrimaryKey = New DataColumn() {dtResult.Columns("VersionId")}

        ' Add Columns
        If Not dtResultsAttributes Is Nothing Then
            For Each dr As DataRow In dtResultsAttributes.Rows
                dtResult.Columns.Add(New DataColumn(dr("AttributeName"), GetType(System.String), Nothing, MappingType.Element))
            Next
        End If

        ' Sort Results
        Dim sbSort As New System.Text.StringBuilder
        Dim sbFilter As New System.Text.StringBuilder

        If intSortAttributeId < 10000 Then
            'Get sort attribute column name
            Dim dvAttributeId As New DataView(dsSearchResults.Tables(1))
            dvAttributeId.RowFilter = "AttributeId=" & intSortAttributeId
            sbSort.Append(dvAttributeId(0)("AttributeName"))
        Else
            Select Case intSortAttributeId
                Case 10000
                    sbSort.Append("Document")
                Case 10005
                    sbSort.Append("SeqTotal")
                Case 10010
                    sbSort.Append("VersionNum")
                Case 10015
                    sbSort.Append("AddDate")
                Case 10020
                    sbSort.Append("ModifiedDate")
                Case 10030
                    sbSort.Append("ScorePercent")
                Case 10040
                    sbSort.Append("HitCount")
            End Select
        End If

        Select Case intSortDirection
            Case 0
                sbSort.Append(" ASC")
            Case 1
                sbSort.Append(" DESC")
        End Select

        ' Loop through the search results and populate the table
        ' using only one row per version id.
        Dim drResult As DataRow, dcResult As DataColumn
        For Each dr As DataRow In dtSearchResults.Rows
            Functions.AddResult(dr, dtResult, bolAdvancedSearch)
        Next

        Dim returnDV As New DataView(dtResult, Nothing, sbSort.ToString, DataViewRowState.CurrentRows)

        Dim dtResultsTable As New DataTable

        dtResultsTable.Columns.Add(New DataColumn("RowId", GetType(System.Int32), Nothing, MappingType.Element))
        dtResultsTable.Columns.Add(New DataColumn("VersionId", GetType(System.Int32), Nothing, MappingType.Element))

        Dim intRowId As Integer = 0

        For x As Integer = 0 To returnDV.Count - 1
            Dim dr As DataRow = dtResultsTable.NewRow
            returnDV(x)("RowId") = intRowId
            intRowId += 1
            dr("RowId") = returnDV(x)("RowId")
            dr("VersionId") = returnDV(x)("VersionId")
            dtResultsTable.Rows.Add(dr)
        Next

        dtResults = dtResultsTable
        Return returnDV

    End Function


    Private Function SortResults(ByVal cookieSearch As cookieSearch, ByRef dsResults As DataSet, ByRef conSql As SqlClient.SqlConnection) As DataView
        '
        ' Get Sort Attribute Name
        '
        Dim bolCloseCon As Integer
        Dim dvReturn As DataView
        Try

            If Not conSql.State = ConnectionState.Open Then
                conSql.Open()
                bolCloseCon = True
            End If

            Dim strAttribute As String
            Dim cmdSql As SqlCommand
            If cookieSearch.SortColumn < 10000 Then
                cmdSql = New SqlCommand("SELECT AttributeName FROM Attributes WHERE AttributeId = @AttributeId", conSql)
                cmdSql.Parameters.Add("@AttributeId", cookieSearch.SortColumn)

                strAttribute = cmdSql.ExecuteScalar
            End If

            If dsResults.Tables(0).Rows.Count > 0 Then
                dvReturn = Functions.ResultsTable(dsResults, cookieSearch.SortColumn, cookieSearch.SortDirection, strAttribute)
            Else
                dvReturn = Functions.ResultsTable(dsResults, cookieSearch.SortColumn, cookieSearch.SortDirection, strAttribute)
            End If

        Catch ex As Exception
            Throw ex
        Finally
            If bolCloseCon Then
                If Not conSql.State = ConnectionState.Closed Then conSql.Close()
            End If
        End Try

        Return dvReturn


    End Function

    Private Sub AddResultRow(ByVal drSearchResults As DataRow, ByRef dtResult As DataTable, ByVal bolAdvancedSearch As Boolean)
        Dim drResult As DataRow
        '
        ' Check for existing versionid
        drResult = dtResult.Rows.Find(drSearchResults("VersionId"))
        If drResult Is Nothing Then
            drResult = dtResult.NewRow
            drResult("VersionId") = drSearchResults("VersionId")
            drResult("Document") = drSearchResults("DocumentName")
            drResult("ModifiedDate") = drSearchResults("ModifiedDate")
            drResult("ModifiedDateFormatted") = CType(drSearchResults("ModifiedDate"), DateTime).ToShortDateString & " " & CType(drSearchResults("ModifiedDate"), DateTime).ToShortTimeString
            drResult("AddDate") = drSearchResults("AddDate")
            drResult("AddDateFormatted") = CType(drSearchResults("AddDate"), DateTime).ToShortDateString & " " & CType(drSearchResults("AddDate"), DateTime).ToShortTimeString
            drResult("VersionNum") = drSearchResults("VersionNum")
            drResult("SeqTotal") = drSearchResults("SeqTotal")
            drResult("WorkFlowDesc") = drSearchResults("WorkFlowDesc")
            drResult("RowID") = drSearchResults("RowID")

            If bolAdvancedSearch Then
                drResult("ScorePercent") = drSearchResults("ScorePercent")
                drResult("HitCount") = drSearchResults("HitCount")

                ' drResult("SearchText") = drSearchResults("SearchText")
                'Else
                'drResult("SearchText") = ""
            End If
            dtResult.Rows.Add(drResult)
        End If
        '
        ' Check for existing column
        Dim sb As New System.Text.StringBuilder
        If drSearchResults.Table.Columns.Contains("AttributeName") Then
            If Not drSearchResults("AttributeName") Is System.DBNull.Value Then
                'If dtResult.Columns(drSearchResults("AttributeName) Is Nothing Then
                '    dtResult.Columns.Add(New DataColumn(drSearchResults("AttributeName, GetType(System.String), Nothing, MappingType.Element))
                'End If
                '
                ' Append the existing data
                If Not drResult(drSearchResults("AttributeName")) Is System.DBNull.Value Then
                    sb.Append(drResult(drSearchResults("AttributeName")))
                    sb.Append("<BR>")
                End If
                '
                ' Add the new data
                Select Case drSearchResults("AttributeDataType")
                    Case "List"
                        sb.Append(drSearchResults("ValueDescription"))
                    Case "CheckBox"
                        sb.Append("X")
                    Case Else
                        sb.Append(drSearchResults("AttributeValue"))
                End Select
            End If
        End If

        If sb.ToString.Length > 0 Then
            drResult(drSearchResults("AttributeName")) = sb.ToString
        End If
    End Sub

    Private Sub AddResult(ByVal drSearchResults As DataRow, ByRef dtResult As DataTable, ByVal bolAdvancedSearch As Boolean)
        Dim drResult As DataRow
        '
        ' Check for existing versionid
        drResult = dtResult.Rows.Find(drSearchResults("VersionId"))
        If drResult Is Nothing Then
            drResult = dtResult.NewRow
            drResult("VersionId") = drSearchResults("VersionId")
            drResult("Document") = drSearchResults("DocumentName")
            drResult("ModifiedDate") = drSearchResults("ModifiedDate")
            drResult("ModifiedDateFormatted") = CType(drSearchResults("ModifiedDate"), DateTime).ToShortDateString & " " & CType(drSearchResults("ModifiedDate"), DateTime).ToShortTimeString
            drResult("AddDate") = drSearchResults("AddDate")
            drResult("AddDateFormatted") = CType(drSearchResults("AddDate"), DateTime).ToShortDateString & " " & CType(drSearchResults("AddDate"), DateTime).ToShortTimeString
            drResult("VersionNum") = drSearchResults("VersionNum")
            drResult("SeqTotal") = drSearchResults("SeqTotal")
            drResult("FullTextAvailable") = drSearchResults("FullTextAvailable")
            'drResult("RowID") = intRowId

            If bolAdvancedSearch Then
                drResult("ScorePercent") = drSearchResults("ScorePercent")
                drResult("HitCount") = drSearchResults("HitCount")

                ' drResult("SearchText") = drSearchResults("SearchText")
                'Else
                'drResult("SearchText") = ""
            End If
            dtResult.Rows.Add(drResult)
        End If
        '
        ' Check for existing column
        Dim sb As New System.Text.StringBuilder
        If drSearchResults.Table.Columns.Contains("AttributeName") Then
            If Not drSearchResults("AttributeName") Is System.DBNull.Value Then
                'If dtResult.Columns(drSearchResults("AttributeName) Is Nothing Then
                '    dtResult.Columns.Add(New DataColumn(drSearchResults("AttributeName, GetType(System.String), Nothing, MappingType.Element))
                'End If
                '
                ' Append the existing data
                If Not drResult(drSearchResults("AttributeName")) Is System.DBNull.Value Then
                    sb.Append(drResult(drSearchResults("AttributeName")))
                    sb.Append("<BR>")
                End If
                '
                ' Add the new data
                Select Case drSearchResults("AttributeDataType")
                    Case "List"
                        sb.Append(drSearchResults("ValueDescription"))
                    Case "CheckBox"
                        sb.Append("X")
                    Case "Date"
                        If drSearchResults("AttributeValue") <> "" And IsDate(drSearchResults("AttributeValue")) Then
                            drSearchResults("AttributeValue") = CType(drSearchResults("AttributeValue"), DateTime).ToShortDateString
                        End If
                        sb.Append(drSearchResults("AttributeValue"))
                    Case Else
                        sb.Append(drSearchResults("AttributeValue"))
                End Select
            End If
        End If

        If sb.ToString.Length > 0 Then
            drResult(drSearchResults("AttributeName")) = sb.ToString
        End If
    End Sub

    Friend Function TrackPageRequest(ByVal objUser As Accucentric.docAssist.Web.Security.User, _
            ByVal guidSourceId As System.Guid, ByVal strDirection As String, ByVal intVersionId As Integer, _
            ByVal intImageDetId As Integer, ByVal strConnectionEncrypted As String)

        Try

            Accucentric.docAssist.Web.Data.Functions.TrackPageRequest(objUser.UserId, objUser.AccountId, guidSourceId, _
                strDirection, intVersionId, intImageDetId, strConnectionEncrypted)
        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Friend Function TrackPageRequest(ByVal objUser As Accucentric.docAssist.Web.Security.User, _
            ByVal strSourceDescription As String, ByVal strDirection As String, ByVal intVersionId As Integer, _
            ByVal intImageDetId As Integer, ByVal strConnectionEncrypted As String)

        Try
            Accucentric.docAssist.Web.Data.Functions.TrackPageRequest(objUser.UserId, objUser.AccountId, strSourceDescription, _
                strDirection, intVersionId, intImageDetId, strConnectionEncrypted)
        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Private Function CountLines(ByVal strValue As String) As Integer
        Dim intCount As Integer = 1
        Do While strValue.Length > 0
            If InStr(strValue, vbCrLf) > 0 Then
                intCount += 1
                strValue = strValue.Substring(InStr(strValue, vbCrLf) + 1)
            Else
                Exit Do
            End If
        Loop
        Return intCount
    End Function

    Friend Function UnlockCode(ByRef Request As HttpRequest) As String
        Dim rValue As String
        If Request.Url.ToString.StartsWith("http://dev.docassist") Then
            rValue = "dev.docassist?6D31D5633FD83BBCD72B875639D29C20"
        ElseIf Request.Url.ToString.StartsWith("http://asp.docassist") Then
            rValue = "asp.docassist?0CD202B0B6DAE682305076675D9BE52D"
        ElseIf Request.Url.ToString.StartsWith("https://my.docassist") Then
            rValue = "https://my.docassist.com?C7C027C54D941FB4F576C96E44DCF598"
        ElseIf Request.Url.ToString.StartsWith("http://my.docassist") Then
            rValue = "my.docassist.com?9453FF7C7B15B997B4A47906F08FE151"
        ElseIf Request.Url.ToString.StartsWith("https://dev.docassist") Then
            rValue = "https://dev.docassist?C5052D30AC00828A568DA417EB62F494"
        ElseIf Request.Url.ToString.StartsWith("https://asp.docassist") Then
            rValue = "https://asp.docassist?A4E6FAE325025FB4B1F655268F2B8D99"
        ElseIf Request.Url.ToString.StartsWith("http://demo.docassist") Then
            rValue = "demo.docassist.com?A9A0EA93421CDAEB1EC1856F9F333C58"
        ElseIf Request.Url.ToString.StartsWith("http://stage.docassist") Then
            rValue = "stage.docassist.com?6C7BF5759063BAF1DFC7DE056354F16D"
        ElseIf Request.Url.ToString.StartsWith("https://stage.docassist") Then
            rValue = "https://stage.docassist.com?D54D74D3B3226841B7738D11AA4F4E9B"
        ElseIf Request.Url.ToString.IndexOf("docassist") > 0 Then
            rValue = "docassist?61BCB4B41EF4773668CACC34B63E7687"
        ElseIf Request.Url.ToString.IndexOf("localhost") > 0 Then
            rValue = "localhost?FA9538709B40DD62A863F9A38A66BB3E"
        End If
        Return rValue
    End Function

    Public Function ErrPage(ByVal strMessage As String, ByVal blnSupportMessage As Boolean, ByRef objResponse As HttpResponse)

        Dim objEncrypt As New Encryption.Encryption
        Dim strPassMsg As String = objEncrypt.Encrypt(strMessage)

        Dim strue As String = objEncrypt.Decrypt(strPassMsg)

        objResponse.Redirect("ErrorMsg.aspx?Id=" & strPassMsg) ' & "?Support=" & blnSupportMessage)

    End Function

    'Public Function IsWorkflowOnlyUser(ByVal guidUserId As System.Guid, ByRef conSql As SqlClient.SqlConnection) As Boolean

    '    Try
    '        Dim cmdSql As New SqlClient.SqlCommand("WFStatusByUserId", conSql)
    '        cmdSql.CommandType = CommandType.StoredProcedure
    '        cmdSql.Parameters.Add("@UserId", guidUserId)

    '        Dim daSql As New SqlClient.SqlDataAdapter(cmdSql)
    '        Dim dtUsers As New DataTable

    '        daSql.Fill(dtUsers)

    '        If dtUsers.Rows.Count > 0 Then

    '            If dtUsers.Rows(0).Item("WFUserOnly") = "1" Then
    '                Return True
    '            Else
    '                Return False
    '            End If

    '        Else
    '            Return False 'No rows returned from SP
    '        End If

    '    Catch ex As Exception
    '        Throw ex
    '    Finally
    '        'If conSql.State = ConnectionState.Open Then conSql.Close()
    '    End Try

    'End Function

    Public Function CheckForAdministrator(ByVal mconSqlImage As SqlConnection, ByVal mobjuser As Accucentric.docAssist.Web.Security.User, ByVal intVersionID As Integer) As Boolean

        Dim bolReturn As Boolean
        Dim bolCloseCon As Boolean = False
        Dim meWorkflowLevel As Accucentric.docAssist.Data.Images.WorkflowUser
        Dim mintDocumentId As Integer

        Try

            'Dim mobjUser As Accucentric.docAssist.Web.Security.User

            'If mobjuser.SecAdmin Then
            '    meWorkflowLevel = Accucentric.docAssist.Data.Images.WorkflowUser.DocAdministrator
            '    bolReturn = True
            'Else
            '
            ' Create the sql command
            '
            Dim cmdSql As New SqlClient.SqlCommand("acsDocumentSecurityWorkFlowLevelByVersionID", mconSqlImage)
            cmdSql.CommandType = CommandType.StoredProcedure
            cmdSql.Parameters.Add("@UserId", mobjuser.ImagesUserId)
            cmdSql.Parameters.Add("@VersionID", intVersionID)
            '
            ' Open the connection
            '
            mconSqlImage.Open()

            meWorkflowLevel = cmdSql.ExecuteScalar()

            'If meWorkflowLevel = Accucentric.docAssist.Data.Images.WorkflowUser.WorkflowDocumentAdmin Then
            If meWorkflowLevel = -1 Then
                bolReturn = True
            Else
                bolReturn = False
            End If

            ' End If

        Catch ex As Exception

        End Try

        Return bolReturn

    End Function

    Public Function UserAccess(ByVal sqlConn As SqlClient.SqlConnection, ByVal guidUserID As System.Guid, ByVal guidAccountID As System.Guid, Optional ByVal CloseConnection As Boolean = True) As DataTable

        Try

            sqlConn.Open()

            Dim cmdSql As New SqlClient.SqlCommand("acsUserAccess")
            cmdSql.Parameters.Add("@AccountID", guidAccountID)
            cmdSql.Parameters.Add("@UserID", guidUserID)
            cmdSql.CommandType = CommandType.StoredProcedure
            cmdSql.Connection = sqlConn

            Dim daSql As New SqlClient.SqlDataAdapter(cmdSql)
            Dim dtAccess As New DataTable
            daSql.Fill(dtAccess)

            If CloseConnection Then
                sqlConn.Close()
                sqlConn.Dispose()
            End If

            Return dtAccess

        Catch ex As Exception

            sqlConn.Close()
            sqlConn.Dispose()

        End Try

    End Function

    Public Function PasswordStrongValidation(ByVal strPassword As String) As Boolean
        'must include at least one upper case letter, one lower case letter, and one numeric digit
        'Dim regStrongPassword As New System.Text.RegularExpressions.Regex("^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,15}$")
        Dim regStrongPassword As New System.Text.RegularExpressions.Regex("^(?=.*\d).{8,20}$")
        Return regStrongPassword.IsMatch(strPassword)
    End Function

    Public ReadOnly Property PasswordStrongValidationMessage() As String
        Get
            Return "Your password must be 8 to 20 characters and include one number."
        End Get
    End Property


    Public Function CheckWorkflow(ByVal intUserId As Integer, ByVal intVersionId As Integer, ByVal objUser As Accucentric.docAssist.Web.Security.User, Optional ByRef intRouteId As Integer = 0, _
    Optional ByRef DocumentId As Integer = -1) As WorkflowMode

        Try
            Dim objWf As New Accucentric.docAssist.Data.Images.Workflow(Functions.BuildConnection(objUser))

            Dim mode As Integer
            objWf.WFVersionIdMode(intVersionId, intUserId, mode, intRouteId, IIf(DocumentId = -1, -55, DocumentId))

            Return mode
        Catch ex As Exception

        End Try

    End Function

    Public Function VBS(ByVal mVBSStatement As String, ByVal mValue As String) As String

        Dim mScripter As New QWhale.Scripter.ScriptRun
        Dim l As Object

        Dim mFunction As String

        mVBSStatement = mVBSStatement.Replace("'", Chr(34))

        mFunction = "Public Module Script" & vbCrLf & " Public Function Dummy as String " & vbCrLf & "Dim param as string  " & vbCrLf & " param= " & Chr(34) & mValue & Chr(34) & vbCrLf & " Return " & mVBSStatement & vbCrLf & "End Function " & vbCrLf & " End Module"
        mScripter.ScriptText = mFunction

        With mScripter

            .ScriptText = mFunction
            .Language = QWhale.Scripter.ScriptLanguage.VB

            VBS = .RunMethod("Dummy")

            Return VBS

        End With

    End Function

    Public Function UserVersionOnlyCheck(ByVal intImagesUserId As Integer, ByVal intVersionId As Integer, ByRef intReturnedVersionId As Integer, ByRef intRestrict As Integer, _
    ByVal sqlConnCompany As SqlClient.SqlConnection, ByRef intFileType As Integer, ByRef DocId As Integer, ByRef PageCount As Integer)

        Try
            If sqlConnCompany.State <> ConnectionState.Open Then sqlConnCompany.Open()

            Dim sqlCmd As New SqlClient.SqlCommand("UserVersionOnlyCheck", sqlConnCompany)
            sqlCmd.CommandType = CommandType.StoredProcedure
            sqlCmd.Parameters.Add("@UserId", intImagesUserId)
            sqlCmd.Parameters.Add("@VersionId", intVersionId)

            Dim da As New SqlClient.SqlDataAdapter(sqlCmd)
            Dim dt As New DataTable

            da.Fill(dt)

            If dt.Rows.Count > 0 Then
                intReturnedVersionId = dt.Rows(0)("VersionId")
                intRestrict = dt.Rows(0)("RestrictUser")
                intFileType = dt.Rows(0)("ImageType")
                DocId = dt.Rows(0)("DocumentId")
                PageCount = dt.Rows(0)("SeqTotal")
            End If

        Catch ex As Exception

        End Try

    End Function

    Public Function SignOut(ByVal webPage As System.Web.UI.Page, Optional ByVal RedirectToLoginPage As Boolean = True)

        Try

            Dim aCookie As HttpCookie
            Dim i As Integer
            Dim cookieName As String
            Dim limit As Integer = webPage.Request.Cookies.Count - 1
            For i = 0 To limit

                If webPage.Request.Cookies(i).Name = "docAssistSave" Then i += 1
                If webPage.Request.Cookies(i).Name = SESSION_COOKIE_KEY Then i += 1
                cookieName = webPage.Request.Cookies(i).Name
                aCookie = New HttpCookie(cookieName)
                aCookie.Expires = DateTime.Now.AddDays(-1)
                webPage.Response.Cookies.Add(aCookie)

            Next

            webPage.Response.Cookies(SESSION_COOKIE_KEY)(SESSION_COOKIE) = Now
            webPage.Response.Cookies(SESSION_COOKIE_KEY).Expires = Now.AddDays(7) 'Set this so that the next time the browser loads it wont load a timeout message, the login process will reset it to the interval.

            If IsNothing(webPage.Session("CustomLogoId")) Then
                Dim cookieSearch As New cookieSearch(webPage.Page)
                cookieSearch.Expire()
                If RedirectToLoginPage = True Then webPage.Response.Redirect("Default.aspx")
            Else
                Dim strLogoId As String = webPage.Session("CustomLogoId")
                Dim cookieSearch As New cookieSearch(webPage.Page)
                cookieSearch.Expire()
                If RedirectToLoginPage Then webPage.Response.Redirect("Default.aspx?LID=" & strLogoId)
            End If

        Catch ex As Exception

            Throw New Exception("Error logging out: " & ex.Message & ex.StackTrace)

        End Try

    End Function

    Public Function GetSessionId(ByVal strUserId As String) As String

        Dim objEncryption As Encryption.Encryption
        Dim conSql As New SqlClient.SqlConnection(objEncryption.Decrypt(ConfigurationSettings.AppSettings("Connection")))
        conSql.Open()

        Try
            Dim cmdSql As New SqlClient.SqlCommand("SELECT SessionId FROM Users WHERE UserId = @UserId", conSql)
            cmdSql.Parameters.Add("@UserId", strUserId)
            Return cmdSql.ExecuteScalar
        Catch ex As Exception

        Finally
            If conSql.State <> ConnectionState.Closed Then
                conSql.Close()
                conSql.Dispose()
            End If
        End Try

    End Function

    Public Function CheckValidEmailAddress(ByVal Address As String) As Boolean
        Dim regExEmail As New System.Text.RegularExpressions.Regex("\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*")
        Return regExEmail.IsMatch(Address)
    End Function

    Public Function GetDataAsByteArray(ByVal strFilename As String) As Byte()

        Dim fsData As System.IO.FileStream
        Dim xTemp() As Byte

        fsData = New System.IO.FileStream(strFilename, IO.FileMode.Open, IO.FileAccess.Read)
        Dim len As Integer = CType(fsData.Length, Integer)
        Dim rData As New System.IO.BinaryReader(fsData)

        xTemp = rData.ReadBytes(len)

        rData.Close()
        fsData.Close()

        Return xTemp

    End Function
    Public Function GetStringDataAsByteArray(ByVal strFilename As String) As Byte()

        Dim fsData As System.IO.FileStream
        Dim xTemp() As Byte

        fsData = New System.IO.FileStream(strFilename, IO.FileMode.Open, IO.FileAccess.Read)
        Dim len As Integer = CType(fsData.Length, Integer)
        Dim rData As New System.IO.BinaryReader(fsData)

        xTemp = rData.ReadBytes(len)

        rData.Close()
        fsData.Close()

        Return xTemp

    End Function

    Public Function GetByteArrayAsData(ByVal strFilename As String, ByVal Bytes As Byte(), ByVal Directory As String) As String

        Try

            Dim dir As String = Directory & System.Guid.NewGuid.ToString.Replace("-", "") & "\"
            Dim filepath As String = dir & strFilename

            System.IO.Directory.CreateDirectory(dir)

            Dim fs As New System.IO.FileStream(filepath, IO.FileMode.CreateNew)
            Dim writer As New System.IO.BinaryWriter(fs)

            For Each b As Byte In Bytes
                writer.Write(b)
            Next

            writer.Close()
            fs.Close()

            Return filepath

        Catch ex As Exception

        End Try

    End Function

    Public Function GetByteArrayAsFile(ByVal strFilename As String, ByVal Bytes As Byte()) As Boolean

        Try

            Dim filepath As String = strFilename

            Dim fs As New System.IO.FileStream(filepath, IO.FileMode.CreateNew)
            Dim writer As New System.IO.BinaryWriter(fs)

            For Each b As Byte In Bytes
                writer.Write(b)
            Next

            writer.Close()
            fs.Close()

            Return True

        Catch ex As Exception

            Return False

        End Try

    End Function

    Public Function GetBytesAsMegs(ByVal inSize As Object) As String

        Try
            Dim CheckSize As Object, LastSize As Object   ' Coerced into decimal 
            Dim LoopSize As Long

            CheckSize = CDec(1)
            For LoopSize = 0 To 8
                LastSize = CheckSize
                CheckSize = (2 ^ 10) ^ LoopSize
                If (CheckSize > inSize) Then Exit For
            Next LoopSize

            ' Byte, KiloB., MegaB., GigaB., TeraB., PetaB., ExaB., ZettaB., YottaB. 
            Return Format(inSize / IIf(LoopSize < 9, LastSize, CheckSize), IIf(LoopSize > 1, "0.00", "0")) & " " & Choose(LoopSize, "bytes", "KB", "MB", "GB", "Tb", "Pb", "Eb", "Zb", "Yb")

        Catch ex As Exception

        End Try

    End Function

    Public Function CompressBytesAsZipFile(ByVal by() As Byte, ByVal OriginalFileName As String) As Byte()

        Dim mem As New System.IO.MemoryStream
        Dim zip As New ICSharpCode.SharpZipLib.Zip.ZipOutputStream(mem)
        Dim zipentry As New ICSharpCode.SharpZipLib.Zip.ZipEntry(OriginalFileName)
        Dim crc As New ICSharpCode.SharpZipLib.Checksums.Crc32

        zip.SetLevel(6)
        zipentry.DateTime = DateTime.Now
        zipentry.Size = by.Length

        crc.Reset()
        crc.Update(by)

        zip.PutNextEntry(zipentry)
        zip.Write(by, 0, by.Length)

        Dim zipBytes As Byte()

        zip.Finish()
        zip.Close()

        zipBytes = mem.ToArray
        Return zipBytes

    End Function

    Public Function LinkTags(ByVal Tags As String) As String

        Try
            If Tags.Trim.Length = 0 Then Return ""

            Dim TagArray As String() = Tags.Split(" ")
            Dim sb As New System.Text.StringBuilder

            For X As Integer = 0 To TagArray.Length - 1
                If TagArray(X).ToString.Trim.Length > 0 Then sb.Append("<a href=""#"" onclick=""tagSearch('" & TagArray(X).ToString & "');return false;"">" & TagArray(X).ToString & "</a>&nbsp;")
            Next

            Return sb.ToString

        Catch ex As Exception

        End Try

    End Function

    Public Function LinkAttribute(ByVal AttributeValue As String) As String

        Try
            If AttributeValue.Trim.Length = 0 Then Return ""
            Return "<a href=""#""><span onclick=""tagSearch('" & AttributeValue & "');return false;"">" & AttributeValue & "</span></a>&nbsp;"
        Catch ex As Exception

        End Try

    End Function

    Public Function CheckTimeout(ByVal webPage As System.Web.UI.Page)

        Try

            Dim Request As HttpRequest = webPage.Request
            Dim Response As HttpResponse = webPage.Response

            If Request.Cookies("docAssist") Is Nothing Then

                If IsNothing(Request.Cookies(SINGLE_SESSION_COOKIE)) Then
                    'New browser that failed authentication
                    Response.Redirect("Default.aspx" & Request.Url.Query)
                    Exit Function
                Else
                    'Timed out browser

                    If IsNothing(Request.Cookies(SESSION_COOKIE_KEY)) Then
                        'Dim newTimer As New HttpCookie(SESSION_COOKIE_KEY)
                        'Response()
                        Response.Cookies(SESSION_COOKIE_KEY)(SESSION_COOKIE) = Now.AddDays(-1)
                    End If

                    If Not IsNothing(Request.Cookies(SESSION_COOKIE_KEY)) Then

                        If Request.Cookies(SESSION_COOKIE_KEY)(SESSION_COOKIE) = "" Then Response.Cookies(SESSION_COOKIE_KEY)(SESSION_COOKIE) = Now.AddDays(-1)

                        If Request.Cookies(SESSION_COOKIE_KEY)(SESSION_COOKIE) <> "" Then
                            If Now < Request.Cookies(SESSION_COOKIE_KEY)(SESSION_COOKIE) Then
                                Functions.SignOut(webPage, False)
                                Response.Redirect("Default.aspx?ErrId=" & ErrorCodes.SESSION_TIMEOUT)
                                Exit Function
                            End If
                        Else
                            Response.Redirect("Default.aspx" & Request.Url.Query)
                            Exit Function
                        End If

                    Else
                        Response.Redirect("Default.aspx" & Request.Url.Query)
                        Exit Function
                    End If

                End If

            End If
        Catch ex As Exception
            If ex.Message <> "Thread was being aborted." Then ThrowLoginError(webPage, ErrorCodes.ERROR_CHECKING_TIMEOUT)
        End Try

    End Function

    Public Function ThrowLoginError(ByVal webPage As System.Web.UI.Page, ByVal ErrCode As Integer)
        webPage.Response.Redirect("Default.aspx?ErrId=" & ErrCode)
    End Function

    Public Function ModuleSetting(ByVal [Module] As ModuleCode, ByVal User As SECURITY.User, Optional ByVal conCompany As SqlClient.SqlConnection = Nothing) As Boolean

        Dim conSqlImage As SqlClient.SqlConnection

        If Not IsNothing(conCompany) Then
            conSqlImage = conCompany
        Else
            conSqlImage = BuildConnection(User)
        End If

        Try

            Dim cmdSql As New SqlCommand("SMModuleAccess", conSqlImage)
            cmdSql.CommandType = CommandType.StoredProcedure
            cmdSql.Parameters.Add("@AccountID", User.AccountId.ToString)
            cmdSql.Parameters.Add("@ModuleID", [Module])

            If IsNothing(conCompany) Then conSqlImage.Open()

            Return cmdSql.ExecuteScalar

        Catch ex As Exception

        Finally

            If IsNothing(conCompany) Then

                If conSqlImage.State <> ConnectionState.Closed Then
                    conSqlImage.Close()
                    conSqlImage.Dispose()
                End If

            End If

        End Try

    End Function

    Public Function UserCount(ByVal User As SECURITY.User) As Integer

        Dim conSqlImage As SqlClient.SqlConnection
        conSqlImage = BuildConnection(User)

        Try

            Dim cmdSql As New SqlCommand("SELECT COUNT(*) FROM ViewWebUsers", conSqlImage)
            cmdSql.CommandType = CommandType.Text

            conSqlImage.Open()

            Return cmdSql.ExecuteScalar

        Catch ex As Exception

        Finally

            If conSqlImage.State <> ConnectionState.Closed Then
                conSqlImage.Close()
                conSqlImage.Dispose()
            End If

        End Try

    End Function

    Public Function LogAuthentication(ByVal AccountID As String, ByVal UserID As String, ByVal EmailAddress As String, ByVal SourceIP As String, ByVal WebServer As String, ByVal LoginType As String, _
    ByVal Result As String, ByVal OtherData As String)

        Dim conSqlMaster As SqlClient.SqlConnection

        Try
            conSqlMaster = Functions.BuildMasterConnection
            Dim AuthLogging As New Accucentric.docAssist.Data.Images.Logging(conSqlMaster)

            AuthLogging.LogAuthentication(AccountID, UserID, EmailAddress, SourceIP, WebServer, LoginType, Result, OtherData)

        Catch ex As Exception

        Finally

            If conSqlMaster.State <> ConnectionState.Closed Then
                conSqlMaster.Close()
                conSqlMaster.Dispose()
            End If

        End Try

    End Function


    Public Function TrackRecentItem(ByVal UserObject As Accucentric.docAssist.Web.Security.User, ByVal RecentKey As String, ByVal KeyValue As String) As Boolean

        'RecentKey
        'QS - Quick Search
        'VW - View
        'SV - Save

        'KeyValue is version id

        Dim consql As New SqlClient.SqlConnection

        Try
            consql = Functions.BuildConnection(UserObject)
            consql.Open()

            Dim cmdSql As New SqlClient.SqlCommand("SMRecentItemInsert", consql)
            cmdSql.CommandType = CommandType.StoredProcedure
            cmdSql.Parameters.Add("@UserId", UserObject.ImagesUserId)
            cmdSql.Parameters.Add("@RecentKey", RecentKey)
            cmdSql.Parameters.Add("@KeyValue", KeyValue)
            cmdSql.ExecuteNonQuery()

            Return True

        Catch ex As Exception

            Return False

        Finally

            If consql.State <> ConnectionState.Closed Then
                consql.Close()
                consql.Dispose()
            End If

        End Try

        Try

        Catch ex As Exception

        End Try

    End Function

    Function GetAttributesByDocumentId(ByVal DocumentId As Integer, ByVal UserId As Integer, ByVal conCompany As SqlConnection, Optional ByVal CloseConnection As Boolean = False) As DataTable

        Try

            If conCompany.State <> ConnectionState.Open Then conCompany.Open()
            Dim Command As New SqlClient.SqlCommand("acsAttributesByDocumentId", conCompany)
            Command.CommandType = CommandType.StoredProcedure
            Command.Parameters.Add("@DocumentId", DocumentId)
            Command.Parameters.Add("@UserId", UserId)

            Dim DataAdapter As New SqlClient.SqlDataAdapter(Command)
            Dim dt As New DataTable
            DataAdapter.Fill(dt)

            Return dt

        Catch ex As Exception
            Return Nothing
        Finally
            If CloseConnection Then
                If conCompany.State <> ConnectionState.Closed Then
                    conCompany.Close()
                    conCompany.Dispose()
                End If
            End If
        End Try

    End Function

    Function ChangePassword(ByVal UserId As String, ByVal AccountId As String, ByVal NewPassword As String, ByVal conCompany As SqlConnection, Optional ByVal CloseConnection As Boolean = False)

        Try

            If conCompany.State <> ConnectionState.Open Then conCompany.Open()

            Dim Command As New SqlClient.SqlCommand("SECUserUpdatePassword", conCompany)
            Command.CommandType = CommandType.StoredProcedure
            Command.Parameters.Add("@AccountID", AccountId)
            Command.Parameters.Add("@UserID", UserId)
            Command.Parameters.Add("@NewPassword", NewPassword)
            Command.ExecuteScalar()

        Catch ex As Exception
            Return Nothing
        Finally
            If CloseConnection Then
                If conCompany.State <> ConnectionState.Closed Then
                    conCompany.Close()
                    conCompany.Dispose()
                End If
            End If

        End Try

    End Function

    Function LogException(ByVal UserId As String, ByVal AccountId As String, ByVal ErrorNumber As String, ByVal ErrorDescription As String, ByVal ErrorStack As String, ByVal ServerName As String, ByVal ServerDetail As String, ByVal ReferralPage As String, ByVal conMaster As SqlConnection, ByVal CloseConnection As Boolean) As Integer

        Try

            If conMaster.State <> ConnectionState.Open Then conMaster.Open()

            Dim Command As New SqlClient.SqlCommand("SMErrorInsert", conMaster)
            Command.CommandType = CommandType.StoredProcedure
            Command.Parameters.Add("@UserID", UserId)
            Command.Parameters.Add("@AccountId", AccountId)
            Command.Parameters.Add("@ErrorNumber", ErrorNumber)
            Command.Parameters.Add("@ServerName", ServerName)
            Command.Parameters.Add("@ErrorDesc", ErrorDescription)
            Command.Parameters.Add("@ServerDetail", ServerDetail)
            Command.Parameters.Add("@ReferralPage", ReferralPage)
            Command.Parameters.Add("@ErrorStack", ErrorStack)

            Dim paramErrorId As New SqlParameter("@ErrorId", Nothing)
            paramErrorId.Direction = ParameterDirection.Output

            Command.Parameters.Add(paramErrorId)

            Command.ExecuteNonQuery()

        Catch ex As Exception

            Return Nothing

        Finally

            If CloseConnection Then
                If conMaster.State <> ConnectionState.Closed Then
                    conMaster.Close()
                    conMaster.Dispose()
                End If

            End If

        End Try

    End Function

    Function SaveSessionState(ByVal StateGuid As String, ByVal StateData As String)

        Dim conSql As SqlClient.SqlConnection

        Try

            conSql = Functions.BuildMasterConnection

            Dim cmdSql As New SqlClient.SqlCommand("SMSessionSet", conSql)
            cmdSql.CommandType = CommandType.StoredProcedure
            cmdSql.Parameters.Add("@AccountID", "not used")
            cmdSql.Parameters.Add("@WebUserId", "not used")
            cmdSql.Parameters.Add("@StateGUID", StateGuid)
            cmdSql.Parameters.Add("@StateData", StateData)
            cmdSql.ExecuteNonQuery()

        Catch ex As Exception

        Finally

            If conSql.State <> ConnectionState.Closed Then
                conSql.Close()
                conSql.Dispose()
            End If

        End Try

    End Function

    Function LoadSessionState(ByVal StateGuid As String) As String

        Dim conSql As SqlClient.SqlConnection

        Try

            conSql = Functions.BuildMasterConnection

            Dim cmdSql As New SqlClient.SqlCommand("SMSessionGet", conSql)
            cmdSql.CommandType = CommandType.StoredProcedure
            cmdSql.Parameters.Add("@StateGUID", StateGuid)
            Return cmdSql.ExecuteScalar()

        Catch ex As Exception

        Finally

            If conSql.State <> ConnectionState.Closed Then
                conSql.Close()
                conSql.Dispose()
            End If

        End Try

    End Function

    Function FormatNumber(ByVal Number As Double) As String

        Return Number.ToString("0.00")
        'Try
        '    Return Number.ToString("0.00")
        'Catch ex As Exception
        '    Return Number
        'End Try

    End Function

    Sub sendEmailMessage(ByVal MessageType As EmailType, ByVal MailTo As String, ByVal CC As String, ByVal Bcc As String, ByVal Arguments As Collection, ByVal Page As System.Web.UI.Page, Optional ByVal Subject As String = "", Optional ByVal fromAddress As String = "support@docassist.com")

        Dim email As New SendMail.Smtp
        email.to.Add(MailTo)
        email.cc.Add(CC)
        email.bcc.Add(Bcc)
        email.server = ConfigurationSettings.AppSettings("MailServer")
        email.serverport = ConfigurationSettings.AppSettings("MailServerPort")
        email.from = fromAddress
        email.displayname = "docAssist"

        Dim template As System.IO.StreamReader

        Try

            Select Case MessageType
                Case EmailType.NEW_USER

                    template = New System.IO.StreamReader(Page.Server.MapPath("resources/email/") & "new_user.txt")

                    Dim emailMessage As String = template.ReadToEnd

                    For x As Integer = 1 To Arguments.Count
                        Dim prm As docAssistWeb.EmailParameter = CType(Arguments.Item(x), docAssistWeb.EmailParameter)
                        emailMessage = emailMessage.Replace("[" & prm.Name.ToUpper & "]", prm.Val)
                    Next

                    email.bodyHtml = True
                    email.bodyText = emailMessage
                    email.subject = "Your docAssist Account Information"
                    email.Send()

                Case EmailType.SHARE_DOCUMENT


                    template = New System.IO.StreamReader(Page.Server.MapPath("resources/email/") & "shareDocument.html")

                    Dim emailMessage As String = template.ReadToEnd

                    For x As Integer = 1 To Arguments.Count
                        Dim prm As docAssistWeb.EmailParameter = CType(Arguments.Item(x), docAssistWeb.EmailParameter)
                        emailMessage = emailMessage.Replace("[" & prm.Name.ToUpper & "]", prm.Val)
                    Next

                    email.bodyHtml = True
                    email.bodyText = emailMessage
                    email.subject = Subject
                    email.Send()

                Case EmailType.SHARE_FOLDER


                    template = New System.IO.StreamReader(Page.Server.MapPath("resources/email/") & "shareItem.html")

                    Dim emailMessage As String = template.ReadToEnd

                    For x As Integer = 1 To Arguments.Count
                        Dim prm As docAssistWeb.EmailParameter = CType(Arguments.Item(x), docAssistWeb.EmailParameter)
                        emailMessage = emailMessage.Replace("[" & prm.Name.ToUpper & "]", prm.Val)
                    Next

                    email.bodyHtml = True
                    email.bodyText = emailMessage
                    email.subject = Subject
                    email.Send()

            End Select

        Catch ex As Exception

        Finally
            template.Close()
        End Try

    End Sub

    Function GeneratePassword() As String

        'Generate password
        Dim alphabet() As String = {"A", "a", "B", "b", "C", "D", "d", "E", "e", "F", "f", "G", "g", "H", "h", "J", "j", "L", "K", "k", "M", "m", "N", "n", "P", "p", "Q", "q", "R", "r", "S", "s", "T", "t", "U", "u", "V", "v", "W", "w", "X", "x", "Y", "y", "Z", "z"}
        Dim numbers() As String = {"2", "3", "4", "5", "6", "7", "8", "9"}

        Dim rnd As New Random
        Dim random As String = String.Empty
        For i As Integer = 0 To 3
            random += alphabet(rnd.Next(0, 46))
            random += numbers(rnd.Next(0, 8))
        Next

        Return random

    End Function

    Public Function ExportPDF(ByVal objUser As User, ByVal VersionID As Integer, ByVal StartPage As Integer, ByVal EndPage As Integer, ByVal Web As System.Web.UI.Page, _
    ByVal IncludeAnnotations As Boolean) As String

        Dim conSql As SqlClient.SqlConnection = BuildConnection(objUser)
        If Not conSql.State = ConnectionState.Open Then conSql.Open()

        'Dim ai As New Atalasoft.Imaging.AtalaImage

        Try

            Dim fileName As String = System.Guid.NewGuid().ToString & ".pdf"

            'Get the images from the database
            Dim sqlCmd As New SqlClient.SqlCommand("acsImageDetailRange", conSql)
            sqlCmd.CommandType = CommandType.StoredProcedure
            sqlCmd.Parameters.Add("@VersionID", VersionID)
            sqlCmd.Parameters.Add("@PageStart", StartPage)
            sqlCmd.Parameters.Add("@PageEnd", EndPage)

            Dim dt As New DataTable
            Dim da As New SqlClient.SqlDataAdapter(sqlCmd)
            da.Fill(dt)

            ' Create a new PdfImageCollection and add your images.
            'Dim col As PdfImageCollection = New PdfImageCollection
            Dim col As New Atalasoft.Imaging.ImageCollection

            Dim cmd As New SqlClient.SqlCommand("SMScanQualityGet", conSql)
            cmd.CommandType = CommandType.StoredProcedure
            Dim qSet As New SqlClient.SqlParameter
            qSet.ParameterName = "@Quality"
            qSet.Direction = ParameterDirection.Output
            qSet.DbType = DbType.Int32
            cmd.Parameters.Add(qSet)
            cmd.ExecuteNonQuery()
            Dim value As Integer = qSet.Value

            For Each dr As DataRow In dt.Rows
                Dim bytImage As Byte()
                bytImage = dr("Image")

                Dim tmpai As New Atalasoft.Imaging.AtalaImage
                tmpai = tmpai.FromByteArray(bytImage)
                
                Select Case value
                    Case 0 'B/W
                        tmpai.Resolution = New Atalasoft.Imaging.Dpi(150, 150, ResolutionUnit.DotsPerInch)
                        If tmpai.ColorDepth <> 1 Then tmpai = tmpai.GetChangedPixelFormat(Atalasoft.Imaging.PixelFormat.Pixel1bppIndexed)
                    Case 1 'Gray
                        If tmpai.ColorDepth < 8 Then
                            'Cannot apply grayscale, use 1 bit
                            tmpai.Resolution = New Atalasoft.Imaging.Dpi(150, 150, ResolutionUnit.DotsPerInch)
                            If tmpai.ColorDepth <> 1 Then
                                Dim cmdDt As New ImageProcessing.Document.DynamicThresholdCommand
                                tmpai = cmdDt.Apply(tmpai).Image
                            End If
                        Else
                            tmpai.Resolution = New Atalasoft.Imaging.Dpi(200, 200, ResolutionUnit.DotsPerInch)
                            If tmpai.ColorDepth <> 8 Then tmpai = tmpai.GetChangedPixelFormat(Atalasoft.Imaging.PixelFormat.Pixel8bppGrayscale)
                        End If
                    Case 2 'Color
                        Dim rawDepth As Integer = tmpai.ColorDepth
                        If rawDepth < 24 Then
                            'Cant apply color, find best alternative
                            If rawDepth < 8 Then
                                'Cannot apply grayscale, use 1 bit
                                tmpai.Resolution = New Atalasoft.Imaging.Dpi(150, 150, ResolutionUnit.DotsPerInch)
                                'If Image.ColorDepth <> 1 Then Image = Image.GetChangedPixelFormat(Atalasoft.Imaging.PixelFormat.Pixel1bppIndexed)
                                If tmpai.ColorDepth <> 1 Then
                                    Dim cmdDt As New ImageProcessing.Document.DynamicThresholdCommand
                                    tmpai = cmdDt.Apply(tmpai).Image
                                End If
                            Else
                                tmpai.Resolution = New Atalasoft.Imaging.Dpi(200, 200, ResolutionUnit.DotsPerInch)
                                If tmpai.ColorDepth <> 8 Then tmpai = tmpai.GetChangedPixelFormat(Atalasoft.Imaging.PixelFormat.Pixel8bppGrayscale)
                            End If
                        Else
                            tmpai.Resolution = New Atalasoft.Imaging.Dpi(300, 300, ResolutionUnit.DotsPerInch)
                            If tmpai.ColorDepth <> 24 Then tmpai = tmpai.GetChangedPixelFormat(Atalasoft.Imaging.PixelFormat.Pixel24bppBgr)
                        End If

                End Select


                If IncludeAnnotations = True Then
                    If Not dr("Annotation") Is System.DBNull.Value Then

                        Dim layers As New LayerCollection
                        layers.Add(New LayerAnnotation)
                        layers.Add(New LayerAnnotation)

                        Dim bytAnnotation As Byte() = dr("Annotation")

                        Dim msR As New MemoryStream(bytAnnotation, 0, bytAnnotation.Length)
                        msR.Seek(0, SeekOrigin.Begin)

                        Dim tmpViewer As New Atalasoft.Imaging.WebControls.Annotations.WebAnnotationController
                        tmpViewer.Load(msR, AnnotationDataFormat.Xmp)

                        For Each ann As AnnotationUI In tmpViewer.Layers(0).Items
                            layers(0).Items.Add(ann)
                        Next

                        tmpViewer.Dispose()

                        tmpai = BurnAnnotations(layers, tmpai)

                    End If
                End If

                col.Add(tmpai)
            Next

            ' Create the PDF.
            Dim pdf As PdfEncoder = New PdfEncoder

            ' Set any properties.
            pdf.JpegQuality = 100
            pdf.Metadata = New PdfMetadata("docAssist Export", "docAssist", "Document Export", "", "", "docAssist", DateTime.Now, DateTime.Now)

            ' Make each image fit into an 8.5 x 11 inch page (612 x 792 @ 72 DPI).
            pdf.SizeMode = PdfPageSizeMode.FitToPage
            pdf.PageSize = New Size(612, 792)

            'pdf.Save(fs, col, Nothing)
            'ai = ai.GetChangedPixelFormat(Atalasoft.Imaging.PixelFormat.Pixel1bppIndexed)
            'ai.Save(Web.Server.MapPath("tmp\" & fileName), pdf, Nothing)

            'ai.Dispose()

            col.Save(Web.Server.MapPath("tmp\" & fileName), pdf, Nothing)
            col.Dispose()

            Return fileName

        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Public Function ExportPublicPDF(ByVal conSql As SqlClient.SqlConnection, ByVal VersionID As Integer, ByVal StartPage As Integer, _
    ByVal EndPage As Integer, ByVal Web As System.Web.UI.Page, ByVal IncludeAnnotations As Boolean) As Byte()

        If Not conSql.State = ConnectionState.Open Then conSql.Open()

        Try

            Dim fileName As String = System.Guid.NewGuid().ToString & ".pdf"

            'Get the images from the database
            Dim sqlCmd As New SqlClient.SqlCommand("acsImageDetailRange", conSql)
            sqlCmd.CommandType = CommandType.StoredProcedure
            sqlCmd.Parameters.Add("@VersionID", VersionID)
            sqlCmd.Parameters.Add("@PageStart", StartPage)
            sqlCmd.Parameters.Add("@PageEnd", EndPage)

            Dim dt As New DataTable
            Dim da As New SqlClient.SqlDataAdapter(sqlCmd)
            da.Fill(dt)

            ' Create a new PdfImageCollection and add your images.
            'Dim col As PdfImageCollection = New PdfImageCollection
            Dim col As New Atalasoft.Imaging.ImageCollection

            For Each dr As DataRow In dt.Rows
                Dim bytImage As Byte()
                bytImage = dr("Image")

                Dim tmpai As New Atalasoft.Imaging.AtalaImage
                tmpai = tmpai.FromByteArray(bytImage)
                If tmpai.ColorDepth <> 1 Then tmpai = tmpai.GetChangedPixelFormat(Atalasoft.Imaging.PixelFormat.Pixel1bppIndexed)

                'Dim dynThreshold As New Atalasoft.Imaging.ImageProcessing.Document.DynamicThresholdCommand
                'tmpai = dynThreshold.Apply(tmpai).Image

                tmpai.Resolution = New Atalasoft.Imaging.Dpi(150, 150, ResolutionUnit.DotsPerInch)

                If IncludeAnnotations = True Then
                    If Not dr("Annotation") Is System.DBNull.Value Then

                        Dim layers As New LayerCollection
                        layers.Add(New LayerAnnotation)
                        layers.Add(New LayerAnnotation)

                        Dim bytAnnotation As Byte() = dr("Annotation")

                        Dim msR As New MemoryStream(bytAnnotation, 0, bytAnnotation.Length)
                        msR.Seek(0, SeekOrigin.Begin)

                        Dim tmpViewer As New Atalasoft.Imaging.WebControls.Annotations.WebAnnotationController
                        tmpViewer.Load(msR, AnnotationDataFormat.Xmp)

                        For Each ann As AnnotationUI In tmpViewer.Layers(0).Items
                            layers(0).Items.Add(ann)
                        Next

                        tmpViewer.Dispose()

                        tmpai = BurnAnnotations(layers, tmpai)

                    End If
                End If

                col.Add(tmpai)
            Next

            ' Create the PDF.
            Dim pdf As PdfEncoder = New PdfEncoder

            ' Set any properties.
            pdf.JpegQuality = 100
            pdf.Metadata = New PdfMetadata("docAssist Export", "docAssist", "Document Export", "", "", "docAssist", DateTime.Now, DateTime.Now)

            ' Make each image fit into an 8.5 x 11 inch page (612 x 792 @ 72 DPI).
            pdf.SizeMode = PdfPageSizeMode.FitToPage
            pdf.PageSize = New Size(612, 792)

            'pdf.Save(fs, col, Nothing)
            'ai = ai.GetChangedPixelFormat(Atalasoft.Imaging.PixelFormat.Pixel1bppIndexed)
            'ai.Save(Web.Server.MapPath("tmp\" & fileName), pdf, Nothing)

            'ai.Dispose()

            'col.Save(Web.Server.MapPath("../tmp" & fileName), pdf, Nothing)
            'col.Dispose()

            Dim bytes() As Byte
            bytes = col.ToByteArray(pdf, Nothing)
            col.Dispose()

            Return bytes

        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Public Function ExportPublicPDFByImageId(ByVal conSql As SqlClient.SqlConnection, ByVal ImageId As Integer, ByVal StartPage As Integer, _
    ByVal EndPage As Integer, ByVal fileName As String) As String

        If Not conSql.State = ConnectionState.Open Then conSql.Open()

        Try

            'Get the images from the database
            Dim sqlCmd As New SqlClient.SqlCommand("acsImageDetailRange", conSql)
            sqlCmd.CommandType = CommandType.StoredProcedure
            sqlCmd.Parameters.Add("@ImageId", ImageId)
            sqlCmd.Parameters.Add("@PageStart", StartPage)
            sqlCmd.Parameters.Add("@PageEnd", EndPage)

            Dim dt As New DataTable
            Dim da As New SqlClient.SqlDataAdapter(sqlCmd)
            da.Fill(dt)

            Dim col As New Atalasoft.Imaging.ImageCollection

            For Each dr As DataRow In dt.Rows

                Dim bytImage As Byte()
                bytImage = dr("Image")

                Dim tmpai As New Atalasoft.Imaging.AtalaImage
                tmpai = tmpai.FromByteArray(bytImage)
                If tmpai.ColorDepth <> 1 Then tmpai = tmpai.GetChangedPixelFormat(Atalasoft.Imaging.PixelFormat.Pixel1bppIndexed)

                tmpai.Resolution = New Atalasoft.Imaging.Dpi(150, 150, ResolutionUnit.DotsPerInch)

                col.Add(tmpai)

            Next

            ' Create the PDF.
            Dim pdf As PdfEncoder = New PdfEncoder

            ' Set any properties.
            pdf.JpegQuality = 100
            pdf.Metadata = New PdfMetadata("docAssist Export", "docAssist", "Document Export", "", "", "docAssist", DateTime.Now, DateTime.Now)

            ' Make each image fit into an 8.5 x 11 inch page (612 x 792 @ 72 DPI).
            pdf.SizeMode = PdfPageSizeMode.FitToPage
            pdf.PageSize = New Size(612, 792)

            Dim bytes() As Byte

            col.Save(fileName, pdf, Nothing)
            col.Dispose()

            Return fileName

        Catch ex As Exception

            Throw ex

        End Try

    End Function

    Public Function ExportTIF(ByVal objUser As User, ByVal VersionID As Integer, ByVal StartPage As Integer, ByVal EndPage As Integer, ByVal Web As System.Web.UI.Page, _
    ByVal IncludeAnnotations As Boolean) As String

        Dim conSql As SqlClient.SqlConnection = BuildConnection(objUser)
        If Not conSql.State = ConnectionState.Open Then conSql.Open()

        'Dim ai As New Atalasoft.Imaging.AtalaImage

        Try

            Dim fileName As String = System.Guid.NewGuid().ToString & ".tif"

            'Get the images from the database
            Dim sqlCmd As New SqlClient.SqlCommand("acsImageDetailRange", conSql)
            sqlCmd.CommandType = CommandType.StoredProcedure
            sqlCmd.Parameters.Add("@VersionID", VersionID)
            sqlCmd.Parameters.Add("@PageStart", StartPage)
            sqlCmd.Parameters.Add("@PageEnd", EndPage)

            Dim dt As New DataTable
            Dim da As New SqlClient.SqlDataAdapter(sqlCmd)
            da.Fill(dt)

            ' Create a new PdfImageCollection and add your images.
            'Dim col As PdfImageCollection = New PdfImageCollection
            Dim col As New Atalasoft.Imaging.ImageCollection

            For Each dr As DataRow In dt.Rows
                Dim bytImage As Byte()
                bytImage = dr("Image")
                'Dim msR As New MemoryStream(dr("Image"), 0, bytImage.Length)
                'msR.Seek(0, SeekOrigin.Begin)
                'col.Add(New PdfImage(msR, 0, PdfCompressionType.Auto))
                Dim tmpai As New Atalasoft.Imaging.AtalaImage
                tmpai = tmpai.FromByteArray(bytImage)
                If tmpai.ColorDepth <> 1 Then tmpai = tmpai.GetChangedPixelFormat(Atalasoft.Imaging.PixelFormat.Pixel1bppIndexed)

                If IncludeAnnotations = True Then
                    If Not dr("Annotation") Is System.DBNull.Value Then

                        Dim layers As New LayerCollection
                        layers.Add(New LayerAnnotation)
                        layers.Add(New LayerAnnotation)

                        Dim bytAnnotation As Byte() = dr("Annotation")

                        Dim msR As New MemoryStream(bytAnnotation, 0, bytAnnotation.Length)
                        msR.Seek(0, SeekOrigin.Begin)

                        Dim tmpViewer As New Atalasoft.Imaging.WebControls.Annotations.WebAnnotationController
                        tmpViewer.Load(msR, AnnotationDataFormat.Xmp)

                        For Each ann As AnnotationUI In tmpViewer.Layers(0).Items
                            layers(0).Items.Add(ann)
                        Next

                        tmpViewer.Dispose()

                        tmpai = BurnAnnotations(layers, tmpai)

                    End If
                End If

                col.Add(tmpai)
            Next

            Dim tifEncode As New Atalasoft.Imaging.Codec.TiffEncoder(Atalasoft.Imaging.Codec.TiffCompression.Lzw)

            col.Save(Web.Server.MapPath("tmp\" & fileName), tifEncode, Nothing)
            col.Dispose()

            Return fileName

        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Public Function PageCount(ByVal SourceFile As String) As Integer

        If Not Exists(SourceFile) Then Throw New Exception("File not found.")

        Dim ws As Workspace

        Try

            If SourceFile.ToUpper.EndsWith(".PDF") Then
                'ws = New Workspace
                'ws.Open(SourceFile)
                'Return ws.Images.Count
                Dim fs As AtalaFileStream = New AtalaFileStream(SourceFile, FileMode.Open, FileAccess.Read)
                Dim decoder As PdfDecoder = New PdfDecoder
                Dim count As Integer = decoder.GetFrameCount(fs)
                decoder.Dispose()
                fs.Close()
                fs.Dispose()
                Return count
            Else
                ws = New Workspace
                ws.Open(SourceFile)
                Return ws.Images.Count
            End If

        Catch ex As Exception
            Throw ex
        Finally
            If Not IsNothing(ws) Then ws.Dispose()
        End Try

    End Function

    Public Function PadDate(ByVal dt As DateTime) As String

        Dim sbDate As New System.Text.StringBuilder

        Dim strSegments() As String = CStr(dt.Date).Split("/")

        For X As Integer = 0 To strSegments.Length - 1
            If strSegments(X).Length = 1 Then
                sbDate.Append("0" & strSegments(X))
            Else
                sbDate.Append(strSegments(X))
            End If
            sbDate.Append("/")
        Next

        Dim strReturn As String = sbDate.ToString
        If strReturn.EndsWith("/") Then
            strReturn = strReturn.Remove(strReturn.LastIndexOf("/"), 1)
        End If

        Return strReturn

    End Function

    Public Function PadTime(ByVal dt As DateTime) As String

        Dim sbDate As New System.Text.StringBuilder

        Dim strSegments() As String = CStr(dt).Split(":")

        For X As Integer = 0 To strSegments.Length - 1
            If strSegments(X).Length = 1 Then
                sbDate.Append("0" & strSegments(X))
            Else
                sbDate.Append(strSegments(X))
            End If
            sbDate.Append(":")
        Next

        Dim strReturn As String = sbDate.ToString
        If strReturn.EndsWith(":") Then
            strReturn = strReturn.Remove(strReturn.LastIndexOf(":"), 1)
        End If

        Return strReturn

    End Function

    ' Burns the given LayerCollection onto the given image if possible
    ' otherwise, burns the LayerCollection onto a copy of the given image
    Private Function BurnAnnotations(ByVal layers As LayerCollection, ByVal image As AtalaImage) As AtalaImage

        Dim burn As AtalaImage = image

        If Not IsNothing(burn) Then
            ' Can't render annotations on indexed PixelFormats
            If burn.PixelFormat <> PixelFormat.Pixel24bppBgr AndAlso burn.PixelFormat <> PixelFormat.Pixel32bppBgra Then
                Dim cmd As New ChangePixelFormatCommand(PixelFormat.Pixel32bppBgra)
                burn = cmd.Apply(image).Image
            End If

            Dim re As New RenderEnvironment(RenderDevice.Display, burn.GetGraphics(), New PointF(1, 1), AnnotationUnit.Pixel, Nothing, New PointF(CSng(image.Resolution.X), CSng(image.Resolution.Y)))

            For Each layer As LayerAnnotation In layers
                For Each ann As AnnotationUI In layer.Items
                    Dim renderer As IAnnotationRenderer = AnnotationRenderers.[Get](ann.Data.[GetType]())
                    renderer.RenderAnnotation(ann.Data, re)
                Next
            Next

        End If

        Return burn

    End Function

    Public Function ConvertPDFtoTIFF(ByVal SourceFile As String, ByVal OutputFile As String)

        Try

            'initialize licensing
            Dim tmp As AtalaImage = New AtalaImage
            tmp.Dispose()

            Dim pdf As PdfDecoder = New PdfDecoder
            pdf.Resolution = 200

            Dim tick1 As Integer = System.Environment.TickCount
            Dim fsOpen As FileStream = System.IO.File.OpenRead(SourceFile)
            Dim fsSave As AtalaFileStream = New AtalaFileStream(OutputFile, FileMode.Create, FileAccess.Write)

            Dim tiff As New Atalasoft.Imaging.Codec.TiffEncoder(Codec.TiffCompression.Lzw)

            Dim numPages As Integer = pdf.GetFrameCount(fsOpen)
            Dim i As Integer = 0

            Do While i < numPages
                Dim imageRGB As AtalaImage = pdf.Read(fsOpen, i, Nothing)
                imageRGB = imageRGB.GetChangedPixelFormat(PixelFormat.Pixel4bppIndexed)
                imageRGB.Resolution = New Dpi(300, 300, ResolutionUnit.DotsPerInch)
                If i > 0 Then
                    tiff.Append = True
                End If
                fsSave.Seek(0, SeekOrigin.Begin)
                tiff.Save(fsSave, imageRGB, Nothing)
                i += 1
            Loop

            'Dim tick4 As Integer = System.Environment.TickCount
            fsOpen.Close()
            fsSave.Close()

            'Console.WriteLine("Total time " & (tick4 - tick1) & " ms")
            'Console.ReadLine()

        Catch ex As Exception
            Throw (New Exception("Error converting PDF." & vbCr & ex.Message & vbCr & ex.StackTrace))
        End Try

    End Function

    Public Function UpdateCachedResults(ByVal ImageID As Integer, ByVal pg As System.Web.UI.Page)

        Try

            Dim ds As DataSet = pg.Session("DataSource")
            ds.Tables(0).Rows.Remove(ds.Tables(0).Select("ImageID = " & ImageID)(0))
            pg.Session("DataSource") = ds

            pg.Session("ResultCount") = CInt(pg.Session("ResultCount")) - 1

        Catch ex As Exception

        End Try

    End Function

    Public Function UpdateCachedResultsByVersionId(ByVal VersionId As Integer, ByVal pg As System.Web.UI.Page)

        Try

            Dim ds As DataSet = pg.Session("DataSource")
            ds.Tables(0).Rows.Remove(ds.Tables(0).Select("CurrentVersionID = " & VersionId)(0))
            pg.Session("DataSource") = ds

            pg.Session("ResultCount") = CInt(pg.Session("ResultCount")) - 1

        Catch ex As Exception

        End Try

    End Function

    Public Function UpdateCachedAttributes(ByVal ImageId As Integer, ByVal Attributes As DataTable, ByVal Title As String, ByVal Description As String, _
    ByVal Tags As String, ByVal DocumentName As String, ByVal pg As System.Web.UI.Page)

        Try

            Dim ds As DataSet = pg.Session("DataSource")

            For Each row As DataRow In ds.Tables(1).Select("ImageID = " & ImageId)
                ds.Tables(1).Rows.Remove(row)
            Next

            'Add new rows
            For Each newRow As DataRow In Attributes.Rows
                Dim row As DataRow
                row = ds.Tables(1).NewRow
                row("ImageId") = ImageId
                row("AttributeID") = newRow("AttributeID")
                row("AttributeValue") = newRow("AttributeValue")
                row("AttributeName") = newRow("AttributeName")
                row("AttributeDataType") = newRow("AttributeDataType")
                ds.Tables(1).Rows.Add(row)
            Next

            'Update header metadata
            Dim drHeader As DataRow
            drHeader = ds.Tables(0).Select("ImageID = " & ImageId)(0) 'We should never have 2 rows here
            drHeader("DocumentTitle") = IIf(Title.Trim.Length = 0, System.DBNull.Value, Title)
            drHeader("DocumentDescription") = IIf(Description.Trim.Length = 0, System.DBNull.Value, Description)
            drHeader("DocumentTags") = IIf(Tags.Trim.Length = 0, System.DBNull.Value, Tags)
            drHeader("DocumentName") = DocumentName
            'ds.Tables(0).AcceptChanges()

            pg.Session("DataSource") = ds

        Catch ex As Exception

        End Try

    End Function

    Public Function BuildAttributes(ByVal dv As DataView) As String

        If dv.Count = 0 Then Return ""

        Dim sbAttributesTable As New System.Text.StringBuilder

        sbAttributesTable.Append("<TABLE class=ResultDetail>")

        Dim counter As Integer = 0

        Dim intColumns As Integer = 2

        For x As Integer = 0 To dv.Count - 1

            If dv(x)("AttributeValue") <> "" Then

                If counter Mod 3 = 0 Then sbAttributesTable.Append("<tr>")

                For currentAttribute As Integer = x To x + (intColumns - 1)

                    If currentAttribute <= dv.Count - 1 Then

                        Dim AttributeValue As String = dv(currentAttribute)("AttributeValue")
                        Dim AttributeName As String = dv(currentAttribute)("AttributeName")
                        Dim AttributeId As Integer = dv(currentAttribute)("AttributeId")

                        If Not dv(currentAttribute)("AttributeDataType") Is System.DBNull.Value Then

                            Select Case dv(currentAttribute)("AttributeDataType")
                                Case "Date"

                                    Try

                                        If dv(currentAttribute)("AttributeValue").Trim.Length = 6 And IsNumeric(dv(currentAttribute)("AttributeValue").Trim) Then
                                            '6 digits, 2 year date
                                            Dim value, segment1, segment2, segment3 As String
                                            value = dv(currentAttribute)("AttributeValue").Trim
                                            segment1 = value.Substring(0, 2)
                                            segment2 = value.Substring(2, 2)
                                            segment3 = value.Substring(4, 2)

                                            'Y2k year cut off standard
                                            If segment3 < 49 Then
                                                segment3 = "20" & segment3
                                            Else
                                                segment3 = "19" & segment3
                                            End If

                                            AttributeValue = segment1 & "/" & segment2 & "/" & segment3
                                        End If

                                        If dv(currentAttribute)("AttributeValue").Trim.Length = 8 And IsNumeric(dv(currentAttribute)("AttributeValue").Trim) Then
                                            '8 digits, 4 year date
                                            Dim value, segment1, segment2, segment3 As String
                                            value = dv(currentAttribute)("AttributeValue").Trim
                                            segment1 = value.Substring(0, 2)
                                            segment2 = value.Substring(2, 2)
                                            segment3 = value.Substring(4, 4)
                                            AttributeValue = segment1 & "/" & segment2 & "/" & segment3
                                        End If

                                        AttributeValue = PadDate(CDate(AttributeValue).ToShortDateString)

                                    Catch ex As Exception

                                    End Try

                            End Select

                        End If

                        'TODO: " causes script error when inside attribute value

                        'Dim searchAttributeValue As String = AttributeValue

                        'If searchAttributeValue.IndexOf("'") > -1 Or searchAttributeValue.IndexOf("""") > -1 Then
                        '    searchAttributeValue = searchAttributeValue.Replace("'", "\' ")
                        'End If

                        'sbAttributesTable.Append("<TD nowrap onclick=""showSearch(this);"" onmouseout=""hideDrop(this);"" onmouseover=&#34; showDrop(this," & AttributeId & ",'" & searchAttributeValue & "'); &#34;>" & AttributeName & ": " & AttributeValue & "</TD>")
                        'cellId += 1

                        sbAttributesTable.Append("<TD nowrap onclick=""showSearch(this);"" onmouseout=""hideDrop(this);"" onmouseover=""showDrop(this," & AttributeId & ",'" & AttributeValue.Replace("'", "\'").Replace("""", "\""") & "');"">" & AttributeName & ": " & AttributeValue & "</TD>")
                        'sbAttributesTable.Append("<TD nowrap id=t" & cellId & " onmouseover=""showDrop('t" & cellId & "');"">" & AttributeName & ": " & AttributeValue & "</TD>")
                        'cellId += 1

                        x = currentAttribute

                    Else

                        Exit For

                    End If

                Next

                If counter Mod 3 = 0 Then sbAttributesTable.Append("</tr>")

            End If

        Next

        sbAttributesTable.Append("</TABLE>")

        Return sbAttributesTable.ToString

    End Function

    Function ExtractTIFPageFromPDF(ByVal pdfFile As String, ByVal pageNo As Integer, ByRef outputFile As String)

        Dim ai As New AtalaImage(pdfFile, pageNo - 1, Nothing)
        Dim tmpai As AtalaImage

        Dim te As New Codec.TiffEncoder(Codec.TiffCompression.Lzw)

        Dim ms As New MemoryStream(ai.ToByteArray(New PdfEncoder))

        Dim document As New Atalasoft.Imaging.Codec.Pdf.Document(ms)

        If document.Pages(0).SingleImageOnly Then
            Dim extracts As ExtractedImageInfo() = document.Pages(0).ExtractImages()
            tmpai = extracts(0).Image
        Else

        End If

        'Dim pdfDoc As New Atalasoft.PdfDoc.PdfDocument(ms)
        'Dim page As Atalasoft.Imaging.Codec.Pdf.Page

        'If page.SingleImageOnly Then
        '    'only one image per if statement
        'Else
        '    Dim resolution__1 As Integer = Resolution
        '    Dim extracts As ExtractedImageInfo() = page.ExtractImages()
        '    If extracts.Length <> 0 Then
        '        resolution__1 = CInt(extracts(0).Image.Resolution.Y)
        '        'Matches the resolution to the image in an attempt to improve quality
        '    End If
        '    Dim args As New PreRasterEventArgs(resolution__1, page.Height, page.Width, extracts.Length <> 0)
        '    decoder.Resolution = args.Resolution
        '    Return decoder.Read(Stream, frame, Nothing)
        'End If


        'Dim apdf As AdvancedPdfManagement.AdvancedPdfDecoder
        'apdf.


        'pdfdoc.


        'Dim fsis As New FileSystemImageSource(pdfFile, True)
        'ai = fsis.ReAcquire(pageNo)


        tmpai.Save(outputFile, te, Nothing)
        'ai.Dispose()
        'tmpai.Dispose()

    End Function

    Public Function CountOccurrences(ByVal input As String, ByVal searchString As String) As Integer
        Dim r As New Regex(searchString, RegexOptions.IgnoreCase)
        ' Match the regular expression pattern against a text string.
        Dim ma As MatchCollection = r.Matches(input)
        Return ma.Count
        'returns five
    End Function

    Public Function extractAnnotations(ByVal fileName As String) As DataTable

        Dim annotations As New DataTable("Annotations")
        annotations.Columns.Add(New DataColumn("Page", GetType(System.Int32), Nothing, System.Data.MappingType.Element))
        annotations.Columns.Add(New DataColumn("Annotation", GetType(System.Byte()), Nothing, System.Data.MappingType.Element))

        Dim afs As New AtalaFileStream(fileName, FileMode.Open, FileAccess.Read)
        Dim dec As Atalasoft.Imaging.Codec.ImageDecoder = Atalasoft.Imaging.Codec.RegisteredDecoders.GetDecoder(afs)

        If TypeOf dec Is PdfDecoder Then

            'Dim importer As New Atalasoft.Annotate.UI.Importers.PdfAnnotationImporter(afs)

            'Dim layers As New LayerCollection
            'For i As Integer = 0 To importer.PageCount - 1
            '    layers.Add(New Atalasoft.Annotate.UI.LayerAnnotation(importer.Import(i).Data))


            '    Dim dr As DataRow
            '    dr = annotations.NewRow
            '    dr("Page") = pageCount
            '    dr("Annotation") = annobytes
            '    pageCount += 1
            '    annotations.Rows.Add(dr)


            'Next

            'Dim formatter As New XmpFormatter
            ''Dim annoFS As New FileStream(annoFile, FileMode.Create, FileAccess.ReadWrite)
            'formatter.Serialize(annoFS, layers)

            'annoFS.Close()
            'annoFS.Dispose()

            'afs.Position = 0

            'Dim collection As New ImageCollection(afs, Nothing)
            'collection.Save(newFile, New PdfEncoder, Nothing)

        ElseIf TypeOf dec Is Atalasoft.Imaging.Codec.TiffDecoder Then

            Dim tfile As New TiffFile
            tfile.Read(afs)

            Dim annobytes As Byte() = Nothing

            Dim pageCount As Integer = 1
            For Each page As TiffDirectory In tfile.Images

                Dim tag As Atalasoft.Imaging.Metadata.TiffTag = page.Tags.LookupTag(700)

                If Not IsNothing(tag) Then
                    annobytes = DirectCast(tag.Data, Byte())
                End If

                If Not IsNothing(annobytes) Then
                    Dim dr As DataRow
                    dr = annotations.NewRow
                    dr("Page") = pageCount
                    dr("Annotation") = annobytes
                    pageCount += 1
                    annotations.Rows.Add(dr)
                End If

            Next

        End If

        afs.Close()
        afs.Dispose()

        Return annotations

    End Function

    Public Function zeroFillIP(ByVal original As String) As String

        Dim sb As New System.Text.StringBuilder
        Dim sections As String() = original.Split(".")

        Dim count As Integer = 1
        For Each chunk As String In sections
            If count = 4 Then
                sb.Append(chunk.PadLeft(3, "0"))
            Else
                sb.Append(chunk.PadLeft(3, "0") & ".")
            End If
            count += 1
        Next

        Return sb.ToString

    End Function

    Public Function getAccountInfo(ByVal AccountID As String, ByRef AccountDesc As String, ByRef DBServerDNS As String, ByRef DBName As String, ByRef Enabled As Boolean)

        Dim con As New SqlConnection

        Try

            con = Functions.BuildMasterConnection()
            Dim sqlCmd As New SqlCommand("SMAccountDBInfo", con)
            sqlCmd.CommandType = CommandType.StoredProcedure
            sqlCmd.Parameters.Add("@AccountID", AccountID)

            Dim da As New SqlDataAdapter(sqlCmd)
            Dim dt As New DataTable

            da.Fill(dt)

            If dt.Rows.Count = 0 Then
                Throw New Exception("No account found.")
            Else
                AccountDesc = dt.Rows(0)("AccountDesc")
                DBServerDNS = dt.Rows(0)("DBServerDNS")
                DBName = dt.Rows(0)("DBName")
                Enabled = dt.Rows(0)("Enabled")
            End If

        Catch ex As Exception
            Throw ex
        Finally
            If con.State <> ConnectionState.Closed Then
                con.Close()
                con.Dispose()
            End If
        End Try

    End Function

    Public Function BuildConnectionString(ByVal DataSource As String, ByVal InitialCatalog As String, Optional ByVal UserId As String = "", Optional ByVal Password As String = "", Optional ByVal AppName As String = Nothing) As String
        Dim sb As New System.Text.StringBuilder
        sb.Append("Data Source=")
        sb.Append(DataSource)
        sb.Append("; Initial Catalog=")
        sb.Append(InitialCatalog)
        sb.Append("; User Id=")
        sb.Append(UserId)
        sb.Append("; Password=")
        sb.Append(Password)
        sb.Append("; Connect Timeout=120")
        sb.Append("; Application Name=")
        If Not AppName Is Nothing Then
            sb.Append(AppName)
        Else
#If DEBUG Then
            sb.Append("debug")
#Else
            sb.Append("docAssist")
#End If
        End If
        Return sb.ToString
    End Function

    Public Function GetAccountStatus(ByVal AccountID As String) As AccountState

        Dim conMaster As New SqlClient.SqlConnection

        Try

            conMaster = Functions.BuildMasterConnection()

            Dim cmd As New SqlClient.SqlCommand("SMAccountStatus", conMaster)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@AccountID", AccountID)

            Dim res As New SqlClient.SqlParameter("@Result", SqlDbType.Int)
            res.Direction = ParameterDirection.Output
            cmd.Parameters.Add(res)
            cmd.ExecuteNonQuery()
            Return res.Value

        Catch ex As Exception

            Throw New Exception("Error determining account state. (SMAccountStatus) " & ex.Message)

        Finally

            If conMaster.State <> ConnectionState.Closed Then
                conMaster.Close()
                conMaster.Dispose()
            End If

        End Try

    End Function

End Module

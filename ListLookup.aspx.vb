'Imports Accucentric.docAssist.Data.Images

'Partial Class ListLookup
'    Inherits System.Web.UI.Page

'#Region " Web Form Designer Generated Code "

'    'This call is required by the Web Form Designer.
'    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

'    End Sub
'    Protected WithEvents DataGrid1 As System.Web.UI.WebControls.DataGrid
'    Protected WithEvents btnOk As System.Web.UI.WebControls.ImageButton
'    Protected WithEvents dgListItems As System.Web.UI.WebControls.DataGrid

'    'NOTE: The following placeholder declaration is required by the Web Form Designer.
'    'Do not delete or move it.
'    Private designerPlaceholderDeclaration As System.Object

'    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
'        'CODEGEN: This method call is required by the Web Form Designer
'        'Do not modify it using the code editor.
'        InitializeComponent()
'    End Sub

'#End Region

'    Private mobjUser As Accucentric.docAssist.Web.Security.User
'    Private blnHighlight As Boolean = False

'    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

'        Response.Expires = 0
'        Response.Cache.SetNoStore()
'        Response.AppendHeader("Pragma", "no-cache")

'        txtSearchText.Attributes("onkeyup") = "filterList();"

'        hiddenAttributeId.Value = CInt(Request.QueryString("ID"))
'        hiddenDocumentId.Value = CInt(Request.QueryString("DocumentId"))
'        hiddenFolderId.Value = CInt(Request.QueryString("FolderId"))

'        Dim strHostname As String = Request.Url.AbsoluteUri.Substring(0, Request.Url.AbsoluteUri.Length - Request.Url.AbsolutePath.Length - Request.Url.Query.Length)
'#If DEBUG Then
'        strHostname += "/docAssistWeb1"
'#End If
'        ReturnScripts.Add("hostname = '" & strHostname & "';")

'    End Sub

'End Class
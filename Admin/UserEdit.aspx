<%@ Page Language="vb" AutoEventWireup="false" Codebehind="UserEdit.aspx.vb" Inherits="docAssistWeb.UserEdit" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<TITLE>docAssist - User Permissions</TITLE>
		<META http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<META content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<META content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<META content="JavaScript" name="vs_defaultClientScript">
		<META content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../Styles.css" type="text/css" rel="stylesheet">
		<base target="_self">
	</HEAD>
	<BODY id="pageBody" runat="server" onselectstart="document.all.txtEMailAddress.focus();">
		<FORM id="Form1" method="post" runat="server">
			<TABLE class="PageContent" height="100%" cellSpacing="0" cellPadding="0" width="800" align="center"
				border="0" id="tblMain">
				<TBODY>
					<TR>
						<TD height="6" align="center"><IMG height="6" src="images/spacer.gif" width="1"></TD>
					</TR>
					<TR valign="top">
						<TD align="right" valign="top">
							<P><A href="javascript:window.close();"><img src="Images/btn_cancel.gif" border="0"></A>&nbsp;<ASP:LINKBUTTON id="lnkSave1" runat="server"><img src="Images/btn_save.gif" border="0"></ASP:LINKBUTTON></P>
						</TD>
					</TR>
					<TR>
						<TD class="TableHeader" style="BORDER-RIGHT: #d00000 1px solid; BORDER-TOP: #d00000 1px solid; BORDER-LEFT: #d00000 1px solid">
							<P class="TableHeader"><B>User Info</B></P>
						</TD>
					</TR>
					<TR>
						<TD style="BORDER-RIGHT: #d00000 1px solid; BORDER-LEFT: #d00000 1px solid; BORDER-BOTTOM: #d00000 1px solid">
							<ASP:TEXTBOX id="txtWebUserID" VISIBLE="False" Runat="server"></ASP:TEXTBOX>
							<ASP:TEXTBOX id="txtImagesUserId" VISIBLE="False" Runat="server"></ASP:TEXTBOX>
							<asp:label id="lblLockedOut" runat="server" Visible="False" Font-Names="Verdana" Font-Size="8.25pt"
								ForeColor="Red" Font-Bold="False"></asp:label><BR>
							<TABLE id="tableCompanyUser" cellSpacing="0" cellPadding="0" width="100%" bgColor="whitesmoke"
								border="0" runat="server">
								<TR>
									<TD class="transparent" align="right" width="100%">
										<P align="right"><ASP:LABEL id="lblCompanyUser" Runat="server">Select Existing Company User:</ASP:LABEL>&nbsp;<ASP:DROPDOWNLIST id="ddlCompanyUsers" Runat="server" AutoPostBack="True" Width="144px"></ASP:DROPDOWNLIST></P>
									</TD>
								</TR>
							</TABLE>
							<TABLE class="PageContent" cellSpacing="0" cellPadding="1" width="100%" border="0" id="tblUser">
								<TBODY>
									<TR class="TableAlternateItem">
										<TD style="WIDTH: 145px" class="TableAlternateItem">E-Mail Address:&nbsp;
										</TD>
										<TD><ASP:TEXTBOX id="txtEMailAddress" Runat="server" Width="250px" CSSCLASS="InputBox" MAXLENGTH="100"
												Visible="True" TabIndex="1"></ASP:TEXTBOX>&nbsp;<ASP:LABEL id="lblEMailWarning" runat="server" CssClass="red" Visible="False"></ASP:LABEL></TD>
									</TR>
									<TR>
										<TD style="WIDTH: 145px">Password:&nbsp;
										</TD>
										<TD><ASP:TEXTBOX id="txtPassword" Runat="server" CSSCLASS="InputBox" MAXLENGTH="12" TEXTMODE="Password"
												WIDTH="250px" TabIndex="2"></ASP:TEXTBOX></TD>
									</TR>
									<TR class="TableAlternateItem">
										<TD style="WIDTH: 145px" vAlign="top" align="left"><IMG height="16" src="images/spacer.gif" width="1">Confirm:</TD>
										<TD><ASP:TEXTBOX id="txtPasswordConfirm" Runat="server" CSSCLASS="InputBox" WIDTH="250px" TextMode="Password"
												MaxLength="12" TabIndex="3"></ASP:TEXTBOX>&nbsp;
											<ASP:LABEL id="lblPasswordWarning" runat="server" Visible="False" CssClass="red"></ASP:LABEL><BR>
											<ASP:CHECKBOX id="chkChangePassword" Runat="server" Text="Force password change at next logon"
												TabIndex="4"></ASP:CHECKBOX></TD>
									</TR>
									<TR>
										<TD style="WIDTH: 145px">First Name:&nbsp;</TD>
										<TD><ASP:TEXTBOX id="txtFirstName" Runat="server" CSSCLASS="InputBox" MAXLENGTH="30" WIDTH="250px"
												TabIndex="5"></ASP:TEXTBOX>&nbsp;<ASP:LABEL id="lblFirstNameWarning" runat="server" CssClass="red" Visible="False"></ASP:LABEL></TD>
									</TR>
									<TR class="TableAlternateItem">
										<TD style="WIDTH: 145px; HEIGHT: 27px">Last Name:&nbsp;</TD>
										<TD style="HEIGHT: 27px"><ASP:TEXTBOX id="txtLastName" Runat="server" CSSCLASS="InputBox" MAXLENGTH="30" WIDTH="250px"
												TabIndex="6"></ASP:TEXTBOX>&nbsp;
											<ASP:LABEL id="lblLastNameWarning" runat="server" CssClass="red" Visible="False"></ASP:LABEL></TD>
									</TR>
									<TR>
										<TD style="WIDTH: 145px">Contact No:&nbsp;
										</TD>
										<TD vAlign="top"><ASP:TEXTBOX id="txtContactNo" Runat="server" CSSCLASS="InputBox" WIDTH="150px" TabIndex="7"></ASP:TEXTBOX>&nbsp;<ASP:LABEL id="lblContactNo" runat="server" CssClass="red" Visible="False"></ASP:LABEL></TD>
									</TR>
									<TR class="TableAlternateItem">
										<TD style="WIDTH: 145px">System Admin:</TD>
										<TD><ASP:CHECKBOX id="chkSystemAdmin" Runat="server" tabIndex="8"></ASP:CHECKBOX></TD>
									</TR>
									<TR>
										<TD style="WIDTH: 145px">Security Admin:</TD>
										<TD><ASP:CHECKBOX id="chkSecurityAdmin" Runat="server" tabIndex="9"></ASP:CHECKBOX></TD>
									</TR>
									<TR class="TableAlternateItem">
										<TD style="WIDTH: 145px">SmartFolders Admin:</TD>
										<TD><ASP:CHECKBOX id="chkSmartFolders" Runat="server" tabIndex="9"></ASP:CHECKBOX></TD>
									</TR>
									<TR>
										<TD style="WIDTH: 145px">Template Admin:</TD>
										<TD>
											<ASP:CHECKBOX id="chkTemplateAdmin" tabIndex="9" Runat="server"></ASP:CHECKBOX></TD>
									</TR>
									<TR class="TableAlternateItem">
										<TD style="WIDTH: 145px">User Type:</TD>
										<TD>
											<asp:DropDownList id="ddlUserType" runat="server" Width="150px" TabIndex="10">
												<asp:ListItem Value="0">Full User</asp:ListItem>
												<asp:ListItem Value="2">View Only</asp:ListItem>
											</asp:DropDownList></TD>
									</TR>
									<TR>
										<TD style="WIDTH: 145px">Enabled:</TD>
										<TD><ASP:CHECKBOX id="chkUserEnabled" Runat="server" Checked="True" TabIndex="11"></ASP:CHECKBOX></TD>
									</TR>
									<tr style="DISPLAY: none" class="TableAlternateItem">
										<td>Workflow Only:&nbsp;</td>
										<td><asp:checkbox id="chkWFOnlyUser" runat="server" Visible="False" TabIndex="12"></asp:checkbox></td>
									</tr>
								</TBODY>
							</TABLE>
						</TD>
					</TR>
					<TR id="DocumentHeader" runat="server">
						<TD class="TableHeader" style="BORDER-RIGHT: #d00000 1px solid; BORDER-TOP: #d00000 1px solid; BORDER-LEFT: #d00000 1px solid">
							<P class="TableHeader"><B id="B1" runat="server">Document Privileges </B>
							</P>
						</TD>
					</TR>
					<TR id="GridRow" runat="server">
						<TD style="BORDER-RIGHT: #d00000 1px solid; BORDER-LEFT: #d00000 1px solid; BORDER-BOTTOM: #d00000 1px solid"><ASP:LABEL id="lblDocumentWarning" Runat="server" CssClass="Red" Visible="False"></ASP:LABEL>
							<img src="Images/spacer.GIF" height="1" width="1" style="DISPLAY: none" id="SpacerImages"
								runat="server">
							<asp:Label id="lblSystemAdmin" runat="server" Visible="True" style="VISIBILITY: hidden" Font-Name="Verdana"
								Font-Size="9">System administrator accounts have full access to all documents. If this user should not have full access to all documents uncheck the 'System Admin' checkbox.</asp:Label>
							<DIV id="divDocumentRights" style="OVERFLOW: auto; WIDTH: 100%; HEIGHT: 180px" runat="server">
								<!--
								<ASP:DATAGRID id="dgDocumentRights" runat="server" CssClass="PageContent" AutoGenerateColumns="False">
									<ALTERNATINGITEMSTYLE CSSCLASS="TableAlternateItem"></ALTERNATINGITEMSTYLE>
									<HEADERSTYLE FONT-BOLD="True"></HEADERSTYLE>
									<COLUMNS>
										<ASP:TEMPLATECOLUMN HeaderText="Document Name">
											<HEADERSTYLE WIDTH="136px"></HEADERSTYLE>
									
										<ITEMTEMPLATE>
											<asp:Label id=Label1 runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DocumentName") %>'>
											</ASP:LABEL>
											<asp:TextBox id=txtSecValue runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Sec_Value") %>' Visible="False">
											</ASP:TEXTBOX>
										</ITEMTEMPLATE>
									</ASP:TEMPLATECOLUMN>
									<ASP:TEMPLATECOLUMN HeaderText="No Access">
										<HEADERSTYLE HORIZONTALALIGN="Center" WIDTH="72px"></HEADERSTYLE>
										<ITEMSTYLE HORIZONTALALIGN="Center"></ITEMSTYLE>
										<ITEMTEMPLATE>
											<INPUT ID='<%# "rdoSecLevel" + CStr(DataBinder.Eval(Container, "DataItem.sec_value")) + "0" %>' TYPE=radio VALUE=0 NAME='<%# "rdoSecLevel" + CStr(DataBinder.Eval(Container, "DataItem.sec_value")) %>' <%# DataBinder.Eval(Container, "DataItem.NoAccess") %>>
										</ITEMTEMPLATE>
									</ASP:TEMPLATECOLUMN>
									<ASP:TEMPLATECOLUMN HeaderText="Read Only">
										<HEADERSTYLE HORIZONTALALIGN="Center" WIDTH="72px"></HEADERSTYLE>
										<ITEMSTYLE HORIZONTALALIGN="Center"></ITEMSTYLE>
										<ITEMTEMPLATE>
											<INPUT TYPE="radio" ID='<%# "rdoSecLevel" + CStr(DataBinder.Eval(Container, "DataItem.sec_value")) + "1" %>' NAME='<%# "rdoSecLevel" + CStr(DataBinder.Eval(Container, "DataItem.sec_value")) %>' VALUE="1" <%# DataBinder.Eval(Container, "DataItem.ReadOnly") %>>
										</ITEMTEMPLATE>
									</ASP:TEMPLATECOLUMN>
									<ASP:TEMPLATECOLUMN HeaderText="Change">
										<HEADERSTYLE HORIZONTALALIGN="Center" WIDTH="72px"></HEADERSTYLE>
										<ITEMSTYLE HORIZONTALALIGN="Center"></ITEMSTYLE>
										<ITEMTEMPLATE>
											<INPUT TYPE="radio" ID='<%# "rdoSecLevel" + CStr(DataBinder.Eval(Container, "DataItem.sec_value")) + "2" %>' NAME='<%# "rdoSecLevel" + CStr(DataBinder.Eval(Container, "DataItem.sec_value")) %>' VALUE="2" <%# DataBinder.Eval(Container, "DataItem.Change") %>>
										</ITEMTEMPLATE>
									</ASP:TEMPLATECOLUMN>
									<ASP:TEMPLATECOLUMN HeaderText="Delete">
										<HEADERSTYLE HORIZONTALALIGN="Center" WIDTH="72px"></HEADERSTYLE>
										<ITEMSTYLE HORIZONTALALIGN="Center"></ITEMSTYLE>
										<ITEMTEMPLATE>
											<INPUT TYPE="radio" ID='<%# "rdoSecLevel" + CStr(DataBinder.Eval(Container, "DataItem.sec_value")) + "3" %>' NAME='<%# "rdoSecLevel" + CStr(DataBinder.Eval(Container, "DataItem.sec_value")) %>' VALUE="3" <%# DataBinder.Eval(Container, "DataItem.Delete") %>>
										</ITEMTEMPLATE>
									</ASP:TEMPLATECOLUMN>
									<ASP:TEMPLATECOLUMN HeaderText="Creator Only">
										<HEADERSTYLE HORIZONTALALIGN="Center" WIDTH="72px"></HEADERSTYLE>
										<ITEMSTYLE HORIZONTALALIGN="Center"></ITEMSTYLE>
										<ITEMTEMPLATE>
											<ASP:CHECKBOX id="chkCreatorOnly" runat="server" Checked='<%# DataBinder.Eval(Container, "DataItem.CreatorOnly") %>'>
											</ASP:CHECKBOX>
										</ITEMTEMPLATE>
									</ASP:TEMPLATECOLUMN>
									<ASP:TEMPLATECOLUMN HeaderText="Current Version Only">
										<HEADERSTYLE HORIZONTALALIGN="Center" WIDTH="72px"></HEADERSTYLE>
										<ITEMSTYLE HORIZONTALALIGN="Center"></ITEMSTYLE>
										<ITEMTEMPLATE>
											<ASP:CHECKBOX id="chkCurrentVersionOnly" runat="server" Checked='<%# DataBinder.Eval(Container, "DataItem.VersionOnly") %>'>
											</ASP:CHECKBOX>
										</ITEMTEMPLATE>
									</ASP:TEMPLATECOLUMN>
									<ASP:TEMPLATECOLUMN HeaderText="Print">
										<HEADERSTYLE HORIZONTALALIGN="Center" WIDTH="72px"></HEADERSTYLE>
										<ITEMSTYLE HORIZONTALALIGN="Center"></ITEMSTYLE>
										<ITEMTEMPLATE>
											<ASP:CHECKBOX id="chkPrint" runat="server" Checked='<%# DataBinder.Eval(Container, "DataItem.Print") %>'>
											</ASP:CHECKBOX>
										</ITEMTEMPLATE>
									</ASP:TEMPLATECOLUMN>
									<ASP:TEMPLATECOLUMN HeaderText="E-Mail / Export">
										<HEADERSTYLE HORIZONTALALIGN="Center" WIDTH="72px"></HEADERSTYLE>
										<ITEMSTYLE HORIZONTALALIGN="Center"></ITEMSTYLE>
										<ITEMTEMPLATE>
											<asp:CheckBox id=chkExport runat="server" Checked='<%# DataBinder.Eval(Container, "DataItem.Export") %>'>
											</ASP:CHECKBOX>
										</ITEMTEMPLATE>
									</ASP:TEMPLATECOLUMN>
									<ASP:TEMPLATECOLUMN HeaderText="Manage Annotations">
										<HEADERSTYLE HORIZONTALALIGN="Center" WIDTH="72px"></HEADERSTYLE>
										<ITEMSTYLE HORIZONTALALIGN="Center"></ITEMSTYLE>
										<ITEMTEMPLATE>
											<asp:CheckBox id="chkAnnotation" runat="server" Checked='<%# DataBinder.Eval(Container, "DataItem.Annotate") %>'>
											</ASP:CHECKBOX>
										</ITEMTEMPLATE>
									</ASP:TEMPLATECOLUMN>
									<ASP:TEMPLATECOLUMN HeaderText="Manage Redactions">
										<HEADERSTYLE HORIZONTALALIGN="Center" WIDTH="72px"></HEADERSTYLE>
										<ITEMSTYLE HORIZONTALALIGN="Center"></ITEMSTYLE>
										<ITEMTEMPLATE>
											<asp:CheckBox id="chkRedaction" runat="server" Checked='<%# DataBinder.Eval(Container, "DataItem.Redact") %>'>
											</ASP:CHECKBOX>
										</ITEMTEMPLATE>
									</ASP:TEMPLATECOLUMN>
								</COLUMNS>
								</ASP:DATAGRID>
								-->
							</DIV>
						</TD>
					</TR>
					<TR>
						<TD align="right">
							<asp:Literal id="litAccessDenied" runat="server"></asp:Literal><A href="javascript:window.close();"><img src="Images/btn_cancel.gif" border="0"></A>&nbsp;<ASP:LINKBUTTON id="lnkSave2" runat="server"><img src="Images/btn_save.gif" border="0"></ASP:LINKBUTTON></TD>
					</TR>
					<TR>
						<TD><IMG height="4" src="spacer.gif" width="1"></TD>
					</TR>
				</TBODY>
			</TABLE>
			<ASP:DROPDOWNLIST id="ddlTimeZone" Runat="server" Width="100%" CssClass="InputBox" Visible="False"></ASP:DROPDOWNLIST><ASP:LITERAL id="litScript" Runat="server"></ASP:LITERAL></FORM>
		<script language="javascript">
			function SystemAdmin()
			{
				var page;
				page = document.all;
					if (page.chkSystemAdmin.checked)
					{
						page.dgDocumentRights.style.visibility = 'hidden';
						page.dgDocumentRights.style.display = 'none';
						page.lblSystemAdmin.style.visibility = 'visible';
					}
					else
					{
						page.dgDocumentRights.style.visibility = 'visible';
						page.dgDocumentRights.style.display = 'block';
						page.lblSystemAdmin.style.visibility = 'hidden';
					}
			}
		</script>
	</BODY>
</HTML>

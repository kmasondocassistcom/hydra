<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="../Controls/MenuBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="docHeader" Src="../Controls/docHeader.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="GroupAdmin.aspx.vb" Inherits="docAssistWeb.Groups" %>
<%@ Register TagPrefix="uc1" TagName="TopRight" Src="../Controls/TopRight.ascx" %>
<%@ Register TagPrefix="uc1" TagName="AdminNav" Src="../Controls/AdminNav.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<TITLE>docAssist - Groups</TITLE>
		<META content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<META content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<META content="JavaScript" name="vs_defaultClientScript">
		<META content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<META http-equiv="cache-control" content="no-cache">
		<LINK href="../Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<BODY>
		<FORM id="Form1" method="post" runat="server">
			<TABLE height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD width="100%" colSpan="6">
						<uc1:docHeader id="DocHeader1" runat="server"></uc1:docHeader></TD>
					<TD width="100%"></TD>
				</TR>
				<TR vAlign="middle" height="100%">
					<TD class="Navigation1" style="BORDER-RIGHT: #cccccc thin solid; WIDTH: 171px" vAlign="top"
						height="100%"><UC1:ADMINNAV id="AdminNav1" runat="server"></UC1:ADMINNAV></TD>
					<td><img src="spacer.gif" height="1" width="25"></td>
					<TD vAlign="top" width="100%"><BR>
						<table id="tblGroupsHeader" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<TR align="right">
								<td class="PageContent" align="left" width="100%">
									<A class="PageContent" style="TEXT-ALIGN: right" href="javascript:AddGroup();">Add 
										Group...</A>
								</td>
							</TR>
						</table>
						<ASP:DATAGRID id="dgGroups" runat="server" AutoGenerateColumns="False" CSSCLASS="PageContent"
							Width="100%">
							<SelectedItemStyle Height="20px"></SelectedItemStyle>
							<AlternatingItemStyle CssClass="TableAlternateItem" BackColor="#E0E0E0"></AlternatingItemStyle>
							<ItemStyle Height="20px"></ItemStyle>
							<HeaderStyle Font-Bold="True" Height="20px" CssClass="TableHeader"></HeaderStyle>
							<Columns>
								<asp:TemplateColumn>
									<HeaderStyle Width="60px"></HeaderStyle>
									<ItemTemplate>
										<asp:Label id=lblGroupId runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.GroupId") %>'>
										</asp:Label>
										<asp:Label id=lblGroupAllowDelete runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.AllowDelete") %>'>
										</asp:Label>&nbsp;
										<asp:HyperLink id=lnkEditGroup runat="server" ToolTip="Edit Group" NavigateUrl='<%# "javascript:EditGroup(" + CStr(DataBinder.Eval(Container, "DataItem.GroupId")) + ");" %>'>
											<img src="../Images/editIcon.gif" border="0"></asp:HyperLink><IMG height="1" src="spacer.gif" width="5">
										<asp:LinkButton id="lnkDeleteGroup" runat="server" ToolTip="Remove Group" CommandName="Delete">
											<img src="../Images/trash.gif" border="0"></asp:LinkButton><IMG height="1" src="spacer.gif" width="10">
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:BoundColumn DataField="GroupName" HeaderText="Category"></asp:BoundColumn>
							</Columns>
						</ASP:DATAGRID>
						<asp:Label id="lblNoGroups" runat="server" Width="100%" Visible="False"></asp:Label>
					</TD>
					<TD><img src="spacer.gif" height="1" width="25"></TD>
				<tr>
					<td colSpan="5"><uc1:topright id="docFooter" runat="server"></uc1:topright></td>
					<td></td>
				</tr>
			</TABLE>
		</FORM>
		<script language="javascript">
			
			function AddGroup()
			{
				var nXpos = (document.body.clientWidth - 400) / 2; 
				var nYpos = (document.body.clientHeight - 200) / 2;
				//window.open('WorkflowStatus.aspx?ID=0'+'&Mode=0','AddStatus','height=200,width=400,resizable=yes,status=no,toolbar=no,top='+nYpos+',left='+nXpos)
				// show modal dialogs
				alert('add group');
			}

			function EditGroup()
			{
				var nXpos = (document.body.clientWidth - 400) / 2; 
				var nYpos = (document.body.clientHeight - 200) / 2;
				//window.open('WorkflowStatus.aspx?ID=0'+'&Mode=0','AddStatus','height=200,width=400,resizable=yes,status=no,toolbar=no,top='+nYpos+',left='+nXpos)
				alert('edit group');
			}

			function GroupDeleteError()
			{
				alert('Cannot delete this group.');
			}

		</script>
	</BODY>
</HTML>

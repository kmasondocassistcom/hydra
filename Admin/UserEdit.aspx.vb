Imports Accucentric.docAssist
'TODO: Set focus to emailaddress field when form loads
Public Class UserEdit
    Inherits System.Web.UI.Page
    Private guidUserId As System.Guid
    Private mobjUser As Accucentric.docAssist.Web.Security.User
    Protected WithEvents txtEMailAddress As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtPassword As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtFirstName As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtLastName As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtPasswordConfirm As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtContactNo As System.Web.UI.WebControls.TextBox
    Protected WithEvents dgDocumentRights As System.Web.UI.WebControls.DataGrid
    Protected WithEvents lnkSave1 As System.Web.UI.WebControls.LinkButton
    Protected WithEvents lnkSave2 As System.Web.UI.WebControls.LinkButton
    Private mconSqlImage As SqlClient.SqlConnection
    Protected WithEvents txtWebUserID As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtImagesUserId As System.Web.UI.WebControls.TextBox
    Protected WithEvents litScript As System.Web.UI.WebControls.Literal
    Protected WithEvents lblCompanyUser As System.Web.UI.WebControls.Label
    Protected WithEvents ddlCompanyUsers As System.Web.UI.WebControls.DropDownList
    Protected WithEvents tableCompanyUser As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents lblEMailWarning As System.Web.UI.WebControls.Label
    Protected WithEvents lblPasswordWarning As System.Web.UI.WebControls.Label
    Protected WithEvents lblFirstNameWarning As System.Web.UI.WebControls.Label
    Protected WithEvents lblLastNameWarning As System.Web.UI.WebControls.Label
    Protected WithEvents lblContactNo As System.Web.UI.WebControls.Label
    Protected WithEvents ddlTimeZone As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblDocumentWarning As System.Web.UI.WebControls.Label
    Protected WithEvents pageBody As HtmlContainerControl
    Protected WithEvents chkUserEnabled As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkWFOnlyUser As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkChangePassword As System.Web.UI.WebControls.CheckBox
    Protected WithEvents lblLockedOut As System.Web.UI.WebControls.Label
    Protected WithEvents chkSecurityAdmin As System.Web.UI.WebControls.CheckBox
    Protected WithEvents ddlUserType As System.Web.UI.WebControls.DropDownList
    Protected WithEvents chkSystemAdmin As System.Web.UI.WebControls.CheckBox
    Protected WithEvents litAccessDenied As System.Web.UI.WebControls.Literal
    Protected WithEvents DocumentHeader As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents GridRow As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents divDocumentRights As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents B1 As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents lblSystemAdmin As System.Web.UI.WebControls.Label
    Protected WithEvents AdminRow As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents SpacerImages As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents chkSmartFolders As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkTemplateAdmin As System.Web.UI.WebControls.CheckBox


    Private mconSqlWeb As SqlClient.SqlConnection

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Function AddUser() As Boolean
        Dim bolReturn As Boolean = False
        Dim cmdSql As SqlClient.SqlCommand
        Dim trnWeb As SqlClient.SqlTransaction
        Dim trnImage As SqlClient.SqlTransaction

        Try
            If Not Me.mconSqlWeb.State = ConnectionState.Open Then Me.mconSqlWeb.Open()

            'Check to see if user exists in the master db

            Dim cmdSqlCheck As New SqlClient.SqlCommand("SELECT UserID FROM Users WHERE EmailAddress = @EmailAddress ", Me.mconSqlWeb)
            cmdSqlCheck.CommandTimeout = DEFAULT_COMMAND_TIMEOUT
            cmdSqlCheck.Parameters.Add("@EmailAddress", txtEMailAddress.Text)
            Dim strExistingID As System.Guid = cmdSqlCheck.ExecuteScalar


            If strExistingID.ToString = "00000000-0000-0000-0000-000000000000" Then
                guidUserId = System.Guid.NewGuid()
            Else
                guidUserId = strExistingID
            End If

            txtWebUserID.Text = guidUserId.ToString

            trnWeb = Me.mconSqlWeb.BeginTransaction()
            If strExistingID.ToString = "00000000-0000-0000-0000-000000000000" Then

                '
                ' Add the user to the docAssist web user table
                '
                Dim objEnc As New Encryption.Encryption
                cmdSql = New SqlClient.SqlCommand("INSERT INTO Users (UserId, CompanyId, EMailAddress, LastName, FirstName, Password, ContactNo, Enabled, FailedLogin, PasswordExpires, ViewerPopup, AccountLocked) VALUES (@UserId, @CompanyId, @EMailAddress, @LastName, @FirstName, @Password, @ContactNo, @Enabled, 0, @PasswordExpires, 0, 0)", Me.mconSqlWeb)
                cmdSql.CommandTimeout = DEFAULT_COMMAND_TIMEOUT
                cmdSql.Parameters.Add("@UserId", guidUserId)
                cmdSql.Parameters.Add("@CompanyId", Me.mobjUser.CompanyId)
                cmdSql.Parameters.Add("@EMailAddress", txtEMailAddress.Text.Trim)
                cmdSql.Parameters.Add("@LastName", txtLastName.Text.Trim)
                cmdSql.Parameters.Add("@FirstName", txtFirstName.Text.Trim)
                cmdSql.Parameters.Add("@Password", objEnc.Encrypt(txtPassword.Text.Trim))
                cmdSql.Parameters.Add("@Enabled", chkUserEnabled.Checked)
                'cmdSql.Parameters.Add("@DefaultTimeZoneId", ddlTimeZone.SelectedValue)
                If txtContactNo.Text.Trim.Length = 0 Then
                    cmdSql.Parameters.Add("@ContactNo", System.DBNull.Value)
                Else
                    cmdSql.Parameters.Add("@ContactNo", txtContactNo.Text.Trim.Replace("(", "").Replace(")", "").Replace(" ", "").Replace("-", "").Substring(0, 10))
                End If

                If chkChangePassword.Checked = True Then
                    cmdSql.Parameters.Add("@PasswordExpires", Now.Date.AddDays(-1))
                Else
                    cmdSql.Parameters.Add("@PasswordExpires", Now.Date.AddDays(90))
                End If

                cmdSql.Transaction = trnWeb
                cmdSql.ExecuteScalar()

            End If

            ' Track the change
            ' 
            LogUserAccounts(guidUserId, Me.mobjUser.AccountId, Me.mobjUser.UserId, _
                "Add", Me.mconSqlWeb, trnWeb)
            '
            ' Add the user to the docAssist web account table
            '
            cmdSql = New SqlClient.SqlCommand("INSERT INTO UserAccounts (UserId, AccountId, DefaultAccount) VALUES (@UserId, @AccountId, 1)", Me.mconSqlWeb)
            cmdSql.CommandTimeout = DEFAULT_COMMAND_TIMEOUT
            cmdSql.Parameters.Add("@UserId", guidUserId)
            cmdSql.Parameters.Add("@AccountId", Me.mobjUser.AccountId)
            cmdSql.Transaction = trnWeb
            cmdSql.ExecuteNonQuery()
            '
            ' Check to see if the user already exists in the images users table
            '
            cmdSql = New SqlClient.SqlCommand("SELECT UserId FROM viewWebUsers WITH (NOLOCK)  WHERE WebUserId = @WebUserId", Me.mconSqlImage)
            cmdSql.CommandTimeout = DEFAULT_COMMAND_TIMEOUT
            cmdSql.Parameters.Add("@WebUserId", guidUserId)

            If Not Me.mconSqlImage.State = ConnectionState.Open Then Me.mconSqlImage.Open()

            Dim dr As SqlClient.SqlDataReader = cmdSql.ExecuteReader(CommandBehavior.SingleRow)
            Dim intImageUserId As Integer
            If dr.Read Then
                If Not dr(0) Is System.DBNull.Value Then intImageUserId = dr(0) Else intImageUserId = 0
            End If
            dr.Close()

            trnImage = Me.mconSqlImage.BeginTransaction()

            If intImageUserId = 0 Then
                '
                ' Add the user to the Account Images user table
                '
                cmdSql = New SqlClient.SqlCommand("acsUsersInsert", Me.mconSqlImage)
                cmdSql.CommandTimeout = DEFAULT_COMMAND_TIMEOUT
                cmdSql.CommandType = CommandType.StoredProcedure
                cmdSql.Parameters.Add("@UserId", -1)
                cmdSql.Parameters("@UserId").Direction = ParameterDirection.Output
                cmdSql.Parameters.Add("@UserName", guidUserId.ToString())
                cmdSql.Parameters.Add("@SysAdmin", SqlDbType.Bit)
                cmdSql.Parameters("@SysAdmin").Value = chkSystemAdmin.Checked
                cmdSql.Parameters.Add("@SecAdmin", SqlDbType.Bit)
                cmdSql.Parameters("@SecAdmin").Value = chkSecurityAdmin.Checked
                cmdSql.Parameters.Add("@RenditionAdmin", "0")
                cmdSql.Parameters.Add("@SFAdmin", chkSmartFolders.Checked)
                cmdSql.Parameters.Add("@BTAdmin", chkTemplateAdmin.Checked)

                cmdSql.Transaction = trnImage
                cmdSql.ExecuteNonQuery()

                intImageUserId = cmdSql.Parameters("@UserId").Value

                '
                ' Add the user priviledges to the Account UserSec table
                '
                Dim dtUserSec As New Data.Images.UserSec
                For Each dgItem As DataGridItem In dgDocumentRights.Items
                    Dim txtSecValue As TextBox = CType(dgItem.FindControl("txtSecValue"), TextBox)
                    Dim chkExported As CheckBox = CType(dgItem.FindControl("chkExport"), CheckBox)
                    Dim chkPrint As CheckBox = CType(dgItem.FindControl("chkPrint"), CheckBox)
                    Dim chkAnnotation As CheckBox = CType(dgItem.FindControl("chkAnnotation"), CheckBox)
                    Dim chkRedaction As CheckBox = CType(dgItem.FindControl("chkRedaction"), CheckBox)
                    Dim chkCreatorOnly As CheckBox = CType(dgItem.FindControl("chkCreatorOnly"), CheckBox)
                    Dim chkCurrentVersionOnly As CheckBox = CType(dgItem.FindControl("chkCurrentVersionOnly"), CheckBox)


                    '
                    ' Check for value
                    Dim intSecLevel As Integer = Request.Form("rdoSecLevel" & txtSecValue.Text) '0      ' Default to no access
                    Dim drUserSec As Data.Images.UserSecRow
                    If intSecLevel <> 0 Then
                        drUserSec = dtUserSec.NewRow
                        drUserSec.SecValue = CInt(txtSecValue.Text)
                        drUserSec.SecType = Data.Images.SecType.Document
                        drUserSec.UserId = intImageUserId
                        drUserSec.SecLevel = intSecLevel
                        dtUserSec.Rows.Add(drUserSec)
                    End If
                    '
                    ' Check for export
                    If chkExported.Checked Then
                        drUserSec = dtUserSec.NewRow
                        drUserSec.SecType = Data.Images.SecType.Export
                        drUserSec.SecValue = CInt(txtSecValue.Text)
                        drUserSec.UserId = intImageUserId
                        drUserSec.SecLevel = chkExported.Checked
                        dtUserSec.Rows.Add(drUserSec)
                    End If
                    '
                    ' Check for print
                    If chkPrint.Checked Then
                        drUserSec = dtUserSec.NewRow
                        drUserSec.SecType = Data.Images.SecType.Print
                        drUserSec.SecValue = CInt(txtSecValue.Text)
                        drUserSec.SecLevel = chkPrint.Checked
                        drUserSec.UserId = intImageUserId
                        dtUserSec.Rows.Add(drUserSec)
                    End If
                    'Annotation
                    If chkAnnotation.Checked Then
                        drUserSec = dtUserSec.NewRow
                        drUserSec.SecType = Data.Images.SecType.Annotation
                        drUserSec.SecValue = CInt(txtSecValue.Text)
                        If chkAnnotation.Checked Then
                            drUserSec.SecLevel = 1
                        Else
                            drUserSec.SecLevel = 0
                        End If
                        drUserSec.UserId = intImageUserId
                        dtUserSec.Rows.Add(drUserSec)
                    End If
                    'Redaction
                    If chkRedaction.Checked Then
                        drUserSec = dtUserSec.NewRow
                        drUserSec.SecType = Data.Images.SecType.Redaction
                        drUserSec.SecValue = CInt(txtSecValue.Text)
                        If chkRedaction.Checked Then
                            drUserSec.SecLevel = 1
                        Else
                            drUserSec.SecLevel = 0
                        End If
                        drUserSec.UserId = intImageUserId
                        dtUserSec.Rows.Add(drUserSec)
                    End If
                    'Creator Only
                    If chkCreatorOnly.Checked Then
                        drUserSec = dtUserSec.NewRow
                        drUserSec.SecType = Data.Images.SecType.CreatorOnly
                        drUserSec.SecValue = CInt(txtSecValue.Text)
                        If chkCreatorOnly.Checked Then
                            drUserSec.SecLevel = 1
                        Else
                            drUserSec.SecLevel = 0
                        End If
                        drUserSec.UserId = intImageUserId
                        dtUserSec.Rows.Add(drUserSec)
                    End If
                    'Current Version Only
                    If chkCurrentVersionOnly.Checked Then
                        drUserSec = dtUserSec.NewRow
                        drUserSec.SecType = Data.Images.SecType.CurrentVersionOnly
                        drUserSec.SecValue = CInt(txtSecValue.Text)
                        If chkCurrentVersionOnly.Checked Then
                            drUserSec.SecLevel = 1
                        Else
                            drUserSec.SecLevel = 0
                        End If
                        drUserSec.UserId = intImageUserId
                        dtUserSec.Rows.Add(drUserSec)
                    End If
                Next

                If dtUserSec.Rows.Count > 0 Then
                    dtUserSec.Update(Me.mconSqlImage, trnImage)
                End If
                '
                ' Commit the transactions
                '
                If Not trnWeb Is Nothing Then trnWeb.Commit()
                If Not trnImage Is Nothing Then trnImage.Commit()
            Else
                '
                ' Commit the transactions
                '
                If Not trnWeb Is Nothing Then trnWeb.Commit()
                If Not trnImage Is Nothing Then trnImage.Commit()
                '
                ' Call the update user for the rest
                txtImagesUserId.Text = intImageUserId.ToString()
                UpdateUser()
            End If

            bolReturn = True

        Catch ex As Exception
#If DEBUG Then
            Throw ex
#End If
            If Not trnWeb Is Nothing Then trnWeb.Rollback()
            If Not trnImage Is Nothing Then trnImage.Rollback()

        Finally
            If Not Me.mconSqlWeb.State = ConnectionState.Closed Then Me.mconSqlWeb.Close()
            If Not Me.mconSqlImage.State = ConnectionState.Closed Then Me.mconSqlImage.Close()
        End Try

        Return bolReturn

    End Function

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        txtPassword.Attributes("onkeypress") = "document.all.chkChangePassword.checked = true;document.all.txtPasswordConfirm.value='';"
        'RegisterStartupScript("focus", "<script language='javascript'>if (document.all.txtEMailAddress) {document.all.txtEMailAddress.focus();}</script>")
        'RegisterStartupScript("focus", "<script language='javascript'>document.all.txtEMailAddress.focus();</script>")
        chkSystemAdmin.Attributes("onclick") = "SystemAdmin();"


        ' Build the user object
        Try
            Me.mobjUser = BuildUserObject(Request.Cookies("docAssist"))
            Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
            Me.mconSqlWeb = New SqlClient.SqlConnection(Functions.ConnectionString())

        Catch ex As Exception
            If ex.Message = "User is not logged on" Then
                Response.Redirect("Default.aspx")
            Else
                'lblErrorMessage.Text = ex.Message
                'lblErrorMessage.Visible = True
            End If
        End Try

        'Check user access
        If Not (Me.mobjUser.SecAdmin = True Or Me.mobjUser.SysAdmin = True) Then
            litAccessDenied.Text = "<script language='javascript'>alert('You do not have access to this feature. Please contact your administrator');window.location.href='Default.aspx';</script>"
            Exit Sub
        End If

        'Workflow section security
        If IsAccountWorkflowEnabled(Me.mobjUser.AccountId) = True Then
            'ddlUserType.Items.Remove("Workflow Only")
            Dim lstItem As New ListItem("Workflow Only", "1")
            ddlUserType.Items.Add(lstItem)
        End If

        Session("EditUser") = CInt(Request.QueryString("Id"))

        '
        ' Hide the warning fields
        '
        txtEMailAddress.CssClass = "InputBox"
        txtFirstName.CssClass = "InputBox"
        txtLastName.CssClass = "InputBox"
        txtPassword.CssClass = "InputBox"
        txtPasswordConfirm.CssClass = "InputBox"
        lblEMailWarning.Text = ""
        lblFirstNameWarning.Text = ""
        lblLastNameWarning.Text = ""
        lblPasswordWarning.Text = ""
        lblEMailWarning.Visible = False
        lblFirstNameWarning.Visible = False
        lblLastNameWarning.Visible = False
        lblPasswordWarning.Visible = False

        Try
            If Not Page.IsPostBack Then

                If Not Session("EditUser") Is Nothing Then
                    '
                    ' Get the User Document Security
                    '
                    Dim intUserId As Integer = Session("EditUser")

                    txtImagesUserId.Text = intUserId.ToString()
                    '
                    ' Get User Information
                    '
                    Dim cmdSql As SqlClient.SqlCommand
                    'If intUserId > 0 Then
                    lblCompanyUser.Visible = False
                    ddlCompanyUsers.Visible = False
                    tableCompanyUser.Visible = False
                    cmdSql = New SqlClient.SqlCommand("SELECT UserName FROM Users WHERE UserId = @UserId", Me.mconSqlImage)
                    cmdSql.Parameters.Add("@UserId", intUserId)
                    Me.mconSqlImage.Open()
                    txtWebUserID.Text = cmdSql.ExecuteScalar()

                    If intUserId > 0 Then
                        LoadUserInfo()
                    End If

                    Me.mconSqlImage.Close()

                    LoadDocuments()

                End If

            End If

        Catch ex As Exception

        End Try

    End Sub

    Private Sub lnkSave2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkSave2.Click, lnkSave1.Click
        Dim cmdSql As SqlClient.SqlCommand

        'Set mode
        Dim gmode As String
        If CInt(txtImagesUserId.Text) > 0 Then
            gmode = "Edit"
        Else
            gmode = "Add"
        End If

        '
        ' Validate Fields
        '
        Dim bolIsValid As Boolean = True
        Dim regEx As New System.Text.RegularExpressions.Regex("\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*")
        '
        ' EMail
        If Not txtEMailAddress.Text.Trim.Length > 0 Then
            lblEMailWarning.Text = "Please enter a valid E-Mail Address"
            lblEMailWarning.Visible = True
            bolIsValid = False
        Else
            ' Check for valid e-mail address
            If Not regEx.IsMatch(txtEMailAddress.Text) Then
                lblEMailWarning.Text = "Invalid E-Mail Address."
                lblEMailWarning.Visible = True
                bolIsValid = False
            Else
                ' Check for existing user
                If gmode = "Add" Then
                    cmdSql = New SqlClient.SqlCommand("SELECT 1 FROM Users WHERE EMailAddress = @EMailAddress", Me.mconSqlWeb)
                    cmdSql.CommandTimeout = DEFAULT_COMMAND_TIMEOUT
                    cmdSql.Parameters.Add("@EMailAddress", txtEMailAddress.Text.Trim)
                    If Not Me.mconSqlWeb.State = ConnectionState.Open Then Me.mconSqlWeb.Open()
                    Dim bolExists As Boolean = cmdSql.ExecuteScalar
                    If bolExists Then
                        ' Check to see if the user is assigned to this account
                        cmdSql.CommandText = "SELECT 1 FROM UserAccounts, Users WHERE UserAccounts.UserId = Users.UserId AND Users.EMailAddress = @EmailAddress"
                        bolExists = cmdSql.ExecuteScalar
                        If bolExists Then
                            lblEMailWarning.Text = "User already exists."
                            lblEMailWarning.Visible = True
                            bolIsValid = False
                        End If
                    End If
                    Me.mconSqlWeb.Close()
                End If
            End If
        End If
        '
        ' First Name
        If Not txtFirstName.Text.Trim.Length > 0 Then
            lblFirstNameWarning.Text = "Please enter the users first name."
            lblFirstNameWarning.Visible = True
            bolIsValid = False
        End If
        '
        ' Last Name
        If Not txtLastName.Text.Trim.Length > 0 Then
            lblLastNameWarning.Text = "Please enter the users last name."
            lblLastNameWarning.Visible = True
            bolIsValid = False
        End If
        '
        ' Password
        If txtWebUserID.Text.Length = 0 Then
            If txtPassword.Text.Trim.Length = 0 Then
                lblPasswordWarning.Text = "Please enter a password."
                lblPasswordWarning.Visible = True
                bolIsValid = False
            End If
        End If
        If (Not txtPassword.Text = txtPasswordConfirm.Text) And lblPasswordWarning.Visible = False Then
            lblPasswordWarning.Text = "Passwords don't match.  Please re-enter."
            lblPasswordWarning.Visible = True
            bolIsValid = False
        End If

        'Passwords are the same, lets make sure its strong now.
        If Not gmode = "Edit" And txtPassword.Text <> "**********" Then
            If Functions.PasswordStrongValidation(txtPassword.Text) = False Then
                lblPasswordWarning.Text = "Password must be between 6 and 15 characters, and must include at least one upper case letter, one lower case letter, and one numeric digit."
                lblPasswordWarning.Visible = True
                bolIsValid = False
            End If
        End If


        '
        ' Documents
        For Each dgItem As DataGridItem In dgDocumentRights.Items
            Dim chkExport As CheckBox = dgItem.FindControl("chkExport")
            Dim chkPrint As CheckBox = dgItem.FindControl("chkPrint")
            Dim chkAnnotation As CheckBox = dgItem.FindControl("chkAnnotation")
            Dim chkRedaction As CheckBox = dgItem.FindControl("chkRedaction")
            Dim txtSecValue As TextBox = dgItem.FindControl("txtSecValue")
            Dim intSecLevel As Integer = Request.Form("rdoSecLevel" & txtSecValue.Text)
            Dim chkCurrentVersionOnly As CheckBox = CType(dgItem.FindControl("chkCurrentVersionOnly"), CheckBox)
            Dim chkCreatorOnly As CheckBox = CType(dgItem.FindControl("chkCreatorOnly"), CheckBox)

            If (chkExport.Checked Or chkPrint.Checked Or chkCreatorOnly.Checked Or chkCurrentVersionOnly.Checked) And intSecLevel = 0 Then
                lblDocumentWarning.Text = "Cannot assign Print, E-Mail/Export, Current Version or Creator only options when the user has no rights to the document."
                lblDocumentWarning.Visible = True
                bolIsValid = False
            End If
        Next

        ' Show/Hide Document Admin
        If chkSystemAdmin.Checked Then
            dgDocumentRights.Style.Item("DISPLAY") = "none"
            dgDocumentRights.Style.Item("VISIBILITY") = "hidden"
            lblSystemAdmin.Style.Item("VISIBILITY") = "visible"
        Else
            dgDocumentRights.Style.Item("DISPLAY") = "block"
            dgDocumentRights.Style.Item("VISIBILITY") = "visible"
            lblSystemAdmin.Style.Item("VISIBILITY") = "hidden"
        End If

        '
        ' Update the user
        '
        If bolIsValid Then

            Dim result As Boolean
            If gmode = "Edit" Then
                result = UpdateUser()
            Else
                result = AddUser()
            End If

            If result Then litScript.Text = "<SCRIPT language=""javascript"">window.returnValue = '1';window.close();</SCRIPT>"

        End If

    End Sub

    Private Function UpdateUser() As Boolean

        'HAM BABY!

        Dim chkChangePassword As CheckBox = CType(Me.FindControl("chkChangePassword"), CheckBox)

        Dim bolReturn As Boolean = False
        Try
            '
            ' Update the web user
            '
            Dim sb As New System.Text.StringBuilder
            sb.Append("UPDATE Users SET UserId=@UserId, EMailAddress=@EMailAddress, LastName=@LastName, FirstName=@FirstName, Enabled=@Enabled,")

            'Check length and make sure both match
            If txtPassword.Text.Trim <> "**********" Then
                sb.Append("Password=@Password, ")
            End If

            If chkUserEnabled.Checked = True Then
                sb.Append("FailedLogin=@FailedLogin, ")
            End If

            'Force password change parameter if needed.
            sb.Append("PasswordExpires=@PasswordExpires, ")

            sb.Append("ContactNo=@ContactNo WHERE UserId = @UserId")
            Dim cmdUpdate As New SqlClient.SqlCommand(sb.ToString, Me.mconSqlWeb)
            cmdUpdate.CommandTimeout = DEFAULT_COMMAND_TIMEOUT

            If chkUserEnabled.Checked = True Then
                cmdUpdate.Parameters.Add("@FailedLogin", "0")
            End If

            cmdUpdate.Parameters.Add("@UserId", txtWebUserID.Text)
            cmdUpdate.Parameters.Add("@EMailAddress", txtEMailAddress.Text)
            cmdUpdate.Parameters.Add("@LastName", txtLastName.Text)
            cmdUpdate.Parameters.Add("@FirstName", txtFirstName.Text)
            cmdUpdate.Parameters.Add("@Enabled", chkUserEnabled.Checked)

            If chkChangePassword.Checked = True Then
                cmdUpdate.Parameters.Add("@PasswordExpires", Now.Date.AddDays(-1))
            Else
                cmdUpdate.Parameters.Add("@PasswordExpires", Now.Date.AddDays(90))
            End If

            If txtPassword.Text.Length > 0 And txtPasswordConfirm.Text = txtPassword.Text Then
                Dim objEnc As New Encryption.Encryption
                cmdUpdate.Parameters.Add("@Password", objEnc.Encrypt(txtPassword.Text.Trim))
            End If
            cmdUpdate.Parameters.Add("@ContactNo", txtContactNo.Text)

            If Not Me.mconSqlWeb.State = ConnectionState.Open Then
                Me.mconSqlWeb.Open()
            End If
            cmdUpdate.ExecuteNonQuery()
            '
            ' Add the user to the account if they don't exist
            '
            Dim cmdSql As New SqlClient.SqlCommand("SELECT 1 FROM UserAccounts WHERE UserId = @UserId AND AccountId = @AccountId", Me.mconSqlWeb)
            cmdSql.CommandTimeout = DEFAULT_COMMAND_TIMEOUT
            cmdSql.Parameters.Add("@UserId", txtWebUserID.Text)
            cmdSql.Parameters.Add("@AccountId", Me.mobjUser.AccountId)
            Dim intExists As Integer = cmdSql.ExecuteScalar
            If Not intExists = 1 Then
                cmdUpdate = New SqlClient.SqlCommand("INSERT INTO UserAccounts (UserId, AccountId, DefaultAccount, PasswordExpires) VALUES (@UserId, @AccountId,1,PasswordExpires)", Me.mconSqlWeb)
                cmdSql.CommandTimeout = DEFAULT_COMMAND_TIMEOUT
                cmdUpdate.Parameters.Add("@UserId", txtWebUserID.Text)
                cmdUpdate.Parameters.Add("@AccountId", Me.mobjUser.AccountId)

                cmdUpdate.ExecuteNonQuery()

                ' Track the change
                LogUserAccounts(New System.Guid(txtWebUserID.Text), Me.mobjUser.AccountId, Me.mobjUser.UserId, "Add", Me.mconSqlWeb)
            Else
                LogUserAccounts(New System.Guid(txtWebUserID.Text), Me.mobjUser.AccountId, Me.mobjUser.UserId, "Change", Me.mconSqlWeb)
            End If

            Me.mconSqlWeb.Close()

            ' Update the image user sec level
            If Not Me.mconSqlImage.State = ConnectionState.Open Then Me.mconSqlImage.Open()
            cmdUpdate = New SqlClient.SqlCommand("UPDATE Users SET SecAdmin = @SecAdmin, SysAdmin = @SysAdmin,  UserType = @UserType,  SFAdmin = @SFAdmin,  BTAdmin = @BTAdmin WHERE UserId = @UserId", Me.mconSqlImage)
            cmdUpdate.CommandTimeout = DEFAULT_COMMAND_TIMEOUT
            cmdUpdate.Parameters.Add("@UserId", txtImagesUserId.Text)
            cmdUpdate.Parameters.Add("@SysAdmin", chkSystemAdmin.Checked)
            cmdUpdate.Parameters.Add("@SecAdmin", chkSecurityAdmin.Checked)
            cmdUpdate.Parameters.Add("@UserType", ddlUserType.SelectedValue)
            cmdUpdate.Parameters.Add("@SFAdmin", chkSmartFolders.Checked)
            cmdUpdate.Parameters.Add("@BTAdmin", chkTemplateAdmin.Checked)


            If Not Me.mconSqlImage.State = ConnectionState.Open Then Me.mconSqlImage.Open()
            cmdUpdate.ExecuteNonQuery()

            ' Update the image user security
            Dim dtUserSec As New Data.Images.UserSec
            Dim cmdUserSec As New SqlClient.SqlCommand("acsUserSec", Me.mconSqlImage)
            cmdUserSec.CommandTimeout = DEFAULT_COMMAND_TIMEOUT
            cmdUserSec.CommandType = CommandType.StoredProcedure
            cmdUserSec.Parameters.Add("@UserId", CInt(txtImagesUserId.Text))
            Dim da As New SqlClient.SqlDataAdapter(cmdUserSec)
            Try
                da.Fill(dtUserSec)
            Catch exSql As SqlClient.SqlException
                Dim intError As Integer = 1
            End Try

            dtUserSec.PrimaryKey = New DataColumn() {dtUserSec.SecType, dtUserSec.SecValue}
            For Each dgItem As DataGridItem In dgDocumentRights.Items
                Dim txtSecValue As TextBox = CType(dgItem.FindControl("txtSecValue"), TextBox)
                Dim chkExport As CheckBox = CType(dgItem.FindControl("chkExport"), CheckBox)
                Dim chkPrint As CheckBox = CType(dgItem.FindControl("chkPrint"), CheckBox)
                'Dim chkWorkflow As CheckBox = CType(dgItem.FindControl("chkWorkflow"), CheckBox)
                Dim chkAnnotation As CheckBox = CType(dgItem.FindControl("chkAnnotation"), CheckBox)
                Dim chkRedaction As CheckBox = CType(dgItem.FindControl("chkRedaction"), CheckBox)
                Dim chkCurrentVersionOnly As CheckBox = CType(dgItem.FindControl("chkCurrentVersionOnly"), CheckBox)
                Dim chkCreatorOnly As CheckBox = CType(dgItem.FindControl("chkCreatorOnly"), CheckBox)

                '
                ' Check for document value
                Dim intSecLevel As Integer = Request.Form("rdoSecLevel" & txtSecValue.Text) '0      ' Default to no access
                Dim intSearch(2) As Integer
                intSearch(0) = Data.Images.SecType.Document
                intSearch(1) = CInt(txtSecValue.Text)
                Dim drUserSec As Data.Images.UserSecRow = dtUserSec.Rows.Find(New Object() {intSearch(0), intSearch(1)})
                If drUserSec Is Nothing And intSecLevel > 0 Then
                    drUserSec = dtUserSec.NewRow
                    drUserSec.SecLevel = intSecLevel
                    drUserSec.SecValue = txtSecValue.Text
                    drUserSec.SecType = Data.Images.SecType.Document
                    drUserSec.UserId = txtImagesUserId.Text
                    dtUserSec.Rows.Add(drUserSec)
                End If
                If Not drUserSec Is Nothing Then
                    If drUserSec.SecLevel <> intSecLevel Then
                        drUserSec.SecLevel = intSecLevel
                    End If
                End If
                '
                ' Check for export value
                intSearch(0) = Data.Images.SecType.Export
                drUserSec = dtUserSec.Rows.Find(New Object() {intSearch(0), intSearch(1)})
                If drUserSec Is Nothing And chkExport.Checked Then
                    drUserSec = dtUserSec.NewRow
                    drUserSec.SecType = Data.Images.SecType.Export          ' Export Document
                    drUserSec.SecValue = txtSecValue.Text               ' Document Id
                    drUserSec.SecLevel = chkExport.Checked              ' Security Setting
                    drUserSec.UserId = txtImagesUserId.Text
                    dtUserSec.Rows.Add(drUserSec)
                ElseIf Not drUserSec Is Nothing Then
                    drUserSec.SecLevel = chkExport.Checked
                End If
                '
                ' Check for print value
                intSearch(0) = Data.Images.SecType.Print
                drUserSec = dtUserSec.Rows.Find(New Object() {intSearch(0), intSearch(1)})
                If drUserSec Is Nothing And chkPrint.Checked Then
                    drUserSec = dtUserSec.NewRow
                    drUserSec.SecType = Data.Images.SecType.Print           ' Print Document
                    drUserSec.SecValue = txtSecValue.Text               ' Document Id
                    drUserSec.SecLevel = chkPrint.Checked               ' Security Setting
                    drUserSec.UserId = txtImagesUserId.Text
                    dtUserSec.Rows.Add(drUserSec)
                ElseIf Not drUserSec Is Nothing Then
                    drUserSec.SecLevel = chkPrint.Checked
                End If

                'Annotation
                intSearch(0) = Data.Images.SecType.Annotation
                drUserSec = dtUserSec.Rows.Find(New Object() {intSearch(0), intSearch(1)})
                If drUserSec Is Nothing And chkAnnotation.Checked Then
                    drUserSec = dtUserSec.NewRow
                    drUserSec.SecType = Data.Images.SecType.Annotation
                    drUserSec.SecValue = txtSecValue.Text
                    If chkAnnotation.Checked Then
                        drUserSec.SecLevel = 1
                    Else
                        drUserSec.SecLevel = 0
                    End If
                    drUserSec.UserId = txtImagesUserId.Text
                    dtUserSec.Rows.Add(drUserSec)
                ElseIf Not drUserSec Is Nothing Then
                    If chkAnnotation.Checked Then
                        drUserSec.SecLevel = 1
                    Else
                        drUserSec.SecLevel = 0
                    End If
                End If

                'Redaction
                intSearch(0) = Data.Images.SecType.Redaction
                drUserSec = dtUserSec.Rows.Find(New Object() {intSearch(0), intSearch(1)})
                If drUserSec Is Nothing And chkRedaction.Checked Then
                    drUserSec = dtUserSec.NewRow
                    drUserSec.SecType = Data.Images.SecType.Redaction
                    drUserSec.SecValue = txtSecValue.Text
                    If chkRedaction.Checked Then
                        drUserSec.SecLevel = 1
                    Else
                        drUserSec.SecLevel = 0
                    End If
                    drUserSec.UserId = txtImagesUserId.Text
                    dtUserSec.Rows.Add(drUserSec)
                ElseIf Not drUserSec Is Nothing Then
                    If chkRedaction.Checked Then
                        drUserSec.SecLevel = 1
                    Else
                        drUserSec.SecLevel = 0
                    End If
                End If

                'Creator Only
                intSearch(0) = Data.Images.SecType.CreatorOnly
                drUserSec = dtUserSec.Rows.Find(New Object() {intSearch(0), intSearch(1)})
                If drUserSec Is Nothing And chkCreatorOnly.Checked Then
                    drUserSec = dtUserSec.NewRow
                    drUserSec.SecType = Data.Images.SecType.CreatorOnly
                    drUserSec.SecValue = txtSecValue.Text
                    If chkCreatorOnly.Checked Then
                        drUserSec.SecLevel = 1
                    Else
                        drUserSec.SecLevel = 0
                    End If
                    drUserSec.UserId = txtImagesUserId.Text
                    dtUserSec.Rows.Add(drUserSec)
                ElseIf Not drUserSec Is Nothing Then
                    If chkCreatorOnly.Checked Then
                        drUserSec.SecLevel = 1
                    Else
                        drUserSec.SecLevel = 0
                    End If
                End If

                'Current Version Only
                intSearch(0) = Data.Images.SecType.CurrentVersionOnly
                drUserSec = dtUserSec.Rows.Find(New Object() {intSearch(0), intSearch(1)})
                If drUserSec Is Nothing And chkCurrentVersionOnly.Checked Then
                    drUserSec = dtUserSec.NewRow
                    drUserSec.SecType = Data.Images.SecType.CurrentVersionOnly
                    drUserSec.SecValue = txtSecValue.Text
                    If chkCurrentVersionOnly.Checked Then
                        drUserSec.SecLevel = 1
                    Else
                        drUserSec.SecLevel = 0
                    End If
                    drUserSec.UserId = txtImagesUserId.Text
                    dtUserSec.Rows.Add(drUserSec)
                ElseIf Not drUserSec Is Nothing Then
                    If chkCurrentVersionOnly.Checked Then
                        drUserSec.SecLevel = 1
                    Else
                        drUserSec.SecLevel = 0
                    End If
                End If



            Next

            dtUserSec.Update(Me.mconSqlImage)

            bolReturn = True

        Catch ex As Exception
            Throw ex
        Finally
            If Not Me.mconSqlImage.State = ConnectionState.Closed Then Me.mconSqlImage.Close()
            If Not Me.mconSqlWeb.State = ConnectionState.Closed Then Me.mconSqlWeb.Close()
        End Try

        Return bolReturn

    End Function

    Private Sub ddlCompanyUsers_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlCompanyUsers.SelectedIndexChanged

        If ddlCompanyUsers.Items(ddlCompanyUsers.SelectedIndex).Text.Length > 0 Then
            Dim guidUserId As New System.Guid(ddlCompanyUsers.SelectedValue)
            Dim intUserId As Integer = Functions.GetImagesUserId(Me.mconSqlImage, guidUserId)
            txtWebUserID.Text = guidUserId.ToString()
            txtImagesUserId.Text = intUserId

            LoadUserInfo()

        Else
            txtContactNo.Text = ""
            txtEMailAddress.Text = ""
            chkSystemAdmin.Checked = False
            txtFirstName.Text = ""
            txtImagesUserId.Text = "0"
            txtLastName.Text = ""
            txtPassword.Text = ""
            txtPasswordConfirm.Text = ""
            txtWebUserID.Text = ""

            LoadDocuments()
        End If

    End Sub

    Private Sub LoadUserInfo()

        Try
            Dim cmdSql As New SqlClient.SqlCommand("acsUsersSecurity", Me.mconSqlWeb)
            cmdSql.CommandTimeout = DEFAULT_COMMAND_TIMEOUT
            cmdSql.CommandType = CommandType.StoredProcedure
            cmdSql.Parameters.Add("@UserId", txtWebUserID.Text)
            '
            ' Get User Info
            '
            Me.mconSqlWeb.Open()
            Dim dr As SqlClient.SqlDataReader = cmdSql.ExecuteReader(CommandBehavior.SingleRow)
            If dr.Read Then
                txtWebUserID.Text = CType(dr("UserId"), System.Guid).ToString()
                guidUserId = CType(dr("UserId"), System.Guid)
                txtContactNo.Text = IIf(dr("ContactNo") Is System.DBNull.Value, "", dr("ContactNo"))
                txtEMailAddress.Text = dr("EMailAddress")
                txtFirstName.Text = dr("FirstName")
                txtLastName.Text = dr("LastName")
                chkUserEnabled.Checked = dr("Enabled")
                Dim objEncryption As New Encryption.Encryption
                txtPassword.Attributes.Add("VALUE", "**********") ' objEncryption.Decrypt(dr("Password"))
                txtPasswordConfirm.Attributes.Add("VALUE", "**********") ' objEncryption.Decrypt(dr("Password"))
            End If

            'Locked out message
            If dr("Locked") = 1 Then
                lblLockedOut.Text = "This account has been locked out because 5 invalid login attempts were made. You must enable this account below before the user can login again."
                lblLockedOut.Visible = True
            End If

            dr.Close()
            Me.mconSqlWeb.Close()
            '
            ' Check for SecAdmin
            '
            cmdSql = New SqlClient.SqlCommand("SELECT 1 FROM Users WHERE SecAdmin = 1 AND UserId = @UserId", Me.mconSqlImage)
            cmdSql.CommandTimeout = DEFAULT_COMMAND_TIMEOUT
            cmdSql.Parameters.Add("@UserId", CInt(txtImagesUserId.Text))
            If Not Me.mconSqlImage.State = ConnectionState.Open Then Me.mconSqlImage.Open()
            chkSecurityAdmin.Checked = cmdSql.ExecuteScalar()

            'Check for sysadmin
            cmdSql = New SqlClient.SqlCommand("SELECT 1 FROM Users WHERE SysAdmin = 1 AND UserId = @UserId", Me.mconSqlImage)
            cmdSql.CommandTimeout = DEFAULT_COMMAND_TIMEOUT
            cmdSql.Parameters.Add("@UserId", CInt(txtImagesUserId.Text))
            If Not Me.mconSqlImage.State = ConnectionState.Open Then Me.mconSqlImage.Open()
            chkSystemAdmin.Checked = cmdSql.ExecuteScalar()

            'Check for Smart folders admin
            cmdSql = New SqlClient.SqlCommand("SELECT SFAdmin FROM Users WHERE UserId = @UserId", Me.mconSqlImage)
            cmdSql.CommandTimeout = DEFAULT_COMMAND_TIMEOUT
            cmdSql.Parameters.Add("@UserId", CInt(txtImagesUserId.Text))
            If Not Me.mconSqlImage.State = ConnectionState.Open Then Me.mconSqlImage.Open()
            chkSmartFolders.Checked = cmdSql.ExecuteScalar()

            'Check for template admin
            cmdSql = New SqlClient.SqlCommand("SELECT BTAdmin FROM Users WHERE UserId = @UserId", Me.mconSqlImage)
            cmdSql.CommandTimeout = DEFAULT_COMMAND_TIMEOUT
            cmdSql.Parameters.Add("@UserId", CInt(txtImagesUserId.Text))
            If Not Me.mconSqlImage.State = ConnectionState.Open Then Me.mconSqlImage.Open()
            chkTemplateAdmin.Checked = cmdSql.ExecuteScalar()

            'Show/hide document details on load
            If chkSystemAdmin.Checked Then
                dgDocumentRights.Style.Item("DISPLAY") = "none"
                dgDocumentRights.Style.Item("VISIBILITY") = "hidden"
                'AdminRow.Style.Item("DISPLAY") = "none"
                'DocumentHeader.Style.Item("DISPLAY") = "none"
                'GridRow.Style.Item("DISPLAY") = "none"
                lblSystemAdmin.Style.Item("VISIBILITY") = "visible"
                'divDocumentRights.Style.Item("DISPLAY") = "none"
                'SpacerImages.Height = "100%"
            Else
                dgDocumentRights.Style.Item("DISPLAY") = "block"

                dgDocumentRights.Style.Item("VISIBILITY") = "visible"
                'AdminRow.Style.Item("DISPLAY") = "block"
                'DocumentHeader.Style.Item("DISPLAY") = "block"
                'GridRow.Style.Item("DISPLAY") = "block"

                'lblSystemAdmin.Style.Item("VISIBILITY") = "hidden"
                lblSystemAdmin.Style.Item("VISIBILITY") = "hidden"

                'divDocumentRights.Style.Item("DISPLAY") = "block"
            End If

            'Load User Type
            cmdSql = New SqlClient.SqlCommand("SELECT UserType FROM Users WHERE UserId = @UserId", Me.mconSqlImage)
            cmdSql.CommandTimeout = DEFAULT_COMMAND_TIMEOUT
            cmdSql.Parameters.Add("@UserId", CInt(txtImagesUserId.Text))
            If Not Me.mconSqlImage.State = ConnectionState.Open Then Me.mconSqlImage.Open()
            ddlUserType.SelectedValue = cmdSql.ExecuteScalar()

            '' Get the WFOnlyUser flag for this user on this account
            'cmdSql.CommandType = CommandType.Text
            'cmdSql.CommandText = "SELECT WFUserOnly FROM Users WHERE userid = @UserID"
            'cmdSql.Parameters.Clear()
            'cmdSql.Parameters.Add("@UserID", CInt(txtImagesUserId.Text))
            'chkWFOnlyUser.Checked = cmdSql.ExecuteScalar()

            LoadDocuments()

            Me.mconSqlImage.Close()

        Catch ex As Exception

        End Try

    End Sub

    Private Sub LoadDocuments()
        '
        ' Load Document Security
        ' 
        Me.mconSqlWeb.Close()
        Dim dt As New DataTable("UserSec")
        Dim cmdSql As New SqlClient.SqlCommand("acsUserSecWeb", Me.mconSqlImage)
        cmdSql.CommandTimeout = DEFAULT_COMMAND_TIMEOUT
        cmdSql.CommandType = CommandType.StoredProcedure
        cmdSql.Parameters.Add("@UserId", CInt(txtImagesUserId.Text))

        Dim da As New SqlClient.SqlDataAdapter(cmdSql)
        da.Fill(dt)
        da.Dispose()

        dgDocumentRights.DataSource = dt
        dgDocumentRights.DataBind()

        'For Each drItem As DataGridItem In dgDocumentRights.Items
        '    Try
        '        Dim txtSecValue As TextBox = CType(drItem.FindControl("txtSecValue"), TextBox)
        '        Dim chkExported As CheckBox = CType(drItem.FindControl("chkExport"), CheckBox)
        '        Dim chkPrint As CheckBox = CType(drItem.FindControl("chkPrint"), CheckBox)
        '        Dim chkAnnotation As CheckBox = CType(drItem.FindControl("chkAnnotation"), CheckBox)
        '        Dim chkRedaction As CheckBox = CType(drItem.FindControl("chkRedaction"), CheckBox)
        '        Dim chkCreatorOnly As CheckBox = CType(drItem.FindControl("chkCreatorOnly"), CheckBox)
        '        Dim chkCurrentVersionOnly As CheckBox = CType(drItem.FindControl("chkCurrentVersionOnly"), CheckBox)
        '
        '        If Not txtSecValue Is Nothing Then
        '            Dim rdoSecValue As RadioButton = CType(drItem.FindControl("rdoSecLevel" & (drItem.ItemIndex + 1) & txtSecValue.Text), RadioButton)
        '        End If
        '
        '    Catch ex As Exception
        '
        '    End Try
        'Next


    End Sub

    Private Sub chkWFOnlyUser_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If chkWFOnlyUser.Checked = True Then
            dgDocumentRights.Enabled = False
        Else
            dgDocumentRights.Enabled = True
        End If
    End Sub

    Private Sub dgDocumentRights_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgDocumentRights.ItemDataBound





    End Sub

End Class

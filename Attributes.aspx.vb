Imports Accucentric.docAssist
Imports Accucentric.docAssist.Data.Images
Imports Telerik.WebControls

Partial Class Attributes
    Inherits System.Web.UI.Page

    Private mobjUser As Accucentric.docAssist.Web.Security.User
    Private mconSqlImage As SqlClient.SqlConnection
    Protected WithEvents imgCapture As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents frameConfirm As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents iframeConfirm As System.Web.UI.HtmlControls.HtmlGenericControl

    Private mSecurityLevel As Web.Security.SecurityLevel
    Private mbolRollback As Boolean
    Private mcookieSearch As cookieSearch
    Protected WithEvents IMG1 As System.Web.UI.HtmlControls.HtmlImage

    Private ActivateFunction As String
    Protected WithEvents Hidden1 As System.Web.UI.HtmlControls.HtmlInputHidden
    Private AddingNewRow As Boolean = False

    Dim dtAttributes As Data.Images.Attributes

    Private Enum AttributeAccess
        NONE = 0
        READ_ONLY = 1
        CHANGE = 2
        DELETE = 3
    End Enum

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Function NumericAttribute(ByVal strAttribute As String) As Boolean
        'As long as there is not a letter in the attribute I assume it is numeric.
        'We don't care about any characters. (i.e. -,./) -AB

        Dim charCharacters As Char() = strAttribute.ToCharArray

        For x As Integer = 0 To charCharacters.Length - 1
            If Char.IsLetter(charCharacters(x)) Then Return False
        Next

        Return True

    End Function

    Friend Function ValidateAttribute(ByRef intRowIndex As Integer) As Boolean

        Dim rValue As Boolean = False

        ' Define Grid Controls
        Dim dgRow As DataGridItem = dgAttributes.Items(intRowIndex)
        Dim btnCalendar As Button
        Dim ddlAttributeId As DropDownList
        Dim lblAttributeId As Label
        Dim lblAttributeDataType As Label
        Dim lblDataFormat As Label
        Dim lblLength As Label
        Dim txtAttributeValue As TextBox
        Dim ddlAttributeList As DropDownList
        Dim chkBox As CheckBox
        Dim lblEditError As Label

        ' Define Row Values
        Dim intAttributeId As Integer
        Dim intLength As Integer
        Dim strAttributeValue As String
        Dim strDataFormat As String
        Dim strDataType As String
        Dim sqlCon As New SqlClient.SqlConnection

        ' Save button validation controls
        Dim lblAttributeValue As Label

        ' Get the row controls
        If dgRow.ItemIndex = dgAttributes.EditItemIndex Then
            lblAttributeId = CType(dgRow.FindControl("lblAttributeIdEdit"), Label)
            lblAttributeDataType = CType(dgRow.FindControl("lblAttributeDataTypeEdit"), Label)
            lblDataFormat = CType(dgRow.FindControl("lblDataFormatEdit"), Label)
            lblLength = CType(dgRow.FindControl("lblLengthEdit"), Label)
            txtAttributeValue = CType(dgRow.FindControl("txtAttributeValue"), TextBox)
            ddlAttributeList = CType(dgRow.FindControl("ddlAttributeList"), DropDownList)
            chkBox = CType(dgRow.FindControl("chkBox"), CheckBox)
            lblEditError = CType(dgRow.FindControl("lblEditError"), Label)
        Else
            'Save button validation.
            lblAttributeId = CType(dgRow.FindControl("lblAttributeId"), Label)
            lblAttributeDataType = CType(dgRow.FindControl("lblAttributeDataType"), Label)

            If lblAttributeDataType.Text.Trim = "" Then 'This item was put here by the capture object and does not have data formatting information bound to it.

                Try
                    sqlCon = BuildConnection(Me.mobjUser)
                    sqlCon.Open()
                    Dim sqlCmd As New SqlClient.SqlCommand("acsAdminAttributeValues", sqlCon)
                    sqlCmd.CommandType = CommandType.StoredProcedure
                    sqlCmd.Parameters.Add("@AttributeID", lblAttributeId.Text)
                    Dim daSql As New SqlClient.SqlDataAdapter(sqlCmd)
                    Dim dtFormatting As New DataTable
                    daSql.Fill(dtFormatting)
                    sqlCon.Close()
                    sqlCon.Dispose()

                    'Find the actual labels first so we can set the values on them
                    lblDataFormat = CType(dgRow.FindControl("lblDataFormat"), Label)
                    lblLength = CType(dgRow.FindControl("lblLength"), Label)

                    ddlAttributeList = CType(dgRow.FindControl("ddlAttributeList"), DropDownList)

                    lblAttributeValue = CType(dgRow.FindControl("lblAttributeValue"), Label)
                    lblAttributeId = CType(dgRow.FindControl("lblAttributeId"), Label)

                    lblAttributeDataType.Text = dtFormatting.Rows(0)("AttributeDataType")
                    lblDataFormat.Text = dtFormatting.Rows(0)("DataFormat")
                    lblLength.Text = dtFormatting.Rows(0)("Length")

                    strAttributeValue = lblAttributeValue.Text
                    txtAttributeValue = New TextBox
                    txtAttributeValue.Text = strAttributeValue

                    lblEditError = CType(dgRow.FindControl("lblRowError"), Label) 'lblRowError

                Catch ex As Exception
                    If sqlCon.State <> ConnectionState.Closed Then sqlCon.Close()
                    sqlCon.Dispose()
                End Try

            Else
                lblDataFormat = CType(dgRow.FindControl("lblDataFormat"), Label)
                lblLength = CType(dgRow.FindControl("lblLength"), Label)
                lblAttributeValue = CType(dgRow.FindControl("lblAttributeValue"), Label)
                strAttributeValue = lblAttributeValue.Text
                txtAttributeValue = New TextBox
                txtAttributeValue.Text = strAttributeValue
                lblEditError = CType(dgRow.FindControl("lblRowError"), Label) 'lblRowError
            End If

        End If
        '
        ' Check for valid data type
        If lblAttributeDataType.Text.Trim.Length > 0 Then
            Select Case lblAttributeDataType.Text
                Case "Numeric"
                    If NumericAttribute(txtAttributeValue.Text) = False Then
                        Dim strNumericError As String = "Attribute is numeric and cannot contain letters."
                        'If lblEditError Is Nothing Then

                        'Else
                        lblEditError.Text = "Attribute is numeric and cannot contain letters."
                        lblEditError.Visible = True
                        'ReturnControls.Add(lblEditError)
                        Return False
                        'End If
                    Else
                        lblEditError.Text = ""
                        lblEditError.Visible = False
                        'ReturnControls.Add(lblEditError)
                    End If
                Case "Date"

                    If Not IsDate(txtAttributeValue.Text) Then
                        lblEditError.Text = "Date is not in a valid format."
                        lblEditError.Visible = True
                        'ReturnControls.Add(lblEditError)
                        Return False
                    End If
                Case "ListView"

                    Dim lblListValidate As Label = dgRow.FindControl("lblListValidate")

                    If lblListValidate.Text = "True" Then
                        sqlCon = Functions.BuildConnection(Me.mobjUser)
                        sqlCon.Open()
                        Dim objList As New ListAttributes(sqlCon, False)
                        Dim numMatches = objList.AttributeListValidate(lblAttributeId.Text, txtAttributeValue.Text, ddlDocuments.SelectedValue, FolderId.Value)
                        If numMatches > 0 Then

                        Else
                            'No match
                            lblEditError.Text = "Invalid list value."
                            lblEditError.Visible = True
                            'ReturnControls.Add(lblEditError)
                            Return False
                        End If
                    End If

            End Select

        End If
        '
        ' Only check the attributes that have a format specified
        If txtAttributeValue.Text.Trim.Length > 0 And lblDataFormat.Text.Trim.Length > 0 Then
            '
            ' Get the row values
            If lblAttributeId Is Nothing Then
                intAttributeId = ddlAttributeId.SelectedValue
            Else
                intAttributeId = lblAttributeId.Text.Trim
            End If
            strAttributeValue = txtAttributeValue.Text
            strDataFormat = lblDataFormat.Text
            ' Data length
            If lblLength.Text.Trim.Length > 0 Then
                intLength = lblLength.Text
            Else
                intLength = Constants.DEFAULT_ATTRIBUTE_LENGTH
            End If
            ' Attribute data type
            If lblAttributeDataType.Text.Trim.Length > 0 Then
                strDataType = lblAttributeDataType.Text.Trim
            Else
                strDataType = Constants.DEFAULT_ATTRIBUTE_DATATYPE
            End If
            '
            ' Parse the search and check each value
            Dim strAttributes() As String = strAttributeValue.Split(";")
            Dim strValue As String, intCount As Integer
            For Each strValue In strAttributes
                Dim bolFormatError As Boolean = False
                Select Case strDataType
                    Case "String"

                        'Length of the mask and the attribute value should be the same or we know the format is invalid.
                        If strValue.Length <> strDataFormat.Length Then
                            lblEditError.Text = "Attribute value is not in the format required (" & strDataFormat & ")."
                            lblEditError.Visible = True
                            'ReturnControls.Add(lblEditError)
                            Return False
                        End If

                        Dim charAttributeValues() As Char = strValue.Trim.ToCharArray()
                        Dim charDataFormats() As Char = strDataFormat.ToCharArray()

                        'Process individual characters
                        'Todo: Change this to a case statement to allow different scenarios, wildcards(*) ?
                        For intCount = charAttributeValues.GetLowerBound(0) To charAttributeValues.GetUpperBound(0)
                            Select Case charDataFormats(intCount)
                                Case "@"
                                    'There needs to be a letter in this position
                                    If Char.IsLetter(charAttributeValues(intCount)) = False Then
                                        lblEditError.Text = "Attribute value is not in the format required (" & strDataFormat & ")."
                                        lblEditError.Visible = True
                                        'ReturnControls.Add(lblEditError)
                                        Return False
                                        Exit For
                                    End If
                                Case "#"
                                    'There needs to be a number in this position
                                    If Char.IsNumber(charAttributeValues(intCount)) = False Then
                                        lblEditError.Text = "Attribute value is not in the format required (" & strDataFormat & ")."
                                        lblEditError.Visible = True
                                        'ReturnControls.Add(lblEditError)
                                        Return False
                                        Exit For
                                    End If
                                Case "*"
                                    'Wildcard, skip this item
                                    intCount += 1
                                Case Else
                                    'Not a number or letter, match exactly
                                    If charAttributeValues(intCount) <> charDataFormats(intCount) Then
                                        lblEditError.Text = "Attribute value is not in the format required (" & strDataFormat & ")."
                                        lblEditError.Visible = True
                                        'ReturnControls.Add(lblEditError)
                                        Return False
                                        Exit For
                                    End If
                            End Select
                        Next

                    Case "Numeric"

                        'Length of the mask and the attribute value should be the same or we know the format is invalid.
                        If strValue.Length <> strDataFormat.Length Then
                            lblEditError.Text = "Attribute value is not in the format required (" & strDataFormat & ")."
                            lblEditError.Visible = True
                            'ReturnControls.Add(lblEditError)
                            Return False
                        End If

                        Dim charAttributeValues() As Char = strValue.Trim.ToCharArray()
                        Dim charDataFormats() As Char = strDataFormat.ToCharArray()

                        'Process individual characters.
                        'Todo: Change this to a case statement to allow different scenarios, wildcards(*) ?
                        For intCount = charAttributeValues.GetLowerBound(0) To charAttributeValues.GetUpperBound(0)
                            Select Case charDataFormats(intCount)
                                Case "@"
                                    'There needs to be a letter in this position
                                    If Char.IsLetter(charAttributeValues(intCount)) = False Then
                                        lblEditError.Text = "Attribute value is not in the format required (" & strDataFormat & ")."
                                        lblEditError.Visible = True
                                        'ReturnControls.Add(lblEditError)
                                        Return False
                                        Exit For
                                    End If
                                Case "#"
                                    'There needs to be a number in this position
                                    If Char.IsNumber(charAttributeValues(intCount)) = False Then
                                        lblEditError.Text = "Attribute value is not in the format required (" & strDataFormat & ")."
                                        lblEditError.Visible = True
                                        'ReturnControls.Add(lblEditError)
                                        Return False
                                        Exit For
                                    End If
                                Case "*"
                                    'Wildcard, skip this item
                                    intCount += 1
                                Case Else
                                    'Not a number or letter, match exactly
                                    If charAttributeValues(intCount) <> charDataFormats(intCount) Then
                                        lblEditError.Text = "Attribute value is not in the format required (" & strDataFormat & ")."
                                        lblEditError.Visible = True
                                        'ReturnControls.Add(lblEditError)
                                        Return False
                                        Exit For
                                    End If
                            End Select
                        Next

                    Case "Date"

                        'Date format validation

                End Select

                If bolFormatError Then Exit For

            Next

        End If

        Return True

    End Function

    Private Function GetImagesUserId() As Integer
        Return Functions.GetImagesUserId(Me.mconSqlImage, Me.mobjUser)
    End Function

    Private Function ReverseBindAttributes(Optional ByVal KeepBlankValues As Boolean = False) As dsImageAttributes

        Dim rValue As New dsImageAttributes

        Dim drImageAttributes As dsImageAttributes.ImageAttributesRow

        ' Define Row Values
        Dim bolRequired As Boolean
        Dim intAttributeId As Integer
        Dim intImageAttributeId As Integer
        Dim intLength As Integer
        Dim strAttributeDataType As String
        Dim strAttributeName As String
        Dim strAttributeValue As String
        Dim strDataFormat As String
        Dim strListViewStyle As String
        Dim strValueDescription As String
        Dim intAccessLevel As String
        Dim blnListValidate As Boolean
        Dim blnControlTotal As Boolean

        ' Get the current edit item from the index
        Dim intEditItemIndex As Integer = dgAttributes.EditItemIndex

        ' Loop through the grid
        Dim dgRow As DataGridItem
        For Each dgRow In dgAttributes.Items

            ' Get the row values
            If dgRow.ItemIndex <> intEditItemIndex Then

                If CType(dgRow.FindControl("lblRequired"), Label).Text.Length > 0 And CType(dgRow.FindControl("lblRequired"), Label).Text = "1" Then
                    bolRequired = True
                Else
                    bolRequired = False
                End If

                If CType(dgRow.FindControl("lblAttributeId"), Label).Text.Trim.Length > 0 Then
                    intAttributeId = CType(dgRow.FindControl("lblAttributeId"), Label).Text
                End If

                If CType(dgRow.FindControl("lblImageAttributeId"), Label).Text.Trim.Length > 0 Then
                    intImageAttributeId = CType(dgRow.FindControl("lblImageAttributeId"), Label).Text
                End If

                If CType(dgRow.FindControl("lblLength"), Label).Text.Trim.Length > 0 Then
                    intLength = CType(dgRow.FindControl("lblLength"), Label).Text
                Else
                    intLength = Constants.DEFAULT_ATTRIBUTE_LENGTH
                End If

                strAttributeDataType = CType(dgRow.FindControl("lblAttributeDataType"), Label).Text
                strAttributeName = CType(dgRow.FindControl("lblAttributeName"), Label).Text
                strAttributeValue = CType(dgRow.FindControl("txtAttributeValue"), TextBox).Text
                strDataFormat = CType(dgRow.FindControl("lblDataFormat"), Label).Text
                strListViewStyle = CType(dgRow.FindControl("lblListViewStyle"), Label).Text
                strValueDescription = CType(dgRow.FindControl("lblListViewItem"), Label).Text
                intAccessLevel = CType(dgRow.FindControl("lblAccessLevel"), Label).Text

                Dim lblListValidate As Label = CType(dgRow.FindControl("lblListValidate"), Label)
                If lblListValidate.Text.Trim = "" Then
                    blnListValidate = False
                Else
                    blnListValidate = CType(dgRow.FindControl("lblListValidate"), Label).Text
                End If

                If CType(dgRow.FindControl("lblControlAttributeId"), Label).Text.Trim.Length > 0 Then
                    blnControlTotal = CType(dgRow.FindControl("lblControlAttributeId"), Label).Text
                Else
                    blnControlTotal = False
                End If

            Else

                If CType(dgRow.FindControl("lblRequiredEdit"), Label).Text.Length > 0 And CType(dgRow.FindControl("lblRequiredEdit"), Label).Text = "1" Then
                    bolRequired = True
                Else
                    bolRequired = False
                End If

                intAttributeId = CType(dgRow.FindControl("ddlAttribute"), DropDownList).SelectedValue
                If CType(dgRow.FindControl("lblImageAttributeIdEdit"), Label).Text.Trim.Length > 0 Then
                    intImageAttributeId = CType(dgRow.FindControl("lblImageAttributeIdEdit"), Label).Text
                End If
                If CType(dgRow.FindControl("lblLengthEdit"), Label).Text.Trim.Length Then
                    intLength = CType(dgRow.FindControl("lblLengthEdit"), Label).Text
                Else
                    intLength = Constants.DEFAULT_ATTRIBUTE_LENGTH
                End If
                strAttributeDataType = CType(dgRow.FindControl("lblAttributeDataTypeEdit"), Label).Text
                strAttributeName = CType(dgRow.FindControl("ddlAttribute"), DropDownList).SelectedItem.Text
                strValueDescription = ""
                Select Case strAttributeDataType
                    Case "String", "Numeric", "Date"
                        strAttributeValue = CType(dgRow.FindControl("txtAttributeValue"), TextBox).Text
                    Case "ListView"
                        strAttributeValue = CType(dgRow.FindControl("ddlAttributeValue"), DropDownList).SelectedValue
                        strValueDescription = CType(dgRow.FindControl("ddlAttributeValue"), DropDownList).SelectedItem.Text
                    Case "CheckBox"
                        strAttributeValue = CType(dgRow.FindControl("chkBox"), CheckBox).Checked
                    Case Else
                        strAttributeValue = CType(dgRow.FindControl("txtAttributeValue"), TextBox).Text
                End Select
                strDataFormat = CType(dgRow.FindControl("lblDataFormatEdit"), Label).Text
                strListViewStyle = CType(dgRow.FindControl("lblListViewStyleEdit"), Label).Text
                Dim lblListValidate As Label = CType(dgRow.FindControl("lblListValidate"), Label)
                If lblListValidate.Text.Trim = "" Then
                    blnListValidate = False
                Else
                    blnListValidate = CType(dgRow.FindControl("lblListValidate"), Label).Text
                End If

                If CType(dgRow.FindControl("lblControlAttributeId"), Label).Text.Trim.Length > 0 Then
                    blnControlTotal = CType(dgRow.FindControl("lblControlAttributeId"), Label).Text
                Else
                    blnControlTotal = False
                End If

            End If

            ' Build the new row
            If KeepBlankValues Then

                drImageAttributes = rValue.ImageAttributes.NewRow
                drImageAttributes.AttributeDataType = strAttributeDataType
                drImageAttributes.AttributeId = intAttributeId
                drImageAttributes.AttributeName = strAttributeName
                drImageAttributes.AccessLevel = intAccessLevel
                drImageAttributes.ListValidate = blnListValidate
                drImageAttributes.ControlTotalAttributeID = blnControlTotal
                If strAttributeDataType = "Date" And Not Me.mbolRollback Then
                    If IsDate(strAttributeValue) = True Then
                        Dim datAttributeValue As DateTime = strAttributeValue
                        strAttributeValue = datAttributeValue.ToString("yyyy/MM/dd")
                    Else
                        strAttributeValue = strAttributeValue.ToString
                    End If
                End If
                drImageAttributes.AttributeValue = strAttributeValue
                drImageAttributes.DataFormat = strDataFormat
                If lblVersionId.Value.Length > 0 Then drImageAttributes.ImageAttributeId = intImageAttributeId
                drImageAttributes.Length = intLength
                drImageAttributes.ListViewStyle = strListViewStyle
                drImageAttributes.Required = bolRequired
                drImageAttributes.ValueDescription = strValueDescription
                rValue.ImageAttributes.Rows.Add(drImageAttributes)

            Else

                If strAttributeValue.Trim.Length > 0 Then
                    drImageAttributes = rValue.ImageAttributes.NewRow
                    drImageAttributes.AttributeDataType = strAttributeDataType
                    drImageAttributes.AttributeId = intAttributeId
                    drImageAttributes.AttributeName = strAttributeName
                    drImageAttributes.AccessLevel = intAccessLevel
                    drImageAttributes.ListValidate = blnListValidate
                    drImageAttributes.ControlTotalAttributeID = blnControlTotal
                    If strAttributeDataType = "Date" And Not Me.mbolRollback Then
                        If IsDate(strAttributeValue) = True Then
                            Dim datAttributeValue As DateTime = strAttributeValue
                            strAttributeValue = datAttributeValue.ToString("yyyy/MM/dd")
                        Else
                            strAttributeValue = strAttributeValue.ToString
                        End If
                    End If
                    drImageAttributes.AttributeValue = strAttributeValue
                    drImageAttributes.DataFormat = strDataFormat
                    If lblVersionId.Value.Length > 0 Then drImageAttributes.ImageAttributeId = intImageAttributeId
                    drImageAttributes.Length = intLength
                    drImageAttributes.ListViewStyle = strListViewStyle
                    drImageAttributes.Required = bolRequired
                    drImageAttributes.ValueDescription = strValueDescription
                    rValue.ImageAttributes.Rows.Add(drImageAttributes)

                End If
            End If
        Next

        Return rValue

    End Function

    Private Function UpdateImage(ByVal intVersionId As Integer, ByVal dsAttributes As dsImageAttributes, ByVal iDocumentID As Integer, Optional ByVal dtWorkflow As DataTable = Nothing, _
    Optional ByVal FolderId As Integer = 0) As Boolean

        If intVersionId = 0 Or dsAttributes Is Nothing Then
            Throw New Exception("Improper information was passed to properly update the image.")
            Return False
        End If

        Dim bolValue As Boolean = False

        Try
            '
            ' Load the data from the server
            '
            Dim dsVersion As Data.dsVersionControl = Session(intVersionId & "VersionControl")
            'Dim oVersionControl As New Accucentric.docAssist.Data.VersionControl(intVersionId, Me.mconSqlImage)
            'oVersionControl.VersionId = intVersionId
            '
            'Set the documentID in case it was changed

            dsVersion.Images.Rows(0).Item("DocumentID") = iDocumentID

            ' Get the datatables and setup the rows
            Dim drVersionAttribute As Data.dsVersionControl.ImageAttributesRow  'As data.Images.ImageAttributesRow
            Dim dtAttributes As dsImageAttributes.ImageAttributesDataTable = dsAttributes.ImageAttributes
            Dim drAttributes As dsImageAttributes.ImageAttributesRow
            dtAttributes.PrimaryKey = New DataColumn() {dtAttributes.ImageAttributeIdColumn}
            '
            ' Update the datagrid
            '
            For Each drVersionAttribute In dsVersion.ImageAttributes.Rows 'oVersionControl.ImageAttributes.Rows
                drAttributes = dtAttributes.Rows.Find(drVersionAttribute.ImageAttributeId)
                If drAttributes Is Nothing Then
                    '
                    ' delete the record from version control if it's no longer in the grid
                    drVersionAttribute.Delete()
                Else
                    '
                    ' check to see if the record values has changed, and update them
                    If drVersionAttribute.AttributeId <> drAttributes.AttributeId Then drVersionAttribute.AttributeId = drAttributes.AttributeId
                    If drVersionAttribute.AttributeValue <> drAttributes.AttributeValue Then drVersionAttribute.AttributeValue = drAttributes.AttributeValue
                End If
            Next

            '
            ' Get the new rows
            Dim dvAttributes As New DataView(dtAttributes, dtAttributes.ImageAttributeIdColumn.ColumnName & "< 0", dtAttributes.ImageAttributeIdColumn.ColumnName, DataViewRowState.CurrentRows)
            Dim dvrAttributes As DataRowView
            For Each dvrAttributes In dvAttributes
                drAttributes = dvrAttributes.Row
                drVersionAttribute = dsVersion.ImageAttributes.NewRow() 'oVersionControl.ImageAttributes.NewRow
                drVersionAttribute.AttributeId = drAttributes.AttributeId
                drVersionAttribute.ImageId = CType(dsVersion.Images.Rows(0), Data.dsVersionControl.ImagesRow).ImageID 'CType(oVersionControl.Images.Rows(0), data.Images.ImagesRow).ImageId
                drVersionAttribute.AttributeValue = drAttributes.AttributeValue
                dsVersion.ImageAttributes.Rows.Add(drVersionAttribute)
            Next
            dvrAttributes = Nothing
            dvAttributes.Dispose()
            '
            ' Save the changes back to the database
            Dim oSave As New Accucentric.docAssist.Data.ImageSave(Functions.GetImagesUserId(Me.mconSqlImage, Me.mobjUser), Me.mconSqlImage)
            bolValue = oSave.UpdateImage(dsVersion, Nothing, dtWorkflow, , intVersionId, FolderId)
            If bolValue Then
                lblVersionId.Value = CType(dsVersion.ImageVersion.Rows(dsVersion.ImageVersion.Rows.Count - 1), Data.dsVersionControl.ImageVersionRow).VersionID
                Me.mcookieSearch.VersionID = lblVersionId.Value
                Session(intVersionId & "VersionControl") = Accucentric.docAssist.Data.GetVersionControl(Me.mcookieSearch.VersionID, Me.mconSqlImage, Me.mobjUser.ImagesUserId)
            Else
                Session(intVersionId & "VersionControl") = Nothing
            End If
        Catch ex As Exception
            Throw ex
            bolValue = False
        End Try

        Return bolValue

    End Function

    Public Function DocumentSecurityLevel(ByVal intDocumentId As Integer) As Web.Security.SecurityLevel
        Return Functions.DocumentSecurityLevel(Me.mcookieSearch.VersionID, Me.mobjUser, Me.mconSqlImage)
    End Function

    Private Sub AssignRights()

        Select Case Me.mSecurityLevel
            Case Web.Security.Functions.SecurityLevel.Change
                'Me.mImageAttributes.Enabled = True
                If ddlDocuments.SelectedValue <> "0" Then lnkAdd.Visible = True
                If ddlDocuments.SelectedValue <> "0" Then btnRemove.Visible = True

                ddlDocuments.Visible = True
                'dgAttributes.Columns(0).Visible = True
                txtDocumentName.Visible = False

                txtTitle.Visible = True
                txtTags.Visible = True
                txtComments.Visible = True
                lblTitle.Visible = False
                lblTags.Visible = False
                lblComments.Visible = False

            Case Web.Security.Functions.SecurityLevel.Delete
                'TODO: Add script code to disable the save button or add the security to the IndexNavigation control
                'AttributeNavigation1.SaveButton.Visible = True
                If ddlDocuments.SelectedValue <> "0" Then lnkAdd.Visible = True
                If ddlDocuments.SelectedValue <> "0" Then btnRemove.Visible = True
                ddlDocuments.Visible = True
                'dgAttributes.Columns(0).Visible = True
                txtDocumentName.Visible = False

                txtTitle.Visible = True
                txtTags.Visible = True
                txtComments.Visible = True
                lblTitle.Visible = False
                lblTags.Visible = False
                lblComments.Visible = False

            Case Web.Security.Functions.SecurityLevel.Read

                txtTitle.Visible = False
                txtTags.Visible = False
                txtComments.Visible = False

                If lblTitle.Text.Trim.Length = 0 Then
                    titleRow.Visible = False
                Else
                    lblTitle.Visible = True
                End If

                If lblComments.Text.Trim.Length = 0 Then
                    descRow.Visible = False
                Else
                    lblComments.Visible = True
                End If

                If lblTags.Text.Trim.Length = 0 Then
                    tagRow.Visible = False
                Else
                    lblTags.Visible = True
                End If

                txtDocumentName.BorderStyle = BorderStyle.None
                txtFolder.BorderStyle = BorderStyle.None

                If txtFolder.Text.Trim.Length = 0 Then tblFolder.Visible = False

                ErrorRow.Visible = False

                btnOcr.Visible = False
                btnCapture2.Visible = False
                btnRemove.Visible = False

                'txtFolder.Width = "100%"
                ReadOnlyUser.Value = 1


        End Select

    End Sub

    Private Sub InitializeAttributeDropDown(ByVal intDocumentId As Integer, Optional ByVal e As DataGridItem = Nothing)

        Dim ddlAttribute As DropDownList
        Dim lblAttributeId As Label
        If e Is Nothing Then
            ddlAttribute = CType(dgAttributes.Items(dgAttributes.SelectedIndex).FindControl("ddlAttribute"), DropDownList)
            lblAttributeId = CType(dgAttributes.Items(dgAttributes.SelectedIndex).FindControl("lblAttributeId"), Label)
        Else
            ddlAttribute = CType(e.FindControl("ddlAttribute"), DropDownList)
            lblAttributeId = CType(e.FindControl("lblAttributeId"), Label)
        End If

        If (AddingNewRow And CType(e.FindControl("lblAttributeId"), Label).Text = "") Or (CInt(CType(e.FindControl("lblAccessLevel"), Label).Text) > 1) Then
            Dim dv As New DataView(dtAttributes)
            dv.RowFilter = "AccessLevel > 1"
            ddlAttribute.DataSource = dv
        Else
            ddlAttribute.DataSource = dtAttributes
        End If

        ddlAttribute.DataTextField = dtAttributes.AttributeName.ColumnName
        ddlAttribute.DataValueField = dtAttributes.AttributeId.ColumnName
        ddlAttribute.Visible = True
        ddlAttribute.DataBind()

        If e Is Nothing Then
            lblAttributeId.Text = ddlAttribute.SelectedItem.Value
        Else
            Dim li As ListItem = ddlAttribute.Items.FindByValue(lblAttributeId.Text)
            If Not li Is Nothing Then li.Selected = True
        End If

    End Sub

    Private Sub InitializeAttributeFields(ByVal intAttributeId As Integer, Optional ByVal e As DataGridItem = Nothing)

        Try

            Dim dgRow As DataGridItem
            dgRow = e

            Dim ddlAttribute As DropDownList = CType(dgRow.FindControl("ddlAttribute"), DropDownList)
            Dim lblAttributeDataType As Label = CType(dgRow.FindControl("lblAttributeDataType"), Label)
            Dim lblAttributeId As Label = CType(dgRow.FindControl("lblAttributeId"), Label)
            Dim lblDataFormat As Label = CType(dgRow.FindControl("lblDataFormat"), Label)
            Dim lblLength As Label = CType(dgRow.FindControl("lblLength"), Label)
            Dim lblRequired As Label = CType(dgRow.FindControl("lblRequired"), Label)
            Dim intLength As Label = CType(dgRow.FindControl("lblLength"), Label)
            Dim btnDelete As ImageButton = CType(dgRow.FindControl("btnDelete"), ImageButton)
            Dim txtAttributeValue As TextBox = CType(dgRow.FindControl("txtAttributeValue"), TextBox)
            Dim imgBrowse As System.Web.UI.WebControls.Image = CType(dgRow.FindControl("imgBrowse"), System.Web.UI.WebControls.Image)
            Dim lblListValidate As Label = CType(dgRow.FindControl("lblListValidate"), Label)
            Dim lblControlAttributeId As Label = CType(dgRow.FindControl("lblControlAttributeId"), Label)

            If intAttributeId > 0 Then
                Dim dtAttributes As New Data.Images.Attributes
                Dim cmdSql As New SqlClient.SqlCommand("SELECT * FROM Attributes WHERE AttributeId = @AttributeId", Me.mconSqlImage)
                cmdSql.Parameters.Add("@AttributeId", intAttributeId)
                Dim odb As New Data.Images.db
                odb.PopulateTable(dtAttributes, cmdSql)

                If dtAttributes.Rows.Count > 0 Then
                    Dim drAttribute As Data.Images.AttributesRow = dtAttributes.Rows(0)
                    lblAttributeDataType.Text = IIf(Not drAttribute.AttributeDataType Is Nothing, drAttribute.AttributeDataType, Constants.DEFAULT_ATTRIBUTE_DATATYPE)
                    lblDataFormat.Text = IIf(Not drAttribute.AttributeDataFormat Is Nothing, drAttribute.AttributeDataFormat, "")
                    lblLength.Text = IIf(Not drAttribute.Length = 0, drAttribute.Length, Constants.DEFAULT_ATTRIBUTE_LENGTH)

                    If lblDataFormat.Text = "ListView" Then
                        Dim dtDocumentAttributes As New Data.Images.DocumentAttribute
                        dtDocumentAttributes.Fill(ddlDocuments.SelectedValue, Me.mobjUser.ImagesUserId, True, Me.mconSqlImage)
                        Dim dv As New DataView(dtDocumentAttributes)
                        dv.RowFilter = "AttributeId='" & lblAttributeId.Text & "'"
                        lblListValidate.Text = dv(0)("ListValidate")
                    End If

                    If lblAttributeDataType.Text = "Numeric" Then
                        Dim dtCtrl As New Data.Images.DocumentAttribute
                        dtCtrl.Fill(ddlDocuments.SelectedValue, Me.mobjUser.ImagesUserId, False, Me.mconSqlImage)
                        Dim dvCtrl As New DataView(dtCtrl)
                        dvCtrl.RowFilter = "AttributeId='" & lblAttributeId.Text & "'"
                        lblControlAttributeId.Text = dvCtrl(0)("ControlTotalAttributeID")
                    Else
                        lblControlAttributeId.Text = "False"
                    End If

                End If

                imgBrowse.Visible = False
                txtAttributeValue.MaxLength = lblLength.Text

                'Set the controls to display
                Select Case lblAttributeDataType.Text
                    Case "CheckBox"
                        ddlAttribute.Visible = False
                        txtAttributeValue.Visible = False
                    Case "ListView"
                        txtAttributeValue.Visible = True
                        'ddlAttribute.Width = New System.Web.ui.WebControls.Unit("100%")
                        'txtAttributeValue.Width = New System.Web.ui.WebControls.Unit(115)
                        imgBrowse.Visible = True
                        imgBrowse.Attributes("onclick") = "browseAttribute('" & lblAttributeId.Text & "','" & txtAttributeValue.ClientID & "');"
                    Case "Date"
                        ddlAttribute.Visible = True
                        txtAttributeValue.Visible = True
                        'txtAttributeValue.Width = New UI.WebControls.Unit(140)
                    Case "String", "Numeric"
                        ddlAttribute.Visible = True
                        txtAttributeValue.Visible = True
                        'ddlAttribute.Width = New System.Web.ui.WebControls.Unit("100%")
                        'txtAttributeValue.Width = New System.Web.ui.WebControls.Unit(115)
                    Case Else
                        ddlAttribute.Visible = False
                        txtAttributeValue.Visible = True
                        txtAttributeValue.MaxLength = lblLength.Text
                End Select

            End If

        Catch ex As Exception

        Finally

            'ReturnControls.Add(dgAttributes)

        End Try

    End Sub

    Private Sub InitializeAttributeValueEdit(Optional ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs = Nothing)
        '
        ' Define controls
        '
        Dim btnShowCalendar As HtmlAnchor
        'Dim chkBox As CheckBox
        Dim ddlAttribute As DropDownList
        Dim ddlAttributeValue As DropDownList
        Dim lblAttributeId As Label
        Dim lblAttributeDataType As Label
        Dim lblDataFormat As Label
        Dim lblLength As Label
        Dim txtAttributeValue As TextBox
        Dim dgRow As DataGridItem
        '
        ' Load the controls
        '
        If Not e Is Nothing Then
            dgRow = e.Item
            selectedIndex.Value = e.Item.ItemIndex
        Else
            dgRow = dgAttributes.Items(dgAttributes.EditItemIndex)
            selectedIndex.Value = dgAttributes.EditItemIndex
            'Update screen
            'ReturnControls.Add(selectedIndex)
        End If

        btnShowCalendar = CType(dgRow.FindControl("lnkCal"), HtmlAnchor)
        'chkBox = CType(dgRow.FindControl("chkBox"), CheckBox)
        ddlAttribute = CType(dgRow.FindControl("ddlAttribute"), DropDownList)
        ddlAttributeValue = CType(dgRow.FindControl("ddlAttributeValue"), DropDownList)
        lblAttributeId = CType(dgRow.FindControl("lblAttributeId"), Label)
        lblAttributeDataType = CType(dgRow.FindControl("lblAttributeDataType"), Label)
        lblDataFormat = CType(dgRow.FindControl("lblDataFormat"), Label)
        lblLength = CType(dgRow.FindControl("lblLength"), Label)
        txtAttributeValue = CType(dgRow.FindControl("txtAttributeValue"), TextBox)
        Dim imgBrowse As System.Web.UI.WebControls.Image = CType(dgRow.FindControl("imgBrowse"), System.Web.UI.WebControls.Image)

        imgBrowse.Visible = False
        '
        ' Get the values
        '
        Dim intAttributeId
        If lblAttributeId.Text.Trim.Length > 0 Then
            intAttributeId = lblAttributeId.Text
            Dim liAttribute As ListItem
            For Each liAttribute In ddlAttribute.Items
                If intAttributeId = liAttribute.Value Then
                    liAttribute.Selected = True
                    Exit For
                End If
            Next
        Else
            lblAttributeId.Text = ddlAttribute.SelectedItem.Value
            intAttributeId = lblAttributeId.Text
        End If
        Dim strDataFormat As String = lblDataFormat.Text
        Dim strAttributeDataType As String = lblAttributeDataType.Text
        Dim intLength As Integer
        If lblLength.Text.Trim.Length > 0 Then
            intLength = CInt(lblLength.Text.Trim)
        Else
            intLength = Constants.DEFAULT_ATTRIBUTE_LENGTH
        End If

        imgBrowse.Visible = False
        txtAttributeValue.MaxLength = lblLength.Text

        'Set the controls to display
        Select Case strAttributeDataType
            Case "CheckBox"
                ddlAttribute.Visible = False
                txtAttributeValue.Visible = False
            Case "ListView"
                txtAttributeValue.Visible = True
                'ddlAttribute.Width = New System.Web.ui.WebControls.Unit("100%")
                'txtAttributeValue.Width = New System.Web.ui.WebControls.Unit(115)
                imgBrowse.Visible = True
                imgBrowse.Attributes("onclick") = "browseAttribute('" & lblAttributeId.Text & "','" & txtAttributeValue.ClientID & "');"
            Case "Date"
                ddlAttribute.Visible = True
                txtAttributeValue.Visible = True
                'txtAttributeValue.Width = New UI.WebControls.Unit(140)
            Case "String", "Numeric"
                ddlAttribute.Visible = True
                txtAttributeValue.Visible = True
                'ddlAttribute.Width = New System.Web.ui.WebControls.Unit("100%")
                'txtAttributeValue.Width = New System.Web.ui.WebControls.Unit(115)
            Case Else
                ddlAttribute.Visible = False
                txtAttributeValue.Visible = True
        End Select

        'ReturnControls.Add(dgAttributes)

    End Sub

    Private Sub LoadImage()

        Dim intVersionId As Integer = Me.mcookieSearch.VersionID

        'Load the data and save to the session
        Dim dsVersionControl As Accucentric.docAssist.Data.dsVersionControl = Accucentric.docAssist.Data.GetVersionControl(CInt(lblVersionId.Value), Me.mconSqlImage, Me.mobjUser.ImagesUserId)
        Session(intVersionId & "VersionControl") = dsVersionControl
        lblPageNo.Value = 1

        Dim intDocumentId As Integer = CType(dsVersionControl.Images.Rows(0), Accucentric.docAssist.Data.dsVersionControl.ImagesRow).DocumentID
        Me.mSecurityLevel = DocumentSecurityLevel(intDocumentId)

        ddlDocuments.SelectedValue = intDocumentId
        Session("NewDocumentId") = intDocumentId

        dtAttributes = New Data.Images.Attributes
        dtAttributes.Fill(intDocumentId, Data.Images.Attributes.AttributeParameterType.DocumentId, Me.mobjUser.ImagesUserId, Me.mconSqlImage)

        'Bind the data
        dgAttributes.DataSource = dsVersionControl.ImageAttributes
        dgAttributes.DataBind()

        If dgAttributes.Items.Count > 0 Then System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.Empty.ToString, "setTimeout('dataEntered = true;',300);", True)

        ' Select the document type
        Dim li As ListItem
        For Each li In ddlDocuments.Items
            If li.Value = intDocumentId Then
                li.Selected = True
                txtDocumentName.Text = li.Text
                lblDocumentId.Text = ddlDocuments.Items.IndexOf(li)
                Exit For
            End If
        Next

        If intDocumentId = 0 Then
            lnkAdd.Visible = False
            lnkAdd.Enabled = False
            btnCapture2.Enabled = False
            dgAttributes.DataSource = Nothing
            dgAttributes.DataBind()
            dgAttributes.Visible = False
            lnkAdd.Visible = False
            btnCapture2.Visible = False
            'ReturnControls.Add(lnkAdd)
            'ReturnControls.Add(btnCapture2)
        Else
            lnkAdd.Enabled = True
            btnCapture2.Enabled = True
        End If

        'Get the cabinet and folder id's
        Dim cmdSql As New SqlClient.SqlCommand("SELECT dl.FolderId FROM DocumentLocations dl WHERE DocumentId = @DocumentId", Me.mconSqlImage)
        cmdSql.Parameters.Add("@DocumentId", intDocumentId)
        If Not Me.mconSqlImage.State = ConnectionState.Open Then Me.mconSqlImage.Open()
        Dim intFolderId As Integer = cmdSql.ExecuteScalar
        cmdSql.CommandText = "SELECT CabinetId FROM CabinetFolders WHERE FolderId = @FolderId"
        cmdSql.Parameters.Clear()
        cmdSql.Parameters.Add("@FolderId", intFolderId)
        Dim intCabinetId As Integer = cmdSql.ExecuteScalar
        If Not Me.mconSqlImage.State = ConnectionState.Closed Then Me.mconSqlImage.Close()

        'ReturnControls.Add(lblPageNo)

    End Sub

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        ActivateFunction = "Activate" & System.Guid.NewGuid.ToString.Replace("-", "")

        Dim blnIntegrationEnabled As Boolean = True

        btnCapture2.Attributes("onclick") = "postback();"
        lnkAdd.Attributes("onclick") = "clearCookie(""Attributes"");"
        txtTitle.Attributes("onkeyup") = "ActivateSave();"
        txtComments.Attributes("onkeyup") = "ActivateSave();"
        txtTags.Attributes("onkeyup") = "ActivateSave();"
        txtFolder.Attributes("onkeypress") = "catchKey();"
        '        RegisterExcludeControl(btnCapture2)
        btnSave.Style.Item("display") = "none"
        lblAttributeError.Text = ""
        lblAttributeError.Visible = False
        selectedIndex.Value = -1
        btnSave.Attributes("onclick") = "trackAnnotations();document.all.lblAttributeError.innerHTML = '';"

        Me.mcookieSearch = New cookieSearch(Me)

        'Build the user object
        Try
            Me.mobjUser = BuildUserObject(Request.Cookies("docAssist"))
            Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
        Catch ex As Exception
            Response.Redirect("Default.aspx")
        End Try

        Dim intVersionId As Integer = Me.mcookieSearch.VersionID


        Try

            If Not Page.IsPostBack And Not Me.mobjUser Is Nothing Then

                '## CONTROLS VERSION ##
                Dim conSqlMaster As SqlClient.SqlConnection = Functions.BuildMasterConnection
                Dim cmdSql As New SqlClient.SqlCommand("acsGetVersionValue", conSqlMaster)
                cmdSql.Parameters.Add("@VersionType", "WebVersionObject")
                cmdSql.CommandTimeout = DEFAULT_COMMAND_TIMEOUT
                cmdSql.CommandType = CommandType.StoredProcedure
                Dim intVersion As Integer = cmdSql.ExecuteScalar
                conSqlMaster.Close()
                conSqlMaster.Dispose()
                ObjectCheck.Value = intVersion

                hostname.Value = Request.Url.AbsoluteUri.Substring(0, Request.Url.AbsoluteUri.Length - Request.Url.AbsolutePath.Length)
#If DEBUG Then
                hostname.Value = hostname.Value & "/docAssistWeb1"
#End If
                hiddenTripCount.Value = 1
                hiddenDirty.Value = 0

                Dim dtCabinets As New Data.Images.Cabinets

                Dim sqlCon As SqlClient.SqlConnection = Me.mconSqlImage
                If sqlCon.State <> ConnectionState.Open Then sqlCon.Open()

                'Load Title, Description, Tags
                Dim objAttachments As New Data.Images.FileAttachments(sqlCon, False)
                Dim ds As New DataSet
                ds = objAttachments.HeaderGet(Me.mcookieSearch.VersionID)

                Dim dt As DataTable = ds.Tables(0)

                If dt.Rows.Count > 0 Then
                    txtTitle.Text = Server.UrlDecode(dt.Rows(0)("DocumentTitle"))
                    lblTitle.Text = Server.UrlDecode(dt.Rows(0)("DocumentTitle"))
                    txtComments.Text = Server.UrlDecode(dt.Rows(0)("Description"))
                    lblComments.Text = Server.UrlDecode(dt.Rows(0)("Description"))
                    txtTags.Text = Server.UrlDecode(dt.Rows(0)("Tags"))
                    lblTags.Text = Server.UrlDecode(dt.Rows(0)("Tags"))
                End If

                If ds.Tables(1).Rows.Count > 0 Then
                    If ds.Tables(1).Rows(0)("FolderId") > 0 Then
                        txtFolder.Text = CStr(ds.Tables(1).Rows(0)("FolderName")).Replace("&#43;", "+")
                        FolderId.Value = ds.Tables(1).Rows(0)("FolderId")
                    Else
                        txtFolder.Text = "<None>"
                        FolderId.Value = 0
                    End If
                End If
                txtFolder.Attributes("onmouseover") = "Tip(" & txtFolder.ClientID & ".value);"

                Dim oDb As New Data.Images.db
                Dim sqlCmd As New SqlClient.SqlCommand
                sqlCmd.CommandType = CommandType.StoredProcedure

                'Retrieve the documents
                Dim dtDocuments As New Data.Images.Documents
                sqlCmd = New SqlClient.SqlCommand("acsDocumentsByCabinetFolder", sqlCon)
                sqlCmd.CommandType = CommandType.StoredProcedure
                sqlCmd.Parameters.Add("@UserName", Me.mobjUser.UserId)
                sqlCmd.Parameters.Add("@CabinetId", "0")
                sqlCmd.Parameters.Add("@FolderId", "0")

                Try
                    If Not IsNothing(Request.Cookies("Workflow")("Enabled")) Then
                        'Workflow only user, no access to any documents but has to get into this specific document
                        If Request.Cookies("Workflow")("Enabled") = True Then
                            sqlCmd.Parameters.Add("@VersionId", Me.mcookieSearch.VersionID)
                            Response.Cookies("Workflow")("Enabled") = Nothing
                        End If
                    End If
                Catch ex As Exception

                End Try

                oDb.PopulateTable(dtDocuments, sqlCmd)

                ddlDocuments.DataSource = dtDocuments
                ddlDocuments.DataTextField = dtDocuments.DocumentName.ColumnName
                ddlDocuments.DataValueField = dtDocuments.DocumentId.ColumnName
                ddlDocuments.DataBind()
                lblDocumentId.Text = ddlDocuments.SelectedIndex

                'Build Attributes
                'Dim dsAttributes As New dsImageAttributes
                'dgAttributes.DataSource = dsAttributes.ImageAttributes
                'dgAttributes.DataBind()

                If Me.mcookieSearch.VersionID > 0 Then lblVersionId.Value = Me.mcookieSearch.VersionID

                If lblVersionId.Value.Trim.Length > 0 Then
                    lnkAdd.Visible = False
                    btnRemove.Visible = False
                    ddlDocuments.Visible = False
                    txtDocumentName.Visible = True
                    LoadImage()
                    AssignRights()
                ElseIf lblFileNameResult.Value.Trim.Length > 0 Then
                    If ddlDocuments.SelectedValue <> "0" Then lnkAdd.Visible = True
                    If ddlDocuments.SelectedValue <> "0" Then btnRemove.Visible = True
                    ddlDocuments.Visible = True
                    txtDocumentName.Visible = False
                End If

                InitOCRRemoveButtons()

                'Column widths
                For Each dc As DataGridColumn In dgAttributes.Columns
                    If dc.Visible Then
                        dc.HeaderStyle.Width = New System.Web.UI.WebControls.Unit("50%")
                        dc.ItemStyle.Width = New System.Web.UI.WebControls.Unit("50%")
                    End If
                Next

                'Account level document type/attribute setting
                If Functions.ModuleSetting(Functions.ModuleCode.DOCUMENT_TYPES_ATTRIBUTES, Me.mobjUser) = False Then
                    'Account flag turned off
                    ddlDocuments.Visible = False
                    dgAttributes.Visible = False
                    dgAttributes.DataSource = Nothing
                    dgAttributes.DataBind()
                    lnkAdd.Visible = False
                    btnRemove.Visible = False
                    Label3.Visible = False
                    blnIntegrationEnabled = False
                End If

                blnIntegrationEnabled = Functions.ModuleSetting(Functions.ModuleCode.APP_INTEGRATION, Me.mobjUser)

            Else
                If Not Me.mobjUser Is Nothing Then
                    hiddenTripCount.Value = CInt(hiddenTripCount.Value) + 1
                End If
            End If

            ''Account level distribution setting
            'If Functions.ModuleSetting(Functions.ModuleCode.DISTRIBUTION, Me.mobjUser) = False Then
            '    imgDistribution.Visible = False
            'Else

            '    If Not Page.IsPostBack Then

            '        Dim objDist As New Accucentric.docAssist.Data.Images.Distribution(Me.mconSqlImage, False)
            '        Dim dtDistribution As New DataTable
            '        dtDistribution = objDist.GetDistributionDetail(Me.mcookieSearch.VersionID)

            '        Dim totalAmount As Double

            '        For Each dr As DataRow In dtDistribution.Rows
            '            totalAmount = totalAmount + dr("Amount")
            '        Next

            '        Session("DistributionTotal" & Me.mcookieSearch.VersionID) = Functions.FormatNumber(totalAmount)

            '    End If

            '    imgDistribution.Visible = True
            '    imgDistribution.Attributes("onclick") = "showDistributionDialog(document.all.ddlDocuments.value + ',' + document.all.FolderId.value);"
            'End If
            ''ReturnControls.Add(imgDistribution)

        Catch ex As Exception

            If ex.Message = "Version has been deleted." Then
                System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.Empty.ToString, "parent.window.location = 'VersionDeleted.aspx';", True)
                Exit Sub
            End If

        End Try

        Me.mSecurityLevel = DocumentSecurityLevel(ddlDocuments.SelectedValue)

        If Me.mSecurityLevel = Web.Security.Functions.SecurityLevel.Read Or Me.mSecurityLevel = Web.Security.Functions.SecurityLevel.NoAccess Then
            ClearFolder.Visible = False
            ChooseFolder.Visible = False
        End If

        If lblImageId.Text = "" Then
            Dim dsVersionControl As Accucentric.docAssist.Data.dsVersionControl
            dsVersionControl = Session(intVersionId & "VersionControl")
            lblImageId.Text = CType(dsVersionControl.Images.Rows(0), Accucentric.docAssist.Data.dsVersionControl.ImagesRow).ImageID
        End If

        'Check if user has access to at least one attribute

        If IsNothing(dtAttributes) Then
            dtAttributes = New Data.Images.Attributes
            dtAttributes.Fill(ddlDocuments.SelectedValue, Data.Images.Attributes.AttributeParameterType.DocumentId, Me.mobjUser.ImagesUserId, Me.mconSqlImage)
        End If

        Dim dv As New DataView(dtAttributes)
        dv.RowFilter = "AccessLevel > 1"

        If dv.Count = 0 Then
            lnkAdd.Visible = False
            btnRemove.Visible = False
            btnCapture2.Visible = False
            btnOcr.Visible = False
            'ReturnControls.Add(btnOcr)
            'ReturnControls.Add(btnCapture2)
            'ReturnControls.Add(lnkAdd)
            'ReturnControls.Add(btnRemove)
        End If

        If Not Page.IsPostBack Then RefreshGLDistribution()

        If Page.IsPostBack = False Then
            'Read access users cannot capture from the accounting application.
            If Me.mSecurityLevel = Web.Security.Functions.SecurityLevel.Read Or Me.mSecurityLevel = Web.Security.Functions.SecurityLevel.NoAccess Then
                btnCapture2.Visible = False
            Else
                'Integration
                Try

                    If Not Me.mconSqlImage.State = ConnectionState.Open Then
                        Me.mconSqlImage.Open()
                    End If

                    Dim sqlCommand As New SqlClient.SqlCommand("acsIntegrationCaptureData", Me.mconSqlImage)
                    sqlCommand.CommandType = CommandType.StoredProcedure
                    sqlCommand.Parameters.Add("@VersionID", Me.mcookieSearch.VersionID)
                    sqlCommand.Parameters.Add("@UserId", Me.mobjUser.ImagesUserId)

                    Dim daSql As New SqlClient.SqlDataAdapter
                    daSql.SelectCommand = sqlCommand

                    Dim dsIntegration As New DataSet
                    daSql.Fill(dsIntegration)

                    If dsIntegration.Tables(0).Rows.Count = 0 Then
                        btnCapture2.Visible = False
                    Else
                        btnCapture2.Visible = True
                        'Parse out the integration 'map'

                        Dim charField As Char = "|"
                        Dim charRow As Char = "�"

                        Dim sbHeader As New System.Text.StringBuilder
                        Dim dr As DataRow

                        Dim intHeaderCount As Integer = 0

                        'Header
                        For Each dr In dsIntegration.Tables(0).Rows

                            CaptureType.Value = dr("AttributeCaptureMode")

                            If dr("FolderMap") = True Then
                                FolderMap.Value = "1"
                                FormID.Value = dr("FormID")
                                DocumentID.Value = dr("DocumentID")
                            Else
                                FolderMap.Value = "0"
                            End If

                            'ReturnControls.Add(FolderMap)
                            'ReturnControls.Add(FormID)
                            'ReturnControls.Add(DocumentID)

                            intHeaderCount += 1
                            sbHeader.Append(dr("IntegrationID") & charField)
                            btnCapture2.ToolTip = "Capture " & dr("PublicName")
                            ApplicationName.Value = CStr(dr("PublicName")).Replace(",", "")
                            'ReturnControls.Add(ApplicationName)
                            sbHeader.Append(dr("PublicName") & charField)
                            sbHeader.Append(dr("WIndowName") & charField)
                            sbHeader.Append(dr("WindowClass") & charField)
                            sbHeader.Append(dr("Top") & charField)
                            sbHeader.Append(dr("Left") & charField)
                            sbHeader.Append(dr("Width") & charField)
                            sbHeader.Append(dr("Height") & charField)
                            sbHeader.Append(dr("Action") & charField)
                            sbHeader.Append(dr("ActionClickX") & charField)
                            sbHeader.Append(dr("ActionClickY") & charField)
                            sbHeader.Append(dr("ActionSendKey"))
                            If intHeaderCount < dsIntegration.Tables(0).Rows.Count Then sbHeader.Append(charRow)
                        Next

                        Dim sbDetail As New System.Text.StringBuilder
                        Dim intDetailCount As Integer = 0

                        'Detail
                        For Each dr In dsIntegration.Tables(1).Rows
                            intDetailCount += 1
                            sbDetail.Append(dr("AttributeID") & charField)
                            sbDetail.Append(dr("Expression") & charField)
                            'sbDetail.Append(dr("IntegrationID") & charField)
                            sbDetail.Append(dr("PosTop") & charField)
                            sbDetail.Append(dr("PosLeft") & charField)
                            sbDetail.Append(dr("Width") & charField)
                            sbDetail.Append(dr("Height"))
                            If intDetailCount < dsIntegration.Tables(1).Rows.Count Then sbDetail.Append(charRow)
                        Next

                        txtHeader.Text = sbHeader.ToString
                        txtDetail.Text = sbDetail.ToString

                    End If

                    If blnIntegrationEnabled = False Then
                        btnCapture2.Visible = False
                        btnCapture2.Enabled = False
                    End If

                Catch ex As Exception

                    Functions.LogException(mobjUser.UserId.ToString, mobjUser.AccountId.ToString, "", ex.Message, ex.StackTrace, Server.MachineName, "", Request.UrlReferrer.ToString, Functions.BuildMasterConnection, True)

                End Try

            End If

        End If

    End Sub

    Private Sub ddlDocuments_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlDocuments.SelectedIndexChanged

        Try

            'Check if active workflow is going on before allowing change of document type
            'Select Case CheckWorkflow(Me.mobjUser.ImagesUserId, Me.mcookieSearch.VersionID, Me.mobjUser, , )
            '    Case Functions.WorkflowMode.AUTHORIZED_SUBMITTER_NOT_SUBMITTED, Functions.WorkflowMode.NO_ACCESS
            '        'OK
            '    Case Else
            '        lblAttributeError.Text = "<BR>Unable to change document type. Workflow is active on this document."
            '        lblAttributeError.Visible = True
            '        ddlDocuments.SelectedIndex = CInt(lblDocumentId.Text)
            '        'ReturnControls.Add(lblAttributeError)
            '        Exit Sub
            'End Select

            InitializeDocumentAttributes(ddlDocuments.SelectedItem.Value)

            InitOCRRemoveButtons()

            If ddlDocuments.SelectedValue = 0 Then
                lnkAdd.Enabled = False
                btnCapture2.Enabled = False
                dgAttributes.Visible = False
                lnkAdd.Visible = False
                btnRemove.Visible = False
                btnCapture2.Visible = False
                'ReturnControls.Add(lnkAdd)
                'ReturnControls.Add(btnCapture2)
                'ReturnScripts.Add("parent.hideWorkflowPane();")
                RefreshGLDistribution()
                'Exit Sub
            Else
                lnkAdd.Visible = True
                btnCapture2.Visible = True
                lnkAdd.Enabled = True
                btnRemove.Visible = True
                btnCapture2.Enabled = True
                dgAttributes.Visible = True
                'ReturnControls.Add(lnkAdd)
                'ReturnControls.Add(btnCapture2)
            End If

            'Check if the new document type should trigger the workflow submit pane
            Dim routeid As Integer
            Dim mode As WorkflowMode = CheckWorkflow(Me.mobjUser.ImagesUserId, Me.mcookieSearch.VersionID, Me.mobjUser, routeid, ddlDocuments.SelectedValue)
            Select Case mode
                Case Functions.WorkflowMode.AUTHORIZED_SUBMITTER_NOT_SUBMITTED, Functions.WorkflowMode.ACTION_ACCESS_REVIEWER_PENDING, Functions.WorkflowMode.AUTHORIZED_SUBMITTER_RESUBMIT
                    System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.Empty.ToString, "parent.showWorkflowPane(" & CInt(mode) & "," & routeid & ");", True)
                Case Else
                    System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.Empty.ToString, "parent.hideWorkflowPane();", True)
            End Select

            lblDocumentId.Text = ddlDocuments.SelectedIndex

            If dgAttributes.EditItemIndex >= 0 Then
                Me.mSecurityLevel = DocumentSecurityLevel(ddlDocuments.SelectedValue)
                InitializeAttributeDropDown(ddlDocuments.SelectedValue)
                InitializeAttributeFields(CInt(CType(dgAttributes.Items(dgAttributes.EditItemIndex).FindControl("lblAttributeIdEdit"), Label).Text))
                InitializeAttributeValueEdit()
                selectedIndex.Value = dgAttributes.EditItemIndex
            End If

            RefreshIntegrationMap(ddlDocuments.SelectedValue)
            RefreshGLDistribution()

            Session("NewDocumentId") = ddlDocuments.SelectedValue
            'ReturnScripts.Add("alert('load');loadDistribution();")


        Catch ex As Exception

            lblAttributeError.Text = "<BR>An error has occured. Please try again."
            lblAttributeError.Visible = True

        Finally

            System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.Empty.ToString, "ActivateSave();", True)

        End Try

    End Sub

    Private Sub dgAttributes_ItemDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgAttributes.ItemDataBound

        Dim lblAttributeName As Label = e.Item.FindControl("lblAttributeName")
        Dim lblAttributeValue As Label = e.Item.FindControl("lblAttributeValue")
        Dim lblAttributeId As Label = e.Item.FindControl("lblAttributeId")
        Dim txtAttributeValue As TextBox = e.Item.FindControl("txtAttributeValue")
        Dim ddlAttribute As DropDownList = e.Item.FindControl("ddlAttribute")

        Try

            If Not IsNothing(txtAttributeValue) Then

                txtAttributeValue.Attributes("onkeypress") = "ActivateSave();"
                ddlAttribute.Attributes("onchange") = "ActivateSave();"

                txtAttributeValue.Attributes("onfocus") = "document.getElementById('ActiveRow').value = '" & e.Item.ItemIndex & "';" & ActivateFunction & "('" & e.Item.ItemIndex & "');"
                ddlAttribute.Attributes("onfocus") = "document.getElementById('ActiveRow').value = '" & e.Item.ItemIndex & "';" & ActivateFunction & "('" & e.Item.ItemIndex & "');"
                e.Item.Attributes("onclick") = "document.getElementById('ActiveRow').value = '" & e.Item.ItemIndex & "';" & ActivateFunction & "('" & e.Item.ItemIndex & "');"

                Dim intDocumentId As Integer = ddlDocuments.SelectedValue

                InitializeAttributeDropDown(intDocumentId, e.Item)
                InitializeAttributeValueEdit(e)

                ddlAttribute.SelectedValue = lblAttributeId.Text
                ddlAttribute.AutoPostBack = True

                Select Case Me.mSecurityLevel
                    Case Web.Security.Functions.SecurityLevel.NoAccess
                        lblAttributeName.Visible = True
                        lblAttributeValue.Visible = True
                        ddlAttribute.Visible = False
                        txtAttributeValue.Visible = False
                    Case Else
                        lblAttributeName.Visible = False
                        ddlAttribute.Visible = True
                End Select

                Dim lblAccessLevel As Label = e.Item.FindControl("lblAccessLevel")
                Dim lblAttributeDataType As Label = CType(e.Item.FindControl("lblAttributeDataType"), Label)

                'Set the controls to display
                Select Case lblAttributeDataType.Text
                    Case "ListView"

                        Dim imgBrowse As System.Web.UI.WebControls.Image = CType(e.Item.FindControl("imgBrowse"), System.Web.UI.WebControls.Image)

                        Select Case CType(lblAccessLevel.Text, AttributeAccess)
                            Case AttributeAccess.NONE
                                e.Item.Attributes("style") = "DISPLAY: none"
                            Case AttributeAccess.READ_ONLY
                                txtAttributeValue.Enabled = False
                                ddlAttribute.Enabled = False
                                imgBrowse.Enabled = False
                            Case AttributeAccess.CHANGE
                                txtAttributeValue.Enabled = True
                                ddlAttribute.Enabled = True
                                imgBrowse.Enabled = True
                            Case AttributeAccess.DELETE
                                txtAttributeValue.Enabled = True
                                ddlAttribute.Enabled = True
                                imgBrowse.Enabled = True
                        End Select

                    Case Else

                        Select Case CType(lblAccessLevel.Text, AttributeAccess)
                            Case AttributeAccess.NONE
                                e.Item.Attributes("style") = "DISPLAY: none"
                            Case AttributeAccess.READ_ONLY
                                txtAttributeValue.Enabled = False
                                ddlAttribute.Enabled = False
                            Case AttributeAccess.CHANGE
                                txtAttributeValue.Enabled = True
                                ddlAttribute.Enabled = True
                            Case AttributeAccess.DELETE
                                txtAttributeValue.Enabled = True
                                ddlAttribute.Enabled = True
                        End Select

                End Select

            End If

        Catch ex As Exception

        Finally

            'ReturnControls.Add(ddlAttribute)
            'ReturnControls.Add(lblAttributeName)
            'ReturnControls.Add(txtAttributeValue)
            'ReturnControls.Add(lblAttributeValue)

        End Try

    End Sub

    Private Sub lnkAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles lnkAdd.Click

        dtAttributes.Fill(ddlDocuments.SelectedValue, Data.Images.Attributes.AttributeParameterType.DocumentId, Me.mobjUser.ImagesUserId, Me.mconSqlImage)

        'Validate last row if they focused out of it rather than commiting it.
        If dgAttributes.EditItemIndex <> -1 Then
            If ValidateAttribute(dgAttributes.EditItemIndex) = False Then
                dgAttributes.SelectedIndex = dgAttributes.EditItemIndex
                'ReturnControls.Add(dgAttributes)
                Exit Sub
            End If
        End If

        ' Add the new row and rebind
        Dim ds As dsImageAttributes
        If dgAttributes.Items.Count > 0 Then
            ds = ReverseBindAttributes(True)
        Else
            ds = New dsImageAttributes
        End If

        'Add the remaining attributes if we are missing any, if not just add a new row
        Dim dt As New DataTable
        dt = Functions.GetAttributesByDocumentId(ddlDocuments.SelectedValue, Me.mobjUser.ImagesUserId, Me.mconSqlImage, False)

        'Exclude current attributes
        Dim sb As New System.Text.StringBuilder
        If ds.Tables(0).Rows.Count > 0 Then
            sb.Append("AttributeId NOT IN (")
            Dim counter As Integer = 1
            For Each dr As DataRow In ds.Tables(0).Rows
                sb.Append(dr("AttributeId"))
                If counter < ds.Tables(0).Rows.Count Then sb.Append(", ")
                counter += 1
            Next
            sb.Append(")")
        End If

        Dim dvNewAttributes As New DataView(dt, sb.ToString, Nothing, DataViewRowState.Unchanged)
        Dim dtNew As dsImageAttributes.ImageAttributesDataTable = ds.ImageAttributes

        If dvNewAttributes.Count > 0 Then
            For count As Integer = 0 To dvNewAttributes.Count - 1

                'Dim dv As New DataView(dt)
                'dv.RowFilter = "AttributeId='" & "test" & "'"

                Dim dr As dsImageAttributes.ImageAttributesRow
                dr = dtNew.NewRow
                dr.AccessLevel = 3
                dr.AttributeId = dvNewAttributes(count)("AttributeId")
                dr.AttributeDataType = dvNewAttributes(count)("AttributeDataType")

                If dvNewAttributes(count)("DataFormat") Is System.DBNull.Value Then
                    dr.DataFormat = ""
                Else
                    dr.DataFormat = dvNewAttributes(count)("DataFormat")
                End If

                If dvNewAttributes(count)("Length") Is System.DBNull.Value Then
                    dr.Length = DEFAULT_ATTRIBUTE_LENGTH
                Else
                    dr.Length = dvNewAttributes(count)("Length")
                End If

                dr.ListValidate = dvNewAttributes(count)("ListValidate")
                dr.ControlTotalAttributeID = dvNewAttributes(count)("ControlTotalAttributeID")
                dtNew.Rows.Add(dr)
            Next
        Else
            Dim dr As dsImageAttributes.ImageAttributesRow
            dr = dtNew.NewRow
            dr.AccessLevel = 3
            dr.ControlTotalAttributeID = False
            dtNew.Rows.Add(dr)
        End If

        AddingNewRow = True
        dgAttributes.DataSource = ds
        dgAttributes.DataMember = ds.ImageAttributes.TableName
        dgAttributes.DataBind()

        selectedIndex.Value = dgAttributes.EditItemIndex

        'Initialize new row
        Dim ddlAttribute As DropDownList = dgAttributes.Items(dgAttributes.Items.Count - 1).FindControl("ddlAttribute")
        Dim lblAttributeName As Label = dgAttributes.Items(dgAttributes.Items.Count - 1).FindControl("lblAttributeName")
        Dim lblAttributeId As Label = dgAttributes.Items(dgAttributes.Items.Count - 1).FindControl("lblAttributeId")

        ddlAttribute.Visible = True
        lblAttributeName.Visible = False

        InitializeAttributeDropDown(ddlDocuments.SelectedValue, CType(dgAttributes.Items(dgAttributes.Items.Count - 1), DataGridItem))

        lblAttributeId.Text = ddlAttribute.SelectedItem.Value
        InitializeAttributeFields(CInt(lblAttributeId.Text), CType(dgAttributes.Items(dgAttributes.Items.Count - 1), DataGridItem))
        AddingNewRow = False

        InitOCRRemoveButtons()
        RefreshGLDistribution()

        System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.Empty.ToString, ActivateFunction & "('" & dgAttributes.Items.Count - 1 & "');setTimeout('document.all." & dgAttributes.Items(dgAttributes.Items.Count - 1).FindControl("ddlAttribute").ClientID & ".focus();',300);clearCookie('Attributes');ActivateSave();setAttributesHeight();", True)

    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click

        If FolderId.Value = "" Then FolderId.Value = "0"

        Dim strVersionId As String = Me.mcookieSearch.VersionID
        Dim dtWorkflow As New DataTable
        Dim blnWorkflowActive As Boolean = False

        Try

            'Save distribution
            Dim dtDist As New DataTable
            dtDist = Session("Distribution" & Me.mcookieSearch.VersionID)

            'Check if cached results need to be updated
            If Session("FolderId") <> FolderId.Value Then
                Functions.UpdateCachedResults(lblImageId.Text, Me)
            End If

            'If Functions.ModuleSetting(Functions.ModuleCode.DISTRIBUTION, Me.mobjUser) Then

            '    Dim objDist As New Accucentric.docAssist.Data.Images.Distribution(Me.mconSqlImage, False)
            '    Dim dt As New DataTable
            '    dt = objDist.DistributionConfiguration(ddlDocuments.SelectedValue)

            '    If dt.Rows.Count > 0 Then

            '        'Check if a control attribute is present
            '        Dim blnControlFound As Boolean = False
            '        For Each dgi As DataGridItem In dgAttributes.Items
            '            Dim lblControlAttributeId As Label = dgi.FindControl("lblControlAttributeId")
            '            If Not IsNothing(lblControlAttributeId) Then
            '                If lblControlAttributeId.Text = "True" Or lblControlAttributeId.Text = "1" Then
            '                    blnControlFound = True
            '                    Exit For
            '                End If
            '            End If
            '        Next

            '        If dt.Rows(0)("ControlTotalEnabled") Then
            '            If dt.Rows(0)("ControlTotalUpdateAttribute") = False And blnControlFound = False Then
            '                lblAttributeError.Text = "Distribution total does not equal control total. "
            '                lblAttributeError.Visible = True
            '                'ReturnControls.Add(lblAttributeError)
            '                Exit Sub

            '            Else
            '                'Check if total matches

            '                For Each dgi As DataGridItem In dgAttributes.Items
            '                    Dim lblControlAttributeId As Label = dgi.FindControl("lblControlAttributeId")
            '                    If Not IsNothing(lblControlAttributeId) Then
            '                        If lblControlAttributeId.Text = "True" Or lblControlAttributeId.Text = "1" Then

            '                            If Functions.ModuleSetting(Functions.ModuleCode.DISTRIBUTION, Me.mobjUser) Then
            '                                'Attribute found, enforce totaling
            '                                Dim txtAttributeValue As TextBox = dgi.FindControl("txtAttributeValue")

            '                                If txtAttributeValue.Text.Trim = "" Then
            '                                    lblAttributeError.Text = "Distribution total does not equal control total. "
            '                                    lblAttributeError.Visible = True
            '                                    'ReturnControls.Add(lblAttributeError)
            '                                    Exit Sub
            '                                End If

            '                                If FormatNumber(CDbl(txtAttributeValue.Text)) <> Session("DistributionTotal" & Me.mcookieSearch.VersionID) Then
            '                                    'Mismatch
            '                                    lblAttributeError.Text = "Distribution total does not equal control total. "
            '                                    lblAttributeError.Visible = True
            '                                    'ReturnControls.Add(lblAttributeError)
            '                                    Exit Sub
            '                                End If
            '                                Exit For
            '                            End If
            '                        End If
            '                    End If
            '                Next

            '            End If

            '        End If

            '    End If

            'End If

            'Account setting, document types and attributes
            If Functions.ModuleSetting(Functions.ModuleCode.DOCUMENT_TYPES_ATTRIBUTES, Me.mobjUser) Then

                'A document must have one of the following: type, tag, title, comment
                If CType(Session("FileType"), Functions.FileType) <> Functions.FileType.ATTACHMENT Then
                    If ddlDocuments.SelectedValue = 0 And txtTitle.Text.Trim.Length = 0 And txtComments.Text.Trim.Length = 0 And txtTags.Text.Trim.Length = 0 Then
                        lblAttributeError.Text = "You must assign a document type, Title, Tag, or Description."
                        lblAttributeError.Visible = True
                        'ReturnControls.Add(lblAttributeError)
                        Exit Sub
                    End If
                End If

            Else

                'A document must have a folder
                If CType(Session("FileType"), Functions.FileType) <> Functions.FileType.ATTACHMENT Then
                    If txtFolder.Text.Trim.Length = 0 Then
                        lblAttributeError.Text = "You must choose a folder to save this document in."
                        lblAttributeError.Visible = True
                        'ReturnControls.Add(lblAttributeError)
                        Exit Sub
                    End If
                End If

            End If

            Dim ds As dsImageAttributes
            If dgAttributes.Items.Count > 0 Then
                ds = ReverseBindAttributes()
            Else
                ds = New dsImageAttributes
            End If

            'Add new row 
            dgAttributes.DataSource = ds
            dgAttributes.DataMember = ds.ImageAttributes.TableName
            dgAttributes.DataBind()


            Dim dcActionType As DataColumn = New DataColumn("ActionType", GetType(System.String), Nothing, MappingType.Element)
            Dim dcWorkflowID As DataColumn = New DataColumn("WorkflowID", GetType(System.Int32), Nothing, MappingType.Element)
            Dim dcQueueID As DataColumn = New DataColumn("QueueID", GetType(System.Int32), Nothing, MappingType.Element)
            Dim dcStatusID As DataColumn = New DataColumn("StatusID", GetType(System.Int32), Nothing, MappingType.Element)
            Dim dcSubmittedUserID As DataColumn = New DataColumn("SubmittedUserID", GetType(System.Int32), Nothing, MappingType.Element)
            Dim dcUrgent As DataColumn = New DataColumn("Urgent", GetType(System.Int32), Nothing, MappingType.Element)
            Dim dcVersionID As DataColumn = New DataColumn("VersionID", GetType(System.Int32), Nothing, MappingType.Element)
            Dim dcNotes As DataColumn = New DataColumn("Notes", GetType(System.String), Nothing, MappingType.Element)
            Dim dcRouteId As DataColumn = New DataColumn("RouteId", GetType(System.Int32), Nothing, MappingType.Element)

            dtWorkflow.Columns.Add(dcActionType)
            dtWorkflow.Columns.Add(dcWorkflowID)
            dtWorkflow.Columns.Add(dcQueueID)
            dtWorkflow.Columns.Add(dcStatusID)
            dtWorkflow.Columns.Add(dcSubmittedUserID)
            dtWorkflow.Columns.Add(dcUrgent)
            dtWorkflow.Columns.Add(dcVersionID)
            dtWorkflow.Columns.Add(dcNotes)
            dtWorkflow.Columns.Add(dcRouteId)

            Dim intmode As Integer = 0 'Mode.Value

            If Not IsNothing(Session(strVersionId & "Mode")) Then intmode = CInt(Session(strVersionId & "Mode"))

            If intmode = Functions.WorkflowMode.AUTHORIZED_SUBMITTER_NOT_SUBMITTED Or Functions.WorkflowMode.ACTION_ACCESS_REVIEWER_PENDING Then
                Select Case intmode

                    Case Functions.WorkflowMode.AUTHORIZED_SUBMITTER_NOT_SUBMITTED
                        blnWorkflowActive = True

                        Try

                            If Not IsNothing(Session(strVersionId & "WorkflowId")) Then
                                If CStr(Session(strVersionId & "WorkflowId")).Trim.Length = 0 Or Session(strVersionId & "WorkflowId") = "0" Then
                                    'No workflow selected.

                                Else
                                    Dim intWorkflowId As Integer = Session(strVersionId & "WorkflowId")
                                    Dim intQueueId As Integer = Session(strVersionId & "QueueId")
                                    Dim strWorkflowNotes As String = Session(strVersionId & "Notes")
                                    Dim intUrgent As Integer = Session(strVersionId & "Urgent")
                                    Dim intRouteId As Integer = Session(strVersionId & "Route")

                                    Dim dr As DataRow = dtWorkflow.NewRow
                                    dr("ActionType") = "Submit"
                                    dr("WorkflowID") = intWorkflowId
                                    dr("QueueID") = intQueueId
                                    dr("SubmittedUserID") = Me.mobjUser.ImagesUserId
                                    dr("Urgent") = System.Math.Abs(intUrgent)
                                    dr("VersionID") = Me.mcookieSearch.VersionID
                                    dr("Notes") = strWorkflowNotes
                                    dr("RouteId") = intRouteId

                                    dtWorkflow.Rows.Add(dr)

                                End If

                            End If

                        Catch ex As Exception

                        End Try

                    Case Functions.WorkflowMode.ACTION_ACCESS_REVIEWER_PENDING
                        blnWorkflowActive = True

                        Try

                            If Not IsNothing(Session(strVersionId & "WorkflowId")) Then

                                If CStr(Session(strVersionId & "WorkflowId")).Trim.Length = 0 Or Session(strVersionId & "WorkflowId") = "0" Then
                                    'No workflow selected.

                                Else
                                    Dim intWorkflowId As Integer = Session(strVersionId & "WorkflowId")
                                    Dim intQueueId As Integer = Session(strVersionId & "QueueId")
                                    Dim strWorkflowNotes As String = Session(strVersionId & "Notes")
                                    Dim intUrgent As Integer = Session(strVersionId & "Urgent")
                                    'Dim intRouteId As Integer = RoutedetId.Value

                                    Dim dr As DataRow = dtWorkflow.NewRow
                                    dr("ActionType") = "Review"
                                    dr("WorkflowID") = 0
                                    dr("QueueID") = intQueueId
                                    dr("StatusID") = intWorkflowId
                                    dr("SubmittedUserID") = Me.mobjUser.ImagesUserId
                                    dr("Urgent") = System.Math.Abs(intUrgent)
                                    dr("VersionID") = Me.mcookieSearch.VersionID
                                    dr("Notes") = strWorkflowNotes
                                    dr("RouteId") = Session(strVersionId & "Route")

                                    dtWorkflow.Rows.Add(dr)

                                End If

                            End If

                        Catch ex As Exception

                        End Try

                End Select
            End If

        Catch ex As Exception


        End Try

        'Try
        '    'Dim strAnnotationValue As String = Annotation.Text
        '    'Dim intVersionId As Integer = lblVersionId.Value
        '    'Dim intSeqId As Integer = IIf(Session("FileType") = 3, 1, lblPageNo.Value)
        '    'Dim dsVersion As Accucentric.docAssist.Data.dsVersionControl

        '    ''Dim AnnotationValue() As Byte = Convert.FromBase64String(strAnnotationValue)

        '    '' Load the existing dataset
        '    'If Not Session(intVersionId & "VersionControl") Is Nothing Then
        '    '    dsVersion = Session(intVersionId & "VersionControl")
        '    'End If

        '    '' Check for the page already existing
        '    ''First get the ImageDetID column for the page being processed
        '    'Dim objFind(1) As Object
        '    'objFind(0) = intVersionId
        '    'objFind(1) = intSeqId
        '    'Dim drImageVersionDetail As Data.dsVersionControl.ImageVersionDetailRow = dsVersion.ImageVersionDetail.Rows.Find(objFind)

        '    ''Then get the annotation for the page processed
        '    'Dim objFind2(1) As Object
        '    'objFind2(0) = drImageVersionDetail("VersionID")
        '    'objFind2(1) = drImageVersionDetail("ImageDetID")

        '    'Dim drImageAnnotation As Data.dsVersionControl.ImageAnnotationRow
        '    'drImageAnnotation = dsVersion.ImageAnnotation.Rows.Find(objFind2)


        '    '' Save the annotation
        '    'If Not drImageAnnotation Is Nothing Then
        '    '    drImageAnnotation.Annotation = AnnotationValue
        '    'Else
        '    '    Dim drImageAnnotationRow As Data.dsVersionControl.ImageAnnotationRow
        '    '    drImageAnnotationRow = dsVersion.ImageAnnotation.NewImageAnnotationRow
        '    '    drImageAnnotationRow("VersionID") = drImageVersionDetail("VersionID")
        '    '    drImageAnnotationRow("ImageDetID") = drImageVersionDetail("ImageDetID")
        '    '    drImageAnnotationRow("Annotation") = AnnotationValue
        '    '    dsVersion.ImageAnnotation.Rows.Add(drImageAnnotationRow)
        '    'End If
        '    '
        '    ' Save the session variable
        '    Session(intVersionId & "VersionControl") = dsVersion

        'Catch ex As Exception

        'End Try

        Dim bolRequiredOk As Boolean = True
        Dim dtAttributes As dsImageAttributes.ImageAttributesDataTable
        Dim dsAttributes As dsImageAttributes
        Dim intDocumentId As Integer

        Try

            'Proceed to save
            intDocumentId = ddlDocuments.SelectedValue
            dsAttributes = ReverseBindAttributes()
            dtAttributes = dsAttributes.ImageAttributes

            If Me.mSecurityLevel > Web.Security.Functions.SecurityLevel.Read Then

                'Loop through all attributes and mark any errors. This is necessary in case line specific validation is bypassed and
                'when the capture puts items in they are usually not formatted correctly.
                Dim blnErrors As Boolean = False

                Dim dgi As DataGridItem
                For Each dgi In dgAttributes.Items
                    Try
                        If ValidateAttribute(dgi.ItemIndex) = False Then
                            blnErrors = True
                        End If
                    Catch ex As Exception
                        lblAttributeError.Text = "Error while validating attributes."
                        lblAttributeError.Visible = True
                        'ReturnControls.Add(lblAttributeError)
                        Exit Sub
                    End Try
                Next

                'Check if we had any errors
                If blnErrors = True Then
                    lblAttributeError.Text = "Please correct the attributes with errors."
                    lblAttributeError.Visible = True
                    'ReturnControls.Add(lblAttributeError)
                    Exit Sub
                End If


                ' Save the grid changes
                If dgAttributes.SelectedIndex <> -1 Then
                    ValidateAttribute(dgAttributes.SelectedIndex)
                End If

                ' Check for required attributes
                Dim dtDocumentAttributes As New Data.Images.DocumentAttribute
                dtDocumentAttributes.Fill(intDocumentId, Me.mobjUser.ImagesUserId, True, Me.mconSqlImage)

                Dim sbMissingAttributes As New System.Text.StringBuilder

                If dtAttributes.Rows.Count = 0 And dtDocumentAttributes.Rows.Count > 0 Then

                    For Each drReq As DataRow In dtDocumentAttributes.Rows

                        If sbMissingAttributes.Length = 0 Then
                            sbMissingAttributes.Append("The following attributes are required:<BR>")
                        End If
                        sbMissingAttributes.Append("� " & drReq("AttributeName"))
                        sbMissingAttributes.Append("<BR>")

                        'Add missing attribute to grid for user to fill in
                        Dim AttributeId As Integer = drReq("AttributeId")
                        Dim cmdSql As New SqlClient.SqlCommand("SELECT * From Attributes WHERE AttributeId = @AttributeId", Me.mconSqlImage)
                        cmdSql.Parameters.Add("@AttributeId", AttributeId)

                        Dim dt As New DataTable
                        Dim da As New SqlClient.SqlDataAdapter(cmdSql)
                        da.Fill(dt)

                        Dim dr As DataRow
                        dr = dsAttributes.Tables(0).NewRow
                        dr("DataFormat") = dt.Rows(0)("DataFormat")
                        dr("ImageAttributeId") = (AttributeId + 50) * -1
                        dr("AttributeId") = drReq("AttributeId")
                        dr("AccessLevel") = 3
                        dr("Length") = dt.Rows(0)("Length")
                        dr("AttributeDataType") = dt.Rows(0)("AttributeDataType")

                        If dr("AttributeDataType") = "ListView" Then
                            Dim dtDocs As New Data.Images.DocumentAttribute
                            dtDocs.Fill(ddlDocuments.SelectedValue, Me.mobjUser.ImagesUserId, True, Me.mconSqlImage)
                            Dim dv As New DataView(dtDocs)
                            dv.RowFilter = "AttributeId='" & drReq("AttributeId") & "'"
                            dr("ListValidate") = dv(0)("ListValidate")
                        End If

                        dsAttributes.Tables(0).Rows.Add(dr)

                    Next

                    lblAttributeError.Text = "<BR>" & sbMissingAttributes.ToString.Remove(sbMissingAttributes.ToString.Length - 4, 4)
                    lblAttributeError.Visible = True

                    dgAttributes.DataSource = dsAttributes
                    dgAttributes.DataBind()

                    Exit Sub

                Else

                    If dtDocumentAttributes.Rows.Count > 0 Then

                        Dim intCount As Integer = 100
                        For Each drReq As DataRow In dtDocumentAttributes.Rows
                            Dim AttributeId As Integer = drReq("AttributeId")
                            Dim AttributeName As String = drReq("AttributeName")
                            Dim blnAttributeFound As Boolean = False

                            For Each dr As DataRow In dtAttributes.Rows

                                If dr("AttributeId") = AttributeId Then
                                    blnAttributeFound = True
                                End If

                            Next

                            If blnAttributeFound = False Then
                                If sbMissingAttributes.Length = 0 Then
                                    sbMissingAttributes.Append("The following attributes are required:<BR>")
                                End If
                                sbMissingAttributes.Append("� " & drReq("AttributeName"))
                                sbMissingAttributes.Append("<BR>")

                                'Add missing attribute to grid for user to fill in
                                Dim AttributeId2 As Integer = drReq("AttributeId")
                                Dim cmdSql As New SqlClient.SqlCommand("SELECT * From Attributes WHERE AttributeId = @AttributeId", Me.mconSqlImage)
                                cmdSql.Parameters.Add("@AttributeId", AttributeId2)

                                Dim dt As New DataTable
                                Dim da As New SqlClient.SqlDataAdapter(cmdSql)
                                da.Fill(dt)

                                Dim dr As DataRow
                                dr = dsAttributes.Tables(0).NewRow
                                dr("DataFormat") = dt.Rows(0)("DataFormat")
                                dr("AttributeDataType") = dt.Rows(0)("AttributeDataType")
                                intCount += 1

                                Dim intImageAttributeId As Integer = (dsAttributes.Tables(0).Compute("MIN(ImageAttributeId)", "")) - 1
                                dr("ImageAttributeId") = intImageAttributeId
                                dr("AttributeId") = drReq("AttributeId")
                                dr("AccessLevel") = 3
                                dr("Length") = dt.Rows(0)("Length")

                                If dr("AttributeDataType") = "ListView" Then
                                    Dim dtDocs As New Data.Images.DocumentAttribute
                                    dtDocs.Fill(ddlDocuments.SelectedValue, Me.mobjUser.ImagesUserId, True, Me.mconSqlImage)
                                    Dim dv As New DataView(dtDocs)
                                    dv.RowFilter = "AttributeId='" & drReq("AttributeId") & "'"
                                    dr("ListValidate") = dv(0)("ListValidate")
                                End If


                                dsAttributes.Tables(0).Rows.Add(dr)



                            End If

                        Next

                        If sbMissingAttributes.ToString.Length > 0 Then

                            lblAttributeError.Text = "<BR>" & sbMissingAttributes.ToString.Remove(sbMissingAttributes.ToString.Length - 4, 4)
                            lblAttributeError.Visible = True

                            dgAttributes.DataSource = dsAttributes
                            dgAttributes.DataBind()

                            Exit Sub
                        End If

                    End If

                End If

            End If


            Dim drAttribute As dsImageAttributes.ImageAttributesRow

            If bolRequiredOk Then
                If lblVersionId.Value.Trim.Length = 0 Then
                    Dim dsVersion As Data.dsVersionControl = Session(Me.mcookieSearch.VersionID & "VersionControl")
                    'Update the documentid in case it has changed
                    'dsVersion.Images.Rows(0).Item("DocumentID") = intDocumentId
                    '
                    ' Update the images table
                    CType(dsVersion.Images.Rows(0), Data.dsVersionControl.ImagesRow).DocumentID = Me.ddlDocuments.SelectedValue
                    '
                    ' Update the image attributes table
                    For Each drAttribute In dtAttributes.Rows
                        Dim drVersionAttribute As Data.dsVersionControl.ImageAttributesRow = dsVersion.ImageAttributes.NewRow()
                        drVersionAttribute.ImageId = CType(dsVersion.Images.Rows(0), Data.dsVersionControl.ImagesRow).ImageID
                        drVersionAttribute.AttributeId = drAttribute.AttributeId
                        drVersionAttribute.AttributeValue = drAttribute.AttributeValue
                        dsVersion.ImageAttributes.Rows.Add(drVersionAttribute)
                    Next

                    Dim objSave As New Data.ImageSave(Functions.GetImagesUserId(Me.mconSqlImage, Me.mobjUser), Me.mconSqlImage)
                    If objSave.UpdateImage(dsVersion) Then
                        'lblAttributeError.Text = "Save completed successfully"
                        'lblAttributeError.Visible = True
                        lblVersionId.Value = CType(dsVersion.ImageVersion.Rows(0), Data.dsVersionControl.ImageVersionRow).VersionID
                        Me.mcookieSearch.VersionID = lblVersionId.Value
                        Session(Me.mcookieSearch.VersionID & "VersionControl") = dsVersion
                    End If
                Else

                    Dim intVersionId As Integer = Me.mcookieSearch.VersionID 'CType(CType(Session(intVersionId & "VersionControl"), Data.dsVersionControl).ImageVersion.Rows(0), Data.dsVersionControl.ImageVersionRow).VersionID

                    'Add Title,Description, and Tags
                    Dim drTitle As docAssistWeb.dsImageAttributes.ImageAttributesRow
                    drTitle = dsAttributes.Tables(0).NewRow
                    drTitle.AttributeId = -1
                    drTitle.AttributeValue = Server.UrlEncode(txtTitle.Text)
                    drTitle.ImageId = intVersionId
                    dsAttributes.Tables(0).Rows.Add(drTitle)

                    Dim drDescription As docAssistWeb.dsImageAttributes.ImageAttributesRow
                    drDescription = dsAttributes.Tables(0).NewRow
                    drDescription.AttributeId = -2
                    drDescription.AttributeValue = Server.UrlEncode(txtComments.Text)
                    drDescription.ImageId = intVersionId
                    dsAttributes.Tables(0).Rows.Add(drDescription)

                    Dim drTags As docAssistWeb.dsImageAttributes.ImageAttributesRow
                    drTags = dsAttributes.Tables(0).NewRow
                    drTags.AttributeId = -3
                    drTags.AttributeValue = Server.UrlEncode(txtTags.Text)
                    drTags.ImageId = intVersionId
                    dsAttributes.Tables(0).Rows.Add(drTags)

                    ' Update the attributes
                    If UpdateImage(CInt(lblVersionId.Value), dsAttributes, intDocumentId, dtWorkflow, IIf(FolderId.Value = "", 0, FolderId.Value)) Then
                        'lblAttributeError.Text = "Save completed successfully"
                        'lblAttributeError.Visible = True
                        Session(intVersionId & "VersionControl") = Nothing
                        Session(intVersionId & "VersionControl") = Accucentric.docAssist.Data.GetVersionControl(Me.mcookieSearch.VersionID, Me.mconSqlImage, Me.mobjUser.ImagesUserId)
                    Else
                        lblAttributeError.Text = "Save error. Correct any errors and try again."
                        lblAttributeError.Visible = True
                    End If

                End If

                'Dim sb As New System.Text.StringBuilder
                'If litTimeout.Text.Length > 0 Then
                '    sb.Append(litTimeout.Text)
                'End If
                'sb.Append("<SCRIPT langauge='javascript'>parent.document.all.frameImage.src = 'ImageViewer.aspx';</SCRIPT>")
                'litTimeout.Text = sb.ToString

            End If

            'Remove title, desc., and tags
            For X As Integer = dsAttributes.Tables(0).Rows.Count - 1 To 0 Step -1
                If dsAttributes.Tables(0).Rows(X)("AttributeId") < 0 Then
                    dsAttributes.Tables(0).Rows.RemoveAt(X)
                End If
            Next

            dgAttributes.SelectedIndex = -1
            dgAttributes.EditItemIndex = -1
            dgAttributes.DataSource = dsAttributes
            dgAttributes.DataMember = dsAttributes.ImageAttributes.TableName
            dgAttributes.DataBind()


            If blnWorkflowActive = True And lblAttributeError.Text.Trim.Length = 0 Then

                If Not IsNothing(Session(strVersionId & "WorkflowId")) And Not IsNothing(Session(strVersionId & "QueueId")) Then

                    If CStr(Session(strVersionId & "WorkflowId")).Trim.Length > 0 And Session(strVersionId & "WorkflowId") <> "0" And CStr(Session(strVersionId & "QueueId")).Trim.Length > 0 Then
                        System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.Empty.ToString, "parent.SaveHistory();", True)
                        Session(strVersionId & "WorkflowId") = Nothing
                        Session(strVersionId & "QueueId") = Nothing
                        Session(strVersionId & "Notes") = Nothing
                        Session(strVersionId & "Urgent") = Nothing
                        Session(strVersionId & "Route") = Nothing
                    End If

                End If

            End If


            'Save distribution
            'If Functions.ModuleSetting(Functions.ModuleCode.DISTRIBUTION, Me.mobjUser) Then

            '    Dim dtDist As New DataTable
            '    dtDist = Session("Distribution" & Me.mcookieSearch.VersionID)

            '    If Not IsNothing(dtDist) Then

            '        Dim objDist As New Accucentric.docAssist.Data.Images.Distribution(Me.mconSqlImage, False)

            '        'Clear items for this document
            '        objDist.DistributionDetailInsert(Me.mobjUser.ImagesUserId, Me.mcookieSearch.VersionID, 0, "0", "0", "0", "0", "0", "0")

            '        For Each dr As DataRow In dtDist.Rows
            '            Dim intReturn As Integer = objDist.DistributionDetailInsert(Me.mobjUser.ImagesUserId, Me.mcookieSearch.VersionID, Session("DistributionId" & Me.mcookieSearch.VersionID), IIf(dr("Col1Value") Is System.DBNull.Value, "", dr("Col1Value")), _
            '            IIf(dr("Col1Desc") Is System.DBNull.Value, "", dr("Col1Desc")), IIf(dr("Col2Value") Is System.DBNull.Value, "", dr("Col2Value")), IIf(dr("Col2Desc") Is System.DBNull.Value, "", dr("Col2Desc")), _
            '            IIf(dr("Memo") Is System.DBNull.Value, "", dr("Memo")), IIf(dr("Amount") Is System.DBNull.Value, "0.00", dr("Amount")))
            '            Dim x = 8
            '        Next

            '    End If

            'End If

            InitOCRRemoveButtons()
            RefreshGLDistribution()

            lblAttributeError.Text = "Save completed successfully."
            lblAttributeError.Visible = True

        Catch ex As Exception

            Select Case ex.Message
                Case "Error saving metadata."
                    lblAttributeError.Text = "Unable to save metadata. Please try again."
                Case "Workflow has changed."
                    lblAttributeError.Text = "Unable to save. This document has already been reviewed by another user."
                Case Else
                    lblAttributeError.Text = "An error has occured. Please try again. If the error continues please reload the document."
                    Functions.LogException(mobjUser.UserId.ToString, mobjUser.AccountId.ToString, "", ex.Message, ex.StackTrace, Server.MachineName, "", Request.UrlReferrer.ToString, Functions.BuildMasterConnection, True)
            End Select

            lblAttributeError.Visible = True

        Finally

            If lblAttributeError.Text.Trim.Length = 0 Or lblAttributeError.Text = "Save completed successfully." Then
                hiddenDirty.Value = "0"
                System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.Empty.ToString, "DisableSave();", True)
            End If

            'ReturnControls.Add(lblAttributeError)
            'ReturnControls.Add(dgAttributes)
            'ReturnControls.Add(hiddenDirty)

        End Try

    End Sub

    Private Sub AttributeChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

        Dim ddlAttribute As DropDownList = CType(sender, DropDownList)
        Dim lblAttributeId As Label = CType(ddlAttribute.Parent.NamingContainer.FindControl("lblAttributeId"), Label)

        lblAttributeId.Text = ddlAttribute.SelectedItem.Value

        InitializeAttributeFields(CInt(lblAttributeId.Text), ddlAttribute.Parent.NamingContainer)

        System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.Empty.ToString, "setTimeout('document.all." & ddlAttribute.ClientID & ".focus();',300);", True)

    End Sub

    Private Sub AttributeNavigation1_SaveEvent(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        btnSave_Click(sender, e)
    End Sub

    Private Function RefreshIntegrationMap(ByVal intDocumentId As Integer)

        Try

            Me.mobjUser = BuildUserObject(Request.Cookies("docAssist"))
            Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
            Me.mcookieSearch = New cookieSearch(Me)

            If Not Me.mconSqlImage.State Then
                Me.mconSqlImage.Open()
            End If

            Dim sqlCommand As New SqlClient.SqlCommand("acsIntegrationCaptureData", Me.mconSqlImage)
            sqlCommand.CommandType = CommandType.StoredProcedure
            sqlCommand.Parameters.Add("@VersionID", Me.mcookieSearch.VersionID)
            sqlCommand.Parameters.Add("@DocumentID", intDocumentId)
            sqlCommand.Parameters.Add("@UserID", Me.mobjUser.ImagesUserId)

            Dim daSql As New SqlClient.SqlDataAdapter
            daSql.SelectCommand = sqlCommand

            Dim dsIntegration As New DataSet
            daSql.Fill(dsIntegration)

            If dsIntegration.Tables(0).Rows.Count = 0 Then
                btnCapture2.Visible = False
                txtHeader.Text = ""
                txtDetail.Text = ""
                Exit Function
            Else
                btnCapture2.Visible = True
                'Parse out the integration 'map'

                Dim charField As Char = "|"
                Dim charRow As Char = "�"

                Dim sbHeader As New System.Text.StringBuilder
                Dim dr As DataRow

                Dim intHeaderCount As Integer = 0

                'Header
                For Each dr In dsIntegration.Tables(0).Rows

                    CaptureType.Value = dr("AttributeCaptureMode")

                    If dr("FolderMap") = True Then
                        FolderMap.Value = "1"
                        FormID.Value = dr("FormID")
                        DocumentID.Value = dr("DocumentID")
                    Else
                        FolderMap.Value = "0"
                    End If

                    'ReturnControls.Add(FolderMap)
                    'ReturnControls.Add(FormID)
                    'ReturnControls.Add(DocumentID)

                    intHeaderCount += 1
                    sbHeader.Append(dr("IntegrationID") & charField)
                    sbHeader.Append(dr("PublicName") & charField)
                    btnCapture2.ToolTip = "Capture " & dr("PublicName")
                    ApplicationName.Value = CStr(dr("PublicName")).Replace(",", "")
                    'ReturnControls.Add(ApplicationName)
                    sbHeader.Append(dr("WIndowName") & charField)
                    sbHeader.Append(dr("WindowClass") & charField)
                    sbHeader.Append(dr("Top") & charField)
                    sbHeader.Append(dr("Left") & charField)
                    sbHeader.Append(dr("Width") & charField)
                    sbHeader.Append(dr("Height") & charField)
                    sbHeader.Append(dr("Action") & charField)
                    sbHeader.Append(dr("ActionClickX") & charField)
                    sbHeader.Append(dr("ActionClickY") & charField)
                    sbHeader.Append(dr("ActionSendKey"))
                    If intHeaderCount < dsIntegration.Tables(0).Rows.Count Then sbHeader.Append(charRow)
                Next

                Dim sbDetail As New System.Text.StringBuilder
                Dim intDetailCount As Integer = 0

                'Detail
                For Each dr In dsIntegration.Tables(1).Rows
                    intDetailCount += 1
                    sbDetail.Append(dr("AttributeID") & charField)
                    sbDetail.Append(dr("Expression") & charField)
                    'sbDetail.Append(dr("IntegrationID") & charField)
                    sbDetail.Append(dr("PosTop") & charField)
                    sbDetail.Append(dr("PosLeft") & charField)
                    sbDetail.Append(dr("Width") & charField)
                    sbDetail.Append(dr("Height"))
                    If intDetailCount < dsIntegration.Tables(1).Rows.Count Then sbDetail.Append(charRow)
                Next

                txtHeader.Text = sbHeader.ToString
                txtDetail.Text = sbDetail.ToString

            End If

        Catch ex As Exception

            Throw ex

        Finally

            'ReturnControls.Add(CaptureType)

        End Try

    End Function

    Private Sub dgAttributes_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgAttributes.ItemCreated

        Dim ddlAttribute As DropDownList = e.Item.FindControl("ddlAttribute")

        If Not IsNothing(ddlAttribute) Then
            AddHandler ddlAttribute.SelectedIndexChanged, AddressOf AttributeChanged
        End If

    End Sub

    Private Sub btnRemove_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnRemove.Click

        Try

            If ActiveRow.Value.Trim = "" Then Exit Sub

            'Validate last row if they focused out of it rather than commiting it.
            If dgAttributes.EditItemIndex <> -1 Then
                If ValidateAttribute(dgAttributes.EditItemIndex) = False Then
                    dgAttributes.SelectedIndex = dgAttributes.EditItemIndex
                    'ReturnControls.Add(dgAttributes)
                    Exit Sub
                End If
            End If

            Dim intActiveRow As Integer = CInt(ActiveRow.Value)

            'Add the new row and rebind
            Dim ds As dsImageAttributes
            If dgAttributes.Items.Count > 0 Then
                ds = ReverseBindAttributes(True)
            Else
                ds = New dsImageAttributes
            End If

            'Remove row
            If intActiveRow <= ds.Tables(0).Rows.Count - 1 Then
                ds.Tables(0).Rows(intActiveRow).Delete()
                ds.Tables(0).AcceptChanges()
            End If

            dgAttributes.DataSource = ds
            dgAttributes.DataMember = ds.ImageAttributes.TableName
            dgAttributes.DataBind()

            If ds.Tables(0).Rows.Count = 0 Then

            ElseIf intActiveRow = ds.Tables(0).Rows.Count - 1 Then
                System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.Empty.ToString, ActivateFunction & "('" & ds.Tables(0).Rows.Count - 1 & "');setTimeout('document.all." & dgAttributes.Items(ds.Tables(0).Rows.Count - 1).FindControl("txtAttributeValue").ClientID & ".focus();',300);", True)
            ElseIf intActiveRow >= 0 And intActiveRow < ds.Tables(0).Rows.Count - 1 Then
                System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.Empty.ToString, ActivateFunction & "('" & intActiveRow & "');setTimeout('document.all." & dgAttributes.Items(intActiveRow).FindControl("txtAttributeValue").ClientID & ".focus();',300);", True)
            End If

            InitOCRRemoveButtons()
            RefreshGLDistribution()

        Catch ex As Exception

        Finally

            System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.Empty.ToString, "clearCookie('Attributes');ActivateSave();setAttributesHeight();", True)

        End Try

    End Sub

    Private Sub dgAttributes_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgAttributes.PreRender

        Dim blnControlAttributeFound As Boolean = False

        Dim sb As New System.Text.StringBuilder

        Try

            sb.Append("function " & ActivateFunction & "(rownum){")

            For row As Integer = dgAttributes.Items.Count - 1 To 0 Step -1

                Dim ddlAttribute As DropDownList = dgAttributes.Items(row).FindControl("ddlAttribute")
                Dim txtAttributeValue As TextBox = dgAttributes.Items(row).FindControl("txtAttributeValue")
                Dim lblAttributeValue As Label = dgAttributes.Items(row).FindControl("lblAttributeValue")
                Dim lblAccessLevel As Label = dgAttributes.Items(row).FindControl("lblAccessLevel")
                Dim lblControlAttributeId As Label = dgAttributes.Items(row).FindControl("lblControlAttributeId")

                ddlAttribute.Attributes("onchange") = "ActivateSave();"

                'Bind enter key to each textbox
                If row <= dgAttributes.Items.Count - 1 And row <> 0 Then
                    Dim prevtxtAttributeValue As TextBox = dgAttributes.Items(row - 1).FindControl("txtAttributeValue")
                    prevtxtAttributeValue.Attributes("onkeypress") = "dataEntered = true;ActivateSave();if (event.keyCode == 13){document.all." & txtAttributeValue.ClientID & ".focus();event.cancelBubble = true;event.returnValue = false;}"
                    'ReturnControls.Add(prevtxtAttributeValue)
                End If

                If row = dgAttributes.Items.Count - 1 Then
                    txtAttributeValue.Attributes("onkeypress") = "dataEntered = true;ActivateSave();if (event.keyCode == 13){document.all.lnkAdd.click();}"
                End If

                Select Case CType(lblAccessLevel.Text, AttributeAccess)
                    Case AttributeAccess.NONE, AttributeAccess.READ_ONLY
                        'Hide add/remove icons
                        sb.Append("if (rownum == '" & dgAttributes.Items(row).ItemIndex & "'){ document.all.btnRemove.style.display = 'none'; if (document.all.imgOcr != undefined) { document.all.imgOcr.style.display = 'none'; } document.all." & txtAttributeValue.ClientID & ".style.backgroundColor = '#E0E0E0';document.all." & ddlAttribute.ClientID & ".style.backgroundColor = '#E0E0E0';" & "}else{ document.all." & txtAttributeValue.ClientID & ".style.backgroundColor = '#FFFFFF';document.all." & ddlAttribute.ClientID & ".style.backgroundColor = '#FFFFFF';}")
                    Case AttributeAccess.CHANGE, AttributeAccess.DELETE
                        sb.Append("if (rownum == '" & dgAttributes.Items(row).ItemIndex & "'){ document.all.btnRemove.style.display = 'inline'; if (document.all.imgOcr != undefined) { document.all.imgOcr.style.display = 'none'; } document.all." & txtAttributeValue.ClientID & ".style.backgroundColor = '#E0E0E0';document.all." & ddlAttribute.ClientID & ".style.backgroundColor = '#E0E0E0';" & "}else{ document.all." & txtAttributeValue.ClientID & ".style.backgroundColor = '#FFFFFF';document.all." & ddlAttribute.ClientID & ".style.backgroundColor = '#FFFFFF';}")
                End Select

                Dim lblAttributeDataType As Label = CType(dgAttributes.Items(row).FindControl("lblAttributeDataType"), Label)
                If lblAttributeDataType.Text = "Date" Then
                    If IsDate(txtAttributeValue.Text) Then
                        txtAttributeValue.Text = CDate(txtAttributeValue.Text).ToShortDateString
                        lblAttributeValue.Text = txtAttributeValue.Text
                        'ReturnControls.Add(txtAttributeValue)
                        'ReturnControls.Add(lblAttributeValue)
                    End If
                End If

                If lblControlAttributeId.Text = "True" Or lblControlAttributeId.Text = "1" Then
                    blnControlAttributeFound = True
                    'txtAttributeValue.Attributes("onblur") = "setControl('" & txtAttributeValue.ClientID & "');"

                    Dim sbscript As New System.Text.StringBuilder
                    sbscript.Append("function updateControlAttribute(newValue) { ")
                    sbscript.Append("var txtControlBox = document.all." & txtAttributeValue.ClientID & ";")
                    sbscript.Append("if (txtControlBox == undefined) {")
                    sbscript.Append("document.all.ControlValue.value = newValue;document.all.btnAddControlItem.click();")
                    sbscript.Append("}else{")
                    sbscript.Append("txtControlBox.value = newValue;")
                    sbscript.Append("}")
                    sbscript.Append("}")
                    'ReturnScripts.Add("function updateControlAttribute(newValue) { document.all." & txtAttributeValue.ClientID & ".value = newValue;ActivateSave(); } ")
                    System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.Empty.ToString, sbscript.ToString, True)
                    System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.Empty.ToString, "controlTextbox = document.all." & txtAttributeValue.ClientID & ";", True)
                    controlName.Value = txtAttributeValue.ClientID
                    'ReturnControls.Add(controlName)
                    txtAttributeValue.Attributes("onkeypress") = "return NumericOnlyandDecimals(event);"
                End If

            Next

            If blnControlAttributeFound = False Then
                System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.Empty.ToString, "controlTextbox = undefined;", True)
            End If

            sb.Append("}")

            'If dgAttributes.Items.Count > 0 Then
            '    ddlDocuments.Attributes("onchange") = "if (dataEntered == true){if (confirm('Are you sure you want to change document type? All attributes will be removed.')){}else{document.getElementById('ddlDocuments').selectedIndex = '" & ddlDocuments.SelectedIndex & "';return false;}}"
            'Else
            '    ddlDocuments.Attributes("onchange") = ""
            'End If

        Catch ex As Exception

        Finally

            System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.Empty.ToString, sb.ToString, True)

        End Try

    End Sub

    Private Sub InitializeDocumentAttributes(ByVal DocumentId As Integer)

        Try

            Dim ds As New dsImageAttributes
            ds = ReverseBindAttributes()

            'If IsNothing(dtAttributes) Then
            '    dtAttributes = New Data.Images.Attributes
            '    dtAttributes.Fill(DocumentId, Data.Images.Attributes.AttributeParameterType.DocumentId, Me.mobjUser.ImagesUserId, Me.mconSqlImage)
            'End If

            'Dim dtAttributes As New Data.Images.Attributes

            'dtAttributes.Fill(DocumentId, Data.Images.Attributes.AttributeParameterType.DocumentId, Me.mobjUser.ImagesUserId, Me.mconSqlImage)

            dtAttributes.Columns.Add("AttributeValue")
            dtAttributes.Columns.Add("ImageAttributeId")
            dtAttributes.Columns.Add("ValueDescription")
            dtAttributes.Columns.Add("Required")

            Me.mconSqlImage.Open()
            Dim intCount As Integer = 1

            For Each dr As Data.Images.AttributesRow In dtAttributes.Rows

                Dim cmdSql As New SqlClient.SqlCommand("SELECT * From Attributes WHERE AttributeId = @AttributeId", Me.mconSqlImage)
                cmdSql.Parameters.Add("@AttributeId", dr("AttributeId"))

                Dim dt As New DataTable
                Dim da As New SqlClient.SqlDataAdapter(cmdSql)
                da.Fill(dt)
                dr("DataFormat") = dt.Rows(0)("DataFormat")
                dr("ImageAttributeId") = intCount * -1

                For Each row As DataRow In ds.Tables(0).Rows
                    If row("AttributeId") = dr("AttributeId") Then
                        dr("AttributeValue") = row("AttributeValue")
                        Exit For
                    End If
                Next

                intCount += 1

            Next

            dgAttributes.DataSource = dtAttributes
            dgAttributes.DataBind()

        Catch ex As Exception

        Finally

            System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.Empty.ToString, "dataEntered = false;", True)

            If dgAttributes.Items.Count > 0 Then
                System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.Empty.ToString, ActivateFunction & "('0');setTimeout('document.all." & dgAttributes.Items(0).FindControl("txtAttributeValue").ClientID & ".focus();',300);", True)
            End If

        End Try

    End Sub

    Private Sub btnClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClear.Click

        Try
            dgAttributes.DataSource = Nothing
            dgAttributes.DataBind()
        Catch ex As Exception

        Finally
            'ReturnControls.Add(dgAttributes)
        End Try

    End Sub

    Private Sub btnIntegration_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnIntegration.Click

        Try

            Dim strValues As String = IntegrationValues.Value

            If Server.UrlDecode(strValues.EndsWith("�")) Then
                strValues = Server.UrlDecode(strValues).TrimEnd("�")
            Else
                strValues = Server.UrlDecode(strValues)
            End If

            Dim strAttributes() As String = strValues.Split("�")
            Dim X As Integer = 0

            Dim ds As New dsImageAttributes

            Select Case CaptureType.Value
                Case 0 'Append (keep attributes in common)
                    ds = ReverseBindAttributes()
                Case 1 'Replace
                    ds = New dsImageAttributes
                    dgAttributes.DataSource = Nothing
                    dgAttributes.DataBind()
            End Select

            For X = 0 To strAttributes.Length - 1

                Dim strInsertValues() As String
                strInsertValues = strAttributes(X).ToString.Split("|")
                Dim strAttributeId As String = strInsertValues(0).ToString
                Dim strAttributeDesc As String = "Lookup"
                Dim strAttributeValue As String = strInsertValues(1).ToString

                If strAttributeId <> "" And CInt(strAttributeId) < 0 Then

                    'Folder mappings
                    If FolderMap.Value = "1" Then

                        Dim cmd As New SqlClient.SqlCommand("acsIntegrationFolderMapGet", Me.mconSqlImage)
                        cmd.CommandType = CommandType.StoredProcedure
                        cmd.Parameters.Add("@FormID", FormID.Value)
                        cmd.Parameters.Add("@FieldID", CInt(strAttributeId) * -1)
                        cmd.Parameters.Add("@DocumentID", DocumentID.Value)
                        cmd.Parameters.Add("@CaptureValue", strAttributeValue)
                        Dim dtFolderMappings As New DataTable
                        Dim da As New SqlClient.SqlDataAdapter(cmd)
                        da.Fill(dtFolderMappings)

                        If dtFolderMappings.Rows.Count > 0 Then
                            FolderId.Value = dtFolderMappings.Rows(0)("FolderId")
                            txtFolder.Text = dtFolderMappings.Rows(0)("FolderPath")
                            'ReturnControls.Add(FolderId)
                            'ReturnControls.Add(txtFolder)
                        End If

                    End If

                Else

                    Dim dt As dsImageAttributes.ImageAttributesDataTable = ds.ImageAttributes
                    Dim dr As dsImageAttributes.ImageAttributesRow

                    Dim blnAlreadyExist As Boolean = False

                    For Each drCheck As DataRow In ds.Tables(0).Rows
                        If drCheck("AttributeId") = strAttributeId And UCase(drCheck("AttributeValue")) = UCase(strAttributeValue) Then
                            blnAlreadyExist = True
                            Exit For
                        End If
                    Next

                    If Not strAttributeValue.Trim = "" And blnAlreadyExist = False Then

                        ' Add new row 
                        dr = dt.NewRow
                        dr.Item("AttributeId") = strAttributeId
                        dr.Item("AttributeValue") = strAttributeValue

                        Dim cmd As New SqlClient.SqlCommand
                        cmd.Connection = Me.mconSqlImage
                        Dim prm As New SqlClient.SqlParameter("@AttributeId", strAttributeId)
                        cmd.CommandType = CommandType.Text
                        cmd.CommandText = "SELECT AttributeName FROM Attributes WHERE AttributeId = @AttributeId"
                        cmd.Parameters.Add(prm)
                        If Me.mconSqlImage.State <> ConnectionState.Open Then Me.mconSqlImage.Open()
                        dr.Item("AttributeName") = cmd.ExecuteScalar()

                        'TODO: Find the corect defaults for this attribute
                        dr.AccessLevel = 3
                        dr.ListValidate = False
                        dr.ControlTotalAttributeID = False

                        dt.Rows.Add(dr)

                    End If

                End If

            Next

            ' Rebind the grid
            dgAttributes.DataSource = ds
            dgAttributes.DataMember = ds.ImageAttributes.TableName
            dgAttributes.DataBind()
            'ReturnControls.Add(dgAttributes)

            Me.mconSqlImage.Close()

        Catch ex As Exception

            'ReturnControls.Add(lblAttributeError)
            lblAttributeError.Text = "Error occured while capturing attributes. Please try again."
            lblAttributeError.Visible = True

        Finally

            System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.Empty.ToString, "ActivateSave();", True)

        End Try

    End Sub


    Private Sub InitOCRRemoveButtons()

        If dgAttributes.Items.Count = 0 Then
            btnRemove.Visible = False
        Else
            btnRemove.Visible = True
        End If

        If ddlDocuments.SelectedItem.Value = 0 Or CType(Session("FileType"), Functions.FileType) = Functions.FileType.ATTACHMENT Or dgAttributes.Items.Count = 0 Then
            btnOcr.Visible = False
        Else
            btnOcr.Visible = True
        End If

        If Me.mSecurityLevel = Web.Security.Functions.SecurityLevel.NoAccess Or Me.mSecurityLevel = Web.Security.Functions.SecurityLevel.Read Then
            btnOcr.Visible = False
            btnRemove.Visible = False
        End If

        'ReturnControls.Add(btnRemove)
        'ReturnControls.Add(btnOcr)

    End Sub

    Private Sub RefreshGLDistribution()

        Try

            If Functions.ModuleSetting(Functions.ModuleCode.DISTRIBUTION, Me.mobjUser) Then

                Dim objDist As New Accucentric.docAssist.Data.Images.Distribution(Me.mconSqlImage, False)
                Dim dt As New DataTable
                dt = objDist.DistributionConfiguration(ddlDocuments.SelectedValue)

                If dt.Rows.Count = 0 Then
                    imgDistribution.Visible = False
                Else
                    imgDistribution.Visible = True

                    'Check if a control attribute is present
                    Dim blnControlFound As Boolean = False
                    For Each dgi As DataGridItem In dgAttributes.Items
                        Dim lblControlAttributeId As Label = dgi.FindControl("lblControlAttributeId")
                        If Not IsNothing(lblControlAttributeId) Then
                            If lblControlAttributeId.Text = "True" Or lblControlAttributeId.Text = "1" Then
                                blnControlFound = True
                                Exit For
                            End If
                        End If
                    Next

                    If dt.Rows(0)("ControlTotalEnabled") Then

                        If dt.Rows(0)("ControlTotalUpdateAttribute") = False And blnControlFound = False Then
                            imgDistribution.Attributes("onclick") = "alert('No control attribute has been defined. Please add one and try again.');"
                        Else
                            imgDistribution.Attributes("onclick") = "showDistributionDialog(document.all.ddlDocuments.value + ',' + document.all.FolderId.value);"
                        End If

                    Else

                        imgDistribution.Attributes("onclick") = "showDistributionDialog(document.all.ddlDocuments.value + ',' + document.all.FolderId.value);"

                    End If

                End If

            End If

        Catch ex As Exception

        Finally

            'ReturnControls.Add(imgDistribution)

        End Try

    End Sub

    Private Sub btnAddControlItem_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAddControlItem.Click

        'ControlValue.Value
        'Validate last row if they focused out of it rather than commiting it.
        If dgAttributes.EditItemIndex <> -1 Then
            If ValidateAttribute(dgAttributes.EditItemIndex) = False Then
                dgAttributes.SelectedIndex = dgAttributes.EditItemIndex
                'ReturnControls.Add(dgAttributes)
                Exit Sub
            End If
        End If

        ' Add the new row and rebind
        Dim ds As dsImageAttributes
        If dgAttributes.Items.Count > 0 Then
            ds = ReverseBindAttributes(True)
        Else
            ds = New dsImageAttributes
        End If

        Dim intAttributeId As Integer

        Dim objDist As New Accucentric.docAssist.Data.Images.Distribution(Me.mconSqlImage, False)
        Dim dt As New DataTable
        dt = objDist.DistributionConfiguration(ddlDocuments.SelectedValue)

        Dim dr As dsImageAttributes.ImageAttributesRow
        Dim dtNew As dsImageAttributes.ImageAttributesDataTable = ds.ImageAttributes
        dr = dtNew.NewRow
        dr.AccessLevel = 3
        dr.ControlTotalAttributeID = False
        dr.AttributeValue = ControlValue.Value

        If dt.Rows.Count > 0 Then
            intAttributeId = dt.Rows(0)("ControlTotalAttributeId")
            dr.AttributeId = intAttributeId
        End If

        dtNew.Rows.Add(dr)

        AddingNewRow = True
        dgAttributes.DataSource = ds
        dgAttributes.DataMember = ds.ImageAttributes.TableName
        dgAttributes.DataBind()

        selectedIndex.Value = dgAttributes.EditItemIndex

        'Initialize new row
        Dim ddlAttribute As DropDownList = dgAttributes.Items(dgAttributes.Items.Count - 1).FindControl("ddlAttribute")
        Dim lblAttributeName As Label = dgAttributes.Items(dgAttributes.Items.Count - 1).FindControl("lblAttributeName")
        Dim lblAttributeId As Label = dgAttributes.Items(dgAttributes.Items.Count - 1).FindControl("lblAttributeId")

        ddlAttribute.Visible = True
        lblAttributeName.Visible = False

        InitializeAttributeDropDown(ddlDocuments.SelectedValue, CType(dgAttributes.Items(dgAttributes.Items.Count - 1), DataGridItem))

        lblAttributeId.Text = ddlAttribute.SelectedItem.Value
        InitializeAttributeFields(CInt(lblAttributeId.Text), CType(dgAttributes.Items(dgAttributes.Items.Count - 1), DataGridItem))
        AddingNewRow = False

        InitOCRRemoveButtons()

        System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.Empty.ToString, ActivateFunction & "('" & dgAttributes.Items.Count - 1 & "');setTimeout('document.all." & dgAttributes.Items(dgAttributes.Items.Count - 1).FindControl("ddlAttribute").ClientID & ".focus();',300);clearCookie('Attributes');ActivateSave();setAttributesHeight();", True)

    End Sub

End Class
Public Class ErrorCodes

    'Codes
    Friend Const SESSION_TIMEOUT = 124
    Friend Const ERROR_CHECKING_TIMEOUT = 125
    Friend Const ERROR_PRINT_ACCESS_DENIED = 126
    Friend Const ERROR_INTEGRATION_ACCESS_DENIED = 127

    'Messages
    Friend Const MESSAGE_ERROR_CHECKING_TIMEOUT = "Error loading session information. Please login to continue."
    Friend Const MESSAGE_ERROR_LOAD_ACCOUNT_STATUS = "An error occured while loading the status of this account."
    Friend Const MESSAGE_PRINT_ACCESS_DENIED = "This account does not support printing. Please contact docAssist to upgrade your account."
    Friend Const MESSAGE_INTEGRATION_ACCESS_DENIED = "This account does not support integration. Please contact docAssist to upgrade your account."

    'Distributions
    Friend Const DISTRIBUTION_INVALID_LIST_ITEM = "Please select a valid G/L distribution item."
    Friend Const DISTRIBUTION_OUT_OF_BALANCE = "Distribution items do not match the control total."

End Class
<%@ Register TagPrefix="uc1" TagName="AdminNav" Src="Controls/AdminNav.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ConfigureCabinets.aspx.vb" Inherits="docAssistWeb._Cabinets"%>
<%@ Register TagPrefix="uc1" TagName="docHeader" Src="Controls/docHeader.ascx" %>
<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="Controls/MenuBar.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<TITLE>docAssist</TITLE>
		<META content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<META content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<META content="JavaScript" name="vs_defaultClientScript">
		<META content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<BODY onload="ShowConfiguration();">
		<FORM id="Form1" onsubmit="SelectAllAssigned();" method="post" runat="server">
			<INPUT id="hostname" type="hidden" runat="server">
			<TABLE height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TBODY>
					<TR>
						<TD width="100%" colSpan="3"><UC1:DOCHEADER id="docHeader" runat="server"></UC1:DOCHEADER></TD>
					</TR>
					<TR vAlign="middle" height="100%">
						<TD class="Navigation1" vAlign="top" width="180" height="100%"><UC1:ADMINNAV id="AdminNav1" runat="server"></UC1:ADMINNAV></TD>
						<TD class="PageContent" vAlign="middle" align="center" height="100%">
							<P class="Heading"><ASP:LABEL id="lblPageHeading" CssClass="PageHeading" Runat="server">Configuration Manager</ASP:LABEL></P>
							<TABLE class="transparent" cellSpacing="0" cellPadding="0" align="center" border="0">
								<TBODY>
									<TR>
										<TD class="TableHeader" style="BORDER-RIGHT: #d00000 1px solid; BORDER-TOP: #d00000 1px solid; BORDER-LEFT: #d00000 1px solid"
											width="364">Cabinets</TD>
										<TD>&nbsp;</TD>
										<TD class="TableHeader" style="BORDER-RIGHT: #d00000 1px solid; BORDER-TOP: #d00000 1px solid; BORDER-LEFT: #d00000 1px solid"
											width="336">
											<TABLE class="transparentnopadding" cellSpacing="0" cellPadding="0" width="100%" border="0">
												<TR class="TableHeader">
													<TD>Modify Cabinet</TD>
													<TD align="right"><A id="lnkNew" style="DISPLAY: none" href="javascript:Clear();">New</A>&nbsp;<A id="lnkAdd" href="javascript:AddCabinet();">Add</A></TD>
												</TR>
											</TABLE>
										</TD>
									</TR>
									<TR>
										<TD style="BORDER-RIGHT: #d00000 1px solid; BORDER-LEFT: #d00000 1px solid; BORDER-BOTTOM: #d00000 1px solid">
											<IMG src="imags/spacer.gif" width="8" height="1"><ASP:LISTBOX id="lstAssigned" runat="server" CssClass="InputBox" Width="160px" Height="216px"></ASP:LISTBOX></TD>
										<TD>&nbsp;</TD>
										<TD style="BORDER-RIGHT: #d00000 1px solid; BORDER-LEFT: #d00000 1px solid; BORDER-BOTTOM: #d00000 1px solid"
											vAlign="top"><INPUT id="txtCabinetId" type="hidden">
											<TABLE class="transparent" cellSpacing="0" cellPadding="0" width="100%" border="0">
												<TR>
													<TD>Cabinet&nbsp;Name:&nbsp;</TD>
													<TD width="100%"><ASP:TEXTBOX id="txtCabinetName" CssClass="InputBox" Runat="server" Width="100%"></ASP:TEXTBOX></TD>
													<TD>&nbsp;</TD>
												</TR>
											</TABLE>
										</TD>
									</TR>
									<TR>
										<TD colSpan="2"><SPAN class="red" id="showMessage" runat="server"></SPAN></TD>
										<TD align="right"><ASP:LINKBUTTON id="btnSave" Runat="server">Save</ASP:LINKBUTTON></TD>
									</TR>
								</TBODY>
							</TABLE>
							<INPUT id="hiddenCabinets" type="hidden" runat="server">
						</TD>
						<TD class="RightContent" width="160">&nbsp;</TD>
					</TR>
					<TR height="1">
						<TD style="BACKGROUND-COLOR: #959394" width="160"><IMG height="1" src="images/spacer.gif" width="160"></TD>
						<TD style="BACKGROUND-COLOR: #959394" width="100%"><IMG height="1" src="images/spacer.gif" width="100%"></TD>
						<TD style="BACKGROUND-COLOR: #959394" width="80"><IMG height="1" src="images/spacer.gif" width="80"></TD>
					</TR>
				</TBODY>
			</TABLE>
			<SCRIPT language="javascript">
		        document.all.docHeader_btnSearch.src = "./Images/toolbar_search_white.gif";
		        document.all.docHeader_btnIndex.src = "./Images/toolbar_index_white.gif";
		        document.all.docHeader_btnScan.src = "./Images/toolbar_scan_white.gif";
		        document.all.docHeader_btnOptions.src = "./Images/toolbar_options_grey.gif";
			</SCRIPT>
			<SCRIPT language="javascript">
				var blnCabinetId = -1;
				var selectedCabinetId = 0;
				var blnDirtyData = false;
				var hostName;
			
				function AddCabinet()
				{
					var strCabinetName = document.all.txtCabinetName.value;
					var intCabinetId = -1;
					if(document.all.lstAssigned.selectedIndex < 0)
					{
						document.all.lstAssigned.options[document.all.lstAssigned.options.length] = new Option(strCabinetName, intCabinetId);
						document.all.showMessage.innerHTML = "Document added successfully.";
						blnDirtyData = true;
					}
					else 
					{
						if (document.all.lstAssigned.selectedIndex >= 0)
							document.all.lstAssigned.options[document.all.lstAssigned.selectedIndex].text = strCabinetName;
						document.all.showMessage.innerHTML = "Document updated successfully.";
						blnDirtyData = true;
					}
				}

				function CabinetSelected(listBox)
				{
					var strText = listBox.options[listBox.selectedIndex].text;
					var intValue = listBox.options[listBox.selectedIndex].value;
					document.all.txtCabinetId.value = intValue;
					document.all.txtCabinetName.value = strText;
					document.all.lnkNew.style.display = "inline";
					document.all.lnkAdd.innerHTML = "Change";

				}

				function Clear()
				{
					document.all.lnkAdd.innerHTML = "Add";
					document.all.txtCabinetId.value = -1;
					document.all.txtCabinetName.value = "";
					document.all.lnkNew.style.display = "none";
					document.all.lstAssigned.selectedIndex = -1;
						
				}

				function Disable()
				{
					document.all.txtCabinetName.disabled = true;
					document.all.lnkNew.disabled = true;
					document.all.lnkAdd.disabled = true;
				}

				function Enable()
				{
					document.all.txtCabinetName.disabled = false;
					document.all.lnkNew.disabled = false;
					document.all.lnkAdd.disabled = false;
				}
			
				
				function SelectAllAssigned()
				{
					var lstAssigned = document.all.lstAssigned;
					lstAssigned.onClick = "";
					lstAssigned.multiple = true;
					var selectedItems = new Array();
					for(var i=0; i<lstAssigned.options.length; i++)
					{	
						selectedItems[i] = lstAssigned.options[i].value + ";" + lstAssigned.options[i].text;
					}
					document.all.hiddenCabinets.value = selectedItems;
				}

				function ShowConfiguration()
				{
					document.all.ConfigurationLinks.style.display='block';
					document.all.lnkShowCabinets.innerHTML = document.all.lnkShowCabinets.innerHTML + "<IMG src='images//arrow_left.gif' border=0>";
					hostName = document.all.hostname.value; 
					
				}
				
			</SCRIPT>
		</FORM>
	</BODY>
</HTML>

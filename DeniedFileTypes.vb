' According to http://msdn2.microsoft.com/en-us/library/system.web.httppostedfile.aspx 
' "Files are uploaded in MIME multipart/form-data format. 
' By default, all requests, including form fields and uploaded files, 
' larger than 256 KB are buffered to disk, rather than held in server memory." 
' So we can use an HttpHandler to handle uploaded files and not have to worry 
' about the server recycling the request do to low memory. 
' don't forget to increase the MaxRequestLength in the web.config. 
' If you server is still giving errors, then something else is wrong. 
' I've uploaded a 1.3 gig file without any problems. One thing to note, 
' when the SaveAs function is called, it takes time for the server to 
' save the file. The larger the file, the longer it takes. 
' So if a progress bar is used in the upload, it may read 100%, but the upload won't 
' be complete until the file is saved. So it may look like it is stalled, but it 
' is not. 

Imports System
Imports System.Data
Imports System.Configuration
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.IO

''' <summary> 
''' Upload handler for uploading files. 
''' </summary> 

Public Class DeniedFileTypes
    Implements System.Web.IHttpHandler

    Public Sub New()
    End Sub

#Region "IHttpHandler Members"

    Public ReadOnly Property IsReusable() As Boolean Implements System.Web.IHttpHandler.IsReusable
        Get
            Return True
        End Get
    End Property

    Public Sub ProcessRequest(ByVal context As HttpContext) Implements System.Web.IHttpHandler.ProcessRequest

        context.Response.Redirect("Default.aspx")
        'Dim ws As New Images.docAssistImages
        'Try

        '    If context.Request.Files.Count > 0 Then

        '        Dim objUser As Accucentric.docAssist.Web.Security.User
        '        objUser = BuildUserObject(context.Request.Cookies("docAssist"))

        '        ' get the applications path 
        '        Dim tempFile As String = context.Request.PhysicalApplicationPath

        '        ws.Url = "https://my.docassist.com/docassistimages.asmx"

        '        Try

        '            If context.Request.Url.ToString.StartsWith("http://dev.docassist") Then
        '                ws.Url = "https://dev.docassist.com/docAssistWeb1/docassistimages.asmx"
        '            ElseIf context.Request.Url.ToString.StartsWith("https://stage.docassist") Then
        '                ws.Url = "https://stage.docassist.com/docassistimages.asmx"
        '            ElseIf context.Request.Url.ToString.StartsWith("https://my.docassist") Then
        '                ws.Url = "https://my.docassist.com/docassistimages.asmx"
        '            ElseIf context.Request.Url.ToString.IndexOf("localhost") > 0 Then
        '                ws.Url = "https://localhost/docAssistWeb1/docassistimages.asmx"
        '            End If

        '        Catch ex As Exception
        '            ws.Url = "https://my.docassist.com/docassistimages.asmx"
        '        End Try

        '        For j As Integer = 0 To context.Request.Files.Count - 1

        '            Dim uploadFile As HttpPostedFile = context.Request.Files(j)

        '            If uploadFile.ContentLength > 0 Then

        '                Dim fileName As String = uploadFile.FileName
        '                tempFile = context.Server.MapPath("../tmp") & "\" & System.Guid.NewGuid.ToString.Replace("-", "") & ".tmp"
        '                uploadFile.SaveAs(tempFile)

        '                Dim folderId As Integer = CInt(context.Request.Cookies("docAssistUpload")("BulkFolderId"))

        '                Dim x As Byte()
        '                ws.InsertAttachment(objUser.SessionID.ToString, objUser.AccountId.ToString, objUser.ImagesUserId, folderId, fileName, GetDataAsByteArray(tempFile), uploadFile.ContentLength)

        '                'Dim logfile As String = context.Server.MapPath("../tmp") & "\" & System.Guid.NewGuid.ToString.Replace("-", "") & ".txt"
        '                'Dim fs As New System.IO.StreamWriter(logfile)
        '                'fs.write(fileName & " inserted.")
        '                'fs.Close()

        '                Try
        '                    System.IO.File.Delete(tempFile)
        '                Catch ex As Exception

        '                End Try

        '            End If

        '        Next

        '    End If

        '    ' Used as a fix for a bug in mac flash player that makes the 
        '    HttpContext.Current.Response.Write(" ")

        'Catch ex As Exception

        '    Dim logfile As String = context.Server.MapPath("../tmp") & "\" & "MultiUploadError" & System.Guid.NewGuid.ToString.Replace("-", "") & ".txt"
        '    Dim fs As New System.IO.StreamWriter(logfile)
        '    fs.Write(ws.Url & Chr(13) & ex.Message & Chr(13) & ex.StackTrace)
        '    fs.Close()

        '    'Throw ex

        'End Try

    End Sub

    Public Function BuildUserObject(ByRef cookie As HttpCookie) As Accucentric.docAssist.Web.Security.User

        Dim rValue As Accucentric.docAssist.Web.Security.User
        Try
            If cookie Is Nothing Then Err.Raise(5)

            'Dim strUserId As String = cookie("UserId")
            'Dim strUserId As String = Accucentric.docAssist.Web.Data.Functions.guidSessionId.ToString
            Dim strUserId As String = cookie("UserId")

            If Not strUserId Is Nothing Then
                If strUserId.Trim.Length <> "36" Then
                    Err.Raise(5)
                Else
                    rValue = New Accucentric.docAssist.Web.Security.User(strUserId, ConfigurationSettings.AppSettings("Connection"))

                End If

                If rValue Is Nothing Then
                    Err.Raise(5)
                End If

                Dim guidAccountId As New System.Guid(cookie("AccountId"))
                If Not rValue.UserId.ToString = System.Guid.Empty.ToString Then
                    If Not rValue.AccountId.ToString = guidAccountId.ToString Then
                        rValue.AccountId = guidAccountId
                    End If
                Else
                    Err.Raise(5)
                End If
            Else
                Err.Raise(5)
            End If
        Catch ex As Exception
            If Err.Number = 5 Then
                Throw New Exception("User is not logged on")
            Else
                Throw New Exception(ex.Message)
            End If
        End Try

        Return rValue

    End Function

    Public Function GetDataAsByteArray(ByVal strFilename As String) As Byte()

        Dim fsData As System.IO.FileStream
        Dim xTemp() As Byte

        fsData = New System.IO.FileStream(strFilename, IO.FileMode.Open, IO.FileAccess.Read)
        Dim len As Integer = CType(fsData.Length, Integer)
        Dim rData As New System.IO.BinaryReader(fsData)

        xTemp = rData.ReadBytes(len)

        rData.Close()
        fsData.Close()

        Return xTemp

    End Function

#End Region

End Class

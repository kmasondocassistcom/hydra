<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="UserEdit.aspx.vb" Inherits="docAssistWeb.UserEdit" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>docAssist - User Permissions</title>
    <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <link href="Styles.css" type="text/css" rel="stylesheet">
    <base target="_self">
</head>
<body id="pageBody" runat="server">
    <form id="Form1" method="post" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table class="PageContent" cellspacing="0" cellpadding="0" width="650" border="0">
                <!-- Parent grey container -->
                <tr>
                    <td>
                        <div class="Container">
                            <div class="north">
                                <div class="east">
                                    <div class="south">
                                        <div class="west">
                                            <div class="ne">
                                                <div class="se">
                                                    <div class="sw">
                                                        <div class="nw">
                                                            <table cellspacing="0" cellpadding="0" width="95%" border="0">
                                                                <tr>
                                                                    <td nowrap align="left">
                                                                        <span class="Heading">User Information:</span>
                                                                    </td>
                                                                    <td width="100%">
                                                                    </td>
                                                                    <td nowrap align="right">
                                                                        <asp:CheckBox ID="chkGenPw" TabIndex="-6" Text="Generate Password" CssClass="Text"
                                                                            runat="server" Visible="False"></asp:CheckBox><asp:CheckBox ID="chkUserEnabled" runat="server"
                                                                                CssClass="Text" Text="Enabled" TabIndex="-6" Checked="True"></asp:CheckBox>&nbsp;
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <table cellspacing="0" cellpadding="0" width="95%" border="0">
                                                                <tr>
                                                                    <td align="right">
                                                                        <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                                                            <tr>
                                                                                <td nowrap align="right">
                                                                                    <img height="1" src="spacer.gif" width="7">
                                                                                </td>
                                                                                <td nowrap align="right">
                                                                                    <span class="Text" id="EMailAddressLabel">E-Mail Address:</span> <span class="Text"
                                                                                        id="ReadOnlyEmail" style="width: 200px; text-align: left" runat="server"></span>
                                                                                    <asp:TextBox ID="txtEmailAddress" runat="server" Width="200px" CssClass="Input" TabIndex="1"></asp:TextBox>
                                                                                </td>
                                                                                <td nowrap align="right">
                                                                                </td>
                                                                                <td nowrap align="right" width="100%">
                                                                                    <span class="Text" id="PasswordLabel">Password:</span>
                                                                                    <asp:TextBox ID="txtPassword" runat="server" Width="200px" CssClass="Input" TextMode="Password"
                                                                                        TabIndex="4"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td nowrap align="right">
                                                                                </td>
                                                                                <td nowrap align="right">
                                                                                    <span class="Text" id="FirstNameLabel">First Name:</span>
                                                                                    <asp:TextBox ID="txtFirstName" runat="server" Width="200px" CssClass="Input" TabIndex="2"></asp:TextBox>
                                                                                </td>
                                                                                <td nowrap align="right">
                                                                                </td>
                                                                                <td nowrap align="right">
                                                                                    <span class="Text" id="PasswordConfirmLabel">Confirm Password:</span>
                                                                                    <asp:TextBox ID="txtPasswordConfirm" runat="server" Width="200px" CssClass="Input"
                                                                                        TextMode="Password" TabIndex="5"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td nowrap align="right">
                                                                                </td>
                                                                                <td nowrap align="right">
                                                                                    <span class="Text" id="LastNameLabel">Last Name:</span>
                                                                                    <asp:TextBox ID="txtLastName" runat="server" Width="200px" CssClass="Input" TabIndex="3"></asp:TextBox>
                                                                                </td>
                                                                                <td nowrap align="right">
                                                                                    <img height="1" src="spacer.gif" width="7">
                                                                                </td>
                                                                                <td nowrap align="right">
                                                                                    <span class="Text" id="PhoneNumberLabel">Phone Number:</span>
                                                                                    <asp:TextBox ID="txtPhoneNumber" runat="server" Width="200px" CssClass="Input" TabIndex="6"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td nowrap align="right">
                                                                                </td>
                                                                                <td nowrap align="left">
                                                                                    <img height="1" src="spacer.gif" width="90">
                                                                                    <asp:CheckBox ID="chkForcePasswordChange" runat="server" CssClass="Text" Text="Force password change."
                                                                                        TabIndex="-1"></asp:CheckBox>
                                                                                </td>
                                                                                <td nowrap align="right">
                                                                                </td>
                                                                                <td nowrap align="right">
                                                                                    <span class="Text" id="UserTypeLabel" runat="server">User Type:</span>
                                                                                    <asp:DropDownList ID="ddlUserType" runat="server" Width="200px" CssClass="Input"
                                                                                        TabIndex="7">
                                                                                        <asp:ListItem Value="0">Imaging User</asp:ListItem>
                                                                                        <asp:ListItem Value="2">Standard User</asp:ListItem>
                                                                                        <asp:ListItem Value="3">Read Only</asp:ListItem>
                                                                                    </asp:DropDownList>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                        <div id="groupSection" runat="server">
                                                                            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                                                                <tr>
                                                                                    <td nowrap align="left">
                                                                                        <span class="Heading">Group Membership:</span>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                                                                <tr>
                                                                                    <td width="50%">
                                                                                        <img height="1" src="spacer.gif" width="10">
                                                                                    </td>
                                                                                    <td>
                                                                                        <span class="Text">Available Groups</span><br>
                                                                                        <asp:ListBox ID="lstAvailable" runat="server" Width="262px" Height="175px" SelectionMode="Multiple">
                                                                                        </asp:ListBox>
                                                                                    </td>
                                                                                    <td>
                                                                                        <img height="1" src="spacer.gif" width="10">
                                                                                    </td>
                                                                                    <td valign="top">
                                                                                        <br>
                                                                                        <br>
                                                                                        <asp:ImageButton ID="btnAddAll" runat="server" ImageUrl="Images/addall.gif"></asp:ImageButton><br>
                                                                                        <br>
                                                                                        <asp:ImageButton ID="btnAddOne" runat="server" ImageUrl="Images/addone.gif"></asp:ImageButton><br>
                                                                                        <br>
                                                                                        <asp:ImageButton ID="btnRemoveOne" runat="server" ImageUrl="Images/removeone.gif">
                                                                                        </asp:ImageButton><br>
                                                                                        <br>
                                                                                        <asp:ImageButton ID="btnRemoveAll" runat="server" ImageUrl="Images/removeall.gif">
                                                                                        </asp:ImageButton><br>
                                                                                    </td>
                                                                                    <td>
                                                                                        <img height="1" src="spacer.gif" width="10">
                                                                                    </td>
                                                                                    <td>
                                                                                        <span class="Text">Assigned Groups</span><br>
                                                                                        <asp:ListBox ID="lstAssigned" runat="server" Width="262px" Height="175px" SelectionMode="Multiple">
                                                                                        </asp:ListBox>
                                                                                    </td>
                                                                                    <td>
                                                                                        <img height="1" src="spacer.gif" width="10">
                                                                                    </td>
                                                                                    <td width="50%">
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                        <br>
                                                                        <table cellspacing="0" cellpadding="0" width="100%" border="0" class="PageContent">
                                                                            <tr>
                                                                                <td align="left">
                                                                                    <img height="1" src="spacer.gif" width="5">
                                                                                    <asp:Label ID="lblError" runat="server" CssClass="ErrorText" Visible="False"></asp:Label>
                                                                                </td>
                                                                                <td nowrap align="right">
                                                                                    <a href="javascript:window.close()">
                                                                                        <img src="Images/smallred_cancel.gif" border="0" id="IMG1" runat="server"></a>
                                                                                    <img height="1" src="spacer.gif" width="5">
                                                                                    <asp:ImageButton ID="btnSave" runat="server" ImageUrl="Images/smallred_save.gif">
                                                                                    </asp:ImageButton>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
            &nbsp;
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:Literal ID="litOut" runat="server"></asp:Literal>
    </form>

    <script language="javascript">

        function ignoreEvent() {
            if (event.keyCode == 13) {
                event.cancelBubble = true;
                event.returnValue = false;
            }
        }

        function checkForce() {
            var check = document.getElementById('chkForcePasswordChange');
            check.checked = true;
        }

        function generatePassword() {
            if (document.getElementById('chkGenPw').checked) {
                document.getElementById('txtPassword').value = '***************';
                document.getElementById('txtPasswordConfirm').value = '***************';
                document.getElementById('txtPassword').disabled = true;
                document.getElementById('txtPasswordConfirm').disabled = true;
            }
            else {
                document.getElementById('txtPassword').value = '';
                document.getElementById('txtPasswordConfirm').value = '';
                document.getElementById('txtPassword').disabled = false;
                document.getElementById('txtPasswordConfirm').disabled = false;
            }
        }
			
    </script>

</body>
</html>

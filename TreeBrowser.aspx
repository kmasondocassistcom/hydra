<%@ Register TagPrefix="componentart" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="TreeBrowser.aspx.vb" Inherits="docAssistWeb.TreeBrowser"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Choose Folder</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
		<base target="_self">
	</HEAD>
	<body>
		<form class="PageContent" id="Form1" method="post" runat="server">
			<DIV id="svcImages" style="BEHAVIOR: url(webservice.htc)"></DIV>
			<TABLE style="BORDER-RIGHT: #cccccc 2px solid; BORDER-TOP: #cccccc 2px solid; BORDER-LEFT: #cccccc 2px solid; BORDER-BOTTOM: #cccccc 2px solid"
				height="500" cellSpacing="0" cellPadding="0" width="275" border="0">
				<TR>
					<TD noWrap bgColor="#ffffff">
						<table width="100%">
							<tr>
								<td noWrap>
									<table class="menu" cellSpacing="0" width="100%" border="0">
										<tr style="BACKGROUND-POSITION-Y: bottom; BACKGROUND-IMAGE: url(Images/Search/quickMenu/bar.png); BACKGROUND-REPEAT: repeat-x">
											<th onclick="showNewMenu();" width="50%">
												<IMG id="quickMenuIcon" height="22" src="Images/Search/quickMenu/expand.gif" width="22"
													align="right">New
											</th>
											<th onclick="showEditMenu();" width="50%">
												<IMG id="editMenuIcon" height="22" src="Images/Search/quickMenu/expand.gif" width="22"
													align="right">Edit
											</th>
										</tr>
									</table>
									<table class="menu" id="quickMenu" onmouseover="showMenu();" style="BORDER-RIGHT: #cccccc 2px solid; BORDER-TOP: #cccccc 0px solid; DISPLAY: none; BORDER-LEFT: #cccccc 2px solid; BORDER-BOTTOM: #cccccc 2px solid"
										onmouseout="hideMenu();" cellSpacing="2" cellPadding="0" width="50%" bgColor="#ffffff"
										border="0" runat="server">
										<tr>
											<td class="disabled" id="tdNewFolder"><IMG id="imgNewFolder" height="20" src="Images/folders/folder.add.btn.gif" width="20"
													align="left">&nbsp;Folder
											</td>
										</tr>
										<tr>
											<td id="tdNewCabinet"><IMG id="imgNewCabinet" height="20" src="Images/folders/cabinet.add.btn.gif" width="20"
													align="left">&nbsp;Cabinet
											</td>
										</tr>
									</table>
									<table class="menu" id="editMenu" onmouseover="showEdit();" style="BORDER-RIGHT: #cccccc 2px solid; BORDER-TOP: #cccccc 0px solid; DISPLAY: none; BORDER-LEFT: #cccccc 2px solid; BORDER-BOTTOM: #cccccc 2px solid"
										onmouseout="hideEditMenu();" cellSpacing="2" cellPadding="0" width="50%" bgColor="#ffffff"
										border="0" runat="server">
										<tr runat="server">
											<td id="tdRename"><IMG id="imgRename" height="20" src="Images/folders/rename.btn.gif" width="20" align="left">&nbsp;Rename
											</td>
										</tr>
										<tr runat="server">
											<td id="tdDelete"><IMG id="imgDelete" height="20" src="Images/folders/remove.btn.gif" width="20" align="left">&nbsp;Delete
											</td>
										</tr>
										<tr runat="server">
											<td class="disabled" id="tdSecurity"><IMG id="imgSecurity" height="20" src="Images/folders/properties.btn.off.gif" width="20"
													align="left">&nbsp;Properties
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<TABLE class="PageContent" style="BORDER-RIGHT: #eaf0f7 1px solid; BORDER-TOP: #eaf0f7 1px solid; BORDER-LEFT: #eaf0f7 1px solid; BORDER-BOTTOM: #eaf0f7 1px solid"
							height="480" width="100%">
							<tr>
								<td></td>
							</tr>
							<tr>
								<td vAlign="top">
									<DIV class="PageContent" id="divTree" style="OVERFLOW: auto; WIDTH: 255px; HEIGHT: 499px; BACKGROUND-COLOR: #ffffff"
										runat="server"><componentart:treeview id="treeSearch" runat="server" HoverNodeCssClass="NodeHover" SelectedNodeCssClass="SelectedNode"
											NodeRowCssClass="TreeFont" ShowLines="True" LineImagesFolderUrl="images/lines/" AutoScroll="False" ClientScriptLocation="./treeview_scripts"
											CollapseNodeOnSelect="False" ItemSpacing="0" ClientSideOnNodeRename="treeSearch_onNodeRename" ClientSideOnNodeExpand="Expand"
											ClientSideOnNodeCollapse="Collapse" ClientSideOnNodeCheckChanged="NodeChecked" ContentLoadingImageUrl="./images/spinner.gif"
											ClientSideOnNodeSelect="PopupNodeClick" CollapseDuration="0" ExpandDuration="0" ExpandSlide="None" ExpandSelectedPath="False"
											Width="100%"></componentart:treeview></DIV>
								</td>
							</tr>
							<tr>
								<td align="right" height="20"><A href="#"><IMG onclick="NodeClick();" src="Images/save_small.gif" border="0"></A>
								</td>
							</tr>
						</TABLE>
					</TD>
				</TR>
			</TABLE>
			</TD></TR></TD></TABLE>
			<script language="javascript" src="scripts/searchToolbar.js"></script>
			<script language="javascript">
				function setValue(val){
					document.getElementById('NodeValue').value = val;
				}
				function NodeClick(){
					var NodeValue = document.getElementById('NodeValue');
					
					if ((NodeValue.value	== "CabsFolders") || (NodeValue.value == "")) {
						alert('You must choose a valid folder.');
						return false;
					}
					
					if (NodeValue.value.substring(0,1) == "S") {
						alert('Cannot save to a cabinet. Please choose a folder.');
						return false;
					}

					if (NodeValue.value == "NOACCESS") {
						alert('You do not have access to save files in this folder.');
						return false;
					}
					
					//loop client side to get folder path instead of db hit
					var node = window['treeSearch'].FindNodeById(NodeValue.value);
					var path = node.Text;
					var newnode = node;

					do { 
						newnode = newnode.get_parentNode();
						path = newnode.Text + '/' + path; 
					} while (newnode.ID.substring(0,1) != "S"); 

					var pass = NodeValue.value + "�" + node.Text;
					if (pass.substring(0,1) == "W") {
						alert(NodeValue.value + "�" + path.replace('&#43;','+'));
						//window.returnValue = node.ID + "�" + path.replace('&#43;','+'); 
						//window.close();
					}
				}
			</script>
			<INPUT id="NodeId" type="hidden" runat="server"> <INPUT id="NewText" type="hidden" runat="server">
			<INPUT id="NodeText" type="hidden" runat="server"> <INPUT id="ParentFolder" type="hidden" runat="server">
			<INPUT id="NodeValue" type="hidden" runat="server" NAME="Hidden1">
			<asp:imagebutton id="btnRename" runat="server" Width="0px" Height="0px"></asp:imagebutton></form>
	</body>
</HTML>

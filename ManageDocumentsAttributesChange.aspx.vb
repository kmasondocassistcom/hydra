Partial Class ManageDocumentsAttributesChange
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region




    Private mobjUser As Accucentric.docAssist.Web.Security.User
    Private mconSqlImage As SqlClient.SqlConnection

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        txtDocumentName.Attributes("onkeypress") = "catchSumbit();"

        If Not Page.IsPostBack Then
            txtDocumentName.Text = Session("mAdminDocumentName")
            txtDocumentName.ReadOnly = True
            PopulateAttributeList(Session("mAdminDocumentID"))
        End If

    End Sub




    Private Function PopulateAttributeList(ByVal iDocumentID As Integer) As Integer
        Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
        Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))

        Me.mobjUser.AccountId = guidAccountId
        Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
        Me.mconSqlImage.Open()

        Dim cmdSql As SqlClient.SqlCommand

        Dim dtsql As New DataTable
        cmdSql = New SqlClient.SqlCommand("acsAdminDocumentAttributesGet", Me.mconSqlImage)
        cmdSql.CommandType = CommandType.StoredProcedure
        cmdSql.Parameters.Add("@DocumentID", iDocumentID)
        Dim dasql As New SqlClient.SqlDataAdapter(cmdSql)
        dasql.Fill(dtsql)


        If dtsql.Rows.Count > 0 Then
            SelectAttributesAssigned(dtsql)

        Else
            Return 0

        End If



    End Function

    Private Sub SelectAttributesAssigned(ByVal dtsql As DataTable)
        Dim dr As DataRow

        lstAttributes.Items.Clear()
        For Each dr In dtsql.Rows
            Dim lstitem As New System.Web.UI.WebControls.ListItem

            lstitem.Value = dr("AttributeID")
            If dr("HasData") = True Then
                lstitem.Text = dr("AttributeName") & " [Contains indexed data]"
            Else
                lstitem.Text = dr("AttributeName")
            End If

            lstitem.Selected = dr("Selected")
            lstAttributes.Items.Add(lstitem)
        Next


    End Sub


    Private Function DocumentExists(ByVal strDocumentName As String) As Boolean
        Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
        Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))

        Me.mobjUser.AccountId = guidAccountId
        Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
        Me.mconSqlImage.Open()

        Dim cmdSql As SqlClient.SqlCommand

        Dim dtsql As New DataTable
        cmdSql = New SqlClient.SqlCommand("acsAdminDocumentCheck", Me.mconSqlImage)
        cmdSql.CommandType = CommandType.StoredProcedure
        cmdSql.Parameters.Add("@DocumentName", strDocumentName)
        Dim dasql As New SqlClient.SqlDataAdapter(cmdSql)
        dasql.Fill(dtsql)


        If dtsql.Rows.Count > 0 Then
            Return True
        Else
            Return False

        End If
    End Function

    Private Sub lnkFinish_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)


        If DocumentExists(txtDocumentName.Text) And txtDocumentName.Text <> Session("mAdminDocumentName") Then
            lblError.Text = "Document " & txtDocumentName.Text & " already exists."
            Exit Sub
        End If

        Session("AttributesSelected") = AttributesSelected()
        If Session("AttributesSelected").rows.count <= 0 Then
            lblError.Text = "You must select at least one attribute."
            Exit Sub
        End If

        lblError.Text = ""
        Response.Redirect("ManageDocumentsAttributesOrderChange.aspx")


    End Sub

    Private Function UpdateDocument(ByVal strDocumentName As String) As Boolean

        Dim cmdSql As SqlClient.SqlCommand
        Dim dtsql As New DataTable
        cmdSql = New SqlClient.SqlCommand("acsAdminDocumentChange", Me.mconSqlImage)
        cmdSql.CommandType = CommandType.StoredProcedure
        cmdSql.Parameters.Add("@DocumentID", Session("mAdminDocumentID"))
        cmdSql.Parameters.Add("@NewDocumentName", strDocumentName)
        cmdSql.ExecuteNonQuery()

    End Function



    Private Function UpdateDocumentFolders() As Boolean



        Dim cmdSql As SqlClient.SqlCommand
        Dim dtsql As New DataTable
        Dim drFolder As DataRow
        dtsql = Session("FoldersSelected")

        For Each drFolder In dtsql.Rows
            cmdSql = New SqlClient.SqlCommand("acsAdminDocumentFoldersChange", Me.mconSqlImage)
            cmdSql.CommandType = CommandType.StoredProcedure
            cmdSql.Parameters.Add("@DocumentID", Session("mAdminDocumentID"))
            cmdSql.Parameters.Add("@FolderID", drFolder("FolderID"))
            cmdSql.ExecuteNonQuery()
            cmdSql.Parameters.Clear()
        Next


    End Function


    Private Sub UpdateDocumentAttributes(ByVal iNewDocumentID As Integer)

        Dim cmdSql As SqlClient.SqlCommand

        'Clear any existing document attributes
        cmdSql = New SqlClient.SqlCommand("acsAdminDocumentAttributesClear", Me.mconSqlImage)
        cmdSql.CommandType = CommandType.StoredProcedure
        cmdSql.Parameters.Add("@DocumentID", iNewDocumentID)
        cmdSql.ExecuteNonQuery()

        Dim icount As Integer = lstAttributes.Items.Count
        Dim x As Integer

        For x = 0 To icount - 1

            cmdSql = New SqlClient.SqlCommand("acsAdminDocumentAttributesAdd", Me.mconSqlImage)
            cmdSql.CommandType = CommandType.StoredProcedure
            cmdSql.Parameters.Add("@DocumentID", iNewDocumentID)
            cmdSql.Parameters.Add("@AttributeID", lstAttributes.Items(x).Value)
            cmdSql.Parameters.Add("@AttributeDisplayOrder", x + 1)
            cmdSql.Parameters.Add("@Required", lstAttributes.Items(x).Selected)
            cmdSql.ExecuteNonQuery()

        Next


    End Sub


    Private Function AttributesSelected() As DataTable
        Dim dtAttributes As New DataTable
        Dim dcID As New DataColumn("AttributeID", GetType(System.Int64), Nothing)
        Dim dcName As New DataColumn("AttributeName", GetType(System.String), Nothing)
        dtAttributes.Columns.Add(dcID)
        dtAttributes.Columns.Add(dcName)


        Dim iCount As Integer = lstAttributes.Items.Count
        Dim x


        For x = 0 To iCount - 1
            If lstAttributes.Items(x).Selected = True Then
                Dim dr As DataRow
                dr = dtAttributes.NewRow
                dr("AttributeID") = lstAttributes.Items(x).Value
                dr("AttributeName") = lstAttributes.Items(x).Text
                dtAttributes.Rows.Add(dr)
            End If

        Next

        Return dtAttributes

    End Function


    Private Sub btnNext_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnNext.Click

        If DocumentExists(txtDocumentName.Text) And txtDocumentName.Text <> Session("mAdminDocumentName") Then
            lblError.Text = "Document " & txtDocumentName.Text & " already exists."
            Exit Sub
        End If

        Session("AttributesSelected") = AttributesSelected()
        If Session("AttributesSelected").rows.count <= 0 Then
            lblError.Text = "You must select at least one attribute."
            Exit Sub
        End If

        lblError.Text = ""
        Response.Redirect("ManageDocumentsAttributesOrderChange.aspx")
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("ManageDocuments.aspx")

    End Sub
End Class

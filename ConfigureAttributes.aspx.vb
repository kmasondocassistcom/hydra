Imports Accucentric.docAssist

Partial Class _Attributes
    Inherits System.Web.UI.Page

    Private mobjUser As Web.Security.User
    Private mconSqlImage As SqlClient.SqlConnection

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        '
        ' 
        '
        Try
            If Request.Cookies("docAssist") Is Nothing Then
                Response.Redirect("Default.aspx")
                Exit Sub
            End If

            Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
            Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))

            Me.mobjUser.AccountId = guidAccountId
            Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)

        Catch ex As Exception
            Response.Redirect("default.aspx")
            Response.Flush()
            Response.End()
        End Try
        '
        ' Add object events
        '
        btnSave.Attributes("OnClick") = "SelectAllAssigned();"
        ddlDataType.Attributes("OnChange") = "EnableList();"
        lstAssigned.Attributes("OnChange") = "AttributeSelected(this);"
        lstAvailable.Attributes("OnChange") = "AttributeSelected(this);"
        ddlDocuments.Attributes("OnChange") = "ChangeDocuments();"
        '
        ' Load up the objects
        '
        If Not Page.IsPostBack Then
            Try
                hostname.Value = Request.Url.AbsoluteUri.Substring(0, Request.Url.AbsoluteUri.Length - Request.Url.AbsolutePath.Length)
#If DEBUG Then
                hostname.Value = hostname.Value & "/docAssistWeb1"
#End If

                If Not Session("Attributes") Is Nothing Then Session("Attributes") = Nothing
                If Not Session("Documents") Is Nothing Then Session("Documents") = Nothing
                If Not Session("CabinetFolders") Is Nothing Then Session("CabinetFolders") = Nothing

                Dim dtDocuments As New data.Images.Documents
                If Not Me.mconSqlImage.State = ConnectionState.Open Then Me.mconSqlImage.Open()
                '
                ' Load the documents
                '
                dtDocuments.Fill(False, Me.mconSqlImage)
                Dim dv As New DataView(dtDocuments, Nothing, dtDocuments.DocumentName.ColumnName, DataViewRowState.CurrentRows)

                ddlDocuments.DataSource = dv
                ddlDocuments.DataTextField = dtDocuments.DocumentName.ColumnName
                ddlDocuments.DataValueField = dtDocuments.DocumentId.ColumnName
                ddlDocuments.DataBind()
                '
                ' Load all the attributes and save them to a session that gets updated by web services
                '
                Dim dsConfig As dsConfiguration = GetConfiguration()
                Session("Attributes") = dsConfig

            Catch ex As Exception
                Throw ex
            Finally
                If Not Me.mconSqlImage.State = ConnectionState.Closed Then Me.mconSqlImage.Close()
            End Try
        End If


    End Sub

    Private Function GetConfiguration() As dsConfiguration

        Dim ds As New dsConfiguration

        Dim cmdSqlAttributes As New SqlClient.SqlCommand("acsAttributes", Me.mconSqlImage)
        Dim cmdSqlAttributeList As New SqlClient.SqlCommand("SELECT * FROM AttributesLists", Me.mconSqlImage)
        Dim cmdSqlDocuments As New SqlClient.SqlCommand("acsDocuments", Me.mconSqlImage)

        cmdSqlAttributes.CommandTimeout = DEFAULT_COMMAND_TIMEOUT
        cmdSqlAttributes.CommandType = CommandType.StoredProcedure

        cmdSqlAttributeList.CommandTimeout = DEFAULT_COMMAND_TIMEOUT

        cmdSqlDocuments.CommandTimeout = DEFAULT_COMMAND_TIMEOUT
        cmdSqlDocuments.CommandType = CommandType.StoredProcedure
        cmdSqlDocuments.Parameters.Add("@UseSecurity", 0)

        If Not Me.mconSqlImage.State = ConnectionState.Open Then Me.mconSqlImage.Open()

        Dim da As New SqlClient.SqlDataAdapter

        da.SelectCommand = cmdSqlAttributes
        da.Fill(ds.Attributes)

        da.SelectCommand = cmdSqlDocuments
        da.Fill(ds.Documents)

        da.SelectCommand = cmdSqlAttributeList
        da.Fill(ds.AttributesLists)

        Return ds

    End Function

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click

        Dim dsConfig As dsConfiguration = CType(Session("Attributes"), dsConfiguration)
        '
        ' Get the document attributes from the form
        '
        Dim dtFormDocumentAttributes As New DataTable
        dtFormDocumentAttributes.Columns.Add(New DataColumn("AttributeId", GetType(System.Int32)))
        dtFormDocumentAttributes.Columns.Add(New DataColumn("DocumentId", GetType(System.Int32)))
        dtFormDocumentAttributes.Columns.Add(New DataColumn("DisplayOrder", GetType(System.Int32)))
        dtFormDocumentAttributes.PrimaryKey = New DataColumn() {dtFormDocumentAttributes.Columns("AttributeId")}
        Dim intCount As Integer = 0
        Dim dr As DataRow
        Dim lstItems() As String = hiddenDocumentAttributes.Value.Split(",")
        For Each lstItem As String In lstItems
            intCount += 1
            dr = dtFormDocumentAttributes.NewRow
            dr("AttributeId") = CInt(lstItem)
            dr("DocumentId") = ddlDocuments.SelectedValue
            dr("DisplayOrder") = intCount
            dtFormDocumentAttributes.Rows.Add(dr)
        Next
        '
        ' Get the existing document attributes
        '
        Dim cmdSql As New SqlClient.SqlCommand("acsDocumentAttributesByDocumentId", Me.mconSqlImage)
        cmdSql.CommandTimeout = DEFAULT_COMMAND_TIMEOUT
        cmdSql.CommandType = CommandType.StoredProcedure
        cmdSql.Parameters.Add("@DocumentId", ddlDocuments.SelectedValue)
        Dim da As New SqlClient.SqlDataAdapter(cmdSql)
        da.Fill(dsConfig.DocumentAttribute)
        dsConfig.DocumentAttribute.PrimaryKey = New DataColumn() {dsConfig.DocumentAttribute.Columns("AttributeId")}
        '
        ' Check for new rows / modify order
        '
        Dim drConfigDocumentAttribute As dsConfiguration.DocumentAttributeRow
        For Each dr In dtFormDocumentAttributes.Rows
            Dim intSearch As Integer = dr("AttributeId")
            drConfigDocumentAttribute = dsConfig.DocumentAttribute.Rows.Find(intSearch)
            If drConfigDocumentAttribute Is Nothing Then
                drConfigDocumentAttribute = dsConfig.DocumentAttribute.NewRow
                drConfigDocumentAttribute.AttributeID = dr("AttributeId")
                drConfigDocumentAttribute.DocumentID = dr("DocumentId")
                dsConfig.DocumentAttribute.Rows.Add(drConfigDocumentAttribute)
            End If
            drConfigDocumentAttribute.AttributeDisplayOrder = dr("DisplayOrder")
        Next
        '
        ' Check for deleted rows
        '
        For Each drConfigDocumentAttribute In dsConfig.DocumentAttribute.Rows
            Dim intSearch As Integer
            intSearch = drConfigDocumentAttribute.AttributeID
            dr = dtFormDocumentAttributes.Rows.Find(intsearch)
            If dr Is Nothing Then
                drConfigDocumentAttribute.Delete()
            End If
        Next
        '
        ' Save the changes back to the server
        '
        Dim trnSql As SqlClient.SqlTransaction
        Try
            Me.mconSqlImage.Open()
            trnSql = Me.mconSqlImage.BeginTransaction
            Data.Images.UpdateAttributes(dsConfig.Attributes, Me.mconSqlImage, trnSql)
            Data.Images.UpdateAttributesLists(dsConfig.AttributesLists, Me.mconSqlImage, trnSql)
            Data.Images.UpdateDocumentAttributes(dsConfig.DocumentAttribute, Me.mconSqlImage, trnSql)

            trnSql.Commit()

            showMessage.InnerHtml = "Attributes were saved succesfully."
        Catch ex As Exception
            trnSql.Rollback()
#If DEBUG Then
            showMessage.InnerHtml = ex.Message & "<BR>" & ex.StackTrace
#Else
            showMessage.InnerHtml = "An error occured during the save.  Your changes have not been saved."
#End If

        Finally
            trnSql.Dispose()
            If Not Me.mconSqlImage.State = ConnectionState.Closed Then Me.mconSqlImage.Close()
        End Try


    End Sub
End Class

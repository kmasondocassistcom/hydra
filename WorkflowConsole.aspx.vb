Imports Accucentric.docAssist.Web.Security
Imports Accucentric.docAssist.Data.Images

Partial Class WorkflowConsole
    Inherits System.Web.UI.Page


    Private mobjUser As User
    Private mcookieSearch As cookieSearch


    Private dsSubmit As New DataSet

    Private intRouteId As Integer = 0


#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        '
        ' Check that user is authenticated.
        '
        Try
            If Request.Cookies("docAssist") Is Nothing Then
                Response.Redirect("Default.aspx")
                Exit Sub
            End If

            Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
            Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))

            Me.mcookieSearch = New cookieSearch(Me)

            Me.mobjUser.AccountId = guidAccountId
        Catch ex As Exception
            Response.Redirect("Default.aspx")
            Exit Sub
        End Try

        '
        ' Check that user should be seeing this screen.
        '
        'Try
        '    If CheckWorkflow(Me.mobjUser.ImagesUserId, mcookieSearch.VersionID, Me.mobjUser) = False Then
        '        Response.Redirect("Default.aspx")
        '        Exit Sub
        '    End If
        'Catch ex As Exception

        'End Try

        'Get active route id
        intRouteId = CInt(Request.QueryString("Route"))
        litRouteId.Text = ""


        '
        ' Load the needed page
        '
        Try
            Select Case CType(Request.QueryString("Mode"), Functions.WorkflowMode)
                Case Functions.WorkflowMode.ACTION_ACCESS_REVIEWER_PENDING
                    frameWorkflowConsole.Attributes("src") = "WorkflowReview.aspx?Mode=W"

            End Select

        Catch ex As Exception

        End Try

    End Sub

End Class

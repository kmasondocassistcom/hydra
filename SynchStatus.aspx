<%@ Register TagPrefix="uc1" TagName="TopRight" Src="Controls/TopRight.ascx" %>
<%@ Register TagPrefix="uc1" TagName="docHeader" Src="Controls/docHeader.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="SynchStatus.aspx.vb" Inherits="docAssistWeb.SynchStatus"%>
<%@ Register TagPrefix="uc1" TagName="AdminNav" Src="Controls/AdminNav.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>docAssist - Data Synchronization Status</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body class="Prefs" bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0" MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<TABLE height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TBODY>
					<TR>
						<TD width="100%" colSpan="3"><uc1:docheader id="docHeader" runat="server"></uc1:docheader></TD>
					</TR>
					<TR vAlign="middle" height="100%">
						<TD class="Navigation1" style="BORDER-RIGHT: #cccccc thin solid" vAlign="top" height="100%"><uc1:adminnav id="AdminNav2" runat="server"></uc1:adminnav><IMG height="1" src="dummy.gif" width="120"></TD>
						<TD vAlign="top" noWrap align="left" height="100%">&nbsp;
							<TABLE id="Table1" cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
								<TR>
									<TD width="20" rowSpan="6"></TD>
									<TD class="PrefHeader">Data Synchronization Status</TD>
								</TR>
								<TR>
									<TD align="left"><IMG height="1" src="dummy.gif" width="17">&nbsp;<IMG height="5" src="dummy.gif" width="1">
									</TD>
								</TR>
								<TR>
									<TD>
										<TABLE class="Prefs" id="Table2" cellSpacing="0" cellPadding="0" width="100%" align="center"
											border="0">
											<TR>
												<TD width="20"><IMG height="1" src="dummy.gif" width="20"></TD>
												<TD class="Text" noWrap align="left" width="100%"><SPAN class="Text" id="NoRows" runat="server"></SPAN><asp:label id="lblSynchInfo" runat="server"></asp:label><BR>
													<BR>
													<asp:label id="lblLastSynchs" runat="server" Visible="False">Last Successful Synchronizations</asp:label><BR>
													<asp:datagrid id="dgCompletedJobs" runat="server" Font-Names="Trebuchet MS" Font-Size="9pt" AutoGenerateColumns="False"
														Width="100%">
														<AlternatingItemStyle BackColor="WhiteSmoke"></AlternatingItemStyle>
														<HeaderStyle Font-Size="10pt" Font-Bold="True" BackColor="Gainsboro"></HeaderStyle>
														<Columns>
															<asp:BoundColumn DataField="JobName" HeaderText="Job Name">
																<HeaderStyle Wrap="False"></HeaderStyle>
																<ItemStyle Wrap="False"></ItemStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="LastSyncDate" HeaderText="Time">
																<HeaderStyle Wrap="False"></HeaderStyle>
																<ItemStyle Wrap="False"></ItemStyle>
															</asp:BoundColumn>
														</Columns>
													</asp:datagrid><BR>
													<asp:label id="lblPendingSynchs" runat="server" Visible="False">Pending Synchronizations</asp:label><asp:datagrid id="dgPendingJobs" runat="server" Font-Names="Trebuchet MS" Font-Size="9pt" AutoGenerateColumns="False"
														Width="100%">
														<AlternatingItemStyle BackColor="WhiteSmoke"></AlternatingItemStyle>
														<HeaderStyle Font-Size="10pt" Font-Bold="True" BackColor="Gainsboro"></HeaderStyle>
														<Columns>
															<asp:BoundColumn DataField="JobName" HeaderText="Job Name">
																<HeaderStyle Wrap="False"></HeaderStyle>
																<ItemStyle Wrap="False"></ItemStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="ScheduleDateTime" HeaderText="Scheduled Time">
																<HeaderStyle Wrap="False"></HeaderStyle>
																<ItemStyle Wrap="False"></ItemStyle>
															</asp:BoundColumn>
														</Columns>
													</asp:datagrid><BR>
													<asp:label id="lblCompleted" runat="server" Visible="False">Completed Synchronizations</asp:label>
													<asp:datagrid id="dgCompleted" runat="server" Font-Names="Trebuchet MS" Font-Size="9pt" AutoGenerateColumns="False"
														Width="100%">
														<AlternatingItemStyle BackColor="WhiteSmoke"></AlternatingItemStyle>
														<HeaderStyle Font-Size="10pt" Font-Bold="True" BackColor="Gainsboro"></HeaderStyle>
														<Columns>
															<asp:BoundColumn DataField="JobName" HeaderText="Job Name">
																<HeaderStyle Wrap="False"></HeaderStyle>
																<ItemStyle Wrap="False"></ItemStyle>
															</asp:BoundColumn>
															<asp:BoundColumn DataField="ScheduleDateTime" HeaderText="Time">
																<HeaderStyle Wrap="False"></HeaderStyle>
																<ItemStyle Wrap="False"></ItemStyle>
															</asp:BoundColumn>
														</Columns>
													</asp:datagrid></TD>
											</TR>
										</TABLE>
									</TD>
								</TR>
							</TABLE>
						</TD>
						<TD height="100%">&nbsp;</TD>
					</TR>
					<TR>
						<TD class="PageFooter" vAlign="bottom" bgColor="#cccccc" colSpan="3"><uc1:topright id="docFooter" runat="server"></uc1:topright></TD>
					</TR>
					<TR height="1">
						<TD style="BACKGROUND-COLOR: #959394" width="20"><IMG height="1" src="images/spacer.gif" width="20"></TD>
						<TD style="BACKGROUND-COLOR: #959394" width="100%"><IMG height="1" src="images/spacer.gif" width="100%"></TD>
						<TD style="BACKGROUND-COLOR: #959394" width="20"><IMG height="1" src="images/spacer.gif" width="20"></TD>
					</TR>
				</TBODY>
			</TABLE>
		</form>
	</body>
</HTML>

Imports Accucentric.docAssist.Data.Images

Partial Class Synchs
    Inherits System.Web.UI.Page


    Dim mobjUser As Accucentric.docAssist.Web.Security.User

    Dim mconSqlCompany As New SqlClient.SqlConnection
    Dim mconSqlSynchNotifications As New SqlClient.SqlConnection

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents btnSave As System.Web.UI.WebControls.ImageButton

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try
            Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
        Catch ex As Exception
            Response.Redirect("Default.aspx")
            Exit Sub
        End Try

        Try

            If Not Page.IsPostBack Then

                Me.mconSqlSynchNotifications = Functions.BuildConnection(Functions.ConnectionType.Notifications_DataSynch)
                Me.mconSqlCompany = Functions.BuildConnection(Me.mobjUser)

                Dim Sync As New DataSync(Me.mconSqlCompany, False)
                Dim notSync As New DataSync(Me.mconSqlSynchNotifications, False)

                Dim strSyncTable As String = System.Guid.NewGuid().ToString().Replace("-", "")

                Dim sqlCmd As New SqlClient.SqlCommand
                Dim sqlDa As New SqlClient.SqlDataAdapter

                Dim sb As New System.Text.StringBuilder

                Dim dtJobs As New DataTable
                dtJobs = Sync.DIJobListGet

                sb = New System.Text.StringBuilder
                sb.Append("CREATE TABLE ##" & strSyncTable.ToString & " (JobID int NULL, JobDetailID int NULL, JobType varchar(20) NULL, JobName varchar(75) NULL, ODBCName varchar(75) NULL, Enabled bit NOT NULL)" & vbCrLf)

                For Each dr As DataRow In dtJobs.Rows
                    sb.Append("INSERT INTO ##" & strSyncTable & " (JobID, JobDetailID, JobType, JobName, ODBCName,Enabled) VALUES (" & dr("JobID") & ", '" & dr("JobDetailID") & "', '" & dr("JobType") & "', '" & CStr(dr("JobName")).Replace("'", "''") & "', '" & dr("ODBCName") & "', '" & IIf(dr("Enabled") = True, "1", "0") & "')" & vbCrLf)
                Next

                sqlCmd = New SqlClient.SqlCommand(sb.ToString, Me.mconSqlSynchNotifications)
                Dim sqlBld As New SqlClient.SqlCommandBuilder
                sqlCmd.ExecuteNonQuery()

                Dim dt As New DataTable
                Dim fa As New FolderAlerts(Me.mconSqlSynchNotifications)
                dt = notSync.DataSyncGetSubscriberAlerts(Me.mobjUser.AccountId.ToString, Me.mobjUser.TimeDiffFromGMT, "##" & strSyncTable)

                If dt.Rows.Count = 0 Then
                    NoRows.InnerHtml = "No synchronizations have been defined for your account. Click the 'Add Synchronization' link to setup a notification."
                    dgSynch.Visible = False
                Else
                    dgSynch.Visible = True
                    dgSynch.DataSource = dt
                    dgSynch.DataBind()
                End If

            End If

        Catch ex As Exception

        Finally

            If Me.mconSqlSynchNotifications.State <> ConnectionState.Closed Then
                Me.mconSqlSynchNotifications.Close()
                Me.mconSqlSynchNotifications.Dispose()
            End If

            If Me.mconSqlCompany.State <> ConnectionState.Closed Then
                Me.mconSqlCompany.Close()
                Me.mconSqlCompany.Dispose()
            End If

        End Try

    End Sub

    'Private Function LoadDocuments() As DataTable

    '    Dim sqlCmd As SqlClient.SqlCommand = New SqlClient.SqlCommand("acsDocumentsByCabinetFolder", mconSqlCompany)
    '    sqlCmd.CommandType = CommandType.StoredProcedure
    '    sqlCmd.Parameters.Add("@UserName", Me.mobjUser.UserId)
    '    sqlCmd.Parameters.Add("@CabinetId", "0")
    '    sqlCmd.Parameters.Add("@FolderId", "0")

    '    Dim daSql As New SqlClient.SqlDataAdapter(sqlCmd)
    '    Dim dt As New DataTable
    '    daSql.Fill(dt)

    '    Dim dv As New DataView(dt)
    '    dv.RowFilter = "DocumentId > 0"

    '    ddlDocuments.DataSource = dv
    '    ddlDocuments.DataTextField = "DocumentName"
    '    ddlDocuments.DataValueField = "DocumentId"
    '    ddlDocuments.DataBind()

    'End Function

    'Private Function LoadAttributes()

    '    Try

    '        mconSqlCompany = Functions.BuildConnection(Me.mobjUser)

    '        Dim Attributes As DataTable = Functions.GetAttributesByDocumentId(ddlDocuments.SelectedValue, Me.mobjUser.ImagesUserId, mconSqlCompany)
    '        dgAttributes.DataSource = Attributes
    '        dgAttributes.DataBind()

    '    Catch ex As Exception

    '    Finally

    '        ReturnControls.Add(dgAttributes)

    '        If mconSqlCompany.State <> ConnectionState.Closed Then
    '            mconSqlCompany.Close()
    '            mconSqlCompany.Dispose()
    '        End If

    '    End Try

    'End Function

    'Private Sub ddlDocuments_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    LoadAttributes()
    'End Sub

    'Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)

    '    Dim inDirection As String = "IN"
    '    Dim outDirection As String = "IN"

    '    Try

    '        mconSqlSynchNotifications = Functions.BuildConnection(Functions.ConnectionType.Notifications_DataSynch)

    '        Dim Sync As New DataSync(mconSqlSynchNotifications, False)

    '        Dim direction As String
    '        If rdoIn.Checked Then
    '            direction = inDirection
    '        Else
    '            direction = outDirection
    '        End If

    '        Dim daAdd, hostAdd, daUpdate, hostUpdate As Boolean

    '        If direction = inDirection Then
    '            If chkMissingData.Checked Then daAdd = True
    '            If chkUpdateData.Checked Then daUpdate = True
    '        End If

    '        If direction = outDirection Then
    '            If chkMissingData.Checked Then hostAdd = True
    '            If chkUpdateData.Checked Then hostUpdate = True
    '        End If

    '        Dim detailJobId As Integer = Sync.DIJobInsert(chkEnabled.Checked, ddlJobType.SelectedValue, direction, JobName.Text, 0, ddlDocuments.SelectedValue, 0, daAdd, hostAdd, daUpdate, hostUpdate, 0, 0)

    '        For Each dgi As DataGridItem In dgAttributes.Items

    '            Dim Enabled As CheckBox = dgi.FindControl("Enabled")
    '            Dim PrimaryKey As CheckBox = dgi.FindControl("PrimaryKey")
    '            Dim LocalAttributeName As TextBox = dgi.FindControl("LocalAttributeName")
    '            Dim AttributeId As Label = dgi.FindControl("AttributeId")

    '            If Enabled.Checked Then
    '                Sync.DIAttributeSyncDetailInsert(detailJobId, AttributeId.Text, LocalAttributeName.Text, PrimaryKey.Checked)
    '            End If

    '        Next

    '    Catch ex As Exception

    '    Finally

    '        ReturnControls.Add(dgAttributes)

    '        If mconSqlSynchNotifications.State <> ConnectionState.Closed Then
    '            mconSqlSynchNotifications.Close()
    '            mconSqlSynchNotifications.Dispose()
    '        End If

    '    End Try

    'End Sub

    'Private Sub dgAttributes_PreRender(ByVal sender As System.Object, ByVal e As System.EventArgs)

    '    'Build script to allow only one primary key checked
    '    Dim sb As New System.Text.StringBuilder

    '    For Each dgi As DataGridItem In dgAttributes.Items

    '        Dim Enabled As CheckBox = dgi.FindControl("Enabled")
    '        Dim PrimaryKey As CheckBox = dgi.FindControl("PrimaryKey")
    '        Dim LocalAttributeName As TextBox = dgi.FindControl("LocalAttributeName")
    '        Dim AttributeId As Label = dgi.FindControl("AttributeId")

    '        If Not PrimaryKey Is Nothing Then
    '            sb.Append("document.all." & PrimaryKey.ClientID & ".checked = false;")
    '        End If

    '    Next

    '    For Each dgiAdd As DataGridItem In dgAttributes.Items

    '        Dim PrimaryKey As CheckBox = dgiAdd.FindControl("PrimaryKey")

    '        If Not PrimaryKey Is Nothing Then
    '            PrimaryKey.Attributes.Add("onclick", sb.ToString)
    '            PrimaryKey.Attributes.Add("oncheck", "var chkbox = document.all." & PrimaryKey.ClientID & "; if (chkbox.checked = true) { chkbox.checked = true;return true; } else { chkbox.checked = false;return true; }")
    '        End If

    '    Next

    '    ReturnControls.Add(dgAttributes)

    'End Sub

    Private Sub dgSynch_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgSynch.ItemCommand

        Try
            Select Case e.CommandName

                Case "Del"

                    Try
                        Me.mconSqlSynchNotifications = Functions.BuildConnection(Functions.ConnectionType.Notifications_DataSynch)

                        Dim Sync As New DataSync(Me.mconSqlSynchNotifications)

                        Dim JobId As Label = e.Item.FindControl("JobId")
                        Sync.DataSyncDeleteAlert(JobId.Text)

                        e.Item.Visible = False

                        Dim intVisibleItems As Integer = 0
                        For Each dgi As DataGridItem In dgSynch.Items
                            If dgi.Visible Then intVisibleItems += 1
                        Next

                        If intVisibleItems = 0 Then
                            NoRows.InnerHtml = "No synchronizations have been defined for your account. Click the 'Add Synchronization' link to setup a notification."
                            dgSynch.Visible = False
                            NoRows.Visible = True
                        End If

                    Catch ex As Exception

                    Finally

                        'ReturnControls.Add(NoRows)
                        'ReturnControls.Add(dgSynch)

                        If Me.mconSqlCompany.State <> ConnectionState.Closed Then
                            Me.mconSqlCompany.Close()
                            Me.mconSqlCompany.Dispose()
                        End If

                    End Try

            End Select

        Catch ex As Exception

        End Try

    End Sub

    Private Sub dgSynch_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgSynch.ItemDataBound

        Try
            Dim btnDelete As ImageButton = e.Item.FindControl("btnDelete")
            If Not btnDelete Is Nothing Then btnDelete.Attributes("onclick") = "return confirm('Are you sure you want to remove this synchronization?');"
        Catch ex As Exception

        End Try

    End Sub

    Private Sub lnkAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkAdd.Click
        Response.Redirect("DataSynchSchedule.aspx")
    End Sub

End Class
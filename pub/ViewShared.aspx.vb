Imports Accucentric.docAssist.Data.Images
Imports System.Web.UI.HtmlControls

Partial Class ViewShared
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblTitle As System.Web.UI.WebControls.Label
    Protected WithEvents btnDownload As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lblVersionId As System.Web.UI.WebControls.Label
    Protected WithEvents lblImageType As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Const RESULTS_PER_PAGE As Integer = 25

    Private conMaster As New SqlClient.SqlConnection

    Dim ds As New DataSet

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Response.Expires = 0
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        Dim conCompany As New SqlClient.SqlConnection

        Try

            If Not Page.IsPostBack Then

                Dim accountID, id, type, emailaddress, key As String

                Dim enc As New Encryption.Encryption

                Dim items() As String = enc.Decrypt(Request.QueryString(0).ToString.Replace("~", "=").Replace("^", "+")).Split(";")

                accountID = items(0)
                id = items(1)
                type = items(2)
                key = items(3)
                emailaddress = items(4)

                Dim AccountDesc, DBServerDNS, DBName As String
                Dim enabled As Boolean

                Functions.getAccountInfo(accountID, AccountDesc, DBServerDNS, DBName, enabled)

                Dim strMasterDbPassword As String
                strMasterDbPassword = ConfigurationSettings.AppSettings("MasterPW")

                Dim conString As String = Functions.BuildConnectionString(DBServerDNS, DBName, "bluprint", enc.Decrypt(strMasterDbPassword))

                conCompany.ConnectionString = conString
                conCompany.Open()

                '--Verify Share permission for Document or Folder
                'PROCEDURE(SMSharePermVerify)
                '@AccountID varchar(36),
                '@ShareType varchar(1),
                '@ShareKey int,
                '@ShareOption varchar(10),
                '@EmailAddress varchar(200),
                '@Result int OUTPUT (0 allow, -1 account is not active or trial expired, -2 permission is not allowed)

                Dim cmd As New SqlClient.SqlCommand("SMSharePermVerify", conCompany)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.Add("@AccountID", accountID)
                cmd.Parameters.Add("@ShareType", type)
                cmd.Parameters.Add("@ShareKey", id)
                cmd.Parameters.Add("@ShareOption", "")
                cmd.Parameters.Add("@EmailAddress", emailaddress)
                cmd.Parameters.Add("@ShareGrantKey", key)

                Dim res As New SqlClient.SqlParameter("@Result", SqlDbType.Int)
                res.Direction = ParameterDirection.Output
                cmd.Parameters.Add(res)
                cmd.ExecuteNonQuery()

                Select Case res.Value
                    Case 0
                        Dim resultCount As Integer = BrowseFolder(id, conCompany)
                        rptDocuments.DataSource = ds
                        rptDocuments.DataBind()
                    Case -1 'Disabled or expired trial account
                        message.Attributes("class") = "visible"
                        main.Attributes("class") = "hidden"
                    Case -2 'Document is no longer available.
                        message.Attributes("class") = "visible"
                        main.Attributes("class") = "hidden"
                End Select

            End If

        Catch ex As Exception

            message.Attributes("class") = "visible"
            main.Attributes("class") = "hidden"

            Dim conMaster As SqlClient.SqlConnection
            conMaster = Functions.BuildMasterConnection
            Functions.LogException("", "", "", "Public folder view Load error: " & ex.Message, ex.StackTrace, Server.MachineName, "", Request.Url.ToString, conMaster, True)

        Finally

            If conMaster.State <> ConnectionState.Closed Then
                conMaster.Close()
                conMaster.Dispose()
            End If

            If conCompany.State <> ConnectionState.Closed Then
                conCompany.Close()
                conCompany.Dispose()
            End If

        End Try

    End Sub

    Private Function BrowseFolder(ByVal FolderId As Integer, ByVal conSql As SqlClient.SqlConnection) As Integer

        Dim curPage As Integer = CInt(CurrentPage.Value)
        Dim firstRow As Integer = (RESULTS_PER_PAGE * (curPage - 1)) + 1
        Dim lastRow As Integer = 10000 '(RESULTS_PER_PAGE + firstRow) - 1
        Dim ResultCount As Integer

        Try


            Dim objFolders As New Folders(conSql, False)

            Dim diffFromGMT As Integer = 0

            Try
                diffFromGMT = (timeDiff.Value / 60) * -1
            Catch ex As Exception

            End Try

            ds = objFolders.SMFolderBrowse(-1, FolderId, firstRow, lastRow, diffFromGMT, ResultCount)

            If ResultCount < lastRow Then lastRow = ResultCount

            'Select Case ResultCount

            '    Case 0

            '        ReturnScripts.Add("document.all.NoResults.innerHTML = '" & "No document or files were found in this folder." & "';")
            '        lblPages.Text = ""
            '        lblSummary.Text = ""
            '        dgResults.DataSource = Nothing
            '        dgResults.DataBind()
            '        HideMassIcons()

            '    Case -1 'No access

            '        ShowPopupError("Access Denied<br><br>Please contact a docAssist administrator within your organization if you need access to this folder.", False)
            '        lblPages.Text = ""
            '        lblSummary.Text = ""
            '        dgResults.DataSource = Nothing
            '        dgResults.DataBind()
            '        HideMassIcons()

            '    Case Else

            '        SaveNextPreviousDocList(ResultCount)

            '        dgResults.DataSource = ds
            '        dgResults.DataBind()

            '        ReturnScripts.Add("document.all.NoResults.innerHTML = '';")

            '        Dim pages As Integer = Math.Ceiling(ResultCount / Me.mobjUser.DefaultResultCount)
            '        BuildPagination(pages)

            '        Dim dblSeconds As Double = Math.Round(endTime.Subtract(startTime).TotalMilliseconds() / 1000, 2)
            '        lblSummary.Text = "Results <b>" & firstRow & " - " & lastRow & "</b> of <b>" & ResultCount & "</b> (" & IIf(dblSeconds < 1, dblSeconds & " second)", dblSeconds & " seconds)")

            '        If NodeId.Value <> BrowseType.RECYCLE_BIN Then
            '            ShowMassIcons()
            '            HideRestoreButton()
            '        Else
            '            HideMassIcons()
            '            ShowRestoreButton()
            '        End If


            'End Select

            Return ResultCount

        Catch ex As Exception

            Dim conMaster As SqlClient.SqlConnection
            conMaster = Functions.BuildMasterConnection
            Functions.LogException("", "", "", "Public folder view BrowseFolder error: " & ex.Message, ex.StackTrace, Server.MachineName, "", Request.Url.ToString, conMaster, True)

        Finally

            'HideBackButton()

            'blnFolderBrowseMode = False

            'Session("SearchType") = "FolderBrowse"
            'Session("CurrentPage") = curPage
            'Session("ResultCount") = ResultCount
            'Session("DataSource") = ds
            'Session("FolderName") = FolderName.Value
            'Session("FolderId") = FolderId

            'SearchType.Value = "F"
            'ReturnControls.Add(SearchType)

            'ReturnControls.Add(dgResults)
            'ReturnControls.Add(lblPages)
            'ReturnControls.Add(lblSummary)
            'ReturnScripts.Add("stopProgressDisplay();resultsMode();")

            'If Me.mconSqlImage.State <> ConnectionState.Closed Then
            '    Me.mconSqlImage.Close()
            '    Me.mconSqlImage.Dispose()
            'End If

        End Try

    End Function

    Private Sub rptDocuments_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptDocuments.ItemDataBound

        Try

            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then

                Dim title As HtmlGenericControl = e.Item.FindControl("title")
                Dim desc As HtmlGenericControl = e.Item.FindControl("desc")
                Dim countSize As HtmlGenericControl = e.Item.FindControl("countSize")
                Dim modDate As HtmlGenericControl = e.Item.FindControl("modDate")
                Dim resultRow As HtmlGenericControl = e.Item.FindControl("resultRow")
                Dim attributes As HtmlGenericControl = e.Item.FindControl("attributes")
                Dim filename As HtmlGenericControl = e.Item.FindControl("filename")
                Dim icon As HtmlImage = e.Item.FindControl("icon")
                Dim btnDownload As ImageButton = e.Item.FindControl("btnDownload")

                'addhandler btnDownload.Click, addressof  

                Select Case e.Item.DataItem("ImageType")
                    Case 1 'Scan
                        countSize.InnerHtml = e.Item.DataItem("CurrentPageCount") & IIf(e.Item.DataItem("CurrentPageCount") = 1, " page", " pages")
                        'icon.Src = "../Images/scan.gif"
                        icon.Visible = False

                    Case 3 'Attachment
                        countSize.InnerHtml = Functions.GetBytesAsMegs(e.Item.DataItem("SizeKB"))

                        'File Icon
                        Dim strIconPath As String = "../images/icons/" & System.IO.Path.GetExtension(e.Item.DataItem("FileName")).ToUpper.Replace(".", "") & ".gif"
                        If System.IO.File.Exists(Server.MapPath(strIconPath)) Then
                            icon.Src = "./" & strIconPath
                        End If

                End Select

                'Alternate row
                'If e.Item.ItemType = ListItemType.AlternatingItem Then
                '    resultRow.Attributes("class") = "alternateRow"
                'End If

                'Attributes
                Dim dvAttributes As New DataView(ds.Tables(1))
                dvAttributes.RowFilter = "ImageId=" & e.Item.DataItem("ImageId")
                'attributes.InnerHtml = BuildAttributes(dvAttributes)

                Dim rptAttributes As System.Web.UI.WebControls.Repeater = e.Item.FindControl("rptAttributes")
                rptAttributes.DataSource = dvAttributes
                rptAttributes.DataBind()

                'Modified Date
                modDate.InnerHtml = GetNiceDate(e.Item.DataItem("ModifiedDate"))

            End If

        Catch ex As Exception

            Dim conMaster As SqlClient.SqlConnection
            conMaster = Functions.BuildMasterConnection
            Functions.LogException("", "", "", "Public folder view databind error: " & ex.Message, ex.StackTrace, Server.MachineName, "", Request.Url.ToString, conMaster, True)

        End Try

    End Sub

    Private Function GetNiceDate(ByVal ModifiedDate As DateTime) As String

        Dim userTime As DateTime = Now.AddHours((timeDiff.Value / 60) * -1)

        ' Get time span elapsed since the date.
        Dim s As TimeSpan = userTime.Subtract(ModifiedDate)

        ' Get total number of days elapsed.
        Dim dayDiff As Integer = CInt(s.TotalDays)

        ' Get total number of seconds elapsed.
        Dim secDiff As Integer = CInt(s.TotalSeconds)

        ' Don't allow out of range values.
        'If dayDiff < 0 OrElse dayDiff >= 31 Then
        '    Return Nothing
        'End If

        'Handle same-day times.
        If dayDiff = 0 Then
            Return "<b>Today</b> at" & Space(1) & ModifiedDate.ToShortTimeString
        ElseIf dayDiff = 1 Then
            Return "<b>Yesterday</b> at" & Space(1) & ModifiedDate.ToShortTimeString
        Else
            Return String.Format("{0} {1}", ModifiedDate.ToShortDateString, ModifiedDate.ToShortTimeString)
        End If

        ' Handle previous days.
        'If dayDiff = 1 Then
        '    Return "yesterday"
        'End If

        'If dayDiff < 7 Then
        '    Return String.Format("{0} {1}", ModifiedDate.DayOfWeek.ToString, ModifiedDate.ToLongTimeString)
        'End If

        'If dayDiff < 31 Then
        '    Return String.Format("{0} {1}", ModifiedDate.ToShortDateString, ModifiedDate.ToLongTimeString)
        'End If

        'PadDate(CDate(e.Item.DataItem("ModifiedDate")).ToShortDateString) & " " & )

    End Function

    Private Sub rptDocuments_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptDocuments.ItemCommand

        Dim conCompany As New SqlClient.SqlConnection
        Dim accountID, id, type, emailaddress, key As String

        Try

            If e.CommandName = "Download" Then

                Dim lblVersionId As Label = e.Item.FindControl("lblVersionId")
                Dim lblImageType As Label = e.Item.FindControl("lblImageType")
                Dim lblFileName As Label = e.Item.FindControl("lblFileName")

                Dim VersionId As Integer = lblVersionId.Text

                Dim enc As New Encryption.Encryption
                Dim items() As String = enc.Decrypt(Request.QueryString(0).ToString.Replace("~", "=").Replace("^", "+")).Split(";")

                accountID = items(0)
                ID = items(1)
                Type = items(2)
                key = items(3)
                emailaddress = items(4)

                Dim AccountDesc, DBServerDNS, DBName As String
                Dim enabled As Boolean

                Functions.getAccountInfo(accountID, AccountDesc, DBServerDNS, DBName, enabled)

                Dim strMasterDbPassword As String
                strMasterDbPassword = ConfigurationSettings.AppSettings("MasterPW")

                Dim conString As String = Functions.BuildConnectionString(DBServerDNS, DBName, "bluprint", enc.Decrypt(strMasterDbPassword))

                conCompany.ConnectionString = conString
                conCompany.Open()

                Select Case lblImageType.Text

                    Case 1 'Scan

                        Dim imgByte() As Byte = Functions.ExportPublicPDF(conCompany, VersionId, 1, 10000, Me, False)

                        Response.Clear()

                        Response.AddHeader("Content-Disposition", "attachment; filename=docAssist_Document_" & lblVersionId.Text & ".pdf")
                        Response.AddHeader("Content-Length", imgByte.Length)
                        Response.ContentType = "application/pdf"
                        Response.BinaryWrite(imgByte)
                        Response.End()

                    Case 3 'Attachment

                        'Create file and push it to the user
                        Dim objAttachments As New FileAttachments(conCompany, True)

                        Dim dt As New DataTable
                        dt = objAttachments.GetFile(VersionId)

                        Dim imgByte As Byte() = CType(dt.Rows(0)("Data"), Byte())

                        Response.Clear()

                        Response.AddHeader("Content-Disposition", "attachment; filename=" & Server.UrlEncode(lblFileName.Text).Replace("+", "%20"))
                        Response.AddHeader("Content-Length", imgByte.Length)

                        If lblFileName.Text.ToLower.EndsWith(".pdf") Then
                            Response.ContentType = "application/pdf"
                        Else
                            Response.ContentType = "application/octet-stream"
                        End If

                        Response.BinaryWrite(imgByte)
                        Response.End()

                End Select

            End If

        Catch ex As Exception

            Dim conMaster As SqlClient.SqlConnection
            conMaster = Functions.BuildMasterConnection
            Functions.LogException("", "", "", "Public folder view download error: " & ex.Message, ex.StackTrace, Server.MachineName, "", Request.Url.ToString, conMaster, True)

        Finally

            If conCompany.State <> ConnectionState.Closed Then
                conCompany.Close()
                conCompany.Dispose()
            End If

        End Try

    End Sub

    Private Sub btnDownload_Command(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.CommandEventArgs) Handles btnDownload.Command

    End Sub
End Class

Partial Class download
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Not Page.IsPostBack Then

            Dim conCompany As New SqlClient.SqlConnection
            Dim accountID, versionID As String

            Try

                accountID = Encryption.Encryption.Decrypt(Request.QueryString(0).Replace("~", "=").Replace("^", "+")).Split(";")(0)
                versionID = Encryption.Encryption.Decrypt(Request.QueryString(0).Replace("~", "=").Replace("^", "+")).Split(";")(1)

                Dim AccountDesc, DBServerDNS, DBName As String
                Dim enabled As Boolean

                Functions.getAccountInfo(accountID, AccountDesc, DBServerDNS, DBName, enabled)

                Dim strMasterDbPassword As String
                strMasterDbPassword = ConfigurationSettings.AppSettings("MasterPW")

                Dim conString As String = Functions.BuildConnectionString(DBServerDNS, DBName, "bluprint", Encryption.Encryption.Decrypt(strMasterDbPassword))

                conCompany.ConnectionString = conString
                conCompany.Open()

                Dim imgByte() As Byte = Functions.ExportPublicPDF(conCompany, versionID, 1, 10000, Me, False)

                Response.Clear()

                Response.AddHeader("Content-Disposition", "attachment; filename=Document.pdf")
                Response.AddHeader("Content-Length", imgByte.Length)
                Response.ContentType = "application/pdf"
                Response.BinaryWrite(imgByte)
                Response.End()

            Catch ex As Exception

                If ex.Message.IndexOf("was being aborted") < 0 Then

                    Dim conMaster As SqlClient.SqlConnection
                    conMaster = Functions.BuildMasterConnection
                    Functions.LogException("", "", "", "download.aspx: " & ex.Message, ex.StackTrace, Server.MachineName, "", Request.Url.ToString, conMaster, True)

                End If

            Finally

                If conCompany.State <> ConnectionState.Closed Then
                    conCompany.Close()
                    conCompany.Dispose()
                End If

            End Try

        End If

    End Sub

End Class

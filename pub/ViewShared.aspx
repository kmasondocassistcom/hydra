<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ViewShared.aspx.vb" Inherits="docAssistWeb.ViewShared"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>docAssist - Shared Documents</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../css/base.css" type="text/css" rel="stylesheet">
		<LINK href="../css/publicshare.css" type="text/css" rel="stylesheet">
		<script src="../scripts/jquery.js"></script>
		<script src="../scripts/publicshare.js"></script>
	</HEAD>
	<body>
		<form id="frmViewShare" method="post" runat="server">
			<div id="wrapper">
				<div id="message" class="hidden" runat="server">
					<div id="logo"><a href="http://www.docassist.com/index.html?ref=publicShare" target="_blank"><IMG alt="docAssist" src="../Images/docAssist.jpg" border="0"></a></div>
					<h1>This folder is currently unavailable.</h1>
					<label>Access may have been revoked or the folder may have been deleted. Please 
						contact the sender for more information.</label>
				</div>
				<div id="main" runat="server">
					<div id="logo"><a href="http://www.docassist.com/index.html?ref=publicShare" target="_blank"><IMG alt="docAssist" src="../Images/docAssist.jpg" border="0"></a></div>
					<label>You are viewing a shared folder. To download a document click the green 
						download icon at the left of each row.</label>
					<div id="contents">
						<div id="sortDocument">Document Type</div>
						<div id="sortSize">Size/Pages</div>
						<div id="sortDate">Modified</div>
						<div id="results"><asp:repeater id="rptDocuments" runat="server">
								<ItemTemplate>
									<asp:Label id="lblVersionId" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.CurrentVersionID") %>'>
									</asp:Label>
									<asp:Label id="lblImageType" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.ImageType") %>'>
									</asp:Label>
									<asp:Label id="lblFileName" runat="server" Visible="False" Text='<%# Server.UrlDecode(DataBinder.Eval(Container, "DataItem.Filename").ToString()) %>'>
									</asp:Label>
									<div id="resultRow" class="resultRow" runat="server">
										<div class="dlbutton">
											<asp:ImageButton id="btnDownload" runat="server" ImageUrl="../Images/download.png" AlternateText="Download Document"
												CommandName="Download"></asp:ImageButton>
										</div>
										<div class="resultDocument" runat="server" ID="Div1">
											<div class="docType" runat="server" ID="Div2"><%# DataBinder.Eval(Container, "DataItem.DocumentName") %></div>
											<div class="filename" id="filename" runat="server"><img id="icon" runat="server" src="../Images/file.gif"><%# Server.UrlDecode(DataBinder.Eval(Container, "DataItem.Filename").ToString()) %></div>
											<div class="title" id="Div3" runat="server"><%# Server.UrlDecode(DataBinder.Eval(Container, "DataItem.DocumentTitle").ToString()) %></div>
											<div class="desc" id="desc" runat="server"><%# Server.UrlDecode(DataBinder.Eval(Container, "DataItem.DocumentDescription").ToString()) %></div>
										</div>
										<div class="resultSize">
											<div id="countSize" class="resultSize" runat="server"></div>
										</div>
										<div class="resultDate" id="modDate" runat="server">
										</div>
										<div id="attributes" class="attributes" runat="server">
											<asp:repeater id="rptAttributes" runat="server">
												<ItemTemplate>
													<div class="attributeItem">
														<div class="attributeName">
															<%# DataBinder.Eval(Container, "DataItem.AttributeName") %>
															:
														</div>
														<div class="attributeValue">
															<%# DataBinder.Eval(Container, "DataItem.AttributeValue") %>
														</div>
													</div>
												</ItemTemplate>
											</asp:repeater>
										</div>
									</div>
								</ItemTemplate>
							</asp:repeater></div>
					</div>
				</div>
			</div>
			<input id="CurrentPage" type="hidden" value="1" runat="server"> <input id="timeDiff" type="hidden" value="0" runat="server">
		</form>
	</body>
</HTML>

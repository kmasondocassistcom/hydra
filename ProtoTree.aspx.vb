'Imports ComponentArt.Web.UI
'Imports Accucentric.docAssist
'Imports Accucentric.docAssist.Data.Images

'Partial Class ProtoTree
'    Inherits zumiControls.zumiPage

'    Dim mconSqlImage As SqlClient.SqlConnection
'    Dim mobjUser As Accucentric.docAssist.Web.Security.User
'    Dim contextMenus As New ArrayList

'    Protected WithEvents litEdit As System.Web.UI.WebControls.Literal
'    Protected WithEvents btnNewShare As System.Web.UI.WebControls.ImageButton
'    Protected WithEvents btnShareNewFolder As System.Web.UI.WebControls.ImageButton
'    Protected WithEvents btnRemoveFolder As System.Web.UI.WebControls.ImageButton
'    Protected WithEvents tblShare As System.Web.UI.HtmlControls.HtmlTable
'    Protected WithEvents Rename As System.Web.UI.HtmlControls.HtmlAnchor
'    Protected WithEvents imgRenameShare As System.Web.UI.HtmlControls.HtmlImage
'    Protected WithEvents NodeId As System.Web.UI.HtmlControls.HtmlInputHidden
'    Protected WithEvents btnRename As System.Web.UI.WebControls.ImageButton
'    Protected WithEvents NewText As System.Web.UI.HtmlControls.HtmlInputHidden
'    Protected WithEvents NodeText As System.Web.UI.HtmlControls.HtmlInputHidden
'    Protected WithEvents ParentFolder As System.Web.UI.HtmlControls.HtmlInputHidden
'    Protected WithEvents btnOk As System.Web.UI.WebControls.ImageButton
'    Protected WithEvents btnFindFolder As System.Web.UI.WebControls.ImageButton
'    Protected WithEvents SearchFolder As System.Web.UI.WebControls.TextBox
'    Protected WithEvents Message As System.Web.UI.WebControls.Label
'    Protected WithEvents ClearSearch As System.Web.UI.WebControls.LinkButton

'    Private Const FAVORITE_SPLIT = "�"

'#Region " Web Form Designer Generated Code "

'    'This call is required by the Web Form Designer.
'    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

'    End Sub

'    'NOTE: The following placeholder declaration is required by the Web Form Designer.
'    'Do not delete or move it.
'    Private designerPlaceholderDeclaration As System.Object

'    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
'        'CODEGEN: This method call is required by the Web Form Designer
'        'Do not modify it using the code editor.
'        InitializeComponent()
'    End Sub

'#End Region

'    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

'        Response.Expires = 0
'        Response.Cache.SetNoStore()
'        Response.AppendHeader("Pragma", "no-cache")

'        Try

'            Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
'            Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))
'            Me.mobjUser.AccountId = guidAccountId
'            Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)

'            If Not Page.IsPostBack Then
'                BuildTree()
'            End If

'        Catch ex As Exception

'            Response.Write(ex.Message)

'        Finally
'            If Me.mconSqlImage.State <> ConnectionState.Closed Then
'                Me.mconSqlImage.Close()
'                Me.mconSqlImage.Dispose()
'            End If
'        End Try

'    End Sub

'    Private Sub BuildTree()

'        Dim cmd As New System.Text.StringBuilder

'        Dim objFolders As New Folders(Me.mconSqlImage, False)
'        Dim ds As New DataSet
'        ds = objFolders.FoldersGet(Me.mobjUser.ImagesUserId)

'        cmd.Append("var tree = new YAHOO.widget.TreeView('treeDiv1');")
'        cmd.Append("tree.setDynamicLoad(loadNodeData, currentIconMode);")
'        cmd.Append("var root = tree.getRoot();")
'        cmd.Append("var currentIconMode;")

'        For Each drShare As DataRow In ds.Tables(TreeTables.SHARES).Rows

'           Dim expand As String = IIf(drShare("ExpandShare") = True, "true", "false")
'            cmd.Append("var s" & drShare("ShareId") & " = new YAHOO.widget.TextNode('" & drShare("ShareName") & "', root, " & expand & ");")

'            'Set element ID
'            cmd.Append("s" & drShare("ShareId") & ".labelElId = 'S" & drShare("ShareId") & "';")

'            If drShare("HasChildFolder") = True Then
'                cmd.Append("s" & drShare("ShareId") & ".isLeaf = false;")
'            Else
'                cmd.Append("s" & drShare("ShareId") & ".isLeaf = true;")
'            End If

'        Next

'        'Build share children folders
'        For Each drChild As DataRow In ds.Tables(TreeTables.FOLDERS).Rows

'            Dim intParentFolderId As Integer = drChild("ParentFolderId")
'            Dim intFolderLevel As Integer = drChild("FolderLevel")

'            Dim expand As String = IIf(drChild("ExpandFolder") = True, "true", "false")
'            If intFolderLevel = 1 Then
'                cmd.Append("var w" & drChild("FolderId") & " = new YAHOO.widget.TextNode('" & drChild("FolderName") & "', " & "s" & intParentFolderId & ", " & expand & ");")
'            Else
'                cmd.Append("var w" & drChild("FolderId") & " = new YAHOO.widget.TextNode('" & drChild("FolderName") & "', " & "w" & intParentFolderId & ", " & expand & ");")
'            End If

'            'Set element ID
'            cmd.Append("w" & drChild("FolderId") & ".labelElId = 'W" & drChild("FolderId") & "';")

'            'Client side scripting
'            cmd.Append("w" & drChild("FolderId") & ".href = ""javascript:alert('Browse folder " & drChild("FolderId") & "');"";")

'            If drChild("HasChildFolder") = True Then
'                cmd.Append("w" & drChild("FolderId") & ".isLeaf = false;")
'            Else
'                cmd.Append("w" & drChild("FolderId") & ".isLeaf = true;")
'            End If

'            'hasChildren

'            ''Add child to parent
'            'If Not IsNothing(ParentNode) Then

'            '    Dim ChildNode As New TreeViewNode
'            '    ChildNode.Text = drChild("FolderName")
'            '    ChildNode.ID = "W" & drChild("FolderId")
'            '    ChildNode.AutoPostBackOnSelect = True
'            '    ChildNode.ImageUrl = IMAGE_FOLDER
'            '    ChildNode.AutoPostBackOnRename = False
'            '    ChildNode.EditingEnabled = False
'            '    ChildNode.ExpandedImageUrl = IMAGE_FOLDER_OPEN
'            '    'ChildNode.Expanded = True
'            '    'ChildNode.ContextMenuName = strFolderMenuName
'            'If drChild("HasChildFolder") = True Then
'            '    cmd.Append("w" & drChild("FolderId") & ".isLeaf = true;")
'            'End If

'            '    'ChildNode.PostBack = True
'            '    'ChildNode.Checkable = False

'            '    If drChild("HasChildFolder") = True Then
'            '        'ChildNode.ExpandMode = ExpandMode.ServerSideCallBack
'            '        ChildNode.ContentCallbackUrl = "XmlGenerator.aspx?Ref=Popup&NodeId=" & ChildNode.ID
'            '        ChildNode.Expanded = False
'            '    End If

'            '    If drChild("ExpandFolder") = True Then
'            '        ChildNode.Expanded = True
'            '    End If

'            '    'If drChild("CanViewFolderOnly") = True Then
'            '    '    'ChildNode.Expanded = True
'            '    '    ChildNode.AutoPostBackOnSelect = False
'            '    '    ChildNode.ClientSideCommand = "alert('Access denied.');stopProgressDisplay();"
'            '    'End If

'            '    If drChild("CanAddFilesToFolder") = False Then
'            '        ChildNode.ClientSideCommand = "alert('No access to add files to this folder.');"
'            '        ChildNode.ID = "NOACCESS"
'            '    End If

'            '    'NodeCollection.Add(ChildNode)
'            '    'colNodes.Add(ChildNode)

'            '    ParentNode.Nodes.Add(ChildNode)

'            'End If

'        Next

'        cmd.Append("tree.draw();")
'        ReturnScripts.Add(cmd.ToString)

'    End Sub

'End Class
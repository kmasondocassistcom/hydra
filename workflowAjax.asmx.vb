﻿Imports System
Imports System.Collections.Generic
Imports System.Web
Imports System.Web.Script.Services
Imports System.Web.Script.Serialization
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel

Imports Accucentric.docAssist.Data.Images
Imports Accucentric.docAssist
' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
<System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class workflowAjax
    Inherits System.Web.Services.WebService

    Private mobjUser As Accucentric.docAssist.Web.Security.User

    <WebMethod()> _
    Public Function getQueues(ByVal access As Integer) As DataSet

        Dim conCompany As New SqlClient.SqlConnection

        Try

            Dim Request As HttpRequest = Context.Request
            Me.mobjUser = BuildUserObject(Request.Cookies("docAssist"))
            conCompany = Functions.BuildConnection(Me.mobjUser)

            Dim objWf As New Workflow(Functions.BuildConnection(Me.mobjUser))

            Dim ds As New DataSet
            ds = objWf.WFQueueList(Me.mobjUser.ImagesUserId, access)

            ds.Tables(0).TableName = "workflows"
            ds.Tables(1).TableName = "queues"

            Return ds

        Catch ex As Exception

            Dim conMaster As SqlClient.SqlConnection
            conMaster = Functions.BuildMasterConnection
            Functions.LogException(mobjUser.UserId.ToString, mobjUser.AccountId.ToString, "", "getQueues() Error: " & ex.Message, ex.StackTrace, Server.MachineName, "", "", conMaster, True)

            Throw ex

        Finally

            If conCompany.State <> ConnectionState.Closed Then
                conCompany.Close()
                conCompany.Dispose()
            End If

        End Try

    End Function

    <WebMethod()> _
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Function getDocuments(ByVal workflowId As Integer, ByVal queueId As Integer) As String

        Dim conCompany As New SqlClient.SqlConnection

        Try

            Dim Request As HttpRequest = Context.Request
            Me.mobjUser = BuildUserObject(Request.Cookies("docAssist"))
            conCompany = Functions.BuildConnection(Me.mobjUser)

            Dim objWf As New Workflow(Functions.BuildConnection(Me.mobjUser))

            Dim ds As New DataSet
            ds = objWf.WFDashBoardByQueueID(Me.mobjUser.ImagesUserId, workflowId, queueId, Me.mobjUser.TimeDiffFromGMT)

            Dim row As New DocumentRow
            row.DocumentNo = 1
            row.DocumentType = "Invoice"
            'ds.Tables(0).TableName = "doctypes"
            'ds.Tables(1).TableName = "docs"
            'ds.Tables(2).TableName = "attributes"

            Dim res As String = New JavaScriptSerializer().Serialize(row)

            Return res

        Catch ex As Exception

            Dim conMaster As SqlClient.SqlConnection
            conMaster = Functions.BuildMasterConnection
            Functions.LogException(mobjUser.UserId.ToString, mobjUser.AccountId.ToString, "", "getDocuments() Error: " & ex.Message, ex.StackTrace, Server.MachineName, "", "", conMaster, True)

            Throw ex

        Finally

            If conCompany.State <> ConnectionState.Closed Then
                conCompany.Close()
                conCompany.Dispose()
            End If

        End Try

    End Function

End Class

Public Class DocumentRow

    Private docNo As Integer
    Private docType As String

    Public Property DocumentNo() As Integer
        Get
            Return docNo
        End Get
        Set(ByVal value As Integer)
            docNo = value
        End Set
    End Property


    Public Property DocumentType() As String
        Get
            Return docType
        End Get
        Set(ByVal value As String)
            docType = value
        End Set
    End Property

End Class


'Imports ComponentArt.Web.UI
'Imports Accucentric.docAssist
'Imports Accucentric.docAssist.Data.Images

'Partial Class Cabinet
'    Inherits System.Web.UI.Page

'    Dim mconSqlImage As SqlClient.SqlConnection
'    Dim mobjUser As Accucentric.docAssist.Web.Security.User
'    Dim contextMenus As New ArrayList

'    Protected WithEvents litEdit As System.Web.UI.WebControls.Literal
'    Protected WithEvents btnOk As System.Web.UI.WebControls.ImageButton

'    Private Const FAVORITE_SPLIT = "�"

'#Region " Web Form Designer Generated Code "

'    'This call is required by the Web Form Designer.
'    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

'    End Sub

'    'NOTE: The following placeholder declaration is required by the Web Form Designer.
'    'Do not delete or move it.
'    Private designerPlaceholderDeclaration As System.Object

'    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
'        'CODEGEN: This method call is required by the Web Form Designer
'        'Do not modify it using the code editor.
'        InitializeComponent()
'    End Sub

'#End Region

'    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

'        Response.Expires = 0
'        Response.Cache.SetNoStore()
'        Response.AppendHeader("Pragma", "no-cache")

'        Try

'            Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
'            Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))
'            Me.mobjUser.AccountId = guidAccountId
'            Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)

'            BuildTree()

'            UpdateToolbar()

'        Catch ex As Exception

'            Response.Write(ex.Message)

'        Finally
'            If Me.mconSqlImage.State <> ConnectionState.Closed Then
'                Me.mconSqlImage.Close()
'                Me.mconSqlImage.Dispose()
'            End If
'        End Try

'    End Sub

'    Private Sub BuildTree()

'        If Not Page.IsPostBack Then

'            Dim firstDoc As TreeViewNode

'            Dim objFolders As New Folders(Me.mconSqlImage, False)
'            Dim ds As New DataSet
'            ds = objFolders.FoldersGet(Me.mobjUser.ImagesUserId)

'            Dim CatFolderNode As New TreeViewNode
'            CatFolderNode.Text = "Cabinets & Folders"
'            CatFolderNode.ID = "CabsFolders"
'            CatFolderNode.AutoPostBackOnSelect = True
'            CatFolderNode.Expanded = True
'            CatFolderNode.ImageUrl = IMAGE_CABINETS_FOLDERS

'            treeSearch.Nodes.Add(CatFolderNode)

'            For Each drShare As DataRow In ds.Tables(TreeTables.SHARES).Rows

'                'Build share
'                Dim Share As New TreeViewNode
'                Share.Text = drShare("ShareName")
'                Share.ID = "S" & drShare("ShareId")
'                Share.ImageUrl = IMAGE_CABINET
'                Share.AutoPostBackOnSelect = True

'                If drShare("ExpandShare") = True Then
'                    Share.Expanded = True
'                End If

'                CatFolderNode.Nodes.Add(Share)

'            Next

'        End If

'    End Sub

'    Private Sub UpdateToolbar()

'        Try

'            'Find contect menu to load permissions from, avoid another SQL call.
'            Dim strId As String = NodeId.Value


'            Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
'            Dim id As String = Request.QueryString("NodeId")

'            Dim objFolders As New Folders(Functions.BuildConnection(Me.mobjUser))
'            Dim dt As New DataTable

'            DisableToolbar()

'            If NodeId.Value = "CabsFolders" Then
'                If mobjUser.SysAdmin Then
'                    btnNewShare.ImageUrl = TOOLBAR_IMAGE_CABINET_ADD_ON
'                    ReturnScripts.Add("document.all.btnNewShare.disabled = false;")
'                End If
'            End If

'            If strId.Trim.Length > 0 Then
'                Select Case strId.Substring(0, 1)
'                    Case "S" 'Share
'                        dt = objFolders.FolderCabinetPermissions(Me.mobjUser.ImagesUserId, NodeId.Value.Replace("S", "") * -1)
'                        If dt.Rows.Count = 0 Then Exit Sub

'                        If dt.Rows(0)("CanAddShare") = "1" Then
'                            btnNewShare.ImageUrl = TOOLBAR_IMAGE_CABINET_ADD_ON
'                            ReturnScripts.Add("document.all.btnNewShare.disabled = false;")
'                        End If

'                        If dt.Rows(0)("CanRenameShare") = "1" Then
'                            Rename.HRef = "javascript:EditModeNode(document.all.NodeId.value);"
'                            imgRenameShare.Alt = "Rename Share"
'                            imgRenameShare.Src = TOOLBAR_IMAGE_RENAME_ON
'                            ReturnScripts.Add("document.all.Rename.href='javascript:EditModeNode(document.all.NodeId.value);';document.all.imgRenameShare.style.cursor = 'hand';")
'                        End If

'                        If dt.Rows(0)("CanDeleteShare") = "1" Then
'                            ReturnScripts.Add("document.all.btnRemoveFolder.disabled = false;")
'                            btnRemoveFolder.ImageUrl = TOOLBAR_IMAGE_REMOVE_ON
'                            btnRemoveFolder.ToolTip = REMOVE_SHARE
'                            btnRemoveFolder.Attributes("onclick") = "return confirm('Remove the \'' + '" & treeSearch.FindNodeById(NodeId.Value).Text.Replace("'", "\'") & "' + '\' cabinet? All files and subfolders will be permanently deleted.');"
'                            btnRemoveFolder.Attributes("style") = "CURSOR: hand"
'                        End If

'                        If dt.Rows(0)("CanAddFolder") = "1" Then
'                            ReturnScripts.Add("document.all.btnShareNewFolder.disabled = false;")
'                            btnShareNewFolder.ImageUrl = TOOLBAR_IMAGE_FOLDER_ADD_ON
'                        End If

'                    Case "W" 'Folder
'                        dt = objFolders.FolderCabinetPermissions(Me.mobjUser.ImagesUserId, NodeId.Value.Replace("W", ""))
'                        If dt.Rows.Count = 0 Then Exit Sub

'                        If dt.Rows(0)("CanAddFolder") = "1" Then
'                            ReturnScripts.Add("document.all.btnShareNewFolder.disabled = false;")
'                            btnShareNewFolder.ImageUrl = TOOLBAR_IMAGE_FOLDER_ADD_ON
'                        End If

'                        If dt.Rows(0)("CanRenameFolder") = "1" Then
'                            Rename.HRef = "javascript:EditModeNode(document.all.NodeId.value);"
'                            imgRenameShare.Alt = "Rename Folder"
'                            imgRenameShare.Src = TOOLBAR_IMAGE_RENAME_ON
'                            ReturnScripts.Add("document.all.Rename.href='javascript:EditModeNode(document.all.NodeId.value);';document.all.imgRenameShare.style.cursor = 'hand';")
'                        End If

'                        If dt.Rows(0)("CanDeleteFolder") = "1" Then
'                            ReturnScripts.Add("document.all.btnRemoveFolder.disabled = false;")
'                            btnRemoveFolder.ImageUrl = TOOLBAR_IMAGE_REMOVE_ON
'                            btnRemoveFolder.ToolTip = REMOVE_FOLDER
'                            btnRemoveFolder.Attributes("onclick") = "return confirm('Remove the \'' + '" & treeSearch.FindNodeById(NodeId.Value).Text.Replace("'", "\'") & "' + '\' folder? All files and subfolders will be permanently deleted.');"
'                            btnRemoveFolder.Attributes("style") = "CURSOR: hand"
'                        End If

'                    Case "F" 'Favorite

'                        Rename.HRef = "javascript:EditModeNode(document.all.NodeId.value);"
'                        imgRenameShare.Src = TOOLBAR_IMAGE_RENAME_ON
'                        imgRenameShare.Alt = "Rename Favorite"
'                        ReturnScripts.Add("document.all.Rename.href='javascript:EditModeNode(document.all.NodeId.value);';document.all.imgRenameShare.style.cursor = 'hand';")

'                        ReturnScripts.Add("document.all.btnRemoveFolder.disabled = false;")
'                        btnRemoveFolder.ImageUrl = TOOLBAR_IMAGE_REMOVE_ON
'                        btnRemoveFolder.ToolTip = REMOVE_FOLDER
'                        btnRemoveFolder.Attributes("onclick") = "return confirm('Remove the \'' + '" & treeSearch.FindNodeById(NodeId.Value).Text.Replace("'", "\'") & "' + ' \' favorite?');"
'                        btnRemoveFolder.Attributes("style") = "CURSOR: hand"

'                End Select

'            End If

'        Catch ex As Exception

'        Finally

'            ReturnControls.Add(btnNewShare)
'            ReturnControls.Add(Rename)
'            ReturnControls.Add(imgRenameShare)
'            ReturnControls.Add(btnRemoveFolder)
'            ReturnControls.Add(btnShareNewFolder)
'            ReturnControls.Add(btnShareNewFolder)

'        End Try

'    End Sub

'    Private Sub DisableToolbar()

'        btnNewShare.Enabled = False
'        btnNewShare.ImageUrl = TOOLBAR_IMAGE_CABINET_ADD_OFF

'        btnShareNewFolder.Enabled = False
'        btnShareNewFolder.ImageUrl = TOOLBAR_IMAGE_FOLDER_ADD_OFF

'        btnRemoveFolder.ImageUrl = TOOLBAR_IMAGE_REMOVE_OFF
'        btnRemoveFolder.Attributes("style") = "CURSOR: normal"

'        imgRenameShare.Src = TOOLBAR_IMAGE_RENAME_OFF

'        ReturnControls.Add(btnNewShare)
'        ReturnControls.Add(btnShareNewFolder)
'        ReturnControls.Add(btnRemoveFolder)
'        ReturnControls.Add(imgRenameShare)
'        ReturnControls.Add(tblShare)

'        ReturnScripts.Add("document.all.btnNewShare.disabled = true;document.all.btnShareNewFolder.disabled = true;document.all.btnRemoveFolder.disabled = true;")
'        ReturnScripts.Add("document.all.Rename.href='#';document.all.imgRenameShare.disabled = true;document.all.imgRenameShare.style.cursor = 'normal';")

'    End Sub

'    Private Sub treeSearch_NodeSelected(ByVal sender As Object, ByVal e As ComponentArt.Web.UI.TreeViewNodeEventArgs) Handles treeSearch.NodeSelected

'        Try

'            UpdateToolbar()

'        Catch ex As Exception

'        End Try

'    End Sub

'    Private Sub btnNewShare_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnNewShare.Click

'        Try

'            Dim dtPermissions As New DataTable

'            Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)

'            Dim strId As String
'            If NodeId.Value.StartsWith("S") Then 'Share
'                strId = (NodeId.Value.Replace("S", "") * -1)
'            Else 'Folder
'                strId = NodeId.Value.Replace("W", "")
'            End If

'            Dim objFolders As New Folders(Me.mconSqlImage, False)
'            Dim intNewFolderId As Integer
'            Dim strFolderName As String
'            strFolderName = NEW_SHARE
'            intNewFolderId = objFolders.InsertShare(Me.mobjUser.ImagesUserId, strFolderName)

'            If intNewFolderId < 0 Then
'                Dim intCount As Integer = 1
'                Do While intNewFolderId < 0
'                    strFolderName = NEW_SHARE & "[" & intCount.ToString & "]"
'                    intNewFolderId = objFolders.InsertShare(Me.mobjUser.ImagesUserId, strFolderName)
'                    intCount += 1
'                Loop
'            End If

'            Dim currentNode As TreeViewNode = treeSearch.FindNodeById("CabsFolders")

'            Dim sb As New System.Text.StringBuilder
'            sb.Append("var node = window['treeSearch'].findNodeById('" & currentNode.ID & "');")
'            sb.Append("var newnode = new ComponentArt_TreeViewNode();")
'            sb.Append("newnode.SetProperty('Text', '" & "" & "');")
'            sb.Append("newnode.SetProperty('ID', 'S" & intNewFolderId.ToString & "');")
'            sb.Append("newnode.SetProperty('ImageUrl', '" & IMAGE_CABINET & "');")
'            sb.Append("newnode.SetProperty('AutoPostBackOnSelect', 'true');")
'            sb.Append("node.AddNode(newnode);")
'            sb.Append("newnode.SaveState();")
'            sb.Append("node.SaveState();")
'            sb.Append("window['treeSearch'].Render();")
'            sb.Append("node.Expand();")

'            'Edit mode
'            sb.Append("var timer = setTimeout(""EditModeNode(document.getElementById('NodeId').value);clearTimeout(timer);"", 300);")
'            sb.Append("document.all.NodeId.value = '" & "S" & intNewFolderId.ToString & "';")
'            sb.Append("document.all.Rename.click();")

'            ReturnScripts.Add(sb.ToString)

'        Catch ex As Exception

'        Finally

'            If Me.mconSqlImage.State <> ConnectionState.Closed Then
'                Me.mconSqlImage.Close()
'                Me.mconSqlImage.Dispose()
'            End If

'            UpdateToolbar()

'        End Try

'    End Sub

'    Private Sub btnRename_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnRename.Click

'        Dim nodeEdited As TreeViewNode = treeSearch.FindNodeById(NodeId.Value)
'        Dim newT As String = NewText.Value
'        Dim nodeValue As String = NodeText.Value

'        Try

'            If IsNothing(nodeEdited) Then
'                Dim parentNode As TreeViewNode = treeSearch.FindNodeById(ParentFolder.Value)
'                Dim newNode As New TreeViewNode
'                newNode.Text = newT
'                newNode.ID = NodeId.Value
'                parentNode.Nodes.Add(newNode)

'                nodeEdited = newNode

'            End If

'            Dim objFolders As New Folders(Functions.BuildConnection(Me.mobjUser))

'            Dim intResult As Integer
'            Select Case nodeEdited.ID.Substring(0, 1)
'                Case "S"

'                    If newT = "" Then
'                        Dim sb As New System.Text.StringBuilder
'                        sb.Append("var node = window['treeSearch'].findNodeById('" & nodeEdited.ID & "');")
'                        sb.Append("node.SetProperty('Text', '" & nodeEdited.Text.Replace("'", "\'") & "');")
'                        sb.Append("node.SetProperty('ImageUrl', '" & IMAGE_CABINET & "');")
'                        sb.Append("node.SaveState();")
'                        sb.Append("window['treeSearch'].Render();")
'                        sb.Append("var timer = setTimeout(""alert('You must specify a name for this cabinet.');EditModeNode(document.getElementById('NodeId').value);clearTimeout(timer);"", 300);")
'                        ReturnScripts.Add(sb.ToString)
'                        Exit Sub
'                    End If

'                    'Update folder
'                    intResult = objFolders.UpdateShare(nodeEdited.ID.Replace("S", ""), newT)

'                    If intResult = -1 Then
'                        Dim sb As New System.Text.StringBuilder
'                        sb.Append("var node = window['treeSearch'].findNodeById('" & nodeEdited.ID & "');")
'                        sb.Append("node.SetProperty('Text', '" & nodeEdited.Text.Replace("'", "\'") & "');")
'                        sb.Append("node.SetProperty('ImageUrl', '" & IMAGE_CABINET & "');")
'                        sb.Append("node.SaveState();")
'                        sb.Append("window['treeSearch'].Render();")
'                        sb.Append("var timer = setTimeout(""alert('A cabinet with the name you specified already exists. Specify a new cabinet name.');EditModeNode(document.getElementById('NodeId').value);clearTimeout(timer);"", 300);")
'                        ReturnScripts.Add(sb.ToString)
'                    Else
'                        Dim sb As New System.Text.StringBuilder
'                        sb.Append("var node = window['treeSearch'].findNodeById('" & nodeEdited.ID & "');")
'                        sb.Append("node.SetProperty('Text', '" & newT.Replace("'", "\'") & "');")
'                        sb.Append("node.SetProperty('ImageUrl', '" & IMAGE_CABINET & "');")
'                        sb.Append("node.SaveState();")
'                        sb.Append("window['treeSearch'].Render();")
'                        ReturnScripts.Add(sb.ToString)

'                    End If

'                Case "F"
'                    'Update favorite

'                    'If newT = nodeEdited.Text Then Exit Sub

'                    If newT = "" Then
'                        Dim sb As New System.Text.StringBuilder
'                        sb.Append("var node = window['treeSearch'].findNodeById('" & nodeEdited.ID & "');")
'                        sb.Append("node.SetProperty('Text', '" & nodeValue.Replace("'", "\'") & "');")
'                        sb.Append("node.SetProperty('ImageUrl', '" & IMAGE_FOLDER & "');")
'                        sb.Append("node.SaveState();")
'                        sb.Append("window['treeSearch'].Render();")
'                        sb.Append("var timer = setTimeout(""alert('You must specify a name for this favorite.');EditModeNode(document.getElementById('NodeId').value);clearTimeout(timer);"", 300);")
'                        ReturnScripts.Add(sb.ToString)
'                        Exit Sub
'                    End If

'                    Dim FavValues() As String = nodeEdited.ID.Replace("F", "").Split(FAVORITE_SPLIT)
'                    Dim FavId As String = FavValues(0)
'                    Dim FolderId As String = FavValues(1)

'                    intResult = objFolders.FavoriteUpdate(Me.mobjUser.ImagesUserId, FavId, newT)

'                    If intResult = -1 Then
'                        Dim sb As New System.Text.StringBuilder
'                        sb.Append("var node = window['treeSearch'].findNodeById('" & nodeEdited.ID & "');")
'                        sb.Append("node.SetProperty('Text', '" & nodeValue & "');")
'                        sb.Append("node.SetProperty('ImageUrl', '" & IMAGE_FOLDER & "');")
'                        sb.Append("node.SaveState();")
'                        sb.Append("window['treeSearch'].Render();")
'                        sb.Append("var timer = setTimeout(""alert('A favorite with the name you specified already exists. Specify a new folder name.');EditModeNode(document.getElementById('NodeId').value);clearTimeout(timer);"", 300);")
'                        ReturnScripts.Add(sb.ToString)
'                    Else
'                        Dim sb As New System.Text.StringBuilder
'                        sb.Append("var node = window['treeSearch'].findNodeById('" & nodeEdited.ID & "');")
'                        sb.Append("node.SetProperty('Text', '" & newT.Replace("'", "\'") & "');")
'                        sb.Append("node.SetProperty('ImageUrl', '" & IMAGE_FOLDER & "');")
'                        sb.Append("node.SaveState();")
'                        sb.Append("window['treeSearch'].Render();")
'                        ReturnScripts.Add(sb.ToString)
'                    End If

'                Case "W"

'                    If newT = "" Then
'                        Dim sb As New System.Text.StringBuilder
'                        sb.Append("var node = window['treeSearch'].findNodeById('" & nodeEdited.ID & "');")
'                        sb.Append("node.SetProperty('Text', '" & nodeValue.Replace("'", "\'") & "');")
'                        sb.Append("node.SetProperty('ImageUrl', '" & IMAGE_FOLDER & "');")
'                        sb.Append("node.SaveState();")
'                        sb.Append("window['treeSearch'].Render();")
'                        sb.Append("var timer = setTimeout(""alert('You must specify a name for this folder.');EditModeNode(document.getElementById('NodeId').value);clearTimeout(timer);"", 300);")
'                        ReturnScripts.Add(sb.ToString)
'                        Exit Sub
'                    End If

'                    'Update folder
'                    intResult = objFolders.UpdateFolder(nodeEdited.ID.Replace("W", ""), newT)

'                    If intResult = -1 Then
'                        Dim sb As New System.Text.StringBuilder
'                        sb.Append("var node = window['treeSearch'].findNodeById('" & nodeEdited.ID & "');")
'                        sb.Append("node.SetProperty('Text', '" & nodeValue.Replace("'", "\'") & "');")
'                        sb.Append("node.SetProperty('ImageUrl', '" & IMAGE_FOLDER & "');")
'                        sb.Append("node.SaveState();")
'                        sb.Append("window['treeSearch'].Render();")
'                        sb.Append("var timer = setTimeout(""alert('A folder with the name you specified already exists. Specify a new folder name.');EditModeNode(document.getElementById('NodeId').value);clearTimeout(timer);"", 300);")
'                        ReturnScripts.Add(sb.ToString)
'                    Else
'                        Dim sb As New System.Text.StringBuilder
'                        sb.Append("var node = window['treeSearch'].findNodeById('" & nodeEdited.ID & "');")
'                        sb.Append("node.SetProperty('Text', '" & newT.Replace("'", "\'") & "');")
'                        sb.Append("node.SetProperty('ImageUrl', '" & IMAGE_FOLDER & "');")
'                        sb.Append("node.SaveState();")
'                        sb.Append("window['treeSearch'].Render();")
'                        'sb.Append("document.all." & nodeEdited.ClientID & ".click();")
'                        ReturnScripts.Add(sb.ToString)


'                    End If

'            End Select

'        Catch ex As Exception

'        Finally

'        End Try

'    End Sub

'    Private Sub btnShareNewFolder_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnShareNewFolder.Click

'        Try

'            Dim currentNode As TreeViewNode = treeSearch.FindNodeById(NodeId.Value)
'            Dim strFolder As String

'            If IsNothing(currentNode) Then
'                If Not IsNothing(treeSearch.SelectedNode) Then
'                    strFolder = treeSearch.SelectedNode.ID
'                Else
'                    strFolder = ParentFolder.Value
'                End If
'            Else
'                strFolder = currentNode.ID
'            End If

'            Dim dtPermissions As New DataTable

'            Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)

'            Dim strId As String
'            If strFolder.StartsWith("S") Then 'Share
'                strFolder = (strFolder.Replace("S", "") * -1)
'            Else 'Folder
'                strFolder = strFolder.Replace("W", "")
'            End If

'            Dim objFolders As New Folders(Me.mconSqlImage, False)
'            Dim intNewFolderId As Integer
'            Dim strFolderName As String
'            strFolderName = NEW_FOLDER
'            intNewFolderId = objFolders.InsertFolder(Me.mobjUser.ImagesUserId, strFolderName, strFolder, dtPermissions)

'            If intNewFolderId < 0 Then
'                Dim intCount As Integer = 1
'                Do While intNewFolderId < 0
'                    strFolderName = "New Folder[" & intCount.ToString & "]"
'                    intNewFolderId = objFolders.InsertFolder(Me.mobjUser.ImagesUserId, strFolderName, strFolder, dtPermissions)
'                    intCount += 1
'                Loop
'            End If

'            Dim sb As New System.Text.StringBuilder
'            If IsNothing(currentNode) Then
'                If Not IsNothing(treeSearch.SelectedNode) Then
'                    sb.Append("var node = window['treeSearch'].findNodeById('" & treeSearch.SelectedNode.ID & "');")
'                Else
'                    sb.Append("var node = window['treeSearch'].findNodeById('" & ParentFolder.Value & "');")
'                End If
'            Else
'                Dim newNode As New TreeViewNode
'                newNode.Text = strFolderName
'                newNode.ID = "W" & intNewFolderId.ToString
'                currentNode.Nodes.Add(newNode)
'                sb.Append("var node = window['treeSearch'].findNodeById('" & currentNode.ID & "');")
'            End If

'            sb.Append("var newnode = new ComponentArt_TreeViewNode();")
'            sb.Append("newnode.SetProperty('ID', 'W" & intNewFolderId.ToString & "');")
'            sb.Append("newnode.SetProperty('ImageUrl', '" & IMAGE_FOLDER & "');")
'            sb.Append("newnode.SetProperty('AutoPostBackOnSelect', 'true');")
'            sb.Append("newnode.SetProperty('ExpandedImageUrl', '" & IMAGE_FOLDER_OPEN & "');")
'            sb.Append("newnode.SetProperty('LabelPadding', '3');")
'            'sb.Append("node.expand();")
'            sb.Append("node.AddNode(newnode);")
'            sb.Append("newnode.SaveState();")
'            sb.Append("node.SaveState();")
'            sb.Append("window['treeSearch'].Render();")
'            sb.Append("node.Expand();")

'            sb.Append("document.all.Rename.click();")

'            ReturnScripts.Add(sb.ToString)

'            NodeId.Value = "W" & intNewFolderId.ToString

'        Catch ex As Exception

'        Finally

'            If Me.mconSqlImage.State <> ConnectionState.Closed Then
'                Me.mconSqlImage.Close()
'                Me.mconSqlImage.Dispose()
'            End If

'            ReturnControls.Add(NodeId)

'        End Try

'    End Sub

'    Private Sub btnRemoveFolder_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnRemoveFolder.Click

'        Try

'            Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)

'            'Dim NodeId As String = currentNode.Value
'            Dim objFolders As New Folders(Me.mconSqlImage, False)

'            Select Case NodeId.Value.Substring(0, 1)

'                Case "S"

'                    Dim blnCanDelete As Boolean = objFolders.ShareDelete(Me.mobjUser.ImagesUserId, NodeId.Value.Replace("S", ""), False)

'                    If blnCanDelete Then
'                        If objFolders.ShareDelete(Me.mobjUser.ImagesUserId, NodeId.Value.Replace("S", ""), True) = 1 Then
'                            Dim sb As New System.Text.StringBuilder
'                            sb.Append("var node = window['treeSearch'].findNodeById('" & NodeId.Value & "');")
'                            sb.Append("node.remove();")
'                            sb.Append("window['treeSearch'].Render();")
'                            ReturnScripts.Add(sb.ToString)

'                        Else
'                            'error deleting
'                        End If
'                    Else
'                        ReturnScripts.Add("alert('You cannot delete a cabinet which contains folders.');")
'                    End If

'                Case "W"

'                    Dim blnCanDelete As Boolean = objFolders.FolderDelete(Me.mobjUser.ImagesUserId, Me.mobjUser.AccountId.ToString, NodeId.Value.Replace("W", ""), False)

'                    If blnCanDelete Then
'                        If objFolders.FolderDelete(Me.mobjUser.ImagesUserId, Me.mobjUser.AccountId.ToString, NodeId.Value.Replace("W", ""), True) = 1 Then
'                            Dim sb As New System.Text.StringBuilder
'                            sb.Append("var node = window['treeSearch'].findNodeById('" & NodeId.Value & "');")
'                            sb.Append("node.remove();")
'                            sb.Append("window['treeSearch'].Render();")
'                            ReturnScripts.Add(sb.ToString)

'                        Else
'                            'error deleting
'                        End If
'                    Else
'                        ReturnScripts.Add("alert('You do not have permission to delete one or more subfolders.');")
'                    End If

'                Case "F"

'                    If objFolders.FavoriteRemove(Me.mobjUser.ImagesUserId, NodeId.Value.Split("�")(0).Replace("F", "")) = NodeId.Value.Split("�")(0).Replace("F", "") Then
'                        Dim sb As New System.Text.StringBuilder
'                        sb.Append("var node = window['treeSearch'].findNodeById('" & NodeId.Value & "');")
'                        sb.Append("node.remove();")
'                        sb.Append("window['treeSearch'].Render();")
'                        ReturnScripts.Add(sb.ToString)

'                    Else
'                        ReturnScripts.Add("alert('An error occured while removing this favorite. Please refresh the page and try again.');")
'                    End If

'            End Select

'        Catch ex As Exception

'        Finally

'            UpdateToolbar()

'        End Try

'    End Sub

'End Class
<%@ Register TagPrefix="uc1" TagName="TopRight" Src="Controls/TopRight.ascx" %>
<%@ Register TagPrefix="uc1" TagName="docHeader" Src="Controls/docHeader.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="Scan.aspx.vb" Inherits="docAssistWeb.Scan"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>docAssist - Scan</title>
		<META content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<META content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<META content="JavaScript" name="vs_defaultClientScript">
		<META content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href=css/facebox.css type="text/css" rel="stylesheet">
	</HEAD>
	<BODY bottomMargin="0" leftMargin="0" topMargin="0" onunload="exitCheck();" onload="start();">
		<FORM id="Form1" method="post" runat="server">
			<TABLE height="100%" cellSpacing="0" cellPadding="0" width="100%">
				<TR>
					<TD colspan="2"><uc1:docheader id="docHeader" runat="server"></uc1:docheader></TD>
				</TR>
				<TR height="100%">
					<td valign="top">
						<table height="100%" width="100%">
							<tr>
								<TD height="100%" valign="top">
									<div id="scanner"><script src=scripts/embedScanner.js></script></div>
								</TD>
								<td width="275px" align="right">
									<iframe id=Attributes align="right" width="100%" height="100%" src="NewAttributes.aspx" frameborder="0"
										scrolling="no"></iframe>
								</td>
							</tr>
						</table>
					</td>
				</TR>
				<tr>
					<td colspan="2"><uc1:topright id="docFooter" runat="server"></uc1:topright></td>
				</tr>
			</TABLE>
		</FORM>
		<script src="scripts/jquery.js" type="text/javascript"></script>
		<script src="scripts/facebox.js" type="text/javascript"></script>
		<script src="scripts/scan.js" type="text/javascript"></script>
	</BODY>
</HTML>

Imports Accucentric.docAssist
Imports Accucentric.docAssist.Data.Images

Partial Class WorkflowDashboard
    Inherits System.Web.UI.Page

    Private mobjUser As Accucentric.docAssist.Web.Security.User

    Private blnItemsFound As Boolean = False
    Private blnMassApproval As Boolean = False

    Private mconSqlCompany As New SqlClient.SqlConnection

    Private Enum WFDASH
        WORKFLOWS = 0
        QUEUES = 1
        DOCUMENTS = 2
        DOCUMENT_DETAIL = 3
        ATTRIBUTES = 4
        REVIEWERS = 5
        MASS_APPROVE_ACTIONS = 6
        MASS_APPROVE_QUEUES = 7
        MASS_APPROVE_BACK_ACTIONS = 8
    End Enum

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblSelect As System.Web.UI.WebControls.Label
    Protected WithEvents lblFilter As System.Web.UI.WebControls.Label
    Protected WithEvents ddlFilter As OptGroupLists.OptGroupDDL
    Protected WithEvents litLpkLicense As System.Web.UI.WebControls.Literal
    Protected WithEvents litUnlockImageCtrl As System.Web.UI.WebControls.Literal
    Protected WithEvents rdoAll As System.Web.UI.WebControls.RadioButton
    Protected WithEvents rdoReview As System.Web.UI.WebControls.RadioButton
    Protected WithEvents rdoViewOnly As System.Web.UI.WebControls.RadioButton
    Protected WithEvents lblGroupBy As System.Web.UI.WebControls.Label
    Protected WithEvents ddlGroupBy As System.Web.UI.WebControls.DropDownList

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try

            Me.mobjUser = BuildUserObject(Request.Cookies("docAssist"))

        Catch ex As Exception

            If ex.Message = "DualLogin" Then
                Response.Cookies("docAssist")("DualLogin") = True
                If Request.Url.Query.Trim.Length > 0 Then
                    Response.Redirect("Default.aspx" & Request.Url.Query)
                Else
                    Server.Transfer("Default.aspx")
                End If
            Else
                If ex.Message = "User is not logged on" Then
                    Response.Redirect("Default.aspx?Action=WFDash")
                End If
            End If

        End Try

        If Not Page.IsPostBack Then

            'RegisterWaitDisabled(ddlWorkflow)
            'RegisterWaitDisabled(ddlQueue)

            Session("WorkflowMode") = ""

            btnCurrentView.Attributes("style") = "VISIBILITY: hidden"

        End If

        Dim conCompany As New SqlClient.SqlConnection

        Try

            If Not Page.IsPostBack Then

                btnMassApproval.Visible = False

                litInfo.Text = "<script>userId = " & Me.mobjUser.ImagesUserId & ";</script>"

                If Not IsNothing(Request.Cookies("WFSort")) Then
                    ddlSort.SelectedValue = Request.Cookies("WFSort").Value
                    'ReturnControls.Add(ddlSort)
                End If

                'Load years
                Dim userTime As DateTime = Now.AddHours(Me.mobjUser.TimeDiffFromGMT)

                For X As Integer = userTime.Year - 5 To userTime.Year
                    Dim lvi As New ListItem(X, X)
                    ToYear.Items.Add(lvi)
                    FromYear.Items.Add(lvi)
                Next

                conCompany = Functions.BuildConnection(Me.mobjUser)

                Dim dsDocuments As New DataSet
                Dim objWf As New Workflow(conCompany, False)

                dsDocuments = objWf.WFDashboard(Me.mobjUser.ImagesUserId, Me.mobjUser.TimeDiffFromGMT)

                If dsDocuments.Tables.Count > 6 Then blnMassApproval = True
                btnMassApproval.Visible = blnMassApproval

                View.InnerHtml = ""
                'ReturnControls.Add(View)

                If dsDocuments.Tables(0).Rows.Count = 0 Then
                    lblResults.Text = "No documents were found."
                    toggle.Visible = False
                    'ReturnControls.Add(toggle)
                    Exit Sub
                Else
                    toggle.Visible = True
                    'ReturnControls.Add(toggle)
                End If

                lblResults.Text = Pivot(dsDocuments, ddlSort.SelectedValue)

                Dim dtWorkflows As New DataTable
                dtWorkflows = objWf.WFHistoryParams(Me.mobjUser.ImagesUserId, "WORKFLOWNAME", Nothing)
                ddlWorkflow.DataSource = dtWorkflows
                ddlWorkflow.DataTextField = "WorkflowName"
                ddlWorkflow.DataBind()
                ddlWorkflow.Items.Insert(0, "All")
                ddlWorkflow.Items(0).Selected = True

                Dim dtQueues As New DataTable
                dtQueues = objWf.WFHistoryParams(Me.mobjUser.ImagesUserId, "QUEUENAME", Nothing)
                ddlQueue.DataSource = dtQueues
                ddlQueue.DataTextField = "QueueName"
                ddlQueue.DataBind()
                ddlQueue.Items.Insert(0, "All")
                ddlQueue.Items(0).Selected = True

                Dim dtAction As New DataTable
                dtAction = objWf.WFHistoryParams(Me.mobjUser.ImagesUserId, "ACTION", Nothing)
                ddlAction.DataSource = dtAction
                ddlAction.DataTextField = "StatusDesc"
                ddlAction.DataBind()
                ddlAction.Items.Insert(0, "All")
                ddlAction.Items(0).Selected = True

                Dim LoadTime As DateTime = Now.AddHours(Me.mobjUser.TimeDiffFromGMT)
                FromMonth.SelectedValue = IIf(LoadTime.Month.ToString.Length = 1, "0" & LoadTime.Month.ToString, LoadTime.Month)
                FromDay.SelectedValue = IIf(LoadTime.Day.ToString.Length = 1, "0" & LoadTime.Day.ToString, LoadTime.Day)
                FromYear.SelectedValue = LoadTime.Year

                ToMonth.SelectedValue = IIf(LoadTime.Month.ToString.Length = 1, "0" & LoadTime.Month.ToString, LoadTime.Month)
                ToDay.SelectedValue = IIf(LoadTime.Day.ToString.Length = 1, "0" & LoadTime.Day.ToString, LoadTime.Day)
                ToYear.SelectedValue = LoadTime.Year

            End If

        Catch ex As Exception

            lblResults.Text = "An error has occured while loading the workflow dashboard. We have been notified of the error."

            Try
                'Functions.LogException(mobjUser.UserId.ToString, mobjUser.AccountId.ToString, "", "Error loading workflow dashboard: " & ex.Message, ex.StackTrace, Server.MachineName, "", Request.UrlReferrer.ToString, Functions.BuildMasterConnection, True)
                Functions.LogException(mobjUser.UserId.ToString, mobjUser.AccountId.ToString, "", "Error loading workflow dashboard: " & ex.Message, ex.StackTrace, Server.MachineName, "", "", Functions.BuildMasterConnection, True)
            Catch loggingEx As Exception

            End Try

        Finally

            'ReturnControls.Add(lblResults)
            'ReturnControls.Add(lblResults)
            'ReturnControls.Add(ddlFilter)

            If conCompany.State <> ConnectionState.Closed Then
                conCompany.Close()
                conCompany.Dispose()
            End If

        End Try

    End Sub

    Private Function ShowRadios()
        Try
            rdoAll.Enabled = True
            rdoReview.Enabled = True
            rdoViewOnly.Enabled = True
            'ReturnControls.Add(rdoAll)
            'ReturnControls.Add(rdoReview)
            'ReturnControls.Add(rdoViewOnly)
        Catch ex As Exception

        End Try
    End Function

    Private Function HideRadios()
        Try
            'rdoAll.Checked = True
            'rdoReview.Checked = False
            'rdoViewOnly.Checked = False
            rdoAll.Enabled = False
            rdoReview.Enabled = False
            rdoViewOnly.Enabled = False
            'ReturnControls.Add(rdoAll)
            'ReturnControls.Add(rdoReview)
            'ReturnControls.Add(rdoViewOnly)
        Catch ex As Exception

        End Try
    End Function

    Private Function CalculateTimePending(ByVal dtActionDateTime As DateTime) As String

        Try
            Dim intMonths As Integer
            Dim intWeeks As Integer
            Dim intDays As Integer
            Dim intMinutes As Integer

            Dim intSecondsDifference As Integer = DateDiff(DateInterval.Second, dtActionDateTime, DateAdd(DateInterval.Hour, Me.mobjUser.TimeDiffFromGMT, Now))

            Dim sbTimePending As New System.Text.StringBuilder

            'Month
            Dim intSecondsInMonth As Integer = 2629743
            If intSecondsDifference >= intSecondsInMonth Then
                If intSecondsDifference Mod intSecondsInMonth = 0 Then
                    sbTimePending.Append((intSecondsDifference / intSecondsInMonth).ToString.PadLeft(2, "0") & "m ")
                Else
                    intMonths = System.Math.Floor(intSecondsDifference / intSecondsInMonth)
                    sbTimePending.Append(intMonths.ToString.PadLeft(2, "0") & "m ")
                    intSecondsDifference = intSecondsDifference Mod intSecondsInMonth
                End If
            End If

            'Week
            Dim intSecondsInWeek As Integer = 604800
            If intSecondsDifference >= intSecondsInWeek Then
                If intSecondsDifference Mod intSecondsInWeek = 0 Then
                    sbTimePending.Append((intSecondsDifference / intSecondsInWeek).ToString.PadLeft(2, "0") & "w ")
                Else
                    intWeeks = System.Math.Floor(intSecondsDifference / intSecondsInWeek)
                    sbTimePending.Append(intWeeks.ToString.PadLeft(2, "0") & "w ")
                    intSecondsDifference = intSecondsDifference Mod intSecondsInWeek
                End If
            End If

            'Days
            Dim intSecondsInDay As Integer = 86400
            If intSecondsDifference >= intSecondsInDay Then
                If intSecondsDifference Mod intSecondsInDay = 0 Then
                    sbTimePending.Append((intSecondsDifference / intSecondsInDay).ToString.PadLeft(2, "0") & "d ")
                Else
                    intDays = System.Math.Floor(intSecondsDifference / intSecondsInDay)
                    sbTimePending.Append(intDays.ToString.PadLeft(2, "0") & "d ")
                    intSecondsDifference = intSecondsDifference Mod intSecondsInDay
                End If
            End If

            'Hours
            Dim intSecondsInHour As Integer = 3600
            If intSecondsDifference >= intSecondsInHour Then
                If intSecondsDifference Mod intSecondsInHour = 0 Then
                    sbTimePending.Append((intSecondsDifference / intSecondsInHour).ToString.PadLeft(2, "0") & "h ")
                Else
                    Dim intHours As Integer = System.Math.Floor(intSecondsDifference / intSecondsInHour)
                    sbTimePending.Append(intHours.ToString.PadLeft(2, "0") & "h ")
                    intSecondsDifference = intSecondsDifference Mod intSecondsInHour
                End If
            End If

            'Minutes
            Dim intSecondsInMinute As Integer = 60
            If intSecondsDifference >= intSecondsInMinute Then
                If intSecondsDifference Mod intSecondsInMinute = 0 Then
                    sbTimePending.Append((intSecondsDifference / intSecondsInMinute).ToString.PadLeft(2, "0") & "m ")
                Else
                    intMinutes = System.Math.Floor(intSecondsDifference / intSecondsInMinute)
                    sbTimePending.Append(intMinutes.ToString.PadLeft(2, "0") & "m ")
                    intSecondsDifference = intSecondsDifference Mod intSecondsInMinute
                End If
            End If

            If intSecondsDifference < 60 And intMonths = 0 And intWeeks = 0 And intDays = 0 And intMinutes = 0 Then
                Return "01m"
            Else
                Return sbTimePending.ToString
            End If

        Catch ex As Exception

        End Try

    End Function

    Private Function Pivot(ByVal ds As DataSet, Optional ByVal sortString As String = "") As String

        Dim row As Integer = 1
        Dim dtResultsTable As New DataTable
        dtResultsTable.Columns.Add(New DataColumn("Row", GetType(System.Int32), Nothing, MappingType.Element))
        Dim dcVersionId As DataColumn
        dcVersionId = New DataColumn("VersionId", GetType(System.Int32), Nothing, MappingType.Element)
        dcVersionId.Unique = True
        dtResultsTable.Columns.Add(dcVersionId)

        Dim sbCollapseAll As New System.Text.StringBuilder
        sbCollapseAll.Append("<script>function CollapseAll(){")

        Dim sbExpandAll As New System.Text.StringBuilder
        sbExpandAll.Append("<script>function ExpandAll(){")

        Dim sb As New System.Text.StringBuilder
        Dim sbScripting As New System.Text.StringBuilder
        sbScripting.Append("<script>function massApprovals() { clearHidden();")

        lblPending.Text = "Pending workflows as of " & Now.AddHours(Me.mobjUser.TimeDiffFromGMT).ToShortTimeString & ". Click <a onclick=""window.location.reload();"">here</a> to reload."

        'sb.Append("<table width=100% class=heading><tr><td class=heading>Pending workflows as of " & Now.AddHours(Me.mobjUser.TimeDiffFromGMT).ToShortTimeString & ". Click <a onclick=""window.location.reload();"">here</a> to reload.</td></tr></table>")

        sb.Append("<table width=100% border=0 align=center cellpadding=0 cellspacing=0 class=workflowTable style=""BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; BORDER-BOTTOM-STYLE: none""><tr align=right><td align=right style=""TEXT-ALIGN: right""></td></tr></table>")

        For Each dr As DataRow In ds.Tables(WFDASH.WORKFLOWS).Rows

            'Queues
            Dim dvQueues As New DataView(ds.Tables(WFDASH.QUEUES))
            dvQueues.RowFilter = "WorkflowID=" & dr("WorkflowId")
            Dim queueCount As Integer = 1

            Dim WorkflowDocumentCount As Integer = New DataView(ds.Tables(WFDASH.DOCUMENT_DETAIL), "WorkflowId='" & dr("WorkflowId") & "'", Nothing, DataViewRowState.CurrentRows).Count

            If dvQueues.Count > 0 And WorkflowDocumentCount > 0 Then

                sb.Append("<table width=100% border=0 align=center cellpadding=20 cellspacing=0 class=workflowTable>")

                'Workflow band
                Dim strWorkflowName As String = dr("WorkflowName")
                sb.Append("<THEAD>")
                sb.Append("<TR>")
                sb.Append("<TH colspan=7 id=redbar><i><b>" & strWorkflowName & "</b></i></TH>")
                sb.Append("</TR>")
                sb.Append("</THEAD>")

                'Column headers
                sb.Append("<THEAD>")
                sb.Append("<TR>")
                sb.Append("<TH width=3% id=bar>Queues</TH>")
                sb.Append("<TH width=2% id=bar>&nbsp;</TH>")
                sb.Append("<TH width=2% align=center id=bar><div align=center>Doc#</div></TH>")
                sb.Append("<TH id=bar>Document Info</TH>")
                sb.Append("<TH id=bar>&nbsp;</TH>")
                sb.Append("<TH id=bar>&nbsp;</TH>")
                sb.Append("<TH id=bar><p align=right>Posted/Pending</p></TH>")
                sb.Append("</TR>")
                sb.Append("</THEAD>")

                Dim strTotalSpanName As String
                Dim strTotalActionName As String
                Dim strTotalDiffSpanName As String
                Dim strActionCaptionSpanName As String
                Dim strTotalingSpanName As String

                For Each currentQueue As DataRowView In dvQueues

                    Dim totalAttributeId As Integer = 0
                    Dim totalAttributes As Boolean = False
                    Dim queueTotal As String = "0.00"

                    If blnMassApproval Then
                        If currentQueue("TotalAttributeId") > 0 Then
                            totalAttributeId = currentQueue("TotalAttributeId")
                            totalAttributes = True
                        End If
                    End If

                    Dim dvDocuments As New DataView(ds.Tables(WFDASH.DOCUMENT_DETAIL))
                    dvDocuments.RowFilter = "QueueId=" & currentQueue("QueueId") & " AND WorkflowId=" & dr("WorkflowId")

                    Dim strSectionId As String = "row" & System.Guid.NewGuid.ToString.Replace("-", "")
                    Dim strImageId As String = "img" & System.Guid.NewGuid.ToString.Replace("-", "")

                    Dim blnViewOnly As Boolean = False
                    Dim strQueueAccessClass As String
                    Dim strDocumentAccessClass As String
                    Select Case CInt(currentQueue("AccessLevel"))
                        Case 1
                            blnViewOnly = True
                            strQueueAccessClass = "class=queueNameMonitor"
                            strDocumentAccessClass = "class=viewonly"
                        Case 2
                            blnViewOnly = False
                            strQueueAccessClass = "class=queueName"
                            strDocumentAccessClass = ""
                    End Select

                    If dvDocuments.Count > 0 Then

                        'Build reviewers tooltip
                        Dim dvReviewers As New DataView(ds.Tables(WFDASH.REVIEWERS))
                        dvReviewers.RowFilter = "QueueId='" & currentQueue("QueueId") & "'"
                        Dim sbReviewers As New System.Text.StringBuilder

                        sbReviewers.Append("Reviewers:" & vbCr)
                        For Each currentReviewer As DataRowView In dvReviewers
                            sbReviewers.Append(currentReviewer("LastName") & Space(1) & currentReviewer("FirstName") & " - " & currentReviewer("EmailAddress") & vbCr)
                        Next

                        Dim strReviewers As String = sbReviewers.ToString.Substring(0, sbReviewers.ToString.LastIndexOf(vbCr))

                        Dim strQueueName As String = currentQueue("QueueName")
                        sb.Append("<TR class=queue>")

                        'If blnMassApproval And blnViewOnly = False Then
                        '    sb.Append("<TD colspan=4 " & strQueueAccessClass & "><div id=masminus><img style=""CURSOR: hand"" id=""" & strImageId & """ onclick=""toggle('" & strSectionId & "','" & strImageId & "','" & dr("WorkflowId") & "." & currentQueue("QueueId") & "');"" src=""Images/workflow_dashboard/wf_minus.gif"" id=toggle" & queueCount & " width=17 height=16></div>")
                        'Else
                        '    sb.Append("<TD colspan=6 " & strQueueAccessClass & "><div id=masminus><img style=""CURSOR: hand"" id=""" & strImageId & """ onclick=""toggle('" & strSectionId & "','" & strImageId & "','" & dr("WorkflowId") & "." & currentQueue("QueueId") & "');"" src=""Images/workflow_dashboard/wf_minus.gif"" id=toggle" & queueCount & " width=17 height=16></div>")
                        'End If

                        Dim blnHideBand As Boolean = False
                        'Dim cookieName As String = dr("WorkflowId") & "." & currentQueue("QueueId")

                        If currentQueue("State") = 0 Then
                            'Closed
                            blnHideBand = True
                            If blnMassApproval And blnViewOnly = False Then
                                sb.Append("<TD colspan=4 " & strQueueAccessClass & "><div id=masminus><img style=""CURSOR: hand"" id=""" & strImageId & """ onclick=""toggle('" & strSectionId & "','" & strImageId & "','" & dr("WorkflowId") & "." & currentQueue("QueueId") & "', true);"" src=""Images/workflow_dashboard/wf_plus.gif"" id=toggle" & queueCount & " width=17 height=16></div>")
                            Else
                                sb.Append("<TD colspan=6 " & strQueueAccessClass & "><div id=masminus><img style=""CURSOR: hand"" id=""" & strImageId & """ onclick=""toggle('" & strSectionId & "','" & strImageId & "','" & dr("WorkflowId") & "." & currentQueue("QueueId") & "', true);"" src=""Images/workflow_dashboard/wf_plus.gif"" id=toggle" & queueCount & " width=17 height=16></div>")
                            End If
                        Else
                            'Open
                            If blnMassApproval And blnViewOnly = False Then
                                sb.Append("<TD colspan=4 " & strQueueAccessClass & "><div id=masminus><img style=""CURSOR: hand"" id=""" & strImageId & """ onclick=""toggle('" & strSectionId & "','" & strImageId & "','" & dr("WorkflowId") & "." & currentQueue("QueueId") & "', true);"" src=""Images/workflow_dashboard/wf_minus.gif"" id=toggle" & queueCount & " width=17 height=16></div>")
                            Else
                                sb.Append("<TD colspan=6 " & strQueueAccessClass & "><div id=masminus><img style=""CURSOR: hand"" id=""" & strImageId & """ onclick=""toggle('" & strSectionId & "','" & strImageId & "','" & dr("WorkflowId") & "." & currentQueue("QueueId") & "', true);"" src=""Images/workflow_dashboard/wf_minus.gif"" id=toggle" & queueCount & " width=17 height=16></div>")
                            End If
                        End If

                        'If Not IsNothing(Request.Cookies(cookieName)) Then
                        '    Select Case Request.Cookies(cookieName).Value
                        '        Case "open"

                        '        Case "closed"

                        '    End Select
                        'Else
                        '    If blnMassApproval And blnViewOnly = False Then
                        '        sb.Append("<TD colspan=4 " & strQueueAccessClass & "><div id=masminus><img style=""CURSOR: hand"" id=""" & strImageId & """ onclick=""toggle('" & strSectionId & "','" & strImageId & "','" & dr("WorkflowId") & "." & currentQueue("QueueId") & "');"" src=""Images/workflow_dashboard/wf_minus.gif"" id=toggle" & queueCount & " width=17 height=16></div>")
                        '    Else
                        '        sb.Append("<TD colspan=6 " & strQueueAccessClass & "><div id=masminus><img style=""CURSOR: hand"" id=""" & strImageId & """ onclick=""toggle('" & strSectionId & "','" & strImageId & "','" & dr("WorkflowId") & "." & currentQueue("QueueId") & "');"" src=""Images/workflow_dashboard/wf_minus.gif"" id=toggle" & queueCount & " width=17 height=16></div>")
                        '    End If
                        'End If

                        sb.Append("<strong>" & strQueueName & "</strong> &raquo; " & dvDocuments.Count & " documents " & IIf(blnViewOnly, "(View Only)", "(Action Required)") & " <img src=""Images/workflow_dashboard/wf_group.gif"" width=16 height=16 alt=""" & strReviewers & """></TD>")

                        If blnMassApproval And blnViewOnly = False Then

                            Dim dvActions As New DataView(ds.Tables(WFDASH.MASS_APPROVE_ACTIONS))
                            dvActions.RowFilter = "QueueId='" & currentQueue("QueueId") & "'"

                            Dim strActionApprovalName As String = "Action" & currentQueue("QueueId") & dr("WorkflowId")
                            Dim strQueueApprovalName As String = "Queue" & currentQueue("QueueId") & dr("WorkflowId")
                            Dim strUpdateQueues As String = "UpdateQueues" & currentQueue("QueueId") & dr("WorkflowId")

                            If dvActions.Count > 0 Then

                                'Append queue to hidden field
                                sbScripting.Append("appendHidden('" & currentQueue("QueueId") & "�');")

                                If blnMassApproval And blnViewOnly = False Then
                                    sb.Append("<TD class=queueName colspan=4 align=right>")
                                Else
                                    sb.Append("<TD class=queueName>")
                                End If

                                strTotalSpanName = "QueueTotalSpan" & currentQueue("QueueId") & dr("WorkflowId")
                                strTotalActionName = "QueueTotalActionSpan" & currentQueue("QueueId") & dr("WorkflowId")
                                strTotalDiffSpanName = "QueueTotalDiffSpan" & currentQueue("QueueId") & dr("WorkflowId")
                                strActionCaptionSpanName = "QueueActionCaptionDiffSpan" & currentQueue("QueueId") & dr("WorkflowId")
                                strTotalingSpanName = "TotalingSpanName" & currentQueue("QueueId") & dr("WorkflowId")

                                'Approval action drop down
                                sb.Append("<select class=workflowTable onchange=""massApprovals();" & strUpdateQueues & "();"" id=" & strActionApprovalName & ">")
                                sb.Append("<option value=""-1"">Select an action...</option>")

                                Dim dvApprovalQueues As New DataView(ds.Tables(WFDASH.MASS_APPROVE_QUEUES))
                                Dim sbActionIds As New System.Text.StringBuilder
                                sbActionIds.Append("QueueActionId IN (")
                                Dim x As Integer = 1

                                'Build script to update queues drop down list
                                Dim sbUpdateQueues As New System.Text.StringBuilder
                                sbUpdateQueues.Append("<script>function " & strUpdateQueues & "() {")

                                For Each dvItemFilter As DataRowView In dvActions
                                    sbActionIds.Append(dvItemFilter("QueueActionId"))
                                    If x < dvActions.Count Then sbActionIds.Append(", ")
                                    x += 1

                                    'Hiding and showing of drop down list based on the type of review action

                                    sbUpdateQueues.Append("if (document.all." & strActionApprovalName & ".value == '-1') { ")
                                    sbUpdateQueues.Append("document.all." & strQueueApprovalName & ".length = 0;")
                                    sbUpdateQueues.Append("var anOption = new Option('Select a queue...','-1');")
                                    sbUpdateQueues.Append("document.all." & strQueueApprovalName & ".options.add(anOption);")
                                    sbUpdateQueues.Append("document.all." & strQueueApprovalName & ".disabled = true;")
                                    sbUpdateQueues.Append("massApprovals(); }")



                                    If dvItemFilter("WorkflowAction") = "E" Then 'Ending action
                                        sbUpdateQueues.Append("if (document.all." & strActionApprovalName & ".value == '" & dvItemFilter("QueueActionId") & "-" & dvItemFilter("StatusId") & "') { ")

                                        If dvItemFilter("Total") = "0" Then
                                            'sbUpdateQueues.Append("alert('hide');")
                                            If totalAttributes Then sbUpdateQueues.Append("document.all." & strTotalingSpanName & ".style.display = 'none';")
                                        Else
                                            'sbUpdateQueues.Append("alert('show');")
                                            If totalAttributes Then sbUpdateQueues.Append("document.all." & strTotalingSpanName & ".style.display = 'inline';")
                                        End If

                                        sbUpdateQueues.Append("document.all." & strQueueApprovalName & ".style.display = 'none'; ")
                                        sbUpdateQueues.Append("document.all." & strQueueApprovalName & ".length = 0;")
                                        sbUpdateQueues.Append("var anOption = new Option('Cancelled','0');")
                                        sbUpdateQueues.Append("document.all." & strQueueApprovalName & ".options.add(anOption);")
                                        sbUpdateQueues.Append("} massApprovals();")
                                    ElseIf dvItemFilter("WorkflowAction") = "F" Then 'Forward action
                                        sbUpdateQueues.Append("if (document.all." & strActionApprovalName & ".value == '" & dvItemFilter("QueueActionId") & "-" & dvItemFilter("StatusId") & "') { ")
                                        sbUpdateQueues.Append("document.all." & strQueueApprovalName & ".style.display = 'inline';")

                                        If dvItemFilter("Total") = "0" Then
                                            'sbUpdateQueues.Append("alert('hide');")
                                            If totalAttributes Then sbUpdateQueues.Append("document.all." & strTotalingSpanName & ".style.display = 'none';")
                                        Else
                                            'sbUpdateQueues.Append("alert('show');")
                                            If totalAttributes Then sbUpdateQueues.Append("document.all." & strTotalingSpanName & ".style.display = 'inline';")
                                        End If


                                        sbUpdateQueues.Append("} massApprovals();")
                                    ElseIf dvItemFilter("WorkflowAction") = "B" Then 'Back action
                                        Dim dvStatusId As New DataView(ds.Tables(WFDASH.MASS_APPROVE_ACTIONS))
                                        dvStatusId.RowFilter = "QueueActionId='" & dvItemFilter("QueueActionId") & "'"
                                        Dim statusId As Integer = dvStatusId(0)("StatusId")
                                        sbUpdateQueues.Append("if (document.all." & strActionApprovalName & ".value == '" & dvItemFilter("QueueActionId") & "-" & dvItemFilter("StatusId") & "') { ")
                                        sbUpdateQueues.Append("document.all." & strQueueApprovalName & ".style.display = 'inline';")

                                        If dvItemFilter("Total") = "0" Then
                                            'sbUpdateQueues.Append("alert('hide');")
                                            If totalAttributes Then sbUpdateQueues.Append("document.all." & strTotalingSpanName & ".style.display = 'none';")
                                        Else
                                            'sbUpdateQueues.Append("alert('show');")
                                            If totalAttributes Then sbUpdateQueues.Append("document.all." & strTotalingSpanName & ".style.display = 'inline';")
                                        End If

                                        'Add back actions to queue drop down
                                        Dim dvBackActions As New DataView(ds.Tables(WFDASH.MASS_APPROVE_BACK_ACTIONS))
                                        dvBackActions.RowFilter = "QueueId='" & dvItemFilter("QueueId") & "' AND StatusId='" & statusId & "'"

                                        sbUpdateQueues.Append("document.all." & strQueueApprovalName & ".length = 0;")
                                        sbUpdateQueues.Append("var anOption = new Option('Send to...','-1');")
                                        sbUpdateQueues.Append("document.all." & strQueueApprovalName & ".options.add(anOption);")
                                        For Each backItem As DataRowView In dvBackActions
                                            sbUpdateQueues.Append("var anOption = new Option('" & backItem("QueueCode") & "','" & backItem("ChildQueueId") & "');")
                                            sbUpdateQueues.Append("document.all." & strQueueApprovalName & ".options.add(anOption);")
                                            sbUpdateQueues.Append("document.all." & strQueueApprovalName & ".disabled = false;")
                                        Next

                                        sbUpdateQueues.Append("} massApprovals();")

                                    End If

                                Next

                                sbActionIds.Append(")")
                                dvApprovalQueues.RowFilter = sbActionIds.ToString

                                sbUpdateQueues.Append("switch(document.all." & strActionApprovalName & ".value) {")

                                For Each dvItems As DataRowView In dvApprovalQueues

                                    Dim dvStatusId As New DataView(ds.Tables(WFDASH.MASS_APPROVE_ACTIONS), "QueueActionId='" & dvItems("QueueActionId") & "'", "", DataViewRowState.OriginalRows)
                                    Dim statusId As Integer = dvStatusId.Item(0)("StatusId")

                                    sbUpdateQueues.Append("case '" & dvItems("QueueActionId") & "-" & statusId & "':")
                                    'Add items to queue
                                    Dim dvQueuesToAdd As New DataView(ds.Tables(WFDASH.MASS_APPROVE_QUEUES))
                                    dvQueuesToAdd.RowFilter = "QueueActionId='" & dvItems("QueueActionId") & "'"

                                    sbUpdateQueues.Append("document.all." & strQueueApprovalName & ".length = 0;")
                                    sbUpdateQueues.Append("var anOption = new Option('Select a queue...','-1');")
                                    sbUpdateQueues.Append("document.all." & strQueueApprovalName & ".options.add(anOption);massApprovals();")
                                    For Each itemAdd As DataRowView In dvQueuesToAdd
                                        sbUpdateQueues.Append("var anOption = new Option('" & itemAdd("QueueCode") & "','" & itemAdd("QueueId") & "');")
                                        sbUpdateQueues.Append("document.all." & strQueueApprovalName & ".options.add(anOption);")
                                        sbUpdateQueues.Append("document.all." & strQueueApprovalName & ".disabled = false;")
                                    Next
                                    sbUpdateQueues.Append("break;")
                                Next

                                sbUpdateQueues.Append("default: break;}")
                                sbUpdateQueues.Append("}</script>")

                                litQueues.Text = sbUpdateQueues.ToString

                                For Each dvItem As DataRowView In dvActions
                                    sb.Append("<option value=""" & dvItem("QueueActionId") & "-" & dvItem("StatusId") & """>" & dvItem("StatusDesc") & "</option>")
                                Next
                                sb.Append("</select>")

                                'Append action to hidden field
                                sbScripting.Append("appendHidden(document.all." & strActionApprovalName & ".value + '�');")

                                'Queue drop down
                                sb.Append("<select disabled=true class=workflowTable onchange=""massApprovals();"" id=" & strQueueApprovalName & ">")
                                sb.Append("<option value=""-1"">Select a queue...</option>")
                                sb.Append("</select>")

                                'Append Send to hidden field
                                sbScripting.Append("appendHidden(document.all." & strQueueApprovalName & ".value + '�');")

                                sb.Append("</TD>")

                            Else

                                sb.Append("<TD class=queueName>&nbsp;</TD>")

                            End If

                        Else
                            sb.Append("<TD class=queueName>&nbsp;</TD>")
                        End If

                        sb.Append("</TR>")

                        sbCollapseAll.Append("var " & strImageId & " = document.getElementById('" & strImageId & "').src;" & vbCr)
                        sbCollapseAll.Append("if (" & strImageId & ".substring(" & strImageId & ".length - 38, " & strImageId & ".length) == 'Images/workflow_dashboard/wf_minus.gif')" & vbCr)
                        sbCollapseAll.Append("{ toggle('" & strSectionId & "','" & strImageId & "','" & dr("WorkflowId") & "." & currentQueue("QueueId") & "', false); }")

                        sbExpandAll.Append("var imgsrc = document.getElementById('" & strImageId & "').src; if (imgsrc.substring(imgsrc.length - 37, imgsrc.length) == 'Images/workflow_dashboard/wf_plus.gif'){ toggle('" & strSectionId & "','" & strImageId & "','" & dr("WorkflowId") & "." & currentQueue("QueueId") & "', false); }" & vbCr)

                        If blnHideBand Then
                            sb.Append("<TBODY style=""DISPLAY:none"" id=" & strSectionId & ">")
                        Else
                            sb.Append("<TBODY id=" & strSectionId & ">")
                        End If

                        'Documents
                        Dim counter As Integer = 0

                        If sortString <> "" Then
                            dvDocuments.Sort = sortString & " ASC"
                        End If

                        For Each currentDocument As DataRowView In dvDocuments

                            counter += 1

                            Dim strDocId As String = currentDocument("ImageId")
                            Dim strVersionId As String = currentDocument("VersionId")
                            Dim strSubmitter As String = currentDocument("Submitter")
                            Dim strDocType As String = New DataView(ds.Tables(WFDASH.DOCUMENTS), "DocumentId=" & currentDocument("DocumentID"), "", DataViewRowState.OriginalRows).Item(0)("DocumentName")

                            Dim strFilename As String = ""
                            Dim blnFilename As Boolean = False
                            If Not currentDocument("Filename") Is System.DBNull.Value Then
                                If Not CStr(Server.UrlDecode(currentDocument("Filename"))).Trim = "" Then
                                    blnFilename = True
                                    strFilename = Server.UrlDecode(currentDocument("Filename"))
                                End If
                            End If

                            Dim strTitle As String = ""
                            Dim blnTitle As Boolean = False
                            If Not currentDocument("Title") Is System.DBNull.Value Then
                                If Not CStr(Server.UrlDecode(currentDocument("Title"))).Trim = "" Then
                                    blnTitle = True
                                    strTitle = Server.UrlDecode(currentDocument("Title"))
                                End If
                            End If

                            Dim strDesc As String = ""
                            Dim blnDesc As Boolean = False
                            If Not currentDocument("Description") Is System.DBNull.Value Then
                                If Not CStr(Server.UrlDecode(currentDocument("Description"))).Trim = "" Then
                                    blnDesc = True
                                    strDesc = Server.UrlDecode(currentDocument("Description"))
                                End If
                            End If

                            'Document row
                            Dim rowId As String = "row" & System.Guid.NewGuid.ToString.Replace("-", "")
                            If counter Mod 2 = 0 Then
                                sb.Append("<TR id=row" & strVersionId & " style=""CURSOR: hand"" onclick=""LoadImage('" & strVersionId & "');"" class=odd onmouseover=""this.className = 'hlt';"" onmouseout=""this.className = 'odd';"">")
                            Else
                                sb.Append("<TR id=row" & strVersionId & " style=""CURSOR: hand"" onclick=""LoadImage('" & strVersionId & "');"" onmouseover=""this.className = 'hlt';"" onmouseout=""this.className = '';"">")
                            End If

                            Dim strMassApprovalCheckboxName As String = "Checkbox" & strVersionId

                            If blnMassApproval And blnViewOnly = False Then

                                'Append Version Id's to hidden field
                                If totalAttributes Then
                                    'add(value, span, diffspan, tltspan)
                                    sb.Append("<TD valign=bottom align=right><input id=" & strMassApprovalCheckboxName & " type=checkbox onclick=""if (this.checked) { add('REPLACEVALUE','" & _
                                    strTotalActionName & "','" & strTotalDiffSpanName & "','" & strTotalSpanName & "'); } else { subtract('REPLACEVALUE','" & strTotalActionName & _
                                    "','" & strTotalDiffSpanName & "','" & strTotalSpanName & "'); };event.cancelBubble=true;massApprovals();""></TD>")
                                Else
                                    sb.Append("<TD valign=bottom align=right><input id=" & strMassApprovalCheckboxName & " type=checkbox onclick=""event.cancelBubble=true;massApprovals();""></TD>")
                                End If

                                'Get CurrentRouteDetId
                                Dim dv As New DataView(ds.Tables(WFDASH.DOCUMENT_DETAIL))
                                dv.RowFilter = "ImageId='" & currentDocument("ImageId") & "'"
                                Dim currentRouteDetId As Integer = dv(0)("CurrentRouteDetId")

                                sbScripting.Append("if (document.all." & strMassApprovalCheckboxName & ".checked == true) { appendHidden('" & strVersionId & "-" & currentRouteDetId.ToString & ",'); }")
                            Else
                                sb.Append("<TD>&nbsp;</TD>")
                            End If

                            Dim strUrgent As String
                            If currentDocument("Urgent") Then
                                strUrgent = "id=urgent"
                            Else
                                strUrgent = ""
                            End If

                            If counter Mod 2 = 0 Then
                                sb.Append("<TD " & strDocumentAccessClass & " align=center valign=top " & strUrgent & ">&nbsp;</TD>")
                            Else
                                sb.Append("<TD " & strDocumentAccessClass & " align=center valign=top " & strUrgent & ">&nbsp;</TD>")
                            End If

                            sb.Append("<TD " & strDocumentAccessClass & " align=center scope=row><div align=center><a href=#>" & strDocId & "</a></div></TD>")
                            sb.Append("<TD " & strDocumentAccessClass & " scope=row><p><strong>Submitter</strong>: <a href=#>" & strSubmitter & "</a><br>")
                            sb.Append("<strong>Doc. Type</strong>: <a href=# id=docType" & strVersionId & ">" & strDocType & "</a><br>")
                            If blnFilename Then sb.Append("<strong>File Name</strong>: <a href=#>" & strFilename & "</a>")

                            Dim charactersToDisplay As Integer = 50

                            If blnTitle Then
                                'If strTitle.Length > charactersToDisplay Then
                                'sb.Append("<strong>Title</strong>: <a href=# id=title" & strVersionId & ">" & Left(strTitle, charactersToDisplay) & "...</a>")
                                ''ReturnScripts.Add("$(""#title" & strVersionId & """).qtip({ content: '" & strTitle & "', position: { corner: { target: 'bottomRight', tooltip: 'bottomLeft'}}});")
                                'Else
                                sb.Append("<br><strong>Title</strong>: <a href=# id=title" & strVersionId & ">" & strTitle & "</a>")
                                'End If
                            End If

                            If blnDesc Then
                                'If strDesc.Length > charactersToDisplay Then
                                '    sb.Append("<br><strong>Description</strong>: <a href=# id=desc" & strVersionId & ">" & Left(strDesc, charactersToDisplay) & "...</a>")
                                'Else
                                sb.Append("<br><strong>Description</strong>: <a href=# id=desc" & strVersionId & ">" & strDesc & "</a>")
                                'End If
                            End If

                            sb.Append("</p></TD>")

                            Dim dvAttributes As New DataView(ds.Tables(WFDASH.ATTRIBUTES))
                            dvAttributes.RowFilter = "VersionId=" & strVersionId

                            'Append document to next/rev result table
                            Try
                                Dim drTrack As DataRow = dtResultsTable.NewRow
                                drTrack("Row") = row
                                drTrack("VersionId") = strVersionId
                                dtResultsTable.Rows.Add(drTrack)
                                row += 1
                            Catch ex As Exception

                            End Try


                            Dim totalingAttributeValue As String = ""

                            If dvAttributes.Count > 0 Then

                                'Determine number of rows needed
                                Dim intMaxRows As Integer = Math.Ceiling(dvAttributes.Count / 2)

                                'Build first column, odd numbers
                                sb.Append("<TD id=atts1-" & strVersionId & " " & strDocumentAccessClass & " scope=row>")
                                For x As Integer = 0 To dvAttributes.Count - 1
                                    If x < dvAttributes.Count Then
                                        If dvAttributes(x)("AttributeDataType") = "Date" Then
                                            If Not IsDate(dvAttributes(x)("AttributeValue")) Then
                                                'Locale causing an invalid date. Output it as a string.
                                                sb.Append(" " & dvAttributes(x)("AttributeName") & ": <a href=#>" & dvAttributes(x)("AttributeValue") & "</a><br>")
                                            Else
                                                sb.Append(" " & dvAttributes(x)("AttributeName") & ": <a href=#>" & CDate(dvAttributes(x)("AttributeValue")).ToShortDateString & "</a><br>")
                                            End If
                                            'sb.Append(" " & dvAttributes(x)("AttributeName") & ": <a href=#>" & CDate(dvAttributes(x)("AttributeValue")).ToShortDateString & "</a><br>")
                                        Else
                                            sb.Append(" " & dvAttributes(x)("AttributeName") & ": <a href=#>" & dvAttributes(x)("AttributeValue") & "</a><br>")
                                        End If
                                    Else
                                        sb.Append("&nbsp;")
                                    End If

                                    If totalAttributes Then
                                        If dvAttributes(x)("AttributeId") = totalAttributeId Then
                                            If IsNumeric(dvAttributes(x)("AttributeValue")) Then
                                                queueTotal = queueTotal + CDbl(dvAttributes(x)("AttributeValue"))
                                                If totalingAttributeValue = "" Then
                                                    totalingAttributeValue = CStr(CDbl(dvAttributes(x)("AttributeValue")))
                                                    If totalAttributes Then sb.Replace("REPLACEVALUE", totalingAttributeValue)
                                                End If
                                            Else
                                                If totalAttributes Then sb.Replace("REPLACEVALUE", "0")
                                            End If
                                        End If
                                    End If

                                    x += 1

                                Next
                                sb.Append("</TD>")

                                'Build first column, even numbers
                                sb.Append("<TD id=atts2-" & strVersionId & " " & strDocumentAccessClass & " scope=row>")
                                For x As Integer = 1 To dvAttributes.Count - 1

                                    If x < dvAttributes.Count Then
                                        If dvAttributes(x)("AttributeDataType") = "Date" Then

                                            If Not IsDate(dvAttributes(x)("AttributeValue")) Then
                                                sb.Append(" " & dvAttributes(x)("AttributeName") & ": <a href=#>" & dvAttributes(x)("AttributeValue") & "</a><br>")
                                            Else
                                                sb.Append(" " & dvAttributes(x)("AttributeName") & ": <a href=#>" & CDate(dvAttributes(x)("AttributeValue")).ToShortDateString & "</a><br>")
                                            End If

                                        Else
                                            sb.Append(" " & dvAttributes(x)("AttributeName") & ": <a href=#>" & dvAttributes(x)("AttributeValue") & "</a><br>")
                                        End If
                                    Else
                                        sb.Append("&nbsp;")
                                    End If

                                    If totalAttributes Then
                                        If dvAttributes(x)("AttributeId") = totalAttributeId Then
                                            If IsNumeric(dvAttributes(x)("AttributeValue")) Then
                                                queueTotal = queueTotal + CDbl(dvAttributes(x)("AttributeValue"))
                                                If totalingAttributeValue = "" Then
                                                    totalingAttributeValue = CStr(CDbl(dvAttributes(x)("AttributeValue")))
                                                    If totalAttributes Then sb.Replace("REPLACEVALUE", totalingAttributeValue)
                                                End If
                                            Else
                                                If totalAttributes Then sb.Replace("REPLACEVALUE", "0")
                                            End If
                                        End If
                                    End If

                                    x += 1

                                Next

                                sb.Append("&nbsp;</TD>")

                            Else
                                If totalAttributes Then sb.Replace("REPLACEVALUE", "0")
                                sb.Append("<TD " & strDocumentAccessClass & " scope=row>")
                                sb.Append("&nbsp;")
                                sb.Append("</TD>")
                                sb.Append("<TD " & strDocumentAccessClass & " scope=row>")
                                sb.Append("&nbsp;")
                                sb.Append("</TD>")

                            End If

                            sb.Append("<TD " & strDocumentAccessClass & "><div align=right>" & CDate(currentDocument("ActionDateTime")).ToShortDateString & Space(1) & CDate(currentDocument("ActionDateTime")).ToShortTimeString.ToLower.Replace(" ", "") & "<br>")
                            sb.Append(CalculateTimePending(CDate(currentDocument("ActionDateTime"))))
                            sb.Append("</div></TD>")
                            sb.Append("</TR>")

                            If blnMassApproval And blnViewOnly = False And totalAttributes And counter = dvDocuments.Count Then

                                'sb.Append("<DIV id=""" & strTotalingSpanName & """ style=""DISPLAY: none"">")
                                'sb.Append("<TBODY><TR align=right class=none><td width=200>Total: </td>")
                                'sb.Append("<td colspan=6 align=right>")
                                'sb.Append("<SPAN id=""" & strTotalSpanName.ToString & """>")
                                'sb.Append(CDbl(queueTotal).ToString("0.00"))
                                'sb.Append("</SPAN></TD>")
                                'sb.Append("<TBODY><TR align=right class=none><td width=200>Action Total: </td>")
                                'sb.Append("<td colspan=6 align=right>")
                                'sb.Append("<SPAN id=""" & strTotalActionName.ToString & """>")
                                'sb.Append("0.00")
                                'sb.Append("</SPAN></TD>")
                                'sb.Append("<TBODY><TR align=right class=none><td width=200>Difference: </td>")
                                'sb.Append("<td colspan=6 align=right>")
                                'sb.Append("<SPAN id=""" & strTotalDiffSpanName.ToString & """>")
                                'sb.Append(CDbl(queueTotal).ToString("0.00"))
                                'sb.Append("</SPAN></TD>")
                                'sb.Append("</DIV>")

                                sb.Append("<TBODY><TR align=right class=none><td colspan=7 align=right>")
                                sb.Append("<DIV id=""" & strTotalingSpanName & """ style=""DISPLAY: none"">")
                                sb.Append("Total:<img src=dummy.gif height=1 width=48><SPAN id=""" & strTotalSpanName.ToString & """>")
                                sb.Append(CDbl(queueTotal).ToString("0.00"))
                                sb.Append("</SPAN><BR>")

                                sb.Append("Action Total:<img src=dummy.gif height=1 width=14><SPAN id=""" & strTotalActionName.ToString & """>")
                                sb.Append("0.00")
                                sb.Append("</SPAN><BR>")

                                sb.Append("Difference:<img src=dummy.gif height=1 width=22><SPAN id=""" & strTotalDiffSpanName.ToString & """>")
                                sb.Append(CDbl(queueTotal).ToString("0.00"))
                                sb.Append("</SPAN>")
                                sb.Append("</DIV>")
                                sb.Append("</td></TR></TBODY>")

                            End If

                        Next

                        If blnMassApproval And blnViewOnly = False Then
                            sbScripting.Append("appendHidden('�');")
                        End If

                    End If

                    sb.Append("</TBODY>")
                    queueCount += 1

                Next

                sb.Append("<tfoot><TR><TH colspan=7><img src=../images/spacer.gif width=1 height=4></TH></TR></tfoot>")
                sb.Append("</table>")

            End If

        Next

        Session("dtVersionId") = dtResultsTable
        Session("intTotalDocuments") = dtResultsTable.Rows.Count

        sbScripting.Append("}</script>")
        sbCollapseAll.Append("}</script>")
        sbExpandAll.Append("}</script>")

        Collapse.Text = sbCollapseAll.ToString
        Expand.Text = sbExpandAll.ToString
        Scripting.Text = sbScripting.ToString

        Return sb.ToString

    End Function

    Private Function PivotHistory(ByVal ds As DataSet) As String

        btnMassApproval.Visible = False
        'ReturnControls.Add(btnMassApproval)

        Dim sbCollapseAll As New System.Text.StringBuilder
        sbCollapseAll.Append("<script>function CollapseAll(){")

        Dim sbExpandAll As New System.Text.StringBuilder
        sbExpandAll.Append("<script>function ExpandAll(){")

        Dim sb As New System.Text.StringBuilder

        sb.Append("<table width=100% border=0 align=center cellpadding=20 cellspacing=0 class=workflowTable style=""BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; BORDER-BOTTOM-STYLE: none""><tr align=right><td align=right style=""TEXT-ALIGN: right""></td></tr></table>")

        For Each dr As DataRow In ds.Tables(WFDASH.WORKFLOWS).Rows

            'Queues
            Dim dvQueues As New DataView(ds.Tables(WFDASH.QUEUES))
            dvQueues.RowFilter = "WorkflowID='" & dr("WorkflowId") & "'"
            Dim queueCount As Integer = 1

            Dim WorkflowDocumentCount As Integer = New DataView(ds.Tables(WFDASH.DOCUMENT_DETAIL), "WorkflowId='" & dr("WorkflowId") & "'", Nothing, DataViewRowState.CurrentRows).Count

            If dvQueues.Count > 0 And WorkflowDocumentCount > 0 Then

                sb.Append("<table width=100% border=0 align=center cellpadding=20 cellspacing=0 class=workflowTable>")

                'Workflow band
                Dim strWorkflowName As String = dr("WorkflowName")
                sb.Append("<THEAD>")
                sb.Append("<TR>")
                sb.Append("<TH colspan=7 id=redbar><i><b>" & strWorkflowName & "</b></i></TH>")
                sb.Append("</TR>")
                sb.Append("</THEAD>")

                'Column headers
                sb.Append("<THEAD>")
                sb.Append("<TR>")
                sb.Append("<TH width=3% id=bar>Queues</TH>")
                sb.Append("<TH width=2% id=bar>&nbsp;</TH>")
                sb.Append("<TH width=2% align=center id=bar><div align=center>Doc#</div></TH>")
                sb.Append("<TH id=bar>Document Info</TH>")
                sb.Append("<TH id=bar>&nbsp;</TH>")
                sb.Append("<TH id=bar>&nbsp;</TH>")
                sb.Append("<TH id=bar><p align=right>Posted/Action/Current Queue</p></TH>")
                sb.Append("</TR>")
                sb.Append("</THEAD>")

                For Each currentQueue As DataRowView In dvQueues

                    Dim dvDocuments As New DataView(ds.Tables(WFDASH.DOCUMENT_DETAIL))
                    dvDocuments.RowFilter = "QueueId='" & CStr(currentQueue("QueueId")).Replace("'", "''") & "' AND WorkflowId='" & dr("WorkflowId") & "'" '"QueueId='" & currentQueue("QueueId") & "' AND WorkflowId='" & dr("WorkflowId") & "'"

                    Dim strSectionId As String = "row" & System.Guid.NewGuid.ToString.Replace("-", "")
                    Dim strImageId As String = "img" & System.Guid.NewGuid.ToString.Replace("-", "")

                    Dim blnViewOnly As Boolean = False
                    Dim strQueueAccessClass As String
                    Dim strDocumentAccessClass As String

                    blnViewOnly = False
                    strQueueAccessClass = "class=queueName"
                    strDocumentAccessClass = ""

                    If dvDocuments.Count > 0 Then

                        Dim strQueueName As String = currentQueue("QueueName")
                        sb.Append("<TR class=queue>")
                        sb.Append("<TD colspan=6 " & strQueueAccessClass & "><div id=masminus><img style=""CURSOR: hand"" id=" & strImageId & " onclick=""toggle('" & strSectionId & "','" & strImageId & "');"" src=""Images/workflow_dashboard/wf_minus.gif"" id=toggle" & queueCount & " width=17 height=16></div>")
                        sb.Append("<strong>" & strQueueName & "</strong> &raquo; " & dvDocuments.Count & " documents</TD>")
                        sb.Append("<TD class=queueName>&nbsp;</TD>")
                        sb.Append("</TR>")

                        sbCollapseAll.Append("var imgsrc = document.getElementById('" & strImageId & "').src; if (imgsrc.substring(imgsrc.length - 38, imgsrc.length) == 'Images/workflow_dashboard/wf_minus.gif'){ document.getElementById('" & strImageId & "').click(); }")
                        sbExpandAll.Append("var imgsrc = document.getElementById('" & strImageId & "').src; if (imgsrc.substring(imgsrc.length - 37, imgsrc.length) == 'Images/workflow_dashboard/wf_plus.gif'){ document.getElementById('" & strImageId & "').click(); }")

                        sb.Append("<TBODY id=" & strSectionId & ">")

                        'Documents
                        Dim counter As Integer = 0
                        For Each currentDocument As DataRowView In dvDocuments

                            counter += 1

                            Dim strDocId As String = currentDocument("ImageId")
                            Dim strVersionId As String = currentDocument("VersionId")
                            Dim strSubmitter As String = currentDocument("Submitter")
                            Dim strDocType As String = New DataView(ds.Tables(WFDASH.DOCUMENTS), "DocumentId=" & currentDocument("DocumentID"), "", DataViewRowState.OriginalRows).Item(0)("DocumentName")

                            Dim strFilename As String = ""
                            Dim blnFilename As Boolean = False
                            If Not currentDocument("Filename") Is System.DBNull.Value Then
                                If Not CStr(currentDocument("Filename")).Trim = "" Then
                                    blnFilename = True
                                    strFilename = currentDocument("Filename")
                                End If
                            End If

                            Dim strTitle As String = ""
                            Dim blnTitle As Boolean = False
                            If Not currentDocument("Title") Is System.DBNull.Value Then
                                If Not CStr(currentDocument("Title")).Trim = "" Then
                                    blnTitle = True
                                    strTitle = currentDocument("Title")
                                End If
                            End If

                            'Document row
                            Dim rowId As String = "row" & System.Guid.NewGuid.ToString.Replace("-", "")
                            If counter Mod 2 = 0 Then
                                sb.Append("<TR style=""CURSOR: hand"" onclick=""LoadImage('" & strVersionId & "');"" class=odd onmouseover=""this.className = 'hlt';"" onmouseout=""this.className = 'odd';"">")
                            Else
                                sb.Append("<TR style=""CURSOR: hand"" onclick=""LoadImage('" & strVersionId & "');"" onmouseover=""this.className = 'hlt';"" onmouseout=""this.className = '';"">")
                            End If

                            sb.Append("<TD>&nbsp;</TD>")

                            Dim strUrgent As String
                            If currentDocument("Urgent") Then
                                strUrgent = "id=urgent"
                            Else
                                strUrgent = ""
                            End If

                            If counter Mod 2 = 0 Then
                                sb.Append("<TD " & strDocumentAccessClass & " align=center valign=top " & strUrgent & ">&nbsp;</TD>")
                            Else
                                sb.Append("<TD " & strDocumentAccessClass & " align=center valign=top " & strUrgent & ">&nbsp;</TD>")
                            End If

                            sb.Append("<TD " & strDocumentAccessClass & " align=center scope=row><div align=center><a href=#>" & strDocId & "</a></div></TD>")
                            sb.Append("<TD " & strDocumentAccessClass & " scope=row><p><strong>Submitter</strong>: <a href=#>" & strSubmitter & "</a><br>")
                            sb.Append("<strong>Doc. Type</strong>: <a href=#>" & strDocType & "</a><br>")
                            If blnFilename Then sb.Append("<strong>File Name</strong>: <a href=#>" & strFilename & "</a><br>")
                            If blnTitle Then sb.Append("<strong>Title</strong>: <a href=#>" & strTitle & "</a></p></TD>")

                            Dim dvAttributes As New DataView(ds.Tables(WFDASH.ATTRIBUTES))
                            dvAttributes.RowFilter = "VersionId=" & strVersionId

                            If dvAttributes.Count > 0 Then
                                Dim intdf As Integer = 88
                            End If

                            If dvAttributes.Count > 0 Then

                                'Determine number of rows needed
                                Dim intMaxRows As Integer = Math.Ceiling(dvAttributes.Count / 2)

                                'Build first column, odd numbers
                                sb.Append("<TD " & strDocumentAccessClass & " scope=row>")
                                For x As Integer = 0 To intMaxRows
                                    If x < dvAttributes.Count Then
                                        If dvAttributes(x)("AttributeDataType") = "Date" Then

                                            If IsDate(dvAttributes(x)("AttributeValue")) Then
                                                sb.Append(" " & dvAttributes(x)("AttributeName") & ": <a href=#>" & CDate(dvAttributes(x)("AttributeValue")).ToShortDateString & "</a><br>")
                                            Else
                                                sb.Append(" " & dvAttributes(x)("AttributeName") & ": <a href=#>" & dvAttributes(x)("AttributeValue") & "</a><br>")
                                            End If

                                        Else
                                            sb.Append(" " & dvAttributes(x)("AttributeName") & ": <a href=#>" & dvAttributes(x)("AttributeValue") & "</a><br>")
                                        End If
                                    Else
                                        sb.Append("&nbsp;")
                                    End If
                                    x += 1
                                Next
                                sb.Append("</TD>")

                                'Build first column, even numbers
                                sb.Append("<TD " & strDocumentAccessClass & " scope=row>")
                                For x As Integer = 1 To intMaxRows
                                    If x < dvAttributes.Count Then
                                        If dvAttributes(x)("AttributeDataType") = "Date" Then
                                            sb.Append(" " & dvAttributes(x)("AttributeName") & ": <a href=#>" & CDate(dvAttributes(x)("AttributeValue")).ToShortDateString & "</a><br>")
                                        Else
                                            sb.Append(" " & dvAttributes(x)("AttributeName") & ": <a href=#>" & dvAttributes(x)("AttributeValue") & "</a><br>")
                                        End If
                                    Else
                                        sb.Append("&nbsp;")
                                    End If
                                    x += 1
                                Next
                                sb.Append("</TD>")

                            Else

                                sb.Append("<TD " & strDocumentAccessClass & " scope=row>")
                                sb.Append("&nbsp;")
                                sb.Append("</TD>")
                                sb.Append("<TD " & strDocumentAccessClass & " scope=row>")
                                sb.Append("&nbsp;")
                                sb.Append("</TD>")

                            End If


                            'Build reviewers tooltip
                            Dim dvReviewers As New DataView(ds.Tables(WFDASH.REVIEWERS))
                            dvReviewers.RowFilter = "QueueId='" & currentDocument("CurrentQueueId") & "'"
                            Dim sbReviewers As New System.Text.StringBuilder

                            Dim sbEmails As New System.Text.StringBuilder
                            Dim strReviewers As String

                            If dvReviewers.Count > 0 Then

                                sbEmails.Append("mailto:")

                                sbReviewers.Append("E-Mail Reviewers:" & vbCr)
                                For Each currentReviewer As DataRowView In dvReviewers
                                    sbReviewers.Append(currentReviewer("LastName") & Space(1) & currentReviewer("FirstName") & " - " & currentReviewer("EmailAddress") & vbCr)
                                    sbEmails.Append(currentReviewer("EmailAddress") & ";")
                                Next

                                strReviewers = sbReviewers.ToString.Substring(0, sbReviewers.ToString.LastIndexOf(vbCr))

                            End If

                            sb.Append("<TD " & strDocumentAccessClass & "><div align=right>" & CDate(currentDocument("ActionDateTime")).ToShortDateString & Space(1) & CDate(currentDocument("ActionDateTime")).ToShortTimeString.ToLower.Replace(" ", "") & "<br>")

                            If dvReviewers.Count > 0 Then
                                sb.Append("<b>" & currentDocument("StatusDesc") & "</b><br>" & currentDocument("CurrentQueueName") & "&nbsp;<a href=""" & "" & sbEmails.ToString.Substring(0, sbEmails.ToString.LastIndexOf(";")) & """><img alt=""" & strReviewers & """ onclick=""event.cancelBubble=true;"" border=""0"" src=""images/wf_dash_email.gif""" & "</a>")
                            Else
                                sb.Append("<b>" & currentDocument("StatusDesc") & "</b><br>" & currentDocument("CurrentQueueName"))
                            End If

                            sb.Append("</div></TD>")
                            sb.Append("</TR>")

                        Next

                        queueCount += 1
                        sb.Append("</TBODY>")

                    End If

                Next

                sb.Append("<tfoot><TR><TH colspan=7><img src=../images/spacer.gif width=1 height=4></TH></TR></tfoot>")
                sb.Append("</table>")

            End If

        Next

        sbCollapseAll.Append("}</script>")
        sbExpandAll.Append("}</script>")

        Collapse.Text = sbCollapseAll.ToString
        Expand.Text = sbExpandAll.ToString

        Return sb.ToString

    End Function

    Private Sub ddlWorkflow_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlWorkflow.SelectedIndexChanged

        Dim conCompany As SqlClient.SqlConnection

        Try

            conCompany = Functions.BuildConnection(Me.mobjUser)

            Dim objWf As New Workflow(conCompany, False)

            Dim dtQueues As New DataTable
            dtQueues = objWf.WFHistoryParams(Me.mobjUser.ImagesUserId, "QUEUENAME", IIf(ddlWorkflow.SelectedItem.Text = "All", Nothing, ddlWorkflow.SelectedItem.Text))
            ddlQueue.DataSource = dtQueues
            ddlQueue.DataTextField = "QueueName"
            ddlQueue.DataBind()
            ddlQueue.Items.Insert(0, "All")
            ddlQueue.Items(0).Selected = True

        Catch ex As Exception

        Finally

            'ReturnControls.Add(ddlQueue)

            If conCompany.State <> ConnectionState.Closed Then
                conCompany.Close()
                conCompany.Dispose()
            End If

        End Try

    End Sub

    Private Sub btnView_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnView.Click

        Dim conCompany As New SqlClient.SqlConnection

        Dim beginDate As DateTime
        Dim endDate As DateTime

        Dim userDate As DateTime = Now.AddHours(Me.mobjUser.TimeDiffFromGMT)

        Try

            If rdoFixed.Checked Then

                Select Case ddlFixed.SelectedValue
                    Case 0 'today
                        beginDate = userDate
                        endDate = userDate
                    Case 1 'yesterday
                        beginDate = userDate.AddDays(-1)
                        endDate = userDate.AddDays(-1)
                    Case 2 'last 7 days
                        beginDate = userDate.AddDays(-7)
                        endDate = userDate
                    Case 3 ' last 30 days
                        beginDate = userDate.AddDays(-30)
                        endDate = userDate
                End Select

            Else

                'Verify dates
                Dim from As New DateTime(FromYear.SelectedValue, FromMonth.SelectedValue, FromDay.SelectedValue)
                If Not IsDate(from.Date) Then
                    lblError.Text = "Invalid beginning date."
                    lblError.Visible = True
                    'ReturnControls.Add(lblError)
                    Exit Sub
                End If

                Dim toDate As New DateTime(ToYear.SelectedValue, ToMonth.SelectedValue, ToDay.SelectedValue)
                If Not IsDate(toDate.Date) Then
                    lblError.Text = "Invalid ending date."
                    lblError.Visible = True
                    'ReturnControls.Add(lblError)
                    Exit Sub
                End If

                If from.Date > toDate.Date Then
                    lblError.Text = "Invalid date range."
                    lblError.Visible = True
                    'ReturnControls.Add(lblError)
                    Exit Sub
                End If

                beginDate = from ' CDate(FromMonth.SelectedValue & "/" & FromDay.SelectedValue & "/" & FromYear.SelectedValue)
                endDate = toDate 'CDate(ToMonth.SelectedValue & "/" & ToDay.SelectedValue & "/" & ToYear.SelectedValue)

                lblError.Text = ""
                lblError.Visible = False
                'ReturnControls.Add(lblError)

            End If

            conCompany = Functions.BuildConnection(Me.mobjUser)

            Dim dsDocuments As New DataSet
            Dim objWf As New Workflow(conCompany, False)

            dsDocuments = objWf.WFHistoryDetail(Me.mobjUser.ImagesUserId, Me.mobjUser.TimeDiffFromGMT, IIf(ddlWorkflow.SelectedItem.Text = "All", Nothing, ddlWorkflow.SelectedItem.Text), _
            IIf(ddlQueue.SelectedItem.Text = "All", Nothing, ddlQueue.SelectedItem.Text), IIf(ddlAction.SelectedItem.Text = "All", Nothing, ddlAction.SelectedItem.Text), beginDate.Date, endDate.Date)

            View.InnerHtml = "Workflow activity from " & beginDate.Date.ToShortDateString & " to " & endDate.Date.ToShortDateString
            'ReturnControls.Add(View)

            If dsDocuments.Tables(0).Rows.Count = 0 Then
                lblResults.Text = "No documents were found."
                toggle.Visible = False
                'ReturnControls.Add(toggle)
                Exit Sub
            Else
                toggle.Visible = True
                'ReturnControls.Add(toggle)
            End If


            lblResults.Text = PivotHistory(dsDocuments)

            lblError.Text = ""
            lblError.Visible = False

        Catch ex As Exception

            Dim sb As New System.Text.StringBuilder

            Dim type As String
            If rdoFixed.Checked Then
                sb.Append(" Fixed ")
                sb.Append("Type of search: " & ddlFilter.SelectedValue)
            Else
                sb.Append(" Range: ")
                sb.Append(" From: " & FromYear.SelectedValue & "/" & FromMonth.SelectedValue & "/" & FromDay.SelectedValue)
                sb.Append(" To: " & FromYear.SelectedValue & "/" & FromMonth.SelectedValue & "/" & FromDay.SelectedValue)
            End If

            Dim conMaster As SqlClient.SqlConnection
            conMaster = Functions.BuildMasterConnection
            Functions.LogException(mobjUser.UserId.ToString, mobjUser.AccountId.ToString, "", "Historical workflow error: " & sb.ToString & Space(1) & ex.Message, ex.StackTrace, Server.MachineName, "", Request.UrlReferrer.ToString, conMaster, True)

        Finally

            'ReturnControls.Add(lblError)

            If conCompany.State <> ConnectionState.Closed Then
                conCompany.Close()
                conCompany.Dispose()
            End If

        End Try

    End Sub

    Private Sub btnCurrentView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCurrentView.Click


        Dim conCompany As New SqlClient.SqlConnection

        Try

            conCompany = Functions.BuildConnection(Me.mobjUser)

            Dim dsDocuments As New DataSet
            Dim objWf As New Workflow(conCompany, False)

            dsDocuments = objWf.WFDashboard(Me.mobjUser.ImagesUserId, Me.mobjUser.TimeDiffFromGMT)

            If dsDocuments.Tables.Count > 6 Then blnMassApproval = True
            btnMassApproval.Visible = blnMassApproval

            View.InnerHtml = ""
            'ReturnControls.Add(View)

            If dsDocuments.Tables(0).Rows.Count = 0 Then
                lblResults.Text = "No documents were found."
                toggle.Visible = False
                'ReturnControls.Add(toggle)
                Exit Sub
            Else
                toggle.Visible = True
                'ReturnControls.Add(toggle)
            End If

            lblResults.Text = Pivot(dsDocuments, ddlSort.SelectedValue)

        Catch ex As Exception

        Finally

            'ReturnControls.Add(btnMassApproval)
            'ReturnControls.Add(lblResults)
            'ReturnControls.Add(ddlFilter)

            If conCompany.State <> ConnectionState.Closed Then
                conCompany.Close()
                conCompany.Dispose()
            End If

        End Try

    End Sub

    'Private Sub btnMassApproval_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMassApproval.Click

    '    Try

    '        Me.mconSqlCompany = Functions.BuildConnection(Me.mobjUser)

    '        Dim strMassApprovals As String = hiddenApprovals.Value

    '        If strMassApprovals.Trim.Length = 0 Then Exit Sub

    '        'Queue:5�Action:785�SendTo:0�Doc:1790,�Queue:31�Action:0�SendTo:0��Queue:32�Action:0�SendTo:0��Queue:30�Action:0�SendTo:0��Queue:34�Action:0�SendTo:0��

    '        'Split into queues
    '        Dim strQueues As String() = strMassApprovals.Split("�")

    '        Dim blnerrorChange As Boolean = False
    '        Dim blnerrorQueue As Boolean = False

    '        For Each queue As String In strQueues

    '            Dim subItems As String() = queue.Split("�")

    '            Dim strQueue As String = ""
    '            Dim strAction As String = ""
    '            Dim strSendTo As String = ""
    '            Dim strDocs As String = ""
    '            Dim statusId As String = ""

    '            If subItems.Length > 3 Then

    '                strDocs = subItems(3)


    '                For Each doc As String In strDocs.Split(",")

    '                    If doc.Trim.Length > 0 Then

    '                        If subItems.Length > 0 Then strQueue = subItems(0)
    '                        If subItems.Length > 1 Then strAction = subItems(1).Split("-")(0)
    '                        If subItems.Length > 1 Then statusId = subItems(1).Split("-")(1)
    '                        If subItems.Length > 2 Then strSendTo = subItems(2)

    '                        If strQueue <> "-1" And strAction <> "-1" And strSendTo <> "-1" Then

    '                            Dim versionId As Integer = doc.Split("-")(0)
    '                            Dim routeDetId As Integer = doc.Split("-")(1)

    '                            Dim wf As New Accucentric.docAssist.Data.Images.Workflow(Me.mconSqlCompany)
    '                            Dim intReturn As Integer = wf.WFReviewerInsert(versionId, routeDetId, Me.mobjUser.ImagesUserId, 0, statusId, strSendTo, "")

    '                            If intReturn = -1 Then
    '                                blnerrorChange = True
    '                            End If

    '                            If intReturn = -1 Then
    '                                blnerrorQueue = True
    '                            End If

    '                        End If

    '                    End If

    '                Next

    '            End If

    '        Next

    '        Dim sberror As New System.Text.StringBuilder

    '        If blnerrorChange Then sberror.Append("- One or more documents were not submitted because the workflow status has changed or been cancelled.")
    '        If blnerrorQueue Then sberror.Append("\n- One or more documents were not submitted because all rejecting queues were not common across the selected documents.")

    '        If blnerrorChange Or blnerrorQueue Then
    '            'ReturnScripts.Add("alert('The following error(s) occured: \n" & sberror.ToString & "');")
    '        End If

    '        'Reload screen

    '        'Load years
    '        Dim userTime As DateTime = Now.AddHours(Me.mobjUser.TimeDiffFromGMT)

    '        For X As Integer = userTime.Year - 5 To userTime.Year
    '            Dim lvi As New ListItem(X, X)
    '            ToYear.Items.Add(lvi)
    '            FromYear.Items.Add(lvi)
    '        Next

    '        Dim conCompany As New SqlClient.SqlConnection
    '        conCompany = Functions.BuildConnection(Me.mobjUser)

    '        Dim dsDocuments As New DataSet
    '        Dim objWf As New Workflow(conCompany, False)

    '        dsDocuments = objWf.WFDashboard(Me.mobjUser.ImagesUserId, Me.mobjUser.TimeDiffFromGMT)

    '        If dsDocuments.Tables.Count > 6 Then blnMassApproval = True
    '        btnMassApproval.Visible = blnMassApproval

    '        View.InnerHtml = ""
    '        'ReturnControls.Add(View)

    '        If dsDocuments.Tables(0).Rows.Count = 0 Then
    '            lblResults.Text = "No documents were found."
    '            toggle.Visible = False
    '            'ReturnControls.Add(toggle)
    '            Exit Sub
    '        Else
    '            toggle.Visible = True
    '            'ReturnControls.Add(toggle)
    '        End If

    '        lblResults.Text = Pivot(dsDocuments)

    '        Dim dtWorkflows As New DataTable
    '        dtWorkflows = objWf.WFHistoryParams(Me.mobjUser.ImagesUserId, "WORKFLOWNAME", Nothing)
    '        ddlWorkflow.DataSource = dtWorkflows
    '        ddlWorkflow.DataTextField = "WorkflowName"
    '        ddlWorkflow.DataBind()
    '        ddlWorkflow.Items.Insert(0, "All")
    '        ddlWorkflow.Items(0).Selected = True

    '        Dim dtQueues As New DataTable
    '        dtQueues = objWf.WFHistoryParams(Me.mobjUser.ImagesUserId, "QUEUENAME", Nothing)
    '        ddlQueue.DataSource = dtQueues
    '        ddlQueue.DataTextField = "QueueName"
    '        ddlQueue.DataBind()
    '        ddlQueue.Items.Insert(0, "All")
    '        ddlQueue.Items(0).Selected = True

    '        Dim dtAction As New DataTable
    '        dtAction = objWf.WFHistoryParams(Me.mobjUser.ImagesUserId, "ACTION", Nothing)
    '        ddlAction.DataSource = dtAction
    '        ddlAction.DataTextField = "StatusDesc"
    '        ddlAction.DataBind()
    '        ddlAction.Items.Insert(0, "All")
    '        ddlAction.Items(0).Selected = True

    '        Dim LoadTime As DateTime = Now.AddHours(Me.mobjUser.TimeDiffFromGMT)
    '        FromMonth.SelectedValue = IIf(LoadTime.Month.ToString.Length = 1, "0" & LoadTime.Month.ToString, LoadTime.Month)
    '        FromDay.SelectedValue = IIf(LoadTime.Day.ToString.Length = 1, "0" & LoadTime.Day.ToString, LoadTime.Day)
    '        FromYear.SelectedValue = LoadTime.Year

    '        ToMonth.SelectedValue = IIf(LoadTime.Month.ToString.Length = 1, "0" & LoadTime.Month.ToString, LoadTime.Month)
    '        ToDay.SelectedValue = IIf(LoadTime.Day.ToString.Length = 1, "0" & LoadTime.Day.ToString, LoadTime.Day)
    '        ToYear.SelectedValue = LoadTime.Year

    '    Catch ex As Exception

    '    Finally

    '        If Me.mconSqlCompany.State <> ConnectionState.Closed Then
    '            Me.mconSqlCompany.Close()
    '            Me.mconSqlCompany.Dispose()
    '        End If

    '    End Try

    'End Sub

    Private Sub btnReload_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnReload.Click

        'RegisterWaitDisabled(ddlWorkflow)
        'RegisterWaitDisabled(ddlQueue)

        Session("WorkflowMode") = ""

        btnCurrentView.Attributes("style") = "VISIBILITY: hidden"

        Try

            Me.mobjUser = BuildUserObject(Request.Cookies("docAssist"))

        Catch ex As Exception

            If ex.Message = "DualLogin" Then
                Response.Cookies("docAssist")("DualLogin") = True
                If Request.Url.Query.Trim.Length > 0 Then
                    Response.Redirect("Default.aspx" & Request.Url.Query)
                Else
                    Server.Transfer("Default.aspx")
                End If
            Else
                If ex.Message = "User is not logged on" Then
                    Response.Redirect("Default.aspx?Action=WFDash")
                End If
            End If

        End Try

        Dim conCompany As New SqlClient.SqlConnection

        btnMassApproval.Visible = False

        Try

            conCompany = Functions.BuildConnection(Me.mobjUser)

            Dim dsDocuments As New DataSet
            Dim objWf As New Workflow(conCompany, False)

            dsDocuments = objWf.WFDashboard(Me.mobjUser.ImagesUserId, Me.mobjUser.TimeDiffFromGMT)

            If dsDocuments.Tables.Count > 6 Then blnMassApproval = True
            btnMassApproval.Visible = blnMassApproval

            View.InnerHtml = ""
            'ReturnControls.Add(View)

            If dsDocuments.Tables(0).Rows.Count = 0 Then
                lblResults.Text = "No documents were found."
                toggle.Visible = False
                'ReturnControls.Add(toggle)
                Exit Sub
            Else
                toggle.Visible = True
                'ReturnControls.Add(toggle)
            End If

            lblResults.Text = Pivot(dsDocuments)


        Catch ex As Exception

            lblResults.Text = "An error has occured while loading the workflow dashboard. We have been notified of the error."

            Try
                Functions.LogException(mobjUser.UserId.ToString, mobjUser.AccountId.ToString, "", "Error loading workflow dashboard: " & ex.Message, ex.StackTrace, Server.MachineName, "", Request.UrlReferrer.ToString, Functions.BuildMasterConnection, True)
            Catch loggingEx As Exception

            End Try

        Finally

            'ReturnControls.Add(lblResults)
            'ReturnControls.Add(ddlFilter)

            If conCompany.State <> ConnectionState.Closed Then
                conCompany.Close()
                conCompany.Dispose()
            End If

        End Try

    End Sub

    Private Sub ddlSort_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlSort.SelectedIndexChanged

        'RegisterWaitDisabled(ddlWorkflow)
        'RegisterWaitDisabled(ddlQueue)

        Session("WorkflowMode") = ""

        btnCurrentView.Attributes("style") = "VISIBILITY: hidden"

        Response.Cookies.Remove("WFSort")
        Dim cSort As New HttpCookie("WFSort", ddlSort.SelectedValue.ToString)
        cSort.Expires = Now.AddDays(30)
        Response.Cookies.Add(cSort)
        'Response.Cookies("docAssist")("WFSort") = ddlSort.SelectedValue.ToString

        Try

            Me.mobjUser = BuildUserObject(Request.Cookies("docAssist"))

        Catch ex As Exception

            If ex.Message = "DualLogin" Then
                Response.Cookies("docAssist")("DualLogin") = True
                If Request.Url.Query.Trim.Length > 0 Then
                    Response.Redirect("Default.aspx" & Request.Url.Query)
                Else
                    Server.Transfer("Default.aspx")
                End If
            Else
                If ex.Message = "User is not logged on" Then
                    Response.Redirect("Default.aspx?Action=WFDash")
                End If
            End If

        End Try

        Dim conCompany As New SqlClient.SqlConnection

        btnMassApproval.Visible = False

        Try

            conCompany = Functions.BuildConnection(Me.mobjUser)

            Dim dsDocuments As New DataSet
            Dim objWf As New Workflow(conCompany, False)

            dsDocuments = objWf.WFDashboard(Me.mobjUser.ImagesUserId, Me.mobjUser.TimeDiffFromGMT)

            If dsDocuments.Tables.Count > 6 Then blnMassApproval = True
            btnMassApproval.Visible = blnMassApproval

            View.InnerHtml = ""
            'ReturnControls.Add(View)

            If dsDocuments.Tables(0).Rows.Count = 0 Then
                lblResults.Text = "No documents were found."
                toggle.Visible = False
                'ReturnControls.Add(toggle)
                Exit Sub
            Else
                toggle.Visible = True
                'ReturnControls.Add(toggle)
            End If

            lblResults.Text = Pivot(dsDocuments, ddlSort.SelectedValue)
            
        Catch ex As Exception

            lblResults.Text = "An error has occured while loading the workflow dashboard. We have been notified of the error."

            Try
                Functions.LogException(mobjUser.UserId.ToString, mobjUser.AccountId.ToString, "", "Error loading workflow dashboard: " & ex.Message, ex.StackTrace, Server.MachineName, "", Request.UrlReferrer.ToString, Functions.BuildMasterConnection, True)
            Catch loggingEx As Exception

            End Try

        Finally

            'ReturnControls.Add(lblResults)
            'ReturnControls.Add(ddlFilter)

            If conCompany.State <> ConnectionState.Closed Then
                conCompany.Close()
                conCompany.Dispose()
            End If

        End Try

    End Sub

End Class
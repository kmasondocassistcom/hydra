Imports ComponentArt.Web.UI
Imports Accucentric.docAssist
Imports Accucentric.docAssist.Data.Images

Partial Class MoveTree
    Inherits System.Web.UI.Page
    
    Dim mconSqlImage As SqlClient.SqlConnection
    Dim mobjUser As Accucentric.docAssist.Web.Security.User
    Dim contextMenus As New ArrayList

    Protected WithEvents litEdit As System.Web.UI.WebControls.Literal

    Private Const FAVORITE_SPLIT As String = "�"

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Response.Expires = 0
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        Try

            Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
            Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))
            Me.mobjUser.AccountId = guidAccountId
            Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)

            BuildTree()

        Catch ex As Exception

            Response.Write(ex.Message)

        Finally
            If Me.mconSqlImage.State <> ConnectionState.Closed Then
                Me.mconSqlImage.Close()
                Me.mconSqlImage.Dispose()
            End If
        End Try

    End Sub

    Private Sub BuildTree()

        If Not Page.IsPostBack Then

            Dim objFolders As New Folders(Me.mconSqlImage, False)
            Dim ds As New DataSet
            ds = objFolders.FoldersGet(Me.mobjUser.ImagesUserId)

            'Dim CatFolderNode As New RadTreeNode("Cabinets & Folders", "CabsFolders")

            Dim CatFolderNode As New TreeViewNode
            CatFolderNode.Text = "Cabinets & Folders"
            CatFolderNode.ID = "CabsFolders"
            CatFolderNode.AutoPostBackOnSelect = True
            CatFolderNode.Expanded = True
            CatFolderNode.ImageUrl = IMAGE_CABINETS_FOLDERS

            If Me.mobjUser.SysAdmin Then
                ''Build share context menus
                'Dim cm As New RadTreeViewContextMenu.ContextMenu
                'cm.Name = "NewShareMenu"
                'cm.Width = 150
                'contextMenus.Add(cm)

                ''New share
                'Dim mnuNewShare As New RadTreeViewContextMenu.ContextMenuItem(NEW_SHARE)
                'mnuNewShare.Image = IMAGE_CABINET_ADD
                'mnuNewShare.PostBack = True
                'cm.Items.Add(mnuNewShare)

                'CatFolderNode.ContextMenuName = "NewShareMenu"

            End If

            treeSearch.Nodes.Add(CatFolderNode)

            For Each drShare As DataRow In ds.Tables(TreeTables.SHARES).Rows

                'Build share
                Dim Share As New TreeViewNode
                Share.Text = drShare("ShareName")
                Share.ID = "S" & drShare("ShareId")
                'Dim Share As New RadTreeNode(drShare("ShareName"), "S" & drShare("ShareId"))
                Share.ImageUrl = IMAGE_CABINET
                Share.AutoPostBackOnSelect = True
                Share.AutoPostBackOnRename = False

                If drShare("HasChildFolder") = True Then
                    Share.ContentCallbackUrl = "XmlGenerator.aspx?Ref=Popup&NodeId=" & Share.ID
                End If

                If drShare("ExpandShare") = True Then
                    Share.Expanded = True
                End If

                'Share.PostBack = True
                'Share.AutoPostBackOnSelect = True
                'Share.ContextMenuName = strMenuName

                'If drShare("CanRenameShare") = True Then
                '    Share.EditEnabled = False
                'Else
                '    Share.EditEnabled = False
                'End If

                'Share.Checkable = False

                'Add share to tree
                'radTree.Nodes.Add(Share)
                CatFolderNode.Nodes.Add(Share)
                'colNodes.Add(Share)

            Next

            'Build share children folders
            For Each drChild As DataRow In ds.Tables(TreeTables.FOLDERS).Rows

                Dim intParentFolderId As Integer = drChild("ParentFolderId")
                Dim intFolderLevel As Integer = drChild("FolderLevel")

                'Find parent
                Dim ParentNode As New TreeViewNode
                If intFolderLevel = 1 Then
                    ParentNode = treeSearch.FindNodeById("S" & intParentFolderId.ToString)

                    'ParentNode = CatFolderNode.Nodes("S" & intParentFolderId.ToString)
                    'ParentNode = radTree.FindNodeByValue("S" & intParentFolderId.ToString)
                Else
                    'ParentNode = NodeCollection.FindNodeByValue("W" & intParentFolderId.ToString)
                    ParentNode = treeSearch.FindNodeById("W" & intParentFolderId.ToString)
                End If

                'Add child to parent
                If Not IsNothing(ParentNode) Then

                    'Dim strFolderMenuName As String = System.Guid.NewGuid.ToString.Replace("-", "")

                    ''Folder menus
                    'Dim cmFolder As New RadTreeViewContextMenu.ContextMenu
                    'cmFolder.Name = strFolderMenuName
                    'cmFolder.Width = 150
                    'contextMenus.Add(cmFolder)

                    ''New folder
                    'If drChild("CanAddFolder") = True Then
                    '    Dim mnuNewFolder As New RadTreeViewContextMenu.ContextMenuItem(NEW_FOLDER)
                    '    mnuNewFolder.Image = IMAGE_FOLDER_ADD
                    '    mnuNewFolder.PostBack = True
                    '    cmFolder.Items.Add(mnuNewFolder)
                    'End If

                    ''Rename folder
                    'If drChild("CanRenameFolder") = True Then
                    '    Dim mnuRenameFolder As New RadTreeViewContextMenu.ContextMenuItem(RENAME_FOLDER)
                    '    mnuRenameFolder.Image = IMAGE_RENAME
                    '    mnuRenameFolder.PostBack = False
                    '    cmFolder.Items.Add(mnuRenameFolder)
                    'End If

                    ''Remove folder
                    'If drChild("CanDeleteFolder") = True Then
                    '    Dim mnuRemoveFolder As New RadTreeViewContextMenu.ContextMenuItem(REMOVE_FOLDER)
                    '    mnuRemoveFolder.Image = IMAGE_REMOVE
                    '    mnuRemoveFolder.PostBack = True
                    '    cmFolder.Items.Add(mnuRemoveFolder)
                    'End If

                    ''Add files
                    'If drChild("ReadOnly") = False Then
                    '    Dim mnuAddFiles As New RadTreeViewContextMenu.ContextMenuItem(ADD_FILES)
                    '    mnuAddFiles.Image = IMAGE_ADD_FILES
                    '    mnuAddFiles.PostBack = True
                    '    cmFolder.Items.Add(mnuAddFiles)
                    'End If

                    ''Security
                    'If drChild("CanChangeSecurity") = True Then
                    '    Dim mnuSecurityFolder As New RadTreeViewContextMenu.ContextMenuItem(SECURITY)
                    '    mnuSecurityFolder.Image = IMAGE_SECURITY
                    '    mnuSecurityFolder.PostBack = True
                    '    cmFolder.Items.Add(mnuSecurityFolder)
                    'End If

                    ''Add to favorites
                    'If drChild("ReadOnly") = False Then
                    '    Dim mnuAddToFavorites As New RadTreeViewContextMenu.ContextMenuItem(ADD_TO_FAVORITES)
                    '    mnuAddToFavorites.Image = IMAGE_FAVORITES_ADD
                    '    mnuAddToFavorites.PostBack = True
                    '    cmFolder.Items.Add(mnuAddToFavorites)
                    'End If

                    'Dim ChildNode As New RadTreeNode(drChild("FolderName"), "W" & drChild("FolderId"))

                    Dim ChildNode As New TreeViewNode
                    ChildNode.Text = drChild("FolderName")
                    ChildNode.ID = "W" & drChild("FolderId")
                    ChildNode.AutoPostBackOnSelect = True
                    ChildNode.ImageUrl = IMAGE_FOLDER
                    ChildNode.AutoPostBackOnRename = False
                    ChildNode.EditingEnabled = False
                    ChildNode.ExpandedImageUrl = IMAGE_FOLDER_OPEN
                    'ChildNode.Expanded = True
                    'ChildNode.ContextMenuName = strFolderMenuName

                    'If drChild("CanRenameFolder") = True Then
                    '    ChildNode.EditEnabled = False
                    'Else
                    '    ChildNode.EditEnabled = False
                    'End If

                    'ChildNode.PostBack = True
                    'ChildNode.Checkable = False

                    If drChild("HasChildFolder") = True Then
                        'ChildNode.ExpandMode = ExpandMode.ServerSideCallBack
                        ChildNode.ContentCallbackUrl = "XmlGenerator.aspx?Ref=Popup&NodeId=" & ChildNode.ID
                        ChildNode.Expanded = False
                    End If

                    If drChild("ExpandFolder") = True Then
                        ChildNode.Expanded = True
                    End If

                    If drChild("CanViewFolderOnly") = True Then
                        'ChildNode.Expanded = True
                        ChildNode.AutoPostBackOnSelect = False
                        ChildNode.ClientSideCommand = "alert('Access denied.');stopProgressDisplay();"
                    End If

                    'NodeCollection.Add(ChildNode)
                    'colNodes.Add(ChildNode)

                    ParentNode.Nodes.Add(ChildNode)

                End If

            Next

            '    NodeCollection = Nothing

            '    radTree.ContextMenus = contextMenus
            '    radTree.AllowNodeEditing = True

            '    DisableToolbar()

            'Else

            '    contextMenus = radTree.ContextMenus

            'treeSearch_NodeCheckChanged(Nothing, Nothing)

        End If

        'Try
        '    If Not Page.IsPostBack Then radTree_NodeCheck(Nothing, Nothing)
        'Catch ex As Exception

        'End Try

    End Sub

    Private Sub btnMove_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnMove.Click

        Dim conSql As New SqlClient.SqlConnection

        Try

            conSql = Functions.BuildConnection(Me.mobjUser)

            Dim dt As New DataTable
            Dim objFolders As New Accucentric.docAssist.Data.Images.Folders(conSql, True)

            dt = objFolders.FolderCabinetPermissions(Me.mobjUser.ImagesUserId, FolderId.Value.Replace("W", ""))
            If dt.Rows.Count = 0 Then Exit Sub

            If dt.Rows(0)("CanAddFilesToFolder") = "1" Then
                System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType(), "finish", "window.returnValue='" & FolderId.Value.Replace("W", "") & "';window.close();", True)
            Else
                'No access
                System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType(), "finish", "alert('You do not have access to this folder. Please select another folder. If you need access please contact your administrator.');", True)
            End If

        Catch ex As Exception

        Finally

            If conSql.State <> ConnectionState.Closed Then
                conSql.Close()
                conSql.Dispose()
            End If

        End Try

    End Sub

End Class
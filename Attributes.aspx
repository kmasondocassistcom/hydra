<%@ Register TagPrefix="uc1" TagName="AttributeNavigation" Src="AttributeNavigation.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="Attributes.aspx.vb" Inherits="docAssistWeb.Attributes"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
  <HEAD>
		<TITLE></TITLE>
		<META content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<META content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<META content="JavaScript" name="vs_defaultClientScript">
		<META content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
  </HEAD>
	<BODY onresize="setAttributesHeight();" onbeforeunload="CheckNotDirty();" onload="SetupServices();setAttributesHeight();"
		scroll="yes">
		<FORM id="Form1" onsubmit="javascript:hiddenSubmit.value=1;" method="post" runat="server">
			<DIV id="svcOCR" style="BEHAVIOR: url(webservice.htc)" onresult="OCRResult()"></DIV>
			<TABLE class="TransparentNoPadding" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TR>
					<TD>
						<table id="Table1" cellSpacing="0" cellPadding="0" width="100" border="0" runat="server">
							<tr>
								<td><IMG height="10" src="dummy.gif" width="1"></td>
							</tr>
							<tr vAlign="bottom">
								<td vAlign="bottom" noWrap><IMG height="1" src="dummy.gif" width="10"><asp:label id="Label8" runat="server" Font-Names="Trebuchet MS" Height="3px" Font-Size="8pt">Doc. No.</asp:label><IMG height="1" src="dummy.gif" width="28"></td>
								<td vAlign="top" noWrap width="100%" height="15"><asp:label id="lblImageId" runat="server" Font-Names="Trebuchet MS" Font-Size="8pt"></asp:label></td>
							</tr>
						</table>
						<table id="tblFolder" cellSpacing="0" cellPadding="0" width="100%" border="0" runat="server">
							<tr vAlign="baseline">
								<td vAlign="middle" noWrap><IMG height="1" src="dummy.gif" width="10"><asp:label id="Label7" runat="server" Font-Names="Trebuchet MS" Height="3px" Font-Size="8pt">Folder:</asp:label><IMG height="1" src="dummy.gif" width="34"></td>
								<td vAlign="bottom" noWrap height="15"><asp:textbox id="txtFolder" runat="server" Font-Names="Trebuchet MS" Height="22px" Font-Size="8pt"
										Width="100px" Enabled="True" ReadOnly="False" BorderStyle="None"></asp:textbox></td>
								<td vAlign="bottom" width="100%"><IMG height="1" src="spacer.gif" width="1"> <IMG id="ChooseFolder" style="CURSOR: hand" onclick="FolderLookup()" height="19" alt="Choose Folder"
										src="Images/addToFolder_btn.gif" runat="server"><IMG height="1" src="spacer.gif" width="5"><IMG id="ClearFolder" style="CURSOR: hand" onclick="document.getElementById('FolderId').value = '';document.getElementById('txtFolder').value = '';ActivateSave();"
										height="20" alt="Clear Folder" src="Images/clear.gif" runat="server">
								</td>
							</tr>
						</table>
						<table id="tblTextBoxes" cellSpacing="0" cellPadding="0" border="0" runat="server">
							<tr id="titleRow" runat="server">
								<td vAlign="middle" noWrap><IMG height="1" src="dummy.gif" width="10"><asp:label id="Label4" runat="server" Font-Names="Trebuchet MS" Height="3px" Font-Size="8pt">Title:</asp:label><IMG height="1" src="dummy.gif" width="43"></td>
								<td noWrap width="100%"><asp:textbox id="txtTitle" runat="server" Font-Names="Trebuchet MS" Height="22px" Font-Size="8pt"
										Width="160px" Visible="False"></asp:textbox><asp:label id="lblTitle" runat="server" Font-Names="Trebuchet MS" Font-Size="8pt" Width="160px"
										Visible="False"></asp:label></td>
							</tr>
							<tr id="descRow" runat="server">
								<td id="TD1" vAlign="middle" noWrap runat="server"><IMG height="1" src="dummy.gif" width="10"><asp:label id="Label5" runat="server" Font-Names="Trebuchet MS" Height="3px" Font-Size="8pt">Description:</asp:label></td>
								<td noWrap width="100%"><asp:textbox id="txtComments" runat="server" Font-Names="Trebuchet MS" Height="22px" Font-Size="8pt"
										Width="160px" Visible="False"></asp:textbox><asp:label id="lblComments" runat="server" Font-Names="Trebuchet MS" Font-Size="8pt" Width="160px"
										Visible="False"></asp:label></td>
							</tr>
							<tr id="tagRow" runat="server">
								<td noWrap><IMG height="1" src="dummy.gif" width="10"><asp:label id="Label6" runat="server" Font-Names="Trebuchet MS" Height="3px" Font-Size="8pt">Tags:</asp:label><IMG height="1" src="dummy.gif" width="37"></td>
								<td noWrap width="100%"><asp:textbox id="txtTags" runat="server" Font-Names="Trebuchet MS" Height="22px" Font-Size="8pt"
										Width="160px" Visible="False"></asp:textbox><asp:label id="lblTags" runat="server" Font-Names="Trebuchet MS" Font-Size="8pt" Width="160px"
										Visible="False"></asp:label></td>
							</tr>
						</table>
						<TABLE class="Transparent" cellSpacing="2" cellPadding="2" width="100%" border="0">
							<TR>
								<TD noWrap><IMG height="1" src="dummy.gif" width="7"><asp:label id="Label3" runat="server" Font-Names="Trebuchet MS" Height="3px" Font-Size="8pt">Document:</asp:label>&nbsp;<IMG height="1" src="dummy.gif" width="6">
									<ASP:DROPDOWNLIST id="ddlDocuments" runat="server" Font-Names="Trebuchet MS" Height="4px" Font-Size="8pt"
										Width="160px" Visible="False" AutoPostBack="True"></ASP:DROPDOWNLIST><ASP:TEXTBOX id="txtDocumentName" runat="server" Width="160px" ReadOnly="True" BorderStyle="Solid"
										Visible="False" BorderWidth="1px" BorderColor="Silver" CssClass="LabelHighlight" font-size="8pt"></ASP:TEXTBOX><ASP:LABEL id="lblDocumentId" runat="server" Visible="False"></ASP:LABEL></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR id="ErrorRow" runat="server">
					<td id="TD2" width="100%" runat="server">
						<table cellPadding="0" width="100%" border="0">
							<tr width="100%">
								<td><IMG height="1" src="dummy.gif" width="10"></td>
								<TD width="100%"><IMG height="8" src="images/spacer.gif" width="1"><ASP:LABEL id="lblAttributeError" runat="server" Font-Names="Trebuchet MS" Font-Size="8pt"
										Visible="False" ForeColor="Red"></ASP:LABEL></TD>
							</tr>
						</table>
					</td>
				</TR>
				<TR>
					<TD id="TD3" runat="server"><IMG height="1" src="dummy.gif" width="7">
						<ASP:IMAGEBUTTON id="lnkAdd" runat="server" Height="16px" Width="16px" AlternateText="Add Attribute"
							ToolTip="Add Attribute" ImageUrl="Images/bot23.gif"></ASP:IMAGEBUTTON><asp:imagebutton id="btnRemove" runat="server" ToolTip="Remove Attribute" ImageUrl="Images/circle_minus.gif"></asp:imagebutton><asp:imagebutton id="btnCapture2" runat="server" ToolTip="Capture Attributes" ImageUrl="Images/capture.gif"></asp:imagebutton>&nbsp;
						<A id="btnOcr" onclick="svcOCR.onServiceAvailable=OCR();" runat="server"><IMG id="imgOcr" style="CURSOR: hand" alt="smartScan" src="Images/ocr.gif" border="0"></A>&nbsp;
						<img id="imgDistribution" src="Images/btn.distribution.gif" border="0" style="DISPLAY: none; CURSOR: hand"
							alt="Show Distribution" runat="server"><a class=PageContent href="#" onclick="relatedDocs()">Related Documents</a>
      
						<table cellSpacing="0" cellPadding="0" width="50" border="0">
							<tr width="100%">
								<td><IMG height="1" src="dummy.gif" width="7"></td>
								<TD width="100%">
									<ASP:DATAGRID id="dgAttributes" runat="server" Font-Size="8pt" CssClass="PageContent" AutoGenerateColumns="False"
										Width="230px">
										<SelectedItemStyle BorderStyle="Double"></SelectedItemStyle>
										<ItemStyle Font-Size="8pt"></ItemStyle>
										<HeaderStyle Font-Bold="True" HorizontalAlign="Center" ForeColor="White" VerticalAlign="Bottom"
											BackColor="DarkGray"></HeaderStyle>
										<Columns>
											<asp:TemplateColumn Visible="False">
												<ItemStyle BackColor="White"></ItemStyle>
											</asp:TemplateColumn>
											<asp:TemplateColumn Visible="False" HeaderText="AttributeId"></asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Attribute">
												<ItemStyle VerticalAlign="Top"  HorizontalAlign=Center></ItemStyle>
												<ItemTemplate>
													<ASP:DROPDOWNLIST id="ddlAttribute" runat="server" AutoPostBack="True"></ASP:DROPDOWNLIST>
													<ASP:LABEL id=lblAttributeName runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AttributeName") %>' Visible=False>
													</ASP:LABEL>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Value">
												<ItemStyle VerticalAlign="Top"></ItemStyle>
												<ItemTemplate>
													<ASP:TEXTBOX id=txtAttributeValue Width="110" runat="server" BorderStyle="Groove" cssclass="inputBox" Text='<%# DataBinder.Eval(Container, "DataItem.AttributeValue") %>'>
													</ASP:TEXTBOX>
													<ASP:LABEL id=lblAttributeId runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.AttributeId") %>'>
													</ASP:LABEL>
													<ASP:LABEL id="lblAccessLevel" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.AccessLevel") %>'>
													</ASP:LABEL>
													<ASP:LABEL id=lblImageAttributeId runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.ImageAttributeId") %>'>
													</ASP:LABEL>
													<ASP:LABEL id=lblAttributeValue runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.AttributeValue") %>'>
													</ASP:LABEL>
													<span style="CURSOR: hand">
														<asp:Image id="imgBrowse" runat="server" Height="15px" Width="15px" Visible="False" ImageUrl="Images/listbox.btn.gif"></asp:Image></span>
													<ASP:LABEL id=lblListViewItem runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.ValueDescription") %>'>
													</ASP:LABEL>
													<ASP:LABEL id="lblControlAttributeId" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.ControlTotalAttributeID") %>'>
													</ASP:LABEL>
													<asp:Label id="lblRowError" runat="server" Font-Names="Verdana" Visible="False" ForeColor="Red"></asp:Label>
													<ASP:DROPDOWNLIST id="ddlAttributeValue" runat="server" Visible="False"></ASP:DROPDOWNLIST>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn Visible="False" HeaderText="AttributeProperties">
												<ItemStyle VerticalAlign="Top"></ItemStyle>
												<ItemTemplate>
													<ASP:LABEL id=lblAttributeDataType runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.AttributeDataType") %>'>
													</ASP:LABEL>
													<ASP:LABEL id="lblListValidate" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.ListValidate") %>'>
													</ASP:LABEL>
													<ASP:LABEL id=lblDataFormat runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.DataFormat") %>'>
													</ASP:LABEL>
													<ASP:LABEL id=lblLength runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.Length") %>'>
													</ASP:LABEL>
													<ASP:LABEL id=lblListViewStyle runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.ListViewStyle") %>'>
													</ASP:LABEL>
													<ASP:LABEL id=lblRequired runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.Required") %>'>
													</ASP:LABEL>
												</ItemTemplate>
											</asp:TemplateColumn>
										</Columns>
									</ASP:DATAGRID>
								</TD>
							</tr>
						</table>
					</TD>
				</TR>
			</TABLE>
			<div style="DISPLAY: none"><input id="ReadOnlyUser" type="hidden" value="0" runat="server" NAME="ReadOnlyUser">
				<ASP:IMAGEBUTTON id="btnSave" runat="server" Height="0px" Width="0px"></ASP:IMAGEBUTTON><asp:textbox id="txtMapName" runat="server" Height="0px" Width="0px"></asp:textbox><asp:textbox id="txtHeader" runat="server" Height="0px" Width="0px"></asp:textbox><asp:textbox id="txtDetail" runat="server" Height="0px" Width="0px"></asp:textbox><input id="WorkflowId" style="VISIBILITY: hidden" type="text" name="WorkflowId" runat="server"><input id="QueueId" style="VISIBILITY: hidden" type="text" name="QueueId" runat="server"><input id="WorkflowNote" style="VISIBILITY: hidden" type="text" name="WorkflowNote" runat="server">
				<asp:textbox id="PageNo" runat="server" Height="0px" Width="0px"></asp:textbox><input id="Urgent" style="VISIBILITY: hidden" type="text" value="0" name="Urgent" runat="server"><input id="Mode" style="VISIBILITY: hidden" type="text" value="0" name="Mode" runat="server"><input id="RouteId" style="VISIBILITY: hidden" type="text" value="0" name="RouteId" runat="server">
				<input id="FolderId" type="hidden" name="FolderId" runat="server">
<OBJECT id=objCapture style="VISIBILITY: hidden" height=0 width=0 
classid=clsid:D0719215-417C-4086-B157-A0CA6115FA55 VIEWASTEXT>
	<PARAM NAME="_ExtentX" VALUE="26">
	<PARAM NAME="_ExtentY" VALUE="26">
	</OBJECT>
				<asp:textbox id="Annotation" runat="server" Height="0px" Width="0px"></asp:textbox><INPUT id="lblVersionId" type="hidden" name="lblVersionId" runat="server"><INPUT id="lblPageNo" type="hidden" name="lblPageNo" runat="server"><INPUT id="lblFileNameResult" type="hidden" name="lblFileNameResult" runat="server">
				<ASP:TEXTBOX id="txtOldAttributeId" runat="server" Visible="False"></ASP:TEXTBOX><ASP:TEXTBOX id="txtOldAttributeValue" runat="server" VISIBLE="False"></ASP:TEXTBOX><INPUT id="hiddenTripCount" type="hidden" name="hiddenTripCount" runat="server"><INPUT id="ActiveRow" type="hidden" name="ActiveRow" runat="server">
				<asp:imagebutton id="btnShade" runat="server" Height="0px" Width="0px"></asp:imagebutton><asp:imagebutton id="btnClear" runat="server" Height="0px" Width="0px"></asp:imagebutton><INPUT id="IntegrationValues" type="hidden" name="IntegrationValues" runat="server">
				<asp:imagebutton id="btnIntegration" runat="server" Height="0px" Width="0px"></asp:imagebutton><INPUT id="ClearAttributes" type="hidden" name="ClearAttributes" runat="server"><INPUT id="ApplicationName" type="hidden" name="ApplicationName" runat="server"><INPUT id="confirmNav" type="hidden" value="0"><INPUT id="moveNext" type="hidden"><INPUT id="hiddenSubmit" type="hidden"><INPUT id="hiddenDirty" style="VISIBILITY: hidden" type="text" name="hiddenDirty" runat="server"><INPUT id="showCalendar" type="hidden" value="0" name="showCalendar" runat="server"><INPUT id="selectedIndex" type="hidden" name="selectedIndex" runat="server">
				<INPUT id="hostname" type="hidden" name="hostname" runat="server"><INPUT id="FolderMap" type="hidden" value="0" name="hostname" runat="server"><INPUT id="FormID" type="hidden" value="0" name="hostname" runat="server"><INPUT id="DocumentID" type="hidden" value="0" name="hostname" runat="server"><INPUT id="ControlValue" type="hidden" runat="server" NAME="ControlValue">
				<asp:ImageButton id="btnAddControlItem" runat="server" Height="0px" Width="0px"></asp:ImageButton><INPUT id="controlName" type="hidden" runat="server"><input type="hidden" id="CaptureType" runat="server"><INPUT id="ObjectCheck" type="hidden" runat="server"></div>
		</FORM>
		<script language="javascript" src="scripts/tooltip_wrap.js"></script>
		<SCRIPT language="javascript">
				
					var bolLoading = 0;
					var dirtyChecked = 0;
					var ignoreDirty = false;
					var dataEntered = false;
					var glEnabled = true;
					var controlTextbox;

					DisableSave();
					
					setTimeout('setAttributesHeight();',900);
					
					function setAttributesHeight()
					{
						/*
						var divAttributes = document.getElementById('divAttributes');
						if (divAttributes != undefined)
						{
							var newHeight = document.body.clientHeight - 200;
							if (newHeight <= 0 ) {newHeight = 150;}
							divAttributes.style.height = newHeight;
							divAttributes.style.width = 240;
						}
						*/
					}
					
					function setDataEntered()
					{
						dataEntered = true;
					}
					
					function trackAnnotations(){
						top.trackAnnotations();
					}
					
					function postback()
					{

						try
						{
							var objCapture = new ActiveXObject("docAssistVersion.Version");
							var ObjectCheck = document.getElementById('ObjectCheck');
							if (ObjectCheck.value != objCapture.Version()){
								//new version available
								window.open('Install.aspx');
								return false;
							}
							
						}
						catch(ex)
						{
							//nothing installed
							window.open('Install.aspx');
							return false;
						}

						try
						{
							var cap = new ActiveXObject("docAssistCapture.Web");
							var header = document.all.txtHeader.value;
							var detail = document.all.txtDetail.value;
							var captured = cap.MainCall(header, detail, true);
						}
						catch(ex)
						{
							alert('An error has occured while capturing data. Please try again.');
						}
					
						if (captured.substring(0,6) == 'ERROR|')
						{
						
							if (captured.replace('ERROR|','') == 'Unable to locate window')
							{
								var appName = document.getElementById('ApplicationName').value;
								alert('Unable to capture attributes. Please make sure ' + appName + ' is open.');
								return true;
							}
							else
							{
								alert(captured.replace('ERROR|',''));
								event.cancelBubble = true;
								event.returnValue = false;
							}
						}
						else
						{
							document.getElementById('IntegrationValues').value = '';
							document.getElementById('IntegrationValues').value = captured;
							document.getElementById('btnIntegration').click();
						}
					}
					
					function saveCookie(name,value,days) 
					{ 
						if (!days) expires = "" 
						else{ 
							var date = new Date(); 
							date.setTime(date.getTime()+ 
							(days*24*60*60*1000)) 
							var expires = "; expires="+ 
							date.toGMTString() 
							} 
						document.cookie=name+"="+ 
						escape(value)+expires+"; path=/" 
					} 
					
					function clearCookie(name) 
					{
						if (!1) expires = "" 
						else{ 
							var date = new Date(); 
							date.setTime(date.getTime()+ 
							(1*24*60*60*1000)) 
							var expires = "; expires="+ 
							date.toGMTString() 
							} 
						document.cookie=name+"="+ 
						escape("")+expires+"; path=/" 
					}
					
					function readCookie(name) 
					{ 
						name+="=" 
						var ca = document.cookie.split(';') 
						for(var i in ca) 
							{ 
							var c = ca[i]; 
							while (c.charAt(0)==' ') 
							c = c.substring(1,c.length) 
							if (c.indexOf(name) == 0) 
							return unescape(c.substring(name.length,c.length)) 
							} 
						return '' 
					} 				

					function CheckNotDirty()
					{
						var dirty = document.all.hiddenDirty.value;
						if((dirty == "1") && (document.getElementById('ReadOnlyUser').value == 0))
						{
							event.returnValue = 'All your changes will be lost. You must save them before leaving the page.';
						}
					}

					function hourglass()
					{
						document.body.style.cursor = "wait";		
					}
					
					function findPosX(obj)
					{
						var curleft = 0;
						if (obj.offsetParent)
						{
							while (obj.offsetParent)
							{
								curleft += obj.offsetLeft
								obj = obj.offsetParent;
							}
						}
						else if (obj.x)
							curleft += obj.x;
						return curleft;
					}

					function findPosY(obj)
					{
						var curtop = 0;
						if (obj.offsetParent)
						{
							while (obj.offsetParent)
							{
								curtop += obj.offsetTop
								obj = obj.offsetParent;
							}
						}
						else if (obj.y)
							curtop += obj.y;
						return curtop;
					}
					
					function OCR()
					{
						if (!bolLoading)
						{
							var coordinates = parent.document.all.Coordinates;
							var versionId = document.all.lblVersionId;
							var pageNo = top.frames["frameImage"].document.all.lblPageNo.value;
							var lblFileName = document.all.lblFileNameResult.value;
							
							if(coordinates.value != "" && document.all.ActiveRow.value >= 0)
							{
								bolLoading = 1;
								hourglass()
								
								if (versionId.value > 0)
									svcOCR.Ocr.callService("ReadOCRFromImage", versionId.value, pageNo, coordinates.value);
								else
								{
									svcOCR.Ocr.callService("ReadOCRFromFile", lblFileName, coordinates.value);
								}
								top.frames["frameImage"].startLoading('Performing smartScan...');
							}
							else
							{
								if(coordinates.value == "")
									alert("No area has been selected. Use the pointer to select an area and try again.");
								else if (document.all.txtAttributeValue == undefined)
									alert("You need to be in edit mode");
							}
						}
					}
					
					function Save()
					{
						var btnSave = document.all.btnSave;
						btnSave.click();
					}
					
					function SetupServices()
					{
						hostname = document.all.hostname.value;
						svcOCR.useService(hostname + "/docAssistImages.asmx?wsdl", "Ocr");
					}
					
					function ShowCalendar()
					{
						var selectedItem = Math.abs(document.all.selectedIndex.value) + 2;
						strFieldName = "dgAttributes__ctl" + selectedItem + "_txtAttributeValue";
						var txtAttributeValue = document.getElementById(strFieldName);
						x = findPosX(txtAttributeValue); //+ txtAttributeValue.style.offsetWidth;
						y = findPosY(txtAttributeValue);
						var calAttribute = document.all.calAttribute;
						calAttribute.style.top = y + 20;
						calAttribute.style.left = 26;
						calAttribute.style.visibility = "visible";
						document.all.showCalendar.value = 1;
						
					}
					
					function submitHandler() 
					{ 
						document.forms[0].doSubmit(); 
					} 
					function OCRResult()
					{
						
						bolLoading = 0;
						document.body.style.cursor = "default";
						if (event.result.error)
						{
							//Pull the error information from the event.result.errorDetail properties
							var xfaultcode		= event.result.errorDetail.code;
							var xfaultstring	= event.result.errorDetail.string;
							var xfaultsoap		= event.result.errorDetail.raw;
							
							// Add code to handle specific codes here
							if (xfaultstring == "Server was unable to process request. --> Image area is too large, please select a smaller area")
							{
								alert("The area you have selected is too large, please select a smaller area and try again.");
								top.frames["frameImage"].stopLoading();
							}
							else
							{
								//alert("Error: (" + xfaultcode + ") " + xfaultstring);
								alert('An error has occured while scanning this region. Please try again. If the problem continues please contact support.');
								top.frames["frameImage"].stopLoading();
							}
						}
						else
						{
							//document.all.txtAttributeValue.value = event.result.value;
							if(event.result.value.indexOf("(-2)") != 0)
							{
							
								if (event.result.value == '')
								{
									alert('No data was returned from the server. Make sure you have selected an area with text.');
								}
								else
								{
									var selectedItem = Math.abs(document.all.ActiveRow.value) + 2;
									strFieldName = "dgAttributes__ctl" + selectedItem + "_txtAttributeValue";
									var txtAttributeValue = document.getElementById(strFieldName);
									if (txtAttributeValue != undefined) {txtAttributeValue.value = event.result.value; }
									ActivateSave();
								}

								top.frames["frameImage"].stopLoading();
							}
							else
								alert('Oops... no response was recieved. Please try again. If the problem continues contact support.');
								top.frames["frameImage"].stopLoading();
						}					
					}
					
				function textPopup()
				{
					var url;
					window.open("DocumentText.aspx","Text", "height=600,resizable=no,scrollbars=yes,status=yes,toolbar=no,width=500");
					return false;
				}
				
				function LoadAnnotation()
				{
					try
					{
						document.all.PageNo.value = parent.frames["frameImage"].PageNo();
						document.all.Annotation.value = parent.frames["frameImage"].GetAnnotation();
					}
					catch(ex)
					{

					}
				}
		
				function ActivateSave()
				{
					if (document.all.hiddenDirty.value != 1){
					dirtyChecked = '0';
					document.all.hiddenDirty.value = 1;
					top.ActivateSave();}
				}

				function GetDirty()
				{
					return document.all.hiddenDirty.value;
				}

				function DisableSave()
				{
					var hiddenDirty = document.getElementById('hiddenDirty');
					if (hiddenDirty != undefined) {document.all.hiddenDirty.value = 0; }
					dirtyChecked = '1';
					top.DisableSave();
				}

				function Confirmation()
				{
					try
					{
						parent.frames["frameImage"].Confirmation();
					}
					catch(ex)
					{
					}
				}
				
				function WorkFlowIdSet(wfid)
				{
					document.all.WorkflowId.value=wfid;
					if (document.all.hiddenDirty.value != 1)
					{
						document.all.hiddenDirty.value = 1;
						top.ActivateSave();
					}
					
				}
				
				function QueueIdSet(queueid)
				{
					document.all.QueueId.value=queueid;
					if (document.all.hiddenDirty.value != 1)
					{
						document.all.hiddenDirty.value = 1;
						top.ActivateSave();
					}
				}
				
				function WorkflowNoteSet(wfnote)
				{
					document.all.WorkflowNote.value=wfnote;
					if (document.all.hiddenDirty.value != 1)
					{
						document.all.hiddenDirty.value = 1;
						top.ActivateSave();
					}
				}
				
				function Urgent(val)
				{
					document.all.Urgent.value = val;
					if (document.all.hiddenDirty.value != 1)
					{
						document.all.hiddenDirty.value = 1;
						top.ActivateSave();
					}
				}
				
				function Mode(val)
				{
					document.all.Mode.value = val;
					if (document.all.hiddenDirty.value != 1)
					{
						document.all.hiddenDirty.value = 1;
						if(top.tblNavigation)
						{
							top.ActivateSave();
						}
					}
				}

				function RouteId(val)
				{
					document.all.RouteId.value = val;
					if (document.all.hiddenDirty.value != 1)
					{
						document.all.hiddenDirty.value = 1;
						if(top.tblNavigation)
						{
							top.ActivateSave();
						}
					}
				}

				function commitRow(ctrl)
				{
					if (event.keyCode == 13)
					{
						event.cancelBubble = true;
						event.returnValue = false;
						var btnSaveItem = document.getElementById(ctrl);
						btnSaveItem.click();
					}
				}
				
				function clearDirty()
				{
					document.all.hiddenDirty.value = 0;
					document.all.selectedIndex.value = 0;
					document.all.moveNext.value = "0";
					document.all.hiddenSubmit.value = "";
					ignoreDirty = true;
				}
				
				function FolderLookup()
				{
					var val = window.showModalDialog('Tree.aspx?','FolderLookup','resizable:no;status:no;dialogHeight:625px;dialogWidth:260px;scroll:no');
					if (val != undefined)
					{
						varArray = val.split('�');
						document.all.FolderId.value = varArray[0].replace('W','');
						document.all.txtFolder.value = varArray[1];
						ActivateSave();
						var folder = varArray[0].replace('W','');
						try { window.parent.frames['frmDistribution'].setFolder(folder); } catch(ex) {  }
					}
				}
				
				function ClearFolder()
				{
					document.getElementById('FolderId').value = '';
					document.getElementById('txtFolder').value = '';
					ActivateSave();
				}
				
				function setFolder(id,name){
					document.getElementById('FolderId').value = id;
					document.getElementById('txtFolder').value = name;
					ActivateSave();
				}
				
				function catchKey()
				{

					var VK_LEFT = 37; 
					var VK_UP = 38; 
					var VK_RIGHT = 39; 
					var VK_DOWN = 40; 

					switch (event.keyCode)
					{
					case VK_LEFT:
						{window.alert("Left");break} 
					case VK_UP:
						{window.alert("Up");break} 
					case VK_RIGHT:
						{window.alert("Right");break} 
					case VK_DOWN:
						{window.alert("Down");break} 
					default:
						event.cancelBubble = true;
						event.returnValue = false;
						return false;
						break; 
					}
				}
				
				function browseAttribute(attributeId,txtAttributeValue)
				{
					var d = new Date();
					var random = d.getHours()+d.getMinutes()+d.getSeconds()+d.getMilliseconds();
					var folderId = document.all.FolderId.value;
					if (folderId == '') { folderId = 0; }
					var documentId = document.getElementById('ddlDocuments').options[document.getElementById('ddlDocuments').selectedIndex].value;
					var ret = window.showModalDialog('ListLookup.aspx?Id='+attributeId+'&DocumentId='+documentId+'&FolderID='+folderId+'&'+random,'SharePermissions','resizable:yes;status:no;dialogHeight:550px;dialogWidth:450px;scroll:no');
					if (ret != undefined)
					{ 
						var txtAttributeValue = document.getElementById(txtAttributeValue);
						if (txtAttributeValue != undefined)
						{
							txtAttributeValue.value = ret.split('�')[0];
							ActivateSave();
						}
					}
				}
				
				function getFolderId()
				{
					return document.all.FolderId.value;
				}
				
				function getDocumentId()
				{
					return document.getElementById('ddlDocuments').options[document.getElementById('ddlDocuments').selectedIndex].value;
				}
				
				function loadFolder()
				{
					try { window.parent.frames['frmDistribution'].setFolder(document.getElementById('FolderId').value); } catch(ex) {  }
				}
				
				var controlTextbox;

				function getGl()
				{
					return glEnabled;
				}

				function hideDistribution()
				{
					try
					{
						setGlEnabled(true);
						window.parent.hideDistribution();
					}
					catch(Ex)
					{

					}
				}

				function setControl(val)
				{
					try
					{
					
						if (document.all.controlName.value == '')
						{
							document.all.ControlValue.value = val;
							document.getElementById('btnAddControlItem').click();
							return true;
						}
					
						controlTextbox = document.getElementById(document.all.controlName.value);
						
						if (controlTextbox == undefined)
						{
							document.all.ControlValue.value = val;
							document.getElementById('btnAddControlItem').click();
						}
						else
						{
							controlTextbox.value = val;
						}
					}
					catch(ex)
					{

					}
				}
				
				function getControl()
				{
					if (document.all.controlName.value == '') { return '0.00'; };
					controlTextbox = document.getElementById(document.all.controlName.value);
					if (controlTextbox == undefined)
					{
						return '0.00';
					}
					else
					{
						return controlTextbox.value;
					}
				}
				
				function setGlEnabled(setting)
				{
					glEnabled = setting;
				}

				function NumericOnly(evt)
				{
					evt = (evt) ? evt : event;
					var charCode = (evt.charCode) ? evt.charCode : ((evt.keyCode) ? evt.keyCode
					: ((evt.which) ? evt.which : 0));
					if (charCode > 31 && (charCode < 48 || charCode >57)) {return false;}
					return true;
				}
				
				function showDistributionDialog(id,folderid)
				{
					var ctrlTotal = getControl();
                    var returnVal = window.showModalDialog('Distribution.aspx?Id=' + document.all.ddlDocuments.value + '&FolderId=' + document.all.FolderId.value + '&Control=' + ctrlTotal,'Distribution','resizable:no;status:no;dialogHeight:390px;dialogWidth:650px;scroll:no');
                    if (returnVal != undefined)
                    {
						setControl(returnVal);
                    }
	                ActivateSave();
				}

			function NumericOnlyandDecimals(evt)
			{
				evt = (evt) ? evt : event;
				var charCode = (evt.charCode) ? evt.charCode : ((evt.keyCode) ? evt.keyCode
				: ((evt.which) ? evt.which : 0));
				if (charCode == 46) {return true;}
				if (charCode > 31 && (charCode < 48 || charCode >57)) {return false;}
				return true;
			}
			
			function relatedDocs(){
				window.parent.showRelated(document.getElementById('lblImageId').innerHTML);
			}

				
		</SCRIPT>
	</BODY>
</HTML>

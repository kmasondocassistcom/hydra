<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="FolderPermissions.aspx.vb"
    Inherits="docAssistWeb.FolderPermissions" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>Manage Item</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <link href="Styles.css" type="text/css" rel="stylesheet">
    <base target="_self">
</head>
<body bottommargin="0" leftmargin="0" topmargin="0" rightmargin="0">
    <form id="Form1" method="post" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="uFolderPerms" runat="server">
        <ContentTemplate>
            <table height="100%" cellspacing="0" cellpadding="0" width="660" border="0">
                <tr>
                    <td valign="top">
                        <table height="100%" cellspacing="0" cellpadding="0" width="100%" border="0">
                            <tr>
                                <td valign="top" height="100%">
                                    <div class="Container">
                                        <div class="north">
                                            <div class="east">
                                                <div class="south">
                                                    <div class="west">
                                                        <div class="ne">
                                                            <div class="se">
                                                                <div class="sw">
                                                                    <div class="nw">
                                                                        <table id="surround" height="100%" cellspacing="0" cellpadding="0" width="95%" border="0">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td align="right">
                                                                                        <table class="PageContent" cellspacing="0" cellpadding="0" width="100%" border="0">
                                                                                            <tr>
                                                                                                <td align="center" colspan="2">
                                                                                                    <table id="Tabs" cellspacing="0" cellpadding="0" border="0" runat="server">
                                                                                                        <tr style="font-family: 'Trebuchet MS'">
                                                                                                            <td class="selectedTab" id="tdSecurity" onclick="showSecurity();" align="center"
                                                                                                                width="160">
                                                                                                                Security
                                                                                                            </td>
                                                                                                            <td class="unselectedTab">
                                                                                                                <img height="1" src="spacer.gif" width="15">
                                                                                                            </td>
                                                                                                            <td class="unselectedTab" id="tdEmail" onclick="showEmail();" nowrap align="center"
                                                                                                                width="160">
                                                                                                                E-Mail Settings
                                                                                                            </td>
                                                                                                            <td class="unselectedTab" style="border-bottom: #6382ad 1px solid" width="275">
                                                                                                                &nbsp;
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                        <div id="divSecurity" style="width: 100%">
                                                                                            <table class="PageContent" cellspacing="0" cellpadding="0" width="100%" border="0">
                                                                                                <tr>
                                                                                                    <td nowrap width="100%">
                                                                                                        <span class="Heading" id="ScreenMode" runat="server">[Screen Mode]</SCREEN></span>
                                                                                                    </td>
                                                                                                    <td nowrap align="right">
                                                                                                        <asp:CheckBox ID="chkInherit" runat="server" CssClass="Text" Visible="False"></asp:CheckBox>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td nowrap width="100%" colspan="2">
                                                                                                        <span class="Text" id="GroupNameLabel" runat="server"><span class="Text" id="GroupDescLabel"
                                                                                                            runat="server">
                                                                                                            <asp:Label ID="lblName" runat="server" Font-Bold="True" Font-Italic="True" Font-Size="10pt"></asp:Label></span></span>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td nowrap colspan="2">
                                                                                                        <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                                                                                            <tr>
                                                                                                                <td align="left">
                                                                                                                    <div style="overflow: auto; width: 100%; height: 450px">
                                                                                                                        <asp:DataGrid ID="dgPermissions" runat="server" CssClass="SearchGrid" Width="100%"
                                                                                                                            AutoGenerateColumns="False" BorderWidth="0px">
                                                                                                                            <AlternatingItemStyle BackColor="#E0E0E0"></AlternatingItemStyle>
                                                                                                                            <HeaderStyle Font-Bold="True"></HeaderStyle>
                                                                                                                            <Columns>
                                                                                                                                <asp:TemplateColumn HeaderText="Group">
                                                                                                                                    <HeaderStyle Width="350px"></HeaderStyle>
                                                                                                                                    <ItemTemplate>
                                                                                                                                        <asp:Label ID="lblGroupId" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.GroupId") %>'
                                                                                                                                            Visible="False">
                                                                                                                                        </asp:Label>
                                                                                                                                        <asp:Label ID="lblFileLevel" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FileLevel") %>'
                                                                                                                                            Visible="False">
                                                                                                                                        </asp:Label>
                                                                                                                                        <asp:Label ID="lblFolderLevel" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FolderLevel") %>'
                                                                                                                                            Visible="False">
                                                                                                                                        </asp:Label>
                                                                                                                                        <asp:Label ID="lblGroupName" runat="server" Width="100%" Text='<%# DataBinder.Eval(Container, "DataItem.GroupName") %>'>
                                                                                                                                        </asp:Label>
                                                                                                                                    </ItemTemplate>
                                                                                                                                </asp:TemplateColumn>
                                                                                                                                <asp:TemplateColumn HeaderText="Document Permissions">
                                                                                                                                    <HeaderStyle Width="160px"></HeaderStyle>
                                                                                                                                    <ItemTemplate>
                                                                                                                                        <asp:DropDownList ID="ddlFilePermissions" runat="server" Width="150px">
                                                                                                                                            <asp:ListItem Value="0">None</asp:ListItem>
                                                                                                                                            <asp:ListItem Value="1">Read</asp:ListItem>
                                                                                                                                            <asp:ListItem Value="2">Add</asp:ListItem>
                                                                                                                                            <asp:ListItem Value="3">Change</asp:ListItem>
                                                                                                                                            <asp:ListItem Value="4">Delete</asp:ListItem>
                                                                                                                                        </asp:DropDownList>
                                                                                                                                    </ItemTemplate>
                                                                                                                                </asp:TemplateColumn>
                                                                                                                                <asp:TemplateColumn HeaderText="Folder Permissions">
                                                                                                                                    <HeaderStyle Width="160px"></HeaderStyle>
                                                                                                                                    <ItemTemplate>
                                                                                                                                        <asp:DropDownList ID="ddlFolderPermissons" runat="server" Width="150px">
                                                                                                                                            <asp:ListItem Value="0">None</asp:ListItem>
                                                                                                                                            <asp:ListItem Value="1">List</asp:ListItem>
                                                                                                                                            <asp:ListItem Value="2">Add</asp:ListItem>
                                                                                                                                            <asp:ListItem Value="3">Change</asp:ListItem>
                                                                                                                                            <asp:ListItem Value="4">Delete</asp:ListItem>
                                                                                                                                        </asp:DropDownList>
                                                                                                                                    </ItemTemplate>
                                                                                                                                </asp:TemplateColumn>
                                                                                                                            </Columns>
                                                                                                                        </asp:DataGrid>
                                                                                                                    </div>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                        <br>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </div>
                                                                                        <input id="Skip" type="hidden" runat="server">
                                                                                        <div id="divEmail" style="width: 100%" align="left" runat="server">
                                                                                            <br>
                                                                                            <asp:Label ID="lblMessage" runat="server" CssClass="SearchGrid" Visible="False"></asp:Label>
                                                                                            <div id="innerEmail" style="width: 100%" align="left" runat="server">
                                                                                                <table class="SearchGrid" cellspacing="0" cellpadding="0" width="100%" border="0">
                                                                                                    <tr>
                                                                                                        <td align="right">
                                                                                                            <asp:CheckBox ID="chkEmailEnabled" runat="server" Text="Enabled"></asp:CheckBox><img
                                                                                                                height="1" src="spacer.gif" width="10">
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td width="100%">
                                                                                                            Email Address:&nbsp;<asp:TextBox ID="txtAlias" runat="server" Width="192px"></asp:TextBox><asp:Label
                                                                                                                ID="lblEmailDomain" runat="server" Width="200px">@sample.email.docassist.com</asp:Label>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <img height="5" src="spacer.gif" width="1">
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <b>Security Access</b>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td class="SearchGrid" nowrap colspan="2">
                                                                                                            <img height="1" src="spacer.gif" width="5"><asp:RadioButton ID="rdoSendersAll" runat="server"
                                                                                                                Checked="True" Text="Allow all senders" GroupName="Security"></asp:RadioButton><br>
                                                                                                            <img height="1" src="spacer.gif" width="5"><asp:RadioButton ID="rdoSendersSpecified"
                                                                                                                runat="server" Text="Only allow senders from the list below" GroupName="Security">
                                                                                                            </asp:RadioButton><br>
                                                                                                            <table id="tblAdd" cellspacing="0" cellpadding="0" border="0">
                                                                                                                <tr>
                                                                                                                    <td nowrap>
                                                                                                                        <img height="1" src="spacer.gif" width="15"><asp:TextBox ID="txtAddress" runat="server"
                                                                                                                            Width="400px"></asp:TextBox>
                                                                                                                    </td>
                                                                                                                    <td align="left">
                                                                                                                        <asp:Button ID="btnAddEmail" runat="server" Width="60px" Text="Add"></asp:Button>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td class="2">
                                                                                                                        <img height="5" src="spacer.gif" width="1">
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td>
                                                                                                                        <img height="1" src="spacer.gif" width="15"><asp:ListBox ID="lstSendersAllowed" runat="server"
                                                                                                                            Width="400px" BorderStyle="Solid" Height="90px"></asp:ListBox>
                                                                                                                    </td>
                                                                                                                    <td valign="top" align="left" width="100%">
                                                                                                                        <asp:Button ID="btnRemove" runat="server" Width="60px" Text="Remove"></asp:Button>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                            <img height="1" src="spacer.gif" width="5"><br>
                                                                                                            <img height="1" src="spacer.gif" width="5"><asp:CheckBox ID="chkAllowAccountUsersAccess"
                                                                                                                runat="server" Text="Allow account users as authorized senders"></asp:CheckBox><br>
                                                                                                            <img height="1" src="spacer.gif" width="12"><asp:CheckBox ID="chkRequireAccountUserAccess"
                                                                                                                runat="server" Text="Require account users to have folder access"></asp:CheckBox>
                                                                                                        </td>
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <img height="2" src="spacer.gif" width="1">
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td style="height: 14px">
                                                                                                                <b>Save Options</b>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <img height="1" src="spacer.gif" width="5">Document Type&nbsp;<asp:DropDownList ID="cmbDocumentTypes"
                                                                                                                    runat="server" Width="200px" AutoPostBack="True">
                                                                                                                </asp:DropDownList>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <div id="divWorkflow" runat="server">
                                                                                                        </div>
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <b>Workflow Options</b>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <img height="1" src="spacer.gif" width="5">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                                                Workflow&nbsp;
                                                                                                                <asp:DropDownList ID="cmbWorkflow" runat="server" Width="203px" AutoPostBack="True">
                                                                                                                </asp:DropDownList>
                                                                                                                <br>
                                                                                                                <img height="1" src="spacer.gif" width="5"><img height="1" src="spacer.gif" width="5"><img
                                                                                                                    height="1" src="spacer.gif" width="29"><img height="1" src="spacer.gif" width="5">&nbsp;
                                                                                                                Queue&nbsp;
                                                                                                                <asp:DropDownList ID="cmbQueue" runat="server" Width="203px">
                                                                                                                </asp:DropDownList>
                                                                                                                <br>
                                                                                                                <img height="1" src="spacer.gif" width="5"><img height="1" src="spacer.gif" width="5"><img
                                                                                                                    height="1" src="spacer.gif" width="63"><img height="1" src="spacer.gif" width="5">&nbsp;
                                                                                                                <asp:CheckBox ID="chkUrgent" runat="server" Text="Urgent"></asp:CheckBox>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <div>
                                                                                                        </div>
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <b>Processing Options</b>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <img height="1" src="spacer.gif" width="5"><asp:CheckBox ID="chkSaveEmailMessage"
                                                                                                                    runat="server" Visible="False" Text="Save Email Message (Subject/Message Body)">
                                                                                                                </asp:CheckBox><br>
                                                                                                                <img height="1" src="spacer.gif" width="12"><asp:CheckBox ID="chkSendNoAttachmentWarning"
                                                                                                                    runat="server" Text="Send warning message if no attachment received"></asp:CheckBox><br>
                                                                                                                <img height="1" src="spacer.gif" width="12"><asp:CheckBox ID="chkConvertToImages"
                                                                                                                    runat="server" Text="Convert PDF and TIFF attachments to Images"></asp:CheckBox><br>
                                                                                                                <img height="1" src="spacer.gif" width="5"><asp:CheckBox ID="chkSendEmailConfirmation"
                                                                                                                    runat="server" Text="Send Email Confirmation after message is processed"></asp:CheckBox>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                </table>
                                                                                            </div>
                                                                                        </div>
                                                                                        <table class="PageContent" cellspacing="0" cellpadding="0" border="0">
                                                                                            <tr>
                                                                                                <td align="right" width="100%">
                                                                                                    <asp:Label ID="lblError" runat="server" CssClass="ErrorText" Visible="False"></asp:Label>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td align="left">
                                                                                                    <asp:ImageButton ID="btnClear" runat="server" Width="0px" Height="0px"></asp:ImageButton>
                                                                                                </td>
                                                                                                <td nowrap width="180">
                                                                                                    <a href="javascript:window.close();">
                                                                                                        <img src="Images/smallred_cancel.gif" border="0"></a> &nbsp;<asp:ImageButton ID="btnSave"
                                                                                                            runat="server" ImageUrl="Images/smallred_save.gif" ToolTip="Save"></asp:ImageButton>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                </td>
                            </tr>
                        </table>
                        </DIV></DIV></DIV></DIV></DIV></DIV></DIV></DIV></DIV>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
    </TD></TR></TBODY></TABLE> </form>

    <script language="javascript">
		
			showSecurity();
			disabledChecks(false);
			
			var tdSecurity = document.getElementById('tdSecurity');
			if (tdSecurity != undefined)
			{
				document.getElementById('tdSecurity').className = 'selectedTab';
			}
							
			if (document.getElementById('rdoSendersSpecified').checked == false)
			{
				document.getElementById('tblAdd').disabled = true;
			}
			
			/*
			if (document.getElementById('chkSaveAttachments').checked == false)
			{
				document.getElementById('chkSendNoAttachmentWarning').disabled = true;
				document.getElementById('chkConvertToImages').disabled = true;
			}
			else
			{
				document.getElementById('chkSendNoAttachmentWarning').disabled = false;
				document.getElementById('chkConvertToImages').disabled = false;
			}
			*/
			
			if (document.getElementById('chkAllowAccountUsersAccess').checked == false)
			{
				document.getElementById('chkRequireAccountUserAccess').disabled = true;
			}
			else
			{
				document.getElementById('chkRequireAccountUserAccess').disabled = false;
			}
						
			function clearPrompt()
			{
				var chkInherit = document.getElementById('chkInherit');
				if (chkInherit.checked)
				{
					if (confirm('Setting this folder to inherit permissions from its parent will clear all changes made. Are you sure you want to continue?'))
					{
						chkInherit.checked = true;
					}
					else
					{
						chkInherit.checked = false;
					}
				}
 				document.getElementById('btnClear').click();
			}
			
			function showSecurity()
			{
				var divSecurity = document.getElementById('divSecurity');
				if(divSecurity != undefined) { divSecurity.style.display = 'block'; }
				var divEmail = document.getElementById('divEmail');
				if(divEmail != undefined) { divEmail.style.display = 'none'; }
				if (document.getElementById('tdSecurity') != undefined) { document.getElementById('tdSecurity').className = 'selectedTab'; }
				if (document.getElementById('tdEmail') != undefined) { document.getElementById('tdEmail').className = 'unselectedTab'; }
			}
				
			function showEmail()
			{
				var divSecurity = document.getElementById('divSecurity');
				if(divSecurity != undefined) { divSecurity.style.display = 'none'; }
				var divEmail = document.getElementById('divEmail');
				if(divEmail != undefined) { divEmail.style.display = 'block'; }
				document.getElementById('tdSecurity').className = 'unselectedTab';
				document.getElementById('tdEmail').className = 'selectedTab';
			}
			
			function hitReturn()
			{
				if (event.keyCode == 13)
				{
					document.getElementById('btnAddEmail').click();
					//document.getElementById('txtAddress').focus();
				}
			}
			
			function disableAdd(mode)
			{
				document.getElementById('tblAdd').disabled = mode;
			}
			

			function disabledChecks(mode)
			{
				if (mode == true)
				{
					mode = false;
				}
				else
				{
					mode = true;
				}

				//document.getElementById('chkSendNoAttachmentWarning').disabled = mode;
				//document.getElementById('chkConvertToImages').disabled = mode;
			}
			
			function disableFolderAccess(mode)
			{
				if (mode == true)
				{
					mode = false;
				}
				else
				{
					mode = true;
				}

				document.getElementById('chkRequireAccountUserAccess').disabled = mode;
			}
			
    </script>

</body>
</html>

Partial Class UserPreferences
    Inherits System.Web.UI.Page

    Private mobjUser As Accucentric.docAssist.Web.Security.User
    Protected WithEvents Dropdownlist1 As System.Web.UI.WebControls.DropDownList
    Private mconSqlImage As SqlClient.SqlConnection

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Not Page.IsPostBack Then
            Dim dtTimeZones As New DataTable
            Dim sqlCon As New SqlClient.SqlConnection(Functions.ConnectionString)

            Try

                If Request.Cookies("docAssist") Is Nothing Then
                    'Cookie not found, kick user out
                    Response.Redirect("Default.aspx")
                    Exit Sub
                End If

                Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
                Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))

                sqlCon.Open()

                Dim dtAccess As New DataTable
                dtAccess = Functions.UserAccess(Functions.BuildConnection(mobjUser), mobjUser.UserId, mobjUser.AccountId)

                If dtAccess.Rows(0)("Search") = True Then
                    ddlStartPage.Items.Add(New ListItem("Explorer", "0"))
                End If

                mconSqlImage = Functions.BuildConnection(Me.mobjUser)

                If Functions.ModuleSetting(Functions.ModuleCode.WORKFLOW, mobjUser, mconSqlImage) = False Then
                    If Me.mobjUser.EditionID <> 5 Then ddlStartPage.Items.Add(New ListItem("Workflow", "3"))
                End If

                If dtAccess.Rows(0)("Index") = True Then
                    If Me.mobjUser.EditionID <> 5 Then ddlStartPage.Items.Add(New ListItem("Scan", "1"))
                End If

                ddlDefault.SelectedValue = mobjUser.DefaultQuality

                Dim conMaster As New SqlClient.SqlConnection
                Try

                    If Not Page.IsPostBack Then

                        conMaster = Functions.BuildMasterConnection

                        Dim prefs As New Accucentric.docAssist.Data.Images.UserPreferences(conMaster)
                        Dim intDefaultQuality As Integer = prefs.GetAccoutDefaultQuality(mobjUser.AccountId.ToString)

                        If intDefaultQuality < 0 Then
                            ddlDefault.Enabled = False
                            lblDisabled.Visible = True
                        End If

                    End If

                Catch ex As Exception

                Finally

                    If conMaster.State <> ConnectionState.Closed Then
                        conMaster.Close()
                        conMaster.Dispose()
                    End If

                    If mconSqlImage.State <> ConnectionState.Closed Then
                        mconSqlImage.Close()
                        mconSqlImage.Dispose()
                    End If

                End Try

                ddlStartPage.SelectedValue = mobjUser.DefaultStartPage
                ddlDocView.SelectedValue = mobjUser.DefaultDocView

                CloseViewer.Checked = Me.mobjUser.CloseViewer

                Select Case mobjUser.DefaultResultCount
                    Case 25
                        ddlNoResults.SelectedValue = 0
                    Case 50
                        ddlNoResults.SelectedValue = 1
                    Case 100
                        ddlNoResults.SelectedValue = 2
                End Select

                If mobjUser.DefaultSkipResult = "0" Then
                    chkLoad.Checked = False
                Else
                    chkLoad.Checked = True
                End If

                'Close sql connection
                sqlCon.Close()
                sqlCon.Dispose()

            Catch ex As Exception

                'Throw user to error page
                'ErrPage(ex.Message, False, Response) - no good, this will load the error page in a popup.

                If sqlCon.State = ConnectionState.Open Then
                    sqlCon.Close()
                    sqlCon.Dispose()
                End If

            End Try

        End If
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click

        Dim intChecked As Integer
        Dim sqlConn As New SqlClient.SqlConnection(Functions.ConnectionString)

        'Write setting back to db.

        Try

            If Request.Cookies("docAssist") Is Nothing Then
                'Cookie not found, kick user out
                Response.Redirect("Default.aspx")
                Exit Sub
            End If

            Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
            Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))

            sqlConn.Open()

            Dim sqlcmd As New SqlClient.SqlCommand("UPDATE Users SET DefaultStartPage = @DefaultStartPage, DefaultDocView = @DefaultDocView, DefaultResultCount = @DefaultResultCount, DefaultSkipResult = @DefaultSkipResult, ViewerPopup = @ViewerPopup, DefaultQuality = @DefaultQuality, CloseViewer = @CloseViewer WHERE EMailAddress = @EMailAddress", sqlConn)

            sqlcmd.Parameters.Add("@DefaultStartPage", ddlStartPage.SelectedValue)
            sqlcmd.Parameters.Add("@DefaultDocView", ddlDocView.SelectedValue)
            sqlcmd.Parameters.Add("@DefaultResultCount", ddlNoResults.SelectedValue)
            sqlcmd.Parameters.Add("@ViewerPopup", "0")

            'sqlcmd.Parameters.Add("@DefaultTimeZoneID", ddlTimeZone.SelectedValue)

            If chkLoad.Checked = True Then
                intChecked = 1
            Else
                intChecked = 0
            End If

            sqlcmd.Parameters.Add("@DefaultSkipResult", intChecked)
            sqlcmd.Parameters.Add("@EMailAddress", mobjUser.EMailAddress)
            sqlcmd.Parameters.Add("@DefaultQuality", ddlDefault.SelectedValue)
            sqlcmd.Parameters.Add("@CloseViewer", CloseViewer.Checked)

            sqlcmd.ExecuteScalar()

            'Close SQL connection
            sqlConn.Close()
            sqlConn.Dispose()

            'Response.Write("<script language='javascript'>window.close();</script>")

            'Response.Write("<script language='javascript'>window.close();</script>")
            lblError.Text = "Preferences saved."


        Catch ex As Exception

            If sqlConn.State = ConnectionState.Open Then
                sqlConn.Close()
                sqlConn.Dispose()
            End If

        End Try

    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)

    End Sub
End Class

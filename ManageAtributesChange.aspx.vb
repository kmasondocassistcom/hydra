Partial Class ManageAtributesChange
    Inherits System.Web.UI.Page



    Private mobjUser As Accucentric.docAssist.Web.Security.User
    Private mconSqlImage As SqlClient.SqlConnection

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Not Page.IsPostBack Then

            PopulateAttributeList()

            'Request.Cookies("Admin")("AttributeID") = Response.Cookies("Admin")("AttributeID")

            If Session("mAdminAttributesMode") = "CHANGE" Then
                'lblMode.Text = "Manage Attributes - Change Attribute"
                lblAction.Text = "Select Attribute to Change"
                'lnkNext.Text = "Next"
                btnNext.ImageUrl = "Images/btn_nextadmin.gif"
            End If

            If Session("mAdminAttributesMode") = "REMOVE" Then
                'lblMode.Text = "Manage Attributes - Remove Attribute"
                lblAction.Text = "Select Attribute to Remove"
                'lnkNext.Text = "Finish"
                btnNext.ImageUrl = "Images/btn_finish.gif"
            End If



        End If

    End Sub

    Private Sub PopulateAttributeList()
        Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
        Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))

        Me.mobjUser.AccountId = guidAccountId
        Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
        Me.mconSqlImage.Open()

        Dim cmdSql As SqlClient.SqlCommand

        Dim dtsql As New DataTable
        cmdSql = New SqlClient.SqlCommand("acsAdminAttributeList", Me.mconSqlImage)
        cmdSql.CommandType = CommandType.StoredProcedure
        Dim dasql As New SqlClient.SqlDataAdapter(cmdSql)
        dasql.Fill(dtsql)

        If dtsql.Rows.Count <= 0 Then
            'TODO: No Cabinets to change
        Else
            lstAttributes.DataTextField = "AttributeName"
            lstAttributes.DataValueField = "AttributeID"
            lstAttributes.DataSource = dtsql
            lstAttributes.DataBind()
            lstAttributes.Items(0).Selected = True
        End If



    End Sub


    Private Sub lnkNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If lstAttributes.SelectedItem.Value > 0 Then


            If Session("mAdminAttributesMode") = "CHANGE" Then
                Session("mAdminAttributeID") = lstAttributes.SelectedItem.Value
                Session("mAdminAttributeName") = lstAttributes.SelectedItem.Text
                Server.Transfer("ManageAttributeChange2.aspx")
            End If

            If Session("mAdminAttributesMode") = "REMOVE" Then
                Session("mAdminAttributeID") = lstAttributes.SelectedItem.Value
                Session("mAdminAttributeName") = lstAttributes.SelectedItem.Text
                If CheckAttributeUsed(lstAttributes.SelectedItem.Value) = True Then
                    lblError.Text = "Attribute " & lstAttributes.SelectedItem.Text & " has documents assigned. Remove assigned documents and retry."
                    Exit Sub
                Else
                    lblError.Text = ""
                    RemoveAttribute(lstAttributes.SelectedItem.Value)
                    Response.Redirect("ManageAttributes.aspx")
                End If
            End If


        End If

    End Sub

    Private Function CheckAttributeUsed(ByVal iAttributeID As Integer) As Boolean
        Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
        Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))

        Me.mobjUser.AccountId = guidAccountId
        Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
        Me.mconSqlImage.Open()

        Dim cmdSql As SqlClient.SqlCommand

        Dim dtsql As New DataTable
        cmdSql = New SqlClient.SqlCommand("acsAdminAttributeRemoveCheck", Me.mconSqlImage)
        cmdSql.CommandType = CommandType.StoredProcedure
        cmdSql.Parameters.Add("@AttributeID", iAttributeID)
        Dim dasql As New SqlClient.SqlDataAdapter(cmdSql)
        dasql.Fill(dtsql)


        If dtsql.Rows.Count > 0 Then
            Return True
        Else
            Return False

        End If



    End Function

    Private Function RemoveAttribute(ByVal iAttributeID As Integer) As Boolean
        Dim cmdSql As SqlClient.SqlCommand
        Dim dtsql As New DataTable
        cmdSql = New SqlClient.SqlCommand("acsAdminAttributeRemove", Me.mconSqlImage)
        cmdSql.CommandType = CommandType.StoredProcedure
        cmdSql.Parameters.Add("@AttributeID", iAttributeID)
        cmdSql.ExecuteNonQuery()
    End Function

    Private Sub btnNext_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnNext.Click

        If lstAttributes.SelectedItem.Value > 0 Then

            If Session("mAdminAttributesMode") = "CHANGE" Then
                Session("mAdminAttributeID") = lstAttributes.SelectedItem.Value
                Session("mAdminAttributeName") = lstAttributes.SelectedItem.Text
                Server.Transfer("ManageAttributeChange2.aspx")
            End If

            If Session("mAdminAttributesMode") = "REMOVE" Then
                Session("mAdminAttributeID") = lstAttributes.SelectedItem.Value
                Session("mAdminAttributeName") = lstAttributes.SelectedItem.Text
                If CheckAttributeUsed(lstAttributes.SelectedItem.Value) = True Then
                    lblError.Text = "Attribute " & lstAttributes.SelectedItem.Text & " is assigned to a document. Remove them from the documents before deleting."
                    Exit Sub
                Else
                    lblError.Text = ""
                    RemoveAttribute(lstAttributes.SelectedItem.Value)
                    Response.Redirect("ManageAttributes.aspx")
                End If
            End If

        End If

    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCancel.Click
        Response.Redirect("ManageAttributes.aspx")
    End Sub
End Class

Imports Accucentric.docAssist

Partial Class Thumbnails
    Inherits System.Web.UI.Page


    Private VersionId As Integer
    Private mobjUser As Accucentric.docAssist.Web.Security.User
    Private mconSqlImage As SqlClient.SqlConnection

    Private Const MAX_COLS As Integer = 1
    Private Const MAX_ROWS As Integer = 5
    Private Const MAX_WIDTH As Integer = 162
    Private Const MAX_HEIGHT As Integer = 180


#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub WriteScripts()
        Dim sbScripts As New System.Text.StringBuilder
        sbScripts.Append("var hostname=""")
        sbScripts.Append(Request.Url.AbsoluteUri.Substring(0, Request.Url.AbsoluteUri.Length - Request.Url.AbsolutePath.Length - 4))
#If DEBUG Then
        sbScripts.Append("/docAssistWeb1")
#End If
        sbScripts.Append(""";" & vbCrLf)
        System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.NewGuid.ToString, sbScripts.ToString, True)
    End Sub

    Private Function GetThumbnail(ByVal intImageDetailId As Integer, ByVal conSql As SqlClient.SqlConnection) As Byte()

        Dim sqlCmd As SqlClient.SqlCommand
        Dim rValue As Byte()
        Try
            sqlCmd = New SqlClient.SqlCommand("SELECT Image FROM ImageDetail WITH (NOLOCK) WHERE ImageDetId = @ImageDetailId")
            'sqlCmd = New SqlClient.SqlCommand("SELECT Thumbnail FROM ImageDetail WITH (NOLOCK) WHERE ImageDetId = @ImageDetailId")
            sqlCmd.CommandType = CommandType.Text
            sqlCmd.Parameters.Add("@ImageDetailId", intImageDetailId)
            sqlCmd.Connection = conSql
            Dim db As New Data.Images.db
            rValue = db.ExecuteScalar(sqlCmd)

        Catch sqlEx As SqlClient.SqlException
            Throw New Exception("Sql Exception: " & sqlEx.Message)
        End Try

        Return rValue
    End Function

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Not Page.IsPostBack Then

            ' Build the user object
            Try
                Me.mobjUser = BuildUserObject(Request.Cookies("docAssist"))
                Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
            Catch ex As Exception
                If ex.Message = "User is not logged on" Then
                    Response.Redirect("default.aspx")
                End If
            End Try

            VersionId = Request.QueryString("VersionId")

            LoadThumbnails()

            'If Not Page.IsPostBack Then
            '    Try
            '        If Me.mcookieSearch.VersionID > 0 Then
            '            Me.mintVersionId = Me.mcookieSearch.VersionID
            '        Else
            '            Throw New Exception("Error receiving image information.")
            '        End If

            '    Catch ex As Exception
            '        'lblErrorMessage.Text = ex.Message
            '        'lblErrorMessage.Visible = True
            '    End Try
            '    If Me.mintVersionId > 0 Then
            '        LoadThumbnails()
            '    End If
            'End If

        End If

    End Sub

    Private Sub LoadThumbnails(Optional ByVal intStartPage As Integer = 1)

        ' Open connection
        If Not Me.mconSqlImage.State = ConnectionState.Open Then
            Me.mconSqlImage.Open()
        End If

        ' Loop through each page of the version
        Dim sbQuery As New System.Text.StringBuilder
        sbQuery.Append("SeqId >= ")
        sbQuery.Append(intStartPage.ToString)
        sbQuery.Append(" and SeqId < ")
        sbQuery.Append(intStartPage + (MAX_COLS * MAX_ROWS))
        Dim intPageCount As Integer = Session(VersionId & "PageCount") 'CType(dsVersion.ImageVersion.Rows(0), Accucentric.docAssist.Data.dsVersionControl.ImageVersionRow).SeqTotal

        Dim sb As New System.Text.StringBuilder

        For x As Integer = 1 To intPageCount
            'sb.Append("<SPAN onclick=""GoToPage('" & x & "');"" style=""CURSOR: hand; BORDER-RIGHT: black thin solid; BORDER-TOP: black thin solid; BORDER-LEFT: black thin solid; WIDTH: 162px; BORDER-BOTTOM: black thin solid; HEIGHT: 180px"" id=page" & x & "></SPAN><BR><SPAN style=""FONT-SIZE: 8pt; FONT-FAMILY: 'Trebuchet MS'"">" & x.ToString & "</SPAN><BR>")
            sb.Append("<SPAN onclick=""GoToPage('" & x & "');"" class=Thumbnails id=page" & x & "></SPAN><BR><SPAN style=""FONT-SIZE: 8pt; FONT-FAMILY: 'Trebuchet MS'"">" & x.ToString & "</SPAN><BR>")
            'Thumbnails

        Next

        Dim strHostname As String = Request.Url.AbsoluteUri.Substring(0, Request.Url.AbsoluteUri.Length - Request.Url.AbsolutePath.Length)
#If DEBUG Then
        strHostname += "/docAssistWeb1"
#End If

        System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.NewGuid.ToString, "startPage = " & intStartPage & ";endPage = " & intStartPage + MAX_ROWS - 1 & ";versionId = " & VersionId & ";quality = " & "1" & ";accountId = '" & Me.mobjUser.AccountId.ToString & "';pageCount = '" & intPageCount & "';hostname = '" & strHostname & "';", True)

        lblThumbs.Text = sb.ToString

    End Sub

End Class
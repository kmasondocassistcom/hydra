Public Class EmailParameter

    Private strParameterName As String
    Private strParameterValue As String

    Sub New(ByVal Name, ByVal Value)
        Me.Name = Name
        Me.Val = Value
    End Sub

    Public Property Name() As String
        Get
            Return strParameterName
        End Get
        Set(ByVal Value As String)
            strParameterName = Value
        End Set
    End Property

    Public Property Val() As String
        Get
            Return strParameterValue
        End Get
        Set(ByVal Value As String)
            strParameterValue = Value
        End Set
    End Property

End Class
Imports Accucentric.docAssist.Web

Partial Class _Default
    Inherits System.Web.UI.Page

    Private mconSqlImage As New SqlClient.SqlConnection
    Private mconSqlMaster As New SqlClient.SqlConnection

    Private mobjUser As Accucentric.docAssist.Web.Security.User

    Private mcookieImage As cookieImage
    Private mcookieSearch As cookieSearch

    Private strDocumentID As String
    Private strAttributeID As String
    Private strPrintDriverFile As String
    Private strAttributeValue As String

    Private intLockout As Integer = 5 'Number of bad login attempts before the user is locked out.


#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents reEmailAddress As System.Web.UI.WebControls.RegularExpressionValidator
    Protected WithEvents rfvPassword As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents rfvEMailAddress As System.Web.UI.WebControls.RequiredFieldValidator

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Response.Expires = 0
        Response.Cache.SetNoStore()
        Response.AppendHeader("Pragma", "no-cache")

        btnSignOut.Style("visibility") = "hidden"

        'Private label
        Try
            If Not IsNothing(Request.QueryString("LID")) Or Not IsNothing(Session("CustomLogoId")) Then
                Dim logoId As String
                If IsNothing(Request.QueryString("LID")) Then
                    logoId = Session("CustomLogoId")
                Else
                    logoId = Request.QueryString("LID")
                End If
                Dim strLogoFileName As String = Server.MapPath("./images/custom/") & logoId & "_login.gif"
                If System.IO.File.Exists(strLogoFileName) Then
                    imgLogin.Src = "Images/custom/" & logoId & "_login.gif"
                    Session("CustomLogo") = logoId & "_banner.gif"
                    Session("CustomLogoId") = logoId
                    'imgdocAssist.Visible = False
                    imgNotCust.Visible = False
                    'footertext.Visible = False
                    footrLinks.Visible = False
                    supportLink.Visible = False
                End If
            End If
        Catch ex As Exception

        End Try

        If Request.QueryString.Count > 0 Then
            Select Case Request.QueryString("ErrId")
                Case ErrorCodes.SESSION_TIMEOUT
                    'btnSignOut_Click(Nothing, Nothing)
                    Functions.SignOut(Me, False)
                    lblErrorMessage.Visible = True
                    lblErrorMessage.Text = "Your session has timed out due to inactivity."

                    If Not Request.Cookies("docAssistSave") Is Nothing Then
                        '
                        ' Get E-Mail Address
                        If Not Request.Cookies("docAssistSave")("LastLogon") Is Nothing Then
                            txtEMailAddress.Text = Request.Cookies("docAssistSave")("LastLogon")
                        End If

                    End If

                    Exit Sub
            End Select
        End If

#If DEBUG Then
        'Write the build date for bug track testing.
        Response.Write("<!--Build Date:" & System.IO.File.GetLastWriteTime(Server.MapPath("bin/docAssistWeb.dll")).AddHours(-4) & " EST-->")
#End If
        Response.Write("<!--Server:" & Server.MachineName & "-->")

        'If Not Page.IsPostBack Then
        '    Try
        '        If Request.QueryString.Count > 0 Then
        '            Select Case Request.QueryString("ErrId")
        '                Case ErrorCodes.SESSION_TIMEOUT
        '                    lblErrorMessage.Visible = True
        '                    lblErrorMessage.Text = "Your session has timed out due to inactivity. Please login to continue."
        '            End Select
        '        End If
        '    Catch ex As Exception

        '    End Try
        'End If

        Try
            If Not Session("ErrorMessage") Is Nothing And Session("ErrorMessage") <> "" Then
                lblErrorMessage.Text = "<BR>" & Session("ErrorMessage")
                lblErrorMessage.Visible = True
                Functions.SignOut(Me, True)
                Session("ErrorMessage") = Nothing

                ' Check for cookie
                If Not Request.Cookies("docAssistSave") Is Nothing Then
                    ' Get E-Mail Address
                    If Not Request.Cookies("docAssistSave")("LastLogon") Is Nothing Then
                        txtEMailAddress.Text = Request.Cookies("docAssistSave")("LastLogon")
                    End If

                End If

                Exit Sub
            End If
        Catch ex As Exception

        End Try

        If Not Page.IsPostBack Then
            Try
                If Not Request.Cookies("docAssist") Is Nothing Then

                    If Not Request.Cookies("docAssist")("UserId") Is Nothing Then
                        'Check if user is logged in.

                        Dim intDiff As Integer
                        If txtTimeDiff.Value <> "" Then
                            intDiff = (CInt(txtTimeDiff.Value) / 60) * -1
                        End If

                        Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
                        Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))
                        Me.mobjUser.AccountId = guidAccountId

                        'User is logged in
                        txtEMailAddress.Visible = False
                        txtPassword.Visible = False
                        lblWelcome.Text = "Hello, " & Me.mobjUser.UserName & "<BR>" & Me.mobjUser.EMailAddress & "<BR>(Not you? <a href=""#"" onclick=""Logout();"">sign out</a>)<br><br>You are already signed into docAssist. Click 'Sign In' to continue."
                        lblWelcome.Visible = True
                        chkRemember.Visible = False
                        imgEmail.Visible = False
                        imgPass.Visible = False
                        lblErrorMessage.Visible = False
                        Exit Sub

                    End If

                Else

                    Try
                        If Request.QueryString.Count > 0 Then
                            Select Case Request.QueryString("ErrId")
                                Case ErrorCodes.SESSION_TIMEOUT
                                    btnSignOut_Click(Nothing, Nothing)
                                    lblErrorMessage.Visible = True
                                    lblErrorMessage.Text = "Your session has timed out due to inactivity."
                                Case ErrorCodes.ERROR_CHECKING_TIMEOUT
                                    lblErrorMessage.Visible = True
                                    lblErrorMessage.Text = "<BR>" & ErrorCodes.MESSAGE_ERROR_CHECKING_TIMEOUT
                                Case ErrorCodes.ERROR_PRINT_ACCESS_DENIED
                                    lblErrorMessage.Visible = True
                                    lblErrorMessage.Text = "<BR>" & ErrorCodes.MESSAGE_PRINT_ACCESS_DENIED
                                Case ErrorCodes.ERROR_INTEGRATION_ACCESS_DENIED
                                    lblErrorMessage.Visible = True
                                    lblErrorMessage.Text = "<BR>" & ErrorCodes.MESSAGE_INTEGRATION_ACCESS_DENIED
                            End Select
                        End If
                    Catch ex As Exception

                    End Try

                End If

            Catch ex As Exception

            End Try
        Else
            If Not Request.Cookies("docAssist") Is Nothing Then

                If Not Request.Cookies("docAssist")("UserId") Is Nothing Then
                    Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
                    Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))
                    Me.mobjUser.AccountId = guidAccountId

                    'Exit Sub

                End If

            End If
        End If

        Try
            If Response.Cookies("docAssist")("DualLogin") = True Then

                'Delete all cookies
                Dim aCookie As HttpCookie
                Dim i As Integer
                Dim cookieName As String
                Dim limit As Integer = Request.Cookies.Count - 1
                For i = 0 To limit
                    If Request.Cookies(i).Name = "docAssistSave" Then i += 1
                    cookieName = Request.Cookies(i).Name
                    aCookie = New HttpCookie(cookieName)
                    aCookie.Expires = DateTime.Now.AddDays(-1)
                    Response.Cookies.Add(aCookie)
                Next

                If Not Request.Cookies("docAssistSave") Is Nothing Then
                    '
                    ' Get E-Mail Address
                    If Not Request.Cookies("docAssistSave")("LastLogon") Is Nothing Then
                        txtEMailAddress.Text = Request.Cookies("docAssistSave")("LastLogon")
                        chkRemember.Checked = True
                    End If

                End If

                lblErrorMessage.Text = "Your account has been logged in at another browser. The current session has expired."
                lblErrorMessage.Visible = True

                'Clear error
                Response.Cookies("docAssist")("DualLogin") = False

                Exit Sub

            End If
        Catch ex As Exception

        End Try

        Try
            If Not Request.QueryString("PURL") Is Nothing Then
                If Request.QueryString("PURL").Trim.Length > 0 Then
                    strPrintDriverFile = Request.QueryString("PURL")
                Else
                    strPrintDriverFile = ""
                End If
            Else
                strPrintDriverFile = ""
            End If
        Catch ex As Exception

        End Try


        'Check if we are holding search values from the button thingy.
        If Request.QueryString("DID") <> "" And Request.QueryString("IID") <> "" And Request.QueryString("A") = "S" Then
            strDocumentID = Request.QueryString("DID")

            'Find the AttributeId
            Dim strKeys As String() = Request.QueryString.AllKeys
            If strKeys.Length > 0 Then
                For X As Integer = 0 To strKeys.Length - 1
                    If strKeys(X).StartsWith("AV") Then
                        strAttributeID = strKeys(X).Replace("AV", "")
                    End If
                Next
            End If

            strAttributeValue = Request.QueryString("AV")
        End If

        If Request.QueryString("VID") <> "" Then
            Session("iVersionID") = Request.QueryString("VID")
        End If

        If Not Page.IsPostBack Then

            ' Check for cookie
            If Not Request.Cookies("docAssistSave") Is Nothing Then
                ' Get E-Mail Address
                If Not Request.Cookies("docAssistSave")("LastLogon") Is Nothing Then
                    txtEMailAddress.Text = Request.Cookies("docAssistSave")("LastLogon")
                End If
            End If

            Try
                If Not IsNothing(Request.QueryString("u")) Then
                    txtEMailAddress.Text = Request.QueryString("u")
                End If
            Catch ex As Exception

            End Try

        End If

    End Sub

    Private Sub lnkForgotPassword_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Response.Redirect("RequestPwd.aspx")
    End Sub

    Private Sub btnSignIn_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSignIn.Click

        Dim promptWithTerms As Boolean = False
        Dim blockIpAddress As Boolean = False

        If Page.IsPostBack Then

            'Session("Timeout") = Now.AddMinutes(Constants.SESSION_TIMEOUT_MINUTES)
            Response.Cookies(SESSION_COOKIE_KEY)(SESSION_COOKIE) = Now.AddMinutes(Constants.SESSION_TIMEOUT_MINUTES)
            Response.Cookies(SESSION_COOKIE_KEY).Expires = Now.AddDays(100)
            Response.Cookies(SESSION_COOKIE_KEY)("RememberLogin") = True

            Response.Cookies(SINGLE_SESSION_COOKIE)(SINGLE_SESSION_COOKIE_KEY) = True
            Request.Cookies.Add(Request.Cookies(SINGLE_SESSION_COOKIE))
            'Response.Cookies(SINGLE_SESSION_COOKIE).Expires = Now.AddYears(-10)

            If Not Me.mobjUser Is Nothing Then

                Dim cookieDocAssist As System.Web.HttpCookie = Request.Cookies("docAssist")
                If Not cookieDocAssist Is Nothing Then cookieDocAssist.Expires = Now().AddDays(1)

                Dim cook1 As New HttpCookie("docAssist")
                cook1.Expires = Now.AddDays(1)
                cook1.Values.Add("UserId", Me.mobjUser.SessionID.ToString)
                cook1.Values.Add("AccountId", Me.mobjUser.AccountId.ToString)
                Response.Cookies.Add(cook1)

                Dim dtAccess As New DataTable
                dtAccess = Functions.UserAccess(Functions.BuildConnection(mobjUser), mobjUser.UserId, mobjUser.AccountId)

                Dim intWf As Integer = dtAccess.Rows(0)("Workflow")
                Dim intSearch As Integer = dtAccess.Rows(0)("Search")
                Dim intIndexer As Integer = dtAccess.Rows(0)("Index")

                If TrialExpired() = True Then
                    Throw New Exception("Your trial period has expired.")
                End If

                '## INTEGRATION STRING? ##

                'Successful authentication
                Functions.LogAuthentication(Me.mobjUser.AccountId.ToString, Me.mobjUser.UserId.ToString, Me.mobjUser.EMailAddress.ToString, Request.UserHostAddress, Server.MachineName, "Application", "Success", Left(Request.UserAgent.ToString, 2500))

                'Check if user has access to any documents
                If dtAccess.Rows(0)("Workflow") = 0 And dtAccess.Rows(0)("Search") = 0 And dtAccess.Rows(0)("Index") = 0 Then

                    'Check for administrator
                    If Me.mobjUser.SecAdmin = True Or Me.mobjUser.SysAdmin = True Then
                        Session("blnAdminOnlyMode") = True
                        Response.Redirect("UserAdmin.aspx")
                        Exit Sub
                    Else
                        lblErrorMessage.Text = "<BR>" & "No access to any documents. Contact your administrator. <br>"
                        lblErrorMessage.Visible = True
                        Functions.SignOut(Me, True)
                        Exit Sub
                    End If

                Else 'If they do have access, process normal check

                    'Check if we have a query string
                    If Request.Url.Query.Trim.Length > 0 And Request.RawUrl.IndexOf("ErrId") = -1 Then

                        'Where are we supposed to go
                        If Not IsNothing(Request.QueryString("A")) Then
                            Response.Redirect("Integration.aspx" & Request.Url.Query)
                        End If

                        If Not IsNothing(Request.QueryString("PURL")) Then
                            Response.Redirect("DocumentManager.aspx" & Request.Url.Query)
                        End If

                        If Not IsNothing(Request.QueryString("File")) Then
                            Response.Redirect("DocumentManager.aspx" & Request.Url.Query)
                        End If

                        If Not IsNothing(Request.QueryString("VID")) Then
                            Response.Redirect("Index.aspx" & Request.Url.Query)
                        End If

                        If Not IsNothing(Request.QueryString("Action")) Then
                            Select Case Request.QueryString("Action")
                                Case "WFDash"
                                    Response.Redirect("WorkflowDashboard.aspx")
                            End Select
                        End If

                        If Not IsNothing(Request.QueryString("LID")) Then
                            Select Case Me.mobjUser.DefaultStartPage
                                Case 0
                                    Response.Redirect("Search.aspx", False)
                                Case 1
                                    Response.Redirect("DocumentManager.aspx")
                                Case 3
                                    Response.Redirect("WorkflowDashboard.aspx")
                                Case Else
                                    Response.Redirect("Search.aspx")
                            End Select
                        End If

                        Exit Sub

                    Else
                        If intWf = -1 And intSearch = 0 And intIndexer = 0 Then
                            'Wf only user
                            Response.Redirect("WorkflowDashboard.aspx")
                        Else

                            ''Check if there is a message to display to the user first
                            'Me.mconSqlMaster = Functions.BuildMasterConnection

                            'Dim sysUtil As New Accucentric.docAssist.Data.Images.SystemUtilities(Me.mconSqlMaster)
                            'Dim strMessageId As String
                            'Dim strPage As String = sysUtil.LoginMessageGet(Me.mobjUser.UserId.ToString, strMessageId)

                            'If Not IsNothing(strPage) Then
                            '    Response.Redirect(strPage & "?ID=" & strMessageId)
                            'End If


                            Select Case Me.mobjUser.DefaultStartPage
                                Case 0
                                    Response.Redirect("Search.aspx", False)
                                Case 1
                                    Response.Redirect("DocumentManager.aspx")
                                Case 3
                                    Response.Redirect("WorkflowDashboard.aspx")
                                Case Else
                                    Response.Redirect("Search.aspx")
                            End Select
                        End If
                    End If

                End If
            End If
        End If

        Dim intDiff As Integer

        Dim objDecrypt As Encryption.Encryption

        lblErrorMessage.Text = ""
        lblErrorMessage.Visible = False

        Session("blnAdminOnlyMode") = False

        mconSqlMaster = BuildMasterConnection()

        Try
            'Cookie the time zone difference
            If txtTimeDiff.Value <> "" Then
                intDiff = (CInt(txtTimeDiff.Value) / 60) * -1
            End If

            ' Is there an e-mail address
            If txtEMailAddress.Text.Trim.Length = 0 Then
                lblErrorMessage.Text += "Please enter an e-mail address."
                lblErrorMessage.Visible = True
                Exit Sub
            End If

            ' Valid e-mail
            Dim regExEmail As New System.Text.RegularExpressions.Regex("\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*")
            Dim matchExEmail As System.Text.RegularExpressions.Match = regExEmail.Match(txtEMailAddress.Text.Trim)
            If Not matchExEmail.Success Then
                lblErrorMessage.Text += "A valid e-mail address is required."
                lblErrorMessage.Visible = True
                Exit Sub
            End If

            ' Is there a password
            If txtPassword.Text.Trim.Length = 0 Then
                lblErrorMessage.Text += "Please enter a password."
                lblErrorMessage.Visible = True
                Exit Sub
            End If

            ' Validate the user
#If DEBUG Then

#Else

            If Request.Url.ToString.StartsWith("http://dev.docassist") = False And Request.Url.ToString.StartsWith("https://stage.docassist") = False Then
                If Not Request.UserAgent.IndexOf("MSIE") > -1 Then
                    lblErrorMessage.Text = "<BR>" & "You must be using Microsoft Internet Explorer."
                    lblErrorMessage.Visible = True
                    Functions.LogAuthentication(Nothing, Nothing, txtEMailAddress.Text, Request.UserHostAddress, Server.MachineName, "Application", "Unsupported browser", Left(Request.UserAgent.ToString, 2500))
                    Exit Sub
                End If
            End If
#End If


            ' Check for errors
            If lblErrorMessage.Text.Length > 0 Then Throw New Exception("Control Errors")

            ''Before validating the user check if the account is locked out
            'Try
            '    Dim dtLockout As New DataTable

            '    Dim objCmdWeb As New SqlClient.SqlCommand("SELECT FailedLogin FROM Users WHERE EMailAddress = @EMailAddress", mconSqlMaster)
            '    Dim daLockout As New SqlClient.SqlDataAdapter(objCmdWeb)

            '    objCmdWeb.Parameters.Add("@EMailAddress", txtEMailAddress.Text)
            '    objCmdWeb.CommandType = CommandType.Text
            '    daLockout.Fill(dtLockout)

            '    If dtLockout.Rows.Count > 0 Then
            '        Dim intAttempts As Integer = dtLockout.Rows(0)(0)
            '        If intAttempts >= intLockout Then
            '            lblErrorMessage.Text = "<BR>" & "Account has been locked out. Contact your administrator to enable it."
            '            lblErrorMessage.Visible = True
            '            Functions.SignOut(Me, True)
            '            Exit Sub
            '        End If
            '    Else
            '        'wrong username too
            '    End If

            'Catch ex As Exception

            'End Try

            Try
                Dim dtPasswordExpires
                Dim guidSessionId As System.Guid
                Dim blnGenerateNewSession As Boolean = True

                If Not IsNothing(Request.QueryString("Session")) Then
                    If Request.QueryString("Session").Replace(" ", "+").Replace("�", "/") = objDecrypt.Encrypt(System.DateTime.Now.Month & "/" & System.DateTime.Now.Day & "/" & System.DateTime.Now.Year) Then
                        blnGenerateNewSession = False
                    Else
                        lblErrorMessage.Text = "Invalid session identifier."
                        lblErrorMessage.Visible = True
                        Exit Sub
                    End If

                End If

                Try
                    If Not IsNothing(Request.Cookies(txtEMailAddress.Text)) Then
                        guidSessionId = New System.Guid(Request.Cookies(txtEMailAddress.Text)("SessionId").ToString)
                        Me.mobjUser = New SECURITY.User(txtEMailAddress.Text, txtPassword.Text, ConfigurationSettings.AppSettings("Connection"), dtPasswordExpires, guidSessionId, promptWithTerms, blockIpAddress, True, , intDiff, False)
                    Else
                        guidSessionId = New System.Guid
                        Me.mobjUser = New SECURITY.User(txtEMailAddress.Text, txtPassword.Text, ConfigurationSettings.AppSettings("Connection"), dtPasswordExpires, guidSessionId, promptWithTerms, blockIpAddress, Request.UserHostAddress, True, intDiff, blnGenerateNewSession)
                    End If

                    Session("EditionID") = Me.mobjUser.EditionID

                    If blockIpAddress Then
                        lblErrorMessage.Text = "* You are not authorized to login from this IP address. Contact your administrator for access."
                        lblErrorMessage.Visible = True
                        Functions.LogAuthentication(Nothing, Nothing, txtEMailAddress.Text, Request.UserHostAddress, Server.MachineName, "Application", "Invalid IP Range", Left(Request.UserAgent.ToString, 2500))
                        Functions.SignOut(Me, False)
                        Exit Sub
                    End If

                    'Successful authentication
                    Functions.LogAuthentication(Me.mobjUser.AccountId.ToString, Me.mobjUser.UserId.ToString, Me.mobjUser.EMailAddress.ToString, Request.UserHostAddress, Server.MachineName, "Application", "Success", Left(Request.UserAgent.ToString, 2500))

                Catch ex As Exception

                    'Failed authentication
                    Functions.LogAuthentication(Nothing, Nothing, txtEMailAddress.Text, Request.UserHostAddress, Server.MachineName, "Application", "Invalid Password", Left(Request.UserAgent.ToString, 2500))
                    Throw ex

                Finally

                    Response.Cookies(SESSION_COOKIE_KEY)(SESSION_COOKIE) = Now.AddMinutes(Constants.SESSION_TIMEOUT_MINUTES)
                    Response.Cookies(SESSION_COOKIE_KEY).Expires = Now.AddDays(100)

                    Response.Cookies(SINGLE_SESSION_COOKIE)(SINGLE_SESSION_COOKIE_KEY) = True
                    Request.Cookies.Add(Request.Cookies(SINGLE_SESSION_COOKIE))

                    'Response.Cookies(SINGLE_SESSION_COOKIE).Expires = Now.AddYears(-10)

                End Try

                Response.Cookies(txtEMailAddress.Text)("SessionId") = guidSessionId.ToString
                Response.Cookies(txtEMailAddress.Text).Expires = Date.MaxValue

                mconSqlImage = Functions.BuildConnection(Me.mobjUser)
                If mconSqlImage.State <> ConnectionState.Open Then mconSqlImage.Open()

                If TrialExpired() = True Then
                    If Me.mobjUser.EditionID = AccountEdition.SMALL_BUSINESS_EDITION Then

                    Else
                        Throw New Exception(" Your trial period has expired.")
                    End If
                End If

                'Check to see if password is expired
                If ddlAccounts.SelectedValue.Trim = "" Then
                    Session("glbnPasswordExpired") = False
                    If dtPasswordExpires <= Now() Then
                        Session("glbnPasswordExpired") = True
                    End If
                End If

                'If user selected an account populate database
                If ddlAccounts.SelectedValue.Trim <> "" Then
                    Dim sqlCmd As New SqlClient.SqlCommand("acsSetAccountDefaultByUserId")
                    sqlCmd.CommandType = CommandType.StoredProcedure
                    sqlCmd.Parameters.Add("@UserID", Me.mobjUser.UserId)
                    sqlCmd.Parameters.Add("@AccountID", ddlAccounts.SelectedValue)
                    sqlCmd.Connection = mconSqlMaster
                    sqlCmd.ExecuteNonQuery()
                End If

                If Me.mobjUser.UserId.ToString <> Me.mobjUser.UserId.Empty.ToString Then
                    'Reset lockout count
                    Try
                        Dim objEncryption As New Encryption.Encryption
                        Dim objCmdWeb As New SqlClient.SqlCommand("UPDATE Users SET FailedLogin = @FailedLogin WHERE EMailAddress = @EMailAddress", mconSqlMaster)
                        objCmdWeb.CommandText = "UPDATE Users SET FailedLogin = @FailedLogin WHERE EMailAddress = @EmailAddress"
                        objCmdWeb.Parameters.Add("@FailedLogin", "0")
                        objCmdWeb.Parameters.Add("@EmailAddress", txtEMailAddress.Text)
                        objCmdWeb.ExecuteNonQuery()
                    Catch ex As Exception

                    End Try

                    Response.Cookies("docAssist")("UserId") = guidSessionId.ToString 'Me.mobjUser.SessionID.ToString
                    Response.Cookies("docAssist")("AccountId") = Me.mobjUser.AccountId.ToString

                    If promptWithTerms Then
                        Session("Terms") = "1"
                    End If

                    If chkRemember.Checked Then
                        Dim cookieDocAssist As System.Web.HttpCookie = Request.Cookies("docAssist")
                        If Not cookieDocAssist Is Nothing Then cookieDocAssist.Expires = Now().AddMinutes(SESSION_KEEP_SIGNED_IN_DURATION_MINUTES)

                        Dim cook1 As New HttpCookie("docAssist")
                        cook1.Expires = Now.AddDays(1)
                        cook1.Values.Add("UserId", Me.mobjUser.SessionID.ToString)
                        cook1.Values.Add("AccountId", Me.mobjUser.AccountId.ToString)
                        Response.Cookies.Add(cook1)
                        Response.Cookies(SESSION_COOKIE_KEY)("RememberLogin") = True
                        Response.Cookies(SESSION_COOKIE_KEY).Expires = Now.AddDays(100)
                    Else
                        Dim cook1 As New HttpCookie("docAssist")
                        cook1.Values.Add("UserId", guidSessionId.ToString)
                        cook1.Values.Add("AccountId", Me.mobjUser.AccountId.ToString)
                        Response.Cookies.Add(cook1)
                        Response.Cookies(SESSION_COOKIE_KEY)("RememberLogin") = False
                        Response.Cookies(SESSION_COOKIE_KEY).Expires = Now.AddDays(100)
                    End If

                    Response.Cookies(SINGLE_SESSION_COOKIE)(SINGLE_SESSION_COOKIE_KEY) = True
                    'Response.Cookies(SINGLE_SESSION_COOKIE).Expires = Now.AddYears(-10)
                    Request.Cookies.Add(Request.Cookies(SINGLE_SESSION_COOKIE))


                    Response.Cookies("docAssistImage").Expires = Now().AddDays(-1)

                    ' Store the EMail Address
                    Dim cookieSave As HttpCookie
                    cookieSave = New HttpCookie("docAssistSave")
                    cookieSave.Expires = DateAdd(DateInterval.Day, 365, Now())
                    cookieSave("LastLogon") = txtEMailAddress.Text
                    Response.Cookies.Add(cookieSave)

                    'Integration launcher
                    'Session("strIntegrationCommandLine") = IntegrationV2()
                    Try
                        Session("strIntegrationCommandLine") = IntegrationV3()
                    Catch ex As Exception

                    End Try

                    'User is validated, check if we are going to perform a search from the button on the app.
                    Dim blnButtonSearch As Boolean = False
                    Dim blnLoadByVersionID As Boolean = False
                    If strDocumentID <> "" And strAttributeID <> "" And strAttributeValue <> "" Then
                        blnButtonSearch = True
                    End If

                    If Session("iVersionID") <> "" Then
                        blnLoadByVersionID = True
                        Session("iVersionID") = ""
                    End If

                    Dim iQueryLength As Integer
                    If Not IsNothing(strPrintDriverFile) Then
                        If blnButtonSearch = True Or blnLoadByVersionID = True Or strPrintDriverFile.Trim.Length > 0 Then
                            iQueryLength = Request.Url.AbsolutePath.Length + Request.RawUrl.Substring(Request.RawUrl.IndexOf("?")).Length
                        Else
                            iQueryLength = Request.Url.AbsolutePath.Length
                        End If
                    End If

                    '
                    ' Check for the viewer control installation
                    '
                    Dim sbURL As New System.Text.StringBuilder
                    sbURL.Append("<script language='javascript'>location.replace('")
                    sbURL.Append(Request.Url.AbsoluteUri.Substring(0, Request.Url.AbsoluteUri.Length - iQueryLength))
                    '#If DEBUG Then
                    '                    sbURL.Append("/docAssistWeb1")
                    '#End If

                    'Load from application integration button
                    If blnButtonSearch = True Then
                        sbURL.Append("/Index.aspx" & Request.RawUrl.Substring(Request.RawUrl.IndexOf("?")))
                        sbURL.Append("')</script>")
                        litRedirect.Text = sbURL.ToString
                        Exit Sub
                    End If

                    'Load by versionid
                    If blnLoadByVersionID = True Then
                        sbURL.Append("/Index.aspx" & Request.RawUrl.Substring(Request.RawUrl.IndexOf("?")))
                        sbURL.Append("')</script>")
                        litRedirect.Text = sbURL.ToString
                        Exit Sub
                    End If

                    'Check if user has access to any documents
                    Dim sqlCmd As SqlClient.SqlCommand
                    Dim oDb As New Accucentric.docAssist.Data.Images.db
                    'Dim dtDocuments As New Accucentric.docAssist.Data.Images.Documents

                    sqlCmd = New SqlClient.SqlCommand("acsUserAccess", mconSqlImage)
                    sqlCmd.CommandType = CommandType.StoredProcedure
                    sqlCmd.Parameters.Add("@AccountID", Me.mobjUser.AccountId)
                    sqlCmd.Parameters.Add("@UserID", Me.mobjUser.UserId)

                    Dim dtAccess As New DataTable


                    oDb.PopulateTable(dtAccess, sqlCmd)
                    If dtAccess.Rows(0)("Workflow") = 0 And dtAccess.Rows(0)("Search") = 0 And dtAccess.Rows(0)("Index") = 0 Then

                        'Check for administrator
                        If Me.mobjUser.SecAdmin = True Or Me.mobjUser.SysAdmin = True Then
                            Session("blnAdminOnlyMode") = True
                            sbURL.Append("/UserAdmin.aspx")
                        Else
                            lblErrorMessage.Text = "No access to any documents. Contact your administrator."
                            lblErrorMessage.Visible = True
                            Functions.SignOut(Me, False)
                            Exit Sub
                        End If

                    Else 'If they do have access, process normal check

                        Dim intWf As Integer = dtAccess.Rows(0)("Workflow")
                        Dim intSearch As Integer = dtAccess.Rows(0)("Search")
                        Dim intIndexer As Integer = dtAccess.Rows(0)("Index")

                        If Request.Url.Query.Trim.Length > 0 And Request.RawUrl.IndexOf("ErrId") = -1 And IsNothing(Request.QueryString("Session")) Then
                            'Where are we supposed to go
                            If Not IsNothing(Request.QueryString("A")) Then
                                Response.Redirect("Integration.aspx" & Request.Url.Query)
                            End If

                            If Not IsNothing(Request.QueryString("PURL")) Then
                                Response.Redirect("DocumentManager.aspx" & Request.Url.Query)
                            End If

                            If Not IsNothing(Request.QueryString("File")) Then
                                Response.Redirect("DocumentManager.aspx" & Request.Url.Query)
                            End If

                            If Not IsNothing(Request.QueryString("VID")) Then
                                Response.Redirect("Index.aspx" & Request.Url.Query)
                            End If

                            If Not IsNothing(Request.QueryString("loadversion")) Then
                                Response.Redirect("Search.aspx" & Request.Url.Query)
                            End If

                            If Not IsNothing(Request.QueryString("u")) Then
                                Response.Redirect("Search.aspx")
                            End If

                            If Not IsNothing(Request.QueryString("Action")) Then
                                Select Case Request.QueryString("Action")
                                    Case "WFDash"
                                        Response.Redirect("WorkflowDashboard.aspx")
                                End Select
                            End If

                            If Not IsNothing(Request.QueryString("LID")) Then
                                Select Case Me.mobjUser.DefaultStartPage
                                    Case 0
                                        Response.Redirect("Search.aspx", False)
                                    Case 1
                                        Response.Redirect("DocumentManager.aspx")
                                    Case 3
                                        Response.Redirect("WorkflowDashboard.aspx")
                                    Case Else
                                        Response.Redirect("Search.aspx")
                                End Select
                            End If

                            Exit Sub

                        Else
                            If intWf = -1 And intSearch = 0 And intIndexer = 0 Then
                                'WF Only user
                                Response.Redirect("WorkflowDashboard.aspx")
                            Else
                                If Request.QueryString("PURL") <> "" Then
                                    'Response.Redirect("Default.aspx" & Request.Url.Query)
                                    Response.Redirect("/DocumentManager.aspx" & Request.Url.Query)
                                Else
                                    Select Case Me.mobjUser.DefaultStartPage
                                        Case 0
                                            Response.Redirect("Search.aspx", False)
                                        Case 1
                                            Response.Redirect("DocumentManager.aspx")
                                        Case 3
                                            Response.Redirect("WorkflowDashboard.aspx")

                                        Case Else
                                            Response.Redirect("Search.aspx")
                                    End Select

                                End If
                            End If
                        End If

                    End If

                    sbURL.Append("')</script>")

                    litRedirect.Text = sbURL.ToString

                Else

                    'Record failed login attempt
                    Try
                        Dim objEncryption As New Encryption.Encryption
                        Dim objCmdWeb As New SqlClient.SqlCommand("SELECT FailedLogin FROM Users WHERE EMailAddress = @EMailAddress", mconSqlMaster)
                        objCmdWeb.Parameters.Add("@EMailAddress", txtEMailAddress.Text)
                        objCmdWeb.CommandType = CommandType.Text
                        Dim dtLockout As New DataTable
                        Dim daLockout As New SqlClient.SqlDataAdapter(objCmdWeb)
                        daLockout.Fill(dtLockout)

                        If dtLockout.Rows.Count > 0 Then
                            Dim intAttempt As Integer = dtLockout.Rows(0)(0)
                            If (intAttempt + 1) = intLockout Then
                                'Lock out account
                                objCmdWeb.Parameters.Clear()
                                objCmdWeb.CommandText = "UPDATE Users SET Enabled = @Enabled, FailedLogin = @FailedLogin, AccountLockedDateTime = @AccountLockedDateTime WHERE EMailAddress = @EmailAddress"
                                objCmdWeb.Parameters.Add("@Enabled", "0")
                                objCmdWeb.Parameters.Add("@FailedLogin", intAttempt + 1)
                                objCmdWeb.Parameters.Add("@EmailAddress", txtEMailAddress.Text)
                                objcmdweb.Parameters.Add("@AccountLockedDateTime", Now)
                                objCmdWeb.ExecuteNonQuery()
                            Else
                                'Record fail
                                objCmdWeb.Parameters.Clear()
                                objCmdWeb.CommandText = "UPDATE Users SET FailedLogin = @FailedLogin WHERE EMailAddress = @EmailAddress"
                                objCmdWeb.Parameters.Add("@FailedLogin", intAttempt + 1)
                                objCmdWeb.Parameters.Add("@EmailAddress", txtEMailAddress.Text)
                                objCmdWeb.ExecuteNonQuery()
                            End If
                        Else
                            'wrong username too
                        End If

                    Catch ex As Exception

                    End Try

                    lblErrorMessage.Text = "Invalid e-mail address or password."
                    lblErrorMessage.Visible = True
                End If
            Catch ex As Exception
                lblErrorMessage.Text = ex.Message
                Throw ex
            End Try

        Catch ex As Exception
            lblErrorMessage.Visible = True
        Finally
            If mconSqlImage.State <> ConnectionState.Closed Then mconSqlImage.Close()
            mconSqlImage.Dispose()
            If mconSqlMaster.State <> ConnectionState.Closed Then mconSqlMaster.Close()
            mconSqlMaster.Dispose()
        End Try

    End Sub

    Private Function Integration() As String
        'Invoke integration
        'Me.mobjUser = BuildUserObject(Request.Cookies("docAssist"))
        Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)

        If Not Me.mconSqlImage.State Then
            Me.mconSqlImage.Open()
        End If

        Dim sqlCommand As New SqlClient.SqlCommand("acsIntegrationMapData", Me.mconSqlImage)
        sqlCommand.CommandType = CommandType.StoredProcedure
        sqlCommand.Parameters.Add("@UserId", Me.mobjUser.ImagesUserId)
        Dim daSql As New SqlClient.SqlDataAdapter
        daSql.SelectCommand = sqlCommand

        Dim dsIntegration As New DataSet
        daSql.Fill(dsIntegration)

        Dim charField As String = "|"
        Dim charRow As String = "^"

        If dsIntegration.Tables.Count > 1 Then

            Dim sbHeader As New System.Text.StringBuilder
            Dim dr As DataRow

            Dim intHeaderCount As Integer = 0

            'Header
            sbHeader.Append("docAssistMonitor.exe /F ")
            For Each dr In dsIntegration.Tables(0).Rows
                sbHeader.Append(dr("IntegrationID") & charField)
                sbHeader.Append(dr("WIndowName") & charField)
                sbHeader.Append(dr("WindowClass") & charField)
                'sbHeader.Append(dr("DocumentID"))
                sbHeader.Append(charRow)

            Next

            sbHeader.Append(" /M ")


            For Each dr In dsIntegration.Tables(1).Rows
                sbHeader.Append(dr("IntegrationID") & charField)
                sbHeader.Append(dr("AttributeID") & charField)
                sbHeader.Append(dr("DocumentID") & charField)
                sbHeader.Append(dr("DocumentName") & ">")
                sbHeader.Append(dr("AttributeName") & charField)
                sbHeader.Append(dr("BaseUrl") & charField)
                sbHeader.Append(dr("PosLeft") & charField)
                sbHeader.Append(dr("PosTop") & charField)
                sbHeader.Append(dr("Width") & charField)
                sbHeader.Append(dr("Height"))
                If dr("Expression") <> "" Then
                    sbHeader.Append(charField)
                    sbHeader.Append(GenerateScript(dr("Expression")))
                End If
                sbHeader.Append(charRow)
            Next

            If dsIntegration.Tables.Count = 2 And dsIntegration.Tables(0).Rows.Count > 0 And dsIntegration.Tables(1).Rows.Count > 0 Then
                Return sbHeader.ToString
            Else
                Return ""
            End If

        Else
            Return ""
        End If


    End Function

    Private Function IntegrationV2() As String
        'Invoke integration
        'Me.mobjUser = BuildUserObject(Request.Cookies("docAssist"))
        Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)

        If Not Me.mconSqlImage.State Then
            Me.mconSqlImage.Open()
        End If

        Dim sqlCommand As New SqlClient.SqlCommand("acsIntegrationData", Me.mconSqlImage)
        sqlCommand.CommandType = CommandType.StoredProcedure
        sqlCommand.Parameters.Add("@UserId", Me.mobjUser.ImagesUserId)
        Dim daSql As New SqlClient.SqlDataAdapter
        daSql.SelectCommand = sqlCommand

        Dim dsIntegration As New DataSet
        daSql.Fill(dsIntegration)

        Dim charField As String = "|"
        Dim charRow As String = "^"

        If dsIntegration.Tables.Count > 1 Then

            Dim sbHeader As New System.Text.StringBuilder
            Dim dr As DataRow

            Dim intHeaderCount As Integer = 0

            'Header
            sbHeader.Append("docAssistMonitor.exe /F ")
            For Each dr In dsIntegration.Tables(0).Rows
                sbHeader.Append(dr("IntegrationID") & charField)
                sbHeader.Append(dr("WIndowName") & charField)
                sbHeader.Append(dr("WindowClass") & charField)
                sbHeader.Append(charRow)

            Next

            sbHeader.Append(" /M ")


            For Each dr In dsIntegration.Tables(1).Rows
                sbHeader.Append(dr("ActionType") & charField)
                sbHeader.Append(dr("IntegrationID") & charField)
                sbHeader.Append(dr("FieldID") & charField)
                sbHeader.Append(dr("DocumentID") & charField)
                If dr("ActionType") = "S" Then
                    sbHeader.Append(dr("DocumentName") & ">")
                    sbHeader.Append(dr("AttributeName") & charField)
                End If

                If dr("ActionType") = "I" Then
                    sbHeader.Append(dr("DocumentName") & charField)
                End If

                sbHeader.Append(dr("BaseUrl") & charField)
                sbHeader.Append(dr("PosLeft") & charField)
                sbHeader.Append(dr("PosTop") & charField)
                sbHeader.Append(dr("PosWidth") & charField)
                sbHeader.Append(dr("PosHeight"))
                'If dr("Expression") <> "" Then
                ' sbHeader.Append(charField)
                ' sbHeader.Append(GenerateScript(dr("Expression")))
                'End If
                sbHeader.Append(charRow)
            Next

            If dsIntegration.Tables.Count = 2 And dsIntegration.Tables(0).Rows.Count > 0 And dsIntegration.Tables(1).Rows.Count > 0 Then

                If Server.MachineName = "DOC-ABARBERIS" Then
                    Return sbHeader.ToString.Replace("dev.docassist.com", "localhost")
                Else
                    Return sbHeader.ToString
                End If

            Else
                Return ""
            End If

        Else
            Return ""
        End If


    End Function

    Private Function IntegrationV3() As String
        'Invoke integration
        'Me.mobjUser = BuildUserObject(Request.Cookies("docAssist"))
        Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)

        If Not Me.mconSqlImage.State Then
            Me.mconSqlImage.Open()
        End If

        Dim sqlCommand As New SqlClient.SqlCommand("acsIntegrationData", Me.mconSqlImage)
        sqlCommand.CommandType = CommandType.StoredProcedure
        sqlCommand.Parameters.Add("@UserId", Me.mobjUser.ImagesUserId)
        Dim daSql As New SqlClient.SqlDataAdapter
        daSql.SelectCommand = sqlCommand

        Dim dsIntegration As New DataSet
        daSql.Fill(dsIntegration)

        Dim charField As String = "|"
        Dim charRow As String = "^"

        If dsIntegration.Tables.Count > 1 Then

            Dim sbHeader As New System.Text.StringBuilder
            Dim dr As DataRow

            Dim intHeaderCount As Integer = 0

            'Header
            sbHeader.Append("docAssistMonitor.exe /F ")
            For Each dr In dsIntegration.Tables(0).Rows
                sbHeader.Append(dr("IntegrationID") & charField)
                sbHeader.Append(dr("WIndowName") & charField)
                sbHeader.Append(dr("WindowClass") & charField)
                sbHeader.Append("0")
                sbHeader.Append(charRow)

            Next

            sbHeader.Append(" /M ")


            For Each dr In dsIntegration.Tables(1).Rows
                sbHeader.Append(dr("ActionType") & charField)
                sbHeader.Append(dr("IntegrationID") & charField)
                sbHeader.Append(dr("FieldID") & charField)
                sbHeader.Append(dr("DocumentID") & charField)
                If dr("ActionType") = "S" Then
                    sbHeader.Append(dr("DocumentName") & ">")
                    sbHeader.Append(dr("AttributeName") & charField)
                End If

                If dr("ActionType") = "I" Then
                    sbHeader.Append(dr("DocumentName") & charField)
                End If

                sbHeader.Append(dr("BaseUrl") & charField)
                sbHeader.Append(dr("PosLeft") & charField)
                sbHeader.Append(dr("PosTop") & charField)
                sbHeader.Append(dr("PosWidth") & charField)
                sbHeader.Append(dr("PosHeight") & charField)
                sbHeader.Append(dr("AttributeName"))
                'If dr("Expression") <> "" Then
                ' sbHeader.Append(charField)
                ' sbHeader.Append(GenerateScript(dr("Expression")))
                'End If
                sbHeader.Append(charRow)
            Next

            If dsIntegration.Tables.Count = 2 And dsIntegration.Tables(0).Rows.Count > 0 And dsIntegration.Tables(1).Rows.Count > 0 Then
                Return sbHeader.ToString
            Else
                Return ""
            End If

        Else
            Return ""
        End If


    End Function
    Private Function GenerateScript(ByVal mExpression As String) As String
        Dim strScript As String

        strScript = "Function VBSParse(param)" & Chr(10) & _
              "Dim Result" & Chr(10) & _
          "Result = " & mExpression & Chr(10) & _
              "VBSParse = Result" & Chr(10) & _
          "End Function"
        strScript = Base64Encode(strScript)
        Return strScript
    End Function

    Function ReplicateString(ByVal n As Integer, ByVal s As String)
        Dim sb As New System.Text.StringBuilder(Len(s) * n)
        For i As Integer = 1 To n
            sb.Append(s)
        Next
        Return sb.ToString()

    End Function
    Function Base64Encode(ByVal inData)
        'rfc1521
        '2001 Antonin Foller, Motobit Software, http://Motobit.cz
        Const Base64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
        Dim cOut, sOut, I

        'For each group of 3 bytes
        For I = 1 To Len(inData) Step 3
            Dim nGroup, pOut, sGroup

            'Create one long from this 3 bytes.
            nGroup = &H10000 * Asc(Mid(inData, I, 1)) + _
              &H100 * MyASC(Mid(inData, I + 1, 1)) + MyASC(Mid(inData, I + 2, 1))

            'Oct splits the long To 8 groups with 3 bits
            nGroup = Oct(nGroup)

            'Add leading zeros
            nGroup = ReplicateString(8 - Len(nGroup), "0") & nGroup

            'Convert To base64
            pOut = Mid(Base64, CLng("&o" & Mid(nGroup, 1, 2)) + 1, 1) + _
              Mid(Base64, CLng("&o" & Mid(nGroup, 3, 2)) + 1, 1) + _
              Mid(Base64, CLng("&o" & Mid(nGroup, 5, 2)) + 1, 1) + _
              Mid(Base64, CLng("&o" & Mid(nGroup, 7, 2)) + 1, 1)

            'Add the part To OutPut string
            sOut = sOut + pOut

            'Add a new line For Each 76 chars In dest (76*3/4 = 57)
            'If (I + 2) Mod 57 = 0 Then sOut = sOut + vbCrLf
        Next
        Select Case Len(inData) Mod 3
            Case 1 '8 bit final
                sOut = Left(sOut, Len(sOut) - 2) + "=="
            Case 2 '16 bit final
                sOut = Left(sOut, Len(sOut) - 1) + "="
        End Select
        Base64Encode = sOut
    End Function

    Function MyASC(ByVal OneChar)
        If OneChar = "" Then MyASC = 0 Else MyASC = Asc(OneChar)
    End Function

    Private Sub btnSignOut_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSignOut.Click
        Functions.SignOut(Me, True)
    End Sub

    Private Function TrialExpired() As Boolean

        Session("TrialActive") = 0
        Session("TrialDaysRemaining") = 0

        Dim conSqlMaster As SqlClient.SqlConnection = Functions.BuildMasterConnection
        'Check Account Trial Status
        Dim cmdSqlTrial As New SqlClient.SqlCommand
        Dim dtTrial As New DataTable("Trial")

        cmdSqlTrial.CommandType = CommandType.StoredProcedure
        cmdSqlTrial.CommandText = "ACTGAccountStatus"
        cmdSqlTrial.Connection = conSqlMaster

        cmdSqlTrial.Parameters.Clear()
        cmdSqlTrial.Parameters.Add(New SqlClient.SqlParameter("@AccountID", Me.mobjUser.AccountId))

        Dim sqlda As New SqlClient.SqlDataAdapter(cmdSqlTrial)
        sqlda.Fill(dtTrial)

        If dtTrial.Rows.Count > 0 Then
            Dim iAccountMode As Integer = dtTrial.Rows(0)("AccountMode")
            Dim iTrialDaysRemaining As Integer = dtTrial.Rows(0)("TrialDaysRemaining")
            If iAccountMode = 1 Then
                Session("TrialActive") = 1
                Session("TrialDaysRemaining") = iTrialDaysRemaining
            End If

            If iAccountMode = 2 Then 'Trial EXPIRED
                Session("TrialActive") = 2
                Session("TrialDaysRemaining") = 0
                Throw New Exception("Trial period has expired. Please contact docAssist at 877-399-1100.")
            End If
        End If

        conSqlMaster.Close()
        conSqlMaster.Dispose()

    End Function

    Private Sub chkRemember_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkRemember.CheckedChanged

        Try

            'Validate name

            'Validate credit card number


            'Visa: ^4[0-9]{12}(?:[0-9]{3})?$ All Visa card numbers start with a 4. New cards have 16 digits. Old cards have 13.
            'MasterCard: ^5[1-5][0-9]{14}$ All MasterCard numbers start with the numbers 51 through 55. All have 16 digits.
            'American Express: ^3[47][0-9]{13}$ American Express card numbers start with 34 or 37 and have 15 digits.


        Catch ex As Exception

        End Try

    End Sub

End Class
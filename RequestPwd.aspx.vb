Partial Class RequestPwd
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
    End Sub

  
    Private Sub lnkSend_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles lnkSend.Click

        Dim strConnection As String = ConfigurationSettings.AppSettings("Connection")
        Dim conSql As New SqlClient.SqlConnection(Functions.ConnectionString())
        Dim reader As SqlClient.SqlDataReader
        Try

            Dim cmdSql As New SqlClient.SqlCommand("SELECT password FROM Users WHERE EmailAddress = @EMailAddress", conSql)
            cmdSql.Parameters.Add("@EMailAddress", Me.txtEMailAddress.Text)

            conSql.Open()
            reader = cmdSql.ExecuteReader
            If reader.Read Then
                Dim sbBody As New System.Text.StringBuilder
                sbBody.Append("<FONT FACE=Arial SIZE=2>Dear docAssist user,<BR><BR>")
                sbBody.Append("Your docAssist password is: <B>{0}</B><BR><BR>")

                sbBody.Append("<A href=""http://")
#If DEBUG Then
                sbBody.Append("dev.docassist.com:81/docAssistWeb1/Default.aspx")
#Else
                sbbody.Append("my.docassist.com")
#End If
                sbBody.Append(""">Click here to login to docAssist.</A><BR><BR>")

                'sbBody.Append("<P>Your password to the docAssist system is:</P>")
                'sbBody.Append("<P><B>{0}</B></P>")
                sbBody.Append("Please note that your docAssist password is case sensitive.<BR><BR>")
                sbBody.Append("After you regain access to your docAssist account, please make sure to change your password to a more complex one. ")
                sbBody.Append("We strongly recommend you use an 8-digit password combination of letters and numbers (example: 95BLuE33)<BR><BR>")

                sbBody.Append("To change your password follow these steps:<BR><BR>")
                sbBody.Append("1. Login to your docAssist account and click the 'Preferences' link located in the upper right corner of your screen.<BR>")
                sbBody.Append("2. Click the 'Change Password' button.<BR>")
                sbBody.Append("3. Enter your old password and confirm your new password.<BR><BR>")

                sbBody.Append("docAssist Support Team<BR><BR>")

                sbBody.Append("Reminder: docAssist will never ask you to submit your password by email. Should you receive any email appearing to be ")
                sbBody.Append("from docAssist that asks for your password, please forward it to support@docAsssist.com and delete it.")

                Dim enc As New Encryption.Encryption
                Dim strPassword As String = enc.Decrypt(reader("Password"))

                Dim objMail As New SendMail.Smtp
                objMail.from = "support@docassist.com"
                objMail.displayname = "docAssist Support"
                objMail.subject = "docAssist Password Request"
                objMail.to.Add(txtEMailAddress.Text)
                'objMail.bcc.Add(ConfigurationSettings.AppSettings("PasswordCopyBox"))
                objMail.bodyHtml = True
                objMail.bodyText = String.Format(sbBody.ToString, strPassword)
                objMail.server = ConfigurationSettings.AppSettings("MailServer")
                objMail.serverport = ConfigurationSettings.AppSettings("MailServerPort")
                objMail.Send()
                objMail = Nothing

                lblResults.Text = "An email has been sent to " & Me.txtEMailAddress.Text & " containing your login information.<BR><BR><A href=""Default.aspx"">Click here to return to the Login page.</A>"
                lblResults.Visible = True

            Else
                lblResults.Text = "An email has been sent to " & Me.txtEMailAddress.Text & " containing your login information.<BR><BR><A href=""Default.aspx"">Click here to return to the Login page.</A>"
                lblResults.Visible = True
            End If

        Catch exMail As SendMail.SmtpException
            lblResults.Text = "An error occurred while trying to send the request.  Please try again later."
            lblResults.CssClass = "Red"
            lblResults.Visible = True
        Catch ex As Exception
#If DEBUG Then
            lblResults.Text = ex.Message
#Else
            lblResults.Text = "An error occurred while processing this request.  Please try again later, or contact your account administrator"
#End If
            lblResults.CssClass = "Red"
            lblResults.Visible = True
        Finally
            If Not reader.IsClosed Then reader.Close()
            If Not conSql.State = ConnectionState.Closed Then conSql.Close()
        End Try

    End Sub

End Class

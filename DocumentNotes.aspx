<%@ Page Language="vb" AutoEventWireup="false" Codebehind="DocumentNotes.aspx.vb" Inherits="docAssistWeb.DocumentNotes"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>docAssist - Document Notes</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr height="10">
					<td colSpan="2"><BR>
						&nbsp;&nbsp;&nbsp; <IMG src="Images/document_notes.gif"></td>
				</tr>
				<tr>
					<td><IMG height="1" src="dummy.gif" width="15"></td>
					<td valign="top" height="100%">
						<iframe src="Notes.aspx" height="400" width="500" style="BORDER-RIGHT: #cccccc thin solid; BORDER-TOP: #cccccc thin solid; BORDER-LEFT: #cccccc thin solid; BORDER-BOTTOM: #cccccc thin solid"
							frameborder="0" runat="server" id="ifrmNotes">
							<asp:label id="lblNotes" Font-Names="Verdana" Font-Size="8pt"></asp:label></iframe>
						<BR>
						&nbsp;
						<asp:Label id="Label1" runat="server" CssClass="PageContent">Enter Note:</asp:Label>
						<BR>
						<asp:textbox id="txtNote" runat="server" Width="503px" Height="80" TextMode="MultiLine" Font-Size="8pt"
							Font-Names="Verdana"></asp:textbox><BR>
						<asp:imagebutton id="btnSave" runat="server" ImageUrl="Images/btn_save.gif"></asp:imagebutton>&nbsp;&nbsp;<a href="javascript:window.close();"><img src="Images/btn_cancel.gif" border="0"></a>
						<asp:Label id="lblErrorMsg" runat="server" Font-Size="8pt" Font-Names="Verdana" ForeColor="Red"></asp:Label>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>

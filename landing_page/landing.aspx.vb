Imports Accucentric.docAssist

Partial Class landing
    Inherits System.Web.UI.Page

    Private mobjUser As Web.Security.User
    Private mconSqlCompany As SqlClient.SqlConnection


#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try

            If Not Page.IsPostBack Then

                Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
                Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))
                Me.mobjUser.AccountId = guidAccountId
                Me.mconSqlCompany = Functions.BuildConnection(Me.mobjUser)

                'Hide admin section to non-admin users
                If Not (mobjUser.SecAdmin = True Or mobjUser.SysAdmin = True) Then
                    trUsers.Visible = False
                    trGroups.Visible = False
                End If

                'Account alerts
                Dim cmdSql As New SqlClient.SqlCommand("SMAlertsGet", Me.mconSqlCompany)
                cmdSql.Parameters.Add("@UserId", Me.mobjUser.ImagesUserId)
                cmdSql.CommandType = CommandType.StoredProcedure
                Dim dt As New DataTable
                Dim da As New SqlClient.SqlDataAdapter(cmdSql)
                da.Fill(dt)

                If dt.Rows.Count = 0 Then
                    tblAlert.Visible = False
                Else
                    AlertMessage.InnerHtml = dt.Rows(0)("AlertMessage")
                End If

                'Recent items
                'SMRecentItemGet()
                '@UserID int
                cmdSql.Parameters.Clear()
                cmdSql.CommandText = "SMRecentItemGet"
                cmdSql.Parameters.Add("@UserId", Me.mobjUser.ImagesUserId)
                cmdSql.CommandType = CommandType.StoredProcedure
                Dim dtRecent As New DataTable
                da.Fill(dtRecent)

                If dtRecent.Rows.Count = 0 Then

                Else
                    dgRecentItems.DataSource = dtRecent
                    dgRecentItems.DataBind()
                End If

                'Hide scan if user has no access
                Dim dtAccess As New DataTable
                dtAccess = Functions.UserAccess(Me.mconSqlCompany, Me.mobjUser.UserId, Me.mobjUser.AccountId)
                trScan.Visible = dtAccess.Rows(0)("Index")

            End If

        Catch ex As Exception

        Finally

            If Me.mconSqlCompany.State <> ConnectionState.Closed Then
                Me.mconSqlCompany.Close()
                Me.mconSqlCompany.Dispose()
            End If

        End Try

    End Sub

    Private Sub dgRecentItems_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgRecentItems.ItemDataBound

        Try
          
            Dim RecentKey As Label = e.Item.FindControl("RecentKey")
            Dim Value As Label = e.Item.FindControl("Value")
            Dim KeyValue As Label = e.Item.FindControl("KeyValue")
            Dim lblDesc As Label = e.Item.FindControl("lblDesc")
            'Dim imgIcon As HtmlImage = e.Item.FindControl("imgIcon")

            Select Case RecentKey.Text
                Case "QS" 'Quick Search
                    lblDesc.Text = Value.Text
                    e.Item.Attributes("style") = "CURSOR: hand"
                    e.Item.Attributes("onclick") = "quickSearch('" & Value.Text & "');"
            End Select

        Catch ex As Exception

        End Try

    End Sub
End Class

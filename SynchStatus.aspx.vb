Imports Accucentric.docAssist.Data.Images

Partial Class SynchStatus
    Inherits System.Web.UI.Page


    Dim mobjUser As Accucentric.docAssist.Web.Security.User
    Dim mconSqlCompany As New SqlClient.SqlConnection


    Private Enum SynchStatusType
        LAST_SUCCESSFUL_POLL_TIME = 0
        LAST_SUCCESSFUL_JOBS = 1
        PENDING_JOBS = 2
        COMPLETED_JOBS = 3
    End Enum

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents btnSave As System.Web.UI.WebControls.ImageButton

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try
            Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
        Catch ex As Exception
            Response.Redirect("Default.aspx")
            Exit Sub
        End Try

        Try

            If Not Page.IsPostBack Then

                Me.mconSqlCompany = Functions.BuildConnection(Me.mobjUser)

                Dim Sync As New DataSync(Me.mconSqlCompany, False)

                Dim dsSynchInfo As DataSet = Sync.DIDataSyncJobStatusGet(Me.mobjUser.AccountId.ToString, Me.mobjUser.TimeDiffFromGMT)

                'Last successful synch
                If dsSynchInfo.Tables(SynchStatusType.LAST_SUCCESSFUL_POLL_TIME).Rows.Count = 1 Then
                    If Not dsSynchInfo.Tables(SynchStatusType.LAST_SUCCESSFUL_POLL_TIME).Rows(0)("LastPollDateTime") Is System.DBNull.Value Then
                        Dim synchDate As DateTime = dsSynchInfo.Tables(SynchStatusType.LAST_SUCCESSFUL_POLL_TIME).Rows(0)("LastPollDateTime")
                        lblSynchInfo.Text = "On " & synchDate.Date & " at " & synchDate.ToShortTimeString & " your server succesfully communicated with docAssist."
                    End If
                Else
                    lblSynchInfo.Visible = False
                End If

                'Last successful jobs
                If dsSynchInfo.Tables(SynchStatusType.LAST_SUCCESSFUL_JOBS).Rows.Count > 0 Then
                    dgCompletedJobs.DataSource = dsSynchInfo.Tables(SynchStatusType.LAST_SUCCESSFUL_JOBS)
                    dgCompletedJobs.DataBind()
                    lblLastSynchs.Visible = True
                Else
                    dgCompletedJobs.Visible = False
                    lblLastSynchs.Visible = False
                End If

                'Pending jobs
                If dsSynchInfo.Tables(SynchStatusType.PENDING_JOBS).Rows.Count > 0 Then
                    dgPendingJobs.DataSource = dsSynchInfo.Tables(SynchStatusType.PENDING_JOBS)
                    dgPendingJobs.DataBind()
                    lblPendingSynchs.Visible = True
                Else
                    dgPendingJobs.Visible = False
                    lblPendingSynchs.Visible = False
                End If

                'Completed
                If dsSynchInfo.Tables(SynchStatusType.COMPLETED_JOBS).Rows.Count > 0 Then
                    dgCompleted.DataSource = dsSynchInfo.Tables(SynchStatusType.COMPLETED_JOBS)
                    dgCompleted.DataBind()
                    lblCompleted.Visible = True
                Else
                    dgCompleted.Visible = False
                    lblCompleted.Visible = False
                End If

            End If

        Catch ex As Exception

        Finally

            If Me.mconSqlCompany.State <> ConnectionState.Closed Then
                Me.mconSqlCompany.Close()
                Me.mconSqlCompany.Dispose()
            End If

        End Try

    End Sub

End Class
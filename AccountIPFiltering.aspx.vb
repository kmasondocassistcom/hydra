Imports Accucentric.docAssist.Data.Images

Partial Class AccountIPFiltering
    Inherits System.Web.UI.Page
    Private mobjUser As Accucentric.docAssist.Web.Security.User
    Private conMaster As New SqlClient.SqlConnection

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents rdoNeverExpire As System.Web.UI.WebControls.RadioButton
    Protected WithEvents rdoExpireInterval As System.Web.UI.WebControls.RadioButton
    Protected WithEvents txtDays As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnSave As System.Web.UI.WebControls.ImageButton

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try
            mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
            Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))
            mobjUser.AccountId = guidAccountId
        Catch ex As Exception
            Response.Redirect("Default.aspx")
        End Try

        Try

            ipAddress.InnerText = "Your current IP address is " & Request.UserHostAddress

            If Not Page.IsPostBack Then

                conMaster = Functions.BuildMasterConnection

                Dim Util As New SystemUtilities(conMaster, True)

                Dim ds As New DataSet
                ds = Util.IPRestrictList(Me.mobjUser.AccountId.ToString, Me.mobjUser.TimeDiffFromGMT)

                chkEnabled.Checked = ds.Tables(0).Rows(0)("IPRestrict")

                updateGrid(ds.Tables(1))

            End If

        Catch ex As Exception

            Dim conLog As SqlClient.SqlConnection
            conLog = Functions.BuildMasterConnection
            Functions.LogException(mobjUser.UserId.ToString, mobjUser.AccountId.ToString, "", "IP Range Page Load: " & ex.Message, ex.StackTrace, Server.MachineName, "", Request.UrlReferrer.ToString, conLog, True)

        Finally

            If conMaster.State <> ConnectionState.Closed Then
                conMaster.Close()
                conMaster.Dispose()
            End If

        End Try

    End Sub

    Private Sub addRange_ServerClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles addRange.ServerClick

        Try

            If rangeBegin.Value.Trim.Length = 0 Then
                System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.Empty.ToString, "err('Please enter a start range.');", True)
                Exit Sub
            End If

            If rangeEnd.Value.Trim.Length = 0 Then
                System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.Empty.ToString, "err('Please enter an ending range.');", True)
                Exit Sub
            End If

            If rangeDesc.Value.Trim.Length = 0 Then
                System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.Empty.ToString, "err('Please enter a range description.');", True)
                Exit Sub
            End If

            'Validate IP ranges
            Dim regEx As New System.Text.RegularExpressions.Regex("\b(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\b")
            If Not regEx.IsMatch(rangeBegin.Value.Trim) Then
                System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.Empty.ToString, "err('Please enter a valid start range.');", True)
                Exit Sub
            End If

            If Not regEx.IsMatch(rangeEnd.Value.Trim) Then
                System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.Empty.ToString, "err('Please enter a valid ending range.');", True)
                Exit Sub
            End If

            'Validate ranges are in correct order
            Dim start() As String = rangeBegin.Value.Trim.Split(".")
            Dim ending() As String = rangeBegin.Value.Trim.Split(".")

            For x As Integer = 0 To 3
                If start(x) > ending(x) Then
                    System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.Empty.ToString, "err('Ending address must be greater than start address.');", True)
                    Exit Sub
                End If
            Next

            conMaster = Functions.BuildMasterConnection

            Dim Util As New SystemUtilities(conMaster, False)

            Dim id As Integer = Util.IPRestrictInsert(Me.mobjUser.AccountId.ToString, zeroFillIP(rangeBegin.Value), zeroFillIP(rangeEnd.Value), rangeDesc.Value, Me.mobjUser.UserId.ToString)

            System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.Empty.ToString, "$('#add').fadeOut(400);", True)

            Dim ds As New DataSet
            ds = Util.IPRestrictList(Me.mobjUser.AccountId.ToString, Me.mobjUser.TimeDiffFromGMT)

            chkEnabled.Checked = ds.Tables(0).Rows(0)("IPRestrict")

            updateGrid(ds.Tables(1))

        Catch ex As Exception

            System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.Empty.ToString, "err('An error occured while adding this IP range. Our support team has been notified.');", True)

            Dim conLog As SqlClient.SqlConnection
            conLog = Functions.BuildMasterConnection
            Functions.LogException(mobjUser.UserId.ToString, mobjUser.AccountId.ToString, "", "Error adding IP address: " & ex.Message, ex.StackTrace, Server.MachineName, "", Request.UrlReferrer.ToString, conLog, True)

        Finally

            If conMaster.State <> ConnectionState.Closed Then
                conMaster.Close()
                conMaster.Dispose()
            End If

        End Try


    End Sub

    Private Sub chkEnabled_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkEnabled.CheckedChanged

        Try

            conMaster = Functions.BuildMasterConnection

            Dim Util As New SystemUtilities(conMaster, True)
            Util.IPRestrictSetting(Me.mobjUser.AccountId.ToString, chkEnabled.Checked)

        Catch ex As Exception

            Dim conLog As SqlClient.SqlConnection
            conLog = Functions.BuildMasterConnection
            Functions.LogException(mobjUser.UserId.ToString, mobjUser.AccountId.ToString, "", "IP Range Page Load: " & ex.Message, ex.StackTrace, Server.MachineName, "", Request.UrlReferrer.ToString, conLog, True)

        Finally

            If conMaster.State <> ConnectionState.Closed Then
                conMaster.Close()
                conMaster.Dispose()
            End If

        End Try

    End Sub

    Private Sub Ranges_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Ranges.PreRender

        For Each item As DataGridItem In Ranges.Items

            If item.ItemType = ListItemType.Item Or item.ItemType = ListItemType.AlternatingItem Then

                item.Cells(2).Text = formatIP(item.Cells(2).Text)
                item.Cells(3).Text = formatIP(item.Cells(3).Text)

                Dim lnkRemove As LinkButton = CType(item.FindControl("lnkRemove"), LinkButton)
                If Not lnkRemove Is Nothing Then lnkRemove.Attributes("onclick") = "return confirm('Delete this IP range?');"

            End If

        Next

    End Sub

    Private Sub Ranges_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles Ranges.DeleteCommand

        Try

            conMaster = Functions.BuildMasterConnection

            Dim lblId As Label = e.Item.FindControl("lblId")

            Dim Util As New SystemUtilities(conMaster, False)
            Util.IPRestrictDelete(lblId.Text, Me.mobjUser.AccountId.ToString)

            Dim ds As New DataSet
            ds = Util.IPRestrictList(Me.mobjUser.AccountId.ToString, Me.mobjUser.TimeDiffFromGMT)

            chkEnabled.Checked = ds.Tables(0).Rows(0)("IPRestrict")

            updateGrid(ds.Tables(1))

        Catch ex As Exception

            Dim conLog As SqlClient.SqlConnection
            conLog = Functions.BuildMasterConnection
            Functions.LogException(mobjUser.UserId.ToString, mobjUser.AccountId.ToString, "", "IP Range Page Load: " & ex.Message, ex.StackTrace, Server.MachineName, "", Request.UrlReferrer.ToString, conLog, True)

        Finally

            If conMaster.State <> ConnectionState.Closed Then
                conMaster.Close()
                conMaster.Dispose()
            End If

        End Try

    End Sub

    Private Sub updateGrid(ByVal dt As DataTable)

        If dt.Rows.Count = 0 Then
            System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.Empty.ToString, "$(document).ready(function() { addMode(); });", True)
            Ranges.Visible = False
        Else
            Ranges.DataSource = dt
            Ranges.DataBind()
            Ranges.Visible = True
        End If

    End Sub

    Private Function formatIP(ByVal source As String) As String

        Dim sb As New System.Text.StringBuilder
        Dim sections As String() = source.Split(".")

        Dim count As Integer = 1
        For Each chunk As String In sections

            If chunk = "000" Then
                sb.Append("0")
            Else
                sb.Append(chunk.TrimStart("0"))
            End If

            If count < 4 Then sb.Append(".")

            count += 1
        Next

        Return sb.ToString

    End Function

End Class
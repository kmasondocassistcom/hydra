<%@ Register TagPrefix="uc1" TagName="TopRight" Src="../Controls/TopRight.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="DataIntegration.aspx.vb" Inherits="docAssistWeb.DataIntegration"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>docAssist - Data Integration</title>
		<META content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<META content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<META content="JavaScript" name="vs_defaultClientScript">
		<META content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<BODY bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<FORM id="DataIntegration" method="post" runat="server">
			<TABLE height="100%" cellSpacing="0" cellPadding="0" width="100%">
				<TR>
					<TD>[header placeholder]</TD>
				</TR>
				<TR>
					<TD height="100%">
						<OBJECT id="objIntegration" height="100%" width="100%" data="data:application/x-oleobject;base64,IGkzJfkDzxGP0ACqAGhvEzwhRE9DVFlQRSBIVE1MIFBVQkxJQyAiLS8vVzNDLy9EVEQgSFRNTCA0LjAgVHJhbnNpdGlvbmFsLy9FTiI+DQo8SFRNTD48SEVBRD4NCjxNRVRBIGh0dHAtZXF1aXY9Q29udGVudC1UeXBlIGNvbnRlbnQ9InRleHQvaHRtbDsgY2hhcnNldD13aW5kb3dzLTEyNTIiPg0KPE1FVEEgY29udGVudD0iTVNIVE1MIDYuMDAuMjkwMC4yOTYzIiBuYW1lPUdFTkVSQVRPUj48L0hFQUQ+DQo8Qk9EWT4NCjxQPiZuYnNwOzwvUD48L0JPRFk+PC9IVE1MPg0K"
							classid="http:docDataIntegration.dll#docDataIntegration.docDataIntegration" VIEWASTEXT>
						</OBJECT>
					</TD>
				</TR>
				<tr>
					<td><uc1:topright id="docFooter" runat="server"></uc1:topright></td>
				</tr>
			</TABLE>
			<input id="querystring" type="hidden" runat="server">
			<SCRIPT language="javascript">
				
				var objInt = document.all.objIntegration;
				
				try
				{
					objInt.Config(document.all.querystring.value);
				}
				catch(errmsg)
				{
					//alert(errmsg);
					//window.location = "./InstallIndex.aspx";
				}	
				
				/*
				function document.all.manager::SaveComplete(a)
				{
					switch(a)
					{
						case "END":
							alert('An error has occured. Please try again. If the problem continues contact support.');
							document.location.replace("Search.aspx");
							break;
						default:
							var path=document.location.href.substring(0,document.location.href.lastIndexOf("/")+1);
							parent.location.replace('Index.aspx?VID=' + a);
							break;
					}
				}
				*/
			
			</SCRIPT>
		</FORM>
	</BODY>
</HTML>
<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="Controls/MenuBar.ascx" %>
<%@ Register TagPrefix="uc1" TagName="docHeader" Src="Controls/docHeader.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="GeneralError.aspx.vb" Inherits="docAssistWeb.GeneralError" EnableSessionState="False" enableViewState="True" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>docAssist - Error</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body MS_POSITIONING="GridLayout" bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="frmVersionDeleted" method="post" runat="server">
			<table width="100%" height="100%">
				<tr>
					<td width="50%"><img src="spacer.gif" height="1" width="100%"></td>
					<td align="right"><img src="Images/errorPage/128_integration.gif"></td>
					<td><img src="dummy.gif" width="20" height="1"></td>
					<td nowrap>
						<P><span style="FONT-WEIGHT: bold; FONT-SIZE: 24pt; COLOR: gray; FONT-FAMILY: 'Trebuchet MS'">Oops!</span><br>
							<br>
							<span style="FONT-SIZE: 12pt; COLOR: gray; FONT-FAMILY: 'Trebuchet MS'">An error 
								has occured while processing your request. Please try again.<BR>
								<BR>
								Our support team has been notified of your error. </span><SPAN style="FONT-SIZE: 12pt; COLOR: gray; FONT-FAMILY: 'Trebuchet MS'">
								<BR>
								<BR>
							</SPAN>
						</P>
					</td>
					<td width="50%"><img src="spacer.gif" height="1" width="100%"></td>
				</tr>
			</table>
			<asp:ImageButton id="btnStart" runat="server" Height="0px" Width="0px"></asp:ImageButton>
		</form>
	</body>
</HTML>

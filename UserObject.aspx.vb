Partial Class UserObject
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here

        Try
            Dim mobjUser As Accucentric.docAssist.Web.Security.User
            mobjUser = BuildUserObject(Request.Cookies("docAssist"))

            Dim sb As New System.Text.StringBuilder
            sb.Append("Account Id: " & mobjUser.AccountId.ToString & vbCr)
            sb.Append("User Id: " & mobjUser.UserId.ToString & vbCr)
            sb.Append("Account Desc: " & mobjUser.AccountDesc.ToString & vbCr)
            sb.Append("Database Name: " & mobjUser.DBName.ToString & vbCr)
            sb.Append("Session Id: " & mobjUser.SessionID.ToString & vbCr)
            sb.Append("Company Name: " & mobjUser.CompanyName.ToString & vbCr)

            Label1.Text = sb.ToString

        Catch ex As Exception
            If ex.Message = "User is not logged on" Then
                Response.Redirect("Default.aspx")
            Else
                'lblErrorMessage.Text = ex.Message
                'lblErrorMessage.Visible = True
            End If
        End Try

    End Sub

End Class

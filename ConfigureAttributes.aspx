<%@ Register TagPrefix="uc1" TagName="AdminNav" Src="Controls/AdminNav.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ConfigureAttributes.aspx.vb" Inherits="docAssistWeb._Attributes"%>
<%@ Register TagPrefix="uc1" TagName="docHeader" Src="Controls/docHeader.ascx" %>
<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="Controls/MenuBar.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
  <HEAD>
		<TITLE>docAssist</TITLE>
		<META content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<META content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<META content="JavaScript" name="vs_defaultClientScript">
		<META content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
  </HEAD>
	<BODY onload="Disable();ShowConfiguration();SetupServices();ChangeDocuments();document.all.ddlDocuments.enabled=true;">
		<DIV id="svcUpdate" style="BEHAVIOR: url(webservice.htc)" onresult="onUpdateResult()"></DIV>
		<DIV id="svcGet" style="BEHAVIOR: url(webservice.htc)" onresult="onGetResult()"></DIV>
		<DIV id="svcGetAttributes" style="BEHAVIOR: url(webservice.htc)" onresult="onGetAttributeResult()"></DIV>
		<FORM id="Form1" onsubmit="SelectAllAssigned();" method="post" runat="server">
			<INPUT id="hostname" type="hidden" runat="server">
			<TABLE height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<TBODY>
					<TR>
						<TD width="100%" colSpan="3"><UC1:DOCHEADER id="docHeader" runat="server"></UC1:DOCHEADER></TD>
					</TR>
					<TR vAlign="middle" height="100%">
						<TD class="Navigation1" vAlign="top" width="180" height="100%"><UC1:ADMINNAV id="AdminNav1" runat="server"></UC1:ADMINNAV></TD>
						<TD class="PageContent" vAlign="middle" align="center" height="100%">
							<P class="Heading"><ASP:LABEL id="lblPageHeading" Runat="server" CssClass="PageHeading">Configuration Manager</ASP:LABEL></P>
							<TABLE class="transparent" cellSpacing="0" cellPadding="0" align="center" border="0">
								<TBODY>
									<TR>
										<TD class="TableHeader" style="BORDER-RIGHT: #d00000 1px solid; BORDER-TOP: #d00000 1px solid; BORDER-LEFT: #d00000 1px solid"
											width="364">Attributes</TD>
										<TD>&nbsp;</TD>
										<TD class="TableHeader" style="BORDER-RIGHT: #d00000 1px solid; BORDER-TOP: #d00000 1px solid; BORDER-LEFT: #d00000 1px solid"
											width="336">
											<TABLE class="transparentnopadding" cellSpacing="0" cellPadding="0" width="100%" border="0">
												<TR class="TableHeader">
													<TD>Modify Attribute</TD>
													<TD align="right"><A id="lnkNew" style="DISPLAY: none" href="javascript:Clear();">New</A>&nbsp;<A id="lnkAdd" href="javascript:AddAttribute();">Add</A></TD>
												</TR>
											</TABLE>
										</TD>
									</TR>
									<TR>
										<TD style="BORDER-RIGHT: #d00000 1px solid; BORDER-LEFT: #d00000 1px solid; BORDER-BOTTOM: #d00000 1px solid">
											<TABLE class="transparentnopadding" cellSpacing="0" cellPadding="0" border="0">
												<TR>
													<TD align="right" colSpan="3">Document:&nbsp;
														<ASP:DROPDOWNLIST id="ddlDocuments" Runat="server" CssClass="Inputbox" Width="160px"></ASP:DROPDOWNLIST></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 32px" vAlign="top"><IMG height="6" src="images/spacer.gif" width="1"><BR>
														Available&nbsp;Attributes<BR>
														<ASP:LISTBOX id="lstAvailable" runat="server" CssClass="InputBox" Width="160px" Height="216px"></ASP:LISTBOX></TD>
													<TD style="PADDING-RIGHT: 4px; PADDING-LEFT: 4px"><INPUT id="btnAdd" onclick="MoveAttribute(1);" type="button" value=" > "><BR>
														&nbsp;<BR>
														<INPUT id="btnRemove" onclick="MoveAttribute(-1);" type="button" value=" < ">
													</TD>
													<TD vAlign="top">
														<TABLE class="transparentnopadding" cellSpacing="0" cellPadding="0" border="0">
															<TR height="6">
																<TD colSpan="2"><IMG height="6" src="images/spacer.gif" width="1"></TD>
															</TR>
															<TR>
																<TD rowSpan="2">Assigned&nbsp;Attributes<BR>
																	<ASP:LISTBOX id="lstAssigned" runat="server" CssClass="InputBox" Width="160px" Height="216px"></ASP:LISTBOX>
																	<INPUT id="hiddenDocumentAttributes" type="hidden" runat="server">
																</TD>
																<TD vAlign="top" align="center">&nbsp;<BR>
																	<INPUT id="btnUp" type="button" onclick="MoveItemUp();" value="Up">
																</TD>
															</TR>
															<TR>
																<TD vAlign="bottom" align="center"><INPUT id="btnDown" onclick="MoveItemDown();" type="button" value="Dn"></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
											</TABLE>
										</TD>
										<TD>&nbsp;</TD>
										<TD style="BORDER-RIGHT: #d00000 1px solid; BORDER-LEFT: #d00000 1px solid; BORDER-BOTTOM: #d00000 1px solid"
											vAlign="top"><INPUT id="txtAttributeId" type="hidden">
											<TABLE class="transparent" cellSpacing="0" cellPadding="0" width="100%" border="0">
												<TR>
													<TD>Attribute&nbsp;Name:&nbsp;</TD>
													<TD width="100%"><ASP:TEXTBOX id="txtAttributeName" Runat="server" CssClass="InputBox" Width="100%"></ASP:TEXTBOX></TD>
													<TD>&nbsp;</TD>
												</TR>
												<TR class="TableAlternateItem">
													<TD>Data&nbsp;Type:</TD>
													<TD><ASP:DROPDOWNLIST id="ddlDataType" Runat="server" CssClass="InputBox" Width="100%">
<asp:ListItem Value="CheckBox">Check box</asp:ListItem>
<asp:ListItem Value="Date">Date</asp:ListItem>
<asp:ListItem Value="List">List</asp:ListItem>
<asp:ListItem Value="Numeric">Numeric</asp:ListItem>
<asp:ListItem Value="String" Selected="True">String</asp:ListItem>
														</ASP:DROPDOWNLIST></TD>
													<TD><IMG src="images/help.gif"></TD>
												</TR>
												<TR>
													<TD>Data&nbsp;Format:</TD>
													<TD><ASP:TEXTBOX id="txtDataFormat" Runat="server" CssClass="InputBox" Width="100%"></ASP:TEXTBOX></TD>
													<TD><IMG src="images/help.gif"></TD>
												</TR>
												<TR class="TableAlternateItem">
													<TD>Length:<BR>
														<SPAN class="info">255&nbsp;max</SPAN></TD>
													<TD><ASP:TEXTBOX id="txtLength" Runat="server" CssClass="InputBox" Width="100%"></ASP:TEXTBOX></TD>
													<TD>&nbsp;</TD>
												</TR>
												<TR>
													<TD vAlign="top">List:
														<DIV class="info">One line per item</DIV>
													</TD>
													<TD><ASP:TEXTBOX id="txtList" runat="server" Width="100%" Height="92px" TextMode="MultiLine" Enabled="False"></ASP:TEXTBOX></TD>
													<TD><IMG src="images/help.gif"></TD>
												</TR>
											</TABLE>
										</TD>
									</TR>
									<TR>
										<TD colSpan="2"><SPAN class="red" id="showMessage" runat="server"></SPAN></TD>
										<TD align="right"><ASP:LINKBUTTON id="btnSave" Runat="server">Save</ASP:LINKBUTTON></TD>
									</TR>
								</TBODY>
							</TABLE>
						</TD>
						<TD class="RightContent" width="160">&nbsp;</TD>
					</TR>
					<TR height="1">
						<TD style="BACKGROUND-COLOR: #959394" width="160"><IMG height="1" src="images/spacer.gif" width="160"></TD>
						<TD style="BACKGROUND-COLOR: #959394" width="100%"><IMG height="1" src="images/spacer.gif" width="100%"></TD>
						<TD style="BACKGROUND-COLOR: #959394" width="80"><IMG height="1" src="images/spacer.gif" width="80"></TD>
					</TR>
				</TBODY>
			</TABLE>
			<SCRIPT language="javascript">
		        document.all.docHeader_btnSearch.src = "./Images/toolbar_search_white.gif";
		        document.all.docHeader_btnIndex.src = "./Images/toolbar_index_white.gif";
		        document.all.docHeader_btnScan.src = "./Images/toolbar_scan_white.gif";
		        document.all.docHeader_btnOptions.src = "./Images/toolbar_options_grey.gif";
			</SCRIPT>
			<SCRIPT language="javascript">
				var blnAttributeId = -1;
				var selectedAttributeId = 0;
				var blnDirtyData = false;
				var hostName;
			
				function AddAttribute()
				{
					var ddlDocuments = document.all.ddlDocuments;
					if(ddlDocuments.selectedIndex > 0)
					{
						Disable();
						// Start Service
						svcUpdate.onServiceAvailable = UpdateAttribute();
					}
				}

				function AttributeSelected(listBox)
				{
					if(listBox.id == "lstAvailable")
					{
						document.all.lstAssigned.selectedIndex = -1;
						document.all.txtAttributeId.value = document.all.lstAvailable.options[document.all.lstAvailable.selectedIndex].value;
					}
					else
					{
						document.all.lstAvailable.selectedIndex = -1;
						document.all.txtAttributeId.value = document.all.lstAssigned.options[document.all.lstAssigned.selectedIndex].value;
					}
					Disable();					
					svcGet.onServiceAvailable = GetAttributeDetail(document.all.txtAttributeId.value);
				}

				function ChangeDocuments()
				{
					var result;
					var ddlDocuments = document.all.ddlDocuments;
					if(ddlDocuments.selectedIndex >= 0)
					{
						if(blnDirtyData)
							result = confirm("All changes will be lost.  Continue?");
						else
							result = true;
							
						if(result)
						{
							// Load the new document type
							Clear();
							Disable();
							svcGetAttributes.onServiceAvailable = GetDocumentAttributes(ddlDocuments.options[ddlDocuments.selectedIndex].value);
						}
					} 
					else
					{
						Clear();
						Disable();
					}
				}
				
				function Clear()
				{
						document.all.txtAttributeId.value = -1;
						document.all.txtAttributeName.value = "";
						document.all.ddlDataType.selectedIndex = document.all.ddlDataType.options.length-1;
						document.all.txtDataFormat.value = "";
						document.all.txtLength.value = "";
						document.all.txtList.value = "";
						document.all.lnkNew.style.display = "none";
						document.all.lnkAdd.innerHTML = "Add";
						document.all.lstAvailable.selectedIndex = -1;
						document.all.lstAssigned.selectedIndex = -1;
						UpdateButtons();
						
				}

				function Disable()
				{
					document.all.txtAttributeName.disabled = true;
					document.all.ddlDataType.disabled = true;
					document.all.txtDataFormat.disabled = true;
					document.all.txtLength.disabled = true;
					document.all.txtList.disabled = true;
					document.all.lnkNew.disabled = true;
					document.all.lnkAdd.disabled = true;
				}

				function Enable()
				{
					document.all.txtAttributeName.disabled = false;
					document.all.ddlDataType.disabled = false;
					document.all.txtDataFormat.disabled = false;
					document.all.txtLength.disabled = false;
					document.all.lnkNew.disabled = false;
					document.all.lnkAdd.disabled = false;
					EnableList();
				}
			
				function EnableList()
				{
					box = document.all.ddlDataType;
					value = box.options[box.selectedIndex].value;
					if(value=="List")
					{
						document.all.txtList.disabled=false;
					} else if(value=="Date")
					{
						document.all.txtDataFormat.diabled = true;
					}
				}
				
				function GetAttributeDetail(attributeId)
				{
					Disable();
					svcGet.Properties.callService("AttributeProperties", attributeId);
				}
				
				function GetDocumentAttributes(documentId)
				{
					Disable();
					svcGetAttributes.Admin.callService("DocumentAttributes", documentId);
				}
				
				function MoveAttribute(direction)
				{
					var intItemValue;
					var strItemText;
					var lstAvailable = document.all.lstAvailable;
					var lstAssigned  = document.all.lstAssigned;
					var newOption    = document.createElement("OPTION")
					
					if(lstAvailable.selectedIndex >= 0 && direction == 1)
					{
						strItemValue = lstAvailable.options[lstAvailable.options.selectedIndex].text;
						intItemValue = lstAvailable.options[lstAvailable.options.selectedIndex].value;
						lstAvailable.remove(lstAvailable.options.selectedIndex);
						
						lstAssigned.options.add(newOption);
						newOption.innerText = strItemValue;
						newOption.value = intItemValue;
						lstAssigned.selectedIndex = lstAssigned.options.length-1;
					}
					else if (lstAssigned.selectedIndex >= 0 && direction == -1)
					{
						strItemValue = lstAssigned.options[lstAssigned.options.selectedIndex].text;
						intItemValue = lstAssigned.options[lstAssigned.options.selectedIndex].value;
						lstAssigned.remove(lstAssigned.options.selectedIndex);
						
						lstAvailable.options.add(newOption);
						newOption.innerText = strItemValue;
						newOption.value = intItemValue;
						lstAvailable.selectedIndex = lstAvailable.options.length-1;
					}
					UpdateButtons();
					blnDirtyData = true;
				}
				
				function MoveItemUp()
				{
					lstAssigned = document.all.lstAssigned;
					var selectedItemValue = lstAssigned.options[lstAssigned.selectedIndex].value;
					var selectedItemText = lstAssigned.options[lstAssigned.selectedIndex].text;
					var priorItemValue = lstAssigned.options[lstAssigned.selectedIndex-1].value;
					var priorItemText = lstAssigned.options[lstAssigned.selectedIndex-1].text;
					
					lstAssigned.options[lstAssigned.selectedIndex].value = priorItemValue;
					lstAssigned.options[lstAssigned.selectedIndex].text = priorItemText;
					lstAssigned.options[lstAssigned.selectedIndex-1].value = selectedItemValue;
					lstAssigned.options[lstAssigned.selectedIndex-1].text = selectedItemText;
					lstAssigned.selectedIndex = lstAssigned.selectedIndex - 1;
					
					UpdateButtons();
					blnDirtyData = true;
				}
				
				function MoveItemDown()
				{
					lstAssigned = document.all.lstAssigned;
					var selectedItemValue = lstAssigned.options[lstAssigned.selectedIndex].value;
					var selectedItemText = lstAssigned.options[lstAssigned.selectedIndex].text;
					var nextItemValue = lstAssigned.options[lstAssigned.selectedIndex+1].value;
					var nextItemText = lstAssigned.options[lstAssigned.selectedIndex+1].text;
					
					lstAssigned.options[lstAssigned.selectedIndex].value = nextItemValue;
					lstAssigned.options[lstAssigned.selectedIndex].text = nextItemText;
					lstAssigned.options[lstAssigned.selectedIndex+1].value = selectedItemValue;
					lstAssigned.options[lstAssigned.selectedIndex+1].text = selectedItemText;
					lstAssigned.selectedIndex = lstAssigned.selectedIndex + 1;
					
					UpdateButtons();
					blnDirtyData = true;
				}
				
				function onGetAttributeResult()
				{
					// Check for error
					if (event.result.error)
					{
						//Pull the error information from the event.result.errorDetail properties
						var xfaultcode		= event.result.errorDetail.code;
						var xfaultstring	= event.result.errorDetail.string;
						var xfaultsoap		= event.result.errorDetail.raw;
									
						// Add code to handle specific codes here
						alert("Error: (" + xfaultcode + ") " + xfaultstring);
					}
					else if((!event.result.error))
					{
						var lstAvailable = document.all.lstAvailable;
						var lstAssigned = document.all.lstAssigned;
						var sXQLAssigned = "/DocumentAttributes/Assigned";
						var sXQLAvailable = "/DocumentAttributes/Available";
						var oXml = new ActiveXObject("MSXML2.DOMDocument"); //document.all.itemsAttributes;
						oXml.async = false;
						oXml.loadXML(event.result.value);
						
						// Remove the existing options
						for (var intOption = lstAvailable.options.length-1; intOption >=0; intOption--)
							lstAvailable.options.remove(intOption);
						for (var intOption = lstAssigned.options.length-1; intOption >=0; intOption--)
							lstAssigned.options.remove(intOption);

						//query for items to add
						var oNodes = oXml.selectNodes(sXQLAssigned);
						for (var i=0; i<=oNodes.length-1; i++)
						{
							var oOption = document.createElement("OPTION");
							var sAttributeId = oNodes.item(i).childNodes(0).text;
							var sAttributeName = oNodes.item(i).childNodes(1).text;
							lstAssigned.options.add(oOption, i);
							oOption.value = sAttributeId;
							oOption.text = sAttributeName;
						}
						lstAvailable.selectedIndex = -1;
						
						//query for items to add
						var oNodes = oXml.selectNodes(sXQLAvailable);
						for (var i=0; i<=oNodes.length-1; i++)
						{
							var oOption = document.createElement("OPTION");
							var sAttributeId = oNodes.item(i).childNodes(0).text;
							var sAttributeName = oNodes.item(i).childNodes(1).text;
							lstAvailable.options.add(oOption, i);
							oOption.value = sAttributeId;
							oOption.text = sAttributeName;
						}
						lstAvailable.selectedIndex = -1;
						Enable();
						UpdateButtons();
					}
				}	
				
				function onGetResult()
				{
					// Check for error
					if (event.result.error)
					{
						//Pull the error information from the event.result.errorDetail properties
						var xfaultcode		= event.result.errorDetail.code;
						var xfaultstring	= event.result.errorDetail.string;
						var xfaultsoap		= event.result.errorDetail.raw;
									
						// Add code to handle specific codes here
						alert("Error: (" + xfaultcode + ") " + xfaultstring + "\n" + xfaultsoap);
					}
					else if((!event.result.error))
					{
						var list;
						var oXml = new ActiveXObject("MSXML2.DOMDocument"); //document.all.itemsAttributes;
						oXml.async = false;
						oXml.loadXML(event.result.value);
						var sXql = "/Attributes/Attributes";
						
						if (document.all.lstAssigned.selectedIndex >= 0)
							list		= document.all.lstAssigned;
						else
							list		= document.all.lstAvailable;
						attributeName	= document.all.txtAttributeName;
						attributeId     = document.all.txtAttributeId;
						attributeType	= document.all.ddlDataType;
						attributeFormat = document.all.txtDataFormat;
						attributeLength = document.all.txtLength;
						attributeList   = document.all.txtList;
						
					
						var oNodes = oXml.selectNodes(sXql);
						if (oNodes.length == 1)
						{
							oAttributes = oNodes.item(0);
							var sAttributeType = oAttributes.childNodes(2).text;
							for(var i=0; i <= attributeType.options.length-1; i++)
							{
								if ( attributeType.options(i).value == sAttributeType )
									attributeType.selectedIndex = i;
							}
							attributeFormat.value = oAttributes.childNodes(3).text;
							attributeLength.value = oNodes.item(0).childNodes(4).text;
						}

						if(sAttributeType=="List")
						{
							sXql = "/Attributes/AttributesLists";
							oNodes = oXml.selectNodes(sXql);
							var sAttributeList = "";
							for(i=0; i<=oNodes.length-1; i++)
							{
								if(sAttributeList.length > 0)
									sAttributeList = sAttributeList + "\n";
								sAttributeList = sAttributeList + oNodes(i).childNodes(2).text;
							}
							if(sAttributeList.length > 0)
								attributeList.value = sAttributeList;
						}
						else
							attributeList.value = "";
						attributeId.value = list.options[list.options.selectedIndex].value;
						attributeName.value = list.options[list.options.selectedIndex].text;
						document.all.lnkNew.style.display = "inline";
						document.all.lnkAdd.innerHTML = "Change";
						Enable();
						UpdateButtons();
					}
				}

				function onUpdateResult()
				{
					// Check for error
					if((event.result.error))
					{
						//Pull the error information from the event.result.errorDetail properties
						var xfaultcode		= event.result.errorDetail.code;
						var xfaultstring	= event.result.errorDetail.string;
						var xfaultsoap		= event.result.errorDetail.raw;
									
						// Add code to handle specific codes here
						alert("Error: (" + xfaultcode + ") " + xfaultstring);
					}
					else if(!event.result.error)
					{
						var intAttributeId = -1 //event.result.value;
						var strAttributeName = Form1.txtAttributeName.value;

						document.all.lnkAdd.disabled = false;
						document.all.txtAttributeName.disabled = false;
						document.all.ddlDataType.disabled = false;
						document.all.txtDataFormat.disabled = false;
						document.all.txtLength.disabled = false;
						document.all.txtList.disabled = true;
						
						if(document.all.lstAssigned.selectedIndex < 0 && document.all.lstAvailable.selectedIndex < 0)
						{
							document.all.lstAvailable.options[document.all.lstAvailable.options.length] = new Option(strAttributeName, intAttributeId);
							document.all.showMessage.innerHTML = "Attribute added successfully.";
						}
						else 
						{
							if (document.all.lstAssigned.selectedIndex >= 0)
								document.all.lstAssigned.options[document.all.lstAssigned.selectedIndex].text = strAttributeName;
							else if (document.all.lstAvailable.selectedIndex >= 0)
								document.all.lstAvailable.options[document.all.lstAvailable.selectedIndex].text = strAttributeName;
							document.all.showMessage.innerHTML = "Attribute updated successfully.";
						}
						
						Clear();
						UpdateButtons();
						blnDirtyData = true;
					}
				}	
				
				function SelectAllAssigned()
				{
					var lstAssigned = document.all.lstAssigned;
					lstAssigned.onClick = "";
					lstAssigned.multiple = true;
					var selectedItems = new Array();
					for(var i=0; i<lstAssigned.options.length; i++)
					{	
						selectedItems[i] = lstAssigned.options[i].value;
					}
					document.all.hiddenDocumentAttributes.value = selectedItems;
				}

				function SetupServices()
				{
						svcGetAttributes.useService(hostName + "/UserAdmin.asmx?wsdl", "Admin");
						svcGet.useService(hostName + "/UserAdmin.asmx?wsdl", "Properties");
						svcUpdate.useService(hostName + "/UserAdmin.asmx?wsdl", "Update");
						
						if(document.all.ddlDocuments.selectedIndex > 0)
						{
							ChangeDocuments();
						}
				}
	
				function ShowConfiguration()
				{
					document.all.ConfigurationLinks.style.display='block';
					document.all.lnkShowAttributes.innerHTML = document.all.lnkShowAttributes.innerHTML + "<IMG src='images//arrow_left.gif' border=0>";
					hostName = document.all.hostname.value; 
					
				}
				
				function UpdateAttribute()
				{
					strAttributeName = document.all.txtAttributeName.value;
					strAttributeId = document.all.txtAttributeId.value;
					strDataType = document.all.ddlDataType.options[document.all.ddlDataType.selectedIndex].value;
					strDataFormat = document.all.txtDataFormat.value;
					strLength = document.all.txtLength.value;
					strList = document.all.txtList.value
					
					if(strLength.length==0)
						strLength = "255";

					svcUpdate.Update.callService("AddAttribute", strAttributeId, strAttributeName, strDataType, strDataFormat, strLength, strList);
				}

				function UpdateButtons()
				{
					var lstAvailable = document.all.lstAvailable;
					var lstAssigned = document.all.lstAssigned;
					var btnAdd = document.all.btnAdd;
					var btnRemove = document.all.btnRemove;
					var btnUp = document.all.btnUp;
					var btnDown = document.all.btnDown;
					
					btnAdd.disabled = true;
					btnRemove.disabled = true;
					btnUp.disabled = true;
					btnDown.disabled = true;
										
					if(lstAvailable.selectedIndex >= 0)
					{
						btnAdd.disabled = false;
					}
					else if(lstAssigned.selectedIndex >=0)
					{
						btnRemove.disabled = false;
						if(lstAssigned.selectedIndex > 0)
							btnUp.disabled = false;
						if(lstAssigned.selectedIndex < lstAssigned.options.length - 1)
							btnDown.disabled = false;
					}
				}
			</SCRIPT>
		</FORM>
	</BODY>
</HTML>

<%@ Page Language="vb" AutoEventWireup="false" Codebehind="DefaultAccount.aspx.vb" Inherits="docAssistWeb.DefaultAccount"%>
<%@ Register TagPrefix="uc1" TagName="docHeader" Src="Controls/docHeader.ascx" %>
<%@ Register TagPrefix="uc1" TagName="MenuBar" Src="Controls/MenuBar.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>docAssist</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<table height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
					<td colspan="3">
						<uc1:docHeader id="DocHeader1" runat="server"></uc1:docHeader></td>
				</tr>
				<tr>
					<td class="NavigationTop" colSpan="3">
						<uc1:MenuBar id="MenuBar1" runat="server"></uc1:MenuBar></td>
				</tr>
				<tr vAlign="middle" height="100%">
					<td class="Navigation1" vAlign="top" width="180" height="100%">
						&nbsp;
					</td>
					<td class="PageContent" vAlign="middle" align="center" height="100%">
						<P>&nbsp; Please select a default account:<br>
							<asp:DropDownList id="ddlAccounts" runat="server"></asp:DropDownList>
						</P>
						<P>Your current default account is:<br>
							<asp:Label id="Label1" runat="server">Label</asp:Label>
						</P>
					</td>
					<td class="RightContent">&nbsp;</td>
				</tr>
				<TR height="1">
					<TD style="BACKGROUND-COLOR: #959394" width="180"><IMG height="1" src="images/spacer.gif" width="180"></TD>
					<TD style="BACKGROUND-COLOR: #959394" width="100%"><IMG height="1" src="images/spacer.gif" width="100%"></TD>
					<TD style="BACKGROUND-COLOR: #959394" width="90"><IMG src="images/spacer.gif" width="90" height="1"></TD>
				</TR>
			</table>
		</form>
	</body>
</HTML>

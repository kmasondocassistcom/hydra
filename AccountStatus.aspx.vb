Imports Accucentric.docAssist
Imports Accucentric.docAssist.Data.Images

Partial Class AccountStatus

    Inherits System.Web.UI.Page

    Private mobjUser As Web.Security.User
    Private mconSqlImage As New SqlClient.SqlConnection

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try

            If Not Page.IsPostBack Then

                If Request.Cookies("docAssist") Is Nothing Then
                    Response.Redirect("Default.aspx")
                    Exit Sub
                End If

                Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))
                Dim guidAccountId As New System.Guid(Request.Cookies("docAssist")("AccountId"))

                If Not (Me.mobjUser.SecAdmin = True Or Me.mobjUser.SysAdmin = True) Then
                    System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.Empty.ToString, "alert('Access denied.');window.location.href='Default.aspx';", True)
                    Exit Sub
                End If

                Me.mobjUser.AccountId = guidAccountId
                Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)

                Dim objAdmin As New Data.Images.Admin(Me.mconSqlImage, True)
                Dim stats As New DataSet

                stats = objAdmin.DataUsage()

                If stats.Tables(0).Rows.Count > 0 Then

                    Dim sb As New System.Text.StringBuilder
                    sb.Append("<h2>" & Me.mobjUser.CompanyName & " Account Status</h2>")

                    sb.Append("<b>Space Used: " & Functions.GetBytesAsMegs(CDbl(stats.Tables(0).Rows(0)("SpaceUsed"))) & "</b><BR><BR>")

                    'User types
                    For Each dr As DataRow In stats.Tables(1).Rows
                        If dr("UserCount") > 0 Then
                            Dim userCount As Integer = dr("UserCount")
                            Select Case CType(dr("UserType"), UserType)
                                Case Constants.UserType.IMAGING
                                    sb.Append("Imaging Users: " & userCount & "<BR>")
                                Case Constants.UserType.READ_ONLY
                                    sb.Append("Read Only Users: " & userCount & "<BR>")
                                Case Constants.UserType.STANDARD
                                    sb.Append("Standard Users: " & userCount & "<BR>")
                                Case Constants.UserType.WORKFLOW_ONLY
                                    sb.Append("Workflow Only Users: " & userCount & "<BR>")
                            End Select
                        End If
                    Next

                    sb.Append("<BR><b>Total Active Users: " & stats.Tables(0).Rows(0)("ActiveUserCount") & "</b>")

                    'UserType
                    'UserCount

                    sb.Append("" & "<BR>")
                    sb.Append("" & "<BR>")
                    sb.Append("" & "<BR>")
                    sb.Append("" & "<BR>")
                    sb.Append("" & "<BR>")

                    lblStatus.Text = sb.ToString

                    lblError.Visible = False
                    lblError.Text = ""
                Else
                    lblStatus.Text = ""
                    lblError.Visible = True
                    lblError.Text = ErrorCodes.MESSAGE_ERROR_LOAD_ACCOUNT_STATUS
                End If

            End If

        Catch ex As Exception

            If ex.Message = "DualLogin" Then
                Response.Cookies("docAssist")("DualLogin") = True
                Server.Transfer("Default.aspx")
            Else
                Response.Redirect("Default.aspx")
                Response.Flush()
                Response.End()
            End If

        Finally

            If Me.mconSqlImage.State <> ConnectionState.Closed Then
                Me.mconSqlImage.Close()
                Me.mconSqlImage.Dispose()
            End If

        End Try

    End Sub

End Class
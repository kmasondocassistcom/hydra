Imports Accucentric.docAssist.Data.Images

Partial Class SetupMail
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents AutoImage As System.Web.UI.WebControls.CheckBox

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private mobjUser As Accucentric.docAssist.Web.Security.User

    Private conSqlImage As New SqlClient.SqlConnection
    Private conSqlMaster As New SqlClient.SqlConnection

    Private strId As String

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try

            lblError.Text = ""

            Me.mobjUser = New Accucentric.docAssist.Web.Security.User(Request.Cookies("docAssist")("UserId"), ConfigurationSettings.AppSettings("Connection"))

            strId = Request.QueryString("Id").Replace("W", "")

            If Not Page.IsPostBack Then

                conSqlMaster = Functions.BuildMasterConnection
                conSqlImage = Functions.BuildConnection(Me.mobjUser)
                conSqlImage.Open()

                Dim objEmailSettings As New Folders(conSqlMaster, False)
                Dim strEmailPrefix As String

                strEmailPrefix = objEmailSettings.EmailPrefixGet(Me.mobjUser.AccountId.ToString, strId)
                lblAlias.Text = "." & strEmailPrefix.ToLower & "@email.docassist.com"

                Dim sqlCmd As SqlClient.SqlCommand = New SqlClient.SqlCommand("acsDocumentsByCabinetFolder", conSqlImage)
                sqlCmd.CommandType = CommandType.StoredProcedure
                sqlCmd.Parameters.Add("@UserName", Me.mobjUser.UserId)
                sqlCmd.Parameters.Add("@CabinetId", "0")
                sqlCmd.Parameters.Add("@FolderId", "0")
                Dim daSql As New SqlClient.SqlDataAdapter(sqlCmd)
                Dim dt As New DataTable
                daSql.Fill(dt)

                DocType.DataSource = dt
                DocType.DataTextField = "DocumentName"
                DocType.DataValueField = "DocumentId"
                DocType.DataBind()

                Dim dsEmailOptions As DataSet = objEmailSettings.EmailOptionsFolderGet(Me.mobjUser.AccountId.ToString, strId)

                If dsEmailOptions.Tables(0).Rows.Count > 0 Then
                    txtPrefix.Text = dsEmailOptions.Tables(0).Rows(0)("Alias")
                    DocType.SelectedValue = dsEmailOptions.Tables(0).Rows(0)("DocumentId")
                End If

            End If

        Catch ex As Exception

            lblError.Text = "An error has occured. Please try again."

            Dim conMaster As SqlClient.SqlConnection
            conMaster = Functions.BuildMasterConnection
            Functions.LogException(mobjUser.UserId.ToString, mobjUser.AccountId.ToString, "", "E-mail Setup (Small Bus. Edition): " & ex.Message, ex.StackTrace, Server.MachineName, "", Request.UrlReferrer.ToString, conMaster, True)

        Finally

            If conSqlMaster.State <> ConnectionState.Closed Then
                conSqlMaster.Close()
                conSqlMaster.Dispose()
            End If

            If conSqlImage.State <> ConnectionState.Closed Then
                conSqlImage.Close()
                conSqlImage.Dispose()
            End If

        End Try

    End Sub

    Private Sub SaveSettings_ServerClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SaveSettings.ServerClick

        Try

            Dim enableAccount As Boolean = IIf(txtPrefix.Text.Trim.Length > 0, True, False)

            conSqlMaster = Functions.BuildMasterConnection

            Dim objEmailSettings As New Folders(conSqlMaster, True)
            Dim res As Integer = objEmailSettings.EmailFolderInsert(Me.mobjUser.AccountId.ToString, strId, txtPrefix.Text.Trim, enableAccount, False, False, True, True, False, False, False, False, DocType.SelectedValue)

            lit.Text = "<script>parent.GB_hide(false);</script>"

        Catch ex As Exception

            lblError.Text = "An error has occured. Please try again."

            Dim conMaster As SqlClient.SqlConnection
            conMaster = Functions.BuildMasterConnection
            Functions.LogException(mobjUser.UserId.ToString, mobjUser.AccountId.ToString, "", "E-mail Setup (Small Bus. Edition): " & ex.Message, ex.StackTrace, Server.MachineName, "", Request.UrlReferrer.ToString, conMaster, True)

        Finally

            If conSqlMaster.State <> ConnectionState.Closed Then
                conSqlMaster.Close()
                conSqlMaster.Dispose()
            End If

        End Try

    End Sub

End Class

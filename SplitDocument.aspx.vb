Partial Class SplitDocument
    Inherits System.Web.UI.Page

    Private mobjUser As Accucentric.docAssist.Web.Security.User
    Private mconSqlImage As SqlClient.SqlConnection

    Private mcookieImage As cookieImage
    Private mcookieSearch As cookieSearch

    Protected WithEvents lblErrorMessage As System.Web.UI.WebControls.Label

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try
            Me.mobjUser = BuildUserObject(Request.Cookies("docAssist"))
            Me.mconSqlImage = Functions.BuildConnection(Me.mobjUser)
        Catch ex As Exception
            If ex.Message = "User is not logged on" Then
                Response.Redirect("Default.aspx")
            End If
        End Try

        Me.mcookieImage = New cookieImage(Me)
        Me.mcookieSearch = New cookieSearch(Me)

        Try
            txtStart.Attributes("onkeypress") = "return NumericOnly(event);"
            txtEnd.Attributes("onkeypress") = "return NumericOnly(event);"

            If Not Page.IsPostBack Then
                PageCount.Value = Request.QueryString("PageCount")
            End If

        Catch ex As Exception

        End Try

    End Sub

    Private Sub btnSplit_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSplit.Click

        Try

            If txtStart.Text.Trim = "" Then
                lblError.Visible = True
                lblError.Text = "Please specify a start page."
                Exit Sub
            End If

            If txtEnd.Text.Trim = "" Then
                lblError.Visible = True
                lblError.Text = "Please specify an ending page."
                Exit Sub
            End If

            If CInt(txtStart.Text) > CInt(txtEnd.Text) Then
                lblError.Visible = True
                lblError.Text = "Invalid page range"
                Exit Sub
            End If

            If CInt(txtStart.Text) < 1 Then
                lblError.Visible = True
                lblError.Text = "Invalid page range"
                Exit Sub
            End If

            If CInt(txtEnd.Text) > PageCount.Value Then
                lblError.Visible = True
                lblError.Text = "Invalid page range"
                Exit Sub
            End If

            If CInt(txtStart.Text) = 1 And CInt(txtEnd.Text) = PageCount.Value Then
                lblError.Visible = True
                lblError.Text = "Cannot split an entire document."
                Exit Sub
            End If

            Dim conSql As New SqlClient.SqlConnection
            conSql = Functions.BuildConnection(Me.mobjUser)
            conSql.Open()
            Dim cmdSql As New SqlClient.SqlCommand("SMDocumentSplit", conSql)
            cmdSql.CommandType = CommandType.StoredProcedure
            cmdSql.Parameters.Add("@UserId", Me.mobjUser.ImagesUserId)
            cmdSql.Parameters.Add("@VersionId", Me.mcookieSearch.VersionID)
            cmdSql.Parameters.Add("@StartSeqID", CInt(txtStart.Text))
            cmdSql.Parameters.Add("@EndSeqID", CInt(txtEnd.Text))
            cmdSql.Parameters.Add("@InheritAttributes", chkInherit.Checked)

            Dim vid As New SqlClient.SqlParameter("@NewVersionID", System.Data.SqlDbType.Int)
            vid.Direction = ParameterDirection.Output
            cmdSql.Parameters.Add(vid)

            Dim vidChanged As New SqlClient.SqlParameter("@VersionIDChanged", System.Data.SqlDbType.Int)
            vidChanged.Direction = ParameterDirection.Output
            cmdSql.Parameters.Add(vidChanged)

            cmdSql.ExecuteNonQuery()

            If vid.Value > 0 Then
                System.Web.UI.ScriptManager.RegisterStartupScript(Me, Me.GetType, System.Guid.NewGuid.ToString, "window.returnValue = '" & vid.Value & "';window.close();", True)
            Else
                Throw New Exception("Error splitting document.")
            End If

        Catch ex As Exception

#If DEBUG Then
            lblError.Visible = True
            lblError.Text = ex.Message
#Else
            lblError.Visible = True
            lblError.Text = "An error occured while splitting this document."
#End If

        Finally
            'ReturnScripts.Add(lblError)
        End Try

    End Sub

End Class